---
title: Libretro Flycast
description: 
published: true
date: 2021-09-15T10:25:46.019Z
tags: libretro, naomi, flycast
editor: markdown
dateCreated: 2021-05-21T08:14:11.839Z
---

**Libretro Flycast** est un émulateur **Sega Dreamcast multiplate-formes** capable d'émuler les systèmes suivants :

* **NAOMI M1** 
* **NAOMI M2** 
* **NAOMI M4** 

## ![](/emulators/license.svg) Licence

Ce core est sous licence [**GPLv2**](https://github.com/libretro/flycast/blob/master/LICENSE).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ✅ | ✅ | ✅ | ✅ | ❌ | ✅ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |
| Redémarrage | ✔ |
| Captures d'écran | ✔ |
| Sauvegardes | ✔ |
| Options du core | ✔ |
| Cheats RetroArch | ✔ |
| Controls | ✔ |
| Remapping | ✔ |
| Vibration | ✔ |
| Contrôle du disque | ✔ |

## ![](/emulators/bios.svg) BIOS

###  Liste des bios obligatoires

| Nom de fichier | Description | MD5 | Fourni |
| :---: | :---: | :---: | :---: |
| naomi.zip | Format MAME à partir du Romset MAME 0.154 | eb4099aeb42ef089cfe94f8fe95e51f6 | ❌ |

### Liste des bios optionnels

| Nom du fichier bios | Description | MD5 | Fourni |
| :---: | :---: | :---: | :---: |
| airlbios.zip | BIOS Naomi pour le jeu « Airline Pilots Deluxe » de MAME | 3f348c88af99a40fbd11fa435f28c69d | ❌ |
| hod2bios.zip | BIOS Naomi pour le jeu « The House of the Dead 2 » de MAME | 9c755171b222fb1f4e1439d5b709dbf1 | ❌ |

### Emplacement

Placez les bios comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 dc
┃ ┃ ┃ ┃ ┣ 🗒 **naomi.zip**
┃ ┃ ┃ ┃ ┣ 🗒 **airlbios.zip**
┃ ┃ ┃ ┃ ┣ 🗒 **hod2bios.zip**

## ![](/emulators/roms.png) Roms

Flycast se base sur le Romset de **Mame**  mais aussi sur des format **nullDC** pour sa partie Naomi.

### Romset Mame

Seul les roms Naomi **issue d'un romset MAME 0.135 ou supérieur** sont compatible !   
Nous vous conseillons le dernier romset 0.230 qui apportera son lot de compatibilité supplémentaire !
Pour plus d'info sur la version du Romset en cour : [MameDev](https://www.mamedev.org/release.html).

>Le fichier dat afin de trier vos roms arcade est disponible dans le dossier :`/recalbox/share/bios/dc/naomi-0.235_noclones_v1.0_barhi.dat`
{.is-info}

Vous pouvez aussi le télécharger ci-dessous :

[naomi-0.235_noclones_v1.0_barhi.dat](/emulators/arcade/naomi-0.235_noclones_v1.0_barhi.dat)

### Romset NullDC

Ces roms sont compatibles avec Flycast mais moins fiable que des roms issue d'un Romset Mame.

>Les Roms **NullDC** sont en format : _`.bin + .lst`_
{.is-info}

Exemple pour le jeu `cfield.zip` :

* `/recalbox/share/roms/naomi/cfield/...`
* `/recalbox/share/roms/naomi/cfield/cfield.bin`
* `/recalbox/share/roms/naomi/cfield/cfield.lst`

### Emplacement

Placez les roms comme ceci : 

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 naomi
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

### Accéder aux options

Vous pouvez configurer diverses options de deux façons différentes.

* Via le Menu RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via le fichier `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Options du core

| Options | Valeur des options | Variable (pour surcharge) | Valeurs des variables |
| --- | --- | --- | --- |
| System Type (Restart) | `Auto` ✅ / `Dreamcast` / `NAOMI` / `Atomiswave` | `flycast_system` | `auto` / `dreamcast` / `naomi` / `atomiswave` |
| Internal resolution (restart) | `320x240` ✅ / `640x480` / `800x600` / `960x720` / `1024x768` / `1280x960` / `1440x1080` / `1600x1200` / `1920x1440` / `2560x1920` / `2880x2160` / `3200x2400` / `3840x2880` / `4480x3360` / `5120x3840` / `5760x4320 / ``6400x4800` / `7040x5280` / `7680x5760` / `8320x6240` / `8960x6720` / `9600x7200` / `10240x7680` / `10880x8160` / `11520x8640` / `12160x9120` / `12800x9600` | `flycast_internal_resolution` | `320x240` / `640x480` / `800x600` / `960x720` / `1024x768` / `1280x960` / `1440x1080` / `1600x1200` / `1920x1440` / `2560x1920` / `2880x2160` / `3200x2400` / `3840x2880` / `4480x3360` / `5120x3840` / `5760x4320` / `6400x4800` / `7040x5280` / `7680x5760` / `8320x6240` / `8960x6720` / `9600x7200` / `10240x7680` / `10880x8160` / `11520x8640` / `12160x9120` / `12800x9600` |
| Screen Orientation | `Horizontal` ✅ / `Vertical` | `flycast_screen_rotation` | `horizontal` / `vertical` |
| Alpha Sorting | `Per-Strip (fast, least accurate)` / `Per-Triangle (normal)` ✅ | `flycast_alpha_sorting` | `per-strip (fast, least accurate)` / `per-triangle (normal)` |
| Mipmapping | `Enabled` ✅ / `Disabled` | `flycast_mipmapping` | `enabled` / `disabled` |
| Fog Effects | `Enabled` ✅ / `Disabled` | `flycast_fog` | `enabled` / `disabled` |
| Volume Modifier | `Enabled` ✅ / `Disabled` | `flycast_volume_modifier_enable` | `enabled` / `disabled` |
| Widescreen Hack (Restart) | `Disabled` ✅ / `Enabled` | `flycast_widescreen_hack` | `disabled` / `enabled` |
| Widescreen Cheats (Restart) | `Disabled` ✅ / `Enabled` | `flycast_widescreen_cheats` | `disabled` / `enabled` |
| Region | `Default` ✅ / `Japan` / `USA` / `Europe` | `flycast_region` | `Default` / `Japan` / `USA` / `Europe` |
| DIV Matching | `Disabled` / `Auto` ✅ | `flycast_div_matching` | `disabled` / `auto` |
| Analog Stick Deadzone | `0%` / `5%` / `10%` / `15%` ✅ / `20%` / `25%` / `30%` | `flycast_analog_stick_deadzone` | `0%` / `5%` / `10%` / `15%` / `20%` / `25%` / `30%` |
| Trigger Deadzone | `0%` ✅ / `5%` / `10%` / `15%` / `20%` / `25%` / `30%` | `flycast_trigger_deadzone` | `0%` / `5%` / `10%` / `15%` / `20%` / `25%` / `30%` |
| Digital Triggers | `Disabled` ✅ / `Enabled` | `flycast_digital_triggers` | `disabled` / `enabled` |
| Enable DSP | `Disabled`  / `Enabled` ✅ | `flycast_enable_dsp` | `disabled` / `enabled` |
| Anisotropic Filtering | `Off` / `2` / `4` ✅ / `8` / `16` | `flycast_anisotropic_filtering` | `disabled` / `2` / `4` / `8` / `16` |
| PowerVR2 Post-processing Filter | `Disabled` ✅ / `Enabled` | `flycast_pvr2_filtering` | `disabled` / `enabled` |
| Texture Upscaling (xBRZ) | `Off` ✅ / `2` / `4` / `6` | `flycast_texupscale` | `disabled` / `2` / `4` / `6` |
| Enable RTT (Render To Texture) Buffer | `Disabled` ✅ / `Enabled` | `flycast_enable_rttb` | `disabled` / `enabled` |
| Render To Texture Upscaling | `1x` ✅ / `2x` / `3x` / `4x` / `8x` | `flycast_render_to_texture_upscaling` | `1x` / `2x` / `3x` / `4x` / `8x` |
| Threaded Rendering (Restart) | `Disabled`  / `Enabled` ✅ | `flycast_threaded_rendering` | `disabled` / `enabled` |
| Synchronous Rendering | `Disabled`  / `Enabled` ✅ | `flycast_synchronous_rendering` | `disabled` / `enabled` |
| Delay Frame Swapping | `Disabled` ✅ / `Enabled` | `flycast_delay_frame_swapping` | `disabled` / `enabled` |
| Frame Skipping | `Disabled` ✅ / `1` / `2` / `3` / `4` / `5` / `6` | `flycast_frame_skipping` | `off` / `1` / `2` / `3` / `4` / `5` / `6` |
| Allow NAOMI Service Buttons | `Disabled` ✅ / `Enabled` | `flycast_allow_service_buttons` | `disabled` / `enabled` |
| Enable NAOMI 15KHz Dipswitch | `Disabled` ✅ / `Enabled` | `flycast_enable_naomi_15khz_dipswitch` | `disabled` / `enabled` |
| Load Custom Textures | `Disabled` ✅ / `Enabled` | `flycast_custom_textures` | `disabled` / `enabled` |
| Dump Textures | `Disabled` ✅ / `Enabled` | `flycast_dump_textures` | `disabled` / `enabled` |
| Show Light Gun Settings | `Enabled` / `Disabled` ✅ | `flycast_show_lightgun_settings` | `enabled` / `disabled` |
| Gun Crosshair 1 Display | `Disabled` ✅ / `White` / `Red` / `Green` / `Blue` | `flycast_lightgun1_crosshair` | `disabled` / `White` / `Red` / `Green` / `Blue` |
| Gun Crosshair 2 Display | `Disabled` ✅ / `White` / `Red` / `Green` / `Blue` | `flycast_lightgun2_crosshair` | `disabled` / `White` / `Red` / `Green` / `Blue` |
| Gun Crosshair 3 Display | `Disabled` ✅ / `White` / `Red` / `Green` / `Blue` | `flycast_lightgun3_crosshair` | `disabled` / `White` / `Red` / `Green` / `Blue` |
| Gun Crosshair 4 Display | `Disabled` ✅ / `White` / `Red` / `Green` / `Blue` | `flycast_lightgun4_crosshair` | `disabled` / `White` / `Red` / `Green` / `Blue` |

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/libretro/flycast/](https://github.com/libretro/flycast/)
* **Documentation Libretro** : [https://docs.libretro.com/library/flycast/](https://docs.libretro.com/library/flycast/)
* **Code source officiel** : [https://github.com/flyinghead/flycast/](https://github.com/flyinghead/flycast)