---
title: Libretro MAME 2003
description: 
published: true
date: 2021-09-15T07:56:20.353Z
tags: libretro, mame, mame2003
editor: markdown
dateCreated: 2021-05-21T08:13:38.603Z
---

**Libretro MAME2003** est un émulateur plus récent que imame4all. Beaucoup plus de jeux sont fonctionnels.

## ![](/emulators/license.svg) Licence

Ce core est sous licence [**MAME non-commerciale**](https://github.com/libretro/mame2003-libretro/blob/master/LICENSE.md).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |
| Sauvegardes | ✔ |
| Sauvegardes instantanées | Selon les jeux |
| Rembobinage | ✔ |
| Netplay | ✔ |
| Native Cheats | ✔ |
| Controllers | ✔ |
| Multi-Mouse | ✔ |

## ![](/emulators/bios.svg) BIOS

Les romsets BIOS ne sont pas nécessaires lorsque vous utilisez des romsets d'arcade "Full Non-Merged". Pour les roms "Split" et "Non-Merged", placez les BIOS nécessaires dans le même répertoire que leromset du jeu.

>**Remarque** :
>Placez les bios dans le répertoire : `/recalbox/share/roms/mame/`   
>ou dans son sous-répertoire si vous utilisez plusieurs cores de Mame, par exemple :`/recalbox/share/roms/mame/Mame 2003/`
{.is-warning}

## ![](/emulators/roms.png) Roms

* Basé sur le romset : MAME 0.78 (Décembre 2003)
* Taille : 28gb
* Romsets émulés : 4 721 (incluant les clones, etc...)
* Active Sets : 4721
* Parents : 1042
* Clones : 2039
* Autres : 1624
* BIOS : 16
* CHDs : 30
* Samples : 2013
* Fichier dat : [mame2003.xml](https://github.com/libretro/mame2003-libretro/blob/master/metadata/mame2003.xml)

### Emplacement

Placez les roms comme ceci : 

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 mame
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**
┃ ┃ ┃ ┃ ┣ 📁 **jeu**
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **jeu.chd**

Vous pouvez aussi opter pour un sous répertoire (utile si vous voulez avoir un set mame pour un autre core).

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 mame
┃ ┃ ┃ ┃ ┣ 📁 Mame 2003
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**
┃ ┃ ┃ ┃ ┃ ┣ 📁 **jeu**
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 **jeu.chd**

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

### Accéder aux options

Vous pouvez configurer diverses options de deux façons différentes.

* Via le Menu RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via le fichier `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg** 

### Options du core

## ![](/emulators/external-links.png) Liens externes

* **Progettosnaps** : [https://www.progettosnaps.net/](https://www.progettosnaps.net/)
* **Code source utilisé** : [https://github.com/libretro/mame2003-libretro/](https://github.com/libretro/mame2003-libretro)
* **Documentation Libretro** : [https://docs.libretro.com/library/mame_2003/](https://docs.libretro.com/library/mame_2003/)