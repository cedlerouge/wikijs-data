---
title: Libretro MAME 2003 Plus
description: 
published: true
date: 2021-09-15T07:57:05.660Z
tags: libretro, mame, mame2003+, mame2003plus
editor: markdown
dateCreated: 2021-05-21T08:13:31.316Z
---

**Libretro MAME2003_Plus** (également appelé MAME 2003+ et mame2003-plus) est un noyau d'émulateur de système arcade **Libretro** qui met l'accent sur des performances élevées et une compatibilité étendue avec les périphériques mobiles, les ordinateurs à carte unique, les systèmes intégrés et autres plates-formes similaires.

Afin de tirer parti des performances et des exigences matérielles moindres d'une architecture MAME antérieure, MAME 2003-Plus a commencé avec le code base de MAME 2003, lui-même dérivé de MAME 0.78.

Sur cette base, les contributeurs de MAME 2003-Plus ont rétro porté le support de plusieurs centaines de jeux supplémentaires, ainsi que d’autres fonctionnalités non présentes à l’origine dans MAME 0.78.

## ![](/emulators/license.svg) Licence

Ce core est sous licence [**MAME non-commerciale**](https://github.com/libretro/mame2003-plus-libretro/blob/master/LICENSE.md).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |
| Sauvegardes | ✔ |
| Sauvegardes instantanées | Selon les jeux |
| Rembobinage | ✔ |
| Netplay | ✔ |
| Native Cheats | ✔ |
| Controllers | ✔ |
| Multi-Mouse | ✔ |

## ![](/emulators/bios.svg) BIOS

Les romsets BIOS ne sont pas nécessaires lorsque vous utilisez des romsets d'arcade "Full Non-Merged". Pour les roms "Split" et "Non-Merged", placez le BIOS dans le même répertoire que le romset du jeu.

>**Remarque** :
>Placez les bios dans le répertoire : `/recalbox/share/roms/mame/`   
>ou dans son sous-répertoire si vous utilisez plusieurs cores de Mame :`/recalbox/share/roms/mame/Mame 2003-Plus/`
{.is-warning}

## ![](/emulators/roms.png) Roms

* Basé sur le romset : 0.78-0.188 (MAME 0.78 en tant que ligne de base avec d'autres ROMs basées sur des jeux de roms MAME ultérieurs)
* Taille : 32gb
* Romsets émulés : 4941 (incluant les clones, etc...)
* Active Sets : 4941
* Parents : 1089
* Clones : 2123
* Autres : 1713
* BIOS : 16
* CHDs : 30
* Samples : 66 + 6 Optional "Soundtrack Samples"
* Fichier dat : [mame2003-plus.xml](https://github.com/libretro/mame2003-plus-libretro/blob/master/metadata/mame2003-plus.xml)

### Emplacement

Placez les roms comme ceci : 

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 mame
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**
┃ ┃ ┃ ┃ ┣ 📁 **jeu**
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **jeu.chd**

Vous pouvez aussi opter pour un sous répertoire (utile si vous voulez avoir un set mame pour un autre core).

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 mame
┃ ┃ ┃ ┃ ┣ 📁 Mame 2003-Plus
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**
┃ ┃ ┃ ┃ ┃ ┣ 📁 **jeu**
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 **jeu.chd**

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

### Accéder aux options

Vous pouvez configurer diverses options de deux façons différentes.

* Via le menu RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via le fichier `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Options du core

## ![](/emulators/external-links.png) Liens externes

* **Progettosnaps** : [https://www.progettosnaps.net/](https://www.progettosnaps.net/)
* **Code source utilisé** : [https://github.com/libretro/mame2003-plus-libretro/](https://github.com/libretro/mame2003-plus-libretro)
* **Documentation Libretro** : [https://docs.libretro.com/library/mame2003\_plus/](https://docs.libretro.com/library/mame2003_plus/)