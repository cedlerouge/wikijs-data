---
title: Libretro FBNeo
description: 
published: true
date: 2021-09-15T07:52:12.420Z
tags: libretro, fbneo, finalburn, neo
editor: markdown
dateCreated: 2021-05-21T08:13:03.717Z
---

**FinalBurn Neo**, un émulateur pour les jeux d'arcade et certaines consoles. Il est basé sur les émulateurs FinalBurn et les anciennes versions de [MAME](https://www.mamedev.org/) (émulant moins de bornes d'arcade) mais il fonctionne suivant les mêmes principes.

FB Neo (a.k.a. FinalBurn Neo) est un émulateur d'arcade qui supporte les plateformes suivantes :

* Capcom CPS-1
* Capcom CPS-2
* Capcom CPS-3
* Cave
* Jeux basé sur Data East DEC-0, DEC-8, Cassette System, DECO IC16 et DECO-32
* Matériel bsé sur Galaxian
* Matériel Irem M62, M63, M72, M90, M92 et M107
* Kaneko 16
* Konami
* Namco Mappy, System 86, System 1 & 2 et autres
* Neo-Geo
* NMK16
* Matériel basé sur Pacman
* PGM
* Matériel basé sur Psikyo 68EC020 et SH-2
* Sega System 1, System 16 (et similaire), System 18, X-Board et Y-Board
* Matériel basé sur Seta-Sammy-Visco (SSV)
* Super Kaneko Nova System
* Toaplan 1
* Toaplan 2
* Taito F2, F3, X, Z et autres
* Divers pilotes pour d'autres matériels
* FB Neo contient aussi des pilotes en cours de travail pour les système suivants :
  * Sega Megadrive/Genesis
  * ColecoVision
  * Sega SG-1000
  * Sega Master System
  * Game Gear
  * MSX-1
  * ZX Spectrum
  * PC-Engine
  * TurboGrafx 16
  * SuperGrafx
  * Fairchild Channel F.

## ![](/emulators/license.svg) Licence

Ce core est sous licence [**non commerciale**](https://github.com/finalburnneo/FBNeo/blob/master/src/license.txt).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |
| Sauvegardes | ✔ |
| Sauvegardes instantanées | ✔ |
| Rembobinage | ✔ |
| Netplay | ✔ |
| RetroAchievements | ✔ |
| Cheats RetroArch | ✔ |
| Cheats natifs | ✔ |
| Controllers | ✔ |
| Multi-Mouse | ✔ |
| Subsystem | ✔ |

## ![](/emulators/bios.svg) Bios

### Liste des bios

En fonction des jeux, certains bios seront nécessaires et seront à placer dans le même répertoire que les jeux.

### Emplacement

Placez les bios comme ceci : 

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 fbneo
┃ ┃ ┃ ┃ ┣ 🗒 **bios.zip**

## ![](/emulators/roms.png) Roms

### Extensions supportées

Tous les jeux FinalBurn Neo utilisent les jeux au formats suivants :

* .fba
* .zip
* .7z

### Emplacement

Placez les roms comme ceci : 

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 fbneo
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

>Pour plus d'information sur les roms, rendez-vous sur [ce tutoriel](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuration avancée des émulateurs

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

### Accéder aux options

Vous pouvez configurer diverses options de deux façons différentes.

* Via le Menu RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via le fichier `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Options du core



## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/libretro/FBNeo/tree/master](https://github.com/libretro/FBNeo/tree/master)
* **Documentation Libretro** : [https://docs.libretro.com/library/fbneo/](https://docs.libretro.com/library/fbneo/)
* **Site officiel** : [https://neo-source.com/](https://neo-source.com/)
* **Wiki du core** : [http://emulation.gametechwiki.com/index.php/FinalBurn_Neo](http://emulation.gametechwiki.com/index.php/FinalBurn_Neo)
* **Wiki officiel** : [https://github.com/finalburnneo/FBNeo/wiki](https://github.com/finalburnneo/FBNeo/wiki)