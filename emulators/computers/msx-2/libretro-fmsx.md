---
title: Libretro FMSX
description: 
published: true
date: 2021-10-03T16:50:37.023Z
tags: msx2, ordinateurs
editor: markdown
dateCreated: 2021-05-21T08:17:18.649Z
---



## ![](/emulators/license.svg) Licence

Ce core est sous licence [**non commerciale**](https://github.com/libretro/fmsx-libretro/blob/master/LICENSE).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités



## ![](/emulators/bios.svg) BIOS

### Liste des bios obligatoires

| Nom de fichier | Description | MD5 | Fourni |
| :---: | :---: | :---: | :---: |
| CARTS.SHA | - | d6dedca1112ddfda94cc9b2e426b818b | ✅ |
| CYRILLIC.FNT | - | 85b38e4128bbc300e675f55b278683a8 | ✅ |
| DISK.ROM | DiskROM/BDOS | 80dcd1ad1a4cf65d64b7ba10504e8190 | ✅ |
| FMPAC16.ROM | - | af8537262df8df267072f359399a7635 | ✅ |
| FMPAC.ROM | FMPAC BIOS | 6f69cc8b5ed761b03afd78000dfb0e19 | ✅ |
| ITALIC.FNT | - | c83e50e9f33b8dd893c414691822740d | ✅ |
| KANJI.ROM | Kanji Font | febe8782b466d7c3b16de6d104826b34 | ✅ |
| MSX2EXT.ROM | MSX2 ExtROM | 2183c2aff17cf4297bdb496de78c2e8a | ✅ |
| MSX2PEXT.ROM | MSX2+ ExtROM | 7c8243c71d8f143b2531f01afa6a05dc | ✅ |
| MSX2P.ROM | MSX2+ BIOS | 6d8c0ca64e726c82a4b726e9b01cdf1e | ✅ |
| MSX2.ROM | BIOS MSX2 | ec3a01c91f24fbddcbcab0ad301bc9ef | ✅ |
| MSXDOS2.ROM | MSX-DOS 2 | 6418d091cd6907bbcf940324339e43bb | ✅ |
| MSX.ROM | BIOS MSX | aa95aea2563cd5ec0a0919b44cc17d47 | ✅ |
| PAINTER.ROM | Yamaha Painter | 403cdea1cbd2bb24fae506941f8f655e | ✅ |
| RS232.ROM | - | 279efd1eae0d358eecd4edc7d9adedf3 | ✅ |

### Emplacement

Placez les BIOS comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 🗒 **CARTS.SHA**
┃ ┃ ┃ ┣ 🗒 **CYRILLIC.FNT**
┃ ┃ ┃ ┣ 🗒 **DISK.ROM**
┃ ┃ ┃ ┣ 🗒 **FMPAC16.ROM**
┃ ┃ ┃ ┣ 🗒 **FMPAC.ROM**
┃ ┃ ┃ ┣ 🗒 **ITALIC.FNT**
┃ ┃ ┃ ┣ 🗒 **KANJI.ROM**
┃ ┃ ┃ ┣ 🗒 **MSX2EXT.ROM**
┃ ┃ ┃ ┣ 🗒 **MSX2PEXT.ROM**
┃ ┃ ┃ ┣ 🗒 **MSX2P.ROM**
┃ ┃ ┃ ┣ 🗒 **MSX2.ROM**
┃ ┃ ┃ ┣ 🗒 **MSXDOS2.ROM**
┃ ┃ ┃ ┣ 🗒 **MSX.ROM**
┃ ┃ ┃ ┣ 🗒 **PAINTER.ROM**
┃ ┃ ┃ ┣ 🗒 **RS232.ROM**

## ![](/emulators/roms.png) Roms

### Extensions supportées

Les roms doivent avoir les extensions suivantes 

* .rom
* .mx1
* .mx2
* .dsk
* .cas
* .zip
* .7z

Ce système supporte les roms compressées au format .zip/.7z. Attention toutefois, il ne s'agit que d'une archive.

Les fichiers contenus dans les .zip/.7z doivent correspondre aux extensions citées précédemment.  
De plus, chaque fichier .zip/.7z ne doit contenir qu'une seule rom compressée.

### Emplacement

Placez les roms comme ceci : 

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 msx2
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

>Les roms au format **No-Intro** sont vivement conseillées.
{.is-success}

>Pour plus d'information sur les roms, rendez-vous sur [ce tutoriel](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

### Accéder aux options

Vous pouvez configurer diverses options de deux façons différentes.

* Via le menu RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via le fichier `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Options du core

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/libretro/fmsx-libretro/](https://github.com/libretro/fmsx-libretro/)
* **Documentation Libretro** : [https://docs.libretro.com/library/fmsx/](https://docs.libretro.com/library/fmsx/)