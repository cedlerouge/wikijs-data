---
title: Libretro BK
description: 
published: true
date: 2021-10-03T16:47:20.464Z
tags: libretro, elektronika, bk, 7.2+
editor: markdown
dateCreated: 2021-05-21T08:16:48.484Z
---



## ![](/emulators/license.svg) Licence

Ce core est sous licence [**personnalisé**](https://github.com/libretro/bk-emulator/blob/master/COPYING).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅  | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités



## ![](/emulators/bios.svg) BIOS

### Liste des bios obligatoires

| Nom de fichier | Description | MD5 | Fourni |
| :---: | :---: | :---: | :---: |
| B11M_BOS.ROM | Requis | fe4627d1e3a1535874085050733263e7 | ❌ |
| B11M_EXT.ROM | Requis | dc52f365d56fa1951f5d35b1101b9e3f | ❌ |
| BAS11M_0.ROM | Requis | 946f6f23ded03c0e26187f0b3ca75993 | ❌ |
| BAS11M_1.ROM | Requis | 1e6637f32aa7d1de03510030cac40bcf | ❌ |
| DISK_327.ROM | Requis | 5015228eeeb238e65da8edcd1b6dfac7 | ❌ |
| FOCAL10.ROM | Requis | 5737f972e8638831ab71e9139abae052 | ❌ |
| MONIT10.ROM | Requis | 95f8c41c6abf7640e35a6a03cecebd01 | ❌ |

### Emplacement

Placez les bios comme ceci : 

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 bk
┃ ┃ ┃ ┃ ┣ 🗒 **B11M_BOS.ROM**
┃ ┃ ┃ ┃ ┣ 🗒 **B11M_EXT.ROM**
┃ ┃ ┃ ┃ ┣ 🗒 **BAS11M_0.ROM**
┃ ┃ ┃ ┃ ┣ 🗒 **BAS11M_1.ROM**
┃ ┃ ┃ ┃ ┣ 🗒 **DISK_327.ROM**
┃ ┃ ┃ ┃ ┣ 🗒 **FOCAL10.ROM**
┃ ┃ ┃ ┃ ┣ 🗒 **MONIT10.ROM**

## ![](/emulators/roms.png) Roms

### Extensions supportées

Les roms doivent avoir les extensions suivantes :

* .bin
* .zip

Ce système supporte les roms compressées au format .zip/.7z. Attention toutefois, il ne s'agit que d'une archive.

Les fichiers contenus dans les .zip/.7z doivent correspondre aux extensions citées précédemment.
De plus, chaque fichier .zip/.7z ne doit contenir qu'une seule rom compressée.

### Emplacement

Placez les roms comme ceci : 

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 bk
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

>Les roms au format **TOSEC** sont vivement conseillées.
{.is-success}

>Pour plus d'information sur les roms, rendez-vous sur [ce tutoriel](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

### Accéder aux options

Vous pouvez configurer diverses options de deux façons différentes.

* Via le menu RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via le fichier `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Options du core

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/libretro/bk-emulator/](https://github.com/libretro/bk-emulator/)