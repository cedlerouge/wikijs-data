---
title: Libretro DOSBox Pure
description: 
published: true
date: 2021-10-03T16:43:54.847Z
tags: libretro, dos, msdos, dosbox, pure, 7.2+
editor: markdown
dateCreated: 2021-05-21T08:16:42.176Z
---



## ![](/emulators/license.svg) Licence

Ce core est sous licence [**GPLv2**](https://gitlab.com/recalbox/packages/libretro/libretro-dosbox-pure/-/blob/main/LICENSE).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités



## ![](/emulators/bios.svg) BIOS

>**Aucun bios n'est requis.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensions supportées

Les roms doivent avoir les extensions suivantes :

* .zip

### Emplacement

Placez les roms comme ceci : 

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 dos
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](/fr/advanced-usage/configuration-override).
{.is-info}

### Accéder aux options

Vous pouvez configurer diverses options de deux façons différentes.

* Via le menu RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via le fichier `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Options du core



## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://gitlab.com/recalbox/packages/libretro/libretro-dosbox-pure/](https://gitlab.com/recalbox/packages/libretro/libretro-dosbox-pure/)
* **Code source original** : [https://github.com/libretro/dosbox-pure](https://github.com/libretro/dosbox-pure)/