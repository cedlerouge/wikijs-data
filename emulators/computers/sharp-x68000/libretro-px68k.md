---
title: Libretro PX68k
description: 
published: true
date: 2021-10-03T17:00:49.479Z
tags: libretro, x68000, px68k
editor: markdown
dateCreated: 2021-05-21T08:18:07.836Z
---



## ![](/emulators/license.svg) Licence

Ce core est sous licence [**GPLv2**](https://github.com/libretro/px68k-libretro/blob/master/COPYING).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités



## ![](/emulators/bios.svg) BIOS

### Liste des bios obligatoires

| Nom de fichier | Description | MD5 | Fourni |
| :---: | :---: | :---: | :---: |
| iplrom.dat | BIOS X68000 | 7fd4caabac1d9169e289f0f7bbf71d8e | ✅ |
| cgrom.dat | Fichier police | cb0a5cfcf7247a7eab74bb2716260269 | ✅ |

### Emplacement

Placez les BIOS comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 keropi
┃ ┃ ┃ ┃ ┣ 🗒 **iplrom.dat**
┃ ┃ ┃ ┃ ┣ 🗒 **cgrom.dat**

### Liste des bios optionnels

| Nom de fichier | Description | MD5 | Fourni |
| :---: | :---: | :---: | :---: |
| iplrom30.dat | BIOS X68000 #2 | f373003710ab4322642f527f567e020a | ⚠️ |
| iplromco.dat | BIOS X68000 #3 | cc78d4f4900f622bd6de1aed7f52592f | ⚠️ |
| iplromxv.dat | BIOS X68000 #4 | 0617321daa182c3f3d6f41fd02fb3275 | ⚠️ |

⚠️ Fourni mais n'est pas le bon bios.

### Emplacement

Placez les bios comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 keropi
┃ ┃ ┃ ┃ ┣ 🗒 **iplrom30.dat**
┃ ┃ ┃ ┃ ┣ 🗒 **iplromco.dat**
┃ ┃ ┃ ┃ ┣ 🗒 **iplromxv.dat**

## ![](/emulators/roms.png) Roms

### Extensions supportées

Les roms doivent avoir les extensions suivantes :

* .dim
* .img
* .d88
* .88d
* .hdm
* .dup
* .2hd
* .xdf
* .hdf
* .cmd
* .m3u
* .zip
* .7z

Ce système supporte les roms compressées au format .zip/.7z. Attention toutefois, il ne s'agit que d'une archive.

Les fichiers contenus dans les .zip/.7z doivent correspondre aux extensions citées précédemment.
De plus, chaque fichier .zip/.7z ne doit contenir qu'une seule rom compressée.

### Emplacement

Placez les roms comme ceci : 

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 x68000
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

>Les roms au format **TOSEC** sont vivement conseillées.
{.is-success}

>Pour plus d'information sur les roms, rendez-vous sur [ce tutoriel](/fr/tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

### Accéder aux options

Vous pouvez configurer diverses options de deux façons différentes.

* Via le menu RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via le fichier `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Options du core

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/libretro/px68k-libretro/](https://github.com/libretro/px68k-libretro/)
* **Documentation Libretro** : [https://docs.libretro.com/library/px68k/](https://docs.libretro.com/library/px68k/)