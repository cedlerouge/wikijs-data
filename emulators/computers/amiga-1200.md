---
title: Amiga 1200
description: 
published: true
date: 2021-10-03T15:58:05.174Z
tags: amiga-1200, amiga, 1200, commodore, ordinateurs
editor: markdown
dateCreated: 2021-05-21T07:55:31.933Z
---

![](/emulators/computers/amiga1200.svg){.align-center}

## Fiche technique

* **Fabricant** : Commodore
* **Année de sortie** : 1992
* **Système d’exploitation** : AmigaOS 3.0/3.1
* **Processeur** : Motorola 68EC020 running @ 14.32 MHz
* **RAM** : 2 MB
* **ROM** : 512 KB Kickstart 3.0-3.1 ROM 
* **Processeur graphique** : Advanced Graphics Architecture
* **Puce sonore** : 4 canaux 8-bit PCM
* **Résolution** : 320x200 à 1504x484 (16.8 million de couleurs)

## Présentation

L'**Amiga 1200**, ou **A1200**, était le micro-ordinateur Amiga de troisième génération de Commodore International, destiné au marché familial. il sortit en octobre 1992, au prix de 399 £ au Royaume-Uni et 599 $ aux États-Unis.

Comme ses prédécesseurs, l'Amiga 500 et l'Amiga 600 (dont il reprend d'ailleurs le design de ce dernier, un pavé numérique en plus), il était conçu comme un « tout en un », incorporant processeurs, clavier, lecteur de disquettes, dans une seule unité. Il était vendu avec 2 Mio de mémoire, la troisième génération de chipset graphique Amiga (AGA), et l'AmigaOS 3.0. Il utilise un processeur Motorola MC68EC020 cadencé à 14 MHz. En extension, l'Amiga 1200 comporte un slot mémoire/CPU et un port PCMCIA Type II1.

Comme la mémoire est partagée entre le processeur principal et les coprocesseurs vidéo et sonore, étendre la mémoire au-delà des 2 Mio augmente beaucoup les performances de la machine. Plusieurs constructeurs tiers proposaient des accélérateurs contenant des processeurs 68020, 68030, 68040, 68060 et, sur le tard, des PowerPC, ce qui augmente encore significativement la puissance de la machine et nécessite parfois de remonter l'Amiga en tour, afin d'éviter une surchauffe de la carte-mère originelle.

## Émulateurs

[Amiberry](amiberry)
[Libretro PUAE](libretro-puae)