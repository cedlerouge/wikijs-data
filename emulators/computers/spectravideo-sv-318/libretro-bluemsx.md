---
title: Libretro blueMSX
description: 
published: true
date: 2021-10-03T17:02:28.323Z
tags: libretro, bluemsx, sv-318
editor: markdown
dateCreated: 2021-05-21T08:18:13.833Z
---



## ![](/emulators/license.svg) Licence

Ce core est sous licence [**GPLv2**](https://github.com/libretro/blueMSX-libretro/blob/master/license.txt).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités



## ![](/emulators/bios.svg) BIOS

### Liste des bios obligatoires

| Nom de fichier | Description | MD5 | Fourni |
| :---: | :---: | :---: | :---: |
| svi318.rom | - | ee6ad7ea29e791b03a28a9443e622648 | ✅ |
| svi328a.rom | - | 352f054ab09605070bdff49d73f335cc | ✅ |
| svi806.rom | - | 82f7bc9c08f43db1d79bcb565a0de12b | ✅ |
| svi328a.rom | - | 352f054ab09605070bdff49d73f335cc | ✅ |
| svi806se.rom | - | 6a5536c1eb4f0477c4f76488cd8ca3ad | ✅ |
| svi328a.rom | - | 352f054ab09605070bdff49d73f335cc | ✅ |
| svi806.rom | - | 82f7bc9c08f43db1d79bcb565a0de12b | ✅ |
| svi328.rom | - | 2cebd1db9dda475a011b7bce65c984a2 | ✅ |

### Emplacement

Placez les BIOS comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 Machines
┃ ┃ ┃ ┃ ┣ 📁 SVI - Spectravideo SVI-318
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **svi318.rom**
┃ ┃ ┃ ┃ ┣ 📁 SVI - Spectravideo SVI-328 80 Column
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **svi328a.rom**
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **svi806.rom**
┃ ┃ ┃ ┃ ┣ 📁 SVI - Spectravideo SVI-328 80 Swedish
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **svi328a.rom**
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **svi806se.rom**
┃ ┃ ┃ ┃ ┣ 📁 SVI - Spectravideo SVI-328 MK2
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **svi328a.rom**
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **svi806.rom**
┃ ┃ ┃ ┃ ┣ 📁 SVI - Spectravideo SVI-328
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **svi328.rom**

## ![](/emulators/roms.png) Roms

### Extensions supportées

Les roms doivent avoir les extensions suivantes :

* .cas
* .bin
* .zip

Ce système supporte les roms compressées au format .zip. Attention toutefois, il ne s'agit que d'une archive.

Les fichiers contenus dans les .zip doivent correspondre aux extensions citées précédemment.
De plus, chaque fichier .zip ne doit contenir qu'une seule rom compressée.

### Emplacement

Placez les roms comme ceci : 

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 spectravideo
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

>Les roms au format **TOSEC** sont vivement conseillées.
{.is-success}

>Pour plus d'information sur les roms, rendez-vous sur [ce tutoriel](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

### Accéder aux options

Vous pouvez configurer diverses options de deux façons différentes.

* Via le menu RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via le fichier `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Options du core

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/libretro/blueMSX-libretro/](https://github.com/libretro/blueMSX-libretro/)
* **Documentation Libretro** : [https://docs.libretro.com/library/bluemsx/](https://docs.libretro.com/library/bluemsx/)