---
title: BBC Micro
description: 
published: false
date: 2021-10-03T16:27:35.465Z
tags: 7.3+, bbc micro, ordinateurs
editor: markdown
dateCreated: 2021-09-15T08:09:09.777Z
---

![](/emulators/computers/bbcmicro.svg){.align-center}

## Fiche technique

* **Fabricant** : Acorn Computers
* **Année de sortie** : 1981
* **Système d'exploitation** : Acorn MOS
* **Processeur** : MOS Technology 6502/6512 @ 2MHz
* **RAM** : 64KB - 128 KB
* **Lecteur** : lecteur de cassettes, lecteur de disquette 5 1/4" ou 3 1/2", disque dur
* **Puce sonore** : Texas Instruments SN76489, 4 canaux, mono
* **Résolution** : 640x256, 8 couleurs (modes de framebuffer variés)

## Présentation

Le BBC Microcomputer System, ou BBC Micro, est une série de micro-ordinateurs construits par Acorn Computers Ltd pour le BBC Computer Literacy Project initié par la British Broadcasting Corporation. Créés pour une utilisation pédagogique, les ordinateurs de la gamme BBC Micro sont réputés pour leur modularité et la qualité de leurs systèmes d'exploitation.

## Émulateurs

[BeebEm](beebem)