---
title: TI-99/4A
description: 
published: false
date: 2021-10-03T17:05:59.783Z
tags: 7.3+, ti-99/4a, ordinateurs, texas instruments
editor: markdown
dateCreated: 2021-08-21T09:28:32.455Z
---

![](/emulators/computers/ti994a.svg){.align-center}

## Fiche technique

* **Fabricant** : Texas Instruments
* **Année de sortie** : 1979
* **Système d'exploitation** : Basic owner
* **Processeur** : Texas Instruments TMS9900 @ 3,3MHz
* **RAM** : 16/24 KB
* **Résolution** : 256 x 192 maximum, 16 couleurs
* **Son** : 3 voix sur 5 octaves et canal de bruit

## Présentation

Produit par Texas Instruments, le **TI-99/4** est l'un des premiers ordinateurs familiaux (début des années 1980). La version commercialisée en France est vite devenue le **TI-99/4A**.

Cet ordinateur présente quelques similarités avec le concept de console de jeux vidéo. En effet, il peut se brancher sur un téléviseur par l'intermédiaire d'une prise péritel, peut lire des cartouches et dispose d'un connecteur permettant de lui adjoindre deux manettes de jeu. Il peut également utiliser des cassettes audio pour charger ou sauvegarder des programmes à l'aide d'un magnétophone standard.

En juin 1979, la société américaine Texas Instruments présente le TI-99/4, basé sur le prototype TI-99/3. Les premiers exemplaires se vendent au mois de novembre pour 1 150 dollars. Les premiers jeux sont Connect Four, Hangman, Yahtzee et Zero Zap.

Un successeur, le TI-99/7, est prévu pour le monde professionnel mais ne verra jamais le jour.

Le TI-99/4A est présenté au grand public en juin 1981. Le « A » fait référence au nouveau processeur graphique : la puce TMS9918A. Cette dernière, contrairement au TMS9918, a un mode bitmap.

En décembre 1983, Texas Instruments annonce officiellement qu’elle cesse la production du TI-99/4A.

## Émulateurs

[TI-99/Sim](ti-99-sim)