---
title: Libretro NP2Kai
description: 
published: true
date: 2021-10-03T16:56:12.489Z
tags: libretro, pc-98, np2kai
editor: markdown
dateCreated: 2021-05-21T08:17:43.103Z
---



## ![](/emulators/license.svg) Licence

Ce core est sous licence [**MIT**](https://github.com/AZO234/NP2kai/blob/master/LICENSE).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités



## ![](/emulators/bios.svg) BIOS

### Liste des bios obligatoires

| Nom de fichier | Description | MD5 | Fourni |
| :---: | :---: | :---: | :---: |
| bios.rom | - | 052f5f6010877522f78803498a625727 0c052b1f1bfd0ece11286d2ff45d296f 36293e23fe0c86cb115d59b653c13876 3af0ae018c5710eec6e2891064814138 50274bb5dcb707e4450011b09accffcb c70ee9df11794bd5cc8aadb3721b4a03 cd237e16e7e77c06bb58540e9e9fca68 e246140dec5124c5e404869a84caefce | ❌ |
| font.bmp | Affichage du texte | 0c8624e9ced7cca8769e5c7b0dd4279b 7da1e5b7c482d4108d22a5b09631d967 | ❌ |
| font.rom | Police alternative | 38d32748ae49d1815b0614970849fd40 4133b0be0d470920da60b9ed28d2614f 693fd1da3239d4bbeafc77d211718fc5 829b963e21334e0fc2092ebd58f2ab4a add4a225048c85ca2bc588696c6ecdc5 ca87908a99ea423093f6d497fc367f7d fd0b856899aec843cbe69d2940df547f | ❌ |
| itf.rom | - | 1d295699ffeab0f0e24e09381299259d 72ea51443070f0e9212bfc9b793ee28e a13d96da03a28af8418d7f86ab951f1a b49ea39a1f730f1c966babc11961dc9a e9fc3890963b12cf15d0a2eea5815b72 | ❌ |
| sound.rom | - | 42c271f8b720e796a484cc1165ff4914 524473c1a5a03b17e21d86a0408ff827 a77fc2bc7c696dd68dba18e02f89d386 caf90f22197aed6f14c471c21e64658d | ❌ |

### Emplacement

Placez les BIOS comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 np2kai
┃ ┃ ┃ ┃ ┣ 🗒 **bios.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **font.bmp**
┃ ┃ ┃ ┃ ┣ 🗒 **font.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **itf.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **sound.rom**

### Liste des bios optionnels

| Nom de fichier | Description | MD5 | Fourni |
| :---: | :---: | :---: | :---: |
| 2608_hd.wav | YM2608 RYTHM sample | 9c6637930b1779abe00b8b63e4e41f50 d94546e70f17fd899be8df3544ab6cbb | ⚠️ |
| 2608_hh.wav | YM2608 RYTHM sample | 73548a1391631ff54a1f7c838d67917e 08c54a0c1f774a5538a848a6665a34b4 | ⚠️ |
| 2608_rim.wav | YM2608 RYTHM sample | 43d54b3e05c081fa280c9bace3af1043 465ea0768b27da404aec45dfc501404b | ⚠️ |
| 2608_sd.wav | YM2608 RYTHM sample | 08124ccb84a9f65e2affc29581e690c9 d71004351c8bbfdad53b18222c061d49 | ⚠️ |
| 2608_tom.wav | YM2608 RYTHM sample | faed5664a2dd8b1b2308e8a50ac25ea 96a4ead13f364734f79b0c58af2f0e1f | ⚠️ |
| 2608_top.wav | YM2608 RYTHM sample | 3721ace646ffd56439aebbb2154e9263 593cff6597ab9380d822b8f824fd2c28 | ⚠️ |

⚠️ Fourni mais n'est pas le bon bios.

### Emplacement

Placez les bios comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 np2kai
┃ ┃ ┃ ┃ ┣ 🗒 **2608_hd.wav**
┃ ┃ ┃ ┃ ┣ 🗒 **2608_hh.wav**
┃ ┃ ┃ ┃ ┣ 🗒 **2608_rim.wav**
┃ ┃ ┃ ┃ ┣ 🗒 **2608_sd.wav**
┃ ┃ ┃ ┃ ┣ 🗒 **2608_tom.wav**
┃ ┃ ┃ ┃ ┣ 🗒 **2608_top.wav**

## ![](/emulators/roms.png) Roms

### Extensions supportées

Les roms doivent avoir les extensions suivantes :

* .d98
* .98d
* .dcp
* .fdi
* .fdd
* .nfd
* .2hd
* .tfd
* .d88
* .88d
* .hdm
* .xdf
* .dup
* .cmd
* .hdi
* .thd
* .nhd
* .hdd
* .hdn
* .m3u
* .zip
* .7z

Ce système supporte les roms compressées au format .zip/.7z. Attention toutefois, il ne s'agit que d'une archive.

Les fichiers contenus dans les .zip/.7z doivent correspondre aux extensions citées précédemment.
De plus, chaque fichier .zip/.7z ne doit contenir qu'une seule rom compressée.

### Emplacement

Placez les roms comme ceci : 

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 pc98
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

>Les roms au format **TOSEC** sont vivement conseillées.
{.is-success}

>Pour plus d'information sur les roms, rendez-vous sur [ce tutoriel](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

### Accéder aux options

Vous pouvez configurer diverses options de deux façons différentes.

* Via le menu RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via le fichier `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Options du core

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/AZO234/NP2kai/](https://github.com/AZO234/NP2kai/)
* **Documentation Libretro** : [https://docs.libretro.com/library/neko_project_ii_kai/](https://docs.libretro.com/library/neko_project_ii_kai/)