---
title: GSPlus
description: 
published: true
date: 2021-10-03T16:11:42.749Z
tags: gsplus, apple-2gs
editor: markdown
dateCreated: 2021-05-21T08:15:24.105Z
---

**GSPlus** est un émulateur crée sur les restes de KEGS et GSPort pour émuler l'Apple ]\[GS (Apple 2GS).

## ![](/emulators/license.svg) Licence

Ce core est sous licence [**GPLv2**](https://github.com/digarok/gsplus/blob/master/LICENSE.txt).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités



## ![](/emulators/bios.svg) BIOS

### Liste des bios obligatoires

| Nom de fichier | Description | MD5 | Fourni |
| :---: | :---: | :---: | :---: |
| apple2gs.rom | - | 20a0334c447cb069a040ae5be1d938df ba89edf2729a28a17cd9e0f7a0ac9a39 | ❌ |

### Emplacement

Placez les bios comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 🗒 apple2gs.rom

## ![](/emulators/roms.png) Roms

### Extensions supportées

Les roms doivent avoir les extensions suivantes :

* .2mg
* .po
* .hdv

### Emplacement

Placez les roms comme ceci : 

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 apple2gs
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.2mg**

>Les roms au format **TOSEC** sont vivement conseillées.
{.is-success}

>Pour plus d'information sur les roms, rendez-vous sur [ce tutoriel](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/digarok/gsplus/](https://github.com/digarok/gsplus/)
* **Documentation** : [http://apple2.gs/plus/](http://apple2.gs/plus/)