---
title: Libretro vice_xscpu64
description: 
published: true
date: 2021-10-03T16:41:51.863Z
tags: libretro, commodore-64, c64, vice, xscpu64
editor: markdown
dateCreated: 2021-05-21T08:15:42.495Z
---



## ![](/emulators/license.svg) Licence

Ce core est sous licence [**GPLv2**](https://github.com/libretro/vice-libretro/blob/master/COPYING).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités



## ![](/emulators/bios.svg) BIOS

### Liste des bios optionnels

| Nom de fichier | Description | MD5 | Fourni |
| :---: | :---: | :---: | :---: |
| scpu-dos-1.4.bin | CMD SuperCPU Kernal 1.4 | cda2fcd2e1f0412029383e51dd472095 | ⚠️ |
| scpu-dos-2.04.bin | CMD SuperCPU Kernal 2.04 | b2869f8678b8b274227f35aad26ba509 | ⚠️ |

⚠️ Fourni mais n'est pas le bon bios.

### Emplacement

Placez les BIOS comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 vice
┃ ┃ ┃ ┃ ┣ 📁 SCPU64
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **scpu-dos-1.4.bin**
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **scpu-dos-2.04.bin**

## ![](/emulators/roms.png) Roms

### Extensions supportées

Les roms doivent avoir les extensions suivantes :

* .crt
* .d64
* .d71
* .d81
* .g64
* .nbz
* .nib
* .p00
* .prg
* .t64
* .tap
* .z64
* .zip
* .7z

Ce système supporte les roms compressées au format .zip/.7z. Attention toutefois, il ne s'agit que d'une archive.

Les fichiers contenus dans les .zip/.7z doivent correspondre aux extensions citées précédemment.  
De plus, chaque fichier .zip/.7z ne doit contenir qu'une seule rom compressée.

### Emplacement

Placez les roms comme ceci : 

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 c64
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

>Les roms au format **No-Intro** sont vivement conseillées.
{.is-success}

>Pour plus d'information sur les roms, rendez-vous sur [ce tutoriel](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

### Accéder aux options

Vous pouvez configurer diverses options de deux façons différentes.

* Via le menu RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via le fichier `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Options du core

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/libretro/vice-libretro/](https://github.com/libretro/vice-libretro/)
* **Documentation Libretro** : [https://docs.libretro.com/library/vice/](https://docs.libretro.com/library/vice/)