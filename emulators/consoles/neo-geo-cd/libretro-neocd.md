---
title: Libretro NeoCD
description: 
published: true
date: 2021-10-03T20:24:55.294Z
tags: libretro, neo-geo cd, neocd
editor: markdown
dateCreated: 2021-05-21T08:23:07.444Z
---

## ![](/emulators/license.svg) Licence

Ce core est sous licence [**LGPLv3**](https://github.com/libretro/neocd_libretro/blob/master/LICENSE.md).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |
|  | ✔ |

## ![](/emulators/bios.svg) BIOS

### Liste des bios obligatoires

| Nom de fichier | Description | MD5 | Fourni |
| :---: | :---: | :---: | :---: |
| neocd.bin | CDZ BIOS (MAME) | f39572af7584cb5b3f70ae8cc848aba2 | ❌ |
| uni-bioscd.rom | Universe BIOS CD 3.3 | 08ca8b2dba6662e8024f9e789711c6fc | ❌ |
| top-sp1.bin | Top Loader BIOS (MAME) | 122aee210324c72e8a11116e6ef9c0d0 | ❌ |
| front-sp1.bin | Front Loader BIOS (MAME) | 5c2366f25ff92d71788468ca492ebeca | ❌ |
| neocd_sz.rom | CDZ BIOS (SMKDAN 0.7b DEC 2010) | 971ee8a36fb72da57aed01758f0a37f5 | ❌ |
| neocd_z.rom | CDZ BIOS | 11526d58d4c524daef7d5d677dc6b004 | ❌ |
| neocd_st.rom | Top Loader BIOS (SMKDAN 0.7b DEC 2010) | f6325a33c6d63ea4b9162a3fa8c32727 | ❌ |
| neocd_t.rom | Top Loader BIOS | de3cf45d227ad44645b22aa83b49f450 | ❌ |
| neocd_sf.rom | Front Loader BIOS (SMKDAN 0.7b DEC 2010) | 043d76d5f0ef836500700c34faef774d | ❌ |
| neocd_f.rom | Front Loader BIOS | 8834880c33164ccbe6476b559f3e37de | ❌ |
| 000-lo.lo | ZOOM Rom | fc7599f3f871578fe9a0453662d1c966 | ❌ |
| ng-lo.rom | ZOOM Rom | e255264d85d5765013b1b2fa8109dd53 | ❌ |

### Emplacement

Placez les BIOS comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 neocd
┃ ┃ ┃ ┃ ┣ 🗒 **neocd.bin**
┃ ┃ ┃ ┃ ┣ 🗒 **uni-bioscd.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **top-sp1.bin**
┃ ┃ ┃ ┃ ┣ 🗒 **front-sp1.bin**
┃ ┃ ┃ ┃ ┣ 🗒 **neocd_sz.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **neocd_z.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **neocd_st.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **neocd_t.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **neocd_sf.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **neocd_f.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **000-lo.lo**
┃ ┃ ┃ ┃ ┣ 🗒 **ng-lo.rom**

## ![](/emulators/isos.svg) Isos

### Extensions supportées

Les ISOs doivent avoir les extensions suivantes :

* .bin/.cue
* .iso/.cue
* .img/.cue
* .chd

### Emplacement

Placez les roms comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 neogeocd
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.cue**
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.bin**

>Pour plus d'information sur les roms, rendez-vous sur [ce tutoriel](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

### Accéder aux options

Vous pouvez configurer diverses options de deux façons différentes.

* Via le menu RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via le fichier `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Options du core

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/libretro/neocd_libretro/](https://github.com/libretro/neocd_libretro/)
* **Documentation Libretro** : -