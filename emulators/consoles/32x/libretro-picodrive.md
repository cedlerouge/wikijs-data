---
title: Libretro PicoDrive
description: 
published: true
date: 2021-10-03T17:49:36.153Z
tags: libretro, 32x, picodrive
editor: markdown
dateCreated: 2021-05-21T08:18:56.516Z
---

**Libretro PicoDrive** est un émulateur open-source Sega [8](./../master-system)/[16](./../megadrive) bit et [32X](./../32x). Il a été réfléchi pour les appareils portable sur l'architecture [ARM](https://fr.wikipedia.org/wiki/Architecture_ARM).

## ![](/emulators/license.svg) Licence

Ce core est sous licence [**non-commerciale**](https://github.com/libretro/picodrive/blob/master/COPYING).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |
| Redémarrage | ✔ |
| Sauvegardes | ✔ |
| Sauvegardes instantanées | ✔ |
| Rembobinage | ✔ |
| Netplay | ✔ |
| Options du core | ✔ |
| RetroAchievements | ✔ |
| Cheats RetroArch | ✔ |
| Controls | ✔ |
| Remapping | ✔ |
| Contrôle du disque | ✔ |

## ![](/emulators/bios.svg) BIOS

>**Aucun bios n'est requis.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensions supportées

Les roms doivent avoir les extensions suivantes :

* .32x
* .bin
* .smd
* .zip
* .7z

Ce système supporte les roms compressées au format .zip/.7z. Attention toutefois, il ne s'agit que d'une archive.

Les fichiers contenus dans les .zip/.7z doivent correspondre aux extensions citées précédemment.
De plus, chaque fichier .zip/.7z ne doit contenir qu'une seule rom compressée.

### Emplacement

Placez les roms comme ceci : 

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 **sega32x**
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

>Les roms au format **No-Intro** sont vivement conseillées.
{.is-success}

>Pour plus d'information sur les roms, rendez-vous sur [ce tutoriel](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

### Compatibilité

| Jeux 32x | Problèmes connus |
| :--- | :--- |
| Brutal Unleashed – Above the Claw | Softlock après le premier combat. |
| FIFA Soccer ’96 | Texte du menu principal avec des glitches. |
| Knuckles’ Chaotix | Graphismes avec glitches sur l'écran de sélection du joueur. |
| NBA Jam Tournament Edition | Problèmes de framerate. |
| NFL Quarterback Club | Certains graphismes du menu sont manquants. |
| Virtua Racing Deluxe | Ligne clignotante pendant l'affichage du logo SEGA. |
| World Series Baseball Starring Deion Sanders | Plante quand un match commence. |
| WWF Raw | Certains graphismes sont manquants. |

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

### Accéder aux options

Vous pouvez configurer diverses options de deux façons différentes.

* Via le menu RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via le fichier `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Options du core

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/libretro/picodrive/](https://github.com/libretro/picodrive)
* **Documentation Libretro** : [https://docs.libretro.com/library/picodrive/](https://docs.libretro.com/library/picodrive/)