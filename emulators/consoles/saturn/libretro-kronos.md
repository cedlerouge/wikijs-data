---
title: Libretro Kronos
description: 
published: true
date: 2021-10-03T21:04:38.032Z
tags: libretro, saturn, kronos
editor: markdown
dateCreated: 2021-05-21T08:26:12.853Z
---

**Libretro Kronos** est un port de l'émulateur **Yabause** pour Libretro prenant en charge la Sega Saturn.

## ![](/emulators/license.svg) Licence

Ce core est sous licence [**GPLv2**](https://github.com/libretro/yabause/blob/master/yabause/COPYING).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ✅ |

## ![](/emulators/features.png) Fonctionnalités



## ![](/emulators/bios.svg) BIOS

### Liste des bios obligatoires

| Nom de fichier | Description | MD5 | Fourni |
| :---: | :---: | :---: | :---: |
| mpr-18811-mx.ic1 | The King of Fighters '95 - Requis pour ce jeu | 255113ba943c92a54facd25a10fd780c | ❌ |
| mpr-19367-mx.ic1 | Ultraman: Hikari no Kyojin Densetsu - Requis pour ce jeu | 1cd19988d1d72a3e7caa0b73234c96b4 | ❌ |
| saturn_bios.bin | BIOS Saturn | af5828fdff51384f99b3c4926be27762 | ❌ |
| stvbios.zip | BIOS ST-V | 2b99d9dcecde22db4371580b6bb33f32 53a094ad3a188f86de4e64624fe9b3ca 17116d2aea72454096b3f80ab714bfba 2b99d9dcecde22db4371580b6bb33f32 4cb34733b741ada074f11e3abcdc56d8 6b6d422a6c392eaec178b64b92987e99 88b9da6cda268ba1d9e37dd82d038801 8c34608cc140e1bfbf9c945a12fed0e8 b876d86e244d00b336980b4e26e94132
 | ❌ |

### Emplacement

Placez les BIOS comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 🗒 **mpr-18811-mx.ic1**
┃ ┃ ┃ ┣ 🗒 **mpr-19367-mx.ic1**
┃ ┃ ┃ ┣ 🗒 **saturn_bios.bin**
┃ ┃ ┃ ┣ 🗒 **stvbios.zip**

## ![](/emulators/isos.svg) Isos

### Extensions supportées

Les isos doivent avoir les extensions suivantes :

* .bin/.cue
* .m3u
* .img/.ccd
* .chd

### Emplacement

Placez les isos comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 saturn
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.bin**
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.cue**

>Les isos au format **Redump** sont vivement conseillés.
{.is-success}

>Pour plus d'information sur les isos, rendez-vous sur [ce tutoriel](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

### Accéder aux options

Vous pouvez configurer diverses options de deux façons différentes.

* Via le menu RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via le fichier `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Options du core

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/libretro/yabause/commit/c538bcac82770c1acb691eeca95688860fd264f0](https://github.com/libretro/yabause/commit/c538bcac82770c1acb691eeca95688860fd264f0)