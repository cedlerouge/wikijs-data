---
title: Libretro Mednafen_Saturn
description: 
published: true
date: 2021-10-03T21:06:01.050Z
tags: libretro, mednafen, saturn
editor: markdown
dateCreated: 2021-05-21T08:26:19.564Z
---

**Libretro Mednafen_Saturn** est un port de l'émulateur **Mednafen Saturn** (standalone) à Libretro prenant en charge la Sega Saturn.

## ![](/emulators/license.svg) Licence

Ce core est sous licence [**GPLv2**](https://github.com/libretro/beetle-saturn-libretro/blob/master/COPYING).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalités | Supporté |
| :--- | :--- |
| Redémarrage | ✔ |
| Captures d'écran | ✔ |
| Sauvegardes | ✔ |
| Sauvegardes instantanées | ✔ |
| Rembobinage | ✔ |
| Netplay | ✔ |
| Options du core | ✔ |
| RetroAchievements | ✔ |
| Controls | ✔ |
| Remapping | ✔ |
| Multi-Mouse | ✔ |

## ![](/emulators/bios.svg) BIOS

### Liste des bios obligatoires

| Nom de fichier | Description | MD5 | Fourni |
| :---: | :---: | :---: | :---: |
| sega_101.bin | BIOS Saturn JP - Requis pour les jeux JAP | 85ec9ca47d8f6807718151cbcca8b964 | ❌ |
| mpr-17933.bin | BIOS Saturn US.mdEU - Requis pour les jeux EUR/USA | 3240872c70984b6cbfda1586cab68dbe | ❌ |
| mpr-18811-mx.ic1 | The King of Fighters '95 - Requis pour ce jeu | 255113ba943c92a54facd25a10fd780c | ❌ |
| mpr-19367-mx.ic1 | Ultraman: Hikari no Kyojin Densetsu - Requis pour ce jeu | 1cd19988d1d72a3e7caa0b73234c96b4 | ❌ |

### Emplacement

Placez les BIOS comme ceci :

┣ 📁 recalbox  
┃ ┣ 📁 share  
┃ ┃ ┣ 📁 bios  
┃ ┃ ┃ ┣ 🗒 **sega_101.bin**  
┃ ┃ ┃ ┣ 🗒 **mpr-17933.bin**  
┃ ┃ ┃ ┣ 🗒 **mpr-18811-mx.ic1**  
┃ ┃ ┃ ┣ 🗒 **mpr-19367-mx.ic1**  

## ![](/emulators/isos.svg) Isos

### Extensions supportées

Les isos doivent avoir les extensions suivantes :

* .cue/.bin
* .m3u
* .img/.ccd
* .iso
* .chd

### Emplacement

Placez les isos comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 saturn
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.bin**
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.cue**

>Les isos au format **Redump** sont vivement conseillés.
{.is-success}

>Pour plus d'information sur les isos, rendez-vous sur [ce tutoriel](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

### Accéder aux options

Vous pouvez configurer diverses options de deux façons différentes.

* Via le menu RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via le fichier `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Options du core

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/libretro/beetle-saturn-libretro/](https://github.com/libretro/beetle-saturn-libretro/)
* **Documentation Libretro** : [https://docs.libretro.com/library/beetle_saturn/](https://docs.libretro.com/library/beetle_saturn/)