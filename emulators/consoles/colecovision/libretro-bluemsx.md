---
title: Libretro blueMSX
description: 
published: true
date: 2021-10-03T18:10:28.765Z
tags: libretro, bluemsx, colecovision
editor: markdown
dateCreated: 2021-05-21T08:19:58.636Z
---

## ![](/emulators/license.svg) Licence

Ce core est sous licence [**GPLv2**](https://github.com/libretro/blueMSX-libretro/blob/master/license.txt).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |
| Redémarrage | ✔ |
| Captures d'écran | ✔ |
| Options du core | ✔ |
| RetroAchievements | ✔ |
| Controls | ✔ |
| Remapping | ✔ |
| Contrôle du disque | ✔ |

## ![](/emulators/bios.svg) BIOS

### Liste des bios obligatoires

| Nom de fichier | Description | MD5 | Fourni |
| :---: | :---: | :---: | :---: |
| czz50-1.rom | Requis | 1de922acdd742d31349c2801e9768c35 | ✅  |
| czz50-2.rom | Requis | 72b089dc55b7fe7ffb5028f365e8c045 | ✅  |
| coleco.rom | Requis | 2c66f5911e5b42b8ebe113403548eee7 | ✅  |
| coleco.rom | Requis | 2c66f5911e5b42b8ebe113403548eee7 | ✅  |
| SVI603.ROM | Requis | c60a2e85572c0ccb69505a7646d5c1b6 | ✅  |

### Emplacement

Placez les bios comme ceci :

┣ 📁 recalbox  
┃ ┣ 📁 share  
┃ ┃ ┣ 📁 bios  
┃ ┃ ┃ ┣ 📁 Machines  
┃ ┃ ┃ ┃ ┣ 📁 COL - Bit Corporation Dina
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **czz50-1.rom**
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **czz50-2.rom**
┃ ┃ ┃ ┃ ┣ 📁 COL - ColecoVision with Opcode Memory Extension
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **coleco.rom**
┃ ┃ ┃ ┃ ┣ 📁 COL - ColecoVision
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **coleco.rom**
┃ ┃ ┃ ┃ ┣ 📁 COL - Spectravideo SVI-603 Coleco
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **SVI603.ROM**

## ![](/emulators/roms.png) Roms

### Extensions supportées

Les roms doivent avoir les extensions suivantes :

* .col

### Emplacement

Placez les roms comme ceci : 

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 colecovision
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.col**

>Les roms au format **No-Intro** sont vivement conseillées.
{.is-success}

>Pour plus d'information sur les roms, rendez-vous sur [ce tutoriel](./../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

### Accéder aux options

Vous pouvez configurer diverses options de deux façons différentes.

* Via le menu RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via le fichier `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Options du core

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/libretro/blueMSX-libretro/](https://github.com/libretro/blueMSX-libretro/)
* **Documentation Libretro** : [https://docs.libretro.com/library/bluemsx/](https://docs.libretro.com/library/bluemsx/)