---
title: Reicast
description: 
published: true
date: 2021-10-03T18:17:28.673Z
tags: dreamcast, reicast
editor: markdown
dateCreated: 2021-05-21T08:20:17.642Z
---

**Reicast** est un émulateur capable d'émuler la **Dreamcast**.

## ![](/emulators/license.svg) Licence

Ce core est sous licence [**BSD-3**](https://github.com/reicast/reicast-emulator/blob/alpha/LICENSE).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌  | ❌ | ✅ | ❌ | ❌ | ❌ | ❌ | ❌ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |
|  |  |

## ![](/emulators/bios.svg) BIOS

### Liste des bios obligatoires

| Nom de fichier | Description | MD5 | Fourni |
| :---: | :---: | :---: | :---: |
| dc_boot.bin | BIOS Dreamcast | e10c53c2f8b90bab96ead2d368858623 | ❌  |
| dc_flash.bin | Date / Heure / Langue | 0a93f7940c455905bea6e392dfde92a4 | ❌  |

### Emplacement

Placez les bios comme ceci :

┣ 📁 recalbox  
┃ ┣ 📁 share  
┃ ┃ ┣ 📁 bios  
┃ ┃ ┃ ┣ 🗒 **dc_boot.bin**  
┃ ┃ ┃ ┣ 🗒 **dc_flash.bin**  

## ![](/emulators/isos.svg) Isos

### Extensions supportées

Les isos doivent avoir les extensions suivantes :

* .bin/.cue
* .bin/.gdi
* .cdi
* .chd (format v4 !)

### Emplacement

Placez les isos comme ceci : 

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 dreamcast
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.gdi**
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.bin**

>Les isos au format **TOSEC** sont vivement conseillés.
{.is-success}

>Pour plus d'information sur les isos, rendez-vous sur [ce tutoriel](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuration avancée des émulateurs

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé :** [https://github.com/reicast/reicast-emulator/](https://github.com/reicast/reicast-emulator/)
* **Documentation** : [https://reicast.com/guide/](https://reicast.com/guide/)