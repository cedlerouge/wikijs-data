---
title: PC-FX
description: NEC PC-FX
published: true
date: 2021-10-03T20:46:28.090Z
tags: consoles, nec, pc-fx
editor: markdown
dateCreated: 2021-05-21T08:00:54.332Z
---

![](/emulators/consoles/pcfx.svg){.align-center}

## Fiche technique

* **Fabricant** : NEC Home Electronics
* **Année de sortie** : 1994
* **Quantités vendues** : 400000
* **Processeur** : 32-bit NEC V810 RISC @ 21.5 MHz
* **RAM** : 2 MB (32 KB backup)
* **VRAM** : 1.25 MB
* **Puce sonore** : 16-bit stereo with ADPCM
* **Résolution** : 640x480 pixels
* **Média** : CD-ROM

## Présentation

La **PC-FX** est une console de jeux vidéo 32 bits, développée par NEC. Elle est commercialisée à partir du 23 décembre 1994, soit quelques semaines après la PlayStation de Sony et un mois après la Sega Saturn. Pensée pour succéder à la PC-Engine et à la SuperGrafX, ses jeux sont au format CD-ROM.

Contrairement à ses prédécesseurs, la PC-FX n'est sortie qu'au Japon. La console a la forme d'un tour verticale comme les boîtiers au format tour de certains PC, et devait être aussi évolutive que ces derniers. Cependant, la PC-FX ne possédait pas de puce graphique capable de gérer la 3D, car à la place, NEC s'est concentré sur le full motion vidéo et les capacités de jeu 2D. Finalement, ce choix a rendu la FX moins puissante que ses concurrents. Elle était également chère et manquait de soutien de la part des développeurs et éditeurs de jeux vidéo. Tout ceci combiné fait qu'elle n'a pas réussi à rivaliser avec les autres consoles de cinquième génération. Le PC-FX est la dernière console de jeu vidéo de NEC et la production a été arrêtée en février 1998, alors que seulement 400 000 exemplaires avaient été vendus en 4 ans.

## Émulateurs

[Libretro Mednafen_PCFX](libretro-mednafen_pcfx)