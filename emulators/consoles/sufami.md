---
title: SuFami Turbo
description: Bandai SuFami Turbo
published: true
date: 2021-10-03T21:07:57.313Z
tags: consoles, bandai, sufami turbo
editor: markdown
dateCreated: 2021-05-21T08:01:25.051Z
---

![](/emulators/consoles/sufamiturbo.svg){.align-center}

## Fiche technique

* **Fabricant** : Bandai
* **Année de sortie** : 1996
* **Média** : application sur cartouche SuFami Turbo +  cartouches de jeu

## Présentation

Le **SuFami Turbo**, quelquefois comparé au Aladdin Deck Enhancer, est un accessoire fournie par Bandai pour la Super Famicom de Nintendo et est disponible en 1996.

Cet appareil est conçu pour s'enclencher sur le dessus de la Super Famicom et contient 2 emplacements de cartouches. Le but est que les jeux peuvent être produites à un coût plus réduit ainsi que de réduire le temps de développement, ne dépendant pas de la production de cartouches de Nintendo. À l'instar du Aladdin Deck Enhancer, cet appareil a été officiellement approuvé par Nintendo avec la condition que Bandai produise toute la fabrication du matériel lui-même.

Les 2 emplacements des cartouches sont conçues pour partager des données entre cartouches. La cartouche placée dans le premier emplacement est le jeu qui est joué, tandis que la cartouche dans le second emplacement fournit des données supplémentaires à utiliser dans le jeu principal. Sur les treize jeux disponibles, 9 d'entre-eux peuvent se lier, entre chaque série de jeux.

Les jeux qui peuvent être liés sont identifiés par un diagramme jaune montrant un SuFami Turbo avec soit une ou deux cartouches dans le coin en bas à droite de la boîte de jeux. Si l'image a 1 cartouche insérée dans le SuFami Turbo, le jeu ne peut pas être lié. Si l'image a 2 cartouches insérées dans le SuFami Turbo, le jeu peut être lié avec la liste de jeux indiquée sur la boîte.

Le SuFami Turbo était disponible seul ou en pack avec un jeu.

## Émulateurs

[Libretro Snes9x](libretro-snes9x)