---
title: Playstation 1
description: Sony Playstation 1
published: true
date: 2021-10-03T20:49:56.951Z
tags: consoles, sony, ps1, playstation 1
editor: markdown
dateCreated: 2021-05-21T08:00:59.932Z
---

![](/emulators/consoles/playstation1.svg){.align-center}

## Fiche technique

* **Fabricant** : Sony Computer Entertainment
* **Année de sortie** : 1994
* **Quantités vendues** : 102.48 millions
* **Meilleur jeu vendu** : Gran Turismo
* **Processeur** : 32-bit R3000A RISC cadencé à 33.8688 Mhz
* **RAM** : 16 Mbits
* **Picture Processor Unit** : 32-bit
* **RAM vidéo** : 8 Mbits
* **RAM audio** : 4 Mbits
* **Processeur graphique** : GFX processor unit

## Présentation

La PlayStation est une console de la cinquième génération, produite par Sony Computer Entertainment à partir de 1994. La PlayStation originale fut la première machine de la gamme PlayStation, déclinée ensuite en PSone (une version plus petite et plus légère que l'originale).

Le 18 mai 2004, soit près de dix ans après son lancement, Sony annonce avoir distribué 100 millions de consoles dans le monde et plus de 962 millions de jeux PlayStation.

## Émulateurs

[PCSX-ReARMed](pcsx-rearmed)
[Libretro PCSX-ReARMed](libretro-pcsx-rearmed)
[Libretro Swanstation](libretro-swanstation)
[Libretro Mednafen_PSX](libretro-mednafen_psx)
[Libretro Mednafen_PSX_HW](libretro-mednafen-psx-hw)

