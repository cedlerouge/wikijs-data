---
title: Atari 7800
description: 
published: true
date: 2021-10-03T18:05:23.912Z
tags: consoles, atari, 7800, atari-7800
editor: markdown
dateCreated: 2021-05-21T07:58:53.729Z
---

![](/emulators/consoles/atari7800.svg){.align-center}

## Fiche technique

* **Fabricant :** Atari Corporation
* **Année de sortie :** 1986
* **Quantités vendues :** 3,77 millions
* **Processeur :** Atari SALLY 6502 ("6502C") @ 1.19-1.79MHz
* **RAM :** 4KB
* **Taille des cartouches :** 48KB
* **Processeur audio / vidéo :** Atari SALLY 6502 ("6502C") @ 1.19-1.79MHz
* **Ports :** 2 ports joystick, 1 port cartouche, 1 port d'extension, alimentation, sortie RF

## Présentation

L'**Atari 7800** est une console de jeux vidéo 8-bit de troisième génération, conçue et commercialisée par le constructeur américain Atari. Elle est sortie début 1986 aux États-Unis, puis dans le courant de l'année au Japon et seulement en 1991 en France.

Succédant à l'Atari 5200, l'Atari 7800 sort comme concurrente à la Nintendo Entertainment System (NES) et à la Sega Master System pendant l'ère des consoles de jeux vidéo de troisième génération. Toutefois, la console ne rivalisera jamais réellement avec ses concurrentes.

## Émulateurs

[Libretro Prosystem](libretro-prosystem)