---
title: Libretro Mednafen_PSX_HW
description: 
published: true
date: 2021-10-03T20:52:47.741Z
tags: libretro, ps1, mednafen, playstation 1, psx hw
editor: markdown
dateCreated: 2021-05-21T08:25:23.938Z
---

[Beetle PSX](https://github.com/libretro/beetle-psx-libretro) est un fork du module PSX de [Mednafen](https://mednafen.github.io/) pour Libretro, il fonctionne actuellement sous Linux, macOS et Windows, ce core est disponible en version `mednafen-psx-hw` qui exige OpenGL 3.3 pour le moteur de rendu OpenGL.

## ![](/emulators/license.svg) Licence

Ce core est sous licence [**GPLv2**](https://github.com/libretro/beetle-psx-libretro/blob/master/COPYING).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |
| Redémarrage | ✔ |
| Sauvegardes | ✔ |
| Sauvegardes instantanées | ✔ |
| Options du core | ✔ |
| RetroAchievements | ✔ |
| Cheats RetroArch | ✔ |
| Contrôles | ✔ |
| Remapping | ✔ |
| Multi-Mouse | ✔ |
| Vibration | ✔ |
| Contrôle de disque | ✔ |

## ![](/emulators/bios.svg) BIOS

### Liste des bios obligatoires

| Nom de fichier | Description | MD5 | Fourni |
| :---: | :---: | :---: | :---: |
| scph5500.bin | BIOS PS1 japonais - requis pour les jeux japonais | 8dd7d5296a650fac7319bce665a6a53c | ❌ |
| scph5501.bin | BIOS PS1 américain - requis pour les jeux américains | 490f666e1afb15b7362b406ed1cea246 | ❌ |
| scph5502.bin | BIOS PS1 européen - requis pour les jeux européens | 32736f17079d0b2b7024407c39bd3050 | ❌ |

### Emplacement

Placez les BIOS comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 🗒 **scph5500.bin**
┃ ┃ ┃ ┣ 🗒 **scph5501.bin**
┃ ┃ ┃ ┣ 🗒 **scph5502.bin**

## ![](/emulators/isos.svg) Isos

### Extensions supportées

Les isos doivent avoir les extensions suivantes :

* .bin/.cue
* .bin/.toc
* .m3u
* .img/.ccd/.sub
* .exe
* .pbp
* .chd

### Emplacement

Placez les isos comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 psx
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.bin**
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.cue**

>Les isos au format **Redump** sont vivement conseillés.
{.is-success}

>Pour plus d'information sur les isos, rendez-vous sur [ce tutoriel](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

### Accéder aux options

Vous pouvez configurer diverses options de deux façons différentes.

* Via le menu RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via le fichier `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Options du core

| Options | Valeurs des options | Variable (pour surcharge) | Valeurs des variables |
| --- | --- | --- | --- |
| **Renderer (Restart)** | `Hardware (auto)` ✅ / `Hardware (OpenGL)` / `Hardware (Vulkan)` / `Software` | `beetle_psx_hw_renderer` | `hardware` / `hardware_gl` / `hardware_vk` / `software` |
| **Software Framebuffer** | `Activé` ✅ / `Désactivé` | `beetle_psx_hw_renderer` | `enabled` / `disabled` |
| **Internal GPU Resolution** | `1x (Native)` ✅ / `2x` / `4x` / `8x` / `16x` | `beetle_psx_hw_internal_resolution` | `1x(native)` / `2x` / `4x` / `8x` / `16x` |
| **Internal Color Depth** | `16 bpp (Native)` ✅ / `32 bpp` | `beetle_psx_hw_depth` | `16bpp(native)` / `32 bpp` |
| **Dithering Pattern** | `1x (Native)` ✅ / `Internal Resolution` / `Désactivé` | `beetle_psx_hw_dither_mode` | `1x(native)` / `internal resolution` / `disabled` |
| **Texture Filtering** | `Nearest` ✅ / `SABR` / `xBR` / `Bilinear` / `3-Point` / `JINC2` | `beetle_psx_hw_filter` | `nearest` / `SABR` / `xBR` / `bilinear` / `3-point` / `JINC2` |
| **Track Textures** | `Activé` / `Désactivé` ✅ | `beetle_psx_hw_track_textures` | `enabled` / `disabled` |
| **Dump Textures** | `Activé` / `Désactivé` ✅ | `beetle_psx_hw_dump_textures` | `enabled` / `disabled` |
| **Replace Textures** | `Activé` / `Désactivé` ✅ | `beetle_psx_hw_replace_textures` | `enabled` / `disabled` |
| **Wireframe Mode (Debug)** | `Activé` / `Désactivé` ✅ | `beetle_psx_hw_wireframe` | `enabled` / `disabled` |
| **Display Full VRAM (Debug)** | `Activé` / `Désactivé` ✅ | `beetle_psx_hw_dispaly_vram` | `enabled` / `disabled` |
| **PGXP Operation Mode** | `Désactivé` ✅ / `Memory Only` / `Memory + CPU (Buggy)` |`beetle_psx_hw_pgxp_mode` | `disabled` / `memory only` / `memory + CPU` |
| **PGXP Vertex Cache** | `Activé` / `Désactivé` ✅ | `beetle_psx_hw_pgxp_vertex` | `enabled` / `disabled` |
| **PGXP Perspective Correct Texturing** | `Activé` / `Désactivé` ✅ | `beetle_psx_hw_pgxp_texture` | `enabled` / `disabled` |
| **Display Internal FPS** | `Activé` / `Désactivé` ✅ | `beetle_psx_hw_display_internal_fps` | `enabled` / `disabled` |
| **Line-to-Quad Hack** | `Default` ✅ / `Aggressive` / `Désactivé` | `beetle_psx_hw_line_render` | `default` / `aggressive` / `disabled` |
| **CPU Dynarec** | `Disabled (Beetle Interpreter)` ✅ / `Max Performance` / `Cycle Timing Check` / `Lightrec Interpreter` | `beetle_psx_hw_cpu_dynarec` | `disabled` / `execute` / `execute_once` / `run_interpreter` |
| **Dynarec Code Invalidation** | `Full` ✅ / `DMA Only (Slightly Faster)` | `beetle_psx_hw_dynarec_invalidate` | `full` / `dma` |
| **Dynarec DMA/GPU Event Cycles** | `128 (Default)` ✅ / `256` / `384` / `512` / `640` / `768` / `896` / `1024` | `beetle_psx_hw_dynarec_eventcycles` | `128` / `256` / `384` / `512` / `640` / `768` / `896` / `1024` |
| **CPU Frequency Scaling (Overclock)** | `50%` à `750%`, par défaut `100% (Native)` ✅ | `beetle_psx_hw_cpu_freq_scale` | `50%` à `750%`, par défaut `100%` |
| **GTE Overclock** | `Activé` / `Désactivé` ✅ | `beetle_psx_hw_gte_overclock` | `enabled` / `disabled` |
| **GPU Resterizer Overclock** | `1x (Native)` ✅ / `2x` / `4x` / `8x` / `16x` / `32x` | `beetle_psx_hw_gpu_overclock` | `1x(native)` / `2x` / `4x` / `8x` / `16x` / `32x` |
| **Skip BIOS** | `Activé` / `Désactivé` ✅ | `beetle_psx_hw_skip_bios` | `enabled` / `disabled` |
| **Core-Reported FPS Timing** | `Progressive Rate (Default)` ✅ / `Force Interlaced Rate` / `Allow Automatic Toggling` | `beetle_psx_hw_core_timing_fps` | `force_progressive` / `force_interlaced` / `auto_toggle` |
| **Core Aspect Ratio** | `Corrected` ✅ / `Uncorrected` / `Force 4:3` / `Force NTSC` | `beetle_psx_hw_aspect_ratio` | `corrected` / `uncorrected` / `4:3` / `ntsc` |
| **Widescreen Mode Hack** | `Activé` / `Désactivé` ✅ | `beetle_psx_hw_widescreen_hack` | `enabled` / `disabled` |
| **PAL (European) Video Timing Override** | `Activé` / `Désactivé` ✅ | `beetle_psx_hw_pal_video_timing_override` | `enabled` / `disabled` |
| **Crop Horizontal Overscan** | `Activé` ✅ / `Désactivé` | `beetle_psx_hw_crop_overscan` | `enabled` / `disabled` |
| **Horizontal Image Offset (GPU Cycles)** | `-24` à `+24`, par défaut `0` ✅ | `beetle_psx_hw_image_offset_cycles` | `-24` à `+24`, par défaut `0` |
| **Initial Scanlines - PAL** | `0` à `40`, par défaut `0` ✅ | `beetle_psx_hw_initial_scanline_pal` | `0` à `40`, par défaut `0` |
| **Last Scanlines - PAL** | `230` à `287`, par défaut `287` ✅ | `beetle_psx_hw_last_scanline_pal` | `230` à `287`, par défaut `287` |
| **CD Access Method (Restart)** | `Synchronous` ✅ / `Asynchronous` / `Pre-Cache` | `beetle_psx_hw_cd_access_method` | `sync` / `async` / `precache` |
| **CD Loading Speed** | `2x (Native)` ✅ / `4x` / `6x` / `8x` / `10x` / `12x` / `14x` | `beetle_psx_hw_cd_fastload` | `2x(native)` / `4x` / `6x` / `8x` / `10x` / `12x` / `14x` |
| **Memory Card 0 Method (Restart)** | `Libretro` ✅ / `Mednafen` | `beetle_psx_hw_use_mednafen_memcard0_method` | `libretro` / `mednafen` |
| **Shared Memory Card 1 (Restart)** | `Activé` ✅ / `Désactivé` | `beetle_psx_hw_shared_memory_cards` | `enabled` / `disabled` |
| **Analog Self-Calibration** | `Activé` / `Désactivé` ✅ | `beetle_psx_hw_analog_calibration` | `enabled` / `disabled` |
| **Enable DualShock Analog Mode Toggle** | `Activé` / `Désactivé` ✅ | `beetle_psx_hw_analog_toggle` | `enabled` / `disabled` |
| **Port 1: Multitap Enable** | `Activé` / `Désactivé` ✅ | `beetle_psx_hw_enable_multitap_port1` | `enabled` / `disabled` |
| **Port 2: Multitap Enable** | `Activé` / `Désactivé` ✅ | `beetle_psx_hw_enable_multitap_port2` | `enabled` / `disabled` |
| **Gun Input Mode** | `Light Gun` ✅ / `Touchscreen` | `beetle_psx_hw_gun_input_mode` | `lightgun` / `touchscreen` |
| **Gun Cursor** | `Cross` ✅ / `Dot` / `Désactivé` | `beetle_psx_hw_gun_cursor` | `cross` / `dot` / `off` |
| **Mouse Sensitivity** | `5%` à `200%`, par défaut `100%` ✅ | `beetle_psx_hw_mouse_sensitivity` | `5%` à `200%`, par défaut `100%` |
| **NegCon Twist Responses** | `Linear` ✅ / `Quadratic` / `Cubic` | `beetle_psx_hw_negcon_response` | `linear` / `quadratic` / `cubic` |
| **NegCon Twist Deadzone** | `0%` ✅ / `5%` / `10%` / `15%` / `20%` / `25%` / `30%` | `beetle_psx_hw_negcon_deadzone` | `5%` / `10%` / `15%` / `20%` / `25%` / `30%` |
| **Memory Card Left Index** | `0` à `63`, par défaut `0` ✅ | `beetle_psx_hw_memcard_left_index` | `0` à `63`, par défaut `0` |
| **Memory Card Right Index** | `0` à `63`, par défaut `1` ✅ | `beetle_psx_hw_memcard_right_index` | `0` à `63`, par défaut `1` |

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/libretro/beetle-psx-libretro/](https://github.com/libretro/beetle-psx-libretro/)
* **Documentation Libretro** : [https://docs.libretro.com/library/beetle_psx_hw/](https://docs.libretro.com/library/beetle_psx_hw/)