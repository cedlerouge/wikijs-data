---
title: Libretro melonDS
description: 
published: true
date: 2021-09-16T06:16:32.692Z
tags: libretro, ds, melonds
editor: markdown
dateCreated: 2021-05-21T08:31:44.847Z
---

**Libretro melonDS** est un émulateur Nintendo DS/DSI prometteur développé en C et C++.

L'émulateur **melonDS** vise à fournir une émulation Nintendo DS rapide et précise. Bien qu'il s'agisse encore d'un travail en cours, il possède un ensemble assez solide de fonctionnalités :

* Cœur presque complet (CPU, vidéo, audio, ...)
* Recompilateur JIT pour une émulation rapide
* Rendu OpenGL, conversion ascendante 3D
* RTC, microphone, couvercle fermé / ouvert
* Prise en charge du joystick
* Savestates
* Différents modes d'affichage / dimensionnement / rotation
* (WIP) Wifi: multijoueur local, connectivité en ligne
* Émulation DSi (WIP)
* DLDI
* (WIP) Modules complémentaires d’emplacement GBA
* et plus sont prévus !

Il a été écrit par **Arisotura** (connu sous le nom de StapleButter), un ancien contributeur à DeSmuME.

## ![](/emulators/license.svg) Licence

Cet émulateur est sous licence [**GPLv3**](https://github.com/libretro/melonDS/blob/master/LICENSE).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |
| Redémarrage | ✔ |
| Captures d'écran | ✔ |
| Sauvegardes | ✔ |
| RetroAchievements | ✔ |
| Cheats RetroArch | ✔ |
| Controls | ✔ |
| Remapping | ✔ |

## ![](/emulators/bios.svg) BIOS

### Liste des bios obligatoires

| Nom de fichier | Description | MD5 | Fourni |
| :---: | :---: | :---: | :---: |
| bios7.bin | BIOS ARM7 | df692a80a5b1bc90728bc3dfc76cd948 | ❌ |
| bios9.bin | BIOS ARM9 | a392174eb3e572fed6447e956bde4b25 | ❌ |
| firmware.bin | Firmware NDS | 145eaef5bd3037cbc247c213bb3da1b3 e45033d9b0fa6b0de071292bba7c9d13 3ad72b2c9a736b24953f2d391da4bfcc | ❌ |

### Emplacement

Placez les bios comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 🗒 **bios7.bin**
┃ ┃ ┃ ┣ 🗒 **bios9.bin**
┃ ┃ ┃ ┣ 🗒 **firmware.bin**

## ![](/emulators/roms.png) Roms

### Extensions supportées

La rom doit avoir l'extension :

* .nds
* .zip
* .7z

### Emplacement

Placez les roms comme ceci : 

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 ds
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

>Les roms au format **No-Intro** sont vivement conseillées.
{.is-success}

>Pour plus d'information sur les roms, rendez-vous sur [ce tutoriel](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

### Accéder aux options

Vous pouvez configurer diverses options de deux façons différentes.

* Via le menu RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via le fichier `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Options du core

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/libretro/melonds/](https://github.com/libretro/melonds/)
* **Documentation Libretro** : [https://docs.libretro.com/library/melonds/](https://docs.libretro.com/library/melonds/)
* **Code source officiel** : [https://github.com/Arisotura/melonDS](https://github.com/Arisotura/melonDS/)
* **Site officiel** : [http://melonds.kuribo64.net/](http://melonds.kuribo64.net/)
* **Forum officiel** : [http://melonds.kuribo64.net/board/](http://melonds.kuribo64.net/board/)