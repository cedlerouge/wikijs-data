---
title: Libretro RACE
description: 
published: true
date: 2021-09-16T06:11:35.727Z
tags: libretro, neo-geo, pocket, ngp, race
editor: markdown
dateCreated: 2021-05-21T08:31:17.567Z
---

**Libretro RACE** est un émulateur de SNK Neo Geo Pocket et Neo Geo Pocket Color.

Il a été écrit par :

* Judge_
* Flavor
* Akop Karapetyan
* theelf
* frangarcj
* negativeExponent

Il a été modifié par **theelf** pour fonctionner sur la PSP.  
Il est probablement dérivé de la version Akop Karapetyan PSP de RACE ([http://psp.akop.org/race](http://psp.akop.org/race)).

## ![](/emulators/license.svg) Licence

Cet émulateur est sous licence [**GPLv2**](https://github.com/libretro/RACE/blob/master/license.txt).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |
| Redémarrage | ✔ |
| Captures d'écran | ✔ |
| Sauvegardes | ✔ |
| Sauvegardes instantanées | ✔ |
| Rembobinage | ✔ |
| Netplay | (pas d'émulation du câble link) |
| Options du core | ✔ |
| RetroAchievements | ✔ |
| Controls | ✔ |
| Remapping | ✔ |

## ![](/emulators/bios.svg) BIOS

>**Aucun bios n'est requis.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensions supportées

La rom doit avoir l'extension :

* .ngp
* .zip
* .7z

Ce système supporte les roms compressées au format .zip/.7z. Attention toutefois, il ne s'agit que d'une archive.

Les fichiers contenus dans les .zip/.7z doivent correspondre aux extensions citées précédemment.
De plus, chaque fichier .zip/.7z ne doit contenir qu'une seule rom compressée.

### Emplacement

Placez les roms comme ceci : 

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 ngp
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

>Les roms au format **No-Intro** sont vivement conseillées.
{.is-success}

>Pour plus d'information sur les roms, rendez-vous sur [ce tutoriel](./../../../tutorials/games/generalities/isos-and-roms) !
{.is-info}

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

### Accéder aux options

Vous pouvez configurer diverses options de deux façons différentes.

* Via le menu RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via le fichier `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Options du core

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé :** [https://github.com/libretro/RACE/](https://github.com/libretro/RACE/)
* **Documentation Libretro :**[ https://docs.libretro.com/library/race/](https://docs.libretro.com/library/race/)
* **Code source officiel :** [https://github.com/TheFlav/RACE-NGPC-Emulator-PSP/](https://github.com/TheFlav/RACE-NGPC-Emulator-PSP/)
* **Documentation officielle** : [http://psp.akop.org/race/doc.htm](http://psp.akop.org/race/doc.htm)
* **Site officiel :** [http://psp.akop.org/race.htm](http://psp.akop.org/race.htm)