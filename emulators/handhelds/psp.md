---
title: PSP
description: 
published: true
date: 2021-09-16T06:17:47.931Z
tags: psp, sony, portables
editor: markdown
dateCreated: 2021-05-21T08:03:58.573Z
---

![](/emulators/handheld/psp.svg){.align-center}

## Fiche technique

* **Fabricant** : Sony Computer Entertainment
* **Année de sortie** : 2004
* **Quantités vendues** : 80.82 millions
* **Meilleur jeu vendu** : GTA Liberty City Stories
* **Processeur** : 32-bit MIPS RS4000 running at 333 Mhz
* **RAM** : 32 Mbits
* **DRAM** : 4 Mbits
* **Résolution** : 480x272 pixels
* **Taille** : 170x74x23 mm
* **Poids** : 260 g

## Présentation

La **PlayStation Portable**, ou (**PSP**) est la première console de jeux vidéo portable de Sony, commercialisée entre 2004 et 2014. Elle marque un tournant dans l'histoire des consoles portables, car elle propose une qualité d'image et des capacités générales approchant celles de la PlayStation 2, une console de salon.

La PlayStation Portable permet de jouer à des jeux vidéo, de regarder des vidéos et des images, d'écouter de la musique et offre la possibilité de naviguer sur Internet. Son système de connexion sans fil Wi-Fi permet de faire communiquer jusqu'à seize machines en même temps les unes avec les autres, grâce à la fonction de « mise en réseau ad hoc ».

## Émulateurs

[PPSSPP](ppsspp)