---
title: Palm
description: 
published: true
date: 2021-09-16T06:18:47.308Z
tags: palm, palm-os, portables
editor: markdown
dateCreated: 2021-05-21T08:03:40.993Z
---

![](/emulators/handheld/palm.svg){.align-center}

## Fiche technique

* **Fabricant** : Palm, Inc.
* **Année de sortie** : 2002
* **Système d'exploitation** : Palm OS version 4.1
* **Processeur** : Motorola DragonBall VZ MC68VZ328 @ 33 MHz
* **RAM** : 16 MB RAM
* **ROM** : 4 MB Flash
* **Affichage** : 160×160 pixels (65,000+ couleurs)

## Présentation

**Palm OS** (aussi connu sous le nom de **Garnet OS**) est un système d'exploitation embarqué développé initialement par U.S. Robotics Corp., propriétaire de la société Palm Computing, Inc. à partir de 1995, pour des PDA (assistants personnels numériques) en 1996.

L'interface utilisateur graphique de Palm OS est prévue pour être utilisée avec un écran tactile. Il est fourni avec une suite d'applications de base pour gestionnaire d'informations personnelles. Plus tard les versions de cet OS ont été améliorées pour fonctionner sur des Smartphones. Plusieurs autres sociétés ont reçu une licence pour construire des appareils électroniques fonctionnant avec Palm OS.

## Émulateurs

[Libretro Mu](libretro-mu)