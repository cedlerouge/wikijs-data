---
title: Wonderswan
description: 
published: true
date: 2021-09-16T06:49:09.868Z
tags: bandai, wonderswan, portables
editor: markdown
dateCreated: 2021-05-21T08:04:16.598Z
---

![](/emulators/handheld/wonderswan.svg){.align-center}

## Fiche technique

* **Fabricant** : Bandai
* **Année de sortie** : 1999
* **Quantités vendues** : 3.5 millions
* **Processeur** : 16Bits NEC V20
* **RAM** : 512k video
* **Résolution** : 224 x 144 pixels
* **Son** : 4 canaux PCM, 32 samples 4Bit pour chaque canal

## Présentation

Le **WonderSwan** sort en mars 1999 au Japon, où il rencontre un succès certain. C'est une console monochrome qui se vend bien malgré la sortie un an plus tôt de la Game Boy Color grâce à un atout : sa petite taille. Ainsi, il présente une grande autonomie, tout en étant performant.

L'inventeur de cette console est Gunpei Yokoi, le géniteur de la Game Boy.

Le WonderSwan connaît deux évolutions : le WonderSwan Color qui apporte un écran couleur et le SwanCrystal qui est une déclinaison du WonderSwan Color avec un meilleur écran.

Ces trois modèles se sont vendus à 2 061 193 exemplaires au Japon, leur unique marché.

## Émulateur

[Libretro Mednafen_WSwan](libretro-mednafen-wswan)