---
title: Tic-80
description: Libretro-tic80
published: true
date: 2021-09-16T04:39:13.746Z
tags: tic-80, fantasy
editor: markdown
dateCreated: 2021-05-21T08:02:38.115Z
---

![](/emulators/fantasy/tic80.svg){.align-center}

## Fiche technique

* **Fabricant** : Vadim Grigoruk
* **Année de sortie** : 2017
* **Fonctionnalités incluses** : Code editor, Sprite Editor, Tile Map Editor, SFX Editor
* **Résolution** : 240x136 pixels, palette de 16 couleurs
* **Sprites** : 256 8x8 foreground sprites, 256 8x8 background tiles
* **Son** : 4 canaux (avec les enveloppes waveform modifiables)

## Présentation

Le TIC-80 est un ordinateur fantastique GRATUIT et OPEN SOURCE pour créer, jouer et partager de petits jeux.

Avec TIC-80, vous disposez d'outils de développement intégrés: code, sprites, cartes, éditeurs de sons et ligne de commande, ce qui est suffisant pour créer un mini-jeu rétro !

Les jeux sont regroupés dans un fichier de cartouche, qui peut être facilement distribué. Le TIC-80 fonctionne sur toutes les plateformes populaires. Cela signifie que votre cartouche peut être lue dans n'importe quel appareil.

Pour créer un jeu de style rétro, l'ensemble du processus de création et d'exécution se déroule sous certaines limites techniques: affichage 240x136 pixels, palette 16 couleurs, 256 sprites couleur 8x8, son 4 canaux, etc.

## Émulateurs

[Libretro Tic80](libretro-tic80)

