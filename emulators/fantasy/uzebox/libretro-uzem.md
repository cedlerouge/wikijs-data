---
title: Libretro uzem
description: 
published: true
date: 2021-09-16T04:41:10.244Z
tags: libretro, uzebox, uzem
editor: markdown
dateCreated: 2021-05-21T08:28:56.440Z
---

**Libretro Uzem** est un émulateur officiel de l'Uzebox (une console de jeu open source 8 bits rétro-minimaliste). L'Uzebox est un système minimal basé sur un microcontrôleur AVR ATmega644.

## ![](/emulators/license.svg) Licence

Ce core est sous licence [**GPLv3**](https://github.com/Uzebox/uzebox/blob/master/gpl-3.0.txt).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |
| Sauvegardes instantanées | ✔ |
| Rembobinage | ✔ |

## ![](/emulators/bios.svg) BIOS

>**Aucun bios n'est requis.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensions supportées

Les roms doivent avoir l'extension :

* .uze
* .zip
* .7z

Les fichiers contenus dans les .zip/.7z doivent correspondre aux extensions citées précédemment.

### Emplacement

Placez les roms comme ceci : 

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 uzebox
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

### Accéder aux options

Vous pouvez configurer diverses options de deux façons différentes.

* Via le Menu RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via le fichier `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Options du core

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/libretro/libretro-uzem/](https://github.com/libretro/libretro-uzem)
* **Code source officiel** : [https://github.com/Uzebox/uzebox/](https://github.com/Uzebox/uzebox)
* **Site officiel** : [http://belogic.com/uzebox/index.asp](http://belogic.com/uzebox/index.asp)
* **Forum officiel** : [http://uzebox.org/forums/](http://uzebox.org/forums/)
* **Wiki officiel** : [http://uzebox.org/wiki/index.php?title=Hello_World](http://uzebox.org/wiki/index.php?title=Hello_World)