---
title: Libretro LowResNX
description: 
published: false
date: 2021-09-12T08:51:27.803Z
tags: libretro, 7.3+, lowresnx
editor: markdown
dateCreated: 2021-07-09T13:03:41.126Z
---

**Libretro LowResNX** est un port de de l'émulateur **LowRes NX**.

## ![](/emulators/license.svg) Licence

Ce core est sous licence [**Zlib**](https://github.com/timoinutilis/lowres-nx/blob/master/LICENSE).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |

## ![](/emulators/bios.svg) BIOS

>**Aucun bios n'est requis.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensions supportées

Les roms doivent avoir l'extension :

* .nx
* .zip
* .7z

Les fichiers contenus dans les .zip/.7z doivent correspondre aux extensions citées précédemment.

### Emplacement

Placez les roms comme ceci : 

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 lowresnx
┃ ┃ ┃ ┃ ┣ 🗒  **jeu.nx**

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

### Options du core

> Ce core n'a pas d'option.
{.is-success}


## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/timoinutilis/lowres-nx/](https://github.com/timoinutilis/lowres-nx/)
* **Site officiel** : [https://lowresnx.inutilis.com/](https://lowresnx.inutilis.com/)
* **Documentation officielle** : [https://lowresnx.inutilis.com/help.php](https://lowresnx.inutilis.com/help.php)