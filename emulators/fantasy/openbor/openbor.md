---
title: OpenBOR
description: 
published: true
date: 2021-09-16T04:35:35.424Z
tags: openbor
editor: markdown
dateCreated: 2021-05-21T08:28:32.096Z
---



## ![](/emulators/license.svg) Licence

Ce core est sous licence [**BSD-3**](https://github.com/DCurrent/openbor/blob/master/LICENSE).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités



## ![](/emulators/bios.svg) BIOS

>**Aucun bios n'est requis.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensions supportées

Les roms doivent avoir l'extension :

* .pak

### Emplacement

Placez les roms comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 openbor
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.pak**

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/DCurrent/openbor/](https://github.com/DCurrent/openbor/)
* **Site officiel de la Senile Team** : [https://www.senileteam.com/](https://www.senileteam.com/)
* **Communauté officielle** : [http://www.chronocrash.com/forum/](http://www.chronocrash.com/forum/)