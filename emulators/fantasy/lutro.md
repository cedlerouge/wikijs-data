---
title: Lutro
description: Lua engine
published: true
date: 2021-09-16T04:34:01.036Z
tags: fantasy, lutro
editor: markdown
dateCreated: 2021-05-21T08:02:13.812Z
---

![](/emulators/fantasy/lutro.svg){.align-center}

## Fiche technique

* **Fabricant** : -
* **Année de sortie** : -

## Présentation

Lutro est un framework de jeu Lua expérimental pour Libretro suivant l'API LÖVE.

Lutro est un logiciel de rendu et implémente uniquement un sous-ensemble de l'API LÖVE. Il cible la portabilité via l'API libretro et s'appuie sur des dépendances.

## Émulateurs

[Libretro Lutro](libretro-lutro)
