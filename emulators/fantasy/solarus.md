---
title: Solarus
description: Arpg Game Engine
published: true
date: 2021-09-16T04:38:03.473Z
tags: fantasy, solarus
editor: markdown
dateCreated: 2021-05-21T08:02:32.474Z
---

![](/emulators/fantasy/solarus.svg){.align-center}

## Fiche technique

* **Fabricant :** Solarus
* **Année de sortie :** 19 septembre 2020

## Présentation

**Solarus** est un moteur de jeu Action-RPG 2D léger, libre et open-source a été spécifiquement conçu avec les Action-RPG cultes de l'ère 2D en tête, comme **The Legend of Zelda: A Link to the Past** et **Secret of Mana** sur Super Nintendo, ou **Soleil** sur Sega Megadrive/Genesis.

## Émulateurs

[Solarus](solarus)