---
title: LowRes NX
description: 
published: false
date: 2021-09-23T21:43:46.907Z
tags: 7.3+, fantasy, lowres-nx
editor: markdown
dateCreated: 2021-07-09T12:58:42.804Z
---

![](/emulators/fantasy/lowresnx.svg){.align-center}

## Fiche technique

* **Développeur** : Timo Kloss
* **Année de sortie** : 2017
* **Affichage** : 160x128 pixels, 60 Hz, 8 dynamic 6-bit palettes avec 4 couleurs chacun
* **Sprites** :  64, max 32x32 pixels
* **Son** : 4 voices, saw/tri/pulse/noise, pulse width, volume, ADSR, LFO

## Présentation

LowRes NX s'est inspiré des véritables systèmes 8 et 16 bits et simule des puces pour les graphismes, le son et les E/S, qui fonctionnent réellement comme du matériel classique. Il prend en charge les sprites matériels ainsi que le défilement parallaxe matériel, et propose même des interruptions verticales de blanc et de trame pour créer d'authentiques effets rétro.

## Émulateurs

[Libretro LowResNX](libretro-lowresnx)