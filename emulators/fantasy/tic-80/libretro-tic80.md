---
title: Libretro Tic80
description: 
published: true
date: 2021-09-16T04:39:44.681Z
tags: libretro, tic-80, tic80
editor: markdown
dateCreated: 2021-05-21T08:28:50.826Z
---

Libretro TIC-80 est un émulateur qui permet d'émuler un ordinateur fantastique pour créer, jouer et partager de petits jeux.

## ![](/emulators/license.svg) Licence

Ce core est sous licence [**MIT**](https://github.com/libretro/TIC-80/blob/master/LICENSE).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |
| Redémarrage | ✔ |
| Sauvegardes | ✔ |
| Options du core | ✔ |
| Netplay | ✔ |
| Native Cheats | ✔ |
| Controls | ✔ |

## ![](/emulators/bios.svg) BIOS

>**Aucun bios n'est requis.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensions supportées

Les roms doivent avoir l'extension :

* .fd
* .sap
* .k7
* .m7
* .rom
* .zip
* .7z

Les fichiers contenus dans les .zip/.7z doivent correspondre aux extensions citées précédemment.

### Emplacement

Placez les roms comme ceci : 

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 tic80
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.zip**

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

### Accéder aux options

Vous pouvez configurer diverses options de deux façons différentes.

* Via le Menu RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via le fichier `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Options du core

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/libretro/TIC-80/](https://github.com/libretro/TIC-80)
* **Code source officiel** : [https://github.com/nesbox/TIC-80/](https://github.com/nesbox/TIC-80/)
* **Site officiel** : [https://tic.computer/](https://tic.computer/)
* **Wiki source officiel** : [https://github.com/nesbox/TIC-80/wiki](https://github.com/nesbox/TIC-80/wiki)
* **Base de données Libretro** : [Libretro TIC-80 Database](https://github.com/libretro/libretro-database/blob/master/rdb/TIC-80.rdb)
* **Codes de triche Libretro Tic-80** : [https://github.com/libretro/libretro-database/tree/master/cht/TIC-80/](https://github.com/libretro/libretro-database/tree/master/cht/TIC-80)