---
title: Arcade
description: Systèmes Arcade
published: true
date: 2021-09-15T07:12:22.896Z
tags: arcade
editor: markdown
dateCreated: 2021-05-21T07:50:29.971Z
---

![](/emulators/arcade/arcade-logo.png)

![](/emulators/arcade/arcade-bartop.svg)

Un jeu d'arcade ou un jeu d'argent est un divertissement à pièces. La plupart des jeux d'arcade sont des jeux vidéo, des flippers, jeux électro-mécaniques, jeux d'échange ou marchandises.

Alors que les dates exactes sont débattues, l'âge d'or des jeux vidéo d'arcade est généralement défini comme une période commençant à la fin des années 1970 et se terminant au milieu des années 1980.

Hors résurgence brève du début des années 90, le secteur des salles de jeux électroniques a ensuite décliné dans l'hémisphère occidental : augmentation du nombre de consoles de jeux vidéo concurrentes dans leurs graphismes et leurs capacités de jeu et à coût réduit.

