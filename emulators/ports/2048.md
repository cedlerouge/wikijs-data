---
title: 2048
description: 
published: true
date: 2021-09-16T04:42:05.703Z
tags: ports, 2048
editor: markdown
dateCreated: 2021-05-22T09:43:31.415Z
---

![](/emulators/ports/2048.png){.align-center}

## Fiche technique

* **Développeur :** Gabriele Cirulli 
* **Éditeur :** Github 
* **Publisher :** Solebon LLC
* **Directeur :** Gabriele Cirulli 
* **Début du projet :** mars 2014 
* **Date de sortie :** 9 mars 2014 
* **Genre :** Réflexion 
* **Mode de jeu :** Un joueur 
* **Plate-forme :** iOS, Android, Navigateur web

## Présentation 

**2048** est un jeu vidéo de type puzzle conçu en mars 2014 par le développeur indépendant italien **Gabriele Cirulli** et publié en ligne sous licence libre via Github le 9 mars 2014.

Le but du jeu est de faire glisser des tuiles sur une grille, pour combiner les tuiles de mêmes valeurs et créer ainsi une tuile portant le nombre 2048. Le joueur peut toutefois continuer à jouer après cet objectif atteint pour faire le meilleur score possible.

Publié initialement sous licence MIT, sans prétention commerciale ou médiatique, le jeu a fait l'objet d'un fort intérêt. Le caractère particulièrement addictif de 2048, en plus d'être gratuit, lui attitrant une notoriété immédiate.

Cirulli atteste s'être inspiré d'un jeu mobile, 1024 de Veewo Studio, dont le développeur s'est lui-même inspiré de Threes! d'Asher Vollmer.

## Émulateurs

[Libretro 2048](libretro-2048)