---
title: Libretro REminiscence
description: REminiscence Core pour: Flashback - The Quest for Identity
published: true
date: 2021-09-16T04:47:31.973Z
tags: libretro, flashback, reminiscence
editor: markdown
dateCreated: 2021-05-21T08:33:03.408Z
---

Stuart Carnie a porté REminiscence, l'émulateur Flashback de Gregory Montoir, sur libretro! REminiscence est une reconstitution du moteur de jeu du jeu d'aventure et d'action de 1992/1993 Flashback. C'est le successeur spirituel de Another World / Out Of This World et il se distingue par des graphismes rotoscopiques, des cinématiques polygonales et un système de jeu de style Prince of Persia.

Ce port est toujours un travail en cours, mais il est en état de fonctionnement. Actuellement, il saute directement dans le jeu, sautant le menu principal.

Nous avons également ajouté le support modplug au cœur pour une lecture de musique améliorée.

Le noyau REminiscence a été créé par

* Grégory Montoir
* Stuart Carnie

## ![](/emulators/license.svg) Licence 

Ce core est sous licence **GPLv3**.

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités

| **Fonctionnalités** | **Supporté** |
| :--- | :--- |
| Captures d'écran | ✔ |
| Sauvegardes | ✔ |
| Controls | ✔ |
| Remapping | ✔ |

## ![](/emulators/bios.svg) BIOS

>**Aucun bios n'est requis.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Comment installer les fichiers nécessaires

Versions de jeu Flashback reconnues :

* Flashback DOS Demo (disquette)
* Flashback DOS Full version (disquette)
* Flashback DOS Full version (CD)

### Emplacement

Placez lez roms comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 ports
┃ ┃ ┃ ┣ 📁 Flashback
┃ ┃ ┃ ┃ ┣ 📁 data
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **instru_e.pal**

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

### Accéder aux options

Vous pouvez configurer diverses options de deux façons différentes.

* Via le menu RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via le fichier `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Options du core

## 🗂 Contenus complémentaires

### Ajouter le son pendant les cinématiques

L'exécutable prend également en charge les musiques pendant les cinématiques de la version **Amiga**. Il faut récupérer les fichiers dans le répertoire `music` des diskettes d'origines OU récupérer les fichiers sur le web et ajouter `mod.flashback-` devant chaque fichier music de la version Amiga.

>Ne pas mettre d'extensions de fichiers.
{.is-danger}

Liste des fichiers à placer dans `roms\ports\Flashback\data\` :

* mod.flashback-ascenseur
* mod.flashback-ceinturea
* mod.flashback-chute
* mod.flashback-desintegr
* mod.flashback-donneobjt
* mod.flashback-fin
* mod.flashback-fin2
* mod.flashback-game_over
* mod.flashback-holocube
* mod.flashback-introb
* mod.flashback-jungle
* mod.flashback-logo
* mod.flashback-memoire
* mod.flashback-missionca
* mod.flashback-options1
* mod.flashback-options2
* mod.flashback-reunion
* mod.flashback-taxi
* mod.flashback-teleport2
* mod.flashback-teleporta
* mod.flashback-voyage

### Ajouter les dialogues en jeu

Pour les dialogues en jeu, vous devez copier le fichier `VOICE.VCE` du répertoire `data` de la version SegaCD dans le répertoire `roms\ports\Flashback\data`

>Ne cherchez pas la version française des voix en SegaCD. Il n'existe que la version (USA) du jeu SegaCD.
{.is-warning}

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/libretro/REminiscence/](https://github.com/libretro/REminiscence)
* **Site officiel** : [http://cyxdown.free.fr/reminiscence/](http://cyxdown.free.fr/reminiscence/)