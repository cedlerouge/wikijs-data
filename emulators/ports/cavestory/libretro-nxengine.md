---
title: Libretro NXEngine
description: 
published: true
date: 2021-09-16T04:43:42.769Z
tags: libretro, cavestory, nxengine
editor: markdown
dateCreated: 2021-05-21T08:32:45.616Z
---

Libretro NXEngine est un moteur de réécriture open-source de Cave Story pour Dingux et MotoMAGX. Auteur - Caitlin Shaw (rogueeve).

## ![](/emulators/license.svg) Licence

Ce core est sous licence [**GPLv3**](https://github.com/gameblabla/nxengine-nspire/blob/master/LICENSE).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |
| Redémarrage | ✔ |
| Captures d'écran | ✔ |
| Sauvegardes | ✔ |
| Controls | ✔ |
| Remapping | ✔ |

## ![](/emulators/bios.svg) BIOS

>**Aucun bios n'est requis.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensions supportées

Les roms doivent avoir l'extension :

* .exe

### Emplacement

Placez les roms comme ceci : 

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 ports
┃ ┃ ┃ ┃ ┣ 📁 Cave Story
┃ ┃ ┃ ┃ ┃ ┣ 📁 CaveStory
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 **Doukutsu.exe**

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

### Accéder aux options

Vous pouvez configurer diverses options de deux façons différentes.

* Via le Menu RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via le fichier `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Options du core

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/libretro/nxengine-libretro/](https://github.com/libretro/nxengine-libretro)
* **Code source officiel** : [https://github.com/EXL/NXEngine/](https://github.com/EXL/NXEngine)
* **Site officiel** : [nxengine.sourceforge.net](http://nxengine.sourceforge.net/)
* **Wiki du jeu** : [https://www.cavestory.org/guides-and-faqs/list.php](https://www.cavestory.org/guides-and-faqs/list.php)