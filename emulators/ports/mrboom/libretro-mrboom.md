---
title: Libretro MrBoom
description: 
published: true
date: 2021-09-16T04:49:35.711Z
tags: libretro, mrboom
editor: markdown
dateCreated: 2021-05-21T08:33:17.479Z
---

Libretro MrBoom est un core pour faire fonctionner un clone de Bomberman jusqu'à 8 joueurs pour LibRetro / RetroArch.

## ![](/emulators/license.svg) Licence

Ce core est sous licence [**MIT**](https://github.com/libretro/mrboom-libretro/blob/master/LICENSE).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |
| Sauvegardes instantanées | ✔ |
| Rembobinage | ✔ |
| Netplay | ✔ |
| Controllers | ✔ |

## ![](/emulators/bios.svg) BIOS

>**Aucun bios n'est requis.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Extensions supportées

Le noyau Mr.Boom ne comporte pas d'utilisation d'extension. Il suffit de charger et de démarrer le core.

### Emplacement

Placez les roms comme ceci :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 ports
┃ ┃ ┃ ┃ ┣ 📁 mrboom
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **mrboom.game**

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

### Accéder aux options

Vous pouvez configurer diverses options de deux façons différentes.

* Via le menu RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via le fichier `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Options du core

## ![](/emulators/external-links.png) Liens externes

* **Code utilisé** : [https://github.com/libretro/mrboom-libretro/](https://github.com/libretro/mrboom-libretro)
* **Site officiel** : [http://mrboom.mumblecore.org/](http://mrboom.mumblecore.org/)