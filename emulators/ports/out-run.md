---
title: Out Run
description: 
published: true
date: 2021-09-16T04:51:05.449Z
tags: ports, out run
editor: markdown
dateCreated: 2021-05-21T08:05:01.075Z
---

![](/emulators/ports/outrun.gif){.align-center}

## Fiche technique

* **Développeur** : Chris White
* **Année de sortie** : 2014

## Présentation

**Out Run**, également orthographié **OutRun** ou **Outrun**, est un jeu vidéo d'arcade de course automobile conçu par Yū Suzuki et Sega-AM2 et commercialisé par Sega en 1986 sur borne d'arcade. Il fut porté sur de nombreux autres supports.

Le jeu a été un grand succès chez les amateurs d'arcade, à tel point qu'il est souvent considéré comme un des meilleurs jeu de course d'arcade. Il est remarquable de par son matériel novateur (comprenant une cabine de jeu mobile sur un système hydraulique), ses graphismes et sa musique innovantes, la possibilité de choisir la musique d'ambiance (soundtrack) ainsi que son parcours, et son thème fort de luxe et de détente.

Lors d'entretiens rétrospectifs au jeu, Yū Suzuki a classé Out Run non pas comme un jeu de course, mais comme un jeu de « conduite », bien qu'ayant avoué s'être en partie inspiré du film L'Équipée du Cannonball (1981).

## Émulateurs

[Libretro Cannonball](libretro-cannonball)