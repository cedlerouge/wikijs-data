---
title: Libretro PrBoom
description: 
published: true
date: 2021-09-16T04:46:20.979Z
tags: libretro, doom, prboom
editor: markdown
dateCreated: 2021-05-21T08:32:57.235Z
---

**PrBoom** est l'aboutissement d'années de travail par diverses personnes et projets sur le code source de Doom.  
PrBoom comprend le travail de tous les projets suivants :

* Original Doom source release
* DosDoom
* [Boom](http://www.teamtnt.com/boompubl/boom2.htm)
* MBF
* [LxDoom](http://lxdoom.linuxgames.com/)
* lSDLDoom

PrBoom est un portage du moteur graphique de Doom. Il peut faire fonctionner Doom 1 et Doom 2.

## ![](/emulators/license.svg) Licence

Ce core est sous licence [**GPLv2**](https://github.com/libretro/libretro-prboom/blob/master/COPYING).

## ![](/emulators/compatibility.png) Compatibilité

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Fonctionnalités

| Fonctionnalité | Supporté |
| :---: | :---: |
| Redémarrage | ✔ |
| Captures d'écran | ✔ |
| Sauvegardes | ✔ |
| Options du core | ✔ |
| RetroArch Cheats | ✔ |
| Native Cheats | ✔ |
| Controls | ✔ |
| Remapping | ✔ |

## ![](/emulators/bios.svg) BIOS

>Le bios est déjà présent dans le répertoire `/recalbox/share/bios/prboom/prboom.wad`
{.is-info}

## ![](/emulators/roms.png) Roms

>Les fichiers **WADs** sont assimilable à des "roms".
{.is-info}

>Placez vos roms Doom dans le répertoire`/recalbox/share/roms/ports/doom/`
{.is-warning}

>N'utilisez que des minuscules pour vos fichiers `.wad`
{.is-warning}

### Quelques infos sur les WADs

Même si les fichiers portent tous l'extension `.wad`, il en existe de plusieurs sortes :

* **IWAD** : c'est un `.wad` qui contient toutes les données et fonctionne tout seul.  
  Pour les plus connus:

  * `doom1.wad` pour les versions shareware de **DOOM.**
  * `doom.wad` pour les versions commerciales de **DOOM.**
  * `doom.wad ou doomu.wad` pour les versions de **The Ultimate DOOM.**
  * `doom2.wad` pour les versions de **DOOM II - Hell on Earth.**
  * `tnt.wad` pour les versions de **Final DOOM - TNT Evilution**
  * `plutonia.wad` pour les versions de **Final DOOM - The Plutonia Experiment**
  * `sigil.wad` pour les versions du dernière épisode de **DOOM - SIGIL**

* **PWAD** : c'est un `.wad` 'add-on' qui aura besoin d'un IWAD pour fonctionner \(par exemple: quasi tous les niveaux amateurs créés par la communauté Doom\).

Les WADs doivent aussi être compatibles avec PrBoom. Certains mods vont très loin dans la modification **(comme Brutal Doom, Doom Raider, Golden Eye,...)** mais **ne fonctionnent pas** avec PrBoom. Vous devrez chercher les infos sur le WAD auquel vous désirez jouer !  
Si un WAD remplace un seul niveau, ce ne sera pas forcément le niveau 1 de l'épisode 1. Il faudra atteindre le bon niveau ou utiliser un cheat pour y accéder directement.

### Liste de compatibilité des jeux WADs

| Fichier WAD | Nom du fichier - MD5 | Statut |
| :--- | :--- | :--- |
| **SIGIL** |  |  |
| SIGIL (v1.21) | `SIGIL_v1_21.WAD` - 743d6323cb2b9be24c258ff0fc350883 | ✅ sur Recalbox |
| **MASTER LEVEL for DOOM II (Registered)** |  |  |
| Master Levels for Doom II (Classic Complete) (PlayStation 3) | `MASTERLEVELS.WAD` - 84cb8640f599c4a17c8eb526f90d2b7a | ❌ |
| Master Levels for Doom II | `20x fichiers WADs séparés` | ❌ |
| **FINAL DOOM (Registred)** |  |  |
| Final Doom - Evilution | `TNT.WAD` - 4e158d9953c79ccf97bd0663244cc6b6 | ✅ |
| Final Doom - The Plutonia Experiment *v1.9* | `PLUTONIA.WAD` - 75c8cf89566741fa9d22447604053bd7 | ✅ |
| **DOOM II - Hell on Earth (Registered)** |  |  |
| Doom II - Hell on Earth (v1.9) | `DOOM2.WAD` - 25e1459ca71d321525f84628f45ca8cd | ✅ |
| **The Ultimate DOOM (Registered)** |  |  |
| Ultimate Doom, The *"v1,9ud"* | `DOOM.WAD, UDOOM.WAD, DOOMU.WAD` - c4fe9fd920207691a9f493668e0a2083 | ✅ |
| **DOOM (Registred)** |  |  |
| Doom (v1.9) | `DOOM.WAD` - 1cd63c5ddff1bf8ce844237f580e9cf3 | ✅ |
| **DOOM (Shareware)** |  |  |
| Doom (v1.9) (Demo) | `DOOM1.WAD` - f0cefca49926d00903cf57551d901abe | ✅ sur Recalbox |

## Jouer à SIGIL :

>Le fichier `doom.wad` dans le répertoire des roms est en réalité la version 1.9 Shareware du jeu **Doom** est devrait être renommé en `doom1.wad`
{.is-info}

* Pour jouer à **SIGIL** vous devez obligatoirement remplacer cette version Shareware `doom.wad` par la version **Registred** de **The Ultimate DOO v1.9.**

>Aucune autre version de `doom.wad` ne pourra faire fonctionner **SIGIL** !
{.is-warning}

* Pour cela :
  * Renommez le fichier existant à la racine de`roms\ports\doom\doom.wad` en `doom1.wad` (ou supprimez-le).
  * Collez le nouveau fichier `doom.wad` issue de la version **The Ultimate DOOM v1.9** en lieu et place de l'ancien.
  * Relancez une mise à jour de votre liste de jeux et lancez **SIGIL**.

>Quand vous lancez **SIGIL**, le lancement des quatre premiers épisodes vous lancera immanquablement le première épisode. Pour lancer **SIGIL**, sélectionnez simplement l'épisode SIGIL.
{.is-info}

>Pour obtenir une version **Registered** de **DOOM**, vous devez avoir les disquettes ou CD-Rom originaux, ou encore les obtenir sur STEAM ou GOG.
{.is-danger}

## Emplacement

Placez les roms comme ceci : 

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 ports
┃ ┃ ┃ ┃ ┣ 📁 Doom
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **\*.wad**

## ![](/emulators/advanced-configuration.png) Configuration avancée de l'émulateur

>Pour pouvoir conserver vos configurations personnalisées lors d'une mise à jour, nous vous conseillons d'utiliser notre fonctionnalité [Surcharges de configuration](./../../../advanced-usage/configuration-override).
{.is-info}

### Accéder aux options

Vous pouvez configurer diverses options de deux façons différentes.

* Via le menu RetroArch :

┣ 📁 Menu RetroArch
┃ ┣ 📁 Options du core
┃ ┃ ┣ 🧩 Name_option

* Via le fichier `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Options du core

## ![](/emulators/external-links.png) Liens externes

* **Code source utilisé** : [https://github.com/libretro/libretro-prboom/](https://github.com/libretro/libretro-prboom)
* **Documentation Libretro** :  [https://docs.libretro.com/library/prboom/](https://docs.libretro.com/library/prboom/)
* **Site officiel** : [http://prboom.sourceforge.net/about.html](http://prboom.sourceforge.net/about.html)