---
title: Hardwarekompatibilität
description: 
published: true
date: 2021-06-21T11:45:56.935Z
tags: 
editor: markdown
dateCreated: 2021-06-21T11:45:56.935Z
---

Auf den folgenden Seiten kannst du überprüfen, ob deine Hardware getestet wurde und unterstützt wird, sowie überprüfen, ob ein bestimmtes System oder ein Emulator mit deiner Hardware kompatibel ist.