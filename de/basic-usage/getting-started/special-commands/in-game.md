---
title: Im Spiel
description: Wenn der Emulator es erlaubt, ist es möglich im Spiel Sonderbefehle auszuführen, so wird das Spielerlebnis verbessertes.
published: true
date: 2021-07-04T21:02:56.987Z
tags: 
editor: markdown
dateCreated: 2021-07-04T20:48:40.898Z
---

> Bei Recalbox benutzen die meisten Tastenkürzel eine Tastenkombination mit der **HOTKEY** (`HK`) Taste. Diese muss zunächst in den Controller-Konfiguration festgelegt werden.
>
> Die Namen der anderen Tasten sind so wie auf dem Super Nintendo Controller (`L1`, `R1`, `L2`, `R2`, `L3` und `R3` orientieren sich am PlayStation Controller).
>
> Siehe [diese Seite](./../../../basic-usage/first-use-and-configuration#configuring-a-controller) für mehr Details.
{.is-info}

![](/basic-usage/getting-started/special-commands/controllers.png)

## Grundlegende Befehle

### Zurück zur Spieleliste

Beende das aktuelle Spiel und kehre zur Spieleliste zurück:

* `HK` + `START`

### Credits hinzufügen [Insert Coin].

Credits bei arcade und NeoGeo Spielen hinzufügen (Münze einwerfen):

* `SELECT`

### Spiel neu starten [Reset]

Starte das aktuelle Spiel neu:

* `HK` + `A`

### Zurückspulen [Rewind]

Das Spiel zurückspulen:

* `HK` + `LEFT`

### Vorspulen

Das Spiel vorspulen:

* `HK` + `RIGHT`

## Speichern

### Speichern [Save State]

Erstelle ein Speicherpunkt im aktuellen Slot:

* `HK` + `Y`

### Laden [Load State]

Lade den letzten Speicherpunkt im aktuellen Slot:

* `HK` + `X`

### Wechsle den Slot [Select Slot].

Wähle einen von 10 verfügbaren Backupslots (voreingestellt, von 0 bis 9 durchnummeriert);

* `HK` + `UP` oder `DOWN`

## CD wechseln

### CD auswerfen [Disk Eject]

Ermöglicht es das auswerfen oder einlegen einer CD in die Konsole zu simulieren (diese Funktion ist für Spiele auf mehreren Discs gedacht):

* `HK` + `L. STICK` nach _OBEN_.

### CDs wechseln [Disk Swap]

> Um CDs wechseln zu können, müssen alle Images des Spiels in einer `.M3U`-Datei sein.
{.is-info}

Vorige oder nächste CD auswählen (diese Funktion ist für Spiele auf mehreren Discs gedacht):

* `HK` + `L. STICK` nach _LINKS_ oder _RECHTS_

> Hier eine Zusammenfassung, wie man die CD in 3 Schritten wechselt:
>
> 1. _**Auswerfen drücken, um das Laufwerk zu öffnen:**_
>* `HK` + `L. STICK` nach _OBEN_. 
>2. _**Die CD wechseln (-1, -2, ... oder +1, +2, ...):**_
>* `HK` + `L. STICK` nach _LINKS_ oder _RECHTS_.
>3. _**Auswerfen drücken, um das Laufwerk zu schließen, sodass das Spiel fortfährt:**_
>* `HK` + `L. STICK` nach _OBEN_. 
{.is-success}

## Bildschirm

### Bildschirmphoto [Screenshot]

Mache ein Schnappschuss des aktuellen Geschehens auf dem Bildschirm (.png Datei wird im Ordner `screenshots` abgelegt):

* `HK` + `L1`

### Videoaufzeichnung [Recording]

Nimm ein Video des Bildschirms auf (Datei wird im Ordner `screenshots` abgelegt):

* Drücke `HK` + `R3` (Drücke `R. Stick`)

>Das erste mal drücken startet die Aufnahme, das zweite mal beendet sie.
{.is-info}

### Den Shader wechseln [Shaders Swap]

Wechsle den Videofilter (Shader) des Bilds:

* `HK` + `L2` oder `R2`

### Übersetzen [Translate]

Übersetze den Text auf dem Bildschirm ad-hoc und zeige ihn in einem Overlay an:

* `HK` + `L. STICK` nach _UNTEN_

>Benötigt eine Internetverbindung. Mehr Infos unter [Retroarch KI (In-game Übersetzung)](./../../../basic-usage/features/retroarch-ai-ingame-translation)
{.is-warning}

### FPS Anzeigen [FPS Display]

Zeige die Anzahl an Bildern (Frames) pro Sekunde (FPS) mit denen das Spiel gerendert wird:

* `HK` + `R. STICK` nach _UNTEN_

## Fortgeschritten

### Öffne das RetroArch-Menü [RetroArch Menu].

Das RetroArch-Menü öffnen:

* `HK` + `B`

>Wenn du Einstellungen im RetroArch-Menü anpassen und speichern möchtest, empfehlen wir dort die Option "**Save settings on Exit**" zu aktivieren.  
>Dannach werden **alle Änderungen** in dem Menü automatisch gespeichert, wenn du es verlässt.
{.is-info}

## Retroflag GPi Gehäuse

![](/basic-usage/getting-started/special-commands/gpicase.png)