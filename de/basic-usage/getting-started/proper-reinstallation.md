---
title: Saubere Neuinstallation
description: 
published: true
date: 2021-07-02T07:45:48.926Z
tags: 
editor: markdown
dateCreated: 2021-07-02T07:42:05.000Z
---

## Was sollte ich vor einer sauberen Neuinstallation des Systems tun?

### 1. Ich nutze keinen Wechseldatenträger, nur die microSD-Karte.

* Habe immer ein Backup deiner ROMs und der BIOS Dateien auf deinem PC oder einem externen Datenträger zur Hand.
* Erstelle ein Backup der Recalbox-Ordner `roms`, `bios` und `saves`, die du im `share`-Verzeichnis deiner Recalbox findest, auf deinem PC oder einem externen Datenträger.
* Lade [die neuste Version von Recalbox](https://download.recalbox.com/) herunter und installiere sie auf deiner microSD.

#### BIOS

* Prüfe die Signaturen deiner BIOS Dateien via EmulationStation.
* Wir fügem immer mal wieder neue Systeme hinzu, die neue andere BIOS Dateien benötigen.

### 2. Ich nutze einen Wechseldatenträger (USB-Stick/externe USB-Festplatte)

* Wir empfehlen das exFAT-Format für Festplatten mit großer Kapazität.
* Schließe die externe Festplatte oder den USB-Stick an deinen PC an und benenne dann den Ordner `recalbox` in `recalbox.old` um.
* Lade [die neuste Version von Recalbox](https://download.recalbox.com/) herunter und installiere sie auf deiner microSD.
* Schließe deine externe Festplatte oder den USB-Stick an deinen Raspberry Pi / Odroid an.
* Wähle dann deinen Wechseldatenträger in Recalbox aus.
* Die Recalbox-Ordnerstruktur wird auf deinem externen Speichermedium neu angelegt.
* Schalte deine Recalbox aus.
* Schließe deine externe Festplatte oder deinen USB-Stick an deinen PC an.
* Ersetze nun AUSCHLIEßLICH die Ordner `roms`, `bios`, und `saves` in der neuen Ordnerstruktur.
* **`gamelist.xml`/scraper Daten, eigene Themes und Overlays SOLLTEN NICHT ERSETZT werden** (verantwortlich für viele Bugs).
* Teste anschließen deine Recalbox mit den Standardeinstellungen.
* Wenn alles gut läuft, kannst du nach und nach mit der Anpassung deiner Einstellungen beginnen.

## Saubere Neuinstallation des Systems

Folge der Anleitung [Vorbereitung und Installation](./../preparation-and-installation)