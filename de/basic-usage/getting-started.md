---
title: Erste Schritte
description: 
published: true
date: 2021-06-24T11:22:24.559Z
tags: 
editor: markdown
dateCreated: 2021-06-24T11:22:24.559Z
---

Unabhängig davon, ob du ein neuer Benutzer bist oder bereits Kenntnisse über die Funktionsweise von Recalbox hast, findest du hier viele Informationen über die ersten Schritte.