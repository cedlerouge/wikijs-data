---
title: Vorbereitung und Installation
description: Wie man Recalbox installiert
published: true
date: 2021-08-26T11:05:06.710Z
tags: 
editor: markdown
dateCreated: 2021-06-23T15:25:29.689Z
---

## I - Kompatible Boards

Recalbox kann auf verschiedenen Gerätetypen installiert werden:

- **Raspberry Pi 4**
- Raspberry Pi 3
- Raspberry Pi 2
- Raspberry Pi 1/Zero
- Odroid Go Advance (Handheld-Konsole)
- Odroid Go Super (Handheld-Konsole)
- 32- oder 64-Bit Computer
- Odroid XU4

Um Recalbox kennenzulernen, ist der **Raspberry Pi 4** definitiv die erste Wahl des Teams!

## II - Recalbox installieren

### 1 - Download und Image flashen

Rufe [download.recalbox.com](https://download.recalbox.com/) auf und folge den Anweisungen, um das Recalbox-Image herunterzuladen und auf dein Speichermedium (SD-Karte, eMMC-Karte oder Festplatte) zu flashen.

>Es steht immer nur die **neueste Version** von Recalbox zum Download zur Verfügung.
>Die **alten Versionen** sind nicht mehr **herunterladbar** bzw. werden **vom Entwicklungsteam nicht länger unterstützt**.
{.is-warning}

>Du musst mindestens ein 8-GB-Medium für das System verwenden (wir empfehlen die **Sandisk Ultra Serie** für SD-Karten).
>
>* Für die **Installation auf Raspberry Pi**:
>   * Als Speichermedium: eine SD-Karte.
>* Für die **Installation auf Odroid**:
>    * Als Speichermedium: eine **SD- oder eMMC-Karte**. 
>* Für die **Installation auf x86 und x86_64**:
>    * Als Speichermedium: eine **Festplatte**.

### 2 - Installation

#### Raspberry Pi / Odroid Go Advance / Odroid Go Super

* Lege die microSD-Karte in das Gerät ein, mit dem die Recalbox verwendet werden soll.
* Schalte das Board ein/stecke es an und die Installation von Recalbox wird automatisch gestartet.
* Die Installation variiert je nach Hardware und dauert einige Minuten.

#### Retroflag GPI Case

Die Installation auf dem GPI-Case ist ebenso einfach: Stecke die Micro-SD-Karte ein, schließe dein GPI-Case an die Stromversorgung an, und Recalbox erkennt und konfiguriert das Case automatisch. Weitere Informationen findest du auf der Seite [RetroFlag GPi Case](./preparation-and-installation/retroflag-gpi-case)

#### Odroid XU4 / Odroid XU4Q

* Setze die microSD- oder eMMC-Karte in das Gerät ein, mit dem du Recalbox verwenden möchtest.
* Starte das Gerät und du solltest sehen, wie Recalbox zum Leben erwacht.

#### x86 / x86_64

Die Installation auf einem USB-Stick (alle Geräte) wird nur zu Testzwecken empfohlen. Da es sich dabei nicht um einen permanenten Vorgang handelt, können Fehler auftreten, die bei der Festplatteninstallation nicht auftreten.

Wir empfehlen daher eine **Installation auf Festplatte:**

* Eine für das **System**.
* Eine für die Speicherung von **ROMs, BIOS, Saves, etc...**

* Starte das Gerät und du solltest sehen, wie Recalbox zum Leben erwacht.

## III - Die notwendige Ausstattung und das Zubehör

Prüfe, ob du das erforderliche Speichergerät und die erforderliche Spannungsversorgung für das gewählte Gerät hast.

| Kategorie | Raspberry Pi 0/0w/1/2/3 | Raspberry Pi 4 | Odroid XU4 | Odroid Go | PC |
| :--- | :--- | :--- | :--- | :--- | :--- |
| Stromversorgung | µUSB 2.5A (gute Qualität) | USB-C 3A 5V | Verwendung des offiziellen Odroid-Netzteils | Interner Akku | Standard PC-Netzteil |
| Video | HDMIc Kabel, HDMI2VGA oder HDMI2DVI | µHDMI-HDMI Kabel | HDMI Kabel | Intern | HDMI/VGA (DVI und DP eventuell) |
| Controller | USB oder Bluetooth | USB oder Bluetooth | USB oder Bluetooth | USB oder Bluetooth | USB oder Bluetooth |
| Systemspeicher | 8GB+ Klasse 10 µSD-Karte außer Pi1: SD | 8GB+ µSD-Karte | 8GB+ µSD-Karte | 8GB+ µSD-Karte | 8GB+ Festplatte |
| Speicherkonfiguration, BIOS und ROMs | externe Festplatte mit eigener Stromversorgung | externe Festplatte mit eigener Stromversorgung | externe Festplatte mit eigener Stromversorgung | µSD Karte | Interne Festplatte oder externe USB-Festplatte (auf der Hauptplatine eingesteckt) |

## Fehlersuche x86 / x86_64:

* **Dein Computer bootet nur in Windows:** Stelle sicher, dass **Secure Boot** deaktiviert ist.

  * Informiere dich im Internet, wo sich die Secure-Boot-Option befindet (je nach Hersteller und Modell des Computers unterschiedlich).
  * Stelle Secure Boot dann aus/deaktiviere es.
  * Starte den Computer dann neu.

* **Recalbox bootet nicht**: Stelle dein **bios** auf Legacy.
  * Um das BIOS auf Legacy einzustellen, drückst du beim Hochfahren des Computers die Bios-Zugriffstaste `F*` (`F1` bis `F12`, je nach Hersteller unterschiedlich) oder `Del`.
  * Informiere dich im Internet, wo sich die Legacy-Option befindet (je nach Computerhersteller und -modell unterschiedlich).
  * **Aktiviere** dann den Legacy Modus
  
* **Im Falle eines dedizierten Multiboot-PCs**, wenn du möchtest, dass deine **Recalbox** vor allen anderen Systemen bootet:
  * Rufe das **Boot-Menü** der Festplatten auf.
  * Drücke `F*`, um das **Bootmenü** der Festplatten aufzurufen (`F1` bis `F12`, je nach Hersteller unterschiedlich).
  * Wähle die **Systemfestplatte** Recalbox.