---
title: Update-Manager
description: 
published: true
date: 2021-07-29T07:20:44.481Z
tags: 
editor: markdown
dateCreated: 2021-07-29T07:20:44.481Z
---

![](/basic-usage/features/update.png)

## Eine neue Art, Aktualisierungen durchzuführen!

Seit Recalbox 7 wurde das Update-System komplett überarbeitet und funktioniert nicht mehr mit älteren Versionen.

* Das neue Aktualisierungssystem ist viel schneller:
  * _**5 bis maximal 10 Minuten für eine Aktualisierung**_ (natürlich ohne Download-Zeit). 
* Das neue Aktualisierungssystem ist wesentlich robuster:
  * Keine abstürzenden Updates mehr, die einen zwingen, wieder von vorne anzufangen (auch wenn das zum Glück selten vorkam) 
* Neue Aktualisierungen werden von EmulationStation übernommen: Fortschrittsanzeige, Fehlerbehandlung usw.
* Ab sofort können Updates auch offline sehr einfach durchgeführt werden: man kopiert einfach das neueste Image in ein Verzeichnis der SD-Karte, dass von allen Betriebssystemen (Windows, Linux und macOS) zugänglich ist. Dann ein Neustart und das System erkennt das Image und führt die Aktualisierung durch!
* Du kannst nun deine eigenen Bootvideos in einem via Netzwerk leicht zugänglichen Verzeichnis speichern (in `/share/bootvideos/`) und auswählen, ob der Player nur Videos von Recalbox, nur von dir oder von allem, was er zur Verfügung hat, abspielen soll.
  * Du fragst dich, was das mit Aktualisierungen zu tun hat? Nichts! Es ist nur so, dass diese neue Möglichkeit mit der neuen Struktur von Recalbox verbunden ist, um Updates zu erleichtern.
* Unabhängig davon, wer Recalbox genauso betrügt und bestiehlt wie seine Kunden, können _**Update-Benachrichtigungen**_ in EmulationStation nicht mehr deaktiviert werden: nur die aufdringliche Popup-Benachrichtigung kann deaktiviert werden. Aber du wirst immer ein nicht-aufdringliches Popup sehen, das dich darüber informiert, daß ein Update verfügbar ist.

## Funktionsweise

Die Aktualisierung kann auf 2 Arten erfolgen: online oder offline.

## {.tabset}

### Online-Methode

Die Online-Methode ist am einfachsten:

* Drücke in EmulationStation die Taste `START` auf deinem Controller.
* Gehe dann zu `UPDATES`.
* Wenn ein Update verfügbar ist, siehst du auf der rechten Seite die Zeile `UPDATE VERFÜGBAR` mit der Angabe `JA`. Wenn ja, gehe zu der Zeile `UPDATE STARTEN` und bestätige die Auswahl.
* Das Update wird nun heruntergeladen.
* Nach dem Herunterladen wird deine Recalbox automatisch neu gestartet und die Aktualisierung wird durchgeführt.

### Offline-Methode

Diese Methode ist etwas komplizierter durchzuführen.

* Auf [dieser Seite](https://download.recalbox.com) findest du alle Images der neuesten Version von Recalbox.
* Lade das deiner Architektur (RPi/Odroid/Pc) entsprechende Image und die entsprechende `.SHA1`-Datei herunter.
* Nach dem Herunterladen gibt es 2 Möglichkeiten:
  * Du kannst Software wie [WinSCP](./Tutorials/system/access/network-access-winscp), [Cyberduck](./Tutorials/system/access/network-access-cyberduck) oder [MobaXTerm](./tutorials/system/access/network-access-mobaxterm) verwenden, um die beiden Dateien in das Verzeichnis `/boot/update/` zu kopieren (indem die [Boot-Partition](./tutorials/system/access/remount-partition-with-write-access) mit Lese-/Schreibzugriff eingebunden wird)
  * Nimm die SD-Karte aus deiner ausgeschalteten Recalbox, stecke sie in deinen PC, öffne das Laufwerk namens `RECALBOX` und lege die 2 Dateien in das Verzeichnis `/update`.
* Sobald dies geschehen ist, startest du deine Recalbox neu und die Aktualisierung wird durchgeführt.

## Im Fehlerfall

Wenn während des Updates alle 7 Balken sofort rot statt weiß werden, schreibe auf, wie viele weiße Balken zu sehen waren, und kontaktiere den Support im [Forum](https://forum.recalbox.com) oder über [Discord](https://discord.gg/NbQFbGM)).