---
title: Die "Kalibrierung"
description: 
published: true
date: 2021-07-31T09:08:48.939Z
tags: 
editor: markdown
dateCreated: 2021-07-31T09:08:48.939Z
---

Bei einigen Spielen, insbesondere bei Arcade-Spielen, ist es oft notwendig, das Spiel vor dem Spielen zu konfigurieren. Im Fall von Arcade-Spielen musst du dies aber nur einmal tun, da die Konfiguration für das jewilige Spiel in deinem NVRAM oder einer anderen .CFG bleibt, die du je nach System und/oder Emulator in deinem "Saves"-Verzeichnis deiner `share` Partition findest.

![](/basic-usage/features/lightgun/calibration/calibration1.png)

Um ein entsprechendes Spiel es zu konfigurieren, musst du aber möglicherweise in die Core- oder sogar in die Spielkonfiguration gehen.