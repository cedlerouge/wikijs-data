---
title: Overlays
description: Auf breiten Bildschirmen Systembilder anzeigen, die sich um den emulierten Bildschirm herum erstrecken.
published: true
date: 2021-08-26T11:03:24.347Z
tags: 
editor: markdown
dateCreated: 2021-07-29T07:44:11.033Z
---

## Beschreibung

**Overlays** (auch *Bezels* genannt) sind Grafiken, die den Bildschirm während des Spielens "schmücken". Diese Illustrationen werden oft auf den schwarzen Bereichen von 16/9-Bildschirmen platziert, die von Spielen, die in 4/3 angezeigt werden, nicht genutzt werden.

**Street of Rage 3** mit dem Megadrive-Overlay, auf einem 16:9-Bildschirm: 
![overlays.png](/basic-usage/features/overlays/overlays.png){.full-width}


**Street of Rage 3** ohne Overlay, auf einem 16/9-Bildschirm:
![43.png](/basic-usage/features/overlays/43.png){.full-width}

**Street of Rage 3** ohne Overlay, auf einem 16:9-Bildschirm, mit gestrecktem Bild:
![169.png](/basic-usage/features/overlays/169.png){.full-width}



## Recalbox Overlays

Recalbox beinhaltet die **Recalbox Overlays**, die standardmäßig auf kompatiblen Bildschirmen angezeigt werden.

Du kannst **Recalbox Overlays** im **Spieleinstellungen** Menü von EmulationStation aktivieren oder deaktivieren:

![overlays-set-option-es.png](/basic-usage/features/overlays/overlays-set-option-es.png){.full-width}

### Aktivieren von Overlays per System

In der `recalbox.conf` kann man Overlays generell aktivieren oder deaktivieren:

```ini
global.recalboxoverlays=1
```

Oder nur für ein bestimmtes System aktivieren/deaktivieren (in diesem Beispiel für SNES)
```ini
snes.recalboxoverlays=0
```

## Eigene Overlays hinzufügen

Es gibt verschiedene Overlay-Pakete, von denen einige ein Overlay pro Spiel bieten!

Wenn du ein fertiges Overlay-Paket heruntergeladen hast, dass zu Recalbox passt, kopiere alle darin enthaltenen Verzeichnisse in das Verzeichnis `overlays` auf deiner `SHARE`-Partition:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 overlays
┃ ┃ ┃ ┣ 📁 megadrive
┃ ┃ ┃ ┃ ┣ 🗒 megadrive.cfg
┃ ┃ ┃ ┃ ┣ 🗒 megadrive_overlay.cfg
┃ ┃ ┃ ┃ ┣ 🗒 megadrive.png
┃ ┃ ┃ ┣ 📁 snes
┃ ┃ ┃ ┃ ┣ 🗒 .....

> Benutzerdefinierte Overlays haben Vorrang vor automatischen Overlays. 
> {.is-info}