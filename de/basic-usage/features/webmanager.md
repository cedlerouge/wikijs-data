---
title: Recalbox Manager
description: 
published: true
date: 2021-07-29T07:34:46.684Z
tags: 
editor: markdown
dateCreated: 2021-07-29T07:30:49.615Z
---

## Aufrufen

Gib einfach den Namen in deinen Webbrowser ein:

## {.tabset}

### Windows

[http://recalbox/](http://recalbox/)

Wenn das nicht funktioniert, kannst du [die IP-Adresse deiner Recalbox](./../../tutorials/network/ip/discover-recalbox-ip) verwenden.

### Linux und macOS

[http://recalbox.local/](http://recalbox.local/)

Wenn das nicht funktioniert, kannst du [die IP-Adresse deiner Recalbox](./../../tutorials/network/ip/discover-recalbox-ip) verwenden.

## Oberfläche

Sobald du auf der Hauptseite bist, kannst du:

* Die Datei `recalbox.conf` bearbeiten.
* ROMs hinzufügen/entfernen.
* Logs einsehen.
* Dein Board überwachen (CPU/Temp/Speicher/etc.)
* Starte des virtuellen Gamepads (nützlich, um in 5 Minuten deine Recalbox für den ersten Gebrauch zu konfigurieren) - nur über Smartphone oder Tablet.

## Deaktivieren des Webmanagers

Standardmäßig wird der Webmanager automatisch gestartet. Wenn du willst, kannst du ihn aber auch deaktivieren.

* Drücke in EmulationStation die Taste `START`.
* Gehe zu `ERWEITERTE EINSTELLUNGEN`.
* Stelle dann den `RECALBOX MANAGER` auf AUS.

## Virtuelles Gamepad

Um direkt darauf zuzugreifen, gib einfach die IP-Adresse deiner Recalbox mit Port 8080 ein.

```text
http://192.168.X.X:8080
```

```text
http://recalbox:8080
```

Schaue dir die Anleitung für virtuelle Gamepads [hier](./../../basic-usage/first-use-and-configuration#virtuelle-controller) an.

Wenn du ein mögliches Problem oder einen Fehler findest, melde ihn bitte [hier](https://github.com/recalbox/recalbox-manager).