---
title: Kodi Media Center
description: 
published: true
date: 2021-07-05T09:25:57.030Z
tags: 
editor: markdown
dateCreated: 2021-07-05T09:25:57.030Z
---

![](/basic-usage/features/kodi/kodilogo.png)

Kodi ist eine in Recalbox integrierte Multi-Medien-Software.

Über eine Ethernet-Verbindung kannst du eine Vielzahl von Streams empfangen, um Filme, Musik und mehr zu genießen.

Mit einer richtig konfigurierten KODI-Installation kann auf Live- und aufgezeichnete Medien zugegriffen werden. Ebenso lässt sich einfach eine Verbindung zu eigenen lokalen Medien über USB oder das Netzwerk herstellen.

>Controller werden in Kodi unterstützt, aber wenn du es vorziehst, kannst du HDMI CEC oder eine Remote-Smartphone-App verwenden. 
{.is-info}

## Grundlagen

### Starten und Beenden

Du kannst **Kodi Media Center** entweder durch Drücken der `X`-Taste (in EmulationStation) oder durch Drücken von `START` + Auswahl des ersten Menüeintrags (KODI) starten.

Um KODI zu beenden, wähle das Power-Symbol oben links im KODI-Hauptmenü und wähle dann eine der beiden folgenden Optionen:

* "Power Off System"
* "Reboot"

Beide Punkte bringen dich zurück zum Hauptmenü von EmulationStation; folge anschlißend den normalen Befehlen/Abläufen in EmulationStation, wenn du deine Recalbox herunterfahren willst.

Das Ausschalten kann nicht direkt aus KODI heraus erfolgen. Du musst erst zu EmulationStation zurückkehren, um deine Recalbox von dort aus herunterzufahren..

### Neue KODI-Benutzer:

Beim ersten Start hat KODI nur wenige Optionen. Du brauchst eine Ethernet-Verbindung, es sei denn, du verwendest nur lokale oder über USB angeschlossene Medien.

Das [KODI-Wiki](http://kodi.wiki/view/Main_Page) enthält weitere Informationen zu den Besonderheiten der Einrichtung und Verwendung von KODI. KODI ist recht intuitiv.
Normalerweise werden USB-Medien automatisch gefunden, wenn du die Video- oder Musikoptionen aufrufst; du solltest deine über USB angeschlossenen Medien oder einen Pfad zu ihnen sehen.

Darüber hinaus verfügen die Registerkarten Video, Musik und andere über "Addons" (ähnlich einer Anwendung).  
Es gibt mehrere "Addons", die du direkt aus der Repository von KODI selbst herunterladen kannst, und viele weitere Repositories (Sammlungen von Addons), die Benutzer erstellt haben, so dass es sich lohnt, manchmal ein wenig tiefer zu graben, um tolle Inhalte zu finden.

* Um Musik- oder Video-Addons zu erhalten, navigiere zu den "Addons"-Registerkarten unter der Registerkarte "Musik" oder "Video" und klicken diese an, oder
* Wähle die Hauptregisterkarte durch Anklicken aus und lass dir so alle lokal angeschlossenen Medien oder Geräte anzeigen, auf denen bereits "Addons" verfügbar sind. Klicke auf "Addons" und suche die Schaltfläche mit der Aufschrift "Get More". Dies führt dich zu einer Liste der von KODI zugelassenen "Addons". Wähle einfach die für dich passenden Addons aus und klick auf "Installieren".

Dies geschieht im Hintergrund, und wenn du eine gute Ethernet-Verbindung hast, dauert der Download und die Installation normalerweise weniger als eine Minute.

Es sei noch einmal erwähnt, dass das [Kodi-Wiki](http://kodi.wiki/view/Main_Page) über zahlreiche und detaillierte Anleitungen verfügt.

Hinweise:

* Wenn du Kodi direkt nach dem Einschalten der Recalbox starten willst, ohne vorher EmulationStation zu starten, gibt es dazu ein paar Optionen in der [recalbox.conf](./../../basic-usage/getting-started/recalboxconf-file)-Datei.
* Einige Leute verkaufen Dongles oder Anleitungen, mit denen man bestimmte Addons in sein Kodi implementieren kann. Das sind nur Addon-Sammlungen oder vorgefertige Verzeichnisse, die du immer auch kostenlos im Netz findest. Also: NICHT KAUFEN!
* Für einige Addons ist ein Abonnement erforderlich, wie z. B. für Netflix oder Amazon Prime. Für andere hingegen genügt es, ein Konto zu erstellen, wie z. B. für Pandora.
* Wenn deine Controller verrückt spielen, wenn du sie in KODI verwendest, sind sie nicht richtig konfiguriert. Überprüfe die Controller-Konfiguration; Abschnitt [Recalbox-Controller](./../../basic-usage/first-use-and-configuration#controller-konfiguration)
* Die Raspberry Pi Version von KODI enthält nicht alle Funktionen und Add-Ons, aber die meisten sind vorhanden.
* Beim ersten sowie bei jedem Start von KODI werden Updates oder Hintergrundprüfungen durchgeführt. Dies kann zu einer unregelmäßigen oder verzögerten Nutzung einiger Funktionen führen. Normalerweise musst du nur eine Weile warten, bis diese durchegführt sind. Je mehr Addons du installiert hast, desto länger kann es dauern.
* Die in Recalbox aktuell verwendete version von KODI ist "Leia".
* Klicke hier für [SMB v2+ auf KODI](https://discourse.osmc.tv/t/kodi-and-smbv1/37441/5).

## Einstellungen

Die Datei [recalbox.conf](./../../basic-usage/getting-started/recalboxconf-file) enthält einige Optionen für Kodi. Du kannst KODI bspw. aktivieren/deaktivieren, die X-Tasten-Verknüpfung aktivieren/deaktivieren oder Kodi automatisch beim Booten starten. All das kann aber auch aus EmulationStation heraus getan werden (`START` > `ERWEITERTE EINSTELLUNGEN` > `KODI EINSTELLUNGEN`).
 
>Controller werden in Kodi unterstützt. 
>
>Du kannst die **HDMI CEC**-Funktion deines Fernsehers nutzen, um Kodi über deine Fernbedienung oder **Yatse**, eine Kodi-Fernbedienung für Smartphones, zu steuern.
{.is-info}

## Sonstiges

### Energieverwaltung mit HDMI CEC

Wenn du versuchst, Kodi zu beenden oder herunterzufahren, sendet dein Raspberry immer einen HDMI-Shutdown-Befehl an deinen A/V-Receiver + TV. Als Ergebnis werden alle deine "Geräte" ausgeschaltet.

Um dieses Verhalten zu verhindern, solltest du die Einstellung "Shut down device on exit" deaktivieren, indem du zu `System` -> `Settings` -> `System` -> `Input devices` -> `Devices` -> `CEC Adapter` gehst.

Mit einem CEC-Adapter lässt sich KODI in einigen Fällen über eine normale TV-Fernbedienung steuern, sofern diese CEC unterstützt. CEC sendet Fernbedienungsbefehle über ein HDMI-Kabel an KODI. [Wiki Kodi CEC](http://kodi.wiki/view/CEC)

### Konfiguration von Yatse

Yatse und einige andere Anwendungen sind Fernbedienungen für KODI. Diese Apps bieten oft mehr Kontrolle und eine intuitivere Steuerung von KODI. Es ist nicht zwingend erforderlich, aber wenn dir KODI gefällt, wirst du die Erfahrung mit der App-Fernbedienung wahrscheinlich noch mehr genießen.

Dazu musst du zunächst KODI auf deiner Recalbox starten.

Nach der Installation der Yatse-Anwendung sucht Yatse in deinem Netzwerk nach verfügbaren KODI-Installationen.

![](/basic-usage/features/kodi/configure_yatse_for_kodi_recalbox_1-300px.png)

Du kannst diesen Schritt überspringen und zur manuellen Konfiguration übergehen. Gebe einfach die [IP-Adresse](./../../tutorials/network/ip/discover-recalbox-ip) der Recalbox in deinem Netzwerk und den Port **8081** ein.

![](/basic-usage/features/kodi/configure_yatse_for_kodi_recalbox_2-300px.png)

Dein Yatse-Controller ist nun angeschlossen:

![](/basic-usage/features/kodi/configure_yatse_for_kodi_recalbox_3-300px.png)

Wenn dir Yatse gefällt, kaufe den **Unlocker**, um neue Funktionen zu erhalten.

### Controller-Konfiguration

Die Konfiguration zwischen einem Joystick und der jeweiligen Aktion in KODI kann in der Datei `/recalbox/share/system/configs/kodi/input.xml` über die hier aufgeführten Aktionen und Funktionen geändert werden: [http://kodi.wiki/view/Keymap#Commands](http://kodi.wiki/view/Keymap#Commands)

Die Tasten auf dem Controller haben folgende Belegung:

| Taste am Joystick | Funktion |
| ---: | :--- |
| `A` | Bestätigen |
| `B` | Abbrechen |
| `X` | Stop |
| `Y` | Menü |
| `START` | Pause |
| `SELECT` | Kontextmenü |
| `HOCH` | Hoch |
| `RUNTER` | Runter |
| `RECHTS` | Rechts |
| `LINKS` | Links |
| `LINKER STICK HOCH` | Lautstärke erhöhen |
| `LINKER STICK RUNTER` | Lautstärke reduzieren |
| `LINKER STICK NACH LINKS` | Lautstärke reduzieren |
| `LINKER STICK NACH RECHTS` | Lautstärke erhöhen |
| `RECHTER STICK NACH OBEN` | Hoch |
| `RECHTER STICK NACH UNTEN` | Runter |
| `RECHTER STICK NACH LINKS` | Links |
| `RECHTER STICK NACH RECHTS` | Rechts |

### Kein Ton am Kopfhörerausgang:

Wenn du Kodi so konfiguriert hast, dass es den Kopfhörer-Audioausgang verwendet, du aber keinen Ton bekommst, versuche, diese Konfigurationsvariablen in der Datei `/boot/recalbox-user-config.txt` zu setzen:

```ini
hdmi_ignore_edid_audio=1
hdmi_force_edid_audio=0
```

Dadurch wird erzwungen, dass der Ton über den Kopfhörerausgang läuft.