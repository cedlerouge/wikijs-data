---
title: Suchfunktion
description: 
published: true
date: 2021-06-30T16:21:48.883Z
tags: 
editor: markdown
dateCreated: 2021-06-30T16:21:48.883Z
---

## Übersicht

Lange von unseren lieben Usern gewünscht, jetzt endlich verfügbar!

_**Durchsuche schnell deine Spielesammlung, indem du ein paar Buchstaben eingibst**_

Die Suche ist in Echtzeit. Die Liste wird aktualisiert, während du etwas eingibst. Du kannst in Namen, Dateien, Beschreibungen oder überall auf einmal suchen.

Natürlich konnten wir ein solches Feature nicht ohne eine gründliche Überprüfung der virtuellen Tastatur anbieten.  
Vor dem Fernseher sitzend, den Controller in der Hand... was könnte da lästiger sein, als auf der alten Vollbildtastatur herumzuhantieren, wo die Eingabe eines einzigen Zeichens viel zu viele Handgriffe erforderte?

Also haben wir eine brandneue, schöne und viel ergonomischere Tastatur entwickelt: eine Tastatur im Arcade-Stil, mit einem Zeichenrad, bei dem das Auswählen, Löschen und Bewegen des Cursors ein wahres Vergnügen ist.

Natürlich ist sie nativ kompatibel mit einer physischen Tastatur. Und um es noch besser zu machen, ist sie halbtransparent, so dass du in Echtzeit sehen kannst, was sich dahinter abspielt.

Ohne Umschweife, hier einige Bilder!

![](/basic-usage/features/search.png){.full-width}

## Wie es funktioniert

* Durch Drücken von `R1` im Konsolenmenü kannst du die Suchfunktion aufrufen.
* Beginne mit der Eingabe des Namens eines Spiels (Beispiel oben: Super).
* Genieße die Anzeige der Liste der Spiele, die mit deinem Suchwort beginnen.
* Du musst nur das Spiel auswählen, das du spielen möchtest.