---
title: Contribution Lizenzen
description: 
published: true
date: 2021-10-02T10:23:52.889Z
tags: contribution, lizenz
editor: markdown
dateCreated: 2021-10-02T10:23:52.889Z
---

Für Softwareentwicklungsprojekte und kreative Werke gilt das übliche Lizenzierungsschema " Inbound=Outbound", sofern nicht anders angegeben.

Wie in den [Open-Source-Leitfäden](https://opensource.guide/legal/) von GitHub klar erläutert, bedeutet dies, dass eine Open-Source-Lizenz implizit als Inbound- (von den Mitwirkenden) und Outbound-Lizenz ( für andere Mitwirkende und Nutzer) dient.

Das bedeutet, dass Du, wenn Du zu einem unserer Projekte beiträgst, die Bedingungen der Lizenz akzeptierst und Deinen Beitrag unter denselben Bedingungen einreichst.