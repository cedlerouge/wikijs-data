---
title: Lizenz
description: Dieser Bereich enthält Erklärungen zu rechtlichen Aspekten und anderen langweiligen, aber wichtigen Dingen
published: true
date: 2021-08-18T07:46:47.625Z
tags: lizenz, rechtliches
editor: markdown
dateCreated: 2021-08-18T07:46:47.625Z
---

Recalbox verwendet mehrere Pakete unter bestimmten Lizenztypen (GPL, BSD, MIT usw.) und Recalbox selbst verwendet eine freie Lizenz.

Dies ist die Beschreibung der für Recalbox verwendeten Lizenz:

>Die Weitergabe und Nutzung des RECALBOX Codes oder eines davon abgeleiteten Werkes ist erlaubt, sofern die folgenden Bedingungen erfüllt sind:
>
>Redistributionen dürfen nicht ohne Erlaubnis verkauft oder für ein Produkt oder eine kommerzielle Aktivität verwendet werden.
>Modifizierte Distributionen müssen den vollständigen Quellcode enthalten, einschließlich des Quellcodes aller Komponenten, die von einer aus den modifizierten Quellen erstellten Binärdistribution verwendet werden.
>Redistributionen müssen den obigen Copyright-Hinweis, diese Liste von Bedingungen und den folgenden Haftungsausschluss in der Dokumentation und/oder anderen mit der Distribution gelieferten Materialien wiedergeben.
>DIESE SOFTWARE WIRD VON DEN URHEBERRECHTSINHABERN UND MITWIRKENDEN SO ZUR VERFÜGUNG GESTELLT, WIE SIE IST, UND JEGLICHE AUSDRÜCKLICHE ODER STILLSCHWEIGENDE GARANTIE, EINSCHLIESSLICH, ABER NICHT BESCHRÄNKT AUF DIE STILLSCHWEIGENDE GARANTIE DER MARKTGÄNGIGKEIT UND DER EIGNUNG FÜR EINEN BESTIMMTEN ZWECK, WIRD ABGELEHNT. IN KEINEM FALL HAFTEN DER URHEBERRECHTSINHABER ODER DIE MITWIRKENDEN FÜR DIREKTE, INDIREKTE, ZUFÄLLIGE, BESONDERE, BEISPIELHAFTE ODER FOLGESCHÄDEN (EINSCHLIESSLICH, ABER NICHT BESCHRÄNKT AUF DIE BESCHAFFUNG VON ERSATZWAREN ODER -DIENSTLEISTUNGEN, NUTZUNGS-, DATEN- ODER GEWINNVERLUSTE ; ODER GESCHÄFTSUNTERBRECHUNG), UNABHÄNGIG VON DER URSACHE UND DER HAFTUNGSTHEORIE, OB AUS VERTRAG, VERSCHULDENSUNABHÄNGIGER HAFTUNG ODER UNERLAUBTER HANDLUNG (EINSCHLIESSLICH FAHRLÄSSIGKEIT ODER SONSTIGEM), DIE SICH IN IRGENDEINER WEISE AUS DER VERWENDUNG DIESER SOFTWARE ERGEBEN, AUCH WENN AUF DIE MÖGLICHKEIT SOLCHER SCHÄDEN HINGEWIESEN WURDE.

>Obwohl es keine rechtliche Voraussetzung für die Lizenz ist, bitten wir jeden, der abgeleitete Werke, modifizierte Versionen oder Forks von Recalbox erstellen möchte, keinen Code zu verwenden, der noch nicht den "Master"-Zweig erreicht hat.
{.is-info}