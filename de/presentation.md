---
title: Weitere Informationen
description: 
published: true
date: 2021-06-21T13:13:21.373Z
tags: 
editor: markdown
dateCreated: 2021-06-21T13:13:21.373Z
---

Hier findest du eine allgemeine Beschreibung von Recalbox, wer wir sind, die Nutzungslizenzen sowie die Möglichkeit, auf verschiedene Art und Weise zum Projekt beizutragen!