---
title: Zum Recalbox Wiki beitragen
description: Bitte lies diese Anleitung, bevor du einen Artikel zum Wiki beiträgst.
published: true
date: 2021-06-21T14:32:54.862Z
tags: 
editor: markdown
dateCreated: 2021-06-21T10:28:26.935Z
---

## Namensgebung der Dokumentation

Die Dokumentation folgt einer bestimmten Nomenklatur, die es dem Recalbox-Wiki ermöglicht, identische URLs für jede Übersetzung jeder Seite zu haben.

Nimm zum Beispiel diese Seite. Die französische Übersetzung hat diese URL:
`/fr/contribute`
Und die deutsche Seite hat diese:
`/de/contribute`

So kann die Übersetzung eines Abschnitts schnell gefunden werden, und die Autoren können leichter Übersetzungen anfertigen.
Aus diesem Grund erscheinen die URLs aller Dokumente auf Englisch.

## Formatierungsregeln

Die Dokumente sind in [markdown](https://docs.requarks.io/editors/markdown) geschrieben. Markdown ist ein sehr einfaches und zugängliches Werkzeug zur Formatierung von Dokumenten. Du kannst dieses Dokument gerne als Beispiel verwenden, um mit dem Schreiben deines eigenen Dokuments zu beginnen.

### Titel

Der Titel der Seite ist in den Seitenoptionen des Editors festzulegen. 
![title-metadata.png](/contribute/title-metadata.png)

Sie sollte die einzige Überschrift der Ebene 1 im Dokument sein.

Innerhalb des Inhalts der Seite sollten die Titel mit einer Überschrift der Ebene 2 beginnen (doppelte Raute: `##`):
![double-hash.png](/contribute/double-hash.png)

### Zitatblöcke

Um Leser auf wichtige Informationen oder Details aufmerksam zu machen, kannst du Zitatblöcke verwenden:

Dies ist der Markdown
```
> Dies ist eine Information!
> {.is-info}
```
für dieses Ergebnis: 
> Dies ist eine Information!
> {.is-info}

## Unterstützte Markdown-Version

Weitere Informationen zu den unterstützten Markdown-Funktionen findest du unter https://docs.requarks.io/editors/markdown.

## Bilder

Wenn Du Bilder zu Deinen Dokumenten hinzufügst, achte darauf, dass Du die Dokumenthierarchie in den Bildordnern replizierst und die Sprache weglässt.

Auf dieser Seite sollten die Bilder zum Beispiel im Verzeichnis `/contribute` erstellt werden.

## Lokalisierung

### Wie man eine Seite übersetzt

1. Melden dich mit deinem Discord-Account im Wiki an
2. Gehe zu der Seite, die du übersetzen willst, in der Standardsprache des Wikis (FR für jetzt)
3. Klicke auf das Sprachsymbol oben auf der Seite und wähle die Sprache, in die du die Seite übersetzen möchtest
![select-lang.png](/contribute/select-lang.png)
4. Ein Dialogfenster bietet dann die Möglichkeit, eine neue Seite zu erstellen. Klicke auf die Schaltfläche **Seite erstellen**
5. Klicke auf den Markdown-Editor
![markdown.png](/contribute/markdown.png)
6. Öffne eine neue Registerkarte auf der Originalseite und klicke auf **Bearbeiten** im Menü Seitenaktionen
![edit.png](/contribute/edit.png)
7. Rufe den Titel, die Tags und die Beschreibung der Seite auf, indem du auf die Schaltfläche PAGE klickst:
![editpage.png](/contribute/editpage.png)
8. Du kannst diese Elemente übersetzen und in Form deiner neu erstellten Seite hinzufügen
9. Nachdem du das Titel-Popup auf der Originalseite geschlossen hast, kopiere den Markdown-Inhalt der Seite und gehe auf https://www.deepl.com/translator, um deinen Text dort einzufügen und eine Übersetzung des kopierten Inhalts zu erhalten.
![deepl.png](/contribute/deepl.png)
10. Jetzt musst du nur noch die Übersetzung, die Bilder und die Links der neu erstellten Seite überprüfen!
11. Vergiss nicht, die letzte Zeile des Textes zu entfernen (die, die deepl automatisch an das Ende kopierter Texte setzt)
12. Vielen Dank für deinen Beitrag 🙏

### Relative Links

Außer in bestimmten Fällen muss ein Seitenlink relativ zur aktuellen Sprache sein. Zum Beispiel sollte auf dieser Seite ein Link auf die Seite `/de/home/` auf `./home` zeigen:

```Markdown
Besuche die [Startseite](./home)
```
Damit kannst du leichter neue Seiten für deine Sprache erstellen, so dass du die Sprache nicht selbst angeben musst.

### Neue Sprachen hinzufügen

Wenn deine Sprache nicht im Wiki-Menü erscheint und du eine Übersetzung starten möchtest, kontaktiere bitte das Team über unsere [sozialen Netzwerke, Discord oder das Forum](./presentation/useful-links).

## Schwierigkeitsgrad der Anleitungen

Um das Recalbox-Erlebnis unterhaltsam und zugänglich zu machen, gib beim Erstellen einer Anleitung deren Schwierigkeitsgrad an, der wie folgt bemessen ist:

1.  Jede Aktion, die keine Eingabe der erweiterten Einstellungen im allgemeinen Menü oder in den Recalbox .conf-Dateien erfordert, wird wie folgt gewertet: _level:star:_

1. Jede Aktion, die eine Konfiguration der erweiterten Einstellungen erfordert, um Änderungen vorzunehmen, die zu Instabilitäten führen könnten: _level:star::star:_

1.  Jede Aktion, die eine Änderung der Einträge in der Datei recalbox.conf erfordert: _level :star::star::star:_ (Vorsicht geboten).

1.  Jede Aktion, die SSH-Eingriffe und/oder Änderungen an der Boot-Datei erfordert (MobaXTerm oder Powershell - das Personal hat sich noch nicht auf die zu verwendende Software geeinigt): _level_ :star::star::star::star:

1. Jede Aktion, die einen Merge-Request auf GitLab notwendig macht: _level_ :star::star::star::star::star:

Beispiel:
- ROM/BIOS kopieren, Sprache ändern: :star:
- Theme ändern, Werkseinstellungen wiederherstellen, Standard-Core ändern: :star::star: 
- Aktiviere Retroarch-KI-Übersetzung: :star::star::star:

*Um das Stern-Emoji in deinen Anleitungen erscheinen zu lassen, setze ":" vor und hinter das Wort "star"*
```
:star:
``` 