---
title: Atomiswave
description: 
published: true
date: 2021-07-28T10:23:44.961Z
tags: 
editor: markdown
dateCreated: 2021-07-28T10:23:44.961Z
---

![](/emulators/arcade/atomiswave.svg){.align-center}

## Technische Daten

* **Hersteller**: Sammy Corporation
* **Jahr der Veröffentlichung**: 2003
* **CPU**: Hitachi SH-4 32-bit RISC CPU (@ 200 MHz 360 MIPS / 1.4 GFLOPS)
* **GPU**: NEC-Videologic PowerVR (PVR2DC/CLX2) @ 100 MHz
* **Sound**: ARM7 Yamaha AICA 45 MHZ (mit internem 32-bit RISC, 64 Kanäle ADPCM)
* **RAM**: 40 MB
* **Medien**: ROM Board (max. Größe von 168 MB)

## Beschreibung

Das Atomiswave ist ein maßgeschneidertes Arcade-System-Board und -Gehäuse der Sammy Corporation. Es basiert auf der Dreamcast-Konsole von Sega (es hat Ähnlichkeiten mit dem NAOMI, da es austauschbare Spiel-Module sowie ein austauschbares Modul zum Ändern des Kontrollschemas verwendet, und es ist üblich, das "Sega"-Logo auf dem Startbildschirm zu sehen. Es wird allgemein angenommen, dass das Atomiswave mehr VRAM und Audio-RAM als eine Dreamcast hat, aber das ist nicht wahr). Das Atomiswave verwendet austauschbare Spiel-Module und das Bedienfeld des Gehäuses kann leicht mit durch verschiedene Steuersätze ausgetauscht werden, einschließlich zwei Joysticks, zwei Lichtpistolen und einem Lenkrad.

Mit der Ausmusterung des alternden Neo Geo MVS-Systems wählte SNK Playmore das Atomiswave als nächstes System, für das Spiele entwickelt werden sollten. In einem Vertrag mit Sammy stimmte SNK Playmore zu, fünf Spiele für das Atomiswave-System zu entwickeln. Metal Slug 6 war das fünfte Spiel von SNK Playmore für das Atomiswave-System. Danach wechselte SNK zu einem Taito Type X2 Arcade-Board.

## Emulatoren

[Libretro Flycast](libretro-flycast)