---
title: Libretro Flycast
description: 
published: true
date: 2021-08-23T11:07:55.253Z
tags: libretro, atomiswave, flycast
editor: markdown
dateCreated: 2021-08-23T11:07:55.253Z
---

**Libretro Flycast** ist ein Multiplattform-Emulator für **Sega Dreamcast**, der das **Sammy Atomiswave** emulieren kann.

## ![](/emulators/license.svg) License

Diese Core steht unter der [**GPLv2**](https://github.com/libretro/flycast/blob/master/LICENSE) Lizenz.

## ![](/emulators/compatibility.png) Kompatibilität

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ❌ | ✅ | ✅ | ✅  🐌 | ✅ | ✅ |

🐌  Geringe Leistungen, aber spielbar

## ![](/emulators/features.png) Merkmale

| Merkmal | Unterstützt |
| :---: | :---: |
| Neustart | ✔ |
| Screenshots | ✔ |
| Saves | ✔ |
| Core Optionen | ✔ |
| RetroArch Cheats | ✔ |
| Controls | ✔ |
| Remapping | ✔ |
| Rumble | ✔ |
| Disk Control | ✔ |

## ![](/emulators/bios.svg) BIOS

### Liste der obligatorischen BIOS

| Dateiname | Beschreibung | MD5 | bereitgestellt |
| :---: | :---: | :---: | :---: |
| awbios.zip | Atomiswave BIOS von MAME | 0ec5ae5b5a5c4959fa8b43fcf8687f7c | ❌ |

### Speicherort

Lege das BIOS wie folgt ab:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 dc
┃ ┃ ┃ ┃ ┣ 🗒 **awbios.zip**

## ![](/emulators/roms.png) ROMs

Libretro Flycast basiert auf dem Romset von **MAME**, aber auch auf **NullDC**-Formaten für den Atomiswave-Teil.

### Unterstützte Dateierweiterungen

ROMs müssen folgende Erweiterungen haben:

* .zip
* .7z
* .bin/.lst
* .dat

## MAME ROMset

Nur Atomiswave-Roms **von einem RomSet MAME 0.135 oder höher** sind kompatibel.  
Wir empfehlen das **RomSet Mame 0.220**, das eine Menge zusätzlicher Kompatibilitäten mit sich bringt.  
Für weitere Informationen über die aktuelle RomSet Version, siehe die [MameDev](https://www.mamedev.org/release.html) Seite.

>Um deine Arcade-ROMs zu sortieren, findest du die **dat-Dateien** im Ordner: `/recalbox/share/bios/dc/atomiswave-0.230_noclones_v1.0_barhi.dat.zip`
{.is-info}

Du kannst sie auch herunterladen:

[atomiswave-0.230_noclones_v1.0_barhi.dat.zip](/emulators/arcade/atomiswave-0.230_noclones_v1.0_barhi.dat.zip)

### Speicherort

Lege deine ROMs wie folgt ab:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 atomiswave
┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**

## NullDC ROMset

Diese ROMs sind mit Flycast kompatibel, aber **weniger zuverlässig** als ROMs **aus einem MAME-Set**.

>Die **NullDC** Roms haben das Format : _`.bin + .lst`_.
{.is-info}

### Soeicherort

Lege deine ROMs wie folgt ab:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 atomiswave
┃ ┃ ┃ ┃ ┣ 📁 **game**
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **game.bin**
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **game.lst**

## ![](/emulators/advanced-configuration.png) Erweiterte Konfiguration des Emulators

>Um deine individuelle Konfiguration während eines Upgrades beizubehalten, raten wir dir, die Seite [Konfiguration überschreiben](./../../../advanced-usage/configuration-override) aufmerksam zu lesen.
{.is-info}

### Zugriff auf die Optionen

Du kannst verschiedene Optionen auf zwei verschiedene Arten konfigurieren.

* Über das RetroArch-Menü:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Über die Datei `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core Optionen

## ![](/emulators/external-links.png) Externe Links

* **Benutzter Quellcode**: [https://github.com/libretro/flycast](https://github.com/libretro/flycast)
* **Libretro Dokumentation**: [https://docs.libretro.com/library/flycast/](https://docs.libretro.com/library/flycast/)
* **Offizieller Quellcode**: [https://github.com/flyinghead/flycast](https://github.com/flyinghead/flycast)