---
title: Libretro Handy
description: 
published: true
date: 2021-07-12T09:39:12.134Z
tags: 
editor: markdown
dateCreated: 2021-07-12T09:11:35.989Z
---

**Libretro Handy** ist ein Atari Lynx Videospielsystem-Emulator, der als Core von libretro verwendet werden kann.  
Handy war der ursprüngliche Name des Lynx-Projekts, das bei Epyx begonnen und dann von Atari beendet wurde.

## ![](/emulators/license.svg) Lizenz

Diese Core steht unter der [**zlib**](https://sourceforge.net/projects/handy/) Lizenz.

## ![](/emulators/compatibility.png) Kompatibilität

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Eigenschaften

| Eigenschaft | Unterstützt |
| :---: | :---: |
| Neustart | ✔ |
| Screenshots | ✔ |
| Schnellspeichern | ✔ |
| Zurückspulen | ✔ |
| Netplay (zustandsabhängig) | ✔ (keine Link-Kabel-Emulation) |
| Core Optionen | ✔ |
| RetroAchievements | ✔ |
| Steuerung | ✔ |
| Remapping | ✔ |

## ![](/emulators/bios.svg) BIOS

### Liste der obligatorischen BIOS

| Dateiname | Beschreibung | MD5 | Bereitgestellt |
| :---: | :---: | :---: | :---: |
| lynxboot.img | Lynx Boot Image | fcd403db69f54290b51035d82f835e7b | ❌ |

### Speicherort BIOS

Lege das BIOS wie folgt ab:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 🗒 **lynxboot.img**

## ![](/emulators/roms.png) ROMs

### Unterstütze Erweiterungen

ROMs müssen folgende Dateierweiterungen haben:

* .lnx
* .o
* .zip
* .7z

Dieses System unterstützt komprimierte ROMs im .zip/.7z-Format. Sei jedoch vorsichtig, denn es handelt sich nur um Archive.

Die in der .zip/.7z enthaltenen Dateien müssen den oben genannten Erweiterungen entsprechen.
Außerdem darf jede .zip/.7z-Datei nur ein komprimiertes ROM enthalten.

### Speicherort ROMs

Lege deine Spiele wie folgt ab:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 lynx
┃ ┃ ┃ ┃ ┣ 🗒 **jeu.lnx**

>Die Roms im **No-Intro**-Format werden ausdrücklich empfohlen.
{.is-success}

>Weitere Informationen über Roms findest du in [dieser Anleitung](./../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Erweiterte Konfiguration des Emulators

>Um eigene Konfigurationen nach einem Upgrades beibehalten zu können, empfehlen wir die Benutzung der Funktion [Konfiguration überschreiben](./../../../advanced-usage/configuration-override).
{.is-info}

### Zugriff auf Optionen

Du kannst verschiedene Optionen auf zwei verschiedene Arten konfigurieren.

* Über das RetroArch-Menü:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Über die Datei `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core Optionen

## ![](/emulators/external-links.png) Externe Links

* **Verwendeter Quellcode**: [https://github.com/libretro/libretro-handy](https://github.com/libretro/libretro-handy)
* **Libretro Dokumentation**: [https://docs.libretro.com/library/handy/](https://docs.libretro.com/library/handy/)
* **Offizieller Quellcode**: [http://handy.sourceforge.net/download.htm](http://handy.sourceforge.net/download.htm)
* **Offizielle Webseite**:  [http://handy.sourceforge.net/](http://handy.sourceforge.net/)