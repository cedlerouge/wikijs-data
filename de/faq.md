---
title: FAQ
description: 
published: true
date: 2021-06-21T12:54:37.724Z
tags: 
editor: markdown
dateCreated: 2021-06-21T12:54:37.724Z
---

## Allgemein

### Auf was kann ich Recalbox installieren?

Du kannst Recalbox auf den folgenden Geräten installieren:

- Raspberry Pi 4
- Raspberry Pi 400
- Raspberry Pi Zero
- Raspberry Pi 1 (B/B+)
- Raspberry Pi 2
- Raspberry Pi 3 (B/B+)
- Odroid XU4
- Odroid Go Advance (Tragbare Konsole)
- Odroid Go Super (Laptop)
- PC x86 (32bit)
- PC x86_64 (64bit)

### Wie kann ich mitmachen?

- Du kannst eine Spende über Paypal tätigen
- Du kannst Bugs oder Verbesserungsvorschläge auf unserem [Gitlab](https://gitlab.com/recalbox/recalbox/issues) melden
- Du kannst im [Forum](http://forum.recalbox.com) aktiv teilnehmen
- Du kannst [zu diesem Wiki beitragen](./contribute)

### Wo kann ich den Fortschritt der Recalbox-Entwicklung sehen?

Auf dem [Recalbox GitLab](https://gitlab.com/recalbox). Du kannst zugreifen auf:  
- [Issues](https://gitlab.com/recalbox/recalbox/issues), die die aktuellen Aufgaben zeigen.  
- [Meilensteine](https://gitlab.com/recalbox/recalbox/-/milestones), wo angezeigt wird, welche Aufgaben noch zu entwickeln sind und welche bereits abgeschlossen wurden.

### Wie viele Spiele enthält die Recalbox?

Wir versuchen, so viele kostenlose Spiele wie möglich in die Veröffentlichung aufzunehmen, damit du das Recalbox-Erlebnis vom ersten Start an genießen kannst!
Besuche [diese Seite](https://forum.recalbox.com/topic/52/homebrews-sur-la-recalbox), um deine Homebrews einzureichen!

### Kann ich Recalbox in einem Kiosk/Bartop verwenden?

Ja, das Recalbox OS ist für die Integration in Kioske und Bartops entwickelt worden.
- Du kannst die Buttons und Joysticks direkt an die GPIOs des Raspberrys anschließen
- Du kannst Menüs deaktivieren, um jeglichen Fehlgebrauch zu vermeiden.
- Es ist auch möglich, die HDMI-/DVI-/VGA-Ausgänge (mit Adapter) oder RCA zu verwenden.
Weitere Informationen findest du auf [der entsprechenden Seite](/en/tutorials/system/installation/bartop-arcade-configuration).

### Recalbox unterstützt viele Systeme, aber ich sehe nur einige wenige in der Benutzeroberfläche, was soll ich tun?

Es werden nur die Systeme angezeigt, für die auch Spiele verfügbar sind. Versuche, deine ROMs über das Netzwerk deiner Recalbox hinzuzufügen und starte die Recalbox dann neu, damit die neuen Systeme angezeigt werden. Außerdem unterstützt manche Hardware nicht alle Konsolen.

### Muss ich meinen Raspberry Pi übertakten?

Beim Raspberry Pi 1 (einschließlich Zero) ist es unbedingt zu empfehlen, den Raspberry im EXTREM-Modus zu übertakten.

## EmulationStation

### Wie kann ich das Aussehen von EmulationStation ändern?

Drücke `Start` auf deinem COntroller → `UI EINSTELLUNGEN` → `THEME`

### Wie stelle ich die Hintergrundmusik aus?

Drücke `Start` auf deinem Controller → `SOUNDEINSTELLUNGEN` → `AUDIO-MODUS` → `AUS`

### Wo befinden sich die Konfigurationsdateien der EmulationStation im Dateisystem?

```
/recalbox/share/system/recalbox.conf
```

### Wo befinden sich die Themes von EmulationStation?

```
/recalbox/share/themes
```

## Emulation

### Warum gibt es mehrere Emulatoren für jedes System?

Die Emulation ist kein fehlerfreier Vorgang. Es gibt ein Gleichgewicht zwischen Leistung, Funktionalität und Wiedergabetreue.
Da Emulation nicht perfekt ist, ist es besser, mehrere Optionen zu haben. Manchmal ist ein schnellerer Emulator besser, kann aber unerwartete Probleme haben, während ein langsamerer vielleicht eine höhere Genauigkeit hat, aber einige spezifische Funktionen nicht unterstützt.

### Wie kann ich den Standard-Emulator für ein bestimmtes System ändern?

Drücke `START` auf dem Controller, gehe dann zu den erweiterten Einstellungen und schließlich zur erweiterten Emulator-Konfiguration. Dort kannst du das gewünschte System auswählen, den Emulator und die Kernel-Einstellungen ändern (beachte, dass sich die Grundeinstellung nicht ändert, wenn die Emulator-Einstellung *Default* lautet).

## System

### Recalbox unterstützt etwa 100 Konsolen, aber ich sehe nur ein paar davon, entweder in der EmulationStation-Oberfläche oder im Share-Ordner!

Je nach verwendeter Hardware gibt es Emulatoren, die damit kompatibel sind und solche, die es nicht sind.  
Bitte schaue dir die Kompatibilitätsliste für mögliche Hardware [hier](/de/hardware-compatibility/) an.

### Warum zeigen einige meiner Spiele einen schwarzen Bildschirm und kehren dann zur EmulationStation zurück, wie z.B. beim GBA?

> Einige Systeme benötigen ein BIOS, um zu funktionieren.

## Allgemeine Fehlersuche

### Meine Recalbox ist abgestürzt, soll ich sie vom Netz trennen und wieder einstecken?

Nein, in den meisten Fällen ist das System nicht komplett abgestürzt, sondern nur EmulationStation.

Es gibt mehrere Möglichkeiten, mit dieser Situation umzugehen. Die einfachste ist, die Debugging-Funktionen des Webmanagers zu nutzen, um EmulationStation neu zu starten oder Recalbox ordnungsgemäß herunterzufahren.

Andernfalls können die Daten beschädigt werden, insbesondere wenn Du einen Raspberry Pi mit einer SD-Karte verwendest..

## Fehlerbehebung bei der Konnektivität

### Windows 10 - Zugriff auf Recalbox über Netzwerkfreigabe nicht möglich

* Fall 1: Keine Computer, einschließlich Recalbox, sind von Windows aus sichtbar

Wenn Du keine Computer im Windows 10-Netzwerk sehen kannst, liegt das daran, dass die Netzwerkerkennung standardmäßig deaktiviert ist und die Netzwerkkonfiguration auf "öffentlich" oder "Diesen PC erkennbar machen" auf "deaktiviert" eingestellt ist.

Du musst in die Netzwerkeinstellungen gehen und dein Netzwerk auf "privat" setzen, oder "diesen PC erkennbar machen" auf "aktiviert" setzen.

* Fall 2: Recalbox ist seit dem Windows 10 Update Nr. 1709 nicht mehr verfügbar

Seit diesem Update wird Recalbox in deinem Netzwerk angezeigt, aber du kannst nicht mehr darauf zugreifen.

Eine offizielle Erklärung gibt es [hier](https://tech.nicolonsky.ch/windows-10-1709-cannot-access-smb2-share-guest-access/) (Englisch) oder [hier](https://support.microsoft.com/fr-fr/help/4046019/guest-access-smb2-disabled-by-default-in-windows-10-server-2016) (Englisch).

Schnelle Lösung: Herunterladen und Anwenden [dieses Patches](https://mega.nz/#!rUA3xDgI!a14w9TqQWinLriLANpk7BF_WkoNg8mw6fHloyPEZMPg)

Mit diesem Patch wird die Sicherheit für anonyme Netzwerkzugriffe in Windows deaktiviert, um den Zugriff auf Recalbox wiederherzustellen.

* Fall 3 (häufigster Fall)**

Gehe zu Alle Einstellungen → Anwendungen → Programme und Funktionen → Windows-Funktionen aktivieren oder deaktivieren und aktiviere: SMB 1.0/CIFS File-Sharing-Unterstützung / SMB1.0/CIFS-Client... danach starte Windows neu.