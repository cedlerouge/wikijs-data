---
title: Restaurer les paramètres d'usine
---



## Présentation

Depuis Recalbox 7.2.0, il y a une nouvelle option nommée Retour aux paramètres d'usine qui vous permet de remettre votre système à neuf, dans le même état après avoir fait une installation au propre tout neuve. Cette option vous permet de revenir à un état neuf avec plus de facilité que de refaire régulièrement des nouvelles installations.


>Cette option **ne touche pas** à vos bios, jeux et sauvegardes !
{.is-success}

## Utilisation de l'option

Cette option se trouve dans EmulationStation. Pour cela, il vous suffit d'appuyer sur `START`, de vous rendre dans `PARAMÈTRES AVANCÉS` et de sélectionner `RESTAURER LES PARAMÈTRES D'USINE`.

![](/migration-images/usage-basique/fonctionnalites/screenshot-2021-05-16t17-40-21-046z.png)

Une fois que vous aurez sélectionné l'option, vous aurez deux fenêtres de mise en garde.

![](/migration-images/usage-basique/fonctionnalites/screenshot-2021-05-16t17-40-26-275z.png)

![](/migration-images/usage-basique/fonctionnalites/screenshot-2021-05-16t17-40-30-458z.png)


>Une fois ces deux fenêtres validées, **vous n'aurez plus la possibilité de revenir en arrière**.
{.is-danger}

Quand la restauration aux paramètres d'usine sera terminé, vous retrouverez votre installation de Recalbox toute propre !

