---
title: Vidéos d'introduction
---



Dans Recalbox, vous avez la possibilité d'ajouter vos propres vidéos personnels pour être lues au démarrage.

Nous allons voir :

* Comment ajouter des vidéos
* Comment paramétrer leur diffusion

## Ajouter de nouvelles vidéos d'introduction

L'ajout de vidéo d'introduction se fait via le répertoire `share/bootvideos`.


>L'ajout de vidéos ne peut se faire que **via le réseau** !
{.is-danger}


>La totalité de vos vidéos ne doit pas dépasser 500 Mo.
{.is-success}

Pour vous connecter via le réseau à votre Recalbox, vous avez plusieurs possibilités :

* Via [WinSCP](/fr/tutorials/system/access/network-access-winscp)
* Via [Cyberduck](/fr/tutorials/system/access/sftp-ssh-cyberduck-macos-win)
* Via votre explorateur de fichiers. Pour cela, vous aurez besoin de [l'adresse IP](/fr/tutorials/network/ip/discover-recalbox-ip) de votre Recalbox et d'insérer l'adresse `\\votre-adresse-ip\` dans votre Explorateur de fichiers.

## Paramètres liés aux vidéos d'introduction

### Fréquence de lecture

Vous pouvez définir la fréquence auquel vous pouvez voir votre ou vos vidéo\(s\) d'introduction via le paramètre `system.splash.select` dans le fichier recalbox.conf.

Voici les options disponibles :

* **all** : le système choisit une vidéo fourni d'origine ou une de vos vidéos
* **custom** : le système choisit une de vos vidéos
* **recalbox** : le système choisit une vidéo fourni d'origine

### Temps de lecture

Vous pouvez définir le temps de lecture de toutes les vidéos \(fourni d'origine ou les vôtres\) via le paramètre `system.splash.length` dans le fichier recalbox.conf.

Voici les options disponibles :

* **0** : la lecture de la vidéo sera arrêtée quand EmulationStation sera prêt
* **-1** : la lecture de la vidéo ira jusqu'au bout avant le démarrage d'EmulationStation
* **Toute valeur supérieure à 0** : la lecture de la vidéo durera le nombre de secondes indiquée avant le démarrage d'EmulationStation

