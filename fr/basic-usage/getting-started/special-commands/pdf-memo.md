---
title: Mémo PDF
description: Télécharger le mémo des commandes spéciales en PDF
---



## Lien de téléchargement

{% file src="../../../.gitbook/assets/pad\_shortcuts\_guide\_a4\_fr.pdf" %}

## Aperçu

![](/migration-images/usage-basique/premieres-notions/commandes-speciales/pad-shortcuts-guide-a4-fr-_page_1_1.jpg)

![](/migration-images/usage-basique/premieres-notions/commandes-speciales/pad-shortcuts-guide-a4-fr-_page_2_1.jpg)

![](/migration-images/usage-basique/premieres-notions/commandes-speciales/pad-shortcuts-guide-a4-fr-_page_3_1.jpg)

![](/migration-images/usage-basique/premieres-notions/commandes-speciales/pad-shortcuts-guide-a4-fr-_page_4_1.jpg)



