---
title: Commandes spéciales
---



Pour vous faciliter vos tâches sur votre Recalbox, nous vous avons intégrer plusieurs commandes spéciales.



* [Dans le Menu](/fr/basic-usage/getting-started/special-commands/in-menu) 
* [Pendant le jeu](/fr/basic-usage/getting-started/special-commands/in-game)

