---
title: Configuration Recalbox pour Bartop / Borne Arcade
description: La borne d'arcade à la maison !
---



Si vous voulez utiliser un Raspberry Pi avec Recalbox pour créer un « **Bartop** » ou une **borne d'arcade**.  
Vous pouvez facilement configurer Recalbox pour répondre à votre besoin.

## A - GPIO <a id="a-gpio"></a>

Vous pouvez **configurer le pilote GPIO** de la Recalbox dans le fichier [recalbox.conf.](/fr/basic-usage/getting-started/recalbox.conf-file)  
Ce pilote **crée deux joysticks sur le système** et directement contrôlés par les **PINS GPIO** du Raspberry Pi.

Pour cela, vous n'avez **pas besoin d'un hub USB** car vous **connecterez vos boutons** directement sur les [GPIO](/fr/tutorials/controllers/gpio/gpio-controllers)

## B - Configuration vidéo <a id="b-configuration-video"></a>

Beaucoup de bartops utilisent un **vieil écran LCD** ou des **écrans de télévision CRT** utilisant une des connexions suivantes :

* **Ecran VGA** : vous aurez besoin d'un convertisseur HDMI / VGA \(convertisseur actif\) coûtant dans les 10€. Si vous êtes sur un écran 4/3 vous aurez besoin de changer le mode vidéo pour le passer à `global.videomode=default` pour tout vos émulateurs dans le fichier [recalbox.conf](/fr/basic-usage/getting-started/recalbox.conf-file).
* **Ecran DVI** : vous aurez besoin d'un adaptateur HDMI / DVI \(convertisseur passif\). Vous aurez probablement besoin de modifier votre fichier [recalbox.conf ](/fr/basic-usage/getting-started/recalbox.conf-file)en vous basant basant sur [ce tutoriel](/fr/tutorials/video/lcd/dvi-screen).
* **Ecran CRT** : Vous aurez besoin d'une prise jack vers RCA avec un câble vidéo. Vous devez changer le mode vidéo pour le positionner sur `global.videomode=default` pour tous vos jeux dans le fichier [recalbox.conf](/fr/basic-usage/getting-started/recalbox.conf-file) et modifier les paramètres décris dans [ce tutoriel](/fr/tutorials/video/crt/crt-screen-with-compostite).

## C - Configuration audio <a id="c-configuration-audio"></a>

Vous pouvez sélectionner le **périphérique de sortie audio** dans le fichier [recalbox.conf](/fr/basic-usage/getting-started/recalbox.conf-file) :

* Utilisez `audio.device=alsa_card.0:analog-output` si vous voulez forcer la **sortie audio sur la prise jack.** 
* Utilisez `audio.device=alsa_card.1:hdmi-output-0` si vous voulez forcer la **sortie audio sur le HDMI.**

## D - Configuration du menu

Vous pouvez désactiver Kodi dans le fichier [recalbox.conf](/fr/basic-usage/getting-started/recalbox.conf-file) _****_en positionnant le paramètre suivant `kodi.enable=0`dans le but de supprimer le raccourci affecté au bouton "X".

Vous pouvez aussi vouloir **changer les réglages du menu** :

* **Pour ES** :
  * En indiquant `system.es.menu=bartop`, vous aurez accès à un minimum d'options quand vous presserez le bouton `Start` sous EmulationStation.
  * En indiquant `system.es.menu=none`, aucun menu ne sera disponible. 
* **Pour les émulateurs** :
  * En indiquant `system.emulators.specialkey` sur `nomenu` , vous désactiverez les menus dans les émulateurs.
  * En indiquant `system.emulators.specialkey` sur `none` , vous ****désactiverez les menus et toutes les fonctions spéciales dans les émulateurs \(sauf la combinaison pour sortir d'un jeu\).

## En rapport :

[Activer le bouton GPIO pour fermer l'émulateur en appuyant sur un seul bouton](/fr/tutorials/controllers/gpio/enable-gpio-button-close-emulator)

