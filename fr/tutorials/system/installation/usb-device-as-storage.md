---
title: Utiliser un périphérique USB de stockage sur Recalbox
description: Pour mettre vos jeux et préservez votre carte SD
---



## Introduction

Vous pouvez très facilement utiliser un périphérique USB de stockage \(clé USB, disque dur externe auto alimenté etc.\) pour stocker vos roms, fichiers perso etc.

* Premièrement, vous devez utiliser un périphérique utilisant le système de fichiers exFAT. Soyez également certain que le système de fichier que vous choisirez soit bien compatible avec le système d'exploitation de votre PC. Recalbox ne formatera pas votre périphérique, il ne créera que de nouveaux fichiers dessus.

## Optionnel : Synchroniser votre clé ou disque USB avec votre carte SD

Recalbox peut synchroniser manuellement votre périphérique USB avec votre carte SD. Cette étape est optionnelle mais doit être réalisée avant de modifier vos paramètres de stockage USB. Voici les étapes:

1. Démarrez votre Recalbox.
2. Branchez votre périphérique USB quand vous voulez.
3. [ Connectez-vous en SSH](/fr/tutorials/system/access/root-access-terminal-cli) sur votre pi.
4. Tapez `cd /recalbox/scripts`
5. La commande `./recalbox-sync.sh list` vous donne la liste des périphériques disponibles. Par example :

```text
# ./recalbox-sync.sh list
INTERNAL
DEV A80A-27A5 NO_NAME
```

Donc le nom de ma clé USB est NO\_NAME, sa référence de périphérique est A80A-27A5. Je peux la synchroniser avec 6.

```text
# ./recalbox-sync.sh sync A80A-27A5
sending incremental file list
./
.keep
bios/
bios/lisez-moi.txt
bios/readme.txt

...

sent 111,817,693 bytes  received 8,256 bytes  9,723,995.57 bytes/sec
total size is 111,758,686  speedup is 1.00
rsync error: some files/attrs were not transferred (see previous errors) (code 23) at main.c(1178) [sender=3.1.2]
```

Vous y êtes! Ne vous inquiétez pas pour les erreurs. Maintenant votre périphérique USB contient la copie des données de votre carte SD. Vous pouvez continuer avec la prochaine étape.

## Configurer Recalbox pour utiliser un périphérique de stockage USB

* Branchez votre périphérique à votre Recalbox et allumez-la. Une fois dans l'interface de Recalbox, pressez le bouton `Start` de votre manette, allez dans `RÉGLAGES SYSTÈME` et dans `MEDIA DE STOCKAGE`. Maintenant, sélectionnez votre périphérique dans la liste, puis validez et attendez que le système redémarre. Durant cette phase de redémarrage, Recalbox va créer à la racine de votre périphérique, un nouveau répertoire nommé `recalbox` qui contiendra toute l'arborescence de votre partition `/share`.
* Pour ajouter vos fichiers \(roms, sauvegardes de jeux, bios, données de scrape etc...\) sur votre périphérique de stockage, vous avez deux possibilités :
  * Vous pouvez éteindre votre Recalbox et brancher votre périphérique à votre PC. Il ne vous reste plus qu'à copier vos fichiers personnels sur votre périphérique depuis votre PC. Une fois le transfert terminé, il ne vous reste plus qu'à rebrancher votre périphérique à votre Recalbox, l'allumer et jouer.
  * Vous pouvez aussi transférer par le réseau local en gardant votre Recalbox allumée. Vous aurez besoin de votre adresse IP. Vous pouvez le faire avec WinSCP ou [Cyberduck](/fr/tutorials/system/access/sftp-ssh-cyberduck-macos-win).

Avec cette méthode, le système \(sur la carte sd\) et la partition share \(sur le périphérique\) sont séparés. Donc si vous deviez réinstaller votre système, vous conserveriez toutes vos données utilisateur. Vous n'aurez alors qu'à rebrancher votre périphérique à votre recalbox, puis le sélectionner dans le système, et jouer.

## Que faire si après le redémarrage Recalbox ne voit toujours pas le disque dur ?

Parfois, après avoir sélectionné le périphérique dans Emulation Station, puis redémarré, Recalbox ne parvient pas à créer le système de fichiers sur celui-ci. Il continue alors d'utiliser les fichiers de la carte SD. Cela se produit notamment avec certains disques durs assez lents à s'initialiser.

Vous pouvez ce que vous pouvez faire:

1. [Connectez-vous en ssh ](/fr/tutorials/system/access/root-access-terminal-cli)
2. Tapez ces commandes:

```text
mount -o remount, rw /boot
cd /boot
nano recalbox-boot.conf
```

1. Rajoutez cette ligne: `sharewait=30`
2. Sauvegardez par `Ctrl X`, `Y`, `Entrée`
3. Tapez `reboot` et validez, Recalbox redémarre.

Si cela ne fonctionne toujours pas, augmentez la valeur de sharewait.

