---
title: Modifier le fichier config.txt
---




>**ATTENTION !**
>
>À partir de Recalbox 7.2.0, les modifications suivantes se font dans le fichier `/boot/recalbox-user-config.txt` !
{.is-danger}

Il est localisé dans le système à cette adresse : `/boot/recalbox-user-config.txt`

**Il y a deux façons de modifier le fichier recalbox-user-config.txt.**

## **SSH**

* [Se connecter avec ssh](/fr/tutorials/system/access/root-access-terminal-cli)
* Utiliser **nano** pour **éditer** le fichier **/boot/recalbox-user-config.txt**

  `nano /boot/recalbox-user-config.txt`

## **Noobs**

* **Branchez un clavier USB** et appuyez sur **Maj** durant le **boot de recalbox** pour accéder au _**menu de Recovery.**_
* **Appuyez sur** **"e"** pour obtenir le **menu édition**, et vous pourrez effectuer des modifications directement sur le fichier.  Vous pouvez **changer la langue** **du clavier** en a**ppuyant sur "l" et "k".**

