---
title: Licences de contribution
description: 
published: true
date: 2021-08-12T07:18:48.871Z
tags: contribution, licence
editor: markdown
dateCreated: 2021-05-21T08:34:08.069Z
---

Pour les projets de développement de logiciels et d'œuvres de création, le régime habituel de licences "entrantes=sortantes" s'applique, sauf indication contraire.

Comme expliqué clairement dans les [guides open-source](https://opensource.guide/fr/legal/) de GitHub, cela signifie qu'une licence open source sert implicitement de licence entrante (de la part des contributeurs) et sortante (vers d'autres contributeurs et utilisateurs).

Ce qui signifie qu'en contribuant à l'un de nos projets, vous acceptez les termes de sa licence et que vous soumettez votre contribution sous les mêmes termes.