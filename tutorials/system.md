---
title: 🎛️ Système
description: 
published: true
date: 2021-09-04T21:44:57.436Z
tags: tutoriel, système
editor: markdown
dateCreated: 2021-05-21T07:52:30.475Z
---

Vous y trouverez tout ce qui concerne la base, c'est-à-dire le système : comment y accéder ou comment modifier certaines options, par exemple.

Voici les catégories disponibles :

[Accès](access)
[Installation](installation)
[Modification](modification)
[Sauvegarde/Clonage](backups-and-clone)