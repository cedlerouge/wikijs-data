---
title: Charger ses roms depuis un partage réseau avec Samba (NAS)
description: 
published: true
date: 2021-09-04T22:11:35.236Z
tags: nas, partage
editor: markdown
dateCreated: 2021-05-21T08:39:17.336Z
---

Avec Recalbox, vous pouvez stocker vos données tels que vos roms, bios, saves, thèmes, etc. depuis un serveur de stockage réseau, aussi connu sous le terme NAS (Network Attached System).

Nous allons voir ci-dessous comment configurer tout le système du côté du serveur de stockage réseau et du côté de Recalbox, le tout avec le protocole Samba.

>Le NAS et votre Recalbox doivent obligatoirement être connectés en Ethernet pour une meilleure stabilité et un meilleur débit !
{.is-warning}

## Sur le serveur de stockage réseau

Ceci a été testé sur un Synology, mais le fonctionnement est identique.

Sur le serveur de stockage réseau :

* Créez un répertoire partagé (appelons le « /roms/ »).
* Créez un utilisateur avec les droits d’écriture/lecture sur ce répertoire (ou réutiliser un utilisateur existant avec ces droits).
* Recréez l'architecture du répertoire des roms de votre Recalbox (via le partage réseau, via FTP, ou à la main si vous voulez. L'important est d'avoir la même architecture, c'est à dire un répertoire par machine émulée).
  
Il est tout à fait possible de faire la même chose depuis le disque dur de votre PC. Depuis Windows :

* Vous pouvez créer un répertoire partagé.
* Le monter de la même manière sur votre Recalbox.

Le seul inconvénient, c'est que le PC servant de serveur de stockage réseau doit être allumé en permanence pour accéder aux roms depuis votre Recalbox.

## Dans votre Recalbox

* Montez la partition de démarrage en mode [lecture/écriture](./../../../tutorials/system/access/remount-partition-with-write-access).
* Modifiez le fichier `/boot/recalbox-boot.conf` avec votre éditeur préféré (nano, vi)
* Remplacez la ligne suivante :

```ini
sharedevice=INTERNAL
```

* Par la ligne suivante :

```ini
sharedevice=NETWORK
```

Ceci sert à indiquer à votre Recalbox que le contenu du dossier `/recalbox/share` doit être monté depuis un partage réseau (et non depuis la carte SD).

### Spécification des répertoires distants

Il est maintenant nécessaire de spécifier, toujours dans le fichier `recalbox-boot.conf`, les informations de connexion au serveur de stockage réseau. Pour cela, vous avez deux options :

### {.tabset}

#### La commande qui englobe tout en même temps

```ini
sharenetwork_<nfs|smb><[0-9]>=<SHARE|ROMS|SAVES|BIOS>@<NAS>:<répertoire partagé>:<options>
```

##### Exemple

```ini
sharenetwork_smb1=ROMS@192.168.0.1:recalbox/roms:username=recalbox,password=recalbox,vers=2.0
sharenetwork_smb2=SAVES@192.168.0.1:recalbox/saves:username=recalbox,password=recalbox,vers=2.0
```

La liste des dossiers pouvant être spécifiés se trouve en bas de cette page.

#### La commande pour chaque répertoire

```ini
sharenetwork_cmd<[0-9]>=<commande à exécuter>
```

##### Exemple

```ini
sharenetwork_cmd1=mount -o port=2049,nolock,proto=tcp 192.168.0.1:/Documents/recalbox /recalbox/share
```

La liste des dossiers pouvant être spécifiés se trouve en bas de cette page.

## Paramétrage avec SMB v2 ou supérieur

Avec SMB en version 2 ou supérieur, vous devez préciser la version du protocole à utiliser. Par exemple en version 2.0 :

```ini
sharenetwork_smb1=ROMS@192.168.0.1:recalbox/roms:username=recalbox,password=recalbox,vers=2.0
sharenetwork_smb2=SAVES@192.168.0.1:recalbox/saves:username=recalbox,password=recalbox,vers=2.0
```

### Versions de Windows minimum par version de Samba

* 1.0: Win 2k/XP/Server 2003/Server 2003 R2
* 2.0: Vista SP1/Server 2008
* 2.1: Win7/Server 2008 R2
* 3.0: Win8/Server 2012
* 3.02: Win8.1/Server 2012
* 3.11: Win10/Server 2016

### Quelques exemples

Voici les montages possibles :

### {.tabset}

#### La commande qui englobe tout en même temps

```ini
sharenetwork_<nfs|smb><[0-9]>=<SHARE|ROMS|SAVES|BIOS|MUSIC|OVERLAYS|SCREENSHOTS|SHADERS|SCRIPTS>@<NAS>:<répertoire partagé>:<options>
```

#### La commande pour chaque répertoire

```ini
sharedevice=NETWORK
sharewait=30
# TOUT LE DOSSIER SHARE EN ENTIER
sharenetwork_smb1=SHARE@<IP_DU_NAS>:<CHEMIN_NAS/SHARE>:username=<ICI_LOGIN>,password=<ICI_PASSWORD>,vers=3.0
```

## Paramétrer avec certains dossier(s) uniquement(s)

Comme vu au-dessus, il est possible de sélectionner uniquement le ou les dossier(s) parmi la liste ci-dessous :

* BIOS
* MUSIC
* OVERLAYS
* ROMS
* SAVES
* SCREENSHOTS
* SHADERS
* SCRIPTS

### Exemple

```ini
sharedevice=NETWORK
sharewait=30
#UNIQUEMENT UNE SELECTION DES DOSSIER
sharenetwork_smb1=ROMS@<IP_DU_NAS>:<CHEMIN_NAS/ROMS>:username=<ICI_LOGIN>,password=<ICI_PASSWORD>,vers=3.0
sharenetwork_smb2=SAVES@<IP_DU_NAS>:<CHEMIN_NAS/SAVES>:username=<ICI_LOGIN>,password=<ICI_PASSWORD>,vers=3.0
sharenetwork_smb3=BIOS@<IP_DU_NAS>:<CHEMIN_NAS/BIOS>:username=<ICI_LOGIN>,password=<ICI_PASSWORD>,vers=3.0
sharenetwork_smb4=MUSIC@<IP_DU_NAS>:<CHEMIN_NAS/MUSIC>:username=<ICI_LOGIN>,password=<ICI_PASSWORD>,vers=3.0
sharenetwork_smb5=OVERLAYS@<IP_DU_NAS>:<CHEMIN_NAS/OVERLAYS>:username=<ICI_LOGIN>,password=<ICI_PASSWORD>,vers=3.0
sharenetwork_smb6=SCREENSHOTS@<IP_DU_NAS>:<CHEMIN_NAS/SCREENSHOTS>:username=<ICI_LOGIN>,password=<ICI_PASSWORD>,vers=3.0
sharenetwork_smb7=SHADERS@<IP_DU_NAS>:<CHEMIN_NAS/SHADERS>:username=<ICI_LOGIN>,password=<ICI_PASSWORD>,vers=3.0
sharenetwork_smb8=SCRIPTS@<IP_DU_NAS>:<CHEMIN_NAS/SCRIPTS>:username=<ICI_LOGIN>,password=<ICI_PASSWORD>,vers=3.0
```

Si besoin, voici un autre exemple. Chaque point de montage peut pointer vers un système de stockage réseau spécifique.

>Pour les overlays, il faut choisir en fonction de la résolution de son écran.
{.is-info}

```ini
sharedevice=NETWORK
sharewait=30

#FULL ROMS
sharenetwork_smb1=ROMS@192.168.1.112:C/RCB/romsfull:username=USER,password=PASS,vers=3.0
#ROMS SELECTION
#sharenetwork_smb1=ROMS@192.168.1.112:C/RCB/roms:username=USER,password=PASS,vers=3.0
#ROMS POUR TESTS
#sharenetwork_smb1=ROMS@192.168.1.110:TEST/roms:username=USER,password=PASS+,vers=3.0

sharenetwork_smb2=SAVES@192.168.1.112:C/RCB/saves:username=USER,password=PASS,vers=3.0
sharenetwork_smb3=BIOS@192.168.1.112:C/RCB/bios:username=USER,password=PASS,vers=3.0
sharenetwork_smb4=SCREENSHOTS@192.168.1.112:C/RCB/screenshots:username=USER,password=PASS,vers=3.0

#720p pour Pi0 Pi2 Pi3
#sharenetwork_smb5=OVERLAYS@192.168.1.112:C/RCB/overlays720:username=USER,password=PASS,vers=3.0
#1080p pour Pi4 XU4 et PC en 1080p
sharenetwork_smb5=OVERLAYS@192.168.1.112:C/RCB/overlays1080:username=USER,password=PASS,vers=3.0
```

>Il est fortement recommandé de désactiver le support SMB v1 de votre machine Windows !
{.is-warning}

### Liens externes

* [http://www.zdnet.com/article/windows-10-tip-stop-using-the-horribly-insecure-smbv1-protocol/](http://www.zdnet.com/article/windows-10-tip-stop-using-the-horribly-insecure-smbv1-protocol/)
* [https://www.howtogeek.com/321072/how-to-disable-smbv1-and-protect-your-windows-pc-from-attack/](https://www.howtogeek.com/321072/how-to-disable-smbv1-and-protect-your-windows-pc-from-attack/)
* [https://www.spamtitan.com/blog/stop-smbv1-ransomware-attacks/](https://www.titanhq.fr/blog/comment-arreter-attaques-ransomware-smbv1/)
* [https://www.bostonhelpdesk.com/disabling-smbv1-one-defense-against-wanna-cry-ransomware/](https://www.bostonhelpdesk.com/disabling-smbv1-one-defense-against-wanna-cry-ransomware/)