---
title: Changer l'adresse MAC du Bluetooth dans Raspberry Pi
description: 
published: true
date: 2021-09-04T22:10:26.599Z
tags: bluetooth, mac, adresse
editor: markdown
dateCreated: 2021-05-21T08:38:43.585Z
---

## Pourquoi ?

Dans certains cas, des Raspberry Pi peuvent mal fonctionner et auquel aucun des ports USB ne fonctionne, ce qui fait qu'il n'est pas possible d'appairer des manettes sans fil. En modifiant l'adresse MAC du Bluetooth, on peut synchroniser des manettes avec d'autres Raspberry Pi en dupliquant l'adresse MAC.

## Installer bdaddr

Connectez-vous via [SSH](./../../../tutorials/system/access/root-access-terminal-cli) et exécutez ces lignes de commandes :

```shell
wget https://www.tbit.com.br/files/static/bdaddr.gz
gunzip bdaddr.gz
mv bdaddr /bin
chmod +x /bin/bdaddr
```

## Modifier votre adresse MAC

Maintenant, vous pouvez modifier l'adresse MAC du Bluetooth avec ces commandes :

```shell
bdaddr -i hci0 -r B8:27:EB:00:00:00
hciconfig hci0 reset
```

## Appliquer la modification à chaque démarrage

Modifiez ou créez le fichier de démarrage à l'adresse `/recalbox/share/userscripts/custom.sh` :

```bash
#!/bin/sh

sleep 2
/bin/bdaddr -i hci0 -r B8:27:EB:00:00:00
hciconfig hci0 reset
```