---
title: Partage
description: 
published: true
date: 2021-09-04T22:11:11.746Z
tags: share
editor: markdown
dateCreated: 2021-05-21T08:08:36.270Z
---

Ici, vous pouvez trouver de l'aide pour tout ce qui est partage sur votre réseau domestique.

Voici les tutoriels disponibles :

[Charger ses roms depuis un partage réseau avec Samba (NAS)](roms-on-a-network-share-samba-nas)
[Impossible d'accéder à Recalbox depuis le partage réseau](access-network-share)