---
title: Wifi
description: 
published: true
date: 2021-09-04T22:13:15.535Z
tags: wifi
editor: markdown
dateCreated: 2021-05-21T08:08:47.880Z
---

Si vous souhaitez activer le wifi sur votre Recalbox ou que vous ayez un soucis de connexion, vous devriez trouver votre bonheur ici.

Voici les tutoriels disponibles :

[Activer le wifi](enable-wifi)
[Indicatif wifi des pays](wifi-country-code)