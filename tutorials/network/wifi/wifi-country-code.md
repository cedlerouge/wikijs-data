---
title: Indicatif wifi des pays
description: 
published: true
date: 2021-09-04T22:14:15.173Z
tags: wifi, indicatif, pays
editor: markdown
dateCreated: 2021-05-21T08:39:37.102Z
---

Vous pouvez changer l'indicatif wifi du pays dans le fichier `recalbox.conf` pour activer plus de canaux wifi concernant votre pays.

## Canaux désactivés par pays

Certaines canaux sont désactivés dans certains pays :

| Canal | Fréquence (Mhz) | Amérique du nord | Japon | Majorité du monde |
| :---: | :---: | :---: | :---: | :---: |
| 1 | 2412 | ✅ | ✅ | ✅ |
| 2 | 2417 | ✅ | ✅ | ✅ |
| 3 | 2422 | ✅ | ✅ | ✅ |
| 4 | 2427 | ✅ | ✅ | ✅ |
| 5 | 2432 | ✅ | ✅ | ✅ |
| 6 | 2437 | ✅ | ✅ | ✅ |
| 7 | 2442 | ✅ | ✅ | ✅ |
| 8 | 2447 | ✅ | ✅ | ✅ |
| 9 | 2452 | ✅ | ✅ | ✅ |
| 10 | 2457 | ✅ | ✅ | ✅ |
| 11 | 2462 | ✅ | ✅ | ✅ |
| 12 | 2467 | ❌ | ✅ | ✅ |
| 13 | 2472 | ❌ | ✅ | ✅ |
| 14 | 2484 | ❌ | ✅  (11b uniquement) | ❌ |

[Source](https://en.wikipedia.org/wiki/List_of_WLAN_channels)

## Paramètrage

Ce paramètre peut se modifier dans le fichier `recalbox.conf` :

* Ouvrez le fichier [recalbox.conf](./../../../basic-usage/getting-started/recalboxconf-file).
* Trouvez les lignes suivantes :

```ini
## Set Wifi region
wifi.region=JP
```

Modifiez la valeur `JP` dansl'exemple par celle de votre choix.

>La valeur par défaut est `JP` car ce pays est le plus permissif en ce qui concerne le wifi.
{.is-info}

## Liste des indicatifs wifi

Voici une liste d'indicatifs wifi qui pourrait vous être utile :

| Pays | Code |
| :--- | :--- |
| Autriche | AT |
| Australie | AU |
| Belgique | BE |
| Bresil | BR |
| Canada | CA |
| Suisse et Liechtenstein | CH |
| Chine | CN |
| Chypre | CY |
| Republique tchéque | CZ |
| Allemagne | DE |
| Danemark | DK |
| Estonie | EE |
| Espagne | ES |
| Finlande | FI |
| France | FR |
| Royaume-Uni | GB |
| Grece | GR |
| Hong Kong | HK |
| Hongrie | HU |
| Indonesie | ID |
| Irlande | IE |
| Israël | IL |
| Inde | IN |
| Islande | IS |
| Italie | IT |
| Japon | JP |
| République de Corée | KR |
| Lituanie | LT |
| Luxembourg | LU |
| Lettonie | LV |
| Malaisie | MY |
| Pays-Bas | NL |
| Norvège | NO |
| Nouvelle Zélande | NZ |
| Philippines | PH |
| Pologne | PL |
| Portugal | PT |
| Suède | SE |
| Singapour | SG |
| Slovenie | SI |
| Slovaquie | SK |
| Thailande | TH |
| Taiwan | TW |
| Etats-Unis | US |
| Afrique du Sud | ZA |

## Autres indicatifs

Vous trouverez plus d'indicatifs de pays [sur cette page](http://www.arubanetworks.com/techdocs/InstantWenger_Mobile/Advanced/Content/Instant%20User%20Guide%20-%20volumes/Country_Codes_List.htm#regulatory_domain_3737302751_1017918).