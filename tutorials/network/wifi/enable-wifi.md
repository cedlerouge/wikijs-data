---
title: Activer le wifi
description: Comment se connecter en wifi ?
published: true
date: 2021-09-04T22:13:36.571Z
tags: wifi, activation
editor: markdown
dateCreated: 2021-05-21T08:39:42.871Z
---

## Pré-requis

* Vérifiez que votre dongle wifi fait parti de la [liste de compatibilité](./../../../hardware-compatibility/compatible-devices/wifi-dongles) ou que vous possédez une board qui à le wifi.
* Vérifiez que votre clé wifi soit en WPA2, ne contienne pas de caractères spéciaux de préférence.
* Vérifiez que votre wifi est en mode découverte.
* Si votre point d'accès utilise le canal 12 ou 13, tentez de changer l'[indicatif wifi du pays ](./wifi-country-code)

## Configuration

La configuration se fait via EmulationStation très simplement.

* Appuyez sur `Start` > `OPTIONS RÉSEAU` et mettez l'option `WIFI` sur **On** 
* Vous avez 2 moyens de configurer le wifi :
  * Vous pouvez renseigner direcetment le SSID et le mot de passe de votre connexion wifi.
  * Vous pouvez aussi passer par la connexion WPS. Pour cela :
    * Sélectionnez `CONNEXION WPS` dans EmulationStation
    * Sur votre modem, vous  devez activer la fonction WPS. Celle-ci restera automatiquement active pendant 2 minutes.