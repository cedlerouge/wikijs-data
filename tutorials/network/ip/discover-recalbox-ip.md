---
title: Connaitre l'adresse IP sur son réseau
description: 
published: true
date: 2021-09-04T22:08:26.177Z
tags: ip, connaitre, adresse
editor: markdown
dateCreated: 2021-05-21T08:38:50.833Z
---

## Pré-requis

Assurez-vous que votre Recalbox est bien allumée et connectée au réseau via un câble Ethernet ou un dongle Wifi.

Vous avez un doute et votre réseau à un accès Internet ? Rien de plus simple, rendez-vous dans le menu de Recalbox puis sur `METTRE A JOUR LE SYSTEME`. Si un pop-up s'affiche avec le message `Une mise à jour est disponible` ou `Aucune mise à jour de disponible`, alors votre Recalbox a bien un accès à Internet et donc, au réseau. Si le message `Veuillez brancher un cable réseau` s'affiche, soit vous n'avez pas Internet sur le réseau ce qui n'est pas problématique, soit vous avez un soucis de configuration sur votre routeur, un cable défectueux ou un dongle wifi qui ne fonctionne pas.

>L'adresse IP n'est pas fixe et est suceptible de changer à chaque redemarrage.   
>Vous pouvez en définir une vous même via votre modem / routeur ou via [ce tutoriel](./../../../tutorials/network/ip/static-manual-ip).
{.is-warning}

## Depuis les paramètres du réseau Recalbox

Appuyez sur `START` et allez dans `OPTIONS RÉSEAU`. L'adresse IP actuelle est affichée à cet endroit.

## Depuis votre ordinateur

## {.tabset}

### Windows

* Dans la barre des tâches, vous avez une zone de texte blanche. Entrez-y la commande `cmd` et appuyez sur `Entrée`.
* Une invite dse commandes doit être ouverte. Tapez `ping recalbox` et appuyer sur `Entrée`.
* Vous devriez avoir le message suivant qui s'affiche :

```dos
> Envoi d'une requête 'ping' sur RECALBOX avec 32 octets de données :  
> Réponse de 192.168.0.XX : octets=32 temps=1 ms TTL=128  
> Réponse de 192.168.0.XX : octets=32 temps=1 ms TTL=128  
> Réponse de 192.168.0.XX : octets=32 temps=1 ms TTL=128  
> Statistiques Ping pour [...]
```

* Vous avez votre adresse IP.

### macOS

* Allez dans `Applications/Utilitaires/Terminal`.
* Une fenêtre du terminal doit être visible. Tapez `arp -a` et appuyez sur `Entrée`.
* La liste de tous les appareils connectés au réseau, sous forme de liste d'adresses IP, doit être visible.
* L’adresse IP de votre Recalbox est souvent du type `192.168.0.XX`.
* Vous avez votre adresse IP.

## Autres solutions

* Des applications smartphones sont disponibles pour scanner votre réseau. Rendez-vous sur le store de votre appareil pour trouver l'application de votre choix. 
* Une autre solution consiste à se rendre dans les paramètres du routeur de votre box Internet. La liste des périphériques connectés doit référencer votre Recalbox.