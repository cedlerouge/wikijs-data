---
title: Définir une adresse IP manuellement
description: 
published: true
date: 2021-09-04T22:09:01.172Z
tags: ip, adresse, manuelle
editor: markdown
dateCreated: 2021-05-21T08:38:57.717Z
---

Avec Recalbox, il est possible de modifier l'adresse IP manuellement dans le fichier [recalbox.conf](./../../../basic-usage/getting-started/recalboxconf-file).

Ouvrez le fichier `recalbox.conf` et trouvez ces lignes :

```ini
## Wifi - static IP
## if you want a static IP address, you must set all 3 values (ip, gateway, and netmask)
## if any value is missing or all lines are commented out, it will fall back to the
## default of DHCP
;wifi.ip=manual ip address
;wifi.gateway=new gateway
;wifi.netmask=new netmask
```

Vous devez supprimer le `;` devant les trois dernières lignes, ou la configuration ne fonctionnera pas, et remplissez-les avec les informations souhaitées, par exemple :

```ini
wifi.ip=192.168.1.10
wifi.gateway=192.168.1.254
wifi.netmask=255.255.255.0
```