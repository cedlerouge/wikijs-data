---
title: Ajouter des thèmes à EmulationStation
description: 
published: true
date: 2021-09-04T18:06:42.225Z
tags: emulationstation, thèmes
editor: markdown
dateCreated: 2021-05-21T08:07:10.543Z
---

Envie de tester un nouveau style pour votre EmulationStation ? Vous êtes au bon endroit.

N’hésitez pas à cliquer sur les liens pour plus d’information et à laisser un commentaire sur les posts respectifs aux différents auteurs.

Encore un grand merci à eux pour leur travail et leur partage.

---

## Ajouter un thème à EmulationStation

Vous pouvez télécharger des thèmes créés par la communauté dans le [forum](https://forum.recalbox.com/category/14/themes-interface), assurez-vous simplement d'utiliser un thème compatible avec votre version de Recalbox (donc un thème à jour avec une version à jour).

Vous pouvez ajouter manuellement le thème de votre choix en suivant ces étapes :

* Décompressez l'archive téléchargée
* Copiez le dossier contenant le thème que vous souhaitez utiliser sur votre EmulationStation de 2 façons différentes :
  * Soit depuis le réseau :`\\recalbox\share\themes\`  
  * Soit sur la carte SD ou sur votre stockage externe le cas échéant :`SHARE/themes/`

> Attention, la totalité des dossiers de votre thème doit être dans un unique dossier et non à la racine du dossier `themes`.
Cela donne donc une arborescence qui ressemble à la suivante :
📁 recalbox
┣ 📁 share
┃ ┣ 📁 themes
┃ ┃ ┣ 📁 Hyperwall 720p
{.is-warning}

Vous pouvez maintenant changer le thème dans les options d'EmulationStation et redémarrer votre Recalbox.

Un tutoriel vidéo est disponible sur YouTube [ici](https://youtu.be/U185lY2tKTg).

## Compatibilité

| |GPICase|PI / OXu4|PC|OGoA|OGoS|16/9|4/3|
|-|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
| Darkswitch  |⚠️|✅|✅|⚠️|⚠️|✅|⚠️|
|  Dreambox   |⚠️|✅|✅|⚠️|⚠️|✅|⚠️|
|  Hyperwall  ||✅|✅|⚠️|⚠️|✅|⚠️|
|   Mukashi   |❌|✅|✅|⚠️|⚠️|✅|⚠️|
| Next Pixel  |✅|✅|✅|⚠️|⚠️|✅|✅|
| Unifiedspin |❌|✅|✅|⚠️|⚠️|✅|⚠️|

Légende :

✅ Optimisé : fonctionne parfaitement, et a été optimisé pour la plateforme en question
⚠️ Compatible : fonctionne mais n'est pas optimisé, certains éléments ne sont pas mis en page tel qui devraient ou les tailles de police ne sont pas optimales mais ces problèmes restent mineurs et n’empêchent pas le bon fonctionnement du thème
❌ Non compatible : ne fonctionne pas ou la mise en page du thème n'est pas du tout adapté (éléments ou textes qui se chevauchent, textes illisibles, images déformées...)


## Les thèmes


### Darkswitch de Dwayne Hurst adapté par Butch Games

- Présence de tous les systèmes virtuels ou non
- Résolution disponible : 480p
- Option : aucune

> Activer trop de systèmes virtuels (+/- 130) fera bugger le thème
{.is-info}

![dark1.png](/tutorials/frontend-customization/add-themes-into-emulationstation/dark1.png)
![dark2.png](/tutorials/frontend-customization/add-themes-into-emulationstation/dark2.png)
Présentation détaillée en cliquant [ici](https://forum.recalbox.com/topic/24439)

### Dreambox de Butch Games

- Présence de tous les systèmes virtuels ou non
- Résolution disponible : 480p
- Option : aucune

![dream1.png](/tutorials/frontend-customization/add-themes-into-emulationstation/dream1.png)
![dream2.png](/tutorials/frontend-customization/add-themes-into-emulationstation/dream2.png)
Présentation détaillée en cliquant [ici](https://forum.recalbox.com/topic/23272)

### Hyperwall de Butch Games

- Présence de tous les systèmes virtuels ou non
- Résolution disponible : 720p
- Option : 4 vues système différentes

![hyperwall1.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/hyperwall1.jpg)
![hyperwall2.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/hyperwall2.jpg)

Présentation détaillée en cliquant [ici](https://forum.recalbox.com/topic/24597)

### Mukashi de mrseguin et airdream

- Manque certains systèmes virtuels ou non mais utilisable
- Résolution disponible : 720p - 1080p (attention très lourd actuellement au chargement)
- Option : aucune

![muka1.png](/tutorials/frontend-customization/add-themes-into-emulationstation/muka1.png)
![muka2.png](/tutorials/frontend-customization/add-themes-into-emulationstation/muka2.png)
Présentation détaillée en cliquant [ici](https://forum.recalbox.com/topic/14323)

### Next Pixel de mYSt

- Manque certains systèmes virtuels ou non mais utilisable
- Résolution disponible : 480p - 720p - 1080p
-	Pour OGoA / OGoS prendre 16/9 480p
-	Pour les Pi et certains PC ne pas dépasser 720p
- Option : très nombreuses
-	Le thème utilise aussi des mix/scrap spécifiquement désignés pour Next Pixel (cf. documentation du thème)

![next1.png](/tutorials/frontend-customization/add-themes-into-emulationstation/next1.png)
![next2.png](/tutorials/frontend-customization/add-themes-into-emulationstation/next2.png)
Présentation détaillée en cliquant [ici](https://forum.recalbox.com/topic/16064)

### Unifiedspin de Butch Games inspiré des thèmes Unified

- Présence de tous les systèmes virtuels ou non
- Résolution disponible : 720p
- Option : aucune
> Activer trop de systèmes virtuels (+/- 130) fera bugger le thème
{.is-info}

![unif1.png](/tutorials/frontend-customization/add-themes-into-emulationstation/unif1.png)
![unif2.png](/tutorials/frontend-customization/add-themes-into-emulationstation/unif2.png)
Présentation détaillée en cliquant [ici](https://forum.recalbox.com/topic/24433)