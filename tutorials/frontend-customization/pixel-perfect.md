---
title: Ajuster l'image (Pixel Perfect)
description: 
published: true
date: 2021-09-04T18:07:07.422Z
tags: pixel, perfect, image, ajuster
editor: markdown
dateCreated: 2021-05-21T08:07:42.063Z
---

## Objectif

Activer le mode d'ajustement à l'échelle de l'image « Pixel Perfect » qui consiste à obtenir le rendu exact des jeux sur un écran moderne.

## Étapes

* Allez dans le menu principal d'EmulationStation en appuyant sur le bouton `START`.
* Allez dans la section `OPTIONS DES JEUX`
* Au niveau de l'option `SHADERS PRÉDÉFINIS`, sélectionnez `SCANLINES` ou `RETRO`.
* Activez l'option `AJUSTER L'ÉCHELLE (PIXEL PERFECT)`.

>Dans ce mode, l'image peut ne plus être en plein écran !
{.is-warning}