---
title: Utilisation d'EmulationStation au clavier / à la souris
description: Si votre seule manette tombe en panne...
published: true
date: 2021-09-04T18:01:44.702Z
tags: emulationstation, clavier, souris
editor: markdown
dateCreated: 2021-05-21T08:09:18.247Z
---

Au cas où vous aurez votre seule manette qui tomberait en passe en pleine utilisation d'EmulationStation, vous pouvez naviguer avec un clavier et souris.

## Commandes de la souris

Voici ce que vous pouvez faire à la souris :

* A : clic droit
* B : clic gauche

## Commandes du clavier

Voici ce que vous pouvez faire au clavier :

* A = S
* B = A
* X = P
* Y = L
* Start = Entrée
* Select = Espace
* L1 = Page Up (⇞)
* R1 = Page Down (⇟)
* Hotkey = Échap

