---
title: Installation du boitier NesPi4Case
description: 
published: true
date: 2021-09-04T18:02:28.038Z
tags: installation, nespi4case
editor: markdown
dateCreated: 2021-06-08T10:08:26.695Z
---

## Introduction

Sur le boitier NesPi4Case, il s’avère que le port SATA du disque dur pose plusieurs problèmes selon la configuration utilisée. Pour faire simple, si le port SATA (intégré dans la partie supérieure du boitier) est connecté via le cable USB3 à la partie inférieure mais qu’aucun disque (disque-dur mécanique ou SSD) n’est branché dessus, alors Recalbox rencontre plusieurs problèmes :

* Problème d’installation lors d’une fresh install ;
* Problème de latence au démarrage (pouvant aller à plusieurs minutes) ;
* Problème de reconnaissance du boitier ;
* Problème d’extinction de l’alimentation et du ventilateur du Nespi4Case lorsqu’on éteint par le bouton power directement.

Pour résoudre ces problèmes et faire fonctionner le boitier correctement en attendant un correctif des développeurs, nous vous proposons ce petit tutoriel afin de profiter pleinement de votre boitier ! Nous vous précisons que c'est une mesure provisoire afin de permettre l'utilisation du boitier en attendant un fix stable.

Nous allons donc distinguer deux cas :

* L’utilisation avec un disque-dur intégré dans la cartouche et connecté au boitier ;
* L’utilisation sans disque-dur.

>Vérifiez bien que les câbles qui doivent être branchés suivent exactement le schéma de montage préconisé par RETROFLAG. Pas d’inversion de câbles USB2/USB3.
{.is-warning}

Dans tous les cas, on part du principe que le Pi4 est installé dans le boitier.

## Mise en place

### Utilisation du Nespi4Case avec un disque dur

C’est le cas le plus simple.

#### Installation
Dans un premier temps, vous formattez votre disque dur au format exFAT. Flashez votre carte µSD avec votre logiciel préféré.

Ensuite, assurez-vous que le bouton safeshutdown du boitier est bien sur ON (voir la notice du boitier) pour l’utilisation du mode safeshutdown.

Dans un second temps, sur le boitier, vous insérez votre carte µSD dans son logement et vous connectez le disque dur au port SATA. Le disque dur _**DOIT**_ être connecté.

![](/tutorials/others/nespi4case-installation/nespi4case1.jpg)

* Allumez votre boitier. L’installation va se lancer rapidement.
* Quand le démarrage a terminé, l’installation a dû se faire proprement, avec reconnaissance automatique du boitier, du safeshutdown et des boutons.
* En appuyant directement sur le bouton power du boîtier, le système va s’éteindre proprement, l’alimentation va se couper et le ventilateur s’arrêter.

Vous pouvez ensuite procéder à l’installation sur votre disque dur, de façon tout à fait classique :

* Allumez voutre boitier de nouveau jusqu'à son chargement complet.
* Avec votre manette, appuyez sur le bouton `START` et allez dans `RÉGLAGES SYSTÈME` > `MÉDIA DE STOCKAGE` et choisissez le disque dur. L’installation sur le disque dur reconnaît également le boitier, le paramètrage est automatique.

Il n’y a absolument aucune autre manipulation à faire, aucun script à installer à côté.

#### Usage

Utilisez votre Recalbox de façon tout à fait classique. Le bouton `Power` éteint proprement, comme indiqué ci-dessus. `Reset` fait un redémarrage propre.

###  Utilisation du Nespi4Case sans disque dur (pour un branchement en réseau par exemple)

Dans ce cas, si le cable USB relié au SATA est branché, les symptômes détaillés en début de ce tutoriel peuvent se produire.

#### Installation

Dans un premier temps, flashez votre carte µSD avec votre logiciel préféré.

Ensuite, assurez-vous que le bouton safeshutdown du boitier est bien sur ON (voir la notice du boitier) pour l’utilisation du mode safeshutdown. Et veillez à bien déconnecter le cable USB3 de la partie supérieure du boitier (relié au port SATA) du port USB3 du Pi4. La partie supérieure du boitier ne doit pas être reliée de la partie inférieure !

Dans un second temps, vous insérez votre carte µSD dans le logement prévu sur le boîtier.

![](/tutorials/others/nespi4case-installation/nespi4case2.jpg)

* Vous allumez. L’installation va se lancer rapidement.
* Quand le démarrage a terminé, l’installation a dû se faire proprement, mais il reste à procéder à la reconnaissance du boitier.

#### Vérification de l'installation

Il reste une petite manipulation à faire.
* Connectez votre Recalbox à Internet, connectez-vous via SSH et autorisez l’écriture sur votre [partition de démarrage](./../../../tutorials/system/access/remount-partition-with-write-access).
* A l’aide de votre logiciel préféré ([WinSCP](./../../../tutorials/system/access/network-access-winscp) / [Cyberduck](./../../../tutorials/system/access/network-access-cyberduck) / [MobaXTerm](./../../../tutorials/system/access/network-access-mobaxterm)), cherchez le fichier `/boot/recalbox-boot.conf` et ouvrez-le.
* Trouvez la ligne suivante et effacez-là :

```ini
case=none:1
```

* Sauvegardez et éteignez le système proprement.
* Connectez le cable USB3 de la partie supérieure du boitier sur le port USB3 disponible du Pi4 et allumez votre Recalbox.

Là, c’est un peu aléatoire. Il n'y a pas d'explication logique mais parfois le boitier est bien reconnu, parfois non. Pour le vérifier, quand tout le système est chargé :

* Connectez-vous via [SSH](./../../../tutorials/system/access/root-access-terminal-cli) et allez lire le fichier `/boot/recalbox-boot.conf`.

![](/tutorials/others/nespi4case-installation/nespi4case3.jpeg)

Si vous trouvez cette ligne :

```ini
case=none:1
```

C’est que le boitier n’est pas reconnu. Dans ce cas, reprenez la vérification de l'installation.

![](/tutorials/others/nespi4case-installation/nespi4case4.jpeg)

Si vous trouvez cette ligne :

```ini
case=NESPi4:1
```

C’est que le boitier a été détecté. À cette étape, vous pouvez éteindre et déconnecter le cable USB3 du boitier du Pi4 (puisqu’aucun disque dur n’est utilisé, cela évitera les problèmes décrits plus haut) tout en gardant active les fonctionnalités du boitier puisqu’il a été détecté.

![](/tutorials/others/nespi4case-installation/nespi4case5.jpeg)

>Si vous avez un disque dur disponible mais que vous ne souhaitez pas vous en servir pour ce boitier, il est conseillé de procéder de faire l'installation avec le disque dur, sans faire les étapes pour définir le disque dur comme média de stockage. Et quand le boitier est reconnu, de retirer le cable UB3 du Pi4 de la même façon.
{.is-info}

## Crédits

Ce tutoriel a sa source sur [cette discussion](https://forum.recalbox.com/topic/24390/recalbox-v7-2-1-tutoriel-d-installation-du-boitier-nespi4case) du forum. Vous pouvez y participer si vous avez toujours un problème malgré avoir bien suivi ce tutoriel.