---
title: Installation
description: 
published: true
date: 2021-09-04T21:53:33.053Z
tags: installation
editor: markdown
dateCreated: 2021-05-21T08:10:07.550Z
---

Cette partie vous concerne si vous souhaitez faire une installation dans un bartop ou pour utiliser un périphérique USB comme lieu de stockage, par exemple.

Voici les tutoriels disponibles :

[Configuration de Recalbox pour Bartop / Borne Arcade](bartop-arcade-configuration)
[Utiliser un périphérique USB de stockage sur Recalbox](usb-device-as-storage)