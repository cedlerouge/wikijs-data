---
title: Augmenter le courant disponible sur les ports USB
description: 
published: true
date: 2021-08-06T09:29:16.220Z
tags: usb, augmenter, courant
editor: markdown
dateCreated: 2021-05-21T08:41:41.103Z
---

>Ceci est réservé au Raspberry Pi 2.
{.is-danger}

Avec les réglages par défaut, le Raspberry Pi 2 limite le courant à **600mA pour l'ensemble des 4 ports USB**. La modification logicielle qui est proposée ici va étendre cette limite à 1200mA.

>* Cette modification n'est pas uniquement destinée à Recalbox.
>* Cette modification est réversible.
>* L'utilisation d'une alimentation de 2000mA est toujours préconisée, quelque soit l'usage du Raspberry Pi 2.
>* Le courant maximal admissible par le Raspberry Pi 2 est de 2000mA.
{.is-warning}

## Pourquoi effectuer cette modification ?

Si vous avec besoin de connecter, sans utiliser un concentrateur USB auto-alimenté, plusieurs périphériques USB énergivores, sans dépasser le courant maximal de 1200mA sur les 4 ports USB. Par exemple : Un disque dur 2.5 pouces USB auto-alimenté.

## Exemples d'utilisation

* Utilisation d'un disque SSD USB auto-alimenté afin de stocker l'ensemble des roms, des bios et des sauvegardes.
* Clé USB wifi gourmande
* Recharger / utiliser plusieurs manettes USB simultanément.

## Comment faire

* Vous devez ouvrir le fichier [recalbox-user-config.txt](./../../../tutorials/system/modification/configtxt-configuration)
* Dans ce fichier, recherchez la présence éventuelle de la ligne `max_usb_current=...`
* Ajoutez la ligne `max_usb_current=1` ou, si elle existe déjà, modifiez sa valeur à 1.
* Enregistrez le fichier `recalbox-user-config.txt`.
* Redémarrez votre Raspberry Pi 2.

>Cette astuce fonctionne aussi sur le Raspberry Pi 3, mais la ligne `max_usb_current=...` n'existe pas dans le fichier `recalbox-user-config.txt`. Il faudra donc l'ajouter manuellement.
{.is-info}