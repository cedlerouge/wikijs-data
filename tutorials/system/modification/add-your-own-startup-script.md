---
title: Ajouter votre propre script au démarrage
description: 
published: true
date: 2021-08-06T09:29:41.699Z
tags: script, ajouter, démarrage
editor: markdown
dateCreated: 2021-05-21T08:41:22.480Z
---

Suivez ces instructions si vous souhaitez créer votre propre script de démarrage qui sera exécuté automatiquement lors du démarrage de Recalbox.

Tous les scripts de démarrage se trouvent dans `/etc/init.d`. Ils sont exécutés l'un après l'autre. Cela signifie que le script `S31emulationstation` sera traité avant `S99MyScript`. En fonction des noms de fichiers, vous pouvez déterminer l'ordre d'exécution.

La procédure de démarrage d'un script sera exécutée au démarrage et la procédure d'arrêt à l'arrêt de la Recalbox.

## Étapes pour créer votre propre script de démarrage :

* Le système est en lecture seule par défaut. Vous devez obtenir un [accès en écriture](./../../../tutorials/system/access/remount-partition-with-write-access).
* Changez le répertoire pour aller dans `/etc/init.d` avec la commande suivante :

```shell
cd /etc/init.d
```

* Tapez `ls` pour voir tous les scripts disponibles dans le répertoire `init.d`.
* Créez un nouveau script avec cette commande :

```shell
nano S99MyScript.py
```

Vous pouvez bien sûr utiliser n'importe quel numéro, nom ou type de script (.py, .sh, etc.) et modifiez-le selon vos besoins. Pour cela, vous pouvez utiliser la structure de script suivante qui contient déjà les méthodes de démarrage, d'arrêt et de redémarrage / rechargement (celle-ci convient aux scripts Bash mais peut l'être pour tout script).

```bash
#!/bin/bash

case "$1" in 
   start)  
         Ajouter votre code de démarrage ici
         ;;  
   stop)  
         Ajouter votre code d'arrêt ici
         ;;  
   restart|reload)  
         Ajouter votre code de redémarrage / rechargement ici
         ;;  
   *)  
esac  

exit $?  
```

* Une fois que votre script est prêt, enregistrez-le et fermez-le avec `CTRL + X`.
* Rendez-le exécutable avec la commande :

```shell
chmod +x S99MyScript.py
```

* Vous pouvez maintenant redémarrer votre Recalbox ou lancer le script par vous-même avec `/etc/init.d/S99MyScript start`.
* Vous êtes prêt !