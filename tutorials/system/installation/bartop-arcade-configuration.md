---
title: Configuration de Recalbox pour Bartop / Borne Arcade
description: La borne d'arcade à la maison !
published: true
date: 2021-08-06T09:19:05.897Z
tags: arcade, bartop
editor: markdown
dateCreated: 2021-05-21T08:40:58.630Z
---

Si vous voulez utiliser un Raspberry Pi avec Recalbox pour créer un « **Bartop** » ou une **borne d'arcade**, vous pouvez facilement configurer Recalbox pour répondre à votre besoin.

## GPIO

Vous pouvez configurer le pilote GPIO de la Recalbox dans le fichier [recalbox.conf.](./../../../basic-usage/getting-started/recalboxconf-file)  
Ce pilote crée deux joysticks sur le système et directement contrôlés par les PINS GPIO du Raspberry Pi.

Pour cela, vous n'avez pas besoin d'un hub USB car vous connecterez vos boutons directement sur les [GPIO](./../../../tutorials/controllers/gpio/gpio-controllers)

## Configuration vidéo

Beaucoup de bartops utilisent un vieil écran LCD ou des écrans de télévision CRT utilisant une des connexions suivantes :

* **Ecran VGA** : vous aurez besoin d'un convertisseur HDMI / VGA (convertisseur actif) coûtant dans les 10€. Si vous êtes sur un écran 4/3 vous aurez besoin de changer le mode vidéo pour le passer à `global.videomode=default` pour tout vos émulateurs dans le fichier [recalbox.conf](./../../../basic-usage/getting-started/recalboxconf-file).
* **Ecran DVI** : vous aurez besoin d'un adaptateur HDMI / DVI (convertisseur passif). Vous aurez probablement besoin de modifier votre fichier [recalbox.conf](./../../../basic-usage/getting-started/recalboxconf-file) en vous basant basant sur [ce tutoriel](./../../../tutorials/video/lcd/dvi-screen).
* **Ecran CRT** : Vous aurez besoin d'une prise jack vers RCA avec un câble vidéo. Vous devez changer le mode vidéo pour le positionner sur `global.videomode=default` pour tous vos jeux dans le fichier [recalbox.conf](./../../../basic-usage/getting-started/recalboxconf-file) et modifier les paramètres décris dans [ce tutoriel](./../../../tutorials/video/crt/crt-screen-with-compostite).

## Configuration audio

Vous pouvez sélectionner le périphérique de sortie audio dans le fichier [recalbox.conf](./../../../basic-usage/getting-started/recalboxconf-file) :

* Utilisez `audio.device=alsa_card.0:analog-output` si vous voulez forcer la **sortie audio sur la prise jack.** 
* Utilisez `audio.device=alsa_card.1:hdmi-output-0` si vous voulez forcer la **sortie audio sur le HDMI.**

Vous pouvez aussi choisir la sortie son dans EmulationStation. Appuyez sur `START` > `OPTIONS DU SON` et modifiez la valeur de l'option `SORTIE AUDIO`.

## Configuration du menu

Vous pouvez désactiver Kodi dans le fichier [recalbox.conf](./../../../basic-usage/getting-started/recalboxconf-file) en positionnant le paramètre `kodi.enable=0`dans le but de supprimer le raccourci affecté au bouton "X".

Vous pouvez aussi vouloir changer les réglages du menu :

* **Pour EmulationStation** :
  * En indiquant `system.es.menu=bartop`, vous aurez accès à un minimum d'options quand vous presserez le bouton `START` sous EmulationStation.
  * En indiquant `system.es.menu=none`, aucun menu ne sera disponible. 
* **Pour les émulateurs** :
  * En indiquant `system.emulators.specialkey` sur `nomenu` , vous désactiverez les menus dans les émulateurs.
  * En indiquant `system.emulators.specialkey` sur `none` , vous désactiverez les menus et toutes les fonctions spéciales dans les émulateurs (sauf la combinaison pour sortir d'un jeu).