---
title: Accès réseau via WinSCP
description: 
published: true
date: 2021-09-04T21:50:18.533Z
tags: winscp, accès, réseau
editor: markdown
dateCreated: 2021-05-21T08:40:01.854Z
---

Ce tutoriel s'adresse aux utilisateurs de Windows. Veuillez consulter [ce tutoriel](./../../../tutorials/system/access/network-access-cyberduck) si vous êtes utilisateur de macOS ou si vous souhaitez utiliser Cyberduck sur Windows.

## Pré-requis

* Assurez-vous que votre Recalbox est bien lancée et connectée au réseau via un câble Ethernet ou un dongle Wifi.

>Vous avez un doute et votre Recalbox est connecté ? Rendez-vous dans [ce tutoriel](./../../../tutorials/network/ip/discover-recalbox-ip) pour connaître l'adresse IP de votre Recalbox.
{.is-info}

* Téléchargez et installez le logiciel [WinSCP](https://winscp.net/).

## Configuration

* Lancez `WinSCP` et une fenêtre va apparaître pour créer votre première connexion.
* Dans cette fenêtre, remplissez les informations comme il suit :
  * Protocole de fichier `SFTP`
  * Nom de l'hôte `recalbox` ou l'adresse IP que vous avez précédemment identifié (la config sera à modifier à chaque redémarrage de Recalbox si vous avez rentré l'adresse ip et que cette dernière est dynamique).
  * Numéro de port `22`
  * Nom d'utilisateur `root`
  * Mot de passe `recalboxroot`
* Cliquez sur `Sauver...`.
* Remplissez les informations comme il suit :
  * Enregistre la session sous : `Recalbox` par exemple.
  * Cochez la case `Enregistrer le mot de passe (non recommandé)` puis cliquez sur `OK`.
* Votre logiciel est maintenant configuré.

## Connexion

* Dans la fenêtre ouverte, sélectionnez votre connexion crée et cliquez sur `Connexion`.
* Une autre fenêtre s'ouvre vous informant de la connexion en cours.
* Vous êtes connecté.

>Une autre fenêtre peut s'ouvrir vous demandant de `Continuer la connexion au serveur inconnu et ajouter la clé d'hôte dans le cache ?` Cliquez sur `Oui` si c'est le cas.
{.is-warning}

## Afficher les fichiers cachés

Cela vous sera utile pour accéder au dossier `.emulationstation` pour modifier le thème par exemple.

* Cliquez sur `Options` > `Préférences`.
* Sélectionnez `Panneaux` dans la partie de gauche.
* Cochez la case `Afficher les fichiers cachés` et cliquez sur `OK`.

## Utilisation

L'utilisation se veut assez intuitive. Par défaut, vous retrouvez à gauche votre ordinateur et à droite, l'arborescence de votre Recalbox.

**Quelques astuces** :

* Pour revenir au dossier supérieur ou à la racine de votre Recalbox, il suffit de cliquer sur le premier dossier avec un picto de retour haut et identifié comme `..`.
* Vous pouvez déposer des dossiers par simple glisser-déposer dans la colonne de droite ou dans la colonne de droite.
* Vous pouvez changer les permissions sur un fichier via un clic droit sur ce dernier puis `Propriétés`.

## Ajouter Notepad++

* Téléchargez [Notepad++](https://notepad-plus-plus.org/downloads/).
* Dans WinSCP, cliquez sur `Options` > `Préférences`.
* Sélectionnez `Éditeurs` dans la partie de gauche.
* Cliquez sur `Ajouter` et recherchez l'endroit où vous avez installé Notepad++.

## Utiliser Winscp pour utiliser la console

Cliquez sur l’icône `Console` puis saisir la commande pour mettre la partition en écriture.

![](/tutorials/system/access/winscp.png)