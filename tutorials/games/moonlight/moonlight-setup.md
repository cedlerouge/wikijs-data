---
title: Mise en route de Moonlight
description: 
published: true
date: 2021-09-04T22:54:02.285Z
tags: moonlight, mise en route
editor: markdown
dateCreated: 2021-05-21T08:38:29.815Z
---

## Introduction

Moonlight est une version open-source de la technologie Gamestream de NVidia.

Si votre PC répond aux exigences, vous pouvez diffuser la plupart de vos jeux en streaming sur votre Recalbox. D'un autre côté, Recalbox lit la configuration des pads depuis EmulationStation et la convertit vers Moonlight.

### Fonctionnalités de Moonlight sur Recalbox

* Diffusez des jeux en streaming sur votre réseau local ou via Internet
* Jusqu'à 4 joueurs supportés
* Jusqu'à 1080p/60fps
* Décodage H264 accéléré par le matériel sur n'importe quelle version du Raspberry Pi
* Supporte le clavier et la souris
* Jusqu'à GFE 3,12

### Pré-requis

Configuration requise pour Recalbox et le PC pour profiter de Moonlight :

* Une manette configurée dans EmulationStation
* Compte Steam (optionnel) ou jeux supportés de façon autonome (voir [https://shield.nvidia.com/game-stream](https://shield.nvidia.com/game-stream))
* Un GPU compatible Nvidia (voir [http://www.geforce.com/geforce-experience/system-requirements](http://www.geforce.com/geforce-experience/system-requirements))
* Une connexion Ethernet est fortement recommandée, le WiFi n'est pas assez fiable.

## Configuration de Moonlight

Recalbox offre un scrap plus agréable, la possibilité de diffuser en continu à partir de plusieurs PC du réseau et une petite option pour trouver les hôtes GFE disponibles.

Voici une démo pour trouver des hôtes GFE, les connecter et les lancer :

```shell
# /recalbox/scripts/moonlight/Moonlight.sh find
Listing available GFE servers :
GFE Host WIN10(192.168.111.35) GeForce GTX 760 running GFE 3.12.0.84
You can now run /recalbox/scripts/moonlight/Moonlight.sh pair <host>
<host> can be empty (not recommended if you have several GFE hosts), an IP or a PC name

# /recalbox/scripts/moonlight/Moonlight.sh pair
() /recalbox/share/system/configs/moonlight/moonlight.conf | /recalbox/share/system/configs/moonlight/keydir
Searching for server...
Connect to 192.168.111.35...
Generating certificate...done
Please enter the following PIN on the target PC: 3843
Succesfully paired
YOLO MODE !!!

# /recalbox/scripts/moonlight/Moonlight.sh init
YOLO Mode
Adding and scraping Brutal Legend ...
Adding and scraping Sacred Citadel ...
Adding and scraping Just Cause 3 ...
Adding and scraping Street Fighter V ...
Adding and scraping Just Cause 2 Multiplayer ...
Adding and scraping Tales of Zestiria ...
Adding and scraping Grand Theft Auto V ...
Adding and scraping Hell Yeah! Wrath of the Dead Rabbit ...
Adding and scraping Ultra Street Fighter IV ...
Adding and scraping Diablo III ...
Adding and scraping Pro Evolution Soccer 2017 ...
Adding and scraping Bionic Commando Rearmed ...
Adding and scraping Just Cause 2 ...
Adding and scraping Pro Evolution Soccer 2016 ...
Adding and scraping Broforce ...
Adding and scraping DmC: Devil May Cry ...
Adding and scraping Naruto Shippuden: Ultimate Ninja Storm 4 ...
Adding and scraping Steam ...
```

## ANNEXE

>Utilisateurs avancés uniquement : Vvus pouvez diffuser en continu à partir d'un ordinateur distant sur Internet. 
{.is-warning}

* Vous devrez modifier `/recalbox/scripts/moonlight/Moonlight.sh` pour :
  * Spécifier l'adresse IP de l'hôte distant,
  * Définir la même adresse IP dans moonlight.conf,
  * Et configurer la redirection de port selon [https://github.com/moonlight-stream/moonlight-android/wiki/Setup-Guide#streaming-over-the-internet](https://github.com/moonlight-stream/moonlight-android/wiki/Setup-Guide#streaming-over-the-internet)