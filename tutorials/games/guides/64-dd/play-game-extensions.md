---
title: Jouer aux extensions des jeux
description: 
published: true
date: 2021-09-08T12:52:25.490Z
tags: disk, 64dd, drive, n64dd, extensions, fzero
editor: markdown
dateCreated: 2021-07-05T18:17:26.915Z
---

## Introduction

Au moins 1 jeu, F-Zero X, possède son extension de jeu au travers du Nintendo 64 Disk Drive. Nous allons voir ici comment procéder.

## Utilisation

1. Placez le jeu `F-Zero X (Japan).z64` dans le répertoire `64dd`.
2. Placez le jeu `F-Zero X - Expansion Kit (Japan).ndd` dans le répertoire `64dd`.
3. Mettez à jour la liste de vos jeux.
4. Dans la liste de jeux, positionnez-vous sur le jeu voulu et appuyer sur `START`.

![](/tutorials/games/guides/64-dd/play-game-extensions/fzerodd1.png){.full-width}

5. Allez dans `MODIFIER LE JEU` > `LANCER AVEC` et choisissez le core `LIBRETRO MUPEN64PLUS_NEXT`.
   
![](/tutorials/games/guides/64-dd/play-game-extensions/fzerodd2.png){.full-width}

6. Fermez tous les menus.
7. Lancez `F-Zero X (Japan)` dans le système `64dd`.
8. Quand le jeu démarre, allez dans le menu de RetroArch en faisant `Hotkey + B`.

![](/tutorials/games/guides/64-dd/play-game-extensions/fzerodd3.png){.full-width}

9. Faites « Retour » 1 fois puis allez dans `Sous-systèmes`.

![](/tutorials/games/guides/64-dd/play-game-extensions/fzerodd4.png){.full-width}

10. Sélectionnez l'option `Load N64 Disk Drive`.

![](/tutorials/games/guides/64-dd/play-game-extensions/fzerodd5.png){.full-width}

11. Sélectionnez le jeu `F-Zero X - Expansion Kit (Japan).ndd`.

![](/tutorials/games/guides/64-dd/play-game-extensions/fzerodd6.png){.full-width}

12. Vous allez automatiquement retourner en arrière. Retournez dans `Sous-systèmes` et choisissez de nouveau `Load N64 Disk Drive`.

![](/tutorials/games/guides/64-dd/play-game-extensions/fzerodd7.png){.full-width}

13. Sélectionnez le jeu `F-Zero X (Japan).z64`.

![](/tutorials/games/guides/64-dd/play-game-extensions/fzerodd8.png){.full-width}

14. Vous allez automatiquement retourner en arrière. Retournez dans `Sous-systèmes` et choisissez `Start N64 Disk Drive`.

![](/tutorials/games/guides/64-dd/play-game-extensions/fzerodd9.png){.full-width}

15. Et votre jeu démarre avec l'extension.

![](/tutorials/games/guides/64-dd/play-game-extensions/fzerodd10.png){.full-width}
![](/tutorials/games/guides/64-dd/play-game-extensions/fzerodd11.png){.full-width}

Bon jeu !