---
title: Jouer avec deux Game Boy
description: 
published: true
date: 2021-09-05T12:47:00.523Z
tags: gameboy, deux
editor: markdown
dateCreated: 2021-05-21T08:46:52.599Z
---

Il est possible de jouer à deux joueurs avec deux GameBoy sur Recalbox. Il existe 3 méthodes pour y arriver.

>Même si cela peut être tentant, Recalbox ne supporte pas le Netplay dans ce mode.
>
>De plus, en mode un jeu pour deux Game Boy, les sauvegardes ne sont pas supportées pour la deuxième Game Boy, contrairement au mode [deux jeux deux Game Boy](./play-two-players-games-with-gamelink), où là encore, le Netplay n'est pas supporté.
{.is-warning}

## La méthode « no edit »

1. Dans EmulationStation, sélectionnez la Game Boy ou la Game Boy Color.
2. Dans la liste de jeux, positionnez-vous sur le jeu voulu et appuyer sur `START`.

![](/tutorials/games/guides/game-boy/play-two-players/gameboytwoplayers1.png){.full-width}

3. Allez dans `MODIFIER LE JEU` > `LANCER AVEC` et choisissez le core `LIBRETRO TGBDUAL`.

![](/tutorials/games/guides/game-boy/play-two-players/gameboytwoplayers2.png){.full-width}

4. Fermez tous les menus et lancez le jeu.
5. Quand le jeu démarre, allez dans le menu de RetroArch en faisant `Hotkey + B`.
6. Faites « Retour » 2 fois puis allez dans `Réglages` > `Configuration`.

![](/tutorials/games/guides/game-boy/play-two-players/gameboytwoplayers3.png){.full-width}

7. Activez le paramètre `Sauvegarder la configuration en quittant`.

![](/tutorials/games/guides/game-boy/play-two-players/gameboytwoplayers4.png){.full-width}

8. Faires « Retour » 2 fois et allez dans `Menu principal` > `Menu rapide` > `Options`.

![](/tutorials/games/guides/game-boy/play-two-players/gameboytwoplayers5.png){.full-width}

9. Activez le paramètre `Link cable emulation (reload)`.

![](/tutorials/games/guides/game-boy/play-two-players/gameboytwoplayers6.png){.full-width}

Vous pouvez remettre désactiver `Sauvegarder la configuration en quittant` après avoir redémarré le jeu.

>En cas d'utilisation de l'option `RESTAURER LES PARAMÈTRES D'USINE`, ces modifications seront perdues !
{.is-warning}

## L'édition du fichier recalbox.conf

* Modifiez le fichier [recalbox.conf](./../../../../basic-usage/getting-started/recalboxconf-file) pour remplacer l'émulateur GameBoy par `tgbdual` :

```text
gb.core=tgbdual ;GameBoy
gb.ratio=custom
gbc.core=tgbdual ;GameBoyColor
gbc.ratio=custom
```

* Enregistrez et redémarrez proprement votre Recalbox.
* Lancez un jeu GameBoy (par exemple `Tetris`).
* Ouvrez le menu RetroArch avec `Hotkey + B`.
* Faites « Retour » 2 fois puis allez dans `Réglages` > `Configuration`.

![](/tutorials/games/guides/game-boy/play-two-players/gameboytwoplayers7.png){.full-width}

* Activez le paramètre `Sauvegarder la configuration en quittant`.

![](/tutorials/games/guides/game-boy/play-two-players/gameboytwoplayers8.png){.full-width}

* Faires « Retour » 2 fois et allez dans `Menu principal` > `Menu rapide` > `Options`.

![](/tutorials/games/guides/game-boy/play-two-players/gameboytwoplayers9.png){.full-width}

* Activez le paramètre `Link cable emulation (reload)`.

![](/tutorials/games/guides/game-boy/play-two-players/gameboytwoplayers10.png){.full-width}

* Retournez en jeu et quittez aussi ce dernier pour retourner sous EmulationStation.
* Lancez de nouveau votre jeu, refaites `Hotkey + B` pour retourner dans le menu RetroArch et allez dans `Options`.

![](/tutorials/games/guides/game-boy/play-two-players/gameboytwoplayers11.png){.full-width}

* Pour l'option `Screen layout`, choisissez `left-right`.

![](/tutorials/games/guides/game-boy/play-two-players/gameboytwoplayers12.png){.full-width}

* Pour l'option `Switch player screens`, choisissez `normal`.

![](/tutorials/games/guides/game-boy/play-two-players/gameboytwoplayers13.png){.full-width}

* Pour l'option `Show player screen`, choisissez `both players`.

![](/tutorials/games/guides/game-boy/play-two-players/gameboytwoplayers14.png){.full-width}

* Faites « Retour » 2 fois puis allez dans `Réglages` > `Vidéo` > `Mise à l'échelle`.

![](/tutorials/games/guides/game-boy/play-two-players/gameboytwoplayers15.png){.full-width}

* Pour l'option `Rapport d'aspect`, choisissez `1:1 PAR (20:9 DAR)`.

![](/tutorials/games/guides/game-boy/play-two-players/gameboytwoplayers16.png){.full-width}

* Retournez en jeu et quittez aussi ce dernier pour retourner sous EmulationStation.

>Pensez que si votre TV n'est pas réglée en 16/9 - si le menu de Recalbox n'occupe pas l'intégralité de l'écran - alors le rapport d'aspect (ratio) ne sera pas bon.
>
>Notez aussi que les jeux mono-joueurs vont aussi êtres doublés. Deux GB côte à côte, bien pour un duel de speed run, sinon inutile.
>
>Pour éviter trop de manipulations, vous pouvez appliquer tous ces paramètres que sur GB ou que sur GBC.
>
>En cas d'utilisation de l'option `RESTAURER LES PARAMÈTRES D'USINE`, ces modifications seront perdues !
{.is-warning}

## L'utilisation de fichiers de surcharge (pour utilisateurs avancés)

Les surcharges de configuration sont un moyen de forcer des paramètres pour un jeu, un sous-dossier ou tout un système. Vous pouvez en savoir plus sur la [page dédiée](./../../../../advanced-usage/configuration-override).

Dans le cas qui nous intéresse, il nous faudra un fichier nommé `nomdujeu.extension.recalbox.conf` (par exemple : `Tetris (World) (Rev 1).zip.recalbox.conf`) contenant les lignes suivantes (forcer le core `Libretro TGBDual` pour ce jeu uniquement) :

```text
gb.core=tgbdual
gbc.core=tgbdual
```

Un fichier `nomdujeu.extension.core.cfg` (par exemple : `Tetris (World) (Rev 1).zip.core.cfg`) contenant ces lignes :

```text
tgbdual_audio_output = "Game Boy #1"
tgbdual_gblink_enable = "enabled"
tgbdual_screen_placement = "left-right"
tgbdual_single_screen_mp = "both players"
tgbdual_switch_screens = "normal"
```

>Ici, on a réglé TGBDual pour utiliser le son de la Game Boy #1, activer le GameLink, afficher deux GameBoy côte à côte (une à gauche et une à droite), et la GameBoy du joueur #1 est à gauche.
{.is-info}

Et un dernier fichier `nomdujeu.extension.retroarch.cfg` (par exemple : `Tetris (World) (Rev 1).zip.retroarch.cfg`) contenant :

```text
aspect_ratio_index = "22"
input_overlay_enable = "false"
```

>On force l'aspect ratio dans le mode 22 : Core Provided, tout en désactivant les overlays.
{.is-info}

Ces fichiers doivent être placés au même endroit que le jeu souhaité (dans l'exemple pris précédemment, au même endroit que le jeu `Tetris (Workd) (Rev 1).zip`).

>Si les fichiers de surcharges se nomment uniquement `.recalbox.conf`, .`retroarch.cfg` et `.core.cfg`, alors ils s'appliqueront à tout le dossier dans lequel ils sont placés, ainsi que les sous-dossiers si il y en a.
{.is-info}

>En cas d'utilisation de l'option `RESTAURER LES PARAMÈTRES D'USINE`, ces modifications seront conservées !
{.is-success}

>Les fichiers de surcharges ne sont pas affectés par la configuration depuis EmulationStation, et ils prennent le dessus sur ces derniers. Il est donc impératif de pouvoir accéder à ces fichiers pour effectuer une modification ultérieure sur les paramètres qu'ils affectent.
{.is-warning}