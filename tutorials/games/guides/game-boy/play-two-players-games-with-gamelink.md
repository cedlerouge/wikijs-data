---
title: Jouer à deux jeux différents en mode GameLink
description: 
published: true
date: 2021-09-05T12:46:37.087Z
tags: gameboy, gamelink, deux, joueurs
editor: markdown
dateCreated: 2021-05-21T08:46:45.940Z
---

TGBDual permet de jouer à la Game Boy et la Game Boy Color en simulant un câble GameLink pour du multi-joueur. Cependant, pour utiliser deux jeux différents, la manipulation à faire est plus complexe.

Ce tutoriel va assumer que vous savez déjà lancer [un jeu sur deux Game Boy avec TGBDual](./play-with-two-game-boy).

>Le fait d'utiliser deux jeux différents permet d'utiliser la sauvegarde des jeux pour chaque GameBoy.
{.is-success}

## Deux jeux avec un même système

La configuration la plus simple est de jouer à deux jeux différents d'un même système (gb+gb ou gbc+gbc), car cela ne demande aucune manipulation de sauvegarde.

Pour cela :

* Lancez un jeu du système souhaité (gb ou gbc) avec TGBDual.
* Une fois le jeu lancé, il faut appuyer sur `Hotkey + B` pour accéder au menu RetroArch.
* Faites « Retour » 1 fois puis allez dans `Sous-systèmes`.
* Dans le menu, vous verrez l'option `Load 2 Player Game Boy Link ★` ainsi que la ligne `Current Content: GameBoy #1`.

![](/tutorials/games/guides/game-boy/play-two-different-games-gamelink/2diffgames1core-1.png){.full-width}

* Sélectionnez l'option disponible et vous serez dans le répertoire auquel vous avez lancé le jeu. Sélectionnez le jeu que vous aviez lancé et validez.

![](/tutorials/games/guides/game-boy/play-two-different-games-gamelink/2diffgames1core-2.png){.full-width}

>Si le jeu est zippé, il vous faudra demander de parcourir l'archive, puis d'ouvrir la rom contenue à l'intérieur.
{.is-warning}

* Une fois validé, vous devez retourner dans `Sous-systèmes`. Vous verrez l'option `Load 2 Player Game Boy Link ★` ainsi que la ligne `Current Content: GameBoy #2`.

![](/tutorials/games/guides/game-boy/play-two-different-games-gamelink/2diffgames1core-3.png){.full-width}

* Sélectionnez l'option disponible et vous serez dans le répertoire auquel vous avez lancé le prmier jeu. Sélectionnez le second jeu et validez.

![](/tutorials/games/guides/game-boy/play-two-different-games-gamelink/2diffgames1core-4.png){.full-width}

>Là encore, si le jeu est zippé, il faudra parcourir l'archive, puis ouvrir la rom contenue à l'intérieur.
{.is-warning}

* Une fois validé, vous devez retourner dans `Sous-systèmes`. Vous verrez l'option `Start 2 Player Game Boy Link ★` avec les jeux à lancer en dessous.

![](/tutorials/games/guides/game-boy/play-two-different-games-gamelink/2diffgames1core-5.png){.full-width}

* Sélectionnez l'option et l'émulateur va complètement redémarrer avec les 2 jeux sélectionnés.

![](/tutorials/games/guides/game-boy/play-two-different-games-gamelink/2diffgames1core-6.png){.full-width}

## Deux jeux, deux systèmes différents.

Dans le cas où l'on voudrait lancer deux jeux, mais l'un de Game Boy Color, et l'autre de la Game Boy, une étape supplémentaire se rajoute. Il faut placer la sauvegarde des deux jeux dans le même dossier avant de démarrer.

Ainsi, si vous souhaitez lancer le système Game Boy Color depuis EmulationStation, vous devrez déplacer la sauvegarde du jeu Game Boy concerné (dans `share\saves\gb`) dans le dossier `share\saves\gbc`.

Ainsi, pour le jeu `Pokemon - Version Bleue (France) (SGB Enhanced)` dans le dossier `gb`, il faut déplacer les fichiers `Pokemon - Version Bleue (France) (SGB Enhanced).rtc` et `Pokemon - Version Bleue (France) (SGB Enhanced).srm` dans le dossier `share\saves\gbc`. puis récupérer ces fichiers une fois la partie terminée et l'émulateur fermé pour les remettre à leur endroit d'origine.