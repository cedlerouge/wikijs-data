---
title: FAQ
description: 
published: true
date: 2021-09-09T22:40:03.134Z
tags: thomson, mo5, to8, faq
editor: markdown
dateCreated: 2021-07-29T17:49:59.594Z
---

## Utilisation

Par défaut, le type d'ordinateur émulé dépend du nom du fichier utilisé.

_**Exemple :**_  
**super-tennis-fil_mo5.k7** provoquera l'émulation du **MO5**.

>Si le fichier ne contient pas le nom d'une des machines émulées, l'émulateur basculera en mode TO8 par défaut.  
>Il est possible de forcer le type de machine émulée via une option dans RetroArch.
{.is-info}

Les jeux ne démarrent pas automatiquement au chargement de la machine.  
Sur les ordinateurs Thomson, il est en effet nécessaire d'appuyer sur une (ou plusieurs) touche(s) du clavier pour lancer un programme. 
  
Afin de faciliter l'utilisation de l'émulateur à la manette, le bouton `START` de la manette essaye de lancer le jeu en choisissant une méthode qui dépend de l'ordinateur émulé ainsi que du type de média inséré (cf. [fichier README de Theodore](https://github.com/Zlika/theodore/blob/master/README-FR.md#video_game-correspondance-des-boutons-de-la-manette)).  
  
En cas d'échec, il faut essayer une autre méthode via le clavier ou le clavier virtuel (cf. plus bas).

## Manettes

Mise à part le bouton `B` de la manette qui permet d'émuler l'unique bouton des joysticks pour Thomson, les autres boutons de la manette sont utilisés pour la fonctionnalité de "clavier virtuel".

Cette fonctionnalité permet de jouer facilement à la plupart des jeux sans avoir besoin d'un clavier réel.

<table>
  <thead>
    <tr>
      <th style="text-align:center">Bouton</th>
      <th style="text-align:left">Description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td style="text-align:center"><b>B</b>
      </td>
      <td style="text-align:left">
        <p>Bouton "Action" quand le clavier virtuel n'est pas affiché.</p>
        <p>Quand le clavier virtuel est affiché :</p>
        <ul>
          <li>Appuie court : Appuie sur la touche du clavier ayant le focus.</li>
          <li>Appuie long : Maintien permanent de la touche (ou relâchement si
            elle était déjà maintenue). Jusqu'à 3 touches
            peuvent être maintenues. La disparition du clavier virtuel relâche
            toutes les touches maintenues.</li>
        </ul>
      </td>
    </tr>
    <tr>
      <td style="text-align:center"><b>Y</b>
      </td>
      <td style="text-align:left">Déplace le clavier virtuel en haut ou en bas de l'écran.</td>
    </tr>
    <tr>
      <td style="text-align:center"><b>Start</b>
      </td>
      <td style="text-align:left">
        <p>Démarrage du jeu si le clavier virtuel n'est pas affiché.</p>
        <p>Raccourci pour appuyer sur la touche "Entrée" si le clavier
          virtuel est affiché.</p>
      </td>
    </tr>
    <tr>
      <td style="text-align:center"><b>Select</b>
      </td>
      <td style="text-align:left">Afficher/cacher le clavier virtuel.</td>
    </tr>
  </tbody>
</table>