---
title: Jouer avec de véritables Wiimotes
description: 
published: true
date: 2021-09-05T13:03:35.818Z
tags: wii, dolphin, bar, wiimotes, manettes
editor: markdown
dateCreated: 2021-06-28T07:47:46.763Z
---

>La barre Dolphin Bar de chez Mayflash est nécessaire pour utiliser pleinement les Wiimotes avec Dolphin.
{.is-info}

* Branchez votre Dolphin Bar à votre Recalbox puis passer la en mode 4.
* Ouvrez  le fichier `/recalbox/share/system/recalbox.conf`
* Dans la section "D3 - Dolphin Controllers" cherchez la ligne suivante :
  * `wii.realwiimotes=0` 
* puis modifiez par ceci :
  * `wii.realwiimotes=1` 
* Voilà, vos Wiimotes fonctionnent maintenant dans Dolphin.