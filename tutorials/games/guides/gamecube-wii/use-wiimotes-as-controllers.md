---
title: Utiliser les Wiimotes comme manettes
description: 
published: true
date: 2021-09-05T13:02:12.001Z
tags: wiimotes, manettes
editor: markdown
dateCreated: 2021-05-21T08:46:26.861Z
---

>Dolphin peut connecter jusqu'a 4 Wiimotes en simultané.
>
>Pour permettre le support des Wiimotes dans Dolphin, la barre Dolphin Bar de chez Mayflash est nécessaire. Vous devrez la mettre en mode 4.
{.is-info}

## Configuration

* Ouvrez le fichier [recalbox.conf](./../../../../basic-usage/getting-started/recalboxconf-file).
* Trouvez le texte suivant :

```ini
## Wiimotes
## Real wiimotes must not be paired with recalbox system so that they can work with the wii emulator
## set emulatedwiimotes to 1 to emulate wiimotes with standard pads
wii.emulatedwiimotes=0
```

* Modifier la ligne suivante :

```ini
wii.emulatedwiimotes=0
```

* Comme cela :

```ini
wii.emulatedwiimotes=1
```

Vous pouvez à présent jouer avec vos Wiimotes.