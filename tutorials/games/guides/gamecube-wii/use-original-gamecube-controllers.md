---
title: Jouer avec de véritables manettes Gamecube
description: 
published: true
date: 2021-09-05T13:02:44.013Z
tags: gamecube, manettes
editor: markdown
dateCreated: 2021-06-27T19:42:00.452Z
---

>Un adaptateur USB pour manette Gamecube est nécessaire pour cette fonction !
{.is-info}

* Branchez votre adaptateur Gamecube a votre Recalbox 
* Ouvrez le fichier : `/recalbox/share/system/recalbox.conf`
  * Dans la section contrôleurs, cherchez la ligne suivante :
    * `gamecube.realgamecubepads=0`  
  * Modifiez par ceci :
    * `gamecube.realgamecubepads=1` 
* Voila votre manette Gamecube fonctionne maintenant dans Dolphin.