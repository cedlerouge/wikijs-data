---
title: Changer le pays et la langue dans le jeu
description: 
published: true
date: 2021-09-05T13:05:05.759Z
tags: model3, langue, pays
editor: markdown
dateCreated: 2021-05-21T08:45:54.696Z
---

>Pour les jeux suivant le pays est lié à la rom (pas de changement possible dans le "TEST MENU")
>
>* Dirt Devils
>* Emergency Call Ambulance
>* Scud Race: 
>   * Pour la version Australienne (Australia), pas de menu prévu pour changer la région.
>   * Pour les autres versions, le changement s'effectue directement dans le "TEST MENU"
{.is-info}

![](/emulators/arcade/supermodel/changelanguage1.png){.full-width}

Pour chaque jeu, lorsqu'il est lancé, entrer dans le "TEST MENU" à l'aide de la touche `L3`.

Ensuite la manipulation diffère suivant le jeu.

![](/emulators/arcade/supermodel/changelanguage2.png){.full-width}

![](/emulators/arcade/supermodel/changelanguage3.png){.full-width}

## Daytona USA 2

>Fonctionne pour « Battle on the edge » et pour « Power Edition ».
{.is-info}

Fonctions des différentes touches dans le "TEST MENU".

| Fonctionnalités | Raccourci |
| :---: | :---: |
| Monter la flèche dans le menu | **Y** |
| Descendre la flèche dans le menu | **X** |
| Sélection croissante | **A** |
| Sélection décroissante | **B** |
| Valider | **L3** |

* Tout en maintenant la touche `START` enfoncée, entrez cette succession de touches : `B`, `B`, `X`, `A`, `Y`, `A`, `X`
* Vous vous retrouvez dans "COUNTRY ASSIGNEMENTS".
  * Déplacez la flèche sur "COUNTRY".
  * Changez à l'aide d'une des touches de sélection jusqu'à avoir le pays voulue (Japan, Korea, Australia, Export, USA).
* Validez.

## Fighting Vipers 2

Fonctions des différentes touches dans le "TEST MENU".

| Fonctionnalités | Raccourci |
| :---: | :---: |
| Monter la flèche dans le menu | **X** |
| Descendre la flèche dans le menu | **B** ou **R3** |
| Valider / Changer la valeur | **A** ou **L3** |

* Dirigez-vous dans le sous menu "GAME ASSIGNMENTS".
  * Placez la flèche sur "Country".
  * Entrez cette succession de touches au D-pad (croix directionnelle) : 3X **Gauche**, 2X **Droite**, 1X **Gauche**.
  * Changez le pays à l'aide de **L3** (Japan, USA, Export, Asia).
* Sortez du menu (Descendre la flèche vers "EXIT").

## Harley Davidson & L.A. Riders

Fonctions des différentes touches dans le "TEST MENU".

| Fonctionnalités | Raccourci |
| :---: | :---: |
| Monter la flèche dans le menu | **X** ou **R1** |
| Descendre la flèche dans le menu | **B** ou **L1** ou **R3** |
| Valider / Changer la valeur | **A** ou **L3** |

* Dirigez-vous dans le sous menu "GAME ASSIGNMENTS".
  * Entrez cette succession de touches : 2x `R1`, 2x `L1`, `X`, `B`, `X`, `B`.
  * La ligne "Country" en bas de l'écran apparait.
  * Descendez la flèche d'un cran vers le bas. 
  * Changez le pays à l'aide de `L3` (Japan, USA, Export, Korea, Australia).
* Sortir du menu (Monter la flèche vers "EXIT").

## L.A. Machine Guns

Fonctions des différentes touches dans le "TEST MENU".

| Fonctionnalités | Raccourci |
| :---: | :---: |
| Descendre la flèche dans le menu | **R3** |
| Valider / Changer la valeur | **L3** |

* Entrez cette succession de touches: 2x `START`, 1x `R3`, 3x `START`,1x `R3`, 1x `L3`.

>Les fonctions des différentes touches dans le "TEST MENU" ne sont pas les même dans le "COUNTRY SELECT".
{.is-warning}

Fonctions des différentes touches dans le "COUNTRY SELECT MODE".

| Fonctionnalités | Raccourci |
| :---: | :---: |
| Changer la valeur | **R3** |
| Valider | **L3** |

* Vous vous retrouvez dans "COUNTRY SELECT MODE".
  * Changer la valeur jusqu'à avoir le pays voulue (Japan, USA, Export, Australia).
* Valider.

## Le Mans 24H

Fonctions des différentes touches dans le "TEST MENU"

| Fonctionnalités | Raccourci |
| :---: | :---: |
| Descendre la flèche dans le menu | **R3** |
| Valider / Changer la valeur | **L3** |

* Entrez cette succession de touches : 2x `START`, 2x `R3`, 1x `START`, 1x `L3` .

>Les fonctions des différentes touches dans le "TEST MENU" ne sont pas les même dans le "COUNTRY SELECT".
{.is-warning}

Fonctions des différentes touches dans le "COUNTRY SELECT MODE".

| Fonctionnalités | Raccourci |
| :---: | :---: |
| Changer la valeur | **R3** |
| Valider | **L3** |

* Vous vous retrouvez dans "COUNTRY SELECT MODE".
  * Changer la valeur jusqu'à avoir le pays voulue (Japan, USA, Export, Australia).
* Valider.

## Magical Truck Adventure

>Il n'y a pour ce jeu que le Japon comme région accessible.
{.is-info}

Fonctions des différentes touches dans le "TEST MENU"

| Fonctionnalités | Raccourci |
| :---: | :---: |
| Descendre la flèche dans le menu | **R3** |
| Valider / Changer la valeur | **L3** |

* Entrez cette succession de touches: `START`, `START`, `R3`, `START`, `R3`, `L3`.
  * Changez la valeur jusqu'à avoir le pays voulue (Japan).
  * Validez.

## Sega Bass Fishing

Fonctions des différentes touches dans le "TEST MENU"

| Fonctionnalités | Raccourci |
| :---: | :---: |
| Descendre la flèche dans le menu | **R3** |
| Valider / Changer la valeur | **L3** |

* Entrez dans le sous menu "C.R.T. TEST".
  * Passez à la page 2/2 (la page est afficher en haut de l'écran) à l'aide de la touche `L3`.
  * Appuyez sur la touche 4x `R3`.
  * Sortez à l'aide de la touche `L3`.
* Vous êtes de nouveau sur "TEST MENU". Sélectionnez le sous menu "GAME ASSIGNEMENTS".
  * Appuyez sur la touche 3x `R3`, ensuite `L3` + `R3`.

>Les fonctions des différentes touches dans le "TEST MENU" ne sont pas les même dans le "COUNTRY SELECT".
{.is-warning}

Fonctions des différentes touches dans le "COUNTRY SELECT".

| Fonctionnalités | Raccourci |
| :---: | :---: |
| Changer la valeur | **R3** |
| Valider | **L3** |

* Vous vous retrouvez dans "COUNTRY SELECT MODE".
  * Changez la valeur jusqu'à avoir le pays voulue (Japan, USA, Export, Australia).
* Validez.

## Sega Rally 2

Fonctions des différentes touches dans le "TEST MENU"

| Fonctionnalités | Raccourci |
| :---: | :---: |
| Descendre la flèche dans le menu | **R3** |
| Valider / Changer la valeur | **L3** |

* Entrez cette succession d'actions avec la touche `R3` (Long ou Court).
  * 4x Appuie Court, 2x Long, 2x Court, 1x Long .

>Les fonctions des différentes touches dans le "TEST MENU" ne sont pas les même dans le "COUNTRY SELECT".
{.is-warning}

Fonctions des différentes touches dans le "COUNTRY SELECT".

| Fonctionnalités | Raccourci |
| :---: | :---: |
| Changer la valeur | **R3** |
| Valider | **L3** |

* Vous vous retrouvez dans "COUNTRY SELECT MODE".
  * Changez la valeur jusqu'à avoir le pays voulue (Japan, USA, Australia, Export).
* Validez.

## Ski Champ

Fonctions des différentes touches dans le "TEST MENU"

| Fonctionnalités | Raccourci |
| :---: | :---: |
| Descendre la flèche dans le menu | **R3** |
| Valider / Changer la valeur | **L3** |

* Entrez cette succession de touches : `Y`, `A`, `Y`, `A`, `R3`, `R3`.

>Les fonctions des différentes touches dans le "TEST MENU" ne sont pas les même dans le "COUNTRY SELECT".
{.is-warning}

Fonctions des différentes touches dans le "COUNTRY SELECT".

| Fonctionnalités | Raccourci |
| :---: | :---: |
| Changer la valeur | **R3** |
| Valider | **L3** |

* Vous vous retrouvez dans "COUNTRY SELECT MODE".
  * Changez la valeur jusqu'à avoir le pays voulue (Japan, USA, Export, Korea, Australia).
* Validez.