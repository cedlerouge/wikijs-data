---
title: TRS-80 Color Computer
description: 
published: true
date: 2021-09-09T17:31:29.440Z
tags: 7.3+, color, trs-80, computer, coco
editor: markdown
dateCreated: 2021-08-29T17:05:37.624Z
---

Vous trouverez ici quelques informations afin de profiter au maximum du TRS-80 Color Computer.

Voici les tutoriels disponibles :

[FAQ](faq)