---
title: Changer de face de disquette
description: 
published: true
date: 2021-09-05T12:15:57.199Z
tags: system, famicom, disk, face
editor: markdown
dateCreated: 2021-07-30T18:43:59.189Z
---

## Introduction

Les jeux sont écrits des 2 côtés d'une disquette, et vous devez la retourner pour pouvoir lire l'autre face.

## Utilisation

Son utilisation dépend de l'émulateur que vous utilisez.

### Libretro FBNeo

Les boutons L3 et R3 permettent pour l'un d'éjecter le disque (L3) et pour l'autre de changer la face et lire la nouvelle face (R3). Il vous faut donc effectuer la manipulation `L3` + `R3` à la suite pour changer la face.

### Les autres émulateurs du système

Les boutons L1 et R1 permettent pour l'une d'éjecter le disque (R1) et pour l'autre de changer la face (L1). Donc il faut faire la combinaison `R1` + `L1` + `R1` (éjecter + retourner + lire) à la suite pour changer la face.