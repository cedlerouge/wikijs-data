---
title: 32X
description: 
published: true
date: 2021-09-04T23:13:43.277Z
tags: 32x, sega
editor: markdown
dateCreated: 2021-07-30T11:03:58.867Z
---

Vous pouvez trouver ici des tutoriels concernant le périphérique 32X pour Megadrive.

Voici les tutoriels disponibles :

[Compatibilité des jeux](games-compatibility)