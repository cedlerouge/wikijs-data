---
title: Jouer aux jeux Singe
description: 
published: true
date: 2021-09-04T23:29:56.276Z
tags: 7.3+, daphne, singe
editor: markdown
dateCreated: 2021-07-06T18:20:32.455Z
---

## Qu'est-ce que les jeux Singe ?

Le terme `SINGE` (provenant du nom du dragon de Dragon's Lair) est le nom d'une extension pour l'émulateur Daphné qui permet à n'importe qui de créer ses propres Jeux Vidéos Animés (FMV: full motion vidéo).

Il permet de faire fonctionner les célèbres jeux American Laser Games (ALD) qui ont fait fureur dans les salles d'arcade en leur temps. Il permet de faire fonctionner les jeux WoW Action Max ainsi que certains autres jeux Arcade.

## Liste des jeux Singe

Voici la liste des jeux Singe ALD :

* Crime Patrol
* Drug Wars
* Mad Dog McCree
* Mad Dog McCree v2.0
* The Last Bounty Hunter
* Time Traveler v2.0
* Who Shot Johnny Rock

Voici la liste des jeux Singe WoW Action Max :

* 38 Ambush Alley
* Blue Thunder
* HydroSub 2021
* Pops Ghostly
* Sonic Fury

Voici la liste des jeux Singe Arcade :

* Ninja Hayate
* TimeGal
* Time Traveler

>Les jeux Singe WoW ActionMax peuvent se jouer sur PC uniquement pour le moment.
{.is-info}

## Installation des jeux

Chaque jeu se sépare en 2 parties :

* Les données du jeu (vidéos, musiques, etc.)
* Les données pour faire fonctionner le jeu (cdrom)

### Pour les données du jeu

Le contenu suivant est requis :

* Tout fichier en `.WAV`
* Tout fichier en `.PNG`
* Tout fichier en `.TTF`
* Tout fichier en `.SINGE` autre que celui nommé `nomdujeu.singe`
* `nomdujeu.cfg`

Vous devez les placer dans le répertoire suivant :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 singe
┃ ┃ ┃ ┃ ┃ ┣ 📁 ALG
┃ ┃ ┃ ┃ ┃ ┃ ┣ 📁 nomdujeu
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 fichier1.wav
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 fichier2.wav
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 fichier3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 fichier4.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 ...

### Pour les données pour faire fonctionner le jeu

Le contenu suivant est requis :

* Fichier `nomdujeu.singe`
* Fichier `nomdujeu.txt`
* Répertoire `cdrom` avec le contenu intact.

Vous devez les placer dans le répertoire suivant :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 nomdujeu.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomdujeu.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomdujeu.txt
┃ ┃ ┃ ┃ ┃ ┣ 📁 cdrom
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 fichier1.dat
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 fichier2.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 fichier3.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 ...

## Emplacement des fichiers par jeu

Comme il est particulièrement difficile de placer les bons fichiers aux bons endroits, vous allez voir ci-dessous comment mettre les fichiers nécessaires de chaque jeu dans l'arborescence de Recalbox.

## {.tabset}

### American Laser Games

#### Crime Patrol

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 crimepatrol.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 crimepatrol.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 crimepatrol.txt
┃ ┃ ┃ ┃ ┃ ┣ 📁 cdrom
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 a_cpintro.dat
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 a_cpintro.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 a_cpintro.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 b_airterrorist.dat
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 ...
┃ ┃ ┃ ┃ ┣ 📁 singe
┃ ┃ ┃ ┃ ┃ ┣ 📁 ALG
┃ ┃ ┃ ┃ ┃ ┃ ┣ 📁 crimepatrol
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 addlg___.ttf
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 badge3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 badge4.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 break.wav
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 ...

#### Drug Wars

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 drugwars.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 drugwars.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 drugwars.txt
┃ ┃ ┃ ┃ ┃ ┣ 📁 cdrom
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 a_dwintro.dat
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 a_dwintro.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 a_dwintro.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 a_partner01.dat
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 etc.
┃ ┃ ┃ ┃ ┣ 📁 singe
┃ ┃ ┃ ┃ ┃ ┣ 📁 ALG
┃ ┃ ┃ ┃ ┃ ┃ ┣ 📁 drugwars
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 addlg___.ttf
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 badge3.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 badge4.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 break.wav
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 ...

#### Mad Dog McCree

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 maddog.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 maddog.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 maddog.txt
┃ ┃ ┃ ┃ ┃ ┣ 📁 cdrom
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 a_bank.dat
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 a_bank.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 a_bank.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 a_barrel-front.dat
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 ...
┃ ┃ ┃ ┃ ┣ 📁 singe
┃ ┃ ┃ ┃ ┃ ┣ 📁 ALG
┃ ┃ ┃ ┃ ┃ ┃ ┣ 📁 maddog
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arcadecoin.wav
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bottlebreak.wav
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bullet.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 choice.easy.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 ...

#### Mad Dog McCree 2

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 maddog2.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 maddog2.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 maddog2.txt
┃ ┃ ┃ ┃ ┃ ┣ 📁 cdrom
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 a_trailer.dat
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 a_trailer.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 a_trailer.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 b_aintro.dat
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 ...
┃ ┃ ┃ ┃ ┣ 📁 singe
┃ ┃ ┃ ┃ ┃ ┣ 📁 ALG
┃ ┃ ┃ ┃ ┃ ┃ ┣ 📁 maddog2
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arcadecoin.wav
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bottlebreak.wav
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bullet.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 cdrom-globals.singe
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 ...

#### Space Pirates

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 spacepirates.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 spacepirates.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 spacepirates.txt
┃ ┃ ┃ ┃ ┃ ┣ 📁 cdrom
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 a_trailer.dat
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 a_trailer.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 a_trailer.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 b_dc01.dat
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 ...
┃ ┃ ┃ ┃ ┣ 📁 singe
┃ ┃ ┃ ┃ ┃ ┣ 📁 ALG
┃ ┃ ┃ ┃ ┃ ┃ ┣ 📁 spacepirates
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 ca.ttf
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 cdrom-globals.singe
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 cdrom-setuplevel.singe
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 cdrom-shipbattle.singe
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 ...

#### The Last Bounty Hunter

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 lastbountyhunter.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 lastbountyhunter.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 lastbountyhunter.txt
┃ ┃ ┃ ┃ ┃ ┣ 📁 cdrom
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 a_trailer.dat
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 a_trailer.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 a_trailer.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 b_armybase.dat
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 ...
┃ ┃ ┃ ┃ ┣ 📁 singe
┃ ┃ ┃ ┃ ┃ ┣ 📁 ALG
┃ ┃ ┃ ┃ ┃ ┃ ┣ 📁 lastbountyhunter
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bottlebreak.wav
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 bullet2.wav
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 captured.png
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 cdrom-globals.singe
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 ...

#### Who Shot Johnny Hunter

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 johnnyrock.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 johnnyrock.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 johnnyrock.txt
┃ ┃ ┃ ┃ ┃ ┣ 📁 cdrom
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 a_intro.dat
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 a_intro.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 a_intro.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 a_start.dat
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 ...
┃ ┃ ┃ ┃ ┣ 📁 singe
┃ ┃ ┃ ┃ ┃ ┣ 📁 ALG
┃ ┃ ┃ ┃ ┃ ┃ ┣ 📁 johnnyrock
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 anchorsteamnf.ttf
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 cashregister.wav
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 cdrom-globals.singe
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 cdrom-hitbox-cans.singe
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 ...

### WoW Action Max

#### 38 Ambush Alley

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 singe
┃ ┃ ┃ ┃ ┃ ┣ 📁 ActionMax
┃ ┃ ┃ ┃ ┃ ┃ ┣ 📁 38AmbushAlley.daphne
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 38AmbushAlley.singe
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 38AmbushAlley.txt
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 video_38AmbushAlley.dat
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 video_38AmbushAlley.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 video_38AmbushAlley.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 Emulator.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 font_BlueStone.ttf
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 font_chemrea.ttf
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 font_LED_Real.ttf
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_ActionMax.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_ASteadyAimIsCritical.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_BadHit.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_GameOver.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_GetReadyForAction.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_GoodHit.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_Gunshot.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sprite_38AmbushAlley.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sprite_ActionMax.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sprite_Bullet.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sprite_Crosshair.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sprite_LightOff.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sprite_LightOn.png

>Le fichier `38AmbushAlley.txt` peut se nommer `frame_38AmbushAlley.txt`, ne pas hésiter à le renommer.
{.is-info}

#### Blue Thunder

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 singe
┃ ┃ ┃ ┃ ┃ ┣ 📁 ActionMax
┃ ┃ ┃ ┃ ┃ ┃ ┣ 📁 BlueThunder.daphne
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 BlueThunder.singe
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 BlueThunder.txt
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 video_BlueThunder.dat
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 video_BlueThunder.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 video_BlueThunder.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 Emulator.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 font_BlueStone.ttf
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 font_chemrea.ttf
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 font_LED_Real.ttf
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_ActionMax.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_ASteadyAimIsCritical.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_BadHit.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_GameOver.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_GetReadyForAction.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_GoodHit.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_Gunshot.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sprite_ActionMax.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sprite_BlueThunder.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sprite_Bullet.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sprite_Crosshair.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sprite_LightOff.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sprite_LightOn.png

>Le fichier `BlueThunder.txt` peut se nommer `frame_BlueThunder.txt`, ne pas hésiter à le renommer.
{.is-info}

#### HydroSub 2021

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 singe
┃ ┃ ┃ ┃ ┃ ┣ 📁 ActionMax
┃ ┃ ┃ ┃ ┃ ┃ ┣ 📁 HydroSub2021.daphne
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 HydroSub2021.singe
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 HydroSub2021.txt
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 video_HydroSub2021.dat
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 video_HydroSub2021.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 video_HydroSub2021.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 Emulator.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 font_BlueStone.ttf
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 font_chemrea.ttf
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 font_LED_Real.ttf
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_ActionMax.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_ASteadyAimIsCritical.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_BadHit.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_GameOver.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_GetReadyForAction.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_GoodHit.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_Gunshot.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sprite_ActionMax.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sprite_Bullet.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sprite_Crosshair.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sprite_HydroSub2021.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sprite_LightOff.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sprite_LightOn.png

>Le fichier `HydroSub2021.txt` peut se nommer `frame_HydroSub2021.txt`, ne pas hésiter à le renommer.
{.is-info}

#### Pops Ghostly

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 singe
┃ ┃ ┃ ┃ ┃ ┣ 📁 ActionMax
┃ ┃ ┃ ┃ ┃ ┃ ┣ 📁 PopsGhostly.daphne
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 PopsGhostly.singe
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 PopsGhostly.txt
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 video_PopsGhostly.dat
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 video_PopsGhostly.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 video_PopsGhostly.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 Emulator.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 font_BlueStone.ttf
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 font_chemrea.ttf
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 font_LED_Real.ttf
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_ActionMax.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_ASteadyAimIsCritical.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_BadHit.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_GameOver.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_GetReadyForAction.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_GoodHit.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_Gunshot.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sprite_ActionMax.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sprite_Bullet.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sprite_Crosshair.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sprite_LightOff.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sprite_LightOn.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sprite_PopsGhostly.png

>Le fichier `PopsGhostly.txt` peut se nommer `frame_PopsGhostly.txt`, ne pas hésiter à le renommer.
{.is-info}

#### Sonic Fury

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 singe
┃ ┃ ┃ ┃ ┃ ┣ 📁 ActionMax
┃ ┃ ┃ ┃ ┃ ┃ ┣ 📁 SonicFury.daphne
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 SonicFury.singe
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 SonicFury.txt
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 video_SonicFury.dat
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 video_SonicFury.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 video_SonicFury.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 Emulator.singe
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 font_BlueStone.ttf
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 font_chemrea.ttf
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 font_LED_Real.ttf
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_ActionMax.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_ASteadyAimIsCritical.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_BadHit.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_GameOver.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_GetReadyForAction.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_GoodHit.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sound_Gunshot.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sprite_ActionMax.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sprite_Bullet.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sprite_Crosshair.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sprite_LightOff.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sprite_LightOn.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 sprite_SonicFury.png

>Le fichier `SonicFury.txt` peut se nommer `frame_SonicFury.txt`, ne pas hésiter à le renommer.
{.is-info}

### Arcade

#### Ninja Hayate

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 hayate.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 hayate.dat
┃ ┃ ┃ ┃ ┃ ┣ 🗒 hayate.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 hayate.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 hayate.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 hayate.txt
┃ ┃ ┃ ┃ ┣ 📁 singe
┃ ┃ ┃ ┃ ┃ ┣ 📁 hayate
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 1up.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 ...

#### TimeGal

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 timegal.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 timegal.dat
┃ ┃ ┃ ┃ ┃ ┣ 🗒 timegal.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 timegal.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 timegal.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 timegal.txt
┃ ┃ ┃ ┃ ┣ 📁 singe
┃ ┃ ┃ ┃ ┃ ┣ 📁 timegal
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 ...

#### Time Traveler

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 timetraveler.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 timetraveler.singe
┃ ┃ ┃ ┃ ┃ ┣ 🗒 timetraveler.txt
┃ ┃ ┃ ┃ ┃ ┣ 📁 dvd
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 vts_01_1.dat
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 vts_01_1.m2v
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 vts_01_1.ogg
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 vts_02_1.dat
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 etc.
┃ ┃ ┃ ┃ ┣ 📁 singe
┃ ┃ ┃ ┃ ┃ ┣ 📁 timetraveler
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 action.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowdown.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowleft.png
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 arrowright.wav
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 ...

>Le fichier `timetraveler.singe` peut se nommer `dvd-timetraveler.singe`, ne pas hésiter à le renommer.
{.is-info}
