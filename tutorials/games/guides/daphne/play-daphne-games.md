---
title: Jouer aux jeux Daphné
description: 
published: true
date: 2021-09-04T23:29:31.641Z
tags: daphne, jeux
editor: markdown
dateCreated: 2021-08-08T15:07:15.471Z
---

## Liste des jeux Daphné

Les jeux Daphné sont les jeux qui n'utilisent pas d'extension tel que Singe.

Voici la liste des jeux Daphné :

* Astron Belt
* Badlands
* Bega's Battle
* Cliff Hanger
* Cobra Command
* Dragon's Lair
* Dragon's Lair II : Time Warp
* Esh's Aurunmilla
* Galaxy Ranger
* GP World
* M.A.C.H. 3
* Road Blaster
* Space Ace
* Super Don Quix-Ote
* Thayer's Quest
* Us Vs Them

## Installation des jeux

Chaque jeu se sépare en 2 parties :

* Les données pour faire fonctionner le jeu (vidéos, musiques, etc.)
* Les données du jeu (fichier zip)

### Pour les données du jeu

Les données du jeu sont, pour chacun, contenu dans un fichier `.ZIP` avec des extensions spécifiques. Ces fichiers ne sont pas à ouvrir et doivent avoir le même nom que le dossier `nomdujeu.daphne`.

Vous devez les placer dans le répertoire suivant :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomdujeu.zip

### Pour les données pour faire fonctionner le jeu

Le contenu suivant est requis :

* 1 fichier `nomdujeu.commands` (facultatif)
* 1 fichier `nomdujeu.dat` (facultatif)
* 1 ou plusieurs fichiers `nomdujeu.m2v`
* 1 ou plusieurs fichiers `nomdujeu.m2v.bf` (facultatif)
* 1 ou plusieurs fichiers `nomdujeu.ogg`
* 1 ou plusieurs fichiers `nomdujeu.ogg.bf` (facultatif)
* 1 fichier `nomdujeu.txt`

Vous devez les placer dans le répertoire suivant :

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 nomdujeu.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomdujeu.commands
┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomdujeu.dat
┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomdujeu.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomdujeu.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 nomdujeu.txt
┃ ┃ ┃ ┃ ┃ ┣ 🗒 ...

>Certains noms de fichiers peuvent ne pas avoir les mêmes noms que le jeu comme les fichiers `.DAT`, `.M2V` et `.OGG`. Ceci n'est pas un problème en soi, du moment que les fichier `.M2V` sont correctement répertoriées dans le fichier `.TXT`, je jeu devrait fonctionner sans problème.
{.is-info}

## Emplacement des fichiers par jeu

Comme il est particulièrement difficile de placer les bons fichiers aux bons endroits, vous allez voir ci-dessous comment mettre les fichiers nécessaires de chaque jeu dans l'arborescence de Recalbox.

## {.tabset}
### Astron Best

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 astron.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 astron.commands
┃ ┃ ┃ ┃ ┃ ┣ 🗒 astron.txt
┃ ┃ ┃ ┃ ┃ ┣ 🗒 astron512.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 astron512.ogg
┃ ┃ ┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┃ ┃ ┣ 🗒 astron.zip

### Badlands

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 badlands.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 badlands-r.dat
┃ ┃ ┃ ┃ ┃ ┣ 🗒 badlands-r.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 badlands-r.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 badlands.commands
┃ ┃ ┃ ┃ ┃ ┣ 🗒 badlands.txt
┃ ┃ ┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┃ ┃ ┣ 🗒 badlands.zip

### Bega's Battle

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 bega.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 bega.commands
┃ ┃ ┃ ┃ ┃ ┣ 🗒 bega.dat
┃ ┃ ┃ ┃ ┃ ┣ 🗒 bega.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 bega.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 bega.txt
┃ ┃ ┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┃ ┃ ┣ 🗒 bega.zip

### Cliff Hanger

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 .daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 .commands
┃ ┃ ┃ ┃ ┃ ┣ 🗒 CH_640x480_24p.dat
┃ ┃ ┃ ┃ ┃ ┣ 🗒 CH_640x480_24p.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 CH_640x480_24p.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 cliff.txt
┃ ┃ ┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┃ ┃ ┣ 🗒 cliff.zip

### Cobra Command

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 cobra.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 cc.dat
┃ ┃ ┃ ┃ ┃ ┣ 🗒 cc.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 cc.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 cobra.commands
┃ ┃ ┃ ┃ ┃ ┣ 🗒 cobra.txt
┃ ┃ ┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┃ ┃ ┣ 🗒 cobra.zip

### Dragon's Lair

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 lair.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 lair.commands
┃ ┃ ┃ ┃ ┃ ┣ 🗒 lair.dat
┃ ┃ ┃ ┃ ┃ ┣ 🗒 lair.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 lair.m2v.bf
┃ ┃ ┃ ┃ ┃ ┣ 🗒 lair.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 lair.ogg.bf
┃ ┃ ┃ ┃ ┃ ┣ 🗒 lair.txt
┃ ┃ ┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┃ ┃ ┣ 🗒 lair.zip

### Dragon's Lair II : Time Warp

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 lair2.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 dl2-00001.dat
┃ ┃ ┃ ┃ ┃ ┣ 🗒 dl2-00001.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 dl2-00001.m2v.bf
┃ ┃ ┃ ┃ ┃ ┣ 🗒 dl2-00001.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 dl2-00001.ogg.bf
┃ ┃ ┃ ┃ ┃ ┣ 🗒 ...
┃ ┃ ┃ ┃ ┃ ┣ 🗒 dl2-44550.dat
┃ ┃ ┃ ┃ ┃ ┣ 🗒 dl2-44550.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 dl2-44550.m2v.bf
┃ ┃ ┃ ┃ ┃ ┣ 🗒 dl2-44550.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 dl2-44550.ogg.bf
┃ ┃ ┃ ┃ ┃ ┣ 🗒 lair2.commands
┃ ┃ ┃ ┃ ┃ ┣ 🗒 lair2.txt
┃ ┃ ┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┃ ┃ ┣ 🗒 lair2.zip

### Esh's Aurunmilla

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 esh.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 esh.dat
┃ ┃ ┃ ┃ ┃ ┣ 🗒 esh.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 esh.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 esh.txt
┃ ┃ ┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┃ ┃ ┣ 🗒 esh.zip

### Galaxy Ranger

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 galaxy.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 galaxy.dat
┃ ┃ ┃ ┃ ┃ ┣ 🗒 galaxy.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 galaxy.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 galaxy.txt
┃ ┃ ┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┃ ┃ ┣ 🗒 galaxy.zip

### GP World

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 gpworld.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 gpdiags.dat
┃ ┃ ┃ ┃ ┃ ┣ 🗒 gpdiags.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 gpdiags.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 gpworld.dat
┃ ┃ ┃ ┃ ┃ ┣ 🗒 gpworld.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 gpworld.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 gpworld.txt
┃ ┃ ┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┃ ┃ ┣ 🗒 gpworld.zip

### M.A.C.H. 3

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 mach3.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 mach3.commands
┃ ┃ ┃ ┃ ┃ ┣ 🗒 mach3.dat
┃ ┃ ┃ ┃ ┃ ┣ 🗒 mach3.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 mach3.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 mach3.txt
┃ ┃ ┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┃ ┃ ┣ 🗒 mach3.zip

### Road Blaster

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 roadblaster.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 rb.d2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 rb.dat
┃ ┃ ┃ ┃ ┃ ┣ 🗒 rb.demuxed.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 rb.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 rb.mpeg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 rb.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 roadblater.txt
┃ ┃ ┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┃ ┃ ┣ 🗒 roadblaster.zip

### Space Ace

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 ace.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 ace.commands
┃ ┃ ┃ ┃ ┃ ┣ 🗒 ace.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 ace.m2v.bf
┃ ┃ ┃ ┃ ┃ ┣ 🗒 ace.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 ace.ogg.bf
┃ ┃ ┃ ┃ ┃ ┣ 🗒 ace.txt
┃ ┃ ┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┃ ┃ ┣ 🗒 ace.zip

### Super Don Quix-Ote

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 sdq.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sdq-blank.dat
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sdq-blank.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sdq-colorbars.dat
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sdq-colorbars.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sdq-crosshairs.dat
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sdq-crosshairs.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sdq.commands
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sdq.dat
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sdq.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sdq.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sdq.txt
┃ ┃ ┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┃ ┃ ┣ 🗒 sdq.zip

### Thayer's Quest

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 tq.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 tq.dat
┃ ┃ ┃ ┃ ┃ ┣ 🗒 tq.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 tq.m2v.bf
┃ ┃ ┃ ┃ ┃ ┣ 🗒 tq.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 tq.ogg.bf
┃ ┃ ┃ ┃ ┃ ┣ 🗒 tq.txt
┃ ┃ ┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┃ ┃ ┣ 🗒 tq.zip

### Us Vs Them

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 daphne
┃ ┃ ┃ ┃ ┣ 📁 uvt.daphne
┃ ┃ ┃ ┃ ┃ ┣ 🗒 uvt-170606.dat
┃ ┃ ┃ ┃ ┃ ┣ 🗒 uvt-170606.m2v
┃ ┃ ┃ ┃ ┃ ┣ 🗒 uvt-170606.ogg
┃ ┃ ┃ ┃ ┃ ┣ 🗒 uvt.commands
┃ ┃ ┃ ┃ ┃ ┣ 🗒 uvt.txt
┃ ┃ ┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┃ ┃ ┣ 🗒 uvt.zip