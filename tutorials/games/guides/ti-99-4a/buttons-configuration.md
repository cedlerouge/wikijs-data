---
title: Configuration des boutons
description: 
published: true
date: 2021-09-09T17:29:02.043Z
tags: configuration, boutons, ti-99/4a
editor: markdown
dateCreated: 2021-08-21T18:40:26.914Z
---

## Introduction

Les touches et autres boutons du TI-99/4A peuvent être difficiles à appréhender, ce tutoriel va vous indiquer quels sont les boutons à utiliser.

## Utilisation

Voici la correspondance des touches pour le clavier :

| Bouton de manette | Bouton du système |
| :--- | :--- |
| `L1` | 1 (sélection au boot) |
| `R1` | 2 (sélection au boot) |
| `X` | 3 (sélection au boot) |
| `Y` | 4 (sélection au boot) |
| `START` | `Entrée` |
| `B` | `Espace` |

Pour le joystick, votre stick gauche de manette fera le travail avec le bouton `A`.

>Un fichier `.P2K` est fourni de base avec l'émulateur.
{.is-success}