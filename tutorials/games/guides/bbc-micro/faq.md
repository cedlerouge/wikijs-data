---
title: FAQ
description: 
published: false
date: 2021-10-01T07:36:31.115Z
tags: 7.3+, faq, bbc, micro
editor: markdown
dateCreated: 2021-09-18T23:31:22.983Z
---

## Quelle est la version de l'émulateur BeebEm ?

Nous avons travaillé pour transposer la dernière version de BeebEm en SDL2 et amélioré pour répondre aux besoin de Recalbox.

## Quels systèmes sont pris en charge par BeebEm ?

Quatre systèmes sont pris en charge :

* BBC Micro
* B Plus
* Integra B
* Master128

## Existe-t-il des raccourcis manette pour BeebEm ?

Oui, voici la liste :

| Combinaison de boutons | Action |
| --- | --- |
| `Hotkey` + `Start` | Quitter |
| `Hotkey` + `X` | Affichage des leds du claviers et des périphériques (les icônes en bas, cassette, disquette, hdd, touches spéciales du clavier) |
| `Hotkey` + `Y` | Capture d'écran en résolution native |
| `Hotkey` + `B` | Redémarrage |
| `Hotkey` + `A` | Activation du lissage |
| `Hotkey` + `L1` / `Hotkey` + `R1` | Cycles cassettes / disquettes. À utiliser avec les fichiers `.M3U` pour changer les cassettes et les disquettes |
| `Hotkey` + `L2` | Pause |
| `Hotkey` + `R2` | Affichage des FPS |
| `Hotkey` + `L3` | Changement d'écran (couleur, noir et blanc, vert, ambre) |
| `Hotkey` + `R3` | Changer la protection des disquettes (les fichiers en lecture seule ou les zip restent en lecture seule) |

## Quel est le mapping du clavier ?

Le clavier est en QWERTY.

## Est-ce que les surcharges de configuration fonctionnent ?

Oui, sur 2 niveaux seulement : `<game>.<ext>.xxx.cfg` et `.xxx.cfg`.

## Les périphériques d'origine fonctionnent ?

Oui, comme le joystick et la souris. Le joystick est calqué sur le stick de gauche et les flèches directionnelles (d-pad) ainsi que les boutons A et B.

Les cartes d'extensions "tube" sont aussi supportées, via les options du core ou par surcharge de configuration.

## Peut-on charger des disques durs ISE ou SCSI ?

Non, cela n'est pas possible pour le moment.

## Est-ce qu'il y a des périphériques ou parties du système non disponible ?

Oui comme le télétexte, le support econet, le débuggueur, le désassembleur ainsi que les ports série.

## Est-ce que certains contrôleurs de disquettes seulement sont supportées ?

Oui, seuls ces contrôleurs sont supportées :

* acorn1770
* opusddos
* watford

Ceux-ci sont à activer dans les options du core.

Pour le modèle B Plus, le contrôleur de disquettes `acorn1770` est activé par défaut.

## Est-ce que les fichiers au format `.ZIP` sont supportées ?

Oui, pour les disquettes uniquement. Les formats cassette (.uef et .csw) sont déjà compressés, le zip fait perdre de la place.