---
title: FAQ
description: 
published: true
date: 2021-09-08T12:46:55.330Z
tags: faq, neo-geo cd
editor: markdown
dateCreated: 2021-08-01T10:57:02.072Z
---

## Remarques concernant le format des jeux

### Version "normale" et "cdz pached"

FBNeo prend les 2 formats, mais d'autres émulateurs Neogeo cd ne prennent que les versions patchés cdz. Il vaut peut-être mieux ne garder que celles-là.

### Format Redump

Les fichiers Redump sont au format `.CUE` avec plusieurs fichiers `.BIN` appelés **multibin**. Ce core n'accepte pas le multibin et vous devez convertir vos jeux pour avoir un seul fichier bin.

## Conversion

Vous pouvez le faire avec le logiciel **CDmage** 1.02.1 :

* **File** &gt; **Open** &gt; Sélectionnez votre jeu au format .CUE
* **File** &gt; **Save As** &gt; Indiquez le nouveau nom de votre jeu pour ne pas écraser l'existant et cliquez sur `Enregistrer`
* Assurez-vous avoir choisi « MODE1/2352 » dans la liste déroulante de droite de la nouvelle fenêtre et cliquez sur `OK`

Sources :

* [https://neo-source.com/index.php?topic=2487.msg26465#msg26465](https://neo-source.com/index.php?topic=2487.msg26465#msg26465)
* [https://github.com/libretro/FBNeo/blob/master/src/burner/libretro/README.md#neogeo-cd-doesnt-work-why-](https://github.com/libretro/FBNeo/blob/master/src/burner/libretro/README.md#neogeo-cd-doesnt-work-why-)