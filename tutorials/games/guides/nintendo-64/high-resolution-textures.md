---
title: Textures haute-résolution
description: 
published: true
date: 2021-09-08T12:49:23.618Z
tags: n64, texture, pack
editor: markdown
dateCreated: 2021-06-02T22:18:08.975Z
---

## Qu'est ce que sont les textures haute-résolution ?

Les textures haute-résolution (Hi-res) sont des packages crée par les fans et sont destinés à embellir les textures par défaut d'un jeu.

**Rice**, développeur du plugin **Rice Vidéo**, fut le premier qui rendit possible d'extraire les textures d'un jeu, de les modifier et des les ré-importer dans une résolution plus haute. Maintenant, il est possible de remplacer les textures floues dans les jeux par des versions haute définition ou par une version complètement différente. Emballé par cette nouvelle possibilité, de nombreux graphistes ont commencé à créer des textures de substitution pour leurs jeux préférés.

>Lorsque vous téléchargez un pack de textures haute-résolution (Google est votre ami), regardez dans le fichier readme la description, les messages du forum ou tout autre élément. Beaucoup de ces packs ne fonctionnent qu'avec un fichier rom spécifique. Par exemple, la rom (U) fonctionne, la rom (E) ne fonctionne pas. Avant de commencer, vous devez le savoir.
{.is-warning}

## Configuration

### Copie des textures haute résolution dans votre Recalbox

Placez le dossier des textures haute résolution de chacun de vos jeux dans le répertoire suivant :

```text
/recalbox/share/bios/Mupen64plus/hires_texture/nom_du_jeu/
```

### Activation de l'émulateur Libretro Mupen64Plus_Next :

Il vous faut indiquer au jeu concerné de le démarrer avec le bon émulateur.

* Après le redémarrage, allez dans EmulationStation sur votre jeu et appuyez sur `START`.
* Maintenant, allez dans `MODIFIER LE JEU` > `LANCER AVEC`.
* Changez l'émulateur pour `LIBRETRO MUPEN64PLUS_NEXT`
* Fermez les menus.

### Activation des textures haute-résolution

Il vous reste maintenant à activer les textures haute-résolution dans l'émulateur.

* Lancez un jeu.
* Quand le jeu démarre, allez dans le menu de RetroArch en faisant `Hotkey + B.`
* Faites « Retour » 2 fois puis allez dans `Réglages` > `Configuration`.

![](/tutorials/games/guides/nintendo-64/hires-textures/hirestextures1.png){.full-width}

* Activez le paramètre `Sauvegarder la configuration en quittant`.

![](/tutorials/games/guides/nintendo-64/hires-textures/hirestextures2.png){.full-width}

* Faires « Retour » 2 fois et allez dans `Menu principal` > `Menu rapide` > `Options`.

![](/tutorials/games/guides/nintendo-64/hires-textures/hirestextures3.png){.full-width}

* Activez le paramètre `Use High-Res textures`.

![](/tutorials/games/guides/nintendo-64/hires-textures/hirestextures4.png){.full-width}

* Faires « Retour » 2 fois et allez  dans `Reprendre`.
* Quittez le jeu et démarrez le jeu qui doit charger les textures haute-résolution.

Quand le pack de textures haute résolution et la rom seront les bons, le jeu démarrera avec le pack de textures haute résolution.

## Dépannage

### Vous avez bien suivi les instructions ci-dessus et cela ne fonctionne toujours pas ?

Peut-être est-ce un problème de nom...  
Beaucoup de ces packs de textures sont fourni avec le bon nom de dossier mais pas tous ! Le nom de dossier doit être exactement le nom interne de votre rom.

>Le nom de fichier est `Mario Kart 64 (USA).v64` mais le nom interne de la rom est `MARIOKART64`
{.is-warning}

### Comment connaitre le nom interne de ma rom ?

Sur Windows, vous pouvez utilisez par **Tool64** (Google est aussi votre ami ici).

* Démarrez **Tool64**;
* `File` -> `Open`
* Sélectionnez votre dossier de rom.

![](/tutorials/games/guides/nintendo-64/hires-textures/hirestextures5.png){.full-width}

* Faites un clic droit sur une rom puis cliquez sur `Rom Properties…`

![](/tutorials/games/guides/nintendo-64/hires-textures/hirestextures6.png){.full-width}

Regardez la ligne `Rom Name` pour avoir le nom interne de la rom.