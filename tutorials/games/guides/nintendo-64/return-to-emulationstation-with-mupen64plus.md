---
title: Retour dans EmulationStation avec mupen64plus
description: 
published: true
date: 2021-09-08T12:48:56.813Z
tags: emulationstation, mupen64plus
editor: markdown
dateCreated: 2021-05-21T08:47:30.464Z
---

## Explications

Mupen64plus n'est pas auto-configuré par Recalbox comme les autres émulateurs. Vous devez donc le faire manuellement.

Le mode vidéo de la N64 est défini dans votre fichier [recalbox.conf](./../../../../basic-usage/getting-started/recalboxconf-file).

Par défaut, nous utilisons le mode vidéo : `n64.videomode=DMT 4 HDMI`. Ce mode affiche une résolution de **640x480**. Nous avons défini cette résolution par défaut, car le Raspberry Pi 2 ne peut pas émuler le N64 à une résolution supérieure avec un bon taux de rafraîchissement. Ainsi, **640x480** est la résolution maximale à utiliser sur votre Recalbox.

Si votre écran ne peut pas changer de résolution (par exemple avec l'écran tactile officiel Pi), vous aurez des bordures noires autour de l'image car elle ne sera pas étirée pour s'adapter à l'écran.

## Instructions

Dans ce cas, vous devez faire ces 2 étapes :

* Définissez la même ligne dans `recalbox.conf` par ceci:

```ini
n64.videomode=default
```

* Modifiez le fichier `/recalbox/share/system/configs/mupen64/mupen64plus.cfg` pour changer la résolution de sortie à celle native de votre écran (ici **800x480**) :

```ini
[Video-General]
# Use fullscreen mode if True, or windowed mode if False
Fullscreen = False
# Width of output window or fullscreen width
ScreenWidth = 800
# Height of output window or fullscreen height
ScreenHeight = 480
```

Mais tous les écrans n'ont pas la même compatibilité de mode vidéo. Ainsi, lorsque vous essayez de démarrer une partie de N64 avec un core mupen64plus, vous pouvez avoir un écran noir avec un retour à EmulationStation. Dans ce cas, vous devez déterminer quels modes vidéo sont compatibles avec votre propre écran.
  
* Connectez-vous via [SSH](./../../../../tutorials/system/access/root-access-terminal-cli), puis tapez ces 2 commandes séparément :

`tvservice -m DMT`
`tvservice -m CEA`

* Les commandes renverront quelque chose comme ça :

```shell
[root@RECALBOX ~]# tvservice -m DMT
Group DMT has 16 modes:
           mode 4: 640x480 @ 60Hz 4:3, clock:25MHz progressive 
           mode 5: 640x480 @ 72Hz 4:3, clock:31MHz progressive 
           mode 6: 640x480 @ 75Hz 4:3, clock:31MHz progressive 
           mode 8: 800x600 @ 56Hz 4:3, clock:36MHz progressive 
           mode 9: 800x600 @ 60Hz 4:3, clock:40MHz progressive 
           mode 10: 800x600 @ 72Hz 4:3, clock:50MHz progressive 
           mode 11: 800x600 @ 75Hz 4:3, clock:49MHz progressive 
           mode 16: 1024x768 @ 60Hz 4:3, clock:65MHz progressive 
           mode 17: 1024x768 @ 70Hz 4:3, clock:75MHz progressive 
           mode 18: 1024x768 @ 75Hz 4:3, clock:78MHz progressive 
           mode 21: 1152x864 @ 75Hz 4:3, clock:108MHz progressive 
           mode 32: 1280x960 @ 60Hz 4:3, clock:108MHz progressive 
           mode 35: 1280x1024 @ 60Hz 5:4, clock:108MHz progressive 
           mode 36: 1280x1024 @ 75Hz 5:4, clock:135MHz progressive 
  (prefer) mode 57: 1680x1050 @ 60Hz 16:10, clock:119MHz progressive 
           mode 58: 1680x1050 @ 60Hz 16:10, clock:146MHz progressive 
```

```shell
[root@RECALBOX ~]# tvservice -m CEA
Group CEA has 5 modes:
           mode 1: 640x480 @ 60Hz 4:3, clock:25MHz progressive 
           mode 2: 720x480 @ 60Hz 4:3, clock:27MHz progressive 
           mode 3: 720x480 @ 60Hz 16:9, clock:27MHz progressive 
  (native) mode 4: 1280x720 @ 60Hz 16:9, clock:74MHz progressive 
           mode 16: 1920x1080 @ 60Hz 16:9, clock:148MHz progressive 
```

* Maintenant, nous disposons de tous les modes vidéo qui peuvent être utilisés sur votre écran. Il faut trouver un mode vidéo, avec une résolution égale ou inférieure à 640x480, puis définir ce mode dans le fichier [recalbox.conf.](./../../../../basic-usage/getting-started/recalboxconf-file).
* Si je choisis ce mode vidéo :

```shell
Group CEA has 5 modes:
           mode 1: 640x480 @ 60Hz 4:3, clock:25MHz progressive
```

* Le fichier [recalbox.conf](./../../../../basic-usage/getting-started/recalboxconf-file) doit être modifié comme suit :

  * mode vidéo par défaut :

```ini
n64.videomode=DMT 4 HDMI
```

  * nouveau mode vidéo :

```ini
n64.videomode=CEA 1 HDMI
```

## Avec un écran CRT

Si vous utilisez un écran CRT, vous devez modifier votre mode vidéo comme suit :  

```ini
n64.videomode=default
```