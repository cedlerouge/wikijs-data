---
title: Gestion des multi-disques au format .M3U
description: 
published: true
date: 2021-09-08T13:01:48.604Z
tags: ps1, m3u, gestion, multi-disques
editor: markdown
dateCreated: 2021-07-13T16:40:57.223Z
---

## Introduction

Plusieurs CD d'un même jeu peuvent être chargés simultanément depuis EmulationStation dans RetroArch en créant un fichier "liste de lecture" `.M3U` (texte brut `.TXT` avec une extension `.M3U`).

>Une liste de lecture `.M3U` peut être utilisé uniquement avec des jeux qui ont un fichier `.CUE` et un seul `.BIN` par disque.
>Tous jeu multi-disques qui a plusieurs fichiers `.BIN` pour un seul disque ne fonctionnera pas. Il est conseillé de convertir ces disques pour obtenir un fichier `.BIN` unique.
{.is-info}

## Utilisation

Nous utiliserons le jeu `Final Fantasy VII (France)` pour les exemples ci-dessous.

## {.tabset}

### Format de disque `.BIN` / `.CUE`

* Créez un nouveau fichier texte que l'on nomme `Final Fantasy VII (France).m3u` et qui contient les informations suivantes :

```text
Final Fantasy VII (France) (Disc 1).cue
Final Fantasy VII (France) (Disc 2).cue
Final Fantasy VII (France) (Disc 3).cue
```

### Format de disque `.CHD`

* Créez un nouveau fichier texte que l'on nomme `Final Fantasy VII (France).m3u` et qui contient les informations suivantes :

```text
Final Fantasy VII (France) (Disc 1).chd
Final Fantasy VII (France) (Disc 2).chd
Final Fantasy VII (France) (Disc 3).chd
```

## Changer de disque en cours de partie

Si l'on a besoin de changer de disque lorsque le jeu est lancé, il faut réaliser les actions suivantes :

* Ouvrir le tiroir CD "virtuel" : Touche `HK` + Joystick Gauche (Direction Haut)
* Changer de disque : Touche `HK` + Joystick Gauche (Direction Droite ou Gauche)
* Fermer le tiroir CD "virtuel" : Touche `HK` + Joystick Gauche (Direction Haut)