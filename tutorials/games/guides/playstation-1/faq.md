---
title: FAQ
description: 
published: true
date: 2021-09-18T23:10:56.308Z
tags: ps1, faq
editor: markdown
dateCreated: 2021-05-21T08:47:54.624Z
---

## Multi-disques

Tout d'abord, nous allons voir les différentes possibilités offertes par Recalbox pour changer de disque.

### Mode PSP

Regroupez tous les disques dans un seul fichier eboot.pbp. Vous pouvez changer de disque dans le menu Retroarch.

#### Avantages :

* Les raccourcis des contrôleurs peuvent être utilisés pour changer de disque.
* Il existe un fichier de sauvegarde pour tous les disques.

#### Inconvénients :

* Les fichiers .sbi ne peuvent pas être utilisés dans les jeux multi-disques.
* Ne permet pas de basculer vers un disque patché (avec la correction de protection LibCrypt).
* Cela prend un certain temps de changer tous les jeux multi-disques en eboot.pbp.

### Mode PCSX-reARMed

Le disque suivant est recherché dans le menu Retroarch par le système de fichiers.

#### Avantages :

* Vos jeux sont au format `.BIN` / `.CUE`.

#### Inconvénients :

* Cela ne fonctionne pas actuellement.
* C'est moche et lent.
* Les fichiers sauvegardés ne sont pas partagés entre les disques.
* Les raccourcis des contrôleurs ne peuvent pas être utilisés pour changer de disque.

Par conséquent, les jeux avec protection LibCrypt et multi-disque sont une vraie plaie dans ces deux modes.

### Mode définitif

Il suffit de créer un fichier `.M3U` pour chaque jeu multi-disque. Ce fichier contiendra les noms des fichiers `.CUE`. Par exemple :

![M3U](/tutorials/games/guides/playstation-1/m3u1.png)

Le dernier aspect devrait être comme suit :

![Dossier PSX](/tutorials/games/guides/playstation-1/m3u2.png)

Comme vous pouvez le voir, il y a des `.CUE` / `.BIN` / `.SBI` par disque et un `.M3U` par jeu multi-disque.

#### Avantages :

* Vos jeux sont au format `.BIN` / `.CUE`.
* Les raccourcis des MANETTES peuvent être utilisés pour changer de disque.
* Les fichiers `.SBI` peuvent être utilisés.
* Il existe un fichier de sauvegarde pour tous les disques.

#### Inconvénients :

* Vous devez modifier le fichier es_systems.cfg (une seule fois).
* Vous devez cacher les fichiers `.CUE` dans le menu Recalbox.

## Jeux multi-pistes

Certains jeux ont plusieurs pistes par disque. Cela signifie que le fichier `.CUE` nécessite plusieurs entrées (une par piste).

Exemple de `.CUE` multi-pistes :

![Cue à pistes multiples](/tutorials/games/guides/playstation-1/cue1.png)

Le dernier aspect devrait être le suivant :

![Jeu multi-pistes](/tutorials/games/guides/playstation-1/cue2.png)

## Extensions

### Extension `.bin` : cas particulier

Les fichiers roms avec l'extension `.bin` sont également supportés mais nécessitent un fichier `.cue` pour être démarrés.

### Extension `.cue` : comment ça marche ?

Un fichier `.cue` est, tout simplement, un fichier texte dont l'extension a été transformée de `.txt` à `.cue` . Il doit porter le même nom que le fichier `.bin` qu'il accompagne et il contient les informations nécessaires au lancement du fichier `.bin` .

### Extension `.m3u` : gérer les jeux multi-CD

Plusieurs CD d'un même jeu peuvent être chargés simultanément depuis EmulationStation dans RetroArch en créant un fichier "liste de lecture" `.m3u` (texte brut `.txt` avec une extension `.m3u`).

>Une liste de lecture .m3u peut être utilisé uniquement avec des jeux qui ont **un fichier `.cue` et un seul `.bin` par disque**.
>Tous jeu multi-disques qui a plusieurs fichiers `.bin` pour un seul disque ne fonctionnera pas (il est conseillé de convertir ces disques pour obtenir un fichier `.bin` unique).
{.is-warning}

## Les étapes pour gérer un jeu multi-CD

1. Renommer les fichiers `.cue` de chaque disque du jeu avec l'extension appropriée `.cd1`, `.cd2`, etc... afin que EmulationStation ne liste que les fichiers `.m3u`et non tous les disques individuels.
2. Créer un nouveau fichier texte que l'on nomme `Final Fantasy VII (France).m3u`

### Exemple avec Final Fantasy VII :

On renomme :

| Fichier à renommer | Fichier renommé |
| :---: | :---: |
| Final Fantasy VII (France) (Disc 1).cue | Final Fantasy VII (France).cd1 |
| Final Fantasy VII (France) (Disc 2).cue | Final Fantasy VII (France).cd2 |
| Final Fantasy VII (France) (Disc 3).cue | Final Fantasy VII (France).cd3 |

On crée le fichier `Final Fantasy (France).m3u` :

```
Final Fantasy VII (France) (Disc 1).cd1
Final Fantasy VII (France) (Disc 2).cd2
Final Fantasy VII (France) (Disc 3).cd3
```

### Exemple avec les jeux en `.chd`

 | Fichier à renommer | Fichier renommé |
 | :---: | :---: |
 | Final Fantasy VII (France) (Disc 1).chd | Final Fantasy VII (France).cd1 |
 | Final Fantasy VII (France) (Disc 2).chd | Final Fantasy VII (France).cd2 |
 | Final Fantasy VII (France) (Disc 3).chd | Final Fantasy VII (France).cd3 |
    
Le fichier `Final Fantasy VII (France).m3u` sera identique à l'exemple précédent.

Avec cette configuration, EmulationStation lancera automatiquement le CD1 à chaque lancement du jeu.

## Changer de disque en cours de partie

Si l'on a besoin de changer de disque lorsque le jeu est lancé, il faut réaliser les actions suivantes :

1. Ouvrir le tiroir CD "virtuel" : `Touche HK + Joystick Gauche (Direction Haut)`
2. Changer de disque : `Touche HK + Joystick Gauche (Direction Droite ou Gauche)`
3. Fermer le tiroir CD "virtuel" : `Touche HK + Joystick Gauche (Direction Haut)`

Ceci fonctionne si vous avez votre jeu milti-disques dans un fichier `.M3U`.

## Existe-t-il un menu pour les options de DuckStation ?

Oui, mais vous devez brancher un clavier et appuyer sur `F12` pour y accéder et utiliser les menus. Ces menus ne sont pas utilisables avec une manette pour le moment.

## Divers

### Que dois-je faire avec les fichiers `.ECM` et `.APE` ?

Vous devez les dézipper. Voir ce [guide](https://www.epforums.org/showthread.php?57757-ECM-And-APE-Guide) pour le faire.

### Comment puis-je activer le Dualshock (analogues) ?

* Dans le jeu, allez dans le menu RetroArch en faisant `Hotkey` + `B`.
* Sélectionnez `Touches` > `Port 1 Touches`.

![](/tutorials/games/guides/playstation-1/enable-analog/psxanalog1.png){.full-width}

* Sélectionnez `analog` dans l'option `Type de périphérique` (identique pour tous les pavés que vous voulez).

![](/tutorials/games/guides/playstation-1/enable-analog/psxanalog2.png){.full-width}

### Je n'ai pas le fichier `.cue`, comment je fais ?

 Si vous n'avez qu'un fichier `.bin`et aucun fichier .`cue`, vous pouvez le générer de plusieurs manière :

* En ligne : [http://nielsbuus.dk/pg/psx_cue_maker/](http://nielsbuus.dk/pg/psx_cue_maker/)
* Avec ce logiciel freeware : [CueMaker](https://github.com/thorst/CueMaker/releases/)

Vous pouvez aussi l'obtenir [auprès de Redump](http://redump.org/cues/psx/).

##  Glossaire

* .ape : fichier compressé pour le fichier .wav.
* .bin : données de jeu et pistes de musique.
* .ccd/.img/.sub : Clonage de fichiers CD. Vous devez les transformer en .cue/.bin (avec IsoBuster par exemple).
* .cue : fichier dans lequel les pistes du disque sont définies. Un par données .bin.
* .ecm : fichier compressé pour le fichier .bin.
* .pbp : Fichier PSP pour les jeux PSX.
* .ppf : fichier correctif pour les jeux avec protection LibCrypt.
* .sbi : fichier qui contient les informations de protection et qui sont nécessaires pour exécuter des jeux protégés dans des émulateurs. Ils doivent porter le même nom que le fichier .cue.
* .wav : fichier de piste musicale. Vous devez le renommer en .bin.

## Liens utiles

* [Guide ECM-APE](https://www.epforums.org/showthread.php?57757-ECM-And-APE-Guide)
* [Liste des jeux PSX](https://psxdatacenter.com/pal_list.html)
* [Fichiers .sbi](http://psxdatacenter.com/sbifiles.html)
* [Générateur de fichier .cue](http://nielsbuus.dk/pg/psx_cue_maker/)