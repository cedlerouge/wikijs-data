---
title: FAQ
description: 
published: true
date: 2021-09-04T23:16:58.572Z
tags: faq
editor: markdown
dateCreated: 2021-06-27T07:51:06.931Z
---

## Extensions

Voici une liste avec explications des différents formats de fichiers pourles systèmes Amiga :

* Disques _**AFD**_ (*.adf), le format de disque **le plus populaire**. **Ne peut pas intégrer** les protections de disque (**peut être zippé ou 7-zippé**). 
* Disques _**IPF**_ (*.ipf), plus généralement **utilisés par les copies de jeu sans intro** (_peut_ intégrer les protections de disque). **Peut être zippé.** 
* _**WHD**_ (dossiers, *.lha/lzh/lzx, *.zip), le format bien connu pour les jeux sur disque dur. Les dossiers WHD sont reconnus en utilisant le fichier *.slave à l'intérieur du dossier, mais il est _fortement_ recommandé de les garder empaquetés en lha ou en zip. Les WHD dépaquetés ne sont pas plus rapides et encore pire, ils peuvent rendre EmulationStation super lent. 
* _**HDD FS**_, "Hard drive on filesystem" (Disque dur sur système de fichier). Doit être un dossier finissant par l'extension ".hd". Rarement utilisé. Pour les vrais fans d'Amiga qui ont sauvegardé le disque dur de leur machine. Peut être zippé, mais ce n'est pas recommandé. 
* _**HDF**_ (*.hdf), Image disque dans un seul fichier. Peut être zippé (Lecture uniquement !) 
* _**RP9**_ (*.rp9), Les paquets tout-en-un d'Amiga Forever

Quand vous jouez à partir de fichiers zippés/7-zippés, notre configurateur essaye d'identifier le type de rom en cherchant des extensions spécifiques à l'intérieur de l'archive.

* Les extensions ADF/IPF/BIN/ISO sont rapides à identifier. 
* Le WHD est rapide, mais LHA devrait être l'option préférée. 
* Le HDDFS peut prendre plus longtemps à identifier et peut donner lieu à des interprétations fausses. Si vous utilisez le HDDFS, laissez-le en tant que fichiers et dossiers normaux et finissez le nom du dossier racine avec  `.hd` pour une identification facile.

## Fonctionnement

### **Jeux ADF**

**Copiez-les** dans votre dossier de **roms Amiga.**  
Les **disques multiples** sont **automatiquement** chargés, **jusqu'à 4** pour les jeux utilisant **la bonne nomenclature** "(Disk 1 of X)".

### **Jeux WHD**

**Les jeux WHD** sont **un peu plus difficiles** mais rien de trop compliqué.

* **Installation :**

Dézippez votre jeu WHDLoad, supprimez le fichier`.info` au même niveau que le dossier, et copiez seulement ce dossier vers votre dossier de roms (vous pouvez utiliser des sous-dossiers si vous le souhaitez -&gt; ex.`/recalbox/share/roms/amiga1200/ShootEmUp`).

* **Utilisation :**

Vous aurez besoin d'un ficher `.uae` au même niveau que le dossier du même nom, donc deux solutions :

* En créer un vide. 
* Utiliser un fichier customisé qui va vous permettre de modifier la configuration de l'émulateur (voir prochain paragraphe).

**Par défaut je recommande de d'abord essayer le dossier amiga1200 pour n'importe quel jeu WHDL. Si certains sont trop rapides, alors essayez-les dans le dossier amiga600.**

Vous pouvez utiliser le script suivant pour générer massivement des fichiers uae : [UaeGenerator](http://www89.zippyshare.com/v/80w859p5/file.html)

* **Modification des jeux WHDL :**

### Modification de fichiers .UAE

Dans le fichier uae, vous pouvez définir des blocs `;hardware`, `;controls` et `;graphics` pour remplacer la configuration standard de l'émulateur.

**Les parties customisées de la configuration dans le fichier .uae vont uniquement être utilisées si elles commencent avec le bon nom de bloc :** `;hardware`**,** `;controls` **ou** `;graphics`

Cela peut vous permettre d'utiliser deux joysticks par exemple, avoir une configuration matérielle très spécifique pour certains jeux qui sont un peu spéciaux, ou définir une résolution personnalisée.

Vous pouvez aussi tout supprimer dans le fichier uae et la configuration de base de l'émulateur sera alors utilisée.

Exemple de fichiers uae entièrement customisés pour le duo amiga600 et amiga1200 :  [UAEs Customisée](https://www100.zippyshare.com/v/1ttgYgks/file.html).

Copiez celui-ci (correspondant au modèle d'Amiga que vous souhaitez utiliser) à côté du dossier du jeu WHDL et renommez-le exactement comme le dossier du jeu (gardez l'extension `.uae` ) ex. : si votre jeu à pour nom de dossier `Rick Dangerous`, votre fichier uae devrait être nommé `Rick Dangerous.uae` et être au même niveau que le dossier.

### Modification de la séquence de démarrage (experts seulement)

La séquence de démarrage standard générée pour les jeux WHDL est`WHDload game.slave Preload` (dans `WHDL/S/Startup-Sequence`).

Mais quelques jeux WHDL peu nombreux (comme History Line : 1914-1918) peuvent demander des paramètres additionnels au démarrage sous peine de simple crash.

Vous pouvez ajouter un deuxième fichier (complètement optionnel) contenant ces paramètres additionnels, par conséquent vous aurez, en prenant l'exemple d'History Line :

* HistoryLine (dossier du jeu WHDL) 
* HistoryLine.uae (qui peut être vide ou perso.) 
* HistoryLine.whdl (optionnel, contenant des paramètres en plus `CUSTOM1=1`)

Le jeu se lancera ensuite.

Voici une petite liste des jeux qui requiert des paramètres additionnels.

| Jeu | Paramètre(s) extra / Contenu du fichier |
| :--- | :--- |
| HistoryLine 1914-1918 | `CUSTOM1=1` pour l'intro, `CUSTOM1=2` pour le jeu |
| The Settlers / Die Siedlers | `CUSTOM1=1` passe l'intro |