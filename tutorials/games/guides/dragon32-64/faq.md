---
title: FAQ
description: 
published: true
date: 2021-09-04T23:34:54.453Z
tags: 7.3+, faq, dragon32, dragon64
editor: markdown
dateCreated: 2021-08-29T17:03:01.355Z
---

## Quelques informations utiles...

- Le core XRoar supporte :
  - Le lissage (dans EmulationStation, `START` > `OPTIONS DES JEUX` > `LISSAGE`).
  - Les surcharges de configuration mais elles ne se surchargent pas entre-elles. La première trouvée dans l'ordre est celle qui est prise en compte : `rom.ext.config` > `<folder>.config` > `<parent>.config` > ... La syntaxe du fichier de configuration ne permet pas de gérer les surcharges sans faire une usine à gaz. Pour générer un fichier config, vous pouvez utiliser cette commande SSH : `xroar --config-all > .config`
- Par défaut, la machine Dragon est un Dragon64
- La machine à utiliser est automatiquement sélectionnée en fonction des sous-répertoires :

```
"/dragon/": "dragon64",
"/dragon32/": "dragon32",
"/dragon64/": "dragon64",
"/tano/": "tano",
"/dragon200/": "dragon200e",
"/dragon200e/": "dragon200e",
```

- Les joystick 0 et 1 sont automatiquement pris en compte quelque soit l'ordre choisi dans EmulationStation.
- Si la machine "imprime", le texte imprimé ira dans le fichier `/recalbox/share/saves/dragon/printer/rom-extension.txt`