---
title: Jeux multi-disques
description: 
published: true
date: 2021-09-05T12:12:30.624Z
tags: dreamcast, multi-disques
editor: markdown
dateCreated: 2021-05-21T08:46:39.076Z
---

Sur **Dreamcast**, il existe **quelques jeux Multidisc**.

**Exemple** :

* _Resident Evil Code Veronica_
* _Skies Of Arcadia_
* _Grandia 2_
* _Etc..._

**Voici comment changer de disque :**

A la fin du disque 1 de votre jeu, le jeu propose une _sauvegarde_. On revient au menu avec **Hotkey** (sur Reicast ça suffit), puis on lance le deuxième disque **en récupérant sa sauvegarde**.

Rien de plus simple !