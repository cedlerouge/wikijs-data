---
title: Accéder au menu des cartes mémoires
description: 
published: true
date: 2021-09-05T12:11:38.557Z
tags: carte, vmu, mémoire
editor: markdown
dateCreated: 2021-05-21T08:46:33.193Z
---

## Objectif

L'objectif est de pouvoir modifier et manipuler le contenu des cartes mémoires. Il existe 2 façons de procéder.

## {.tabset}
### Ejection du disque

* Lancez un jeu Dreamcast.
* Quand le jeu se joue, éjectez le disque avec `Hotkey` + `Stick gauche vers le haut`.
* La console va automatiquement arrêter le jeu et arriver sur le système. D'ici, vous pouvez accéder aux cartes mémoires.
* Pour reprendre le jeu, faites de nouveau `Hotkey` + `Stick gauche vers le haut`.

### Disque vide

* Créez un fichier texte vide dans le dossier `/share/roms/dreamcast` que vous renommerez à votre convenance avec une extension `.cdi` (Dreamcast BIOS.cdi par exemple).
* Actualisez la liste de jeux dans Recalbox.
* Lancez le "jeu" et voilà !