---
title: Dreamcast
description: 
published: true
date: 2021-09-05T12:11:14.909Z
tags: sega, dreamcast
editor: markdown
dateCreated: 2021-05-21T08:37:08.962Z
---

Avec les tutoriels disponibles ici, vous allez découvrir comment gérer le contenu des cartes mémoire ainsi que le changement de disque pour les jeux sur plusieurs disques.

Voici les tutoriels disponibles :

[Accéder au menu des cartes mémoires sur Dreamcast](access-vmu-menu)
[Jeux multi-disques](multidisc-game) 