---
title: Les romsets pour Recalbox
description: 
published: true
date: 2021-09-15T10:30:24.112Z
tags: romsets
editor: markdown
dateCreated: 2021-05-21T08:48:31.282Z
---

**Légende:**

  🥇    ==> Set à utiliser
  🥈    ==> Pour compléter (pour les roms hack, traduction, etc...)
  🥉    ==> En dernier recours
  🎮    ==> Set recommandé pour le Netplay
  ❌    ==> Pas de set disponible pour ce groupe
  ❎    ==> Set existant mais **"Non recommandé"**
  ❓    ==> Inconnu

## Arcade

>Pour l'arcade, le romset est différent selon le core que vous utilisez.
{.is-info}

### Atomiswave

| Core | Romset | .dat | Architecture |
| :---: | :---: | :---: | :---: |
| **Flycast** | MAME 0.135 ou supérieur, 0.235 conseillé |  | x86_64 |

### FinalBurn

| Core | Romset | .dat | Architecture |
| :---: | :---: | :---: | :---: |
| **FBNeo** | FinalBurn Neo v1.0.0.03 |  | Pi2 / Pi3 / Odroid / x86_84 |
| **PiFBA** | fba 0.2.96.71 |  | Pi0 / Pi0w / Pi1 |

### Laserdisc (Daphne)

* Certains jeux peuvent être directement et légalement téléchargés par l'outil DaphneLoader de la [distribution Windows de Daphne](http://www.daphne-emu.com/site3/index_hi.php). 
* Pour d'autres (Dragon's Lair, Dragon's Lair II, Space Ace, ...), vous devez prouver que vous possédez une licence valide du jeu (version DVD par exemple).

Une fois téléchargé, copiez les fichiers nécessaires (ROM et fichiers d'image du disque-laser) sur votre Recalbox, comme expliqué ci-dessous.

### Mame

| Core | Romset | .dat | Architecture |
| :---: | :---: | :---: | :---: |
| AdvanceMame | MAME 0.106 |  | Tous les Pi |
| Mame2000 | MAME 0.37b5 |  | Pi1 |
| Mame2003 | MAME 0.78 |  | Tout |
| Mame2003Plus  | MAME 0.78-0.188 |  | Tout |
| Mame2010 | MAME 0.139 |  | Pi2 / Pi3 / x86 / x86_64 |
| Mame2015 | MAME 0.160 |  | Pi3 / x86 / x86_64 |
| Mame | MAME 0.235 |  | X86 / x86_64 |

### Neo-Geo

| Core | Romset | .dat | Architecture |
| :---: | :---: | :---: | :---: |
| **FBNeo** | FinalBurn Neo v1.0.0.03 |  | Toutes les plateformes |
| **Mame** | Voir les romsets [ci-dessus](./romsets-for-recalbox#mame). |  | Toutes les plateformes |

### Naomi

| Core | Romset | .dat | Architecture |
| :---: | :---: | :---: | :---: |
| **Flycast** | MAME 0.135 ou supérieur, 0.235 conseillé |  | x86_64 |

### Naomi GD-ROM

| Core | Romset | .dat | Architecture |
| :---: | :---: | :---: | :---: |
| **Flycast** | MAME 0.135 ou supérieur, 0.235 conseillé |  | x86_64 |

>Pour plus d'informations sur les roms Arcade, veuillez consulter [cette page](./../../../advanced-usage/arcade-in-recalbox).
{.is-info}

### Sega Model 3

| Core | Romset | .dat | Architecture |
| :---: | :---: | :---: | :---: |
| **Supermodel** | MAME 0.135 ou supérieur, 0.235 conseillé |  | x86_64 |

## Console Fantasy

| Plateforme | No-Intro | Tosec | Redump | Trurip |
| :---: | :---: | :---: | :---: | :---: |
| **EasyRPG** |  |  |  |  |
| **Lutro** |  |  |  |  |
| **Openbor** |  |  |  |  |
| **Solarus** |  |  |  |  |
| **Tic-80** |  |  |  |  |
| **Uzebox** |  |  |  |  |

## Consoles de salon

| Plateforme | No-Intro | TOSEC | Redump | Trurip |
| :---: | :---: | :---: | :---: | :---: |
| **32x** | 🥇 🎮 | 🥈 | ❌ | 🥉 |
| **Amiga CD32** | ❌ | 🥈 | 🥇 | 🥉 |
| **Amiga CDTV** | ❌ | 🥈 | 🥇 | 🥉 |
| **Amstrad GX4000** | ❌ | 🥇 | ❌ | 🥈 |
| **Atari 2600** | 🥇 🎮 | 🥈 | ❌ | ❌ |
| **Atari 5200** | 🥇 | 🥈 | ❌ | 🥉 |
| **Atari 7800** | 🥇 | 🥈 | ❌ | 🥉 |
| **Atari Jaguar** | 🥇 🎮 | 🥈 | ❌ | 🥉 |
| **ColecoVision** | 🥇 | 🥈 | ❌ | 🥉 |
| **Dreamcast** | ❌ | 🥈 | 🥇 | 🥉 |
| **Fairchild Channel F** | 🥇 | 🥈 | ❌ | 🥉 |
| **Famicom Disk System** | 🥇 🎮 | 🥈 | ❌ | 🥉 |
| **GameCube** | ❌ | 🥈 | 🥇 🎮 | 🥉 |
| **Intellivision** | 🥇 | 🥈 | ❌ | 🥉 |
| **Master System** | 🥇 🎮 | 🥈 | ❌ | 🥉 |
| **Megadrive** | 🥇 🎮 | 🥈 | ❌ | 🥉 |
| **Multivision** | ❌ | 🥇 | ❌ | ❌ |
| **NeoGeo CD** | ❌ | ❌ | 🥇 | 🥇 |
| **NES** | 🥇 🎮 | 🥈 | ❌ | ❌ |
| **Nintendo 64** | 🥇 | 🥈 | ❌ | ❌ |
| **Nintendo 64DD** | 🥇 | 🥈 | ❌ | ❌ |
| **Panasonic 3DO** | ❌ | 🥈 | 🥇 | 🥉 |
| **Pc-Engine** | 🥇 🎮 | 🥈 | ❌ | 🥉 |
| **Pc-Engine CD** | ❌ | 🥈 | 🥇 | 🥉 |
| **PCFX** | ❌ | 🥉 | 🥇 | 🥈 |
| **PlayStation 1** | ❌ | 🥉 | 🥇 | 🥈 |
| **Satellaview** | 🥇 | 🥉 | ❌ | ❌ |
| **Saturn** | ❌ | 🥉 | 🥇 | 🥈 |
| **Sega Mega CD** | ❌ | 🥉 | 🥇 | 🥈 |
| **Sega SG1000** | 🥇 🎮 | 🥉 | ❌ | ❌ |
| **Super Famicom** | 🥇 | 🥈 | ❌ | 🥉 |
| **SuFami Turbo** | 🥇 | 🥈 | ❌ | ❌ |
| **Super Nintendo** | 🥇 🎮 | 🥈 | ❌ | ❌ |
| **SuperGrafx** | 🥇 🎮 | 🥈 | ❌ | 🥉 |
| **Vectrex** | 🥇 | 🥈 | ❌ | 🥉 |
| **VideoPac** | 🥇 | 🥈 | ❌ | 🥉 |
| **Virtual Boy** | 🥇 | 🥈 | ❌ | 🥉 |
| **Wii** | ❌ | 🥈 | 🥇 🎮 | ❌ |

## Consoles portables

| Plateforme | No-Intro | Tosec | Redump | Trurip |
| :---: | :---: | :---: | :---: | :---: |
| **Game and Watch** | ❌ | ❌ | ❌ | ❌ |
| **Game Boy** | 🥇 | 🥈 | ❌ | 🥉 |
| **Game Boy Color** | 🥇 | 🥈 | ❌ | 🥉 |
| **Game Boy Advance** | 🥇 | 🥈 | ❌ | 🥉 |
| **Game Gear** | 🥇 | 🥈 | ❌ | 🥉 |
| **Lynx** | 🥇 | 🥈 | ❌ | 🥉 |
| **Nintendo DS** | 🥇 | ❌ | ❌ | ❌ |
| **NeoGeo Pocket** | 🥇 | 🥈 | ❌ | 🥉 |
| **NeoGeo Pocket Color** | 🥇 | 🥈 | ❌ | 🥉 |
| **Palm OS** | 🥈 | ❌ | 🥇 | 🥉 |
| **Pokémini** | 🥇 | 🥈 | ❌ | 🥉 |
| **PSP** | ❌ | ❌ | 🥇 | 🥈 |
| **Wonderswan** | 🥇 | 🥈 | ❌ | 🥉 |
| **Wonderswan Color** | 🥇 | 🥈 | ❌ | 🥉 |

## Ordinateurs

| Plateforme | No-Intro | Tosec | Redump | Trurip |
| :---: | :---: | :---: | :---: | :---: |
| **Amiga 600** | ❌ | 🥇 | ❌ | ❌ |
| **Amiga 1200** | ❌ | 🥇 | ❌ | ❌ |
| **Amstrad CPC** | 🥈 | 🥇 | ❌ | 🥉 |
| **Apple II** | ❌ | 🥇 | ❌ | 🥈 |
| **Apple II Gs** | ❌ | 🥇 | ❌ | 🥈 |
| **Atari 800** | ❌ | 🥇 | ❌ | 🥈 |
| **Atari St** | 🥈 | 🥇 | ❌ | 🥉 |
| **Commodore 64** | 🥈 | 🥇 | ❌ | 🥉 |
| **MSX 1** | 🥈 | 🥇 | ❌ | 🥉 |
| **MSX 2** | 🥈 | 🥇 | ❌ | 🥉 |
| **MSX Turbo R** | 🥈 | 🥇 | ❌ | 🥉 |
| **NEC Pc88** | ❌ | 🥈 | 🥇 | 🥉 |
| **NEC Pc9801** | ❌ | 🥈 | 🥇 | 🥉 |
| **Oric Atmos** | ❌ | 🥇 | ❌ | ❌ |
| **Samcoupé** | ❌ | 🥇 | ❌ | 🥈 |
| **Sharp X1** | ❌ | 🥇 | ❌ | 🥈 |
| **Sharp X68000** | ❌ | 🥈 | 🥇 | 🥉 |
| **Sinclair ZXspectrum** | 🥇 | 🥈 | ❌ | 🥉 |
| **Sinclair ZX81** | ❌ | 🥇 | ❌ | 🥈 |
| **Spectravideo SV-318** | ❌ | 🥇 | ❌ | 🥈 |
| **Thomson MO/TO** | ❌ | 🥇 | ❌ | 🥈 |