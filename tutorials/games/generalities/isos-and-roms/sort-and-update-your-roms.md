---
title: Trier et mettre à jour ses roms
description: 
published: true
date: 2021-09-04T22:58:46.663Z
tags: roms, trier, mettre à jour
editor: markdown
dateCreated: 2021-05-21T08:48:38.192Z
---

Pour trier et mettre à jour vos jeux, vous aurait besoin d'un logiciel, d'un fichier dat et pour certaines consoles un fichier header.

>Cette étape n'est pas à négliger car les roms ont des mises à jour par certains groupes qui permettent d'améliorer les roms en supprimant des bugs présents sur la rom ou pour mieux la faire tourner sur émulateur.
>
>Ces mises à jour ne sont pas fréquentes mais reste néanmoins indispensables pour leurs bons fonctionnements sur émulateur et est essentiel en Netplay. 
{.is-warning}

## Les logiciels

Vous avez plusieurs logiciels pour trier et mettre à jour vos roms.

* [Clrmamepro](https://mamedev.emulab.it/clrmamepro/)
* [Romulus](https://romulus.cc/)
* [Universal Rom Cleaner](https://github.com/Universal-Rom-Tools/Universal-ROM-Cleaner/releases)

>Pour les débutants, il est conseillé d'utiliser Romulus qui est plus facile d'utilisation.
{.is-info}

## Les fichiers dat

Vous pourrez télécharger ces fichiers sur ces sites:

* **Atomiswave** : ❓
* **FBNeo** : ❓
* **MAME** : ❓
* **Naomi** : ❓
* **Naomi GD-Rom** : ❓
* **Neo Geo** : ❓
* **No-Intro** : [https://datomatic.no-intro.org/?page=download](https://datomatic.no-intro.org/?page=download)
* **Redump** : [https://redump.org/downloads/](https://redump.org/downloads/)
* **TOSEC** : [https://www.tosecdev.org/](https://www.tosecdev.org/)
* **Trurip** : [https://database.trurip.org/](https://database.trurip.org/)
* **GoodSets/Goodtools** : [https://cowering.blogspot.com/](https://cowering.blogspot.com/)

Veillez à prendre le fichier dat correspondant au groupe de rom que vous aurez choisi !

## Les fichiers header

Pour certaines consoles, il vous faudra un fichier header (en-tête) en compléments du fichier dat.

Voici la liste des consoles concernées :

* Atari - 7800
* Atari - Lynx
* Nintendo - Family Computer Disk System
* Nintendo - Nintendo Entertainment System

## Les préférences de tri

### National ou international ?

* **National** : si vous voulez jouer uniquement avec les personnes du même pays que vous : gardez uniquement les roms de votre pays puis privilégiez les roms USA et Japon pour les manquants.
* **International** : si vous voulez jouer avec des personnes des autres pays : gardez une rom de chaque pays.

Voici un exemple international :

* Super Street Fighter II (Europe)
* Super Street Fighter II - The New Challengers (Japan)
* Street Fighter II Plus - Champion Edition (Japan, Korea, Asia)

### Les Retroachievements

Certains trophées Retroachievements ont besoin de la rom USA pour valider les retroachievements et certains jeux n'existent qu'en version japonaise.  

Vous trouverez plus d'informations [ici](./../../../../basic-usage/features/retroachievements).

### Les jeux et les régions

Selon la région, les jeux peuvent avoir des différences plus au moins flagrantes. De plus, certains jeux n'existent qu'en version japonaise.

Voici quelques exemples de ces différences :

* Boss différent.
* Couleur de sang différente (rouge sur une version USA qui passe en vert pour une version Europe).
* Fps plus ou moins élevé.
* Fréquence (MHz) plus ou moins élevé.
* Nombre de joueurs différents.

### Les codes et tags dans les roms

Pour savoir à quoi correspondent les codes et tags dans les roms (uniquement pour les roms cartouches et disquettes), vous trouverez plus d'informations [ici](./../tags-used-in-rom-names).