---
title: Généralités
description: 
published: true
date: 2021-09-04T22:54:34.757Z
tags: généralités
editor: markdown
dateCreated: 2021-05-21T08:07:53.185Z
---

Vous allez connaitre ici quelques généralités liés aux jeux et les systèmes autour.

Voici les catégories disponibles :

[Les roms et les isos](isos-and-roms)

Voici les tutoriels disponibles :

[Configuration des shaders](shaders-configuration)
[Où trouver des roms 100% légales](where-to-find-100-legal-roms)
[Tags utilisés dans les noms des roms](tags-used-in-rom-names)