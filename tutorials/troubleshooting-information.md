---
title: 🔖 Dépannage
description: 
published: true
date: 2021-09-04T18:09:51.032Z
tags: dépannage, tutoriel
editor: markdown
dateCreated: 2021-05-21T07:52:36.044Z
---

Cet endroit concerne les plus aguerris d'entre-vous, à base de lignes de commandes SSH. Vous pourrez exécutr certains commandes utiles si on vous le demande.

Voici les tutoriels disponibles :

[Afficher le contenu de la mémoire tampon Linux](display-buffer-memory-linux)
[Lister les modules chargés](list-loaded-modules)
[Récupérer les informations sur les manettes](grab-controllers-informations)
[Script de support](support-script)