---
title: Flasher une image
description: 
published: true
date: 2021-09-04T21:11:15.196Z
tags: image, flasher
editor: markdown
dateCreated: 2021-05-21T08:11:24.001Z
---

Vous pourrez ici apprendre à flasher Recalbox à partir de plusieurs logiciels différents.

Voici les tutoriels disponibles :

[Balena Etcher](balena-etcher)
[Raspberry Pi Imager](raspberry-pi-imager)
[Rufus](rufus)
[Unetbootin](unetbootin)