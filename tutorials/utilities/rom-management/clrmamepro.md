---
title: Clrmamepro
description: 
published: true
date: 2021-10-03T22:14:20.281Z
tags: clrmamepro
editor: markdown
dateCreated: 2021-05-21T08:42:24.386Z
---

Dans ce tutoriel, nous allons vous apprendre en quelques clics à vous servir de Clrmamepro pour extraire (rebuilder) ou pour scanner un romset arcade. 

## I - Avant de commencer

### 1 - Éléments nécessaires

Au préalable il vous faut :

* Le logiciel Clrmamepro [ici](https://mamedev.emulab.it/clrmamepro/).
* Un [romset MAME](./../../../advanced-usage/arcade-in-recalbox) qui correspond à votre émulateur.
* Les samples (optionnel selon le romset). 
* Les [CHDs](./../../../advanced-usage/arcade-in-recalbox) (optionnel selon le romset).
* Un fichiers .DAT ou .XML dans le dossier [share](./../../../basic-usage/file-management)/bios de votre Recalbox.

### 2 - Définitions

* **Set** : Fichier compressé (.zip/.7z/.rar) de jeux, drivers et bios.
* **Rom** : Fichier contenu dans un set.

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro1.png)

* **Qu'est ce qu'un fichier dat ?** Les fichiers dat sont des fichiers textes avec des informations sur les jeux émulés avec une version spécifique de l'émulateur MAME.  Dans ses fichiers dat, on trouve les informations pour des sets particuliers comme : nom de la rom (name), année (year), fabricant (manufacturer), information de fusion (merge), rom parent (romof) ou clone de (cloneof), taille de la rom (size), CRC pour chaque rom. CRC est un algorithme qui permet de vérifier l'intégrité d'un fichier, clrmamepro l'utilise pour vérifier vos rom-sets.
* **ClrmamePro** : C'est un logiciel qui vous permet de vérifier et reconstruire vos romsets arcade en fonction d'un fichier d'information au format xml ou dat.

### 3 - Installation

#### Windows

Téléchargez ClrmamePro pour [Windows](http://mamedev.emulab.it/clrmamepro/#downloads)

#### macOS

Téléchargez ClrmamePro pour [macOS](http://www.emulab.it/)

#### Linux

Téléchargez ClrmamePro sous Linux avec [Wine](https://doc.ubuntu-fr.org/wine).

>Le fichier zip est une version portable.
{.is-info}

* Installez-le.
* Récupérez le fichier dat de l’émulateur arcade souhaité.
* Démarrer Clrmamepro :
  * Exécutez clrmamepro via un clic droit sur l’icône.
  * _Puis "Exécuter en tant qu'administrateur_".

>**Astuce sous Windows** :
>
>* Rendez vous sur l’exécutable cmpro64.exe ou cmpro32.exe
>* Puis clic droit
>* Choisir _Propriétés_
>* Aller dans l'onglet _Compatibilité_
>* Puis cocher la case "_Exécuter en tant qu'administrateur"_.
>* Valider par Ok.
>
>Le programme s’exécutera toujours en administrateur désormais.
{.is-success}

## II - Utilisation

### 1 - Descriptif des fonctions

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro2.png)

### 2 - Rebuilder (reconstruire)

* Démarrer clrmamepro.

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro3.png)

* Pour sélectionner votre fichier .dat cliquez sur "Add DatFile".

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro4.png)

* Sélectionnez votre fichier puis "ouvrir".

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro5.png)

* Une fois sélectionné, vous pouvez indiquer un dossier ou un sous dossier. En sélectionnant "ok" clrmamepro va créer un dossier "NEW DATFILES" automatiquement.

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro6.png)

* Sélectionnez votre fichier .dat puis "Load/Update" afin de le charger

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro7.png)

* Clrmamepro vous propose de créer ou de mettre à jour la configuration. Nous vous conseillons de cliquer sur "Default".

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro8.png)

* Clrmamepro va lire et charger votre fichier et vous devriez arriver sur le menu général.
  * Pour extraire un romset cliquer sur "Rebuilder"

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro9.png)

* Dans la "source" vous devez indiquer le dossier de votre romset MAME.

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro10.png)

* Sélectionnez le dossier ou se trouve votre romset puis "OK".

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro11.png)

* Dans la "destination" vous devez indiquer un dossier pour récolter le romset de l'extraction.

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro12.png)

* Sélectionnez le dossier de destination puis "OK".

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro13.png)

* **Dans le menu Rebuilder, vous pouvez paramétrer** :

  * 1 - [**Le type de romset**](./../../../advanced-usage/arcade-in-recalbox) que vous voulez créer **(Merge Options)** :

    * Non-merged
    * Split
    * Merged

  * **2 - Les Options** : (Nous vous conseillons les paramètres par default)

    * Option de compression (.zip/.7z/.rar).
    * Option de recompressions. Si vous souhaitez ou non recompresser les fichiers.
    * "Show Statistics" pour voir les résultats à la fin du Rebuild.
    * "Remove Matched Sourcefiles". Si cette option est activée, clrmamepro va extraire et supprimer les fichiers correspondant à votre fichier .DAT de votre Romset MAME.
    * "Systems" et" Advanced". Ne vous aventurez pas ici pour le moment.

* Lancez enfin l'extraction en cliquant sur "Rebuild".

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro14.png)

* Laissez faire clrmamepro jusqu'à la fin.

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro15.png)

* Et voilà ! Nous vous conseillons de faire un scanner du dossier de destination afin de vérifier qu'il est parfait🙂.

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro16.png)

### 3 - Scanner (Vérifier)

* Démarrer clrmamepro.

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro3.png)

* Pour sélectionner votre fichier .dat, cliquez sur "Add DatFile".

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro4.png)

* Sélectionnez votre fichier puis "ouvrir". 

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro5.png)

* Une fois sélectionné, vous pouvez indiquer un dossier ou un sous dossier. En sélectionnant "ok", clrmamepro va créer un dossier "NEW DATFILES" automatiquement.

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro6.png)

* Sélectionnez votre fichier .dat puis "Load/Update" afin de le charger.

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro7.png)

* Clrmamepro vous propose de créer ou de mettre à jour la configuration. Nous vous conseillons de cliquer sur "Default".

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro8.png)

* Clrmamepro va lire et charger votre fichier et vous devriez arriver sur le menu général. Pour indiquer ou se trouve votre romset nous allons cliquer sur "Settings".

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro17.png)

* Le chemin vers votre romset dans "ROM-Paths" en cliquant sur "Add".

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro18.png)

* Sélectionnez le dossier ou se trouve votre romset que vous souhaitez scanner puis "OK". 

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro19.png)

* Pour Mame, si le romset doit contenir des samples, vous devez donner le chemin des samples de la même manière avec "Sample-Paths".

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro20.png)

* Sauvegardez les chemins en cliquant sur "Save As Def.". 

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro21.png)

* Clrmamepro vous confirme que c'est pris en compte alors cliquez sur "OK".

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro22.png)

* Vous pouvez fermer cette fenêtre.

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro23.png)

* Une fois revenu sur le menu principal, cliquez sur "Scanner".

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro24.png)

* Dans le menu **Scanner** vous devez paramétrer : 

**Les éléments à scanner (You want to scan)** :

* Sets (bios/jeux/drivers),
* Roms (puces qui composent chaque Sets)
* Samples (Drivers complémentaires pour certains jeux Mame)
* [CHDs](./../../../advanced-usage/arcade-in-recalbox) (Que vous avez ajouté à votre Romset)

[**Le type de romset**](./../../../advanced-usage/arcade-in-recalbox) que vous avez **(You prefer)** :

* Non-merged
* Split
* Merged

**Les messages pendant et après le scan (Prompts)** :

* Ask Before Fixing
* Add/Show Statistics

**Check/Fix** : Clrmamepro va vérifier ce qui est dans la colonne de gauche "check". Si vous souhaitez que clrmamepro fasse des corrections vous pouvez cocher les cases de la colonne de droite "Fix". Attention avec l'option "fix" clrmamepro va enlever les dossier et fichiers qui ne correspondent pas à votre DAT et les placer dans son "backup".

**Options** : Ne vous aventurez pas ici pour le moment 

Lancez enfin le scan en cliquant sur "New Scan"

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro25.png)

* Dans cette fenêtre vont s'afficher les différents problèmes de votre set. (Exemple: missing rom) Vous pouvez exporter une liste de ce qu'il vous manque dans l'onglet "Miss List" afin de compléter.

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro26.png)

* Enfin, vous aurez les résultats du scanner dans la fenêtre "statistics". Le plus important est la partie "Missing" ci-dessous il manque rien donc parfait.

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro27.png)