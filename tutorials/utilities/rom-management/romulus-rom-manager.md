---
title: Romulus Rom Manager
description: Romulus Rom Manager © by F0XHOUND
published: true
date: 2021-09-04T21:41:21.924Z
tags: manager, rom, romulus
editor: markdown
dateCreated: 2021-05-21T08:42:37.720Z
---

## Introduction

Pour de nombreux utilisateurs, la gestion des Roms est toujours un sujet qui semble compliqué au premier abord ce qui entraine beaucoup de questions sur le forum et dans le Discord.

Afin de rendre plus abordable la gestion des Roms, ce tutoriel va vous apprendre à utiliser **Romulus Rom Manager** (nommé **Romulus** par la suite) car c'est l'outil actuellement disponible le plus simple à utiliser. Bien sûr, il existe d'autres outils tels que **clrMamePro** ou **Romcenter** (plus complets pour l'arcade surtout) mais ils sont beaucoup plus compliqués d'utilisation.

La gestion des roms n'est pas exclusivement réservée aux jeux d' arcade, elle est fortement recommandée pour les jeux sur console également.

## Télécharger Romulus

Page officielle : [https://romulus.cc/](https://romulus.cc/)

## Installation

Après avoir téléchargé Romulus, il faut décompresser l'archive dans le répertoire de votre choix.

On obtient alors les fichiers suivants :

![romulus1.png](/tutorials/utilities/rom-management/romulus/romulus1.png)

En lançant **Romulus.exe**, l'écran suivant apparait :

![](/tutorials/utilities/rom-management/romulus/romulus2.png)

Il faut valider en cliquant sur **OK**. Bienvenue dans Romulus :

![](/tutorials/utilities/rom-management/romulus/romulus3.png)

## Romulus pour les Consoles

Rendez-vous sur la page : [Romulus pour les Consoles](romulus-for-consoles)

## Romulus pour l'Arcade

Rendez-vous sur la page : [Romulus pour l'Arcade](romulus-for-arcade)