---
title: Skraper - Scraper ses roms
description: 
published: true
date: 2021-09-04T21:10:30.668Z
tags: roms, scraper, skraper
editor: markdown
dateCreated: 2021-05-21T08:41:59.320Z
---

## Guide



## Tutoriel Vidéo

Le tutorial vidéo se trouve sur [YouTube](https://www.youtube.com/watch?v=G388Gc6kkRs).

## Problème possible

En voici une liste :

* Mal nommé
* Absent de la base de donnée.
* Mauvaise option de recherche dans **Skraper.**
* Serveur de **Screenscaper** en maintenance.

## Aidez à financer l'hébergement et le développement de ScreenScraper

| 1 euro : merci ! | 2 euros : merci beaucoup ! | 5 euros : 1 thread supplémentaire ! | 10 euros : 5 threads supplémentaires ! |
| :---: | :---: | :---: | :---: |
| Contributeur financier niveau BRONZE sur le site ScreenScraper rubrique Membre ! | Contributeur financier niveau ARGENT sur le site ScreenScraper rubrique Membre ! | 1 thread supplémentaire a vie pour scraper plus vite en plus d'apporter votre soutien à la vie. Contributeur financier niveau OR sur le site ScreenScraper rubrique Membre | 5 threads supplémentaires à vie pour scraper 5 fois plus vite en plus d'apporter votre soutien à la vie. Contributeur financier niveau OR sur le site ScreenScraper rubrique Membre ! |

## Site Screenscraper

[https://www.screenscraper.fr/](https://www.screenscraper.fr/)