---
title: Liste d'utilitaires utiles
description: 
published: true
date: 2021-09-04T19:52:34.572Z
tags: utiles, utilitaires, liste
editor: markdown
dateCreated: 2021-05-21T08:11:18.371Z
---

Avant de débuter dans l'aventure Recalbox, voici une liste d'utilitaires que vous devez posséder sur votre PC qui pourront vous aider.

>Utilisateurs de macOS et Linux si vous avez des équivalents, modifiez cette page pour les ajouter.
{.is-info}

## Utilitaires de formatage microSD

* [SDFormatter](https://www.sdcard.org/downloads/formatter_4/) : logiciel pour formater SD/SDHC/SDXC.
* [Minitool Partition wizard free](http://www.partitionwizard.com/free-partition-manager.html) : logiciel permettant de formater dans différents formats.

## Utilitaire pour flasher une image 

* **(Recommandé)** [Raspberry Pi Imager](https://www.raspberrypi.org/software/) : outil disponible sur tous les systèmes (Windows, Linux, macOS, ...) permettant de flasher un fichier `.img` sur des périphériques de stockage (carte microsd/disque dur).
* [WIN32 Disk Imager](https://sourceforge.net/projects/win32diskimager/) : permet de créer une image de votre microSD.
* **(Déconseillé)** Balena [Etcher](https://etcher.io/) : outil cross-platform permettant de flasher un fichier `.img` sur des périphériques de stockage (carte microsd/disque dur).

## Utilitaires pour décompresser vos archives

* [7zip](http://www.7-zip.org/download.html) : idéal pour décompresser vos romsets au format 7zip.001, 7zip.002, etc.

## Utilitaires pour gérer à distance votre Recalbox

## {.tabset}
### Windows
* [MobaXTerm](https://mobaxterm.mobatek.net/)
* [WINSCP](http://winscp.net/)
* [Putty](https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html) 
* [Notepad++](https://notepad-plus-plus.org/downloads/)
\
Consulter [ce tutoriel](./../../tutorials/system/access/network-access-winscp) de configuration de ces utilitaires.

### macOS
* **Terminal** fourni par défaut (Applications > Utilitaires).
* **TextEdit** fourni par défaut.

### Linux
* **Terminal** fourni par défaut.

## Utilitaire pour contrôler les signatures numériques des bios

## {.tabset}
### Windows
* [winmd5free](http://www.winmd5.com/)

* [md5sum](http://esrg.sourceforge.net/utils_win_up/md5sum/)

### macOS
* Commande **md5sum** via le terminal.

### Linux
* Commande **md5sum** via le terminal.

## Utilitaires pour gérer vos roms en local sur votre PC

* [Universal XML Editor – Éditez simplement vos fichiers xml](https://github.com/Universal-Rom-Tools/Universal-XML-Editor/releases) (local) 
* [Universal ROM Cleaner – Nettoyez vos romset d'un clic](https://forum.recalbox.com/topic/2344/soft-universal-rom-cleaner-nettoyez-vos-romset-d-une-main-clean-your-romset-with-one-hand) (local)

## Utilitaires pour gérer les romsets au format CD (iso, img, bin)

Pour Sega CD, PC-Engine CD et PSX.

* [Daemon Tools Lite](https://www.daemon-tools.cc/fra/products/dtLite) : Monter les images CD sur votre PC ; son [tutoriel](./../games/guides/playstation-1/convert-disc-files-to-bin+cue)
* [IMGBurn](https://www.imgburn.com/) : Permet de créer une image au format bin+cue ; son [tutoriel](./../games/guides/playstation-1/convert-disc-files-to-bin+cue)
* [Monkey Audio](http://www.monkeysaudio.com/download.html) : Permet de décompresser les fichiers ape en wav.
* [emctools](https://app.box.com/s/l8x54nof3k53myk6yueaz2d1p02x8d5t) : Permet de décompresser le format bin.ec.
* [psx2psp](https://github.com/recalbox/recalbox-os/wiki/Gestion-multidisc-PSX) : Permet de fusionner plusieurs CD.
* [SegaCD Cue Maker](http://www.racketboy.com/downloads/SegaCueMaker.zip) : Permet de créer les cue manquants pour vos fichiers bin Sega CD uniquement.

## Utilitaire pour renommer le nom interne de la rom

* [N64 Rom Renamer](https://www.romhacking.net/utilities/791/) **:** Par Einstein II.