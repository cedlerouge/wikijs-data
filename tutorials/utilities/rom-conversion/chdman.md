---
title: CHDMAN
description: Convertir vos roms CD en un format compressé lisible.
published: true
date: 2021-09-04T19:59:16.920Z
tags: chd, chdman
editor: markdown
dateCreated: 2021-05-21T08:42:11.451Z
---

## Introduction

Les formats de fichiers à adopter pour les CDs et autres supports optiques.

Nous avons parlé d'un format, dérivé de recherches dont nous avons eus sur MAME, afin de pouvoir représenter au mieux ces supports, de manière compressée, sans perdre l'intégrité des données et que celles-ci puissent rester scrappables.

Ce format est le CHD (Compressed Hunks of Data), datas qui pourraient être pratiques pour les consoles SEGA CD, PS1, PC Engine...

Malheureusement, l'utilisateur classique aura du mal à utiliser le convertisseur, qui est un exécutable en ligne de commandes.

Voici donc un fichier Zip, avec des scripts automatisés, pour passer du format BIN+CUE (format Redump) vers CHD, et inversement.

Il à été ajouté aussi pour le format GDI, qui est pour la Dreamcast.

Pour les jeux PS1 protégés par LibCrypt, normalement vous avez des fichiers SBI (Subchannel Information), vous les conservez et les mettre avec les CHDs, sinon vos jeux ne passeront pas.

Si jamais vous lez perdez ou vous effectuez une mauvaise manipulation, ces fichiers sont disponibles sur les fiches de chaque disque correspondant chez Redump, à côté des fichiers CUE retéléchargeables.

>Évitez d'utiliser pour le moment avec vos sets Saturn, sinon dans les bug reports/fiches d'incident utilisateur sur les émulateurs, les demandes seront invalidées côté RetroArch ou chez les devs des émulateurs.
{.is-warning}

## Logiciel

## {.tabset}
### Windows

Vous pouvez télécharger le logiciel en cliquant sur **CHDMAN.zip** ci-dessous.

![](/tutorials/utilities/rom-conversion/chdman/archive.svg) [CHDMAN.zip](https://cdn.discordapp.com/attachments/438686828418039818/776962833412784128/CHDMAN.zip)

Dans ce zip, vous trouverez 6 fichiers :

| Nom de fichier | Description |
| :--- | :--- |
| **chdman.exe** | Le logiciel CHDMAN. |
| **CUE or GDI to CHD.bat** | Un fichier .bat qui vous permet de convertir vos roms aux formats `CUE` ou `GDI` en `CHD`. |
| **Extract CHD to CUE.bat** | Un fichier .bat qui vous permet de convertir vos roms au format `CHD` en `BIN`/`CUE`. |
| **Extract CHD to GDI.bat** | Un fichier .bat qui vous permet de convertir vos rom au format `CHD` en `GDI`. |
| **README_EN.txt** |  |
| **README_FR.txt** |  |

#### Contenu du fichier Readme

* **CUE or GDI to CHD**

  Compresse tous types de fichiers disques BIN avec un entête CUE ou GDI vers le format CHD (v5).
  Recherche tous les sous-dossiers et crée des fichiers CHD (v5) dans le dossier dans lequel les fichiers sont placés avec CHDMAN.

* **Extract CHD to CUE**

  Décompresse un fichier CHD (V5) en fichier BIN+CUE. 
  Le format CUE est utilisé par les jeux sur CD. Sur le Raspberry Pi, CHD est pris en charge par TurboGrafx-CD / PC Engine CD, Sega CD / Mega CD et Dreamcast.

* **Extract CHD to GDI**
  Décompresse un fichier CHD (V5) vers GDI. (GDI est un format disque pour Dreamcast).

### macOS

CHDMAN existe pour macOS via Homebrew.

* Installation de [**Homebrew**](https://brew.sh/index_fr)

Vous devez utiliser la commande suivante dans le Terminal :

`/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"`

* Installation de [**rom-tools**](https://formulae.brew.sh/formula/rom-tools)

Vous devez utiliser la commande suivante dans le Terminal un fois Homebrew installé :

`brew install rom-tools`

macOS 10.13 High Sierra minimum est requis.

Voici un lien pour télécharger des fichiers pour automatiser les conversions de format :

![](/tutorials/utilities/rom-conversion/chdman/archive.svg) [ChdScripts.zip](/tutorials/utilities/rom-conversion/chdman/chdscripts.zip)

Dans ce zip, vous trouverez 6 fichiers :

| Nom de fichier | Description |
| :--- | :--- |
| **<span>convertFromChdToCue.sh</span>** | Un fichier .sh qui vous permet de convertir vos roms au format `CHD` en `BIN`/`CUE`. |
| **<span>convertFromChdToGdi.sh</span>** | Un fichier .sh qui vous permet de convertir vos roms au format `CHD` en `GDI`. |
| **<span>convertFromCueToChd.sh</span>** | Un fichier .sh qui vous permet de convertir vos roms au format `BIN`/`CUE` en `CHD`. |
| **<span>convertFromGdiToChd.sh</span>** | Un fichier .sh qui vous permet de convertir vos roms au format `GDI` en `CUE`. |
| **README_EN.txt** |  |
| **README_FR.txt** |  |

#### Contenu du fichier Readme

* **<span>convertFromChdToCue.sh</span>**

  Décompresse un fichier CHD (V5) en fichier BIN+CUE.
  Le format CUE est utilisé par les jeux sur CD. CHD est pris en charge par 3DO, Amiga CD32, Amiga CDTV, Dreamcast, Mega CD, Neo-Geo CD, PC Engine CD, PlayStation et Saturn.

* **<span>convertFromChdToGdi.sh</span>**
  Décompresse un fichier CHD (V5) en fichier GDI.
  Le format GDI est utilisé par les jeux sur disque pour Dreamcast.

* **<span>convertFromCueToChd.sh</span>**

  Compresse tout type de fichiers disques BIN avec un entête CUE vers le format CHD (v5). Recherche tous les sous-dossiers et crée des fichiers CHD (v5) dans le dossier dans lequel les fichiers sont placés avec CHDMAN.

* **<span>convertFromGdiToChd.sh</span>**

  Compresse tout type de fichiers disques BIN avec un entête GDI vers le format CHD (v5). Le format GDI est utilisé par les jeux sur disque pour Dreamcast.

### Linux

Si vous avez la méthode d'installation, modifiez le document et ajoutez vos trouvailles.

Voici un lien pour télécharger des fichiers pour automatiser les conversions de format :

![](/tutorials/utilities/rom-conversion/chdman/archive.svg) [ChdScripts.zip](/tutorials/utilities/rom-conversion/chdman/chdscripts.zip)

Dans ce zip, vous trouverez 6 fichiers :

| Nom de fichier | Description |
| :--- | :--- |
| **<span>convertFromChdToCue.sh</span>** | Un fichier .sh qui vous permet de convertir vos roms au format `CHD` en `BIN`/`CUE`. |
| **<span>convertFromChdToGdi.sh</span>** | Un fichier .sh qui vous permet de convertir vos roms au format `CHD` en `GDI`. |
| **<span>convertFromCueToChd.sh</span>** | Un fichier .sh qui vous permet de convertir vos roms au format `BIN`/`CUE` en `CHD`. |
| **<span>convertFromGdiToChd.sh</span>** | Un fichier .sh qui vous permet de convertir vos roms au format `GDI` en `CUE`. |
| **README_EN.txt** |  |
| **README_FR.txt** |  |

#### Contenu du fichier Readme

* **<span>convertFromChdToCue.sh</span>**

  Décompresse un fichier CHD (V5) en fichier BIN+CUE.
  Le format CUE est utilisé par les jeux sur CD. CHD est pris en charge par 3DO, Amiga CD32, Amiga CDTV, Dreamcast, Mega CD, Neo-Geo CD, PC Engine CD, PlayStation et Saturn.

* **<span>convertFromChdToGdi.sh</span>**
  Décompresse un fichier CHD (V5) en fichier GDI.
  Le format GDI est utilisé par les jeux sur disque pour Dreamcast.

* **<span>convertFromCueToChd.sh</span>**

  Compresse tout type de fichiers disques BIN avec un entête CUE vers le format CHD (v5). Recherche tous les sous-dossiers et crée des fichiers CHD (v5) dans le dossier dans lequel les fichiers sont placés avec CHDMAN.

* **<span>convertFromGdiToChd.sh</span>**

  Compresse tout type de fichiers disques BIN avec un entête GDI vers le format CHD (v5). Le format GDI est utilisé par les jeux sur disque pour Dreamcast.

## Utilisation automatisé

## {.tabset}
### Windows
#### Convertir votre jeu depuis `BIN`/`CUE` ou `GDI` en `CHD`

* Placez "**chdman.exe**" et "**CUE or GDI to CHD.bat**" dans le dossier contenant votre jeu comme ci-dessous.
  Exemple pour le jeu "Grandia (France)" :

![](/tutorials/utilities/rom-conversion/chdman/chdman-win1.png){.full-width}

* Cliquez sur le fichier "**CUE or GDI to CHD.bat**" pour lancer la conversion.

![](/tutorials/utilities/rom-conversion/chdman/chdman-win2.png){.full-width}

* Une fois que la fenêtre CMD ci-dessus s'est fermé automatiquement, votre conversion est terminé.

![](/tutorials/utilities/rom-conversion/chdman/chdman-win3.png){.full-width}

* Vous pouvez supprimer "**chdman.exe**" et "**CUE or GDI to CHD.bat**", votre rom est prête.

![](/tutorials/utilities/rom-conversion/chdman/chdman-win4.png){.full-width}

>Vous pouvez également lancer le fichier .bat pour lancer la conversion de plusieurs jeux d'un coup.
{.is-info}

#### Convertir votre jeu depuis `CHD` en `BIN`/`CUE`

* Placez "**chdman.exe**" et "**Extract CHD to CUE.bat**" dans le dossier contenant votre jeu comme ci-dessous.
  Exemple pour le jeu "Grandia (France)" :



* Cliquez sur le fichier "**Extract CHD to CUE.bat**" pour lancer la conversion.



* Une fois que la fenêtre CMD ci-dessus s'est fermé automatiquement, votre conversion est terminé.



* Vous pouvez supprimer "**chdman.exe**" et "**Extract CHD to CUE.bat**", votre rom est prête.



>Vous pouvez également lancer le fichier .bat pour lancer la conversion de plusieurs jeux d'un coup.
{.is-info}

#### Convertir votre jeu depuis `CHD` en `GDI`

* Placez "**chdman.exe**" et "**Extract CHD to GDI.bat**" dans le dossier contenant votre jeu comme ci-dessous.
  Exemple pour le jeu "Grandia (France)" :



* Cliquez sur le fichier "**Extract CHD to GDI.bat**" pour lancer la conversion.



* Une fois que la fenêtre CMD ci-dessus s'est fermé automatiquement, votre conversion est terminé.



* Vous pouvez supprimer "**chdman.exe**" et "**Extract CHD to GDI.bat**", votre rom est prête.



>Vous pouvez également lancer le fichier .bat pour lancer la conversion de plusieurs jeux d'un coup.
{.is-info}

### Linux / macOS

#### Convertir votre jeu depuis `BIN`/`CUE` en `CHD`

* Placez "**<span>convertFromCueToChd.sh</span>**" dans le dossier contenant votre jeu comme ci-dessous.
  Exemple pour le jeu "Grandia (France)" :

![](/tutorials/utilities/rom-conversion/chdman/chdman-unix1.png){.full-width}

* Dans votre Terminal, lancez le fichier avec `./convertCueToChd.sh` pour lancer la conversion.

![](/tutorials/utilities/rom-conversion/chdman/chdman-unix2.png){.full-width}

* Une fois que la fenêtre du Terminal ci-dessus a terminé, votre conversion est terminée.

![](/tutorials/utilities/rom-conversion/chdman/chdman-unix3.png){.full-width}

* Vous pouvez supprimer "**<span>convertFromCueToChd.sh</span>**", votre rom est prête.

#### Convertir votre jeu depuis `GDI` en `CHD`

* Placez "**<span>convertFromGdiToChd.sh</span>**" dans le dossier contenant votre jeu comme ci-dessous.
  Exemple pour le jeu "Resident Evil - Code - Veronica (France)" :

![](/tutorials/utilities/rom-conversion/chdman/chdman-unix6.png){.full-width}

* Dans votre Terminal, lancez le fichier avec `./convertFromGdiToChd.sh` pour lancer la conversion.

![](/tutorials/utilities/rom-conversion/chdman/chdman-unix7.png){.full-width}

* Une fois que la fenêtre du Terminal ci-dessus a terminé, votre conversion est terminée.

![](/tutorials/utilities/rom-conversion/chdman/chdman-unix8.png){.full-width}

* Vous pouvez supprimer "**<span>convertFromGdiToChd.sh</span>**", votre rom est prête.

#### Convertir votre jeu depuis `CHD` en `BIN`/`CUE`

* Placez "**<span>convertFromChdToCue.sh</span>**" dans le dossier contenant votre jeu comme ci-dessous.
  Exemple pour le jeu "Grandia (France)" :

![](/tutorials/utilities/rom-conversion/chdman/chdman-unix4.png){.full-width}

* Dans votre Terminal, lancez le fichier avec `./convertFromChdToCue.sh` pour lancer la conversion.

![](/tutorials/utilities/rom-conversion/chdman/chdman-unix5.png){.full-width}

* Une fois que la fenêtre du Terminal ci-dessus a terminé, votre conversion est terminée.

![](/tutorials/utilities/rom-conversion/chdman/chdman-unix3.png){.full-width}

* Vous pouvez supprimer "**<span>convertFromChdToCue.sh</span>**", votre rom est prête.

#### Convertir votre jeu depuis `CHD` en `GDI`

* Placez "**<span>convertFromChdToGdi.sh</span>**" dans le dossier contenant votre jeu comme ci-dessous.
  Exemple pour le jeu "Resident Evil - Code - Veronica (France)" :

![](/tutorials/utilities/rom-conversion/chdman/chdman-unix9.png){.full-width}

* Dans votre Terminal, lancez le fichier avec `./convertFromChdToGdi.sh` pour lancer la conversion.

![](/tutorials/utilities/rom-conversion/chdman/chdman-unix10.png){.full-width}

* Une fois que la fenêtre du Terminal ci-dessus a terminé, votre conversion est terminée.

![](/tutorials/utilities/rom-conversion/chdman/chdman-unix11.png){.full-width}

* Vous pouvez supprimer "**<span>convertFromChdToGdi.sh</span>**", votre rom est prête.

## Créer le fichier `.M3U`

En conversion CHD pour les jeux multi-disques, il faut faire un fichier **M3U** pour déclarer tous les disques.

Le fichier au format `.M3U` est une liste des différents CD pour un même jeu qui permet de passer d'un cd à un autre de façon simple en utilisant la combinaison de changement du disque (`Hotkey` + `STICK G.` vers la _GAUCHE_ ou vers la _DROITE_).

Exemple pour le jeu "Grandia (France)" :

* Créez un fichier `M3U` via Notepad++ nommé « Grandia (France).m3u ».
* Dans le fichier, renseignez les fichiers `CHD` du jeu :

```text
Grandia (France) (Disc 1).chd
Grandia (France) (Disc 2).chd
```

* Sur Windows, vous devez activer la vue sur extension pour pouvoir créer ce fichier :
  * Allez dans `Affichage` en haut de la fenêtre de l'Explorateur Windows.
  * Cochez « Extensions de noms de fichiers » en haut à droite.

![](/tutorials/utilities/rom-conversion/chdman/chdman-m3u1.png){.full-width}

* Confirmez la modification de l'extension

![](/tutorials/utilities/rom-conversion/chdman/chdman-m3u2.png){.full-width}

## Crédits

Un grand Merci à **Zet-sensei**, l'une des personnes de l'ombre qui travaillent en préservation dans le patrimoine vidéoludique.  
Vous ne le connaissez pas, mais il été à l'origine ou grandement participant sur des projets majeurs à ce sujet, notamment co-fondateur de No-Intro et Redump dans ceux les plus connus.