---
title: Configuration des contrôleurs Xin-mo
description: 
published: true
date: 2021-09-04T22:33:57.858Z
tags: configuration, xin-mo, xinmo, contrôleurs
editor: markdown
dateCreated: 2021-05-21T08:36:56.651Z
---

## Xin-mo

Les interfaces USB Xin Mo fonctionnent en natif avec Recalbox et gère 2 joueurs.

## Pinout

Prenons comme exemple un panel à 6 boutons + stick + 1 bouton crédit (`SELECT`) + 1 bouton `START` (généralement bouton 1 joueur et 2 joueurs) avec cette disposition pour chaque joueur :

```text
. ↑     Ⓨ Ⓧ Ⓛ  
←  →	 Ⓑ Ⓐ Ⓡ    ($) (1P)
  ↓
```

Il est préférable de rajouter un bouton hotkey dédié : Ⓗ

Vous pouvez mapper les deux joueurs de la même manière, mais ce n'est pas une obligation.

Exemple avec les anciens modèles gérant moins de boutons :

![Branchement ancien du Xin-Mo](/tutorials/controllers/usb-encoders/xin-mo/xinmo_arcade_recalbox.jpg)

Avec les modèles plus récents, le branchement est le suivant :

![Branchement nouveau du Xin-Mo](/tutorials/controllers/usb-encoders/xin-mo/xinmo_arcade_new_recalbox.jpg){.full-width}

## Configuration

## {.tabset}

### Modèles anciens

Avec les modèles anciens, il suffit de faire _**seulement**_ la configuration du joueur 1 directement dans EmulationStation. La configuration du joueur 2 devrait fonctionner si le mappage est bien le même.

>N’essayez pas de configurer le joueur 2, ça va écraser la config du joueur 1.
>
>Les joysticks doivent être configurés sur le D-PAD et non sur joystick dans le menu d'Emulationstation.
{.is-warning}

>La configuration joystick est réservé aux sticks analogiques tels que les manettes PlayStation ou Xbox.
{.is-info}

### Modèles récents

Avec les modèles récents, vous devez mapper les joueurs 1 et 2 comme pour les interfaces  DragonRise.