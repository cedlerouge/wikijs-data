---
title: Recalbox pour votre encodeur de clavier USB
description: 
published: true
date: 2021-09-04T22:34:23.235Z
tags: usb, clavier, encodeur
editor: markdown
dateCreated: 2021-05-21T08:36:50.081Z
---

## Introduction

Xarcade2jstick a été patché pour supporter les encodeurs de clavier. Vous devrez peut-être reconfigurer quelques touches de votre encodeur car malheureusement, les appareils Xarcade de X-gaming n'ont pas été mappés comme les contrôleurs habituels de MAME.

## Ajout de votre encodeur de clavier

Pour l'instant, seule la première génération d'IPAC a été incluse. Si vous disposez d'un autre encodeur de clavier, suivez les étapes suivantes :

* Connectez-vous via [SSH](./../../../tutorials/system/access/root-access-terminal-cli).
* Trouvez le nom du périphérique de votre encodeur avec la commande `ls /dev/input/by-id`. Habituellement, le résultat est suivi d'un kbd. Par exemple : _usb-Ultimarc_IPAC_2_Ultimarc_IPAC_2_9-if01-event-kbd_

>Un seul encodeur peut avoir plusieurs noms possibles, alors essayez-les tous par exemple :
>
>* usb-Cypress_I-PAC_Arcade_Control_Interface-event-kbd -> fonctionne
>* usb-Cypress_I-PAC_Arcade_Control_Interface-if01-event-kbd -> ne fonctionne pas
{.is-info}

* Remontez la [partition du système](./../../../tutorials/system/access/remount-partition-with-write-access) en lecture/écriture.
* Créez un fichier vide qui porte le même nom que votre périphérique de clavier trouvé 2 étapes plus haut avec cette commande :

```shell
touch /recalbox/share_init/system/configs/xarcade2jstick/devicename
```

Dans l'exemple précédent :

```shell
touch /recalbox/share_init/system/configs/xarcade2jstick/usb-Ultimarc_IPAC_2_Ultimarc_IPAC_2_9-if01-event-kbd
```

* Modifiez [recalbox.conf](./../../../basic-usage/getting-started/recalboxconf-file) et ajoutez ceci sur une nouvelle ligne :

```ini
controllers.xarcade.enabled=1
```

* Redémarrer en tapant `reboot`

Recalbox est désormais configurée pour ces périphériques.