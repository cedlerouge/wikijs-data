---
title: Manettes
description: 
published: true
date: 2021-09-04T22:45:26.490Z
tags: manettes
editor: markdown
dateCreated: 2021-05-21T08:06:41.756Z
---

Un souci de manette ? Peut-être trouverez-vous votre bonheur ici.

Voici les catégories disponibles :

[Manettes Nintendo Switch sans fil](nintendo-online-wireless-controllers)

Voici les tutoriels disponibles :

[Connecter une Manette IPEGA](connect-ipega-controller)
[Jouer avec votre smartphone en utilisant la manette virtuelle](play-with-smartphone-using-virtual-gamepad)
[Jouer en utilisant une PSP avec la manette de jeu FuSa](play-using-psp-with-fusa-controller)
[Les pilotes des manettes PS3](ps3-controllers-drivers)
[Utilisation de votre périphérique XArcade](xarcade-usage)