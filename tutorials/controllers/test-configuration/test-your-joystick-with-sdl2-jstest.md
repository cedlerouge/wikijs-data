---
title: Testez votre joystick avec sdl2-jstest
description: 
published: true
date: 2021-09-04T22:29:25.838Z
tags: test, sdl2-jstest, joystick
editor: markdown
dateCreated: 2021-05-21T08:36:37.063Z
---

Si vous avez un soucis avec une  manette, il existe une commande qui permet de voir ses propriétés.

## Description

Pour tester votre manette, vous devez :

* Connectez-vous via [SSH](./../../../tutorials/system/access/root-access-terminal-cli)
* Exécuter la commande suivante :

```shell
sdl2-jstest
```

## Lister les détails de vos manettes

Commande à utiliser :

```shell
sdl2-jstest --list
```

Résultat :

```shell
Joystick Name: 'DragonRise Inc.   Generic   USB  Joystick'
Joystick Path: '/dev/input/event0'
Joystick GUID: 030000001008000001e5000010010000
Joystick Number: 0
Number of Axes: 2
Number of Buttons: 10
Number of Hats: 0
Number of Balls: 0
GameController:
not a gamepad
Axis code 0: 0
Axis code 1: 1
Button code 0: 288
Button code 1: 289
Button code 2: 290
Button code 3: 291
Button code 4: 292
Button code 5: 293
Button code 6: 294
Button code 7: 295
Button code 8: 296
Button code 9: 297​
```

## Tester votre manette

Commande à utiliser :

```shell
sdl2-jstest -e 0
```

Ici, 0 est le numéro de votre manette détectée avec la commande `sdl2-jstest --list`.

Vous trouverez un exemple avec une manette N64 en allant [ici](https://forum.recalbox.com/topic/9016/a-lire-manettes-n64).