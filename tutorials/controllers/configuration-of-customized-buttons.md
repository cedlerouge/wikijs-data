---
title: Configuration des boutons personnalisés
description: 
published: true
date: 2021-09-04T22:30:34.376Z
tags: configuration, boutons, personnalisés
editor: markdown
dateCreated: 2021-05-21T08:06:35.829Z
---

Vous aurez tout ce qui concerne les configurations spécifiques des manettes.

Voici les tutoriels disponibles :

[Comment personnaliser le mapping d'une manette](how-to-customize-controller-mapping)
[Configuration des manettes N64](n64-controller-configuration)
[Créer une configuration personnalisée par émulateur](create-custom-configuration-per-emulator)