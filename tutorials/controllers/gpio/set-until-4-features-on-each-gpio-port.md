---
title: Attribuer jusqu'à 4 fonctions sur chaque port GPIO
description: 
published: true
date: 2021-09-04T22:44:53.796Z
tags: gpio, fonctions
editor: markdown
dateCreated: 2021-05-21T08:36:07.312Z
---

Si vous voulez construire votre borne d'arcade avec un Raspberry Pi, vous pouvez ajouter des boutons supplémentaires et les affecter à des fonctions spéciales. Voici comment cela fonctionne.

## À propos des ports GPIO Raspberry Pi

Sur un Raspberry Pi, vous disposez de plusieurs ports GPIO, du GPIO2 au GPIO26 :

![GPIO](/tutorials/controllers/gpio/gpio-functions/pi-gpio.png)

Lorsque vous voulez connecter un bouton, vous devez le brancher entre un port GPIOxx et un port GND. Veuillez noter que les numéros GPIO sont utilisés, et non pas les numéros PIN (1 à 40) !

## Comment l'installer sur Recalbox

1. Ajoutez ceci dans le fichier `/recalbox/share/system/recalbox.conf`:

```ini
# Uncomment to enable custom GPIO script feature
system.rpi.gpio=enable
```

2. Modifiez le fichier `/recalbox/share/system/configs/retroarch/retroarchcustom.cfg` et ajouter cette ligne :

```ini
network_cmd_enable = true
```
3. Configurez le fichier `/recalbox/share/system/configs/rpi-gpio.ini` (voir les commandes disponibles ci-dessous).
4. Redémarrez Recalbox et profitez-en !

## Syntaxe du ficher rpi-gpio.ini

Vous devez commencer par [GPIOxx] et remplacer `xx` par le numéro GPIO en question.  
Ajoutez ensuite ces 4 lignes :

```ini
quick=
standard=
hold=
release=
```

`quick` est une action qui sera exécutée si vous appuyez et relâchez rapidement le bouton.
`standard` est une action qui sera exécutée si vous maintenez et relâchez le bouton pendant environ 1 seconde.  
`hold` est une action qui sera exécutée si vous maintenez le bouton enfoncé pendant une longue période (plus de 2 secondes).
`release` est une action qui sera exécutée si vous relâchez le bouton après un événement `hold`.

Toutes les actions sont facultatives. Si vous ne souhaitez pas attribuer de fonction, laissez simplement un blanc après `=`

Exemple :

```ini
[GPIO26]
quick=VOLUP
standard=VOLDOWN
hold=VOLMUTE
release=
[GPIO16]
quick=RESET
standard=QUIT
hold=
release=
```

## Actions disponibles

### Actions globales

* VOLUP : Augmenter le volume
* VOLDOWN : Diminuer le volume
* VOLMUTE : Volume muet / non muet
* QUIT : Quitter l'émulateur actuel et retourner à EmulationStation

### Actions spécifiques aux émulateurs RetroArch

* CHEAT_INDEX_MINUS
* CHEAT_INDEX_PLUS
* CHEAT_TOGGLE
* DISK_EJECT_TOGGLE
* DISK_NEXT
* DISK_PREV
* FAST_FORWARD
* FAST_FORWARD_HOLD
* FRAMEADVANCE
* FULLSCREEN_TOGGLE
* GRAB_MOUSE_TOGGLE
* LOAD_STATE
* MENU_TOGGLE
* MOVIE_RECORD_TOGGLE
* NETPLAY_FLIP
* OVERLAY_NEXT
* PAUSE_TOGGLE
* RESET
* REWIND
* SAVE_STATE
* SCREENSHOT
* SHADER_NEXT
* SHADER_PREV
* SLOWMOTION
* STATE_SLOT_MINUS
* STATE_SLOT_PLUS