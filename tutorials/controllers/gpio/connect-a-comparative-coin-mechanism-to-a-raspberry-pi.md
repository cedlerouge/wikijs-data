---
title: Brancher un monnayeur comparatif sur un Raspberry Pi
description: 
published: true
date: 2021-09-04T22:41:31.888Z
tags: monnayeur, comparatif, mécanisme
editor: markdown
dateCreated: 2021-05-21T08:35:35.017Z
---

Ce tutoriel reprend des éléments de la notice fournie avec [ce monnayeur exclusif](http://www.smallcab.net/monnayeur-comparateur-facade-silver-p-902.html).

## Principe de fonctionnement du monnayeur

Le monnayeur comparateur fonctionne par comparaison entre la pièce insérée et la pièce dite « témoin » installée en permanence dans le monnayeur. Le monnayeur analyse la signature électronique de la pièce insérée et la compare avec la signature électronique de la pièce témoin.

Si la pièce insérée a une signature proche de la pièce témoin, la pièce est acceptée, sinon la pièce est refusée. Lorsqu'une pièce est acceptée, le monnayeur génère une impulsion électrique à destination du Raspberry Pi.

Cette impulsion est reconnue par Recalbox (bouton `SELECT`) et un crédit de jeu est alors octroyé au joueur.

>Il est possible d'utiliser des jetons métalliques (« _token_ ») à la place des pièces.
{.is-info}

Très simple à faire, à base de quatre fils :

* Rouge : +12V à brancher sur [une alimentation 12V/1A](https://www.amazon.fr/dp/B00B0T5D5W/).
* Blanc : "Insert coin" à brancher sur le GPIO (ou contrôleur USB), correspondant au bouton `SELECT` sous Recalbox.

>Obligatoirement par l'intermédiaire d'un "relais" !
{.is-danger}

* Noir : masse à brancher sur le 0V de l'alimentation 12V/1A.
* Gris : compteur (non utilisé ici).

## Installation avec un Raspberry Pi

Vous l'aurez compris, l'utilisation de CE monnayeur avec un Raspberry Pi nécessite obligatoirement l'utilisation d'un composant électronique intermédiaire appelé « relais » ou « hack clavier » pour les puristes.

Voici donc une solution à moindre frais pour fabriquer vous même ce "hack clavier".

## Liste du matériel à prévoir

* 1 bon fer à souder, du fil pour faire les pistes, de l'étain et de l'huile de coude
* 1 [relais 12V](https://www.amazon.fr/dp/B013SPRXQC) (5 fils)
* 1 [carte prototype](https://www.amazon.fr/dp/B01GUZ5L6G/)
* 2 [borniers](https://www.amazon.fr/dp/B00I00OHHY)

## Câblage

>N'hésitez surtout pas à tester votre carte AVANT de la raccorder à votre Raspberry et votre monnayeur !
{.is-warning}

Le câblage du monnayeur avec un Raspberry Pi nécessite donc l'installation de ce "hack clavier" ENTRE le monnayeur ET le GPIO de votre Raspberry Pi.

Les 2 broches du « Bornier 1 » sont à connecter :

* L'une sur le fil "0V" de votre alimentation 12V/1A.
* L'autre sur le fil "blanc" de votre monnayeur.

Les 2 broches du « bornier 2 » sont à connecter :

* L'une sur la pin « GND » du connecteur GPIO du Raspberry Pi.
* L'autre sur une pin « GPIOxx » du connecteur GPIO du Raspberry Pi (bouton `SELECT`).

Reportez-vous à la documentation spécifique à votre modèle pour bien identifier le connecteur GPIO du Raspberry Pi et la position des broches « GND » et « GPIOxx » au sein de ce connecteur.