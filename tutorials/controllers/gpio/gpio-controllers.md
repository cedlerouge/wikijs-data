---
title: Les contrôleurs GPIO
description: 
published: true
date: 2021-09-04T22:42:40.900Z
tags: gpio, contrôleurs
editor: markdown
dateCreated: 2021-08-07T15:10:54.128Z
---

Recalbox est compilé avec le pilote `mk_arcade_joystick_gpio` qui permet de gérer un contrôleur connecté directement sur les GPIO du Raspberry Pi. Si vous avez prévu de vous construire un Bartop, une borne d'arcade ou un stick d'arcade portable, vous n'avez pas besoin d'investir dans un contrôleur usb.

Le pilote peut gérer jusqu'à 2 contrôleurs composés chacun d'un joystick 4 directions et de 9 boutons.

Si vous utilisez un RPi1 révision B, reportez-vous au pinout `mk_arcade_joystick`.

## Pinout

Prenons comme exemple un panel à 7 boutons avec cette disposition :

```text
. ↑      Ⓨ Ⓧ Ⓛ  
←  →     Ⓑ Ⓐ Ⓡ Ⓗ
  ↓  
```

Avec

```text
Ⓡ = Gâchette droite = TR  |  Ⓛ = Gâchette gauche = TL  |  Ⓗ = HK = Hotkey
```

Sur RPI B+, RPI2 et RPi3, vous devez connecter vos boutons en suivant ce pinout :

(image manquante, si vous avez une image valide, merci de l'ajouter)

>Le bas de l'image correspond au côté du Raspberry Pi où se trouvent les ports USB.
{.is-info}

Vous pouvez connecter vos boutons directement à la masse, étant donné que le pilote active les _**gpio internal pullups**_.

## Configuration

* Dans le fichier [recalbox.conf](./../../../basic-usage/getting-started/recalboxconf-file), activez le pilote GPIO en réglant `controllers.gpio.enabled` sur 1 : `controllers.gpio.enabled=1` et vous êtes prêt à jouer !

Les GPIO sont configurés _out the box_ dans l'interface et les différents systèmes émulés.