---
title: Utilisation des Joy-Cons
description: Comment synchroniser et utiliser les Joy-Cons dans Recalbox
published: true
date: 2021-09-04T22:51:59.989Z
tags: joy-con, 7.2+
editor: markdown
dateCreated: 2021-05-21T08:46:02.002Z
---

Nous allons voir comment synchroniser une paire de Joy-Cons et comment s'en servir séparément ou ensemble (combiné).

## Deux Joy-Cons en 2 manettes séparés

Sur chaque Joy-Con, il y a un petit bouton qui permet d'allumer et d'éteindre le Joy-Con mais ce bouton permet aussi de faire une synchronisation du Joy-Con avec votre boitier.

![Bouton de synchronisation sur chaque Joy-Con](/tutorials/controllers/controllers/switch-joycons/joyconconfig1.jpg)

Dans Recalbox, dans vos listes de systèmes disponibles, appuyez sur **Start** de votre manette habituelle et allez dans **RÉGLAGES MANETTES**.

![](/tutorials/controllers/controllers/switch-joycons/joyconconfig2.png){.full-width}

Dans ce menu, faites **ASSOCIER UNE MANETTE BLUETOOTH**.

![](/tutorials/controllers/controllers/switch-joycons/joyconconfig3.png){.full-width}

Une fois la recherche de périphériques Bluetooth lancé, appuyez sur le bouton de synchronisation du Joy-Con à associer comme indiqué sur la première image. Vous devez avoir les 4 lumières latérales en train de faire des allers-retours.

![](/tutorials/controllers/controllers/switch-joycons/joyconconfig4.png){.full-width}

Une fois que la recherche de périphériques Bluetooth est terminée, vous devriez voir votre Joy-Con dans la liste.

![](/tutorials/controllers/controllers/switch-joycons/joyconconfig5.png){.full-width}

Sélectionnez le Joy-Con et validez. Une fois que vous aurez le message **MANETTE ASSOCIÉE**, vous devez appuyer sur les boutons SL et SR. Votre Joy-Con doit avoir 1 lumière latérale allumée.

Faites de même avec le second Joy-Con en reprenant les mêmes étapes vues ci-dessus.

![](/tutorials/controllers/controllers/switch-joycons/joyconconfig6.png){.full-width}

Validez le second Joy-Con pour finir son association et appuyez bien sur SL+SR en dernière étape. Une fois que vous aurez associé vos 2 Joy-Cons, ils seront visibles dans la liste des manettes.

![](/tutorials/controllers/controllers/switch-joycons/joyconconfig7.png){.full-width}

## Deux Joy-Cons en une seule manette (mode combiné)

Si vous souhaitez utiliser les 2 Joy-Cons comme une seule manette, il vous devez associer de nouveau les Joy-Cons souhaités et, quand le Joy-Con est associée, n'appuyez pas sur SL et SR mais laissez le Joy-Con avec les 4 lumières latérales en train de clignoter et faites de même avec le Joy-Con droit. Une fois que les 2 Joy-Cons ont chacune leurs 4 lumières latérales en train de clignoter, il vous suffit d'appuyer sur les boutons ![](/tutorials/controllers/controllers/switch-joycons/switch_lb.png) et ![](/tutorials/controllers/controllers/switch-joycons/switch_rb.png) du Joy-Con droit pour avoir les 2 Joy-Cons associés en une seule manette !

![](/tutorials/controllers/controllers/switch-joycons/joyconconfig8.png){.full-width}

À vous les joies de jouer avec les Joy-Cons !