---
title: Utilisation de votre périphérique XArcade
description: 
published: true
date: 2021-09-04T22:49:15.977Z
tags: xarcade
editor: markdown
dateCreated: 2021-05-21T08:35:29.196Z
---

## Dispositifs compatibles et exigences

Les sticks suivants sont supportés :

* X-Arcade Modèle Tankstick
* X-Arcade Dual Stick

## Utiliser le stick XArcade sur Raspberry Pi 4

Sur Raspberry Pi 4, le binaire du stick XArcade n'est pas le bon. Pour pouvoir utiliser le stick XArcade sur Raspberry Pi 4, vous devez exécuter quelques commandes via [SSH](./../../system/access/root-access-terminal-cli) et monter la [partition du système](./../../system/access/remount-partition-with-write-access) en lecture/écriture. Les commandes sont les suivantes :

```text
cp /usr/bin/xarcade2jstick /usr/bin/xarcade2jstick-old
cp xarcade2jstick-rpi4 /usr/bin/xarcade2jstick
chmod 755 /usr/bin/xarcade2jstick
```