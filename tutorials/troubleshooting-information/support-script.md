---
title: Script de support
description: 
published: true
date: 2021-09-04T18:11:30.869Z
tags: script, support
editor: markdown
dateCreated: 2021-05-21T08:10:43.738Z
---

Un script nommé recalbox-support&#46;sh a été ajouté afin d'aider les développeurs sur les problèmes que vous pouvez rencontrer avec vos périphériques.

Ce fichier donne les informations suivantes sur :

* joystick
* kodi
* lirc
* system (lsmod, lsusb, tv service, es_settings, recalbox.conf, recalbox.log,config, df, etc...)

## Utilisation

Son utilisation s'effectuer via le Web manager.

* Rendez-vous dans le [Webmanager](./../../basic-usage/features/webmanager).
* Cliquez sur `Dépannage` sur la gauche.
* Cliquez sur `Exécuter le script`.

Au bout d'un moment, un lien vous sera donné. Copiez le lien et collez-le dans un message adressé à la personne qui vous l'a demandé.