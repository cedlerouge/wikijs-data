---
title: Afficher le contenu de la mémoire tampon Linux
description: 
published: true
date: 2021-09-04T18:10:17.230Z
tags: linux, contenu, mémoire
editor: markdown
dateCreated: 2021-05-21T08:10:25.747Z
---

Cette commande affiche le contenu de la mémoire tampon de message du noyau de Linux. Il peut vous être demandé à l'exécuter en fonction de vos problèmes.

## Utilisation

* Connectez-vous [via SSH](./../../tutorials/system/access/root-access-terminal-cli)
* Débranchez votre périphérique USB.
* Tapez la commande suivante : 

```shell
dmesg
```

* Prenez note de ce que la commande va vous afficher.
* Branchez votre périphérique USB.
* Tapez de nouveau la commande suivante :

```shell
dmesg
```

* Une fois la commande exécutée, vous pouvez copier/coller les informations nécessaires là où il vous a été demandé de l'utiliser.

## Exemple pour un dongle wifi

```text
 [    2.817251] usb 1-1.4: new high-speed USB device number 5 using dwc_otg
 [    2.918993] usb 1-1.4: New USB device found, idVendor=7392, idProduct=7811
 [    2.919019] usb 1-1.4: New USB device strings: Mfr=1, Product=2, SerialNumber=3
 [    2.919030] usb 1-1.4: Product: 802.11n WLAN Adapter
 [    2.919041] usb 1-1.4: Manufacturer: Realtek
 [    2.919052] usb 1-1.4: SerialNumber: 00e04c000001
```

## Exemple pour un dongle Bluetooth avec un firmware manquant :

```text
[ 194.192286] usb 1-1.4: new full-speed USB device number 4 using dwc_otg
[ 194.297324] usb 1-1.4: New USB device found, idVendor=0cf3, idProduct=3000
[ 194.297350] usb 1-1.4: New USB device strings: Mfr=0, Product=0, SerialNumber=0
[ 194.303210] usb 1-1.4: Direct firmware load for ath3k-1.fw failed with error -2
[ 194.303265] Bluetooth: Firmware file "ath3k-1.fw" not found
[ 194.303307] ath3k: probe of 1-1.4:1.0 failed with error -2
```