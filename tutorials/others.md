---
title: 📌 Autres
description: 
published: true
date: 2021-09-04T17:56:37.498Z
tags: tutoriel, autres
editor: markdown
dateCreated: 2021-05-21T07:52:23.791Z
---

Ici se trouve les tutoriels "inclassables" qui peuvent s'avérer utiles.

Voici les tutoriels disponibles :

[Ajouter un bouton on/off à votre Recalbox](add-on-off-button-to-your-recalbox)
[Commandes Linux utiles](useful-linux-commands)
[Configuration pour le GameHat](gamehat-configuration)
[Désactiver la puce T2 sur Mac](disable-mac-t2-chip)
[Hyperion sur Recalbox](hyperion-on-recalbox)
[Installation du boitier NesPi4Case](nespi4case-installation)
[Mise en marche/arrêt du Switch Mausberry](add-mausberry-switch-on-off-button)
[Modifier la langue de RetroArch](change-retroarch-language)
[Utilisation d'EmulationStation au clavier / à la souris](emulationstation-use-with-keyboard-and-mouse)