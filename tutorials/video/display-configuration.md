---
title: Configuration d'affichage
description: 
published: true
date: 2021-09-04T22:15:21.863Z
tags: configuration, affichage
editor: markdown
dateCreated: 2021-05-21T08:11:35.419Z
---

Vous y trouverez ici des tutoriels concernant la configuration de l'affichage.

Voici les tutoriels disponibles :

[Configurer un écran externe](external-screen-configuration)
[Guide complet sur la configuration vidéo](complete-video-configuration-guide)
[Réglage de la taille de l'image en utilisant l'overscan sous TFT](image-size-settings-overscan-tft)