---
title: Connectez votre Recalbox à un CRT avec composite
description: 
published: true
date: 2021-09-04T22:19:45.040Z
tags: crt, composite, écran
editor: markdown
dateCreated: 2021-05-21T08:43:12.538Z
---

Si vous souhaitez connecter votre Recalbox sur un écran CRT, vous aurez besoin d'un câble mini-jack vers RCA comme celui-ci :

![RPi Audio/Video Socket](/tutorials/video/crt/rpi_av_socket.jpg)

>Sur les câbles de caméscopes, la sortie vidéo pourrait être sur la prise audio droite (rouge).
>
>De plus, le type de câble nécessaire n'étant pas vraiment répandu, il est préférable de tester au multimètre que le câble utilisé correspond bien au schéma proposé ci-dessus (une inversion de la masse et de la vidéo sur le jack entraine une image noir & blanc sautillante, signe d'un câble inadapté).
{.is-warning}

## Configuration

* Modifiez le fichier [recalbox-user-config.txt](./../../../tutorials/system/modification/configtxt-configuration) en commentant toutes les lignes commençant par `hdmi_` avec le symbole `#`, et ajoutant le `sdtv_mode` supporté :

```ini
sdtv_mode=0    Normal NTSC
sdtv_mode=1    Japanese version of NTSC – no pedesta
sdtv_mode=2    Normal PAL
sdtv_mode=3    Brazilian version of PAL – 525/60 rather than 625/50, different subcarrier
```

* Pour avoir un meilleur son par la sortie composite, activez le pilote audio expérimental pour le Raspberry Pi :

```ini
audio_pwm_mode=2
```

* Ce mode peut créer un ralentissement global de Recalbox. Si c'est le cas, passer la ligne en commentaire et forcer la sortie audio sur le jack :

```ini
#audio_pwm_mode=2
hdmi_drive=1
```

* Finalement, ajoutez `hdmi_ignore_hotplug=1` pour forcer le composite. Votre ficher recalbox-user-config.txt devrait ressembler à ça :

```ini
sdtv_mode=2
hdmi_ignore_hotplug=1
audio_pwm_mode=2
```

* Ensuite modifiez [recalbox.conf](./../../../basic-usage/getting-started/recalboxconf-file) et réglez `global.videomode` à `default`:

```ini
global.videomode=default
```

* Si vous utilisez l'émulateur n64, pensez aussi à commenter cette ligne :

```ini
n64.videomode=DMT 9 HDMI
```

* et décommentez celle-ci :

```ini
n64.videomode=default
```

* Pour finir, désactivez le lissage des jeux et tous les shaders dans le menu EmulationStation (`START` -> `OPTIONS DES JEUX`) ce qui vous permettra d'avoir le rendu original sur tous les systèmes !

## Tout est lent avec le mode composite

`audio_pwm_mode=2` améliore le son mais peut ralentir le système en mode composite, en particulier sur Pi zéro. Dans ce cas, utilisez le mode suivant :

```ini
audio_pwm_mode=0
```