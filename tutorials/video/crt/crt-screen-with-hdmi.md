---
title: Connectez votre Recalbox à un écran CRT en utilisant la sortie HDMI
description: 
published: true
date: 2021-09-04T22:18:43.578Z
tags: crt, hdmi, écran
editor: markdown
dateCreated: 2021-05-21T08:43:25.006Z
---

Pour jouer à sa Recalbox sur un écran CRT connecté en HDMI, plusieurs choix sont possibles, selon les connectiques de son écran.

* Connexion via la prise HDMI à l'aide d'un adaptateur HDMI / VGA ==> VGA / BNC
* Connexion via la prise HDMI à l'aide d'un adaptateur HDMI / VGA ==> VGA / PERITEL

## Matériel

### Connecter sa Recalbox sur un écran CRT disposant d'entrée RGB en BNC, via HDMI

Beaucoup d'écrans CRT professionnels disposent d'entrée RGB en BNC, comme sur cette image en Line 3 (Input).

* Nous aurons donc 3 fils pour les 3 couleurs (Rouge, Vert, Bleu), ainsi que 2 fils pour la synchronisation (verticale et horizontale)

![](/tutorials/video/crt/sony-trinitron.jpg)

### Connectiques

* Vous aurez besoin d'un adaptateur comme [celui-ci](https://www.amazon.fr/dp/B00NBUTHJG/).

![Adaptateur HDMI VGA](/tutorials/video/crt/hdmi-to-vga-adapter.png)

* Une fois connecté au port HDMI de votre Recalbox, vous aurez une prise VGA en sortie, et un jack audio.

>Vous aurez besoin d'une alimentation externe micro-usb, 1A devrait suffire.
{.is-danger}

* Il vous faudra ensuite un adaptateur VGA ==> BNC comme [celui-ci](https://www.amazon.fr/dp/B0033AF5Y0/).

![Adaptateur VGA BNC](/tutorials/video/crt/vga-to-bnc-adapter.png)

* N'ayant pas trouvé d'adaptateur VGA / BNC avec sortie BNC femelle, j'ai dû utiliser 5 connecteurs BNC mâle / mâle, trouvable en boutique spécialisée d'électronique.

![BNC male male](/tutorials/video/crt/bnc-male.png)

## Logiciel

### Configuration du boot de la Recalbox

Les résolutions acceptées par ce type d'écran sont différentes selon le modèle, mais en général, nous voulons :

* Du 480i pour EmulationStation (mon écran ne supporte pas le 480p, et les textes sont illisibles en 240p).
* Du 240p pour les jeux (résolution de sortie pour quasiment toutes les consoles de cette époque).

Voila à quoi devrait ressembler votre fichier [recalbox-user-config.txt](./../../../tutorials/system/modification/configtxt-configuration) :

```ini
# Définition de la résolution du mode custom DMT 87 HDMI pour la résolution des jeux
hdmi_cvt=1920 240 60 1 1 0 0
# Ignore le EDID de votre TV (le CRT peut envoyer de mauvaises informations)
hdmi_ignore_edid=0xa5000080
# Force le mode d'encodage des pixels, 2 correspond à Full RGB
hdmi_pixel_encoding=2
# Désactive le safe mode au boot du raspberry pi
avoid_safe_mode=1
# Ne pas désactiver l'overscan
disable_overscan=0
# Evite les interférences de l'adaptateur en boostant le signal HDMI
config_hdmi_boost=4
# Force le son sur HDMI
hdmi_drive=2
# Selectionne le groupe HDMI CEA
hdmi_group=1
# Résolution CEA 6 = 480 interlaced
hdmi_mode=6

# Kernel utilisé
kernel=zImage
# Utiliser le mode HDMI même si aucun câble n'est connecté
hdmi_force_hotplug=1
# paramètres mémoire
gpu_mem_256=128
gpu_mem_512=256
gpu_mem_1024=512

# Paramètre lié à l'audio
dtparam=audio=on
# Permet de temporiser le boot
boot_delay=3
```

Quelques explications :

* Le Raspberry Pi démarrera en mode `hdmi_group=1` et `hdmi_mode=6`, ce qui correspond à du 480i (interlaced).
* Si le boot s'effectue directement en 240p, les textes des menus et des jeux seraient illisibles.
* La ligne `hdmi_cvt=1920 240 60 1 1 0 0` spécifie une résolution de 1920 x 240 x 60 Hz, qui ne servira pas pour le boot, mais sera enregistrée en tant que mode `DMT 87 HDMI`, que nous utiliserons ultérieurement pour le lancement des jeux.

Voici un peu plus d'explication sur cette ligne :

```ini
hdmi_cvt=<width> <height> <framerate> <aspect> <margins> <interlace> <rb>
width        width in pixels
height       height in pixels
framerate    framerate in Hz
aspect       aspect ratio 1=4:3, 2=14:9, 3=16:9, 4=5:4, 5=16:10, 6=15:9
margins      0=margins disabled, 1=margins enabled
interlace    0=progressive, 1=interlaced
rb           0=normal, 1=reduced blanking
```

### Configuration du fichier recalbox.conf

Bien que le Raspberry Pi boot en mode vidéo CEA 6 (via le fichier recalbox-user-config.txt), il est préférable de forcer cette configuration pour EmulationStation :

```ini
system.es.videomode=CEA 6 HDMI
```

Nous devons ensuite préciser la résolution de boot pour nos émulateurs, de façon globale :

```ini
global.videomode=DMT 87 HDMI
```

Tous les émulateurs seront donc lancés en mode `DMT 87 HDMI` (mode "custom") contenant via le `hdmi_cvt` du fichier `recalbox-user-config.txt`, la résolution 1920 x 240x 60Hz.

>Petite explication pour les 1920 pixels de large :
>
>Pour faire court, un écran CRT n'a besoin que d'un nombre de pixels vertical (240 ici) et d'un taux de rafraîchissement vertical (60 Hz ici).
>
>Nous spécifions donc 1920 pixels de large, car 1920 est un multiple de 160, 320, 384... qui sont des résolutions souvent rencontrées.
>
>Pour les résolutions n'étant pas des multiples, nous devrons calculer avec RetroArch l'overscan, c'est-à-dire les bandes noires nécessaires autour de l'écran pour garder le ratio original.
>
>Tout doit passer dans cette résolution. Mais vous pouvez spécifier une résolution différente par console, avec la ligne suivante :
{.is-info}

```ini
dreamcast.videomode=hdmi_cvt 320 240 60 1 1 0 0
```

### Configuration du viewport RetroArch (garder le ratio d'origine et caler l'image sur son écran)

#### Configuration par système

Lors du lancement d'un jeu, celui-ci doit donc démarrer dans une résolution de 320 x 240 x 60 Hz pixels. Nous pouvons spécifier, pour chaque système, la taille d'affichage réelle et le décalage si besoin.

Ces fichiers de configuration sont à créer dans votre dossier partagé : `/recalbox/share/system/configs/retroarch/`.
Ils devront contenir le nom du système (le même que dans le dossier roms), par exemple : `nes.cfg`, ou `mastersystem.cfg`.

Voici un contenu de fichier `nes.cfg` :

```ini
aspect_ratio_index = "22"
video_smooth = "false"
video_scale_integer = "false"
video_threaded = "false"
custom_viewport_width = "1680"
custom_viewport_height = "224"
custom_viewport_x = "118"
custom_viewport_y = "17"
```

* Pour régler le positionnement de l'écran, il suffit de changer la valeur `custom_viewport_x` et `custom_viewport_y` directement en jeu via le menu RetroArch (touches `Hotkey` + `B`), puis de recopier ces valeurs dans le fichier .cfg du système. 
* Voici un exemple des hauteurs et largeurs pour CRT :

```ini
nes.cfg
custom_viewport_width = "1680"
custom_viewport_height = "224"
        
snes.cfg
custom_viewport_width = "1792"
custom_viewport_height = "224"

gb.cfg gbc.cfg gamegear.cfg
custom_viewport_width = "960"
custom_viewport_height = "144"

gba.cfg
custom_viewport_width = "1440"
custom_viewport_height = "160"

megadrive.cfg neogeo.cfg segacd.cfg sega32x.cfg fba_libretro.cfg
custom_viewport_width = "1920"
custom_viewport_height = "224"

mastersystem.cfg
custom_viewport_width = "1536"
custom_viewport_height = "192"

psx.cfg n64.cfg
custom_viewport_width = "1920"
custom_viewport_height = "240"

pcenginecd.cfg
custom_viewport_width = "1760"
custom_viewport_height = "240"
```

#### Configuration par jeux

Pour l'arcade, j'utilise fba_libretro. Chaque jeu ou presque ayant une résolution différente, j'ai défini un fichier par jeu (pour garder le ratio original).

Même manipulation que par système, sauf que les fichiers se situent dans votre dossier partagé : `/recalbox/share/system/configs/retroarch/fba_libretro/`

Le contenu du fichier est identique à la configuration par système. Le nom du fichier doit être sous la forme `rom.zip.cfg`. Par exemple : `1944.zip.cfg`.

### Cas particuliers

#### AdvanceMame

Pour les jeux arcade Mame, le core AdvanceMame à la particularité de calculer automatiquement la résolution de chaque jeu, en l'adaptant à votre résolution d'écran.

Ainsi, nous nous passons d'effectuer une configuration par jeu (comme c'est le cas pour fba_libretro).

### Thèmes EmulationStation

Le thème doit être réalisé en 4/3 pour avoir une image bien cadrée, nous vous proposons [un thème](https://forum.recalbox.com/topic/6580/release-wip-theme-recalbox-multi-help-needed) crée par Supernature2K.

Ce thème est configurable, et s'adapte parfaitement aux CRT.

![Thème Recalbox Multi](/tutorials/video/crt/crt-theme.png)