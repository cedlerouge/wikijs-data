---
title: Configurez votre écran cathodique sur Raspberry Pi 4
description: Comment rejouer à Recalbox sur vos écrans cathodiques sur Raspberry Pi 4.
published: false
date: 2021-09-10T23:14:27.563Z
tags: video, crt, screen, cathodique, pixel perfect, trinitron, pvm, 240p, 224p, 15khz
editor: markdown
dateCreated: 2021-09-10T22:09:46.057Z
---

## Présentation

En tant qu'utilisateurs de Recalbox, vous savez à quel point il est bon de ressentir cette nostalgie en rejouant aux jeux qui ont bercés notre enfance.

Jouer avec une manette PS4 sur la TV OLED du salon nous permet déjà de retrouver et de partager ce plaisir du jeu retro.

Cependant, certains d'entre nous ont une soif insatiable qui les oblige à aller toujours plus loin dans la recherche d'une expérience la plus proche possible de originale.

Dans cette recherche sans fin, l'affichage, ou l'image du jeu est sans doute le point le plus important.

Et quoi de plus proche de l'original que de jouer sur les écrans CRT de l'époque ?

## Recalbox sur votre écran CRT

Recalbox est capable de reproduire la résolution et les fréquences exactes de la plupart des consoles et des jeux d'arcade de l'époque, sur votre TV CRT (cathodique).

Équipez vous d'un VGA666 LINK et un câble Péritel, ou un RGBPi LINK.

Une fois le VGA-666 ou le RGBPi branché a votre Rasbperry Pi 4 et a la TV, une seule ligne de configuration suffit a activer le mode **Recalbox CRT**:

```
system.crt=vga666
```

**C'est tout**, après avoir redémarre la Recalbox, le système se configure pour envoyer l'image directement sur votre TV.

------------------

## Compatibilité matérielle

* **VGA666**: TV CRT avec câble modifié, Ecran CRT VGA, PVM
Le VGA666 vous permet de branche un câble VGA (anciens écrans PC cathodiques). Avec un câble VGA->PERITEL modifié, il est tout a fait compatible avec les TV CRT
* **RGBPi**: TV CRT, PVM
Le RGBPi est la solution clé en main pour brancher votre Recalbox à votre CRT.


## Configuration intermédiaire
```
system.crt=vga666
# Décalage de l'image vers la gauche (négatif) ou vers la droite (positif)
system.crt.horizontal_offset=-50
# Décalage de l'image vers le haut (négatif) ou vers le bas (positif)
system.crt.horizontal_offset=-2

system.crt=vga666
```

## Configuration avancée

La configuration des modes d'affichage par systèmes et par jeux arcade se trouve dans le dossier `/recalbox/system/configs/crt/`

* `modes.txt` liste les modes d'affiches pouvant être utilisés
* `system.txt` définit pour chaque système le mode a utiliser
* `arcade_games.txt` définit pour chaque jeux arcade le mode a utiliser

Pour surcharger ou remplacer ces configurations, il vous suffit de créer les fichiers correspondants dans le dossier `/recalbox/share/system/configs/crt/`.

Ces fichiers de surcharge ne doivent contenir que les modes, systèmes ou jeux que vous voulez surcharger ou ajouter.


Structure des fichiers :
┣ 📁 recalbox
┃ ┣ 📁 system
┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┣ 📁 crt
┃ ┃ ┃ ┃ ┣ 🗒 arcade_games.txt (readonly)
┃ ┃ ┃ ┃ ┣ 🗒 modes.txt (readonly)
┃ ┃ ┃ ┃ ┣ 🗒 systems.txt (readonly)
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 crt
┃ ┃ ┃ ┃ ┃ ┣ 🗒 arcade_games.txt
┃ ┃ ┃ ┃ ┃ ┣ 🗒 modes.txt
┃ ┃ ┃ ┃ ┃ ┣ 🗒 systems.txt

### Structure des fichiers
#### `modes.txt`
Le fichier `modes.txt` contient un mode par ligne, avec son identifiant, sous le format suivant:
```
MODE_ID,MODE
```

Exemple:
```
snes:nes:ntsc:224@60.0988,1920 1 80 184 312 224 1 9 3 24 0 0 0 60 0 39001717 1
```
Ici le mode MODE_ID est `snes:nes:ntsc:224@60.0988` et le MODE est `1920 1 80 184 312 224 1 9 3 24 0 0 0 60 0 39001717 1`


#### `systems.txt`
Le fichier `systems.txt` permet pour chaque système, de configurer un mode d'affichage par région, et de sélectionner la taille de la zone d'affichage:
```
SYSTEM_ID,REGION,MODE_ID,VIEWPORT_WIDTH,VIEWPORT_HEIGHT
```

Exemple:
```
snes,ntsc,snes:nes:ntsc:224@60.0988,0,0
```

Cette ligne permet a Recalbox de savoir que la Super Nintendo (`snes`), lorsqu'elle va charger un jeu NTSC (non europeen) utilisera le mode d'affichage identifié par `snes:nes:ntsc:224@60.0988` (dans `modes.txt`).

Les deux zéros suivants laissent Recalbox définir la meilleur valeur pour la hauteur et la largeur de l'image.

#### `arcade_games.txt`
Le fichier `arcade_games.txt` permet de sélectionner le mode d'affichage pour chaque jeu arcade, suivant le core (ou l’émulateur) sur lequel il sera lancé. Il permet aussi de modifier la taille de la zone d'affichage:
```
GAME_ID,CORE_ID,MODE_ID,VIEWPORT_WIDTH,VIEWPORT_HEIGHT
```

Exemple:
```
dino,fbneo,snes:nes:ntsc:224@60.0988,0,0
```

Les GAME_ID sont les noms des fichiers des jeux, sans le `.zip`.
Le CORE_ID est le nom du core utilisé par Recalbox.

Cette ligne permet a Recalbox de savoir que le jeu `dino.zip`, lancé sur retroarch avec le core `fbneo`, utilisera le mode `arcade:224@59.637405`.


### Exemple de surcharge

La surcharge permet d'ajouter ou de remplacer des modes, systèmes ou jeux d'arcade.

Chacun des modes, systèmes ou jeux des surcharges est ajouté a la configuration de base, ou la remplace si l'id existe déjà.

Imaginons que je veuille changer le mode d'affiche de `Cadillac and Dinosaurs` sur arcade, et le passer en 224 lignes à 60HZ.

Dans le fichier modes.txt, il existe un mode correspondant a 224 lignes a 60HZ. Il a pour MODE_ID `standard:all:240@60`

Je vais donc créer le fichier `/recalbox/share/system/configs/crt/arcade_games.txt` et y mettre une seule ligne:

```
dino,fbneo,standard:all:240@60,0,0

```

Les VIEWPORT_WIDTH et VIEWPORT_HEIGHT permettent de redimensionner l'image affichée. Ils ne changent pas le mode d'affichage de l’écran.
Vous pouvez vouloir ajouter des configurations spécifiques pour des systèmes ou des jeux d'arcade.

Définissons une fois pour toute la largeur d’écran de `Metal Slug 3` à 1800 pixels dans `/recalbox/share/system/configs/crt/arcade_games.txt`
```
mslug3,fbneo,arcade:224@59.185606,1800,0
```

Relancez `Metal Slug 3` et vous pourrez observer que la largeur de l'écran a diminuée.

