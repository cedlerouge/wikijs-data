---
title: Compatibilité des périphériques
description: 
published: true
date: 2021-08-12T07:45:20.909Z
tags: compatibilité, périphériques
editor: markdown
dateCreated: 2021-05-21T07:51:07.191Z
---

Il s'agit d'un projet mené par plusieurs membres de la communauté pour partager tous les périphériques qui fonctionnent « plug and play » et les périphériques qui ne fonctionnent pas.

N'hésitez pas à effectuer tout ajout ou changement à faire, pour créer une base de données de compatibilité pour Recalbox !