---
title: Compatibilité des émulateurs
description: 
published: true
date: 2021-09-30T10:39:27.589Z
tags: compatibilité, émulateurs
editor: markdown
dateCreated: 2021-05-21T12:31:19.906Z
---

#### Version 7.3 Reloaded

Si vous ne trouvez pas le système dans votre installation, cela signifie qu'il n'a pas encore été intégré.
Soyez patient, nous travaillons dessus... 😃

## Légende

* ✅ Disponible
* ❌ Non disponible
* ❄️ Pas assez puissant pour faire tourner le core (pas d'implantation possible)
* 🌗 Basses performances pour certains jeux sur ce système
* 🐌 Basses performances mais jouable
* 🎮 Compatible Netplay
* 🆙 Un overclock est requis
* 1️⃣ mame2000 (imame4all) - romset 0.37b5
* 2️⃣ Uniquement sur la prise HDMI 1
* 3️⃣ Un GPU avec OpenGL &gt;= 3.1 est requis (pour vérifier la compatibilité de votre GPU, veuillez utiliser cette commande SSH: `DISPLAY=:0.0 glxinfo | grep "OpenGL version string:"`)
* 4️⃣ Un GPU avec OpenGL &gt;= 3.3 est requis (pour vérifier la compatibilité de votre GPU, veuillez utiliser cette commande SSH: `DISPLAY=:0.0 glxinfo | grep "OpenGL version string:"`)

## Emulateurs

### Arcade

| Émulateur | RPI0/RPI1 | RPI2 | RPI3 | RPI4 | RPI-400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| **Atomiswave** |  |  |  |  |  |  |  |  |  |
| Libretro Flycast | ❌ ❄️ | ❌ ❄️ | ❌ ❄️ | ✅ | ✅ | ✅ | ✅ | ❌ | ✅ |
| **Daphne** |  |  |  |  |  |  |  |  |  |
| Hypseus | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **FinalBurn Neo** 🎮 |  |  |  |  |  |  |  |  |  |
| Libretro FBNeo | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| PifBA | ✅ | ✅ | ✅ | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ |
| **MAME** |  |  |  |  |  |  |  |  |  |
| AdvanceMame | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ❌ |
| Libretro MAME | ❌ ❄️ | ❌ ❄️ | ❌ ❄️ | ❌ | ❌ | ❌ | ❌ | ✅ | ✅ |
| Libretro MAME2000 | ✅ | ✅ | ✅ | ✅ | ✅ | ❌ | ❌ | ❌ | ❌ |
| Libretro MAME2003 🎮 | ✅ 🆙 1️⃣ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro MAME2003-plus 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro MAME2010 | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro MAME2015 | ❌ | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Naomi** |  |  |  |  |  |  |  |  |  |
| Libretro Flycast | ❌ | ❌ | ✅ 🐌 | ✅ | ✅ | ✅ | ✅ | ❌ | ✅ |
| **Naomi GD-Rom** |  |  |  |  |  |  |  |  |  |
| Libretro Flycast | ❌ | ❌ | ✅ 🐌 | ✅ | ✅ | ✅ | ✅ | ❌ | ✅ |
| **Sega Model 3** |  |  |  |  |  |  |  |  |  |
| Supermodel | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ✅ | ✅ |

### Consoles fantasy

| Émulateur | RPI0/RPI1 | RPI2 | RPI3 | RPI4 | RPI-400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| **EasyRPG** |  |  |  |  |  |  |  |  |  |
| EasyRPG | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **LowRes NX** |  |  |  |  |  |  |  |  |  |
| Libretro LowResNX | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Lutro** |  |  |  |  |  |  |  |  |  |
| Libretro Lutro | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Openbor** |  |  |  |  |  |  |  |  |  |
| Openbor | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Solarus** |  |  |  |  |  |  |  |  |  |
| Solarus | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Tic-80** |  |  |  |  |  |  |  |  |  |
| Libretro Tic80 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Uzebox** |  |  |  |  |  |  |  |  |  |
| Libretro Uzem | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

### Consoles de salon

| Émulateur | RPI0/RPI1 | RPI2 | RPI3 | RPI4 | RPI-400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| **3DO** |  |  |  |  |  |  |  |  |  |
| Libretro Opera | ❌ | ❌ | ✅ 🆙 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Amiga CD32** |  |  |  |  |  |  |  |  |  |
| Amiberry | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ❌ | ❌ |
| Libretro PUAE | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro UAE4ARM | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ❌ | ❌ |
| **Amiga CDTV** |  |  |  |  |  |  |  |  |  |
| Amiberry | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ❌ | ❌ |
| Libretro PUAE | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro UAE4ARM | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ❌ | ❌ |
| **Amstrad GX4000** |  |  |  |  |  |  |  |  |  |
| Libretro Cap32 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Atari 2600** |  |  |  |  |  |  |  |  |  |
| Libretro Stella 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro Stella2014 🎮 | ✅ | ✅ | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ |
| **Atari 5200** |  |  |  |  |  |  |  |  |  |
| Libretro Atari800 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Atari 7800** |  |  |  |  |  |  |  |  |  |
| Libretro Prosystem | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Atari Jaguar** |  |  |  |  |  |  |  |  |  |
| Libretro Virtualjaguar | ❌ | ❌ | ❌ | ✅ | ✅ | ✅ | ❌ | ✅ | ✅ |
| **Colecovision** |  |  |  |  |  |  |  |  |  |
| Libretro blueMSX | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro FBNeo 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro GearColeco 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Dreamcast** |  |  |  |  |  |  |  |  |  |
| Reicast | ❌ | ❌ | ✅ 🐌 🆙 | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ |
| Libretro Flycast | ❌ | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ❌ | ✅ |
| **Fairchild Channel F** |  |  |  |  |  |  |  |  |  |
| Libretro FBNeo 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro FreeChaF | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Famicom Disk System** |  |  |  |  |  |  |  |  |  |
| Libretro FCEUmm 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro FBNeo 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro Nestopia 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro Mesen 🎮 | ❌ | ❌ | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **GameCube** |  |  |  |  |  |  |  |  |  |
| Dolphin 🎮 | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ✅ 3️⃣ |
| **Inttellivision** |  |  |  |  |  |  |  |  |  |
| Libretro FreeIntv | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Master System** |  |  |  |  |  |  |  |  |  |
| Libretro FBNeo 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro Gearsystem | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro GenesisPlusGX 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro GenesisPlusGX WIDE 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro PicoDrive 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Megadrive** |  |  |  |  |  |  |  |  |  |
| Libretro FBNeo 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro GenesisPlusGX 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro PicoDrive 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Multivision** |  |  |  |  |  |  |  |  |  |
| Libretro Gearsystem | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **NEC PC-FX** |  |  |  |  |  |  |  |  |  |
| Libretro Mednafen_PCFX | ❌ | ❌ | ❌ | ✅ | ✅ | ✅ | ❌ | ✅ | ✅ |
| **Neo-Geo** |  |  |  |  |  |  |  |  |  |
| FBA2X | ✅ | ✅ | ✅ | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ |
| Libretro FBNeo 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro MAME2000 | ✅ | ✅ | ✅ | ✅ | ✅ | ❌ | ❌ | ❌ | ❌ |
| Libretro MAME2003 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro MAME2003-Plus 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro MAME2010 | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro MAME2015 | ❌ | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Neo-Geo CD** |  |  |  |  |  |  |  |  |  |
| Libretro FBNeo 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro NeoCD | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **NES** |  |  |  |  |  |  |  |  |  |
| Libretro FBNeo 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro FCEUmm 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro FCEUNext 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro Nestopia 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro QuickNES 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro Mesen 🎮 | ❌ | ❌ | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Nintendo 64** |  |  |  |  |  |  |  |  |  |
| Libretro Mupen64Plus_Next | ❌ | ❌ | ✅ | ✅ | ✅ | ❌ | ✅ | ✅ | ✅ |
| Libretro ParaLLEl_N64 | ❌ | ❌ | ✅ | ✅ | ✅ | ✅ | ❌ | ❌ | ✅ |
| Mupen64Plus GLideN64 | ❌ | ❌ | ✅ | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ |
| Mupen64Plus GLideN64_20 | ❌ | ✅ | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ |
| Mupen64Plus GLide64MK2 | ❌ | ❌ | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Mupen64Plus N64_GLES2 | ❌ | ✅ | ✅ | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ |
| Mupen64Plus RICE | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ❌ | ✅ |
| Mupen64Plus RICE_GLES2 | ❌ | ✅ | ✅ | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ |
| **Nintendo 64DD** |  |  |  |  |  |  |  |  |  |
| Libretro ParaLLEl_N64 | ❌ | ❌ | ❌ | ✅ | ✅ | ✅ | ❌ | ❌ | ✅ |
| Libretro Mupen64Plus_Next | ❌ | ❌ | ✅ | ✅ | ✅ | ❌ | ✅ | ✅ | ✅ |
| **Odyssey 2 / Videopac** |  |  |  |  |  |  |  |  |  |
| Libretro O2EM | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **PC Engine** |  |  |  |  |  |  |  |  |  |
| Libretro FBNeo 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro Mednafen_SuperGrafx 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro Mednafen_PCE_FAST 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **PC Engine CD** |  |  |  |  |  |  |  |  |  |
| Libretro Mednafen_SuperGrafx 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro Mednafen_PCE_FAST 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **PlayStation 1** |  |  |  |  |  |  |  |  |  |
| PCSX_ReARMed | ✅ 🐌 | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ |
| Libretro Swanstation | ❌ | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro PCSX_ReARMed | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ❌ | ✅ | ✅ |
| Libretro Mednafen_PSX | ❌ | ❌ | ❌ | ✅ 🐌 | ✅ | ✅ | ❌ | ✅ | ✅ |
| Libretro Mednafen_PSX_HW | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ✅ | ✅ |
| DuckStation | ❌ | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ 3️⃣ | ✅ 3️⃣ |
| **Satellaview** |  |  |  |  |  |  |  |  |  |
| Libretro Snes9x | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro Mesen_S | ❌ | ❌ | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Sega 32X** |  |  |  |  |  |  |  |  |  |
| Libretro PicoDrive 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Sega CD** |  |  |  |  |  |  |  |  |  |
| Libretro GenesisPlusGX 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro PicoDrive 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Saturn** |  |  |  |  |  |  |  |  |  |
| Libretro YabaSanshiro | ❌ | ❌ | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ 4️⃣ | ✅ 4️⃣ |
| Libretro Kronos | ❌ | ❌ | ❌ | ❌ | ❌ | ✅ | ✅ | ✅ 4️⃣ | ✅ 4️⃣ |
| Librtro Mednafen_Saturn | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ✅ | ✅ |
| Libretro Yabause | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ✅ | ✅ |
| **SG-1000** |  |  |  |  |  |  |  |  |  |
| Libretro FBNeo 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro Gearsystem | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro GenesisPlusGX 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Super Cassette Vision** |  |  |  |  |  |  |  |  |  |
| Libretro EmuSCV 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **SuFami Turbo** |  |  |  |  |  |  |  |  |  |
| Libretro Snes9x | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Super Nintendo** |  |  |  |  |  |  |  |  |  |
| Libretro Mesen_S | ❌ | ❌ | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro Snes9x 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro Snex9x2002 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro Snes9x2005 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro Snes9x2010 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| PiSNES | ✅ | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ |
| **SuperGrafx** |  |  |  |  |  |  |  |  |  |
| Libretro Mednafen_SuperGrafx 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro FBNeo 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Vectrex** |  |  |  |  |  |  |  |  |  |
| Libretro vecx | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Virtual Boy** |  |  |  |  |  |  |  |  |  |
| Libretro Mednafen_VB | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Wii** |  |  |  |  |  |  |  |  |  |
| Dolphin | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ✅ 3️⃣ |

### Consoles portables

| Émulateur | RPI0/RPI1 | RPI2 | RPI3 | RPI4 | RPI-400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| **Game & Watch** |  |  |  |  |  |  |  |  |  |
| Libretro gw | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Game Boy / Game Boy Color** |  |  |  |  |  |  |  |  |  |
| Libretro Gambatte | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro Mesen_S 🎮 | ❌ | ❌ | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro mGBA | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro SameBoy 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro TGBDual 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Game Boy Advance** |  |  |  |  |  |  |  |  |  |
| Libretro gpSP | ✅ 🐌 🆙 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro Meteor | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro mGBA | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Game Gear** |  |  |  |  |  |  |  |  |  |
| Libretro FBNeo 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro Gearsystem | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro GenesisPlusGX | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Lynx** |  |  |  |  |  |  |  |  |  |
| Librtro Handy 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro Mednafen_Lynx 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Neo-Geo Pocket** |  |  |  |  |  |  |  |  |  |
| Libretro FBNeo 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro Mednafen_NGP 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro RACE 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Neo-Go Pocket Color |  |  |  |  |  |  |  |  |  |
| Libretro Mednafen_NGP 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro RACE 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Nintendo DS** |  |  |  |  |  |  |  |  |  |
| Librtro DeSmuME | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ✅ | ✅ |
| Libretro melonDS | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ✅ | ✅ |
| **Palm OS** |  |  |  |  |  |  |  |  |  |
| Libretro Mu | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **PlayStation Portable** |  |  |  |  |  |  |  |  |  |
| PPSSPP | ❌ | ❌ | ✅ 🐌 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Pocket Challenge V2** |  |  |  |  |  |  |  |  |  |
| Libretro Mednafen_WSwan | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Pokémon Mini** |  |  |  |  |  |  |  |  |  |
| Libretro PokéMini | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Wonderswan / Wonderswan Color** |  |  |  |  |  |  |  |  |  |
| Libretro Mednafen_WSwan | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

### Ordinateurs

| Émulateur | RPI0/RPI1 | RPI2 | RPI3 | RPI4 | RPI-400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| **Amiga 600** |  |  |  |  |  |  |  |  |  |
| Amiberry | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ❌ | ❌ |
| Libretro PUAE | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro UAE4ARM | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ❌ | ❌ |
| **Amiga 1200** |  |  |  |  |  |  |  |  |  |
| Amiberry | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ❌ | ❌ |
| Libretro PUAE | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro UAE4ARM | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ❌ | ❌ |
| **Amstrad CPC** |  |  |  |  |  |  |  |  |  |
| Libretro CrocoDS | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro Cap32 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Apple II** |  |  |  |  |  |  |  |  |  |
| LinApple | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ❌ | ✅ | ✅ |
| GSPlus | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Apple IIGS** |  |  |  |  |  |  |  |  |  |
| GSPlus | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Atari 800** |  |  |  |  |  |  |  |  |  |
| Libretro Atari800 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Atari ST** |  |  |  |  |  |  |  |  |  |
| Hatari | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro Hatari | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Commodore 64** |  |  |  |  |  |  |  |  |  |
| Libretro vice_x64 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro vice_x64sc | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro vice_x128 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro vice_xcbm2 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro vice_xpet | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro vice_xplus4 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro vice_xscpu64 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro vice_xvic | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **DOS** |  |  |  |  |  |  |  |  |  |
| DOSBox | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro DOSBox_Pure | ❌ | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Dragon32/64** |  |  |  |  |  |  |  |  |  |
| XRoar | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Elektronika BK** |  |  |  |  |  |  |  |  |  |
| Libretro BK | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **MSX 1** |  |  |  |  |  |  |  |  |  |
| Libretro FBNeo 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro blueMSX | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro FMSX | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **MSX 2** |  |  |  |  |  |  |  |  |  |
| Libretro blueMSX | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Libretro FMSX | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **MSX Turbo R** |  |  |  |  |  |  |  |  |  |
| Libretro blueMSX | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Oric Atmos** |  |  |  |  |  |  |  |  |  |
| Oricutron | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **PC-88** |  |  |  |  |  |  |  |  |  |
| Libretro QUASI88 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **PC-98** |  |  |  |  |  |  |  |  |  |
| Libretro NP2Kai | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **SAM Coupé** |  |  |  |  |  |  |  |  |  |
| Libretro Simcoupe | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **ScummVM** |  |  |  |  |  |  |  |  |  |
| ScummVM | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **SHARP x1** |  |  |  |  |  |  |  |  |  |
| Libretro Xmil | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **SHARP x68000** |  |  |  |  |  |  |  |  |  |
| Libretro PX68k | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Spectravideo** |  |  |  |  |  |  |  |  |  |
| Libretro blueMSX | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Thomson TO + MO** |  |  |  |  |  |  |  |  |  |
| Libretro Theodore | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **TI-99/4A** |  |  |  |  |  |  |  |  |  |
| TI-99/Sim | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **TRS-80 Color Computer** |  |  |  |  |  |  |  |  |  |
| XRoar | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **VIC-20** |  |  |  |  |  |  |  |  |  |
| Libretro vice_xvic | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Videopac+** |  |  |  |  |  |  |  |  |  |
| Libretro O2EM | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **ZX81** |  |  |  |  |  |  |  |  |  |
| Libretro 81 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **ZX Spectrum** |  |  |  |  |  |  |  |  |  |
| Libretro FBNeo 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| Librtro Fuse | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

### Ports

| Émulateur | RPI0/RPI1 | RPI2 | RPI3 | RPI4 | RPI-400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| **2048** |  |  |  |  |  |  |  |  |  |
| Libretro 2048 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Cave Stoy** |  |  |  |  |  |  |  |  |  |
| Libretro NXEngine | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **DinoThawr** |  |  |  |  |  |  |  |  |  |
| Libretro Dinothawr | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **DOOM** |  |  |  |  |  |  |  |  |  |
| Libretro PrBoom | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Flashback** |  |  |  |  |  |  |  |  |  |
| Libretro REminiscence | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Minecraft** |  |  |  |  |  |  |  |  |  |
| Librtro Craft | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ✅ | ✅ |
| **MrBoom** |  |  |  |  |  |  |  |  |  |
| Libretro MrBoom 🎮 | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Out Run** |  |  |  |  |  |  |  |  |  |
| Libretro Cannonball | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Quake 1** |  |  |  |  |  |  |  |  |  |
| Libretro TyrQuake | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Rick Dangerous** |  |  |  |  |  |  |  |  |  |
| Librtro XRick | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Sigil** |  |  |  |  |  |  |  |  |  |
| Libretro PrBoom | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |
| **Wolfenstein 3D** |  |  |  |  |  |  |  |  |  |
| Libretro ECWolf | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## Matériel

| Périphérique | RPI0/RPI1 | RPI2 | RPI3 | RPI4 | RPI-400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| Gamepad : Wiimote and Wiimote Classical Controller Extension | ✅ | ✅ | ✅ | ❓ | ❓ | ❓ | ❓ | ✅ | ✅ |
| Gamepad : xinmo | ✅ | ✅ | ✅ | ❓ | ❓ | ✅ | ❓ | ✅ | ✅ |
| Boot animation | ✅ | ✅ | ✅ | ✅ | ❓ | ✅ | ❓ | ✅ | ✅ |
| Noyau Linux (support matériel) | 5.10 | 5.10 | 5.10 | 5.10 | 5.10 | 5.4 | 4.4 | 5.8 | 5.8 |
| Bluetooth interne | PI0 uniquement | N/A | ✅ | ✅ | ✅ | N/A | ✅ | ✅ | ✅ |
| Infrarouge interne | N/A | N/A | N/A | N/A | N/A | N/A | N/A | N/A | N/A |
| Problèmes de températures élevées | ✅ | ✅ | ❌ | ❌ | ❌ | ✅ | ❓ | ✅ | ✅ |
| HDMI-CEC (télécommande Kodi) | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ 2️⃣ | ❓ | N/A | N/A |

## Autres

| Émulateur | RPI0/RPI1 | RPI2 | RPI3 | RPI4 | RPI-400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| Kodi | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ❌ | ✅ | ✅ |
| Moonlight | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ❌ |