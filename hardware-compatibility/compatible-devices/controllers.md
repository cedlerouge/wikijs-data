---
title: Manettes
description: 
published: true
date: 2021-09-29T15:04:57.350Z
tags: compatibilité, manettes
editor: markdown
dateCreated: 2021-05-21T14:27:21.756Z
---

## Manettes officielles de consoles

| Périphérique | Filaire | Sans fil | Dernière version testée | Commentaires |
| :---: | :---: | :---: | :---: | :---: |
| **MICROSOFT** |  |  |  |  |
| Manette Xbox de la Xbox originale | ✅ |  |  | ✅ Avec modification USB |
| Manette Xbox 360 | ✅ | ☑️ | 7.1.1 | ☑️ Avec le dongle Microsoft Xbox 360 |
| Manette Xbox One | ✅ | ☑️ | 7.3.0 | ☑️ Avec un dongle : [adaptateur sans fil Xbox](https://www.xbox.com/fr-FR/accessories/adapters/wireless-adapter-windows), 8BitDo (attendre 5s), Janswall,... |
| Manette Xbox One Elite | ✅ |  | 7.1.1 |  |
| Manette Xbox One S | ✅ |  | 7.0.1 |  |
| Manette Xbox One Series X\|S | ✅ |  | 7.0.1 |  |
| **NINTENDO** |  |  |  |  |
| Manette GameCube | ✅ | ☑️ |  | ✅ Adaptateur Mayflash Mayflash GameCube pour PC USB ☑️ Avec un dongle: 8BitDo GBros Adaptator |
| Manette GameCube WaveBird | ❌ | ☑️ |  | ☑️ Avec un dongle: 8BitDo GBros Adaptator (avec un câble d'extension GameCube) |
| Manette NES Mini | ✅ | ☑️ |  | ✅ Avec le convertisseur Mayflash USB ☑️ Avec un dongle : 8BitDo GBros Adaptator |
| SNES Mini Controller | ✅ | ☑️ |  | ✅ Avec le convertisseur Mayflash USB ☑️ Avec un dongle : 8BitDo GBros Adaptator |
| JoyCon |  | ☑️ | 7.2.1 |  |
| Manette Switch Pro Controller |  | ☑️ | 7.2.1 |  |
| Manette Wii Classic Controller | ✅ | ☑️ | v4.1 | ✅ Avec le convertisseur Mayflash USB ☑️ Nativement & avec le dongle 8BitDo GBros Adaptator |
| Manette Wii Classic Pro | ✅ | ☑️ |  | ✅ Avec le convertisseur Mayflash USB ☑️ Avec un dongle : 8BitDo GBros Adaptator |
| Wiimote | ✅ | ☑️ | 7.1.1 | ✅ Avec la barre Mayflash Dolphin Bar |
| Wii U Pro Controller | ❌ | ☑️ | 4.1 | Nativement & avec un dongle : 8BitDo, Janswall,...|
| Manette Super Nintendo Entertainment System for Nintendo Switch |  | ☑️ | 7.2.1 |  |
| **SNK** |  |  |  |  |
| Manette NeoGeo Mini | ❌ |  |  | Quelques problèmes avec un adaptateur USB-C |
| **SONY** |  |  |  |  |
| Manette PlayStation 1 | ✅ |  | v 3.3beta 17 & v4.1.0-dev | Avec adaptateur générique USB pour 2 PS1/PS2 PAD controllers |
| Manette PlayStation 1 Dualshock | ✅ |  | v 3.3beta 17 & v4.1.0-dev | Avec adaptateur générique USB pour 2 PS1/PS2 PAD controllers |
| Manette PlayStation 2 Dualshock 2 | ✅ |  | v 3.3beta 17 & v4.1.0-dev | Avec adaptateur générique USB pour 2 PS1/PS2 PAD controllers |
| PlayStation 3 DualShock 3 | ✅ | ☑️ | 7.0 | ☑️ Avec un dongle: 8BitDo, Janswall,... ou n'importe quel dongle Bluetooth |
| PlayStation 4 DualShock 4 | ✅ | ☑️ | 7.0.1 | ☑️ Avec le dongle officiel Sony PS4 & d'autres dongles : 8BitDo, Janswall,... |
| PlayStation 5 DualSense | ✅ |  | 7.0.1 | ✅ Avec [ceci](https://forum.recalbox.com/topic/8920/a-lire-manettes-ps4). |
| Playstation Classic Mini Controller | ✅ |  |  |  |

## Manettes

| Périphérique | État | Dernière version testée | Commentaires |
| :---: | :---: | :---: | :---: | :---: |
| Collections de manettes 8bitdo | ✅ | 7.0.1 | Voir la page [8bitdo sur Recalbox](./../../hardware-compatibility/compatible-devices/8bitdo-on-recalbox) |
|  |  |  |  |
| **Bigben Interactive** |  |  |  |
| Pad Luxe PS3 | ✅ |  |  |
| PlayStation 3 BB4259 | ✅ |  |  |
| PlayStation 3 BB4401 | ✅ |  |  |
| Big Ben BB5033 (avec dongle USB 2.4GHz RF) | ✅ | v17.11.10.2 |  |
| **Bowick** |  |  |  |
| Wireless Bluetooth Controllers for PS3 | ✅ | v4.0.2 | Sélectionnez `shawan` dans la configuration |
| **BUFFALO** |  |  |  |
| iBuffalo SNES Classic USB Gamepad | ✅ |  | Les 2 boutons spéciaux `Turbo` et `Clear` fonctionnent |
| **CSL-Computer** |  |  |  |
| Gamepad USB SNESw | ✅ |  |  |
| USB Wireless Gamepad (Model 2016) | ✅ |  |  |
| **EasySMX** |  |  |  |
| EMS-9100 Gamepad USB Wired | ✅ |  | A un bouton Home pour la configuration. Supporte Android 4.0 ou supérieur. |
| **Free** |  |  |  |
| Freebox Controller | ✅ |  | Mode analogique |
| **Gamesir** |  |  |  |
| Gamesir G3s | ✅ | v4.0.2 | La manette peut être configuré dans le menu mais mal fonctionner en jeu. |
| **HORI** |  |  |  |
| Fighting Commander | ✅ | v4.0.2 |  |
| Pokken Tournament for Wii U | ✅ | v4.0.2 |  |
| **Kreema** |  |  |  |
| Gen Game S3 |  | v18.04.20 | Ne fonctionne pas correctement avec une connexion Bluetooth (Wireless Bluetooth 3.0) |
| Gen Game S5 | ❌ | 17.13.02 | Peut être appairée mais agit bizarrement pendant la configuration. Reconnu comme un clavier si les 2 sticks analogiques sont pressés. Dans la configuration, chaque bouton appuyée est doublé. Ne fonctionne pas en jeu, uniquement dans le menu principal. |
| **iNNEXT** |  |  |  |
| IN-Y-D002-024\*2 | ✅ | v4.0.2 |  |
| **Konix** |  |  |  |
| Konix PS3/PC | ✅ |  |  |
| KONIX KX-CT-PC | ✅ | v4.0.2 |  |
| **KROM** |  |  |  |
| Kay Pro Gaming Wired Gamepad (NXROMKEY) | ✅ |  | Détectée comme une manette Xbox 360, mais impossible de la configurer. |
| **Logitech** |  |  |  |
| Cordless Rumbleepad 2 (avec dongle USB 2.4GHz RF) | ✅ |  |  |
| Dual Action Gamepad (USB) | ✅ | v4.0.0-beta4 |  |
| Precision Gamepad (USB) | ✅ | v4.0.0-beta4 |  |
| Rumblepad 2 (USB) | ✅ |  |  |
| **Logitech G** |  |  |  |
| F310 Gamepad (USB) | ✅ |  |  |
| F710 Wirelesss Gamepad (avec un dongle USB 2.4GHz RF) | ✅ | v4.0.0-beta4 | Choisissez le mode d'entrée D, pas X. |
| **MadCatz** |  |  |  |
| MadCatz Wireless FightPad for PlayStation 3 (avec un dongle USB 2.4GHz RF) | ✅ |  |  |
| **Nacon** |  |  |  |
| GC-400ES Wired Controller | ✅ | 6.1.1 | [Vendeur](https://www.nacongaming.com/controllers/) (testé en verion 1, 2 et 3 ; reconnu comme manette Xbox générique) |
| Revolution Wired | ✅ | v6.1.1 | [Vendeur](https://nacongaming.com/controllers/) (testé en version 1, 2 et 3 ; reconnu comme manette Xbox générique) |
| Nacon esport controllers/revolution wired et GC-400ES | ✅ |  | [Vendeur](https://www.nacongaming.com/controllers/) (testé en version 1, 2 et 3 ; reconnu comme manette Xbox générique) |
| **NGS** |  |  |  |
| Maverick (PS3/PC) | ✅ |  |  |
| Nvidia |  |  |  |
| Shield 2017 (USB) | ❌ |  | Fonctionne dans EmulationStation mais les jeux peuvent prendre du temps à se jouer. |
| **PDP** |  |  |  |
| PlayStation 3 Rock Candy (avec le dongle Bluetooth) | ✅ |  |  |
| Xbox One & Windows 10 | ❌ | v18.04.20 | Détecté mais ne peut pas être configuré. |
| **PowerA** |  |  |  |
| MOGA PRO Power | ✅ | v17.11.02 |  |
| **Razer** |  |  |  |
| Razer Sabertooth | ✅ | v4.0.0-beta2 |  |
| **RetroUSB** |  |  |  |
| USB Super RetroPad (SNES) | ✅ |  |  |
| **Saitek** |  |  |  |
| P380 | ✅ |  |  |
| P480 | ✅ |  |  |
| P880 | ✅ |  |  |
| **SlickBlue** |  |  |  |
| SlickBlue Wireless Bluetooth Game Pad Controller pour Sony PlayStation 3 | ❌ |  |  |
| **Snakebyte** |  |  |  |
| idroid:con (avec le dongle Bluetooth) | ✅ | v3?3beta17 |  |
| blu:con (avec le dongle Bluetooth) | ❌ | v4.0-rpi2-build15 |  |
| **Speedlink** |  |  |  |
| Speedlink Strike FX - PS3 (câble USB) | ❌ | v4.0.0-rpi2B |  |
| Speedlink Torid Wireless Gamepad | ✅ | v4.0.0-rpi3 |  |
| **SteelSeries** |  |  |  |
| SleetSeries Free Mobile Wireless | ✅ | v4.0.0-rpi3 | Dongle Bluetootth obligatoire |
| SteelSeries Stratus Wireless Gaming Controller | ❌ | v4.0.0-rpi3B | Peut être appairée via Bluetooth mais ne sera plus détectée. |
| **Subsonic** |  |  |  |
| Subsonic Bluetooth Controller PS3 | ✅ |  | (pilote gasia) |
| **ThrustMaster** |  |  |  |
| T-Wireless | ✅ | v17.11.10.2 |  |
| **VZTEC** |  |  |  |
| VZTEC USB Double Shock Controller Game Pad Joystick(VZ-GA6002) | ✅ | v4.0.0-beta3 |  | **Xiaomi** |  |  |  |
| Xiaomi Controller | ✅ |  | Besoin d'appairer après chaque déconnexion |
| **Marque non trouvé** |  |  |  |
| PlayStation 3 Afterglow Wireless Controller (avec dongle USB 2.4GHz RF) | ✅ |  |  |
| PlayStation 3 GASIA Clone | ✅ |  |  |
| PlayStation 3 ShawnWan Clone | ✅ |  |  |
| Sony PlayStation Brandery compatible DualShowk Pad Controller | ✅ | v3.3beta 17 & v4.1.0-dev | Avec adaptateur générique USB pour 2 PS1/PS2 pad controllers |

## Adaptateur de manettes

| Périphérique | État | Dernière version testée | Commentaires |
| :---: | :---: | :---: | :---: | :---: |
| 4nes4snes | ✅ | 3.3.0 |  |
| **Cronusmax** |  |  |  |
| CronusMAX Adapter (Xbox 360 Out) | ✅ | v3.3beta17 |  |
| **Mayflash** |  |  |  |
| Mayflash Dolphin Bar | ✅ | 7.2.1 |  |
| Mayflash GameCube Controller Adapter |  | 6.1 | Ne fonctionne qu'avec Dolphin. |
| Mayflash N64 Controller Adapter | ✅ | 4.0.0-dev |  |
| Mayflash NES/SNES Controller Adapter | ✅ | 4.0.0 | Need Usbhid |
| Mayflash PC052 Wii Classic Controller | ✅ | 4.1.0 | Pour les manettes Wii classiques, NES Mini et SNES Mini |
| Mayflash Sega Saturn Controller Adapter | ✅ | 4.0.0 | Need Usbhid |
| **OriGlam** |  |  |  |
| Wii Sensor Bar USB | ❌ | 2018.x.x |  |
| **Autres** |  |  |  |
| PSP 1000 - 3000 avec FuSa Gamepad et câble MiniUSB | ✅ | 4.0.1 | Le Pi peut ne pas charger votre PSP |
| Retrode |  | 3.2.11 | Need Usbhid |
| SFC/USB | ✅ | 4.0.0 |  |
| Manette Sony PlayStation 3 | ✅ | 4.0.0 |  |
| Trenro Dual PlayStation / PlayStation 2 to PC USB Controller Adapter | ✅ | v3.3beta17 & v4.1.0-dev | Avec adaptateur générique USB pour 2 manettes PS1 / PS2 |

## Stick Arcade

| Périphérique | État | Dernière version testée | Commentaires |
| :---: | :---: | :---: | :---: | :---: |
| **8bitdo** |  |  |  |
| FC30 Arcade Joystick (uniquement USB) | ✅ |  |  |
| NES30 Arcade Joystick (uniquement USB) | ✅ |  |  |
| **Datel** |  |  |  |
| Arcade Pro Joystick inc Rapid Fire for PS3 / Xbox 360 / PC | ✅ |  |  |
| **HORI** |  |  |  |
| Fighting Stick EX2 USB (Xbox 360 / PC) | ✅ |  |  |
| Fighting Stick MINI 3 pour PS3 | ✅ |  |  |
| Fighting Stick Mini 4 pour PS3 / PS4 | ✅ |  |  |
| Real Arcade Pro. 3 Fighting Stick | ✅ |  |  |
| Real Arcade Pro. 4 Kai (PS3 / PS4 / PC) | ✅ |  |  |
| HORI Real Arcade Pro. V (PS4) |  |  |  |
| Tekken 6 Wireless Arcade Stick PS3 | ✅ |  |  |
| **LIONCAST** |  |  |  |
| LIONCAST Arcade Fighting Stick (PS2 / PS3 / PC) | ✅ |  |  |
| **MadCatz** |  |  |  |
| Arcade Fightstick Street Fighter V Tes+ (PS3 / PS4) | ✅ | 4.0.0-beta1 |  |
| Arcade Stick Fightstick Alpha (PS3 / PS4) | ✅ | 4.0.0-beta3 |  |
| Arcade Fightsitck Street Fighter IV Standard Edition for Xbox 360 | ✅ |  |  |
| Street Fighter IV Fightstick Round 1 Tournament Edition | ✅ |  |  |
| Street Fighter IV Round 2 Arcade Fightstick Tournament Editin | ✅ |  |  |
| Arcade Fightstick Tournament Edition 2 (For PS3 / PS4) | ✅ | 4.0.0-beta6 |  |
| **Mayflash** |  |  |  |
| Mayflash XBOX 360 / PS3 / PC Arcade Fighting Stick V2 | ✅ |  |  |
| Mayflash PS2 PS3 PC USB Universal Arcade Fighting Stick | ✅ |  |  |
| **PXN** |  |  |  |
| PXN - 0082 Arcade Joystick | ✅ |  |  |
| **QanBa** |  |  |  |
| QamBa Q4 RAF Joystick Pro Fightstick Arcade 3in1 (Xbox 360 / PS3 / PC) | ✅ |  |  |
| **Sanwa** |  |  |  |
| JLF-TP-8YT (Sanwa) Arcade Joystick | ✅ |  |  |
| **Time Warner** |  |  |  |
| Mortal Kombat - Edition Ultimate (PS3) | ❌ |  |  |
| **Venom** |  |  |  |
| Venom - Arcade Stick (PS3 / PS4) | ✅ |  |  |

## Joystick

| Périphérique | État | Dernière version testée | Commentaires |
| :---: | :---: | :---: | :---: |
| **Madcatz :** |  |  |  |
| Cyborg F.L.Y 5 | ✅ | v18.04.20 |  |

## LightGun

| Périphérique | État | Dernière version testée | Commentaires |
| :---: | :---: | :---: | :---: | :---: |
| **Nintendo** |  |  |  |
| Wiimote | ✅ | 7.2.1 | Avec le convertisseur USB Mayflash (peut être utilisé comme Lightgun) |
| **Ultimarc** |  |  |  |
| AimTrack Lightgun | ✅ | 2018.x.x |  |

## Interface USB

| Périphérique | État | Dernière version testée | Commentaires |
| :---: | :---: | :---: | :---: |
| **Akishop :** |  |  |  |
| PS360+ | ✅ |  |  |
|  |  |  |  |
| Zero Delay USB Encoder PC/PS3 controller |  | v 3.3beta 17 & v4.0.0-dev |  |
| Zero Delay USB Enconder Remix PC/PS3 controller | ✅ | v 3.3beta 17 & v4.0.0-dev |  |

## Arcade Push Buttons

| Périphérique | État | Dernière version testée | Commentaires |
| :---: | :---: | :---: | :---: |
| **Halo :** |  |  |  |
| Halo Button | ✅ |  |  |
|  |  |  |  |
| **Pushbutton :** |  |  |  |
| Pushbutton SL-CV | ✅ |  |  |
