---
title: Stockage
description: 
published: true
date: 2021-08-12T07:49:27.212Z
tags: stockage, compatibilité
editor: markdown
dateCreated: 2021-05-21T15:55:45.341Z
---

### CARTES SD

La majorité des cartes SD fonctionneront dans votre Raspberry Pi. Si votre carte SD fait plus de 32GB, veuillez consulter [ce lien](https://www.raspberrypi.org/documentation/installation/sdxc_formatting.md) (anglais).

Mais attention aux contrefaçons (particulièrement sur Aliexpress, Ebay, Wish et LeBonCoin [et quelquefois aussi sur Amazon](https://www.cpchardware.com/when-amazon-opens-opens-a-swap-conf-sandisk/)) !

| Périphérique | État | Dernière version testée | Taille actuelle |
| :---: | :---: | :---: | :---: |
| **Adata** |  |  |  |
| Adata Premier 32GB microSDHC Card (UHS-I Class 10) | ❌ |  |  |
| **Energizer** |  |  |  |
| Energizer 8GB Micro Sd Card Class 10 UHS-I | ✅ |  |  |
| **Kingston** |  |  |  |
| Kingston SDC10/32GBSP Micro SD SDHC/SDXC Class 10 UHS-I 32Go | ❌ |  |  |
| Kingston MicroSD (Class 4) - 8GB | ✅ | Recalbox 4/RPi 2B | 7,21GB / 4GB libre pour les jeux |
| Kingston SDC10/16GB Micro SD HC | ❌ |  |  |
| Kingston SDC10G2/16GB | ✅ | Recalbox 4.0.2/RPi 3B | 16GB / 11GB libre pour les jeux |
| Kingston SDCS/64GBSP Micro SD SDXC C10 UHS-I 64Go | ✅ | Rasp. Pi3 B - Recalbox 18.07.13 | 58 GB libre pour les jeux |
| **LD** |  |  |  |
| LD MicroSD1 XC I / 64GB | ✅ | Recalbox 18.06.27/RPi 3B | 64GB |
| **Lexar** |  |  |  |
| Lexar 633x UHC-i 16 GB | ❌ |  |  |
| **Netac** |  |  |  |
| Netac 32 GB | ❌ |  |  |
| **Philips** |  |  |  |
| Philips Class10 microSDHC Card 32GB | ✅ |  |  |
| **PNY** |  |  |  |
| PNY Elite microSDXC CARD 64GB Class-10 UHS-I | ✅ | v18.04.20 | 56Gb libre pour les jeux |
| PNY Elite microSDXC CARD 128GB | ✅ | v4.0 final | 106gb libre pour les jeux |
| **QUMOX** |  |  |  |
| QUMOX 64GB MICRO SD MEMORY CARD CLASS 10 UHS-I | ✅ |  |  |
| **Samsung** |  |  |  |
| Samsung plus (MB-MPAGC) (UHS-I Grade 0 Class 10) | ✅ |  |  |
| Samsung Evo (MB-MP16D - MB-MP128D) (UHS-I Grade 1 Class 10) | ✅ |  |  |
| Samsung Evo (MB-MP16DA - MB-MP128DA) (UHS-I Grade 1 Class 10) | ✅ |  |  |
| Samsung Evo (MB-MP32D - MB-MP128D) (UHS-I Grade 1 Class 10) | ✅ |  |  |
| Samsung Evo (MB-MC32GA - MB-MC256GA) (UHS-I Grade 1 Class 10) | ✅ |  |  |
| Samsung Evo Select (MB-ME32GA - MB-ME256GA) (UHS-I Grade 1 Class 10) | ✅ | Recalbox 17.12.02/RPi 3B |  |
| **SanDisk** |  |  |  |
| SanDisk Extreme Plus microSDHC | ✅ |  |  |
| SanDisk SDSQUNC MicroSDXC 128GB-GZFMA Class 10 | ✅ |  |  |
| SanDisk Ultra MicroSDXC 200GB (UHS-I Grade 1 Class 10) | ✅ | Recalbox 4.0.2/RPi 3B | 178GB libre pour les jeux |
| SanDisk Ultra MicroSDHC 32GB UHS-I | ✅ | Recalbox 4.1.0/RPi 3B | 27.1GB libre pour les jeux |
| SanDisk Ultra Plus 64GB UHS-1 Class 10 | ✅ |  |  |
| SanDisk Ultra Series class 10 | ✅ |  |  |
| SanDisk Ultra (GN6MA) (UHS-I Grade 1 Class 10) | ✅ |  |  |
| SanDisk Extreme Pro U3 64GB | ✅ |  |  |
| **Strontium** |  |  |  |
| Strontium 16GB MicroSDHC UHS-1 NITRO 433X Class 10 | ✅ |  |  |
| Strontium 32GB MicroSDHC UHS-1 NITRO 466X Class 10 | ✅ |  |  |
| **Toshiba** |  |  |  |
| Toshiba microSDHC UHS-1 32GB Class 10 | ✅ | Recalbox 4.1.0/RPi 3B |  |
| **Transcend** |  |  |  |
| Transcend 8GB & 32GB class 10 | ✅ |  |  |
| Transcend 32GB (TS32GUSDU1PE) UHS-1 Class 10 | ✅ |  |  |
| Transcend Premium 300x class 10 | ❌ |  |  |
| **Verbatim** |  |  |  |
| Verbatim Micro SDHC Card 32GB Class 10 | ❌ |  |  |
