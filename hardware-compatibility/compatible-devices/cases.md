---
title: Boitiers
description: 
published: true
date: 2021-08-12T07:46:51.632Z
tags: compatibilité, boitiers
editor: markdown
dateCreated: 2021-05-21T13:54:14.431Z
---

## Pi ZERO/Pi ZERO W

### GPi CASE

![Pour Zero et Zero W](/compatibility/cases/gpicase.png)

[Lien pour achat](https://www.kubii.fr/consoles-retro-gaming/2719-gpi-case-retroflag-kubii-3272496299276.html)

## PI2/PI3/PI3B/PI3B+

### NESPi CASE

![](/compatibility/cases/nespicase.png)

[Lien pour achat](https://www.reichelt.com/fr/en/case-for-raspberry-pi-3-nes-design-gray-rpi-nespi-case-p211465.html?&trstct=pos_0&nbc=1)

### NESPi CASE PLUS

![Pour 2, 3, 3B et 3B +](/compatibility/cases/nespicaseplus.png)

[Lien pour achat](https://www.kubii.fr/boitiers-et-supports/2036-boitier-nespi-case-pour-raspberry-pi-3-2-b-kubii-327249600861.html)

### SUPERPI CASE J

![Pour 2B, 3B et 3B+](/compatibility/cases/superpicase.png)

[Lien pour achat](https://www.kubii.fr/boitiers-et-supports/2283-superpicase-j-kubii-3272496011922.html)

### SUPERPI CASE U

![Pour 2, 3 et 3B+](/compatibility/cases/superpicaseusa.png)

[Lien pour achat](https://www.amazon.com/dp/B07G34TTKL?m=A3I7DCARNWUK2P&ref_=v_sp_detail_page&th=1)

### SUPERPI CASE U (New)

![Pour 2, 3 et 3B+](/compatibility/cases/superpicaseusanew.png)

[Lien pour achat](https://www.amazon.com/dp/B07W5L95KK?ref=myi_title_dp)

### MEGAPi CASE M

![Pour 2B, 3B et 3B+](/compatibility/cases/megapicase.png)

[Lien pour achat](https://www.kubii.fr/consoles-retro-gaming/2337-boitier-megapi-case-kubii-3272496012646.html)

## PI4

### NESPi 4 CASE

![Pour 4](/compatibility/cases/nespi4case.png)

[Lien pour achat](https://www.kubii.fr/boitiers/3031-boitier-nespi-case-pour-raspberry-pi-4-3272496302303.html)

### Argon One

![Pour 4](/compatibility/cases/argonone.png)

[Lien pour achat](https://www.kubii.fr/boitiers-et-supports/2762-boitier-argon1-pour-raspberry-pi-4-3272496299672.html)

### Thingivers 

Aucun aperçu, différents boitiers à imprimer avec une imprimante 3D.

[Lien pour achat](https://www.thingiverse.com/tag:raspberry_pi_4)

## PI4 Functional cheap case

### GeeekPi

![Pour 4](/compatibility/cases/geekpi.png)

[Lien pour achat](https://www.amazon.fr/dp/B07XXQ34PZ/ref=cm_sw_r_cp_apa_i_MdpqFb2V1Q6F6)

### GeeekPi

![Pour 4](/compatibility/cases/geekpi2.png)

[Lien pour achat](https://www.amazon.fr/GeeekPi-Raspberry-Ventilateur-40X40X10mm-Dissipateurs/dp/B07XCKNM8J/ref=sr_1_3?__mk_fr_FR=%C3%85M%C3%85%C5%BD%C3%95%C3%91&dchild=1&keywords=boitier+pi+4&qid=1598103872&sr=8-3)

### Bruphny

![Pour 4](/compatibility/cases/bruphny.png)

[Lien pour achat](https://www.amazon.fr/Bruphny-Ventilateur-Alimentation-Dissipateurs-Adaptateur/dp/B07WN3CHGH/ref=sr_1_5?__mk_fr_FR=%C3%85M%C3%85%C5%BD%C3%95%C3%91&dchild=1&keywords=boitier+pi+4&qid=1598103952&sr=8-5)

## Odroid XU4

### Boîtier Gaming

![Pour XU4](/compatibility/cases/ogst.png)

[Lien pour achat](https://www.kubii.fr/odroid/2214-boitier-gaming-pour-odroid-xu4-kubii-3272496011250.html)