---
title: Raspberry PI hats
description: Listes des hats supportés
published: true
date: 2021-08-11T20:27:17.441Z
tags: 7.3+, rpi, hats, compatible
editor: markdown
dateCreated: 2021-08-11T19:26:10.261Z
---

# HATs Raspberry PI compatibles avec Recalbox

## Contrôleurs PoE

Les contrôleurs PoE permettent d'alimenter le Raspberry PI par le connecteur ethernet. Nul besoin d'une alimentation mini-USB ou USB-C!

Pour alimenter le RPI, il faut posséder un switch ethernet compatible IEEE 802.3af ou IEEE 802.3at.

### Waveshare PoE hat (b)

Ce hat permet d'alimenter le rpi par l'ethernet et possède un écran oled monochrome ainsi qu'un ventilateur à courant continu programmable (on/off). Il est possible de forcer l'activation du ventilateur par un switch sur la carte.

Ce hat est compatible RPI 3b+ et RPI 4.

L'utilitaire [recalbox-wpaf](/fr/advanced-usage/fan-controller) supporte ce hat et permet de contrôler le ventilateur (activation/désactivation en fonction de la température du CPU).

### Raspberry PI PoE+

Ce hat permet d'alimenter le RPI par l'ethernet est possède un ventilateur à vitesse variable (PWM).

Ce hat est compatible RPI 3b+ et RPI4.

L'utilitaire [recalbox-wpaf](/fr/advanced-usage/fan-controller) supporte ce hat et permet de contrôler le ventilateur (activation/désactivation en fonction de la température du CPU).

## Cartes avec ventilateur

Ce type de carte dispose d'un ventilateur qui permet de refroidir le CPU.

### Pimoroni fan SHIM

Cette carte se fixe sur une partie du connecteur GPIO du Raspberry PI. Elle dispose d'un ventilateur silencieux, d'une led RGB et d'un bouton. La fonction ventilateur est supportée par l'utilitaire [recalbox-wpaf](/fr/advanced-usage/fan-controller).

