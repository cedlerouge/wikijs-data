---
title: Écrans
description: 
published: true
date: 2021-08-12T07:50:04.119Z
tags: compatibilité, écrans
editor: markdown
dateCreated: 2021-05-21T15:48:08.996Z
---

### Écran TFT HDMI

| Périphérique | Taille | Résolution | État | Commentaires |
| :---: | :---: | :---: | :---: | :---: |
|  | 5" | 800x480 | ✅ | [banggood](https://www.banggood.com/5-Inch-800-x-480-HDMI-TFT-LCD-Touch-Screen-For-Raspberry-PI-2-Model-B-B-A-B-p-1023438.html) |
| PI-TOP CEED | 14" | 1366×768 | ✅* | Fonctionne mais pas de son. Besoin du paquetage "pt-speaker" [Source](https://www.neozone.org/tests/test-pi-top-ceed-lordi-ideal-pour-les-makers-en-herbe/) |

### Écran TFT SPI Bus

| Périphérique | Taille | Résolution | État | Commentaires |
| :---: | :---: | :---: | :---: | :---: |
|  |  |  |  |  |

### Écran TFT DPI Bus

| Périphérique | Taille | Résolution | État | Commentaires |
| :---: | :---: | :---: | :---: | :---: |
|  |  |  |  |  |