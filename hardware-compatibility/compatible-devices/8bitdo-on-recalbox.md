---
title: 8Bitdo sur Recalbox
description: 
published: true
date: 2021-09-23T23:26:36.172Z
tags: 8bitdo, manette, compatibilité
editor: markdown
dateCreated: 2021-05-21T08:05:33.370Z
---

![8bitdo](http://support.8bitdo.com/images/Logo-black.svg)

![Liste des manettes 8Bitdo au 13 novembre 2019](/compatibility/8bitdocollection.png){.full-width}

## Manettes supportées 8Bitdo

### Manettes de style SNES :

| Manette | FW (min / rec*) | USB | BT | Mode de démarrage | Association BT* | Réinitialisation BT | Réinitialisation d'usine |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| SN30 Pro+ | ?? / 4.01 | ✅ | ☑️ | START | Maintenir appuyé le bouton PAIR pendant 2 secondes | Maintenir appuyé le bouton PAIR pendant 2 secondes | [Voir le manuel sur le site de 8Bitdo](https://download.8bitdo.com/Manual/Controller/SN30-Pro+/SN30_Pro+_Manual.pdf) |
| SN30 Pro v2.0 | ? / 1.32 | ✅ | ☑️ | START + B | Maintenir appuyé le bouton PAIR pendant 3 secondes | Maintenir appuyé le bouton Select pendant 3 secondes | [Voir le manuel sur le site de 8Bitdo](https://download.8bitdo.com/Manual/Controller/SN30pro+SF30pro/SN30pro_Manual.pdf) |
| SN30 & SF30 Pro | 1.23 / 1.33 | ✅ | ☑️ | START + B | Maintenir appuyé le bouton PAIR pendant 3 secondes | Maintenir appuyé le bouton Select pendant 3 secondes | [Maintenir appuyé le  bouton Select pendant 8 secondes](https://download.8bitdo.com/Manual/Controller/SN30pro+SF30pro/SN30pro+SF30pro_Manual.pdf) |
| SN30 Pro USB | ? / 1.03 | ? | n/a | n/a | n/a| n/a | [Voir le manuel sur le site de 8Bitdo](https://download.8bitdo.com/Manual/Controller/SN30pro-USB/SN30-Pro-USB.pdf) |
| SN30 GP | ? / 6.11 | ? | ? | START | Maintenir appuyé le bouton Select pendant 3 secondes | Maintenir appuyé le bouton Select pendant 3 secondes | [Voir le manuel sur le site de 8Bitdo](https://download.8bitdo.com/Manual/Controller/SN30+SF30/SN30_Manual.pdf) |
| SN30 Retro Set | ? / 4.10 | ? | ? | START | Maintenir appuyé le bouton Select pendant 3 secondes | Maintenir appuyé le bouton Select pendant 3 secondes | [Voir le manuel sur le site de 8Bitdo](https://download.8bitdo.com/Manual/Controller/SN30+SF30/SN30_Analogue.pdf) |
| SN30 | 2.62 / 4.20 | ✅ | ☑️ | START + R | Maintenir appuyé le bouton Select pendant 3 secondes | Maintenir appuyé le bouton Select pendant 3 secondes | [Maintenir appuyé le bouton Select pendant 8 secondes](https://download.8bitdo.com/Manual/Controller/SN30+SF30/SN30_Analogue.pdf) |
| SF30 &amp; SFC30 | 2.70 / 4.20 | ✅ | ☑️ | START + R | Maintenir appuyé le bouton Select pendant 3 secondes | Maintenir appuyé le bouton Select pendant 3 secondes | [Maintenir appuyé le bouton Select pendant 8 secondes](https://download.8bitdo.com/Manual/Controller/SN30+SF30/SN30+SF30_Manual_V4.pdf) |

### Manettes de style SNESxNES :

| Manette | FW (min / rec*) | USB | BT | Mode de démarrage | Association BT* | Réinitialisation BT | Réinitialisation d'usine |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| N30 Pro 2 | ? / 6.10 | ? | ? | START | Maintenir appuyé le bouton Pair pendant 1 seconde | Maintenir appuyé le bouton Pair pendant 3 secondes | [Voir le manuel sur le site de 8Bitdo](https://download.8bitdo.com/Manual/Controller/N30pro2/N30pro2-Manual.pdf) |
| N30 &amp; NES30 Pro | 1.62 / 4.10 | ✅ | ☑️ | POWER | Maintenir appuyé le bouton Pair pendant 1 seconde | Maintenir appuyé le bouton Pair pendant 3 secondes | [Maintenir appuyé le bouton Start pendant 8 secondes](https://download.8bitdo.com/Manual/Controller/N30pro+F30pro/N30pro+F30pro_Manual_V4.pdf) |
| F30 &amp; FC30 Pro | 1.62 / 4.10 | ✅ | ☑️ | POWER + X | Maintenir appuyé le bouton Pair pendant 1 seconde | Maintenir appuyé le bouton Pair pendant 3 secondes | [Maintenir appuyé le bouton Start pendant 8 secondes](https://download.8bitdo.com/Manual/Controller/N30pro+F30pro/N30pro+F30pro_Manual_V4.pdf) |

### Manettes de style NES :

| Manette | FW (min / rec*) | USB | BT | Mode de démarrage | Association BT* | Réinitialisation BT | Réinitialisation d'usine |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| N30 NS | ? / 6.10 | ? | ? | START | Maintenir appuyé le bouton Select pendant 3 secondes | Maintenir appuyé le bouton Select pendant 3 secondes | [Voir le manuel sur le site de 8Bitdo](https://download.8bitdo.com/Manual/Controller/N30-NS/N30-NS.pdf) |
| N30 &amp; NES30 | 2.62 / 4.20 | ✅ | ☑️ | START + R | Maintenir appuyé le bouton Select pendant 3 secondes | Maintenir appuyé le bouton Select pendant 3 secondes | [Maintenir appuyé le bouton Select pendant 8 secondes](https://download.8bitdo.com/Manual/Controller/N30+F30/N30+F30_Manual_V4.pdf) |
| F30 &amp; FC30 | 2.62 / 4.20 | ✅ | ☑️ | START + R | Maintenir appuyé le bouton Select pendant 3 secondes | Maintenir appuyé le bouton Select pendant 3 secondes | [Maintenir appuyé le bouton Select pendant 8 secondes](https://download.8bitdo.com/Manual/Controller/N30+F30/N30+F30_Manual_V4.pdf) |

### Manettes de style MEGADRIVE :

| Manette | FW (min / rec*) | USB | BT | Mode de démarrage | Association BT* | Réinitialisation BT | Réinitialisation d'usine |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| M30 | 1.13 / 1.14 | ❌ | ☑️ | START | Maintenir appuyé le bouton Select pendant 1 seconde | Maintenir appuyé le bouton Select pendant 3 secondes | [Voir le manuel sur le site de 8Bitdo](https://download.8bitdo.com/Manual/Controller/M30/M30_Manual.pdf) |

### Manettes de style ARCADE :

| Manette | FW (min / rec*) | USB | BT | Mode de démarrage | Association BT* | Réinitialisation BT | Réinitialisation d'usine |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| N30 &amp; NES30 Arcade Stick | 1.41 / 5.01 | ✅ | ☑️ | HOME | Maintenir appuyé le bouton Select pendant 3 secondes | Maintenir appuyé le bouton Select pendant 3 secondes | [Voir le manuel sur le site de 8Bitdo](https://download.8bitdo.com/Manual/Controller/Joystick/N30-Arcade-Manual.pdf) |
| F30 & FC30 Arcade Stick | 1.42 | ✅ | ☑️ | HOME | Maintenir appuyé le bouton Select pendant 3 secondes | Maintenir appuyé le bouton Select pendant 3 secondes | [Voir le manuel sur le site de 8Bitdo](https://download.8bitdo.com/Manual/Controller/Joystick/F30-ArcadeJoystick-eng.pdf) |

### Autres styles de manettes :

| Manette | FW (min / rec*) | USB | BT | Mode de démarrage | Association BT* | Réinitialisation BT | Réinitialisation d'usine |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| Lite | ?? / 1.20 | ❌ | ❌ | HOME | Maintenir appuyé le bouton Pair pendant 2 secondes | Maintenir appuyé le bouton Pair pendant 1 seconde | [Voir le manuel sur le site de 8Bitdo](https://download.8bitdo.com/Manual/Controller/Lite/Lite_Manual.pdf) |
| Zero | n/a | ❌ | ☑️ | START + R1 | Maintenir appuyé le bouton Select pendant 3 secondes | Maintenir appuyé le bouton Select pendant 3 secondes | [Maintenir appuyé le bouton Select pendant 8 secondes](https://download.8bitdo.com/Manual/Controller/ZERO/ZERO_Manual_V4.pdf) |
| Zero 2 | ?? / 1.05 | ✅ | ☑️ | START + R1 | Maintenir appuyé le bouton Select pendant 3 secondes | Maintenir appuyé le bouton Select pendant 3 secondes | [Maintenir appuyé le bouton Select pendant 8 secondes](https://download.8bitdo.com/Manual/Controller/Zero2/Zero2_Manual.pdf) |

* FW = Firmware
* min = Minimum
* rec = Recommandé
* BT = Bluetooth

## Dongles 8Bitdo Supportés

| Dongle | FW (min / rec*) | Compatibilité avec Recalbox | Manettes compatibles |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| USB Adapter for PS Classic (in X-Input Mode) | 1.28 / 1.33 | ✅ | PS3, PS4, Xbox One S, Wii Mote, Wii U Pro, Switch Joycons, Switch Pro, M30, SFC30, SF30 Pro. |
| USB Adapter Receiver (in X-Input Mode) | 1.28 / 2.00 | ✅ | PS3, PS4, PS5, Xbox One S, Xbox One X, Wii Mote, Wii U Pro, Switch Joycons, Switch Pro, M30, SFC30, SF30 Pro. |
| GBros Adapter Emeter (With 8Bitdo Receiver) | 2.23 / 2.24 | ✅ | GameCube, GameCube Wavebrid (avec le câble d'extension), Wii Classic, Wii Classic Pro, NES Mini, SNES Mini. |
| NES & SNES Mini Receiver | ? / 1.14 | ? | ? |
| SNES Receiver | ? / 1.34 | ? | ? |

## Support 8Bitdo

* [Support des manettes 8Bitdo](https://support.8bitdo.com/)
* [Outil de mise à jour](https://support.8bitdo.com/firmware-updater.html) 

Les manettes 8Bitdo ne doivent plus être associées avant de les associer de nouveau. Si vous avez un soucis d'association, vous pouvez tenter la méthode « Réinitialisation du Bluetooth », et si cela ne fonctionne pas, vous pouvez tenter la méthode « Réinitialisation d'usine ».

Vous pouvez trouver le manuel de chacune de vos manettes sur le site de [support 8Bitdo](https://support.8bitdo.com/).

## Faq

### Comment recharger votre manette ?

De préférence, utilisez le port USB de votre ordinateur ou console afin de recharger votre manette.
En cas d'utilisation d'un chargeur classique ou d'un chargeur de téléphone, veillez à ce que celui-ci ne dépasse jamais les 5V 1A.
Une intensité trop élevé peut endommager la batterie et rendre la manette inutilisable en Bluetooth.

### Comment mettre à jour le Firmware ?

Il suffit de vous rendre sur le site de [support 8Bitdo](https://support.8bitdo.com/) et de télécharger le logiciel `Upgrade tool` (Outil de mise à jour) en haut de page.

1. Installez l'application téléchargée `8BitDo Firmware Upgrader`.
2. Ouvrez l'application
3. Une fois l'application ouverte, celle-ci va vous demander de brancher votre manette à votre PC.
4. Votre manette sera automatiquement reconnue. Cliquez sur le bouton `Firmware Update` pour accéder aux mises à jour disponibles pour votre manette.
5. Sélectionnez le firmware souhaité et cliquez sur le bouton bleu `Update`.
6. Suivez la procédure indiquée pour effectuer la mise à jour. Celle-ci aura une barre de progression rouge.
7. Une fois la mise à jour réussie, cliquez sur le bouton vert `Success` et vous pouvez déconnecter votre manette.

### Comment allumer/connecter ma manette correctement ?

En fonction du périphérique (Windows, Mac OS, Android, Switch, etc.), il existe différentes combinaisons d'allumage :

* `START` + `Y` = Mode Switch
* `START` + `A` = Mode macOS
* `START` + `X` = Mode PC
* `START` + `B` = Mode Bluetooth (souvent le meilleur pour Android)

Vous devez maintenir les touches simultanément jusqu'à allumage de la manette et effectuer l'association.

En cas de connection via USB, maintenir les touches simultanément jusqu'à allumage de la manette, puis relier le câble USB.

### Comment connecter ma manette à un autre périphérique ?

La manette ne retient qu'une seule adresse de périphérique externe. Si vous souhaitez passer de votre Switch à Android par exemple, il faudra réinitialiser les données d'association de la manette. En fonction des modèles, la procédure diffère un peu. Veuillez consulter le tableau plus haut pour connaitre la procédure.

### Comment puis-je appairer mon adaptateur USB avec ma manette PS3 ?

Il existe un outil permet de gérer votre manette PS3 et les adaptateurs USB.

### {.tabset}
#### Windows

Vous pouvez télécharger cet [utilitaire](https://download.8bitdo.com/Tools/Receiver/8Bitdo_PS3_Tool_Win_V1.1.zip)

En cas de problème avec Windows, il existe 2 solutions :

**Solution 1** :
Installer SixaxisPairToolSetup-0.3.1.exe. Vous pouvez le télécharger sur [cette page](https://sixaxispairtool.en.lo4d.com/download).

**Solution 2** :
Désactiver temporairement le renforcement de la signature des pilotes de Windows. Vous trouverez des images d'aide dans le dossier `How To Temporarily Disable Driver Signature Enforcement On Win8 And Win10` contenu dans le fichier `8Bitdo_PS3_Tool_Win_V1.1.zip`.

En cas d'autres problèmes, vous pouvez installer le pilote 'libusb-win32'.

>La fonction du renforcement de la signature des pilotes de Windows peut se réactiver lors d'une mise à jour ou autre.
{.is-info}

#### macOS
Vous pouvez télécharger cet [utilitaire](https://download.8bitdo.com/Tools/Receiver/8BitDo_PS3_Tool_MacOS_V1.1.zip)

### Qu'en est-il des sticks Arcade ?

En fonction du périphérique (Windows, Mac OS, Android, Switch, etc.), il existe différentes combinaisons d'allumage :

* Windows : position XINPUT.  
* Android : position DINPUT.  
* MacOS et Switch sont non nécessaire.  
  
Procédure d'allumage :

1. Sélectionnez le mode XINPUT ou DINPUT si nécessaire.
2. Appuyez sur le bouton PAIR pour initialiser l'appareillage (La led bleu clignote).
3. Configurez l'appareillage sur votre périphérique (Windows, macOS, Android ou Switch).
4. La led doit se figer en bleu et la manette est prête à être utilisée.

Vous devez maintenir les touches simultanément jusqu'à allumage de la manette, puis effectuer l'association. En cas de connection via USB, maintenez les touches simultanément jusqu'à allumage de la manette, puis relier le câble USB.

### Quel firmware est conseillé pour Recalbox ?

Veuillez consulter le tableau en haut de page pour connaître les firmwares recommandés pour votre manette.

### Qu'en est-il de la compatibilité avec le pad zero V1 ?

Le pad zero v1 fonctionne en Bluetooth uniquement. Allumez la manette en maintenant les boutons `START` + `R1`.

En cas d'utilisation de deux pads zero V1 :

- Première manette, maintenez les boutons `START` + `R1`.
- Deuxième manette, maintenez les boutons `START` + `R1` + `B`.

### Mon pad n'est pas reconnu avec le cable USB ?

Vérifiez que vous utilisez bien le câble fourni avec la manette. Sinon, assurez-vous d'avoir un câble USB de transfert de données et non le câble USB de charge uniquement. Certains câbles USB ne laisse passer que le courant de charge et non le flux de données.