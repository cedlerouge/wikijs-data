---
title: PC compatibles
description: 
published: true
date: 2021-08-12T07:44:07.034Z
tags: pc, compatibilité
editor: markdown
dateCreated: 2021-05-21T12:33:52.897Z
---

>**Information :**
>
>Si vous **ne voyez pas votre PC** dans les tableaux ci-dessous, ce dernier **n'a pas encore été testé**.
>Vous pouvez aussi tester le matériel avec la dernière version de Recalbox si cela n'a pas encore été fait, les informations peuvent être mises à jour (veuillez nous en informer si le matériel testé est compatible).
>
>* Spécifiez la **marque** et le **modèle.**
>* Si c'est un PC assemblé, spécifiez le CPU et le GPU.
>* N'hésitez pas à **ajouter des détails concernant les performances** de votre configuration. 
>* Si vous avez le moindre problème, veuillez nous le faire savoir.
{.is-info}

## Modèle de PC

### Dell

| Nom | État | Version de Recalbox | Notes |
| :---: | :---: | :---: | :---: |
| Dell Optiplex 390 i5-2400 3.10GHz | ✅ | 6.0 Rc3 | 100% Compatible |
| Dell Optiplex 990 i3 2100 | ✅ | 13.07.18 | 100% Compatible |
| Dell Optiplex 990 USFF i5 2400S 2.5GHZ | ✅ | 6.0.1 | 100% Compatible |
| Dell Optiplex 3020 micro i3 4150T | ✅ | 6.0 Rc3 | 100% Compatible |

### Intel

| Nom | État | Version de Recalbox | Notes |
| :---: | :---: | :---: | :---: |
| Intel NUC7i3BNK | ✅ | 4.1 | i3-7100U / HD Graphics 620 / BIOS 0036:OK / BIOS 0052:No temp probe |
| Intel NUC7i5BNH | ✅ | 6.1 | 100% compatible, autour de 60 FPS pour toutes les consoles, CPU Core i5-7260U, carte graphique Intel IRIS Plus 640 |
| Intel NUC6I7KYK | ✅ | 4.1 | 100% compatible, autour de 60 FPS pour toutes les consoles, dernier bios 0050, tout le matériel est ok: CPU Core i7-6770HQ, carte graphique Intel IRIS PRO 580 entièrement supporté, wifi, bluetooth, sound, usbs, SSD SATA Samsung 1TB |
| Intel NUC8i7BEH | ✅ | 6.1 | 100% compatible, autour de 60 FPS pour toutes les consoles, CPU Core i7-8559U, carte graphique Intel IRIS Plus 655 |

### Divers

| Nom | État | Version de Recalbox | Notes |
| :---: | :---: | :---: | :---: |
| HP Slim Desktop 270-P026 i3-7100T | ✅ | 18.07.13 | 100% Compatible |
| Laptop 17.3" MSI GP72M 7RDX-871XFR | ✅ | 4.1 | Autour de 60 FPS pour toutes les consoles avec la version x86_64 avec les paramètres par défaut, dernier bios flashé, tout le matériel est ok : CPU Core i7-7700HQ, carte graphique Intel HD Graphics 630 entièrement supportée, wifi, bluetooth, son (sur haut-parleurs + casque + HDMI), usbs, SSD SATA Samsung 1TB, tout est ok ! Depuis qu'il a une batterie, les jeux ne sont plus interrompus ! |
| Taichi  Z270 + I5 7500 | ✅ | 4.1 | Autour de 60 FPS sur toutes les consoles sur la version x86_64 |
| ZOTAC  ZBOX Nano AD10 | ✅ | 4.1 | Wii/NGC est ok mais pas jouable. |

### Apple Mac

| Nom | État | Version de Recalbox | Notes |
| :---: | :---: | :---: | :---: |
| MacBook Pro (13 pouces, début 2011) | ✅ | 4.1 | 100% compatible |
| iMac (21.5 pouces, Mid 2011) | ✅ | 4.1 | 100% compatible, quelques bugs mineurs graphiques sur le menu. |
| Mac Pro (fin 2013) | ❌ | 4.1 | Ne fonctionne pas, ne démarre pas. |
| MacBook (13 pouces, fin 2009) | ✅ | 7.2.1 | 100% compatible |
| MacBook Pro (15 pouces, 2018) | ✅ | 7.3 | Pas de sortie son, clavier non fonctionnel. [Désactiver la puce T2 pour démarrer.](./../tutorials/others/disable-mac-t2-chip) |

## CPU

### Intel

| Nom | État | Version de Recalbox | Notes |
| :---: | :---: | :---: | :---: |
| Intel i3-4150T 3.00GHz | ✅ | 6.0 Rc3 | Autour de 60 FPS pour toutes les consoles sur la version x86_64 avec les paramètres par défaut, Dolphin fonctionne bien avec les paramètres par défaut |
| Intel Core i3-7100T | ✅ | 18.07.13 | Autour de 60 FPS pour toutes les consoles sur la version x86_64 avec les paramètres par défaut |
| Intel Core i3-7100U | ✅ | 4.1 |  |
| Intel Core i5-2400 3.10 GHz | ✅ | 4.1 | Fonctionne bien pour toutes les consoles à l'exception de la GameCube et de la Wii (meilleures performances avec une carte graphique) |
| Intel Core i5-2400S 2.5 GHZ | ✅ | 7.0.1 |  |
| Intel Core i5-3570K 3.40 GHz | ✅ | 6.0 Rc3 | Autour de 60 FPS pour toutes les consoles on x64 version with default parameters |
| Intel Core i5-4570S | ✅ | 4.1 |  |
| Intel Core i5-6500 | ✅ | 4.1 |  |
| Intel Core i5-7500 | ✅ | 4.1 |  |
| Intel Core i7-2600K | ✅ | 4.1 | Autour de 60 FPS pour toutes les consoles sur la version x86_64 avec les paramètres par défaut |
| Intel Core i7-6770HQ | ✅ | 4.1 | Autour de 60 FPS pour toutes les consoles sur la version x86_64 avec les paramètres par défaut |
| Intel Core i7-7700HQ | ✅ | 4.1 | Autour de 60 FPS pour toutes les consoles sur la version x86_64 avec les paramètres par défaut |
| Intel Pentium G2130 3.20GHZ | ✅ | 4.1 |  |
| Intel G4600 | ✅ | 18.04.20 | Autour de 60 FPS pour toutes les consoles sur la version x86_64 avec les paramètres par défaut |

### AMD

| Nom | État | Version de Recalbox | Notes |
| :---: | :---: | :---: | :---: |
| AMD X2 4850B | ✅ | 4.1 |  |

## GPU

>**Attention :**  
>Veuillez garder en tête que la carte graphique ne fait pas tout, le kernel Linux contenu dans Recalbox doit aussi être capable de gérer le périphérique.
{.is-info}

### Nouveau

Nouveau est un projet de la fondation X.Org et Freedesktop.org visant à fournir des pilotes graphiques Nvidia libre de droit par rétro-ingéniérie.

Nouveau est basé sur les pilotes libres (mais obscures) nv maintenus par Nvidia pour la gestion 2D. 

Pour supporter OpenGL, le projet utilise Mesa 3D, mais a changé en Février 2018 par son successeur Gallium3D4.5. Le support d'OpenCL se fait aussi par le biais de Gallium3D. 

#### Processus utilisé

Le projet utilise la technique dite rétro-ingéniérie sur les cartes graphiques Nvidia en étudiant comment fonctionne les pilotes 3D pour Linux, fournis par le fabricant (sous licence propriétaire), sans toucher aux pilotes. Cette façon de faire permet au projet d'éviter les conflits avec la licence Nvidia.

* Selon la [page de fonctionnalités de nouveau](https://nouveau.freedesktop.org/wiki/FeatureMatrix/)
* Selon la compatibilité avec NVIDIA ([NVidia codenames](https://nouveau.freedesktop.org/wiki/CodeNames/))
* Le pilote Nouveau dans Recalbox (Version 1.0.15)

>**Remarque :**  
>Les pilotes Nouveau sont utilisés par défaut quand une carte graphique Nvidia est détectée mais pas compatible avec les pilotes officiels.
{.is-warning}

### NVIDIA

Les cartes graphiques Nvidia sont compatibles avec le **pilote officiel** dans sa version 460.67 auquel vous trouverez la liste des cartes graphiques compatible [ici](https://gitlab.com/recalbox/recalbox-hardware/blob/master/videocard/nvidiacheckcompatibility-460.txt).

### Intel

Liste des cartes graphiques intégrées au processeur.

Leur [code source](https://cgit.freedesktop.org/xorg/driver/xf86-video-intel/tree/?id=b57abe20e81f4b8e4dd203b6a9eda7ff441bc8ce) n'est pas très explicite sur les cartes graphiques supportées.

| Nom | État | Version de Recalbox | Notes |
| :---: | :---: | :---: | :---: |
| Intel® HD Graphics 630 | ✅ | 4.1 | Autour de 60 FPS pour toutes les consoles sur la version x86_64 avec les paramètres par défaut |
| Intel® HD Graphics 3000 | ✅ | 4.1 | Autour de 60 FPS pour toutes les consoles sur la version x86_64 avec les paramètres par défaut |
| Intel® Iris™ Plus Graphics 620 | ✅ | 4.1 | ​ |
| Intel® Iris™ Pro Graphics 580 | ✅ | 4.1 | Autour de 60 FPS pour toutes les consoles sur la version x86_64 avec les paramètres par défaut |
| Intel® HD Graphics 4600 | ✅ | 4.1 | Autour de 60 FPS pour toutes les consoles sur la version x86_64 avec les paramètres par défaut |
| Intel® HD Graphics 4400 | ✅ | 6.0 rc3 | Autour de 60 FPS pour toutes les consoles sur la version x86_64 avec les paramètres par défaut |
| Intel® HD Graphics 4850 | ✅ | 6.0 DragonBlaze | Autour de 60 FPS pour toutes les consoles sur la version x86_64 avec les paramètres par défaut |

### AMD

Leur [code source](https://cgit.freedesktop.org/xorg/driver/xf86-video-amdgpu/tree/?h=xf86-video-amdgpu-1.4.0) n'est pas très explicite sur les cartes graphiques supportées.

| Nom | État | Version de Recalbox | Notes |
| :--- | :---: | :---: | :---: |
| AMD Radeon HD 4850 512mo | ✅ | 6.0 Rc3 | Fonctionne avec le CPU Intel i5-3570K |
| AMD Radeon HD 4890 1Go | ✅ | 4.1 | ​ |
| AMD Radeon HD 6750M | ✅ | 4.1 | Quelques bugs graphiques mineurs sur le menu |
| AMD Radeon HD 7970 3G | ❌ | 18.04.20 | Ne fonctionne pas (le menu est très lent et n'est pas jouable) |
| AMD Radeon HD 8280E | ❌ | 18.07.13 | Ne fonctionne pas (le menu est très lent et n'est pas jouable) |
| AMD Radeon RX 480 8gb | ❌ | 6.0 RC2 | Ne fonctionne (le menu est très lent et n'est pas jouable) |