---
title: Comment contribuer au wiki Recalbox
description: Découvrez comment vous pouvez contribuer au wiki
published: true
date: 2021-08-12T07:34:58.915Z
tags: wiki, contribution
editor: markdown
dateCreated: 2021-05-25T19:34:38.241Z
---

## Nomenclature des documents

Les documents suivent une nomenclature spécifique qui permet au wiki de Recalbox d'avoir des URL identiques pour les différentes traductions des documents. 

Par exemple cette page, dans sa traduction française a pour URL :
`/fr/contribute`
Et en anglais :
`/en/contribute`

Cela permet de retrouver très vite la traduction d'un document, ou de créer des traductions plus facilement pour les contributeurs.

C'est pourquoi les URL des documents apparaissent en anglais.

## Règles de formatage

Les documents sont rédigés en [markdown](https://docs.requarks.io/editors/markdown). Le markdown est un outil de mise en forme de document très simple et accessible. N'hésitez pas à prendre exemple sur ce document pour commencer a rédiger le vôtre.

### Titres

Le titre de la page est à définir dans les options de la page. Il est le seul titre de niveau 1 du document.
![title-metadata.png](/contribute/title-metadata.png)

Dans le contenu des pages, les titres commencent donc par le niveau 2 ( double hash en markdown: `##`):
![double-hash.png](/contribute/double-hash.png)

### Blocs de citation

Pour notifier l'utilisateur d'une information ou d'un élément important, vous pouvez utiliser les blocs de citation:

```markdown
> Ceci est une information
> {.is-info}
```

> Ceci est une information
> {.is-info}

## Version de markdown supporté

Pour plus d'informations sur les fonctionnalités markdown supportées, visitez https://docs.requarks.io/editors/markdown.

## Images

Lorsque vous ajoutez des images à vos documents, veillez à reproduire la hiérarchie du document dans les dossiers des images, en omettant la langue.

Par exemple pour cette page, les images seront créées dans le répertoire `/contribute`

## Internationalisation

### Procédure de traduction d'une page

1. Connectez vous sur le wiki avec votre compte Discord
2. Rendez vous sur la page à traduire, dans la langue par défaut du wiki (FR)
3. Cliquez sur l'icone des langues en haut de la page et sélectionnez la langue dans laquelle vous allez traduire la page
![select-lang.png](/contribute/select-lang.png)
4. Un écran vous propose de créer une nouvelle page, cliquez sur le bouton créer la page
5. Cliquez sur l’éditeur Markdown
![markdown.png](/contribute/markdown.png)
6. Ouvrez un nouvel onglet sur la page originale et cliquez sur éditer dans le menu des actions de la page
![edit.png](/contribute/edit.png)
7. Accedez au titre, tags et description de la page en cliquant sur le bouton PAGE:
![editpage.png](/contribute/editpage.png)
8. Vous pouvez traduire ces éléments et les ajouter dans le formulaire de votre page nouvellement créée
9. Aprés avoir fermé la popup du titre sur la page originale, copiez le contenu markdown de la page, et rendez-vous sur https://www.deepl.com/translator pour coller votre texte et récupérer une version pré traduite de la page.
![deepl.png](/contribute/deepl.png)
10. Il ne vous reste plus qu'à vérifier la traduction, les images et les liens de la page nouvelle créée !
11. N'oubliez pas d'enlever la derniere ligne du texte deepl (traduit par deepl)
12. Merci beaucoup pour votre contribution 🙏


### Liens relatifs

Sauf cas exceptionnels, les liens des pages doivent êtres relatifs à la langue courante.

Par exemple, dans cette page, un lien vers la page `/fr/home` devra être renseigné avec l'URL `./home` :

```markdown
Visitez l'[Accueil](./home)
```

Cela permet de créer des pages de traduction ayant déjà des liens valides.

### Ajout de nouvelles langues

Si votre langue n'apparait pas dans le menu du wiki et que vous souhaitez commencer une traduction, veuillez vous rapprocher de l'équipe sur nos **[réseaux sociaux, Discord ou forum](./presentation/useful-links).**

## Niveau de difficulté des tutoriels

Afin de rendre l'expérience Recalbox ludique et accessible, lorsque vous créez un tutoriel, mentionnez son niveau de difficulté, elle est étalonnée de la façon suivante :

1.  Toute action qui ne nécessitera pas d'entrer dans les paramètres avancés du menu général ou les fichiers .conf de Recalbox sera considérée comme : _niveau_ :star:

1. Toute action qui nécessitera de configurer les paramètres avancés afin d'apporter des modifications susceptibles de causer des instabilités : _niveau_ :star::star:

1.  Toute action qui nécessitera de modifier des entrées dans le fichier recalbox.conf : _niveau_ :star::star::star: (prudence recommandée).

1.  Toute action qui nécessitera des manipulations SSH et/ou des modifications de fichiers de boot (MobaXTerm ou Powershell le Staff ne s'est pas encore mis d'accord sur le logiciel à utiliser) : _niveau_ :star::star::star::star:

1.  Toute action qui nécessitera de faire une merge request : _niveau_ :star::star::star::star::star:

1.  Enfin tous tutoriels qui tenteraient d'expliquer l'utilisation de clrmamepro sera d'office rangé dans la catégorie : :scream:_**Cauchamardesque**_

Exemple :

- Mettre une rom, un bios, changer la langue : niveau :star: 
- Changer le thème, restaurer les paramètres d'usines, changer le core par défaut : niveau :star::star: 
- Activer la traduction retroarch IA : niveau :star::star::star:

*Pour faire apparaître l'émoji étoile sur vos tutoriels, mettez des ":" devant et derrière "star" =   : star : tout attaché.*
