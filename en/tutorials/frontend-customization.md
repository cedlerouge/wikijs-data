---
title: 🛠️ Frontend customization
description: (EmulationStation / RetroArch)
published: true
date: 2021-08-06T15:17:47.439Z
tags: tutorial, frontend, customization
editor: markdown
dateCreated: 2021-08-06T08:04:46.491Z
---

Here you can customize EmulationStation like with themes and other music, for example.

Here are the available tutorials:

[Add custom music into EmulationStation](add-custom-music-into-emulationstation)
[Add themes into EmulationStation](add-themes-into-emulationstation)
[Integer scale (Pixel Perfect)](pixel-perfect)