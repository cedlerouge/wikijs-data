---
title: Share
description: 
published: true
date: 2021-08-06T14:55:09.148Z
tags: share
editor: markdown
dateCreated: 2021-08-06T08:26:28.294Z
---

Here you can find help for everything that is sharing on your home network.

Here are the available tutorials:

[Load roms on a network share with Samba (NAS)](roms-on-a-network-share-samba-nas)
[Unable to access to Recalbox from network share](access-network-share)