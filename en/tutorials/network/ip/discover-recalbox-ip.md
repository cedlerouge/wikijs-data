---
title: Discover your Recalbox's IP address
description: 
published: true
date: 2021-08-06T14:56:53.557Z
tags: address, ip, discover
editor: markdown
dateCreated: 2021-08-06T08:34:57.598Z
---

## Prerequisites

Make sure your Recalbox is turned on and connected to the network via an Ethernet cable or a Wifi dongle.

You have a doubt and your network has an Internet access? Nothing could be easier, go to the Recalbox menu and click on `UPDATES`. If a pop-up window appears with the message `An update is available` or `No update is available`, then your Recalbox does have Internet access and therefore, network access. If the message `Please connect a network cable` is displayed, either you don't have Internet on the network which is not a problem, or you have a configuration problem on your router, a defective cable or a wifi dongle that doesn't work.

>The IP address is not fixed and can change at each reboot.   
>You can define one yourself via your modem/router or via [this tutorial](./../../../tutorials/network/ip/static-manual-ip).
{.is-warning}

## From the Recalbox network settings

Press `START` and go to `NETWORK SETTINGS`. The current IP address is displayed here.

## From your computer

## {.tabset}

## Windows

* In the taskbar, you have a blank text box. Type in the command `cmd` and press `Enter`.
* A command prompt should be open. Type `ping recalbox` and press `Enter`.
* You should get the following message:

```shell
> Sending a ping request to RECALBOX with 32 bytes of data:  
> Response from 192.168.0.XX : bytes=32 time=1 ms TTL=128  
> Answer from 192.168.0.XX : bytes=32 time=1 ms TTL=128  
> Response from 192.168.0.XX : bytes=32 time=1 ms TTL=128  
> Ping statistics for [...]
```

* You have your IP address.

### macOS

* Go to `Applications/Utilities/Terminal`.
* A terminal window should be visible. Type `arp -a` and press `Enter`.
* A list of all devices connected to the network, in the form of an IP address list, should be visible.
* The IP address of your Recalbox is often something like `192.168.0.XX`.
* You have your IP address.

## Other solutions

* Smartphone applications are available to scan your network. Go to your device's store to find the application of your choice.
* Another solution is to go to the router settings of your Internet box. The list of connected devices should reference your Recalbox.