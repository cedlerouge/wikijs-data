---
title: Manually define an IP address
description: 
published: true
date: 2021-08-06T14:57:15.396Z
tags: address, ip, manual
editor: markdown
dateCreated: 2021-08-06T08:36:33.206Z
---

With Recalbox, it is possible to change the IP address manually in the [recalbox.conf](./../../../basic-usage/getting-started/recalboxconf-file) file.

Open the `recalbox.conf` file and find these lines:

```ini
## Wifi - static IP
## if you want a static IP address, you must set all 3 values (ip, gateway, and netmask)
## if any value is missing or all lines are commented out, it will fall back to the
## default of DHCP
;wifi.ip=manual ip address
;wifi.gateway=new gateway
;wifi.netmask=new netmask
```

You must remove the `;` in front of the last three lines, or the configuration will not work, and fill them in with the desired information, for example:

```ini
wifi.ip=192.168.1.10
wifi.gateway=192.168.1.254
wifi.netmask=255.255.255.0
```