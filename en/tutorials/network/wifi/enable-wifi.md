---
title: Enable wifi
description: How to connect on wifi?
published: true
date: 2021-10-03T22:25:20.543Z
tags: wifi, enable
editor: markdown
dateCreated: 2021-08-06T08:48:43.715Z
---

## Prerequisites

* Check that your wifi dongle is part of the [compatibility list](./../../../hardware-compatibility/compatible-devices/wifi-dongles) or that you have a board that has wifi.
* Check that your wifi key is in WPA2 and does not contain any special characters.
* Check that your wifi is in discovery mode.
* If your access point uses channel 12 or 13, try to change the [wifi country code](./wifi-country-code)

## Configuration

The configuration is done via EmulationStation very easily.

* Press `Start` > `NETWORK SETTINGS` and set the `ENABLE WIFI` option to **On**. 
* You have 2 ways to configure wifi:
  * You can enter the SSID and password for your wifi connection directly.
  * You can also use the WPS connection. To do this:
    * Select `WPS CONNECTION` in EmulationStation
    * On your modem, you need to enable the WPS function. It will automatically remain active for 2 minutes.