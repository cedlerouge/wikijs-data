---
title: IP address
description: 
published: true
date: 2021-08-06T14:56:17.029Z
tags: address, ip
editor: markdown
dateCreated: 2021-08-06T08:16:51.335Z
---

A problem with the IP address of your Recalbox? You should find your solution there!

Here are the available tutorials:

[Discover your Recalbox's IP address](discover-recalbox-ip)
[Manually define an IP address](static-manual-ip)