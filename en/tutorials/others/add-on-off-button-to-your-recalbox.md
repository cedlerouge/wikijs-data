---
title: Add on/off button to your Recalbox
description: 
published: true
date: 2021-08-06T15:24:11.423Z
tags: on, off, button
editor: markdown
dateCreated: 2021-08-06T15:23:55.166Z
---

>You can now add an on/off button to your Recalbox. The button can be a push button (also called "momentary switch") or an ON/OFF button. 
{.is-info}

## To wire the button to the GPIO of the Raspberry Pi

* Connect one pin to the **GPIO3** (the **fifth** gpio from the top left), and the other to the ground on its right (the **sixth** gpio).

* Finally, you must enable button support in the [recalbox.conf](./../../basic-usage/getting-started/recalboxconf-file) file by adding one of these lines:

  * `system.power.switch=PIN56ONOFF` for an ON/OFF button 
  * `system.power.switch=PIN56PUSH` for a push button

And you have a Recalbox that can be turned off/on with a single button!

## Add a reset button and an LED.

>There is another option where you can add a reset button and an LED.
{.is-info}

* In recalbox.conf, add/uncomment (by removing the ; at the beginning of the line) the following line:

`system.power.switch=PIN356ONOFFRESET` 

* To wire the reset button to the Raspberry Pi GPIO:
   * Connect one pin to GPIO2 (the third gpio from the top left)
   * Connect the other pin to the same ground as the on/off button (the sixth gpio).
* The LED is connected to GPIO14 (the eighth gpio) as well as to the ground.

#### To summarize

* _Power+_ on **GPIO 3** (**PIN 5**) 
* _Reset+_ on **GPIO 2** (**PIN 3**) 
* _LED+_ on **GPIO 14** (**PIN 8**) 
* _Power-_, _Reset-_ and _Led-_ on one of the **masses** (**PIN 6, 9, 14, 20, 25, 30, 34 or 39**)

>Note that this only works with a push button for reset and an ON/OFF button for power
{.is-info}

## Usage

### With the POWER Button (PIN 5 + GROUND)

* Short press _(off)_ : Turn on your Recalbox 
* Short press _(lit)_ : Exit the current emulator and return to the main menu 
* Long press _(lit)_ : Turn off your Recalbox _(equivalent to doing `SELECT` > `Turn off` normally)_

### With the RESET Button (PIN 3 + GROUND)

* Short press _(off)_ : - 
* Short press _(lit)_ : Reset the game, as in the old days on the console 
* Long press _(on)_ : Restart your Recalbox _(equivalent to `SELECT` > `Restart`)_