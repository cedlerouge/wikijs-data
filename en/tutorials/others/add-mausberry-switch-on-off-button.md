---
title: Add Mausberry switch on/off button
description: 
published: true
date: 2021-08-06T16:19:45.961Z
tags: on, off, button, mausberry, switch
editor: markdown
dateCreated: 2021-08-06T16:19:45.961Z
---

## Introduction

Mausberry circuits manage the activation and deactivation of the Raspberry Pi using an ON/OFF switch.  
This type of circuit can be used to use the on/off switch of a NES and its RESET button for example.

>This tutorial is dedicated to the Raspberry Pi 2/3 from Recalbox.
{.is-info}

## Procedure

### Connections and wiring

For the connections on the Raspberry Pi, use the GPIO ports 23 and 24 (pin 16 and 18).
The Mausberry is operational with GPIO23 = OUT / IN = GPIO24.

### Procedure

#### Prerequisites

* Your Raspberry Pi must be connected to the network
* You must know the [IP address](./../network/ip/discover-recalbox-ip) of your Raspberry Pi.

#### Steps

* Open your [recalbox.conf](./../../basic-usage/getting-started/recalboxconf-file) file
* In the `A - System Options` section, uncomment the setting to get this:

```ini
# ------------ A - System Options ----------- #
# Uncomment the system.power.switch you use
system.power.switch=MAUSBERRY # http://mausberry-circuits.myshopify.com/pages/setup
```

Save!

## Annexes

### Connections and wiring

![](/tutorials/others/gpio-mausberry.png)

### Script: setup.sh

```shell
echo '#!/bin/bash

# C'est la broche GPIO connectée au fil de l'interrupteur marqué OUT
GPIOpin1=20

# C'est la broche GPIO connectée au fil de l'interrupteur marqué IN
GPIOpin2=21

echo "$GPIOpin1" > /sys/class/gpio/export
echo "in" > /sys/class/gpio/gpio$GPIOpin1/direction
echo "$GPIOpin2" > /sys/class/gpio/export
echo "out" > /sys/class/gpio/gpio$GPIOpin2/direction
echo "1" > /sys/class/gpio/gpio$GPIOpin2/value
while [ 1 = 1 ]; do
power=$(cat /sys/class/gpio/gpio$GPIOpin1/value)
if [ $power = 0 ]; then
sleep 1
else
poweroff
fi
done' > /recalbox/scripts/mausberry.sh
chmod 777 /recalbox/scripts/mausberry.sh
```

### Script: S99maus

```shell
#!/bin/bash
### BEGIN INIT INFO
# Provides: mausberry.sh
# Required-Start: $network $local_fs $remote_fs
# Required-Stop: $network $local_fs $remote_fs
# Default-Start: 2 3 4 5
# Default-Stop: 0 1 6
# Short-Description: switch mausberry init script.
# Description: Starts and stops SwitchDaemon service.
### END INIT INFO

#VAR
RUN="/recalbox/scripts/mausberry.sh"
BTD_PID=$(ps -eo pid,command | grep "/bin/bash $RUN" | grep -v grep | awk '{print $1}')

serviceStatus() {
   if [ ! -z "$BTD_PID" ]; then
      echo -e '33[0mservice mausberry.sh ['$BTD_PID'] [33[33;32m OK 33[0m]'
   else
      echo -e '33[0mservice mausberry.sh [33[33;31m KO 33[0m]'
   fi
}

# Carry out specific functions when asked to by the system
case "$1" in
   start)
      echo "Starting script $RUN ..."
      if [ -z "$BTD_PID" ]; then
         nice -n 19 $RUN&

         if [ $? -eq 0 ]; then
            echo -e "33[0mscript $RUN [33[33;32m STARTED 33[0m]"
         fi
      else
         echo "script $RUN already started ['$BTD_PID']!"
      fi
      #serviceStatus
   ;;
   stop)
      echo "Stopping script $RUN ..."
      if [ ! -z "$BTD_PID" ]; then
         kill $BTD_PID

         if [ $? -eq 0 ]; then
            echo -e "33[0mscript $RUN [33[33;31m STOPPED 33[0m]"
         fi
      fi
      #serviceStatus
   ;;
   status)
      serviceStatus
   ;;
   *)
      echo "Usage: /etc/init.d/S99maus {start | stop | status}"
      exit 1
   ;;
esac

exit 0
```