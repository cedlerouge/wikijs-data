---
title: HiFiBerry audio
description: 
published: true
date: 2021-08-06T15:13:41.007Z
tags: hifiberry
editor: markdown
dateCreated: 2021-08-06T14:22:25.657Z
---

## General information

To obtain a quality audio signal on the Pi, one solution is to use a HiFiBerry DAC (Digital - Analog Converter). 

These **DAC** exist in several versions:

### Raspberry Pi A+, B+, 2 and 3

* DAC+ Light
* DAC+ Standard RCA
* DAC+ Standard Phone
* DAC+ Pro

### Raspberry Pi A and B

* DAC Standard version with RCA connectors

The specifications of the different cards can be compared on [**hifiberry.com**](http://www.hifiberry.com/).
The DAC+ cards can be connected directly to the GPIO of the Pi. Pins not used by the DAC can be reused for other purposes, in which case additional pins must be soldered to the DAC+ board.

Below is the wiring diagram of a HifiBerry DAC+: hifiberry

![](/tutorials/audio/gpio-hifiberry.png)

>The version for Raspberry A and B must be installed on pin 5.  
>Once the board is installed, there are a few more software configuration steps to get everything working
{.is-info}

## Tree structure and device configuration

* On Recalbox, you just have to add in the [recalbox-user-config.txt](./../system/access/remount-partition-with-write-access) the right DTB, to be set according to your card:
  * DAC/DAC+ Light `dtoverlay=hifiberry-dac`
  DAC+ Standard/Pro `dtoverlay=hifiberry-dacplus` * In the same file, add the following    
* Still in the same file, add the line `force_eeprom_read=0`.
* Then you have to select the default sound card. Then you have to modify `/etc/asound.conf` (create it if it does not exist) with the following content:
  * `pcm.!default {` `type hw card 0` `}` `ctl.!default {` `type hw card 0` `}`
* Now reboot your Raspberry Pi.

Now you should hear sound from your HiFiBerry card.

>You cannot change the sound volume in EmulationStation only by _Alsamixer / Amixer_.
{.is-info}

## Alsamixer / Amixer

## Alsamixer / Amixer

ALSA sound cards (and this is the case of HiFiBerry) can be controlled by Alsamixer.
Alsamixer is a graphical interface, while Amixer works in text mode, especially for scripts.

Since `alsa-utils` is installed on Recalbox, HifiBerry will work without additional utilities.

>The DAC+ Light and the old DAC have no settings possible in ALSA.
{.is-warning}
  
With the command `amix` you will have a text version of the different DAC settings. 
The best way to change the volume is to change the "Overall volume". Its name may differ depending on the card: 'PCM', 'Digital', 'Master', it's up to you.

The following commands allow you to change the volume through the terminal:

* `amixer sset 'Master' -- 90%` (not recommended because the scale is logarithmic)
* `amixer sset 'Master' -- -3dB` (in decibels, recommended setting)
* `amixer sset 'Master' - 2000` (empirical unit, not recommended)

>You can use Google to discover the rest of the possible commands with the keywords: amixer, alsamixer.
{.is-info}

Here is an example in python to handle the volume.

First, import the subprocess command :

```python
from subprocess import call

call(["amixer", "sset", "Digital", "--", str(YourDesiredVolume)+"dB"])
```

This script can be used to change the volume by using the GPIO.

Here is a summary of some commands:

* `amixer sset 'PCM' 70%` (percentage)
* `amixer sset 'Master' 3dB` (Decibel)
* `amixer sset 'Mic' 4` (material values)
* `amixer sset 'PCM' 10%+` (increase of current value. Unit can be % or dB)
* `amixer sset 'Line' cap` (Recording on/off: cap, nocap)
* `amixer sset 'Mic' mute` (Playback on/off: mute, unmute)
* `amixer sset 'Master' off` (On/Off: on/off)
* `amixer sset 'Mic Select' 'Mic1'` (Device name)

Source: [https://wiki.ubuntuusers.de/amixer/](https://wiki.ubuntuusers.de/amixer/)