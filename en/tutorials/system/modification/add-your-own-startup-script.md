---
title: Add your own startup script
description: 
published: true
date: 2021-09-04T21:55:06.078Z
tags: startup, script, add
editor: markdown
dateCreated: 2021-08-06T09:26:46.813Z
---

Follow these instructions if you want to create your own startup script that will be run automatically when Recalbox starts.

All startup scripts are located in `/etc/init.d`. They are executed one after the other. This means that the `S31emulationstation` script will be processed before `S99MyScript`. Depending on the filenames, you can determine the order of execution.

The startup procedure of a script will be executed at startup and the shutdown procedure at shutdown of the Recalbox.

## Steps to create your own startup script:

* The system is read-only by default. You must get [write access](./../access/remount-partition-with-write-access).
* Change the directory to `/etc/init.d` with the following command:

```shell
cd /etc/init.d
```

* Type `ls` to see all the scripts available in the `init.d` directory.
* Create a new script with this command:

```shell
nano S99MyScript.py
```

You can of course use any number, name or type of script (.py, .sh, etc.) and modify it to your needs. For this you can use the following script structure which already contains the start, stop and restart/reload methods (this is suitable for Bash scripts but can be for any script).

```bash
#!/bin/bash

case "$1" in 
   start)  
         Add your startup code here
         ;;  
   stop)  
         Add your stop code here
         ;;  
   restart|reload)  
         Add your restart / reload code here
         ;;  
   *)  
esac  

exit $?  
```

* Once your script is ready, save it and close it with `CTRL + X`.
* Make it executable with the command :

```shell
chmod +x S99MyScript.py
```

* You can now restart your Recalbox or run the script by yourself with `/etc/init.d/S99MyScript start`.
* You are ready to go!