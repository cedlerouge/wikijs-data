---
title: Modification
description: 
published: true
date: 2021-08-06T15:09:40.211Z
tags: modification
editor: markdown
dateCreated: 2021-08-06T08:09:36.476Z
---

This part will concern you if you have some specific modifications to make.

Here are the available tutorials:

[Add your own startup script](add-your-own-startup-script)
[Increase the power available on the USB ports](increase-available-power-on-usb)
[Modify startup configuration files](configtxt-configuration)