---
title: Backups and clone
description: 
published: true
date: 2021-08-06T15:01:28.692Z
tags: clone, backups
editor: markdown
dateCreated: 2021-08-06T08:10:23.236Z
---

If you have an SD card that is starting to have problems, you can find several ways to backup or clone your storage device here.

Here are the available tutorials:

[Clone your SD card](clone-sd-card)