---
title: Recalbox Manager - Web Interface
description: 
published: true
date: 2021-08-06T15:06:31.494Z
tags: web, manager
editor: markdown
dateCreated: 2021-08-06T09:03:17.643Z
---

The Recalbox Web Manager is a web interface to access your Recalbox via a smartphone, tablet or your PC.

## Interface

When you are on the main page, you can :

* Monitor your Recalbox (temperature, CPU cores usage, etc.).
* Edit the `recalbox.conf` file.
* Launch the virtual gamepad (useful to configure your Recalbox in 5 minutes for the first use) - via smartphone or tablet only.
* Check the Recalbox log files.
* Restart EmulationStation or the whole system in case of a crash.
* Take, view and save screenshots.

It allows you to drag and drop :

* Upload roms in `.zip` or extension format supported by emulators (not CD images).

By default, the manager starts automatically. If you wish, you can disable it in `recalbox.conf` by editing the line `system.manager.enabled` and changing the value to 0. But by doing this, in case of problems (Recalbox blocked), you will not be able to turn it off or restart it properly at all!

You can also disable it directly in EmulationStation (`START` > `ADVANCED SETTINGS` > `RECALBOX MANAGER`).

## To login

Simply type in your web browser's name:

## {.tabset}

## Windows

[http://recalbox/](http://recalbox/)

### macOS

[http://recalbox.local/](http://recalbox.local/)

### Linux

[http://recalbox.local/](http://recalbox.local/)

## If this does not work

You can use the IP address of your Recalbox:

* **192.168.1.x**

>Most routers are configured so that you should find your Recalbox between 192.168.1.2 and 192.168.1.254.
>
>To find out its IP address, from the EmulationStation menu, press `Start` > `Network Options` and look for the IP address next to `IP ADDRESS`.
{.is-info}

## Virtual Controller

To access it directly, just enter the IP address of your Recalbox with port 8080. For example: `http://192.168.0.X:8080` or `http://recalbox:8080`

See the [user manual](./../../../basic-usage/first-use-and-configuration#configuring-a-controller) of the virtual controller.

To report any bug related to the web manager, you can do it [here](https://gitlab.com/recalbox/recalbox-manager).