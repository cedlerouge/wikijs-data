---
title: Recalbox for your usb keyboard encoder
description: 
published: true
date: 2021-08-07T13:31:51.535Z
tags: usb, encoder, keyboard
editor: markdown
dateCreated: 2021-08-07T13:31:51.535Z
---

## Introduction

Xarcade2jstick has been patched to support keyboard encoders. You may need to reconfigure some keys on your encoder as unfortunately Xarcade devices from X-gaming have not been mapped like regular MAME controllers.

## Adding your keyboard encoder

For now, only the first generation IPAC has been included. If you have another keyboard encoder, follow these steps:

* Log in via [SSH](./../../system/access/root-access-terminal-cli).
* Find the device name of your encoder with the command `ls /dev/input/by-id`. Usually the result is followed by a kbd. For example: _usb-Ultimarc_IPAC_2_Ultimarc_IPAC_2_9-if01-event-kbd_

>A single encoder can have several possible names, so try them all e.g.:
>
>* usb-Cypress_I-PAC_Arcade_Control_Interface-event-kbd -> works
>* usb-Cypress_I-PAC_Arcade_Control_Interface-if01-event-kbd -> not working
{.is-info}

* Remount the [system partition](./../../system/access/remount-partition-with-write-access) as read/write.
* Create an empty file with the same name as your keyboard device found 2 steps above with this command:

```shell
touch /recalbox/share_init/system/configs/xarcade2jstick/devicename
```

In the previous example:

```shell
touch /recalbox/share_init/system/configs/xarcade2jstick/usb-Ultimarc_IPAC_2_Ultimarc_IPAC_2_9-if01-event-kbd
```

* Edit [recalbox.conf](./../../../basic-usage/getting-started/recalboxconf-file) and add this on a new line:

```ini
controllers.xarcade.enabled=1
```

* Reboot by typing `reboot`.

Recalbox is now configured for these devices.