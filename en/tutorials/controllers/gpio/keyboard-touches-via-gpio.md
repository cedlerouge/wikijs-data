---
title: Keyboard touches via GPIO
description: 
published: true
date: 2021-08-07T15:12:14.691Z
tags: gpio, keyboard, touches
editor: markdown
dateCreated: 2021-08-07T15:12:14.691Z
---

Recalbox includes the `retrogame` module under the name `recalbox-retrogame`. This module will allow to use GPIOs to reproduce the behavior of a real USB keyboard under Recalbox.

>It only works if you have the line `controllers.gpio.enabled=0` in recalbox.conf.
{.is-warning}

* If you want to integrate a Raspberry Pi into an old console and reuse the front panel buttons (e.g. `PAUSE/SELECT` from an Atari 7800) :
   * Wiring (on an arcade terminal) a button that would have the same behavior as the Escape key on a keyboard.
   * Or simply manage to do without a keyboard.

## Installation

### Wiring

You have to use push buttons (that go up after a press) and connect one terminal to a ground (GROUND) and the other terminal to a GPIO.

### Script

In order to launch the module automatically when Recalbox starts, it is important to add a script.

* Create a text file `S99retrogame` without file extension.
* Add the following two lines of code: 

```ini
/usr/bin/recalbox-retrogame
exit 0
```

* Give the corresponding rights:

```shell
chmod 775 /etc/init.d/S99retrogame
```

* Place it in the `/etc/init.d/` directory.
* Reboot and check via [SSH](./../../system/access/root-access-terminal-cli) if the module has started with the command :

```shell
ps aux | grep recalbox-retrogame
```

If the command returns an identifier for a line other than this one, you have succeeded.