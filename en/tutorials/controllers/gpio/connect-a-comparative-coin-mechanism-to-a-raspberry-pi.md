---
title: Connect a comparative coin mechanism to a Raspberry Pi
description: 
published: true
date: 2021-08-08T13:50:07.943Z
tags: coin, comparative, mechanism
editor: markdown
dateCreated: 2021-08-07T14:52:49.978Z
---

This tutorial takes elements from the manual provided with [this exclusive coiner](http://www.smallcab.net/monnayeur-comparateur-facade-silver-p-902.html).

## Principle of operation of the coin selector

The comparator coin changer works by comparing the inserted coin with the so-called "witness" coin permanently installed in the coin changer. The coin acceptor analyzes the electronic signature of the inserted coin and compares it with the electronic signature of the reference coin.

If the inserted coin has a signature close to the witness coin, the coin is accepted, otherwise the coin is rejected. When a coin is accepted, the coin acceptor generates an electric pulse to the Raspberry Pi.

This impulse is recognized by Recalbox (button `SELECT`) and a game credit is then granted to the player.

>It is possible to use metal tokens ("_token_") instead of coins.
{.is-info}

Very simple to make, based on four wires:

* Red: +12V to connect to [a 12V/1A power supply](https://www.amazon.fr/dp/B00B0T5D5W/).
* White: "Insert coin" to connect to the GPIO (or USB controller), corresponding to the `SELECT` button under Recalbox.

>Must be done through a "relay" !
{.is-danger}

* Black: ground to be connected to the 0V of the 12V/1A power supply.
* Grey : counter (not used here).

## Installation with a Raspberry Pi

As you can see, the use of this coin acceptor with a Raspberry Pi requires the use of an intermediate electronic component called "relay" or "keyboard hack" for the purists.

Here is a low cost solution to build this "keyboard hack" yourself.

## List of equipment to bring

* 1 good soldering iron, wire to make the tracks, tin and elbow grease
* 1 [12V relay](https://www.amazon.fr/dp/B013SPRXQC) (5 wires)
* 1 [prototype board](https://www.amazon.fr/dp/B01GUZ5L6G/)
* 2 [terminal blocks](https://www.amazon.fr/dp/B00I00OHHY)

## Wiring

>Don't hesitate to test your board BEFORE connecting it to your Raspberry and your coin acceptor !
{.is-warning}

The wiring of the coin acceptor with a Raspberry Pi requires the installation of this "keyboard hack" BETWEEN the coin acceptor AND the GPIO of your Raspberry Pi.

The 2 pins of the "Terminal Block 1" are to be connected:

* One on the "0V" wire of your 12V/1A power supply.
* The other one on the "white" wire of your coin acceptor.

The 2 pins of the " terminal block 2 " are to be connected :

* One on the "GND" pin of the Raspberry Pi GPIO connector.
* The other to a "GPIOxx" pin of the Raspberry Pi GPIO connector (`SELECT` button).

Refer to the documentation specific to your model to identify the Raspberry Pi GPIO connector and the position of the " GND " and " GPIOxx " pins within this connector.