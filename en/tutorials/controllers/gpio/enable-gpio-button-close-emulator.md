---
title: Enable GPIO button to close the emulator by pressing one button
description: 
published: true
date: 2021-08-08T13:50:27.901Z
tags: gpio, button, emulator, close
editor: markdown
dateCreated: 2021-08-07T14:44:31.325Z
---

This Python script for Raspberry Pi allows to use a button to close the emulator by pressing a single GPIO button. It is compatible with any other function or script previously assigned to buttons.

The default button selected is Player ONE Start; it can be changed during installation to any other as it does not remove any previous function.  
The emulator closes after holding the button down for at least 0.5 sec. The button activation delay can also be changed.

## Required

* A Recalbox on Raspberry Pi that uses GPIO commands (no need to have a free button).
* The Raspberry Pi must have Internet access: you can enable and configure the [wifi](./../../network/wifi/enable-wifi) and hostname, get [IP address](./../../network/ip/discover-recalbox-ip) of your Recalbox.
* A computer from which you can connect via [SSH](./../../system/access/root-access-terminal-cli) on Raspberry Pi.

## Installation

* Connect via [SSH](./../../system/access/root-access-terminal-cli).
* Copy and paste this command:

```shell
wget --quiet --show-progress -O /recalbox/share/system/exit-emu-1b https://gist.githubusercontent.com/DjLeChuck/446cd415575f03c927627e378979027d/raw/9ebe3a5e178ff047b536220afd513981095fb41d/exit-emu-1b-installer && chmod 755 /recalbox/share/system/exit-emu-1b && bash /recalbox/share/system/exit-emu-1b install
```

The installer will display a list of possible GPIO port mappings corresponding to the GPIO mapping for Recalbox, corresponding to the following image:

After selecting the button port you want the script to be assigned to, the installer will ask you for the delay to exit the emulator, you must type the desired delay in seconds (it is important to answer both questions, otherwise the installer will fail).

After a few seconds, if the result is a message on the screen saying "Start button", then everything went well and we now have the button script installed and working.

## Installed files

* [/recalbox/scripts/rpi-exit-emu-1b.py](https://gist.github.com/DjLeChuck/445ce3d37f41f12d5bf8cb9482db4027)
* [/etc/init.d/S98exit-emu-1b](https://gist.github.com/DjLeChuck/5f798b0d4af4071a92111bf61703aeb1)
* [/recalbox/share/system/exit-emu-1b](https://gist.github.com/DjLeChuck/446cd415575f03c927627e378979027d)

## Uninstall

* Connect via [SSH](./../../system/access/root-access-terminal-cli) and type the following command:

```shell
/recalbox/share/system/exit-emu-1b uninstall
```

>In the default RetroArch configuration, the script shuts down emulators by brute force using a _**killall**_ command which, depending on the kernel you are using, could cause some emulator configuration changes made during game play to be lost. For example, this happens with Mame2003.
>
>To avoid this, it has been implemented to close the emulator via a network command for RetroArch. Now RetroArch will close properly and you won't lose configuration changes in the emulators.
{.is-warning}

>This change only affects RetroArch, although it covers most emulators on the system, emulators like **FBA2x**, **Scummvm** or **mupen64plus** are still closed by brute force, until I find another way to apply it.
{.is-info}

## Tips and Tricks

* If at any time you need to assign the emulator close function to another button, just restart the installer with the command `/recalbox/share/system/exit-emu-1b install` and put in the new port number you want.
* If you change the configuration of your emulator and quit with the button assigned in the installation, these are lost. Make sure they are in the `/recalbox/share/system/configs/retroarch/retroarchcustom.cfg` file, or in the RetroArch custom configuration file where you use the `Network_cmd_enable` parameter set to ``true''.
* If for some reason the script stops working and the button loses its function of closing the emulator, you can get it working again with the command `/etc/init.d/S98exit-emu-1b restart`. You will have to enter it inside an [SSH session](./../../system/access/root-access-terminal-cli).