---
title: USB encoders
description: 
published: true
date: 2021-08-07T13:33:01.179Z
tags: usb, encoders
editor: markdown
dateCreated: 2021-08-07T13:23:02.309Z
---

Here you can find help to configure your USB encoders.

Here are the available tutorials:

[Recalbox for your usb keyboard encoder](recalbox-for-your-usb-keyboard-encoder)
[Xin-mo controllers configuration](xin-mo-controllers-configuration)