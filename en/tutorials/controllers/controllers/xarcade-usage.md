---
title: XArcade usage
description: 
published: true
date: 2021-08-07T14:36:42.828Z
tags: xarcade
editor: markdown
dateCreated: 2021-08-07T14:36:42.828Z
---

## Compatible devices and requirements

The following sticks are supported:

* X-Arcade Model Tankstick
* X-Arcade Dual Stick

## Using the XArcade stick on Raspberry Pi 4

On Raspberry Pi 4, the XArcade stick binary is not correct. In order to use the XArcade stick on Raspberry Pi 4, you need to execute some commands via [SSH](./../../system/access/root-access-terminal-cli) and mount the [system partition](./../../system/access/remount-partition-with-write-access) in read/write mode. The commands are as follows:

```text
cp /usr/bin/xarcade2jstick /usr/bin/xarcade2jstick-old
cp xarcade2jstick-rpi4 /usr/bin/xarcade2jstick
chmod 755 /usr/bin/xarcade2jstick
```