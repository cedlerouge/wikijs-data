---
title: Play with your smartphone using the virtual gamepad
description: 
published: true
date: 2021-09-04T22:47:38.809Z
tags: virtual, gamepad, smartphone
editor: markdown
dateCreated: 2021-08-07T14:32:10.848Z
---

[Miroof's project - Virtual Gamepads](https://github.com/miroof/node-virtual-gamepads) brings a new feature to Recalbox: play games with your smartphone as a controller!

You can add up to 4 controllers at a time.

* When you connect your Recalbox to the network, you just have to get your [IP address](./../../network/ip/discover-recalbox-ip) and then connect your smartphone on the same wifi network as your Recalbox and open your web browser on the IP address found with port 8080 (you can check [this tutorial](./../../system/access/recalbox-manager-web-interface) to know how to do it).
* A new controller is then added in Recalbox.

You can then either start a game or go to the controller settings to assign this new controller to a player.

![Virtual Controller](/tutorials/controllers/controllers/virtualgamepad.png)

For more information about the feature, you can check the source [here](https://github.com/jehervy/node-virtual-gamepads#features).