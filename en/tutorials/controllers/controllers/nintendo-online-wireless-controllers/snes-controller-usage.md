---
title: SNES controller usage
description: How to synchronize Nintendo Switch Online controllers - SNES into Recalbox
published: true
date: 2021-08-10T23:14:55.209Z
tags: switch, controller, nintendo, snes, 7.2+
editor: markdown
dateCreated: 2021-08-07T13:44:13.495Z
---

## Synchronization

On each controller, there is a small button that allows you to turn it on and off but this button also allows you to synchronize with your box.

![Sync button](/tutorials/controllers/controllers/switch-joycons/snesconfig1.jpg)

In Recalbox, in your available system lists, press **Start** on your usual controller and go to **CONTROLLER SETTINGS**.

![](/tutorials/controllers/controllers/switch-joycons/snesconfig2-en.png){.full-width}

In this menu, select **PAIR A BLUETOOTH CONTROLLER**.

![](/tutorials/controllers/controllers/switch-joycons/snesconfig3-en.png){.full-width}

Once the search for Bluetooth devices is launched, press the sync button of the controller to associate. It should start to have the 4 lights next to it going back and forth from left to right and vice versa.

![](/tutorials/controllers/controllers/switch-joycons/snesconfig4-en.png){.full-width}

Once the search for Bluetooth devices is complete, you should see your controller in the list.

![](/tutorials/controllers/controllers/switch-joycons/snesconfig5-en.png){.full-width}

Select the controller and validate. Your controller should now be recognized and still have the 4 lights going back and forth.

Once the association is complete, the controller will appear in the list of controllers.

![](/tutorials/controllers/controllers/switch-joycons/snesconfig6-en.png){.full-width}

Now you can enjoy playing with your SNES controller!

## SNES controller setup

Once associated, the controller will have an automatic configuration detected with the **HOTKEY** button on **SELECT**.