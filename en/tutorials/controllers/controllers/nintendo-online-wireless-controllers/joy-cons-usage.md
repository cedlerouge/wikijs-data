---
title: Joy-Cons usage
description: How to synchronize and use Joy-Cons into Recalbox
published: true
date: 2021-08-10T19:22:33.343Z
tags: joy-con, 7.2+
editor: markdown
dateCreated: 2021-08-07T13:46:54.127Z
---

We will see how to synchronize a pair of Joy-Cons and how to use them separately or together (combined).

## Two Joy-Cons as 2 separate controllers

On each Joy-Con, there is a small button to turn on and off the Joy-Con but this button also allows you to synchronize the Joy-Con with your box.

![Sync button on each Joy-Con](/tutorials/controllers/controllers/switch-joycons/joyconconfig1.jpg)

In Recalbox, in your available system lists, press **Start** on your usual controller and go to **CONTROLLER SETTINGS**.

![](/tutorials/controllers/controllers/switch-joycons/joyconconfig2-en.png){.full-width}

In this menu, select **PAIR A BLUETOOTH CONTROLLER**.

![](/tutorials/controllers/controllers/switch-joycons/joyconconfig3-en.png){.full-width}

Once the search for Bluetooth devices is launched, press the sync button of the Joy-Con to associate as shown on the first image. You should have the 4 side lights going back and forth.

![](/tutorials/controllers/controllers/switch-joycons/joyconconfig4-en.png){.full-width}

Once the search for Bluetooth devices is complete, you should see your Joy-Con in the list.

![](/tutorials/controllers/controllers/switch-joycons/joyconconfig5-en.png){.full-width}

Select the Joy-Con and validate. Once you get the message **ASSOCIATED CONTROLLER**, you must press the SL and SR buttons. Your Joy-Con should have 1 side light on.

Do the same with the second Joy-Con by repeating the same steps seen above.

![](/tutorials/controllers/controllers/switch-joycons/joyconconfig6-en.png){.full-width}

Validate the second Joy-Con to finish its association and press SL+SR as the last step. Once you have associated your 2 Joy-Cons, they will be visible in the list of controllers.

![](/tutorials/controllers/controllers/switch-joycons/joyconconfig7-en.png){.full-width}

## Two Joy-Cons in one controller (combined mode)

If you want to use the 2 Joy-Cons as one controller, you have to associate the desired Joy-Cons again and when the Joy-Con is associated, don't press SL and SR but leave the Joy-Con with the 4 side lights flashing and do the same with the right Joy-Con. Once the 2 Joy-Cons have each their 4 side lights blinking, just press the ![](/tutorials/controllers/controllers/switch-joycons/switch_lb.png) and ![](/tutorials/controllers/controllers/switch-joycons/switch_rb.png) buttons of the right Joy-Con to have the 2 Joy-Cons associated in one controller!

![](/tutorials/controllers/controllers/switch-joycons/joyconconfig8-en.png){.full-width}

The joys of playing with Joy-Cons are yours!