---
title: PS3 controllers drivers
description: 
published: true
date: 2021-08-07T14:35:27.236Z
tags: controllers, ps3, drivers
editor: markdown
dateCreated: 2021-08-07T14:35:27.236Z
---

There are different versions of PS3 controllers _sixaxis_ and _dualshock3_ on the market.

Recalbox supports by default the official controllers. But if you have a GASIA or Shanwan clone, you have to modify the driver in [recalbox.conf](./../../../basic-usage/getting-started/recalboxconf-file) by modifying the following line with the content of the table below:

```ini
controllers.ps3.driver=official
```

| Model | Driver |
| :--- | :--- |
| Sixaxis | `official` |
| Dualshock3 | `official` |
| US Sixaxis | `shanwan` ?? |
| Shanwan | `shanwan` |
| Gasia | `gasia` |

To determine which type of clone you have (GASIA or SHANWAN), you can :

* Connect your controller to USB
* Then use the following command:

```shell
dmesg
```

You should then see the type of controller you have.