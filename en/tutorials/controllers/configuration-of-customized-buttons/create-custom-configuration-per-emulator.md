---
title: Create a custom configuration per emulator
description: 
published: true
date: 2021-08-08T13:49:36.614Z
tags: custom, configuration, emulator
editor: markdown
dateCreated: 2021-08-07T13:39:14.792Z
---

You can create your own custom configuration for each emulator on Recalbox.

## RetroArch

The RetroArch configuration can be created by hand by pressing your own configuration on the standard [retroarch.cfg](https://github.com/libretro/RetroArch/blob/master/retroarch.cfg) or by editing it in the RetroArch menu. You will then need to create a new configuration and set up the new `recalbox.conf` config file to be loaded by your emulator.

### Using the RetroArch menu

Let's start with the example of creating a specific config file for the Neo-geo.

* Launch a game of the emulator you want to customize (here neogeo).
* Enter the RetroArch menu (`Hotkey + B`).
* Change all the settings as you wish.
* Go back to the first entry in the RetroArch menu (`Main Menu`) and go to `Config Files` > `Save New Configuration` (the name of the config file should look like `fbneo_libretro.cfg`, with the name of the core you are using).
* When connecting via [SSH](./../../system/access/root-access-terminal-cli), change the name of the configuration file to something easier to remember, e.g. :

```shell
mv /recalbox/share/system/configs/retroarch/fba_libretro.cfg /recalbox/share/system/configs/retroarch/inputs/neogeo_custom.cfg
```

* Add the following line to the `recalbox.conf` file:

```ini
neogeo.configfile=/recalbox/share/system/configs/retroarch/inputs/neogeo_custom.cfg
```

### Inputs

#### Manual modification

You can add system and/or game specific configuration files. There is of course a priority if the same parameter appears in each configuration file. Thus, Game > System > RetroArch, which means that a setting that exists in a game .cfg file will be the one used on a system or the default RetroArch file.

Here are the files you can modify (or create if they don't exist):

* `~/configs/retroarch/<system>.cfg` where `<system>` is the system name as it appears in the `<roms>` folder. For example: `~/configs/retroarch/snes.cfg`.
* `~/configs/retroarch/<system>/<romname>.cfg` where `<system>` is the system name as it appears in the `roms` folder, and `<romname>` is the exact name of the rom with the file extension. For example: `~/configs/retroarch/snes/mario.zip.cfg`

## piFBA

>This part is to be written. If you know how piFBA works, you can add your content here directly.