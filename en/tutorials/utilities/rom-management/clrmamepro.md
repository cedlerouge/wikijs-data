---
title: Clrmamepro
description: 
published: true
date: 2021-10-03T22:24:29.264Z
tags: clrmamepro
editor: markdown
dateCreated: 2021-08-07T00:14:27.599Z
---

In this tutorial, we will teach you in a few clicks how to use Clrmamepro to extract (rebuild) or to scan a romset arcade. 

## I - Before starting

### 1 - Necessary elements

First you need :

* The Clrmamepro software [here](https://mamedev.emulab.it/clrmamepro/).
* A [romset MAME](./../../../advanced-usage/arcade-in-recalbox) which corresponds to your emulator.
* The samples (optional depending on the romset). 
* The [CHDs](./../../../advanced-usage/arcade-in-recalbox) (optional depending on the romset).
* A .DAT or .XML file in the [share] folder (./../../basic-usage/file-management)/bios of your Recalbox.

### 2- Definitions

* **Set** : Compressed file (.zip/.7z/.rar) of games, drivers and bios.
* **Rom** : File contained in a set.

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro1.png)

* What is a dat file? dat files are text files with information about the games emulated with a specific version of the MAME emulator.  In these dat files, we find information for particular sets like: rom name, year, manufacturer, merge information, rom parent or clone, size, CRC for each rom. CRC is an algorithm to check the integrity of a file, clrmamepro uses it to check your rom-sets.
* **ClrmamePro** : It's a software that allows you to check and rebuild your arcade romsets according to an information file in xml or dat format.

### 3 - Installation

#### Windows

Download ClrmamePro for [Windows](http://mamedev.emulab.it/clrmamepro/#downloads)

#### macOS

Download ClrmamePro for [macOS](http://www.emulab.it/)

#### Linux

Download ClrmamePro for Linux with [Wine](https://doc.ubuntu-fr.org/wine).

>The zip file is a portable version.
{.is-info}

* Install it.
* Get the dat file of the desired arcade emulator.
* Start Clrmamepro:
  * Run clrmamepro via a right click on the icon.
  * Then "Run as administrator_".

>**Tip under Windows** :
>
>* Go to the executable cmpro64.exe or cmpro32.exe
>* Then right click
>* Choose _Properties_
>* Go to the _Compatibility_ tab
>* Then check the box "_Run as administrator"_.
>* Validate with Ok.
>
>The program will always run as administrator from now on.
{.is-success}

## II - Use

### 1 - Description of the functions

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro2.png)

### 2 - Rebuilder

* Start clrmamepro.

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro3.png)

* To select your .dat file click on "Add DatFile".

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro4.png)

* Select your file then "open".

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro5.png)

* Once selected, you can indicate a folder or a subfolder. By selecting "ok" clrmamepro will create a folder "NEW DATFILES" automatically.

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro6.png)

* Select your .dat file then "Load/Update" to load it

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro7.png)

* Clrmamepro offers you to create or update the configuration. We advise you to click on "Default".

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro8.png)

* Clrmamepro will read and load your file and you should arrive on the general menu.
  * To extract a romset click on "Rebuild".

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro9.png)

* In the "source" you must indicate the folder of your MAME romset.

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro10.png)

* Select the folder where your romset is located then "OK".

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro11.png)

* In the "destination" you must indicate a folder to collect the extraction romset.

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro12.png)

* Select the destination folder then "OK".

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro13.png)

* **In the Rebuilder menu, you can set** :

  * 1 - [**The type of romset**](./../../../advanced-usage/arcade-in-recalbox) you want to create **(Merge Options)** :

    * Non-merged
    * Split
    * Merged

  * **2 - The Options** : (We recommend the default settings)

    * Compression option (.zip/.7z/.rar).
    * Option of recompressions. If you want to recompress the files or not.
    * "Show Statistics" to see the results at the end of the Rebuild.
    * "Remove Matched Sourcefiles". If this option is activated, clrmamepro will extract and delete the files corresponding to your .DAT file from your Romset MAME.
    * "Systems" and "Advanced". Don't venture here for the moment.

* Finally launch the extraction by clicking on "Rebuild".

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro14.png)

* Let clrmamepro run until the end.

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro15.png)

* And that's it! We advise you to scan the destination folder to make sure it is perfect.

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro16.png)

### 3 - Scanner

* Start clrmamepro.

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro3.png)

* To select your .dat file, click on "Add DatFile".

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro4.png)

* Select your file then "open". 

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro5.png)

* Once selected, you can indicate a folder or a subfolder. By selecting "ok", clrmamepro will create a folder "NEW DATFILES" automatically.

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro6.png)

* Select your .dat file then "Load/Update" to load it.

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro7.png)

* Clrmamepro offers you to create or update the configuration. We advise you to click on "Default".

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro8.png)

* Clrmamepro will read and load your file and you should arrive on the general menu. To indicate where your romset is located we will click on "Settings".

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro17.png)

* The path to your romset in "ROM-Paths" by clicking on "Add".

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro18.png)

* Select the folder where is your romset that you want to scan then "OK". 

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro19.png)

* For Mame, if the romset should contain samples, you have to give the path of the samples in the same way with "Sample-Paths".

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro20.png)

* Save the paths by clicking on "Save As Def. 

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro21.png)

* Clrmamepro confirms that it is taken into account then click on "OK".

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro22.png)

* You can close this window.

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro23.png)

* Once back on the main menu, click on "Scanner".

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro24.png)

* In the **Scanner** menu you have to set up : 

**You want to scan **:

* Sets (bios/games/drivers),
* Roms (chips that make up each set)
* Samples (additional drivers for some Mame games)
* [CHDs](./../../../advanced-usage/arcade-in-recalbox) (That you added to your Romset)

[**The type of romset**](./../../../advanced-usage/arcade-in-recalbox) you have **(You prefer)** :

* Non-merged
* Split
* Merged

**Messages during and after the scan (Prompts)** :

* Ask Before Fixing
* Add/Show Statistics

**Check/Fix** : Clrmamepro will check what is in the left column "check". If you want clrmamepro to make corrections you can check the boxes in the right column "Fix". Be careful with the "fix" option clrmamepro will remove the folders and files that do not match your DAT and place them in its "backup".

**Options** : Do not venture here for the moment 

Finally launch the scan by clicking on "New Scan".

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro25.png)

* In this window you will see the different problems of your set. (Example: missing rom) You can export a list of what you are missing in the "Miss List" tab to complete.

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro26.png)

* Finally, you will have the results of the scanner in the "statistics" window. The most important part is the "Missing" part below, so nothing is missing.

![](/tutorials/utilities/rom-management/clrmamepro/clrmamepro27.png)