---
title: md5sum
description: 
published: true
date: 2021-08-07T00:24:57.431Z
tags: md5sum
editor: markdown
dateCreated: 2021-08-07T00:24:57.431Z
---

To check the signature of a file, in other words its MD5 fingerprint (**checksum**).

## Online:

[HTML5 File Hash Online Calculator](https://md5file.com/calculator)

## Local :

## {.tabset}
## Windows
Download [winmd5free](http://www.winmd5.com/download/winmd5free.zip). Install it and then click the `Browse` button to select your file. Enter the checksum recommended on this page ([See Adding Bios](./../../../basic-usage/file-management#iii-add-content)) by putting it in the `Original file MD5` box and then click `Verify`.

EXAMPLE: **gba_bios.bin**

#### **Alternative method 1**:

Since Powershell v4.0 (Windows 8.1 and above), the `get-filehash` command allows you to natively check the checksum of a file.

1. Run powershell.exe
2. Run the following command:

```shell
Get-FileHash "file to verify" -Algorithm MD5
```

>To test all the files in a folder (recursively), run the following command after moving to the directory:
>
>```shell
>Get-ChildItem -recurse | % {Get-FileHash $_.Fullname -Algorithm MD5} | select Path,Hash | ft -AutoSize
>```
{.is-info}

#### **Alternative method 2**:

You can also download this script: [HackCheck Shell Extension](http://code.kliu.org/hashcheck/) Which allows you to add an extra tab in the properties of a file for Windows 10.

### macOS / Linux

Use the command `md5sum`.  
In terminal, to check the checksum of bios files, use the command shasum followed by the file name.