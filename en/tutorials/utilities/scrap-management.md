---
title: Scraps management
description: 
published: true
date: 2021-08-07T00:18:54.985Z
tags: scrap, management
editor: markdown
dateCreated: 2021-08-06T23:38:05.759Z
---

The scrape consists in adding to your games a multitude of associated information such as a game summary, screenshot, video, game manual, etc.

List of available tutorials:

[Scraping tools](scraping-tools)
[Skraper - scrap your roms](scrape-your-roms-with-skraper)