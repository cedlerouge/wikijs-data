---
title: Balena Etcher
description: 
published: true
date: 2021-08-06T23:59:37.785Z
tags: balena, etcher
editor: markdown
dateCreated: 2021-08-06T23:59:37.785Z
---

> Use Balena Etcher only as a last resort, it flashes Recalbox incorrectly and can make your installation non-operational!
>
> It is strongly recommended to use [Raspberry Pi Imager](./raspberry-pi-imager) first to flash Recalbox, even if you are installing Recalbox on another board than a Raspberry Pi.
{.is-danger}

[BalenaEtcher](https://www.balena.io/etcher/) is a very simple program to use. You can flash the image file to the desired media (for example, a microSD card for a Raspberry Pi). The operation of flashing an image takes only a few seconds of manipulation.

* Choose the ISO image to flash:

  * Click on `Select Image` and look for the file to flash. The name of the image appears under the first icon and a small change allows you to modify this choice.

* Connect a storage media:

	* The second external storage icon lights up.
  * Select a storage media.

Etcher automatically validates what it detects as a USB device. It is possible to change the destination by pressing the small `Change` button. The program indicates the capacity of the device and warns if it is too small to accept the image.

>For installation on a hard disk:
>
>* Go to `Settings` (Top right hand scroll wheel).
>* Disable the "unsafe mode" function.
>* Go back to the home menu.
>* Select a hard drive.
{.is-warning}

![Etcher](/tutorials/utilities/image-write/balena-etcher/balena1.jpg)

* Press the `Start or Flash` button to start the procedure. Etcher takes care of everything, formatting, preparing partitions, copying files, etc.

![](/tutorials/utilities/image-write/balena-etcher/balena2.png)

* The program indicates the work in progress, it also specifies the time needed for the operation and the speed of data transfer.

![](/tutorials/utilities/image-write/balena-etcher/balena3.png)

* It then validates the image.

![](/tutorials/utilities/image-write/balena-etcher/balena4.png)

* And finally reports the good - or bad - operation. Gives you a key to check the integrity of the archive and offers you to restart the operation either with the same image in one click or with a different image.

![](/tutorials/utilities/image-write/balena-etcher/balena5.png)

* You can eject the storage medium.