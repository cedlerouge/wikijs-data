---
title: Raspberry Pi Imager
description: 
published: true
date: 2021-09-04T21:24:36.505Z
tags: pi, raspberry, imager
editor: markdown
dateCreated: 2021-06-29T13:24:04.699Z
---

## Write an image with Raspberry Pi Imager

[Raspberry Pi Imager](https://www.raspberrypi.org/software/) (easy to use), allows you to flash an image file on the desired support, a microSD card for example. Write an image is quick and easy.

* Click on [Raspberry Pi Imager](https://www.raspberrypi.org/software/) to go to the download link and install it on your computer:

![](/tutorials/utilities/image-write/rpi-imager/rpi-imager1.png)

* Once installed, the utility looks like this:

![](/tutorials/utilities/image-write/rpi-imager/rpi-imager2.png)

* Click on `Choose OS`, select the file to be flashed, then click on `Choose Storage` to start the procedure.

>It is highly recommended that you plug in _**only the storage media you wish to flash**_ to avoid erasing your data by mistake.
{.is-warning}

![](/tutorials/utilities/image-write/rpi-imager/rpi-imager3.png)

* You can then click on `Write`. Raspberry Pi Imager takes care of everything, formatting, preparing partitions, copying files, etc...

![](/tutorials/utilities/image-write/rpi-imager/rpi-imager4.png)

* The program shows the work in progress.

![](/tutorials/utilities/image-write/rpi-imager/rpi-imager5.png)

* It then checks the image.

![](/tutorials/utilities/image-write/rpi-imager/rpi-imager6.png)

* And finally reports the progress of the operation.

![](/tutorials/utilities/image-write/rpi-imager/rpi-imager7.png)

As indicated, you can eject the storage medium, all you have to do is insert the microSD into your console and play!
