---
title: Sort and update your roms
description: 
published: true
date: 2021-08-08T13:51:21.022Z
tags: update, roms, sort
editor: markdown
dateCreated: 2021-08-07T15:41:33.924Z
---

To sort and update your games, you would need a software, a dat file and for some consoles a header file.

>This step is not to be neglected because the roms have updates by some groups that allow to improve the roms by removing bugs present on the rom or to better run it on emulator.
>
>These updates are not frequent but are nevertheless essential for their good functioning on emulator and is essential in Netplay. 
{.is-warning}

## Software

You have several software to sort and update your roms.

* [Clrmamepro](https://mamedev.emulab.it/clrmamepro/)
* [Romulus](https://romulus.cc/)
* [Universal Rom Cleaner](https://github.com/Universal-Rom-Tools/Universal-ROM-Cleaner/releases)

>For beginners, it is recommended to use Romulus which is easier to use.
{.is-info}

## Dat files

You can download these files on these sites:

* **Atomiswave** : ❓
* **FBNeo** : ❓
* **MAME**: ❓
* **Naomi**: ❓
* **Naomi GD-Rom**: ❓
* **Neo Geo**: ❓
* **No-Intro**: [https://datomatic.no-intro.org/?page=download](https://datomatic.no-intro.org/?page=download)
* **Redump**: [https://redump.org/downloads/](https://redump.org/downloads/)
* **TOSEC**: [https://www.tosecdev.org/](https://www.tosecdev.org/)
* **Trurip**: [https://database.trurip.org/](https://database.trurip.org/)
* **GoodSets/Goodtools**: [https://cowering.blogspot.com/](https://cowering.blogspot.com/)

Be sure to take the dat file corresponding to the rom group you have chosen !

## Header files

For some consoles, you will need a header file in addition to the dat file.

Here is the list of consoles concerned:

* Atari - 7800
* Atari - Lynx
* Nintendo - Family Computer Disk System
* Nintendo - Nintendo Entertainment System

## Sorting preferences

### National or international?

* **National** : if you want to play only with people from the same country as you : keep only the roms from your country then prefer the USA and Japan roms for the missing ones.
* **International** : if you want to play with people from other countries : keep one rom from each country.

Here is an international example:

* Super Street Fighter II (Europe)
* Super Street Fighter II - The New Challengers (Japan)
* Street Fighter II Plus - Champion Edition (Japan, Korea, Asia)

### Retroachievements

Some Retroachievements trophies need the USA rom to validate the retroachievements and some games exist only in Japanese version.  

You can find more information [here](./../../../../basic-usage/features/retroachievements).

### Games and regions

Depending on the region, the games can have more or less obvious differences. Moreover, some games exist only in Japanese version.

Here are some examples of these differences:

* Different boss.
* Different blood color (red on a US version which becomes green for a European version).
* Fps more or less high.
* Frequency (MHz) more or less high.
* Different number of players.

### Codes and tags in the roms

To know what the codes and tags in the roms correspond to (only for the cartridge and floppy roms), you will find more information [here](./../tags-used-in-rom-names).