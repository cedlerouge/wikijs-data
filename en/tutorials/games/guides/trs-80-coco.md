---
title: TRS-80 Color Computer
description: 
published: true
date: 2021-09-09T17:39:12.500Z
tags: 7.3+, color, trs-80, computer, coco
editor: markdown
dateCreated: 2021-09-09T17:30:47.125Z
---

Here you will find some information to make the most of the TRS-80 Color Computer.

Here are the available tutorials:

[FAQ](faq)