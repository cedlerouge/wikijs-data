---
title: Memory cards management
description: 
published: true
date: 2021-08-08T14:33:28.785Z
tags: card, memory, management, ps1
editor: markdown
dateCreated: 2021-08-08T14:33:28.785Z
---

## Activate the second memory card

### Interest

This allows you to have a second memory card in game that is common to all games (so unique).

### Steps

* Launch any PSX game.
* Go to the RetroArch menu with `Hotkey + B` and go to `Options`.

![](/tutorials/games/guides/playstation-1/memory-card-management/psxbios1-en.png){.full-width}

* Enable the `Enable second memory card` option.

![](/tutorials/games/guides/playstation-1/memory-card-management/psxbios2-en.png){.full-width}

* Exit the menu.

>This manipulation is to be done on each game for which you wish to have access to this memory card. It is recommended to quit the game and then restart it.
{.is-warning}

Now, in game, you will see a usable memory card in slot 2. In the `/share/saves/psx/` folder, you will see a file named `pcsx-card2.mcd` which is the memory card file.

## Go to the memory card editor

### Interesting

This allows you to manage your game saves (copy or delete) if you need to.

### Steps

* Start a game.
* Go to the RetroArch menu with `Hotkey + B` and go to `Options`.

![](/tutorials/games/guides/playstation-1/memory-card-management/psxbios1-en.png){.full-width}

* Enable the `Show Bios Bootlogo` option.

![](/tutorials/games/guides/playstation-1/memory-card-management/psxbios3-en.png){.full-width}

* Return to the game and exit the game completely.
* Restart the game and during the boot sequence, eject the game with `Hotkey + Left Stick up`.
* You should arrive in the console bios to manage your saves.

![](/tutorials/games/guides/playstation-1/memory-card-management/psxbios4.png){.full-width}

And if you have enabled the second memory card as seen above, you should be able to copy your data between cards.

![](/tutorials/games/guides/playstation-1/memory-card-management/psxbios5.png){.full-width}