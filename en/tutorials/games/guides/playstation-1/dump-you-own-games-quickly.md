---
title: Dump your own games quickly
description: 
published: true
date: 2021-08-08T14:23:33.312Z
tags: games, ps1, dump
editor: markdown
dateCreated: 2021-08-08T14:23:33.312Z
---

You have your own physical games and want to create a copy (dump) to play them on Recalbox? This is something possible.

The following tutorial allows you to create a backup copy of your games in `.BIN` + `.CUE` format.

## Prerequisites

You will need a few things before you can start:

* The software [ImgBurn](https://www.imgburn.com/)
* A backup location.

## Procedure

* Put your game in your CD player.
* Open ImgBurn.
* Choose `Create an image from a disc` and point to the CD drive with your original disc.
* Then choose where to put the `.BIN' + `.CUE' files on your SSD or hard drive or SD card.

>The default file extension is `.ISO`, be sure to change it to `.BIN`.
{.is-warning}

* For the copy speed, if you have the possibility to adjust it, try to stay in 2X (above, it is possible that the drive reads some data too fast or does not correct in time if it is necessary to read a sector of the disk again).

## Notes

* The discs are more than 15-20 years old, reading errors are quite possible (a CD is not eternal, it can be scratched, a little more opaque in time in some places, or various traces by the user or his handling in time - fingerprints, dust, others).
* Afterwards, with the European games, especially the most popular ones and from 1997, Sony will implement successive protections (anti-copy, anti-reading on ISO, anti-chip etc.), normally it takes specialized equipment to extract the data correctly and the process can be long for individuals or non-specialized people.
* At Redump, a possible solution has been provided with SBI (SubChannel Information) files, which are in fact descriptions of the hidden tracks and protections on the disks. Offering them remains legal, because they do not contain direct code from the CD, but only separate information (like a patch).
* It is possible to get these SBI files on the files of each disc described at Redump, in the SBI section.
* Here is the catalog of all the games protected on SONY PlayStation http://redump.org/discs/libcrypt/2/

## Credits

* Zet_sensei sur [Discord](https://discord.com/channels/438014472049917953/439141063425392640/848505646391361577).