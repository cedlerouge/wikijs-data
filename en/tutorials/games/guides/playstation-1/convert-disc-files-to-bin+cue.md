---
title: Convert disc images to .BIN + .CUE
description: 
published: true
date: 2021-08-08T14:09:06.094Z
tags: ps1, convert, bin, cue
editor: markdown
dateCreated: 2021-08-08T14:09:06.094Z
---

## Information

SegaCD or PSX games can use different formats:

* `.ISO` + `.CUE` accompanied by track files in `.WAV`, `.BIN` format
* `.ISO` + `.BIN`
* `.BIN` + `.CUE`
* `.CCD` + `.IMG` + `.SUB`
* `.BIN` only

**Example:**

```text
Wonder Dog (1993)(Sega)(PAL)(Track 01 of 21)[!].iso
Wonder Dog (1993)(Sega)(PAL)(Track 02 of 21)[!].wav
Wonder Dog (1993)(Sega)(PAL)(Track 03 of 21)[!].wav
Wonder Dog (1993)(Sega)(PAL)(Track 04 of 21)[!].wav
Wonder Dog (1993)(Sega)(PAL)(Track 05 of 21)[!].wav
Wonder Dog (1993)(Sega)(PAL)(Track 06 of 21)[!].wav
Wonder Dog (1993)(Sega)(PAL)(Track 07 of 21)[!].wav
Wonder Dog (1993)(Sega)(PAL)(Track 08 of 21)[!].wav
Wonder Dog (1993)(Sega)(PAL)(Track 09 of 21)[!].wav
Wonder Dog (1993)(Sega)(PAL)(Track 10 of 21)[!].wav
Wonder Dog (1993)(Sega)(PAL)(Track 11 of 21)[!].wav
Wonder Dog (1993)(Sega)(PAL)(Track 12 of 21)[!].wav
Wonder Dog (1993)(Sega)(PAL)(Track 13 of 21)[!].wav
Wonder Dog (1993)(Sega)(PAL)(Track 14 of 21)[!].wav
Wonder Dog (1993)(Sega)(PAL)(Track 15 of 21)[!].wav
Wonder Dog (1993)(Sega)(PAL)(Track 16 of 21)[!].wav
Wonder Dog (1993)(Sega)(PAL)(Track 17 of 21)[!].wav
Wonder Dog (1993)(Sega)(PAL)(Track 18 of 21)[!].wav
Wonder Dog (1993)(Sega)(PAL)(Track 19 of 21)[!].wav
Wonder Dog (1993)(Sega)(PAL)(Track 20 of 21)[!].wav
Wonder Dog (1993)(Sega)(PAL)(Track 21 of 21)[!].wav
Wonder Dog (1993)(Sega)(PAL)[!].cue   

Rockman 8 - Metal Heroes (Japan) (Track 1).bin   
Rockman 8 - Metal Heroes (Japan) (Track 2).bin   
Rockman 8 - Metal Heroes (Japan) (Track 3).bin   
Rockman 8 - Metal Heroes (Japan) (Track 4).bin   
Rockman 8 - Metal Heroes (Japan).cue
```

## Converting a disk image to `.BIN` + `.CUE`

Here is a technique to convert `.ISO` + `.CUE` with `.WAV` files for those who don't want to create folders to contain all the files of a game.

>This technique is valid for PSX, SEGACD.
{.is-info}

* Download [Daemons Tools Lite](https://www.daemon-tools.cc/fra/products/dtLite).
* Install the software.
* Choose the disk images you want to convert and click the `+` sign.
  * Select the `*.CUE` file and press `Enter`. OR
  * Right click and choose `Mount` from the context menu to create the virtual drive.
* Download [ImgBurn](http://imgburn.com/).
* Install the software.
* Open the program.
* Click on the "Create image from disk" button.
* Choose the virtual drive as the source.
* Choose a destination folder for the BIN and CUE files.
* Click the `Play` button and wait.

Once the conversion is complete, you can unmount the virtual drive (right click on the icon in **Daemon Tools** and click `Unmount`).

## Create a missing `.CUE` file for your single `.BIN` file

Example with the `r-type.bin` file.

* Open [Notepad++](https://notepad-plus-plus.org/downloads/) to a new empty file.
* You will need to write 3 lines to this new file:
  * The first line must contain the **BINARY**
  * The second line must contain the tracks always in **MODE2/2352**.
  * The third line should contain the **01** index
* You get something like this which should work:

```text
FILE "r-type.bin" BINARY
TRACK 01 MODE2/2352
INDEX 01 00:00:00
```

* Save the file with the extension `.CUE`: `r-type.cue`.
* Now you just have to transfer your `.bin` and `.cue` file.

>If you have several games where the `.CUE` file is missing, you can go to the [Redump site] (http://redump.org/downloads/) to get all the .CUE files by system.
{.is-info}