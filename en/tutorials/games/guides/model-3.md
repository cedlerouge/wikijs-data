---
title: Model 3
description: 
published: true
date: 2021-08-08T09:03:36.574Z
tags: sega, model3
editor: markdown
dateCreated: 2021-08-07T23:49:46.878Z
---

A few tutorials for the Model 3 will have their place here.

Here are the available tutorials:

[Change country and language in-game](change-country-and-language-ingame)