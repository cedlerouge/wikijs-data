---
title: Games compatibility
description: 
published: true
date: 2021-08-08T06:09:58.374Z
tags: games, 32x compatibility
editor: markdown
dateCreated: 2021-08-08T06:09:58.374Z
---

## Introduction

Some 32X games may have emulation issues which are listed below.

## Compatibility Chart

| Game | Known Issues |
| :--- | :--- |
| Brutal Unleashed – Above the Claw | Softlock after the first fight. |
| FIFA Soccer ’96 | Text of the main menu with glitches. |
| Knuckles’ Chaotix | Graphics with glitches on the player selection screen. |
| NBA Jam Tournament Edition | Framerate issues. |
| NFL Quarterback Club | Some menu graphics are missing. |
| Virtua Racing Deluxe | Flashing line while the SEGA logo is displayed. |
| World Series Baseball Starring Deion Sanders | Crash when a game starts. |
| WWF Raw | Some graphics are missing. |