---
title: Multidisc games
description: 
published: true
date: 2021-08-08T07:06:09.084Z
tags: dreamcast, multidisc
editor: markdown
dateCreated: 2021-08-08T07:06:09.084Z
---

On **Dreamcast**, there are **some Multidisc games**.

**Example** :

* _Resident Evil Code Veronica_
* _Skies Of Arcadia_
* _Grandia 2_
* _Etc..._

**Here is how to change discs:**

At the end of disk 1 of your game, the game proposes a _save_. You go back to the menu with **Hotkey** (on Reicast it's enough), then you launch the second disk **by recovering its backup**.

Nothing could be easier !