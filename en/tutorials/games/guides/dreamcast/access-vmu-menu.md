---
title: Access VMU menu
description: 
published: true
date: 2021-08-08T07:04:43.523Z
tags: card, memory, vmu
editor: markdown
dateCreated: 2021-08-08T07:04:43.523Z
---

## Objective

The objective is to be able to modify and manipulate the contents of the memory cards. There are 2 ways to proceed.

## {.tabset}
### Eject the disk

* Launch a Dreamcast game.
* When the game is playing, eject the disk with `Hotkey` + `Left stick up`.
* The console will automatically stop the game and come to the system. From here, you can access the memory cards.
* To resume the game, press `Hotkey` + `Left Stick Up` again.

### Empty disk

* Create an empty text file in the `/share/roms/dreamcast` folder, which you can rename to your liking with a `.cdi` extension (Dreamcast BIOS.cdi for example).
* Update the list of games in Recalbox.
* Launch the "game" and voilà!