---
title: Buttons configuration
description: 
published: true
date: 2021-09-09T17:27:47.258Z
tags: configuration, buttons, ti-99/4a
editor: markdown
dateCreated: 2021-09-09T17:14:49.512Z
---

## Introduction

The keys and other buttons of the TI-99/4A can be difficult to understand, this tutorial will show you which buttons to use.

## Use

Here is the correspondence of the keys for the keyboard:

| Joystick button | System button
| :--- | :--- |
| `L1` | 1 (boot selection) |
| `R1` | 2 (boot selection) |
| `X` | 3 (boot selection) |
| `Y` | 4 (boot selection) |
| `START` | `Enter` |
| `B` | `Space` |

For the joystick, your left stick will do the job with the `A` button.

>A `.P2K` file is supplied as standard with the emulator.
{.is-success}