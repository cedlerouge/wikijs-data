---
title: FAQ
description: 
published: true
date: 2021-09-09T22:40:27.875Z
tags: thomson, mo5, to8, faq
editor: markdown
dateCreated: 2021-08-08T09:34:37.776Z
---

## Usage

By default, the type of computer emulated depends on the file name used.

_**Example:**_  
**super-tennis-fil_mo5.k7** will emulate the **MO5**.

>If the file does not contain the name of one of the emulated machines, the emulator will switch to TO8 mode by default.)  
>It is possible to force the type of emulated machine via an option in RetroArch.
{.is-info}

The games do not start automatically when the machine is loaded.  
On Thomson computers, it is indeed necessary to press one (or more) key(s) on the keyboard to launch a program. 
  
In order to facilitate the use of the controller emulator, the `START` button on the controller tries to launch the game by choosing a method which depends on the emulated computer and the type of media inserted (see [Theodore's README file](https://github.com/Zlika/theodore/blob/master/README.md#video_game-gamepad-mapping-of-the-buttons)).  
  
If this fails, try another method via the keyboard or the virtual keyboard (see below).

## Controllers

Apart from the `B` button on the controller which emulates the single button joysticks for Thomson, the other buttons on the controller are used for the "virtual keyboard" feature.

This feature makes it easy to play most games without the need for a real keyboard.

<table>
  <thead>
    <tr>
      <th style="text-align:center">Button</th>
      <th style="text-align:left">Description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td style="text-align:center"><b>B</b>
      </td>
      <td style="text-align:left">
        <p>"Action" button when the virtual keyboard is not displayed.</p>
        <p>When the virtual keyboard is displayed:</p>
        <ul>
          <li>Short press: Press the key on the keyboard that has the focus.</li>
          <li>Long press: Hold the key permanently (or release it if
            it was already held). Up to 3 keys
            can be held. The disappearance of the virtual keyboard releases
            all held keys.</li>
        </ul>
      </td>
    </tr>
    <tr>
      <td style="text-align:center"><b>Y</b>
      </td>
      <td style="text-align:left">Moves the virtual keyboard to the top or bottom of the screen.
    </td>
    <tr>
      <td style="text-align:center"><b>Start</b>
      </td>
      <td style="text-align:left">
        <p>Start the game if the virtual keyboard is not displayed.</p>
        <p>Shortcut to press the "Enter" key if the virtual keyboard
          keyboard is displayed.</p>
      </td>
    </tr>
    <tr>
      <td style="text-align:center"><b>Select</b>
      </td>
      <td style="text-align:left">Show/hide virtual keyboard.</td>
    </tr>
  </tbody>
</table>