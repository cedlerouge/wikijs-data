---
title: FAQ
description: 
published: true
date: 2021-09-23T23:22:04.561Z
tags: 7.3+, atari, st, faq
editor: markdown
dateCreated: 2021-09-04T23:22:31.428Z
---

## Some useful information...

### Which machines can I use with the Atari ST ?

Depending on the subdirectories, the machine to use will be automatically selected according to these criteria:

```text
"/atarist/": ST,
"/atari-st/": ST,
"/st/": ST,
"/atariste/": STE,
"/atari-ste/": STE,
"/ste/": STE,
"/ataritt/": TT,
"/atari-tt/": TT,
"/tt/": TT,
"/atarimegaste/": MEGA_STE,
"/atari-megaste/": MEGA_STE,
"/atari-mega-ste/": MEGA_STE,
"/megaste/": MEGA_STE,
"/mega-ste/": MEGA_STE,
"/falcon/": FALCON,
"/atari-falcon/": FALCON,
```

### There are several bios for this machine, how is this handled?

Depending on the machine, the system will look for the corresponding bios automatically:

- First, the bios of the machine to use:
  - `falcon.img`
  - `megaste.img
  - `st.img`
  - `ste.img`
  - `tt.img`

- If the corresponding bios in the above list is not found for the desired machine, the bios `tos.img` will be searched.

- Finally, if none of the above bios are found, the default EmuTOS bios will be used.

### Does the Atari ST benefit from configuration overrides?

Yes, and for all the emulators provided!

For Libretro cores, the `.retroarch.cfg` files should be used. For the Hatari standalone core, the `.hatari.cfg` files should be used.

### Are both joysticks taken into account?

No, only the first joystick can be used at the moment.

### How are the buttons configured ?

Here is the correspondence of the buttons:

| Joystick button | System button |
| :--- | :--- |
| `A` | right click |
| `B` | left click |
| `X` | open the interface (to swap disks, etc...) |
| `Y` | Shift |
| `Select` | toggle between mouse and joystick modes |
| `R1` | change mouse sensibility |
| `L1` | open virtual keyboard |
| `L2` | open a menu allowing you to see the control mode you are in (mouse/joystick); as well as the level of the mouse sensitivity setting (a number that varies next to MS: - value that you change with R1). |

## Are there any tools for Roms?

Yes, and we provide them to you! There are 3 tools in `/usr/share/hatari` :

* **atari-hd-image.sh** : this script will help you to create a hard disk with partitioning, volume naming, etc. To use it: `bash atari-hd-image.sh`.
* **hmsa**: application that allows you to convert between MSA <=> ST floppy disk formats. To use it: `./hmsa`.
* **zip2st.sh** : it is a converter of zip content => .st floppy image (useful for falcon games that are only available in zip format...). To use it: `bash zip2st.sh`.

## Does the Hatari emulator support overloads?

Yes, just like the Libretro Hatari emulator, the Hatari standalone supports its own overloads with the suffix `.hatari.cfg`. These are usable via `<game>.<ext>.hatari.cfg` for a specific game or `.hatari.cfg` for an entire directory, in the same way as the `.retroarch.cfg` overloads.

## Can I use roms in `.ZIP` format?

Yes, even for the Hatari standalone!