---
title: Use Wiimotes as controllers
description: 
published: true
date: 2021-08-08T08:48:45.270Z
tags: controllers, wiimotes
editor: markdown
dateCreated: 2021-08-08T08:48:45.270Z
---

>Dolphin can connect up to 4 Wiimotes simultaneously.
>
>To enable Wiimote support in Dolphin, the Dolphin Bar from Mayflash is required. You will have to set it to mode 4.
{.is-info}

## Configuration

* Open the file [recalbox.conf](./../../../../basic-usage/getting-started/recalboxconf-file).
* Find the following text:

```ini
## Wiimotes
## Real wiimotes must not be paired with recalbox system so that they can work with the wii emulator
## set emulatedwiimotes to 1 to emulate wiimotes with standard pads
wii.emulatedwiimotes=0
```

* Change the following line:

```ini
wii.emulatedwiimotes=0
```

* Like this:

```ini
wii.emulatedwiimotes=1
```

Now you can play with your Wiimotes.