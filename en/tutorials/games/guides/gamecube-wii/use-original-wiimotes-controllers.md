---
title: Use original Wiimotes controllers
description: 
published: true
date: 2021-08-08T08:29:51.832Z
tags: controllers, wii, dolphin, bar, wiimotes
editor: markdown
dateCreated: 2021-08-08T08:29:33.024Z
---

>The Dolphin Bar from Mayflash is required to fully use the Wiimotes with Dolphin.
{.is-info}

* Connect your Dolphin Bar to your Recalbox and switch it to mode 4.
* Open the file `/recalbox/share/system/recalbox.conf`.
* In the section "D3 - Dolphin Controllers" look for the following line:
  * `wii.realwiimotes=0` 
* then change it to this:
  * `wii.realwiimotes=1` 
* Now your Wiimotes are working in Dolphin.