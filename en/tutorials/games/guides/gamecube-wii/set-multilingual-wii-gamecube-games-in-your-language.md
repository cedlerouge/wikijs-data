---
title: Set multilingual Wii/Gamecube games in your language
description: 
published: true
date: 2021-08-08T08:45:20.031Z
tags: games, language, gamecube, wii, multilingual
editor: markdown
dateCreated: 2021-08-08T08:37:40.561Z
---

## Introduction

On games with multiple languages, you can't choose it at the start of a game since the language is set in the system.

## General use

To switch games to the language of your choice, here is the procedure to follow:

* Launch a GameCube or Wii game.
* With a keyboard, press `Alt + F4`.
* With the mouse, go to `Configure`.

![](/tutorials/games/guides/gamecube-wii/change-games-language/dolphinlanguage1-en.png)

## {.tabset}

### For a GameCube game

* In the new window, click on `GameCube` at the top.
* Change the `System Language` option.

![](/tutorials/games/guides/gamecube-wii/change-games-language/dolphinlanguage2-en.png){.full-width}

* Validate and quit the emulator to restart it.

### For a Wii game

* In the new window, click on `Wii` at the top.
* Change the `System language` option.

![](/tutorials/games/guides/gamecube-wii/change-games-language/dolphinlanguage3-en.png){.full-width}

* Validate and quit the emulator to restart it.