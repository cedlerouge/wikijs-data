---
title: FAQ
description: 
published: true
date: 2021-10-03T22:29:16.563Z
tags: fbneo, finalburn, neo, faq
editor: markdown
dateCreated: 2021-08-08T14:14:47.661Z
---

### The game XXX does not launch, why ?

It is not supported or you have a bad rom, your logs will give you more details. Create a valid romset with clrmamepro as described above. There are also some games marked as not working, try one of their clones.

### I fixed the XXX game and I can't run it, why?

Because it is considered a bad rom as the ccs will not match, however there is a method to use a patched romset, if you put the patched version of the romset in `SYSTEM_DIRECTORY/fbneo/patched` it will work (Note: you can remove it from any file that is not different from the unpatched romset if you want).

### The game XXX has graphic problems, why?

Write a report with details about the problem and your platform.

### The game XXX runs slowly, why?

Your hardware is probably too slow to run the game with normal settings. Try the following:

* Check if there is a Speedhack DIP switch in the main options, set it to "Yes".
* Try setting a value for frameskip in the main options.
* Try reducing the processor clock in the basic options.
* Try disabling rewind, runahead or any other retroarch setting that is known to increase overhead.
* Try reducing the audio settings in the main options.
* If this is not enough, upgrade / overclock your hardware or use another kernel.

We will not accept requests to "make the kernel faster", as far as we are concerned, this kernel has a good balance between precision and speed, and for the most part, it already works very well on cheap arms (rpi3, ...).

### The XXX game has a jerky sound, why ?

Most probably for the same reason as above.

### The XXX game runs faster in MAME2003 / MAME2010, why?

It's not MAME, we usually use more "up-to-date" code. Overall, FB Alpha is slower than the older MAME version but more accurate and less buggy. This libretro port also supports various features that are usually buggy or absent in MAME cores (netplay, rewinding, backlinks, ...). It can use additional resources.

### The cheat code does not work, why?

There should be partial support through the new API based on the main ram exposure.

### The Neogeo CD does not work, why?

There are several things to know:

* You need a copy of neocdz.zip and neogeo.zip in your system directory libretro
* You need to add `--subsystem neocd` to the command line or put your games in a `neocd` folder
* Supported formats are ccd / sub / img (trurip) and the single file MODE1 / 2352 cue / bin (use utilities like "CDmage" to convert your iso if needed), **they should not be compressed**.

You can convert your unsupported isos by following this tutorial:

* Get [CDMage 1.02.1](https://www.videohelp.com/software/CDMage) (free and ad-free)
* File &gt; Open &gt; select your iso (NB: for multi-track, select the .cue file, not the .iso file)
* File &gt; Save as &gt; write the name of your new file
* Make sure you select MODE1 / 2352 in the second drop-down menu
* Press OK, wait for the process to finish (a few seconds on my computer), and it's done!

### Killer instinct won't work, why?

This driver has been disabled for now, it does not meet our quality criteria. ~~There are several things to know:~~

* ~~It only works at a playable speed on x86\_64 (other arches will essentially need a 4Ghz processor as they lack a mips3\ dynarec), and the kernel must be built like this to enable this dynarec: `make -j5 -C src/burner/libretro USE_X64_DRC=1`~
* ~~If your rom is at `ROM_DIRECTORY/kinst.zip`, you will need the uncompressed disk image at `ROM_DIRECTORY/kinst/kinst.img`~~
* ~~To get the uncompressed disk image, you will need to use MAME's chdman tool on the mame chd, the command looks like this: `chdman extracthd -i kinst.chd -o kinst.img`~~

### Hiscore will not work, why?

Having hiscore.dat and the basic option enabled does not guarantee that its scores will work for a specific game, sometimes a driver will miss the necessary code. You can ask for assistance in tracking problems as long as the request is reasonable (i.e. avoid making a list of several dozen/hundred games).

---

## Sample

The samples must be placed under `SYSTEM_DIRECTORY/fbneo/samples`.

## Hiscore.dat

Copy [hiscore.dat](https://github.com/libretro/FBNeo/blob/master/metadata/hiscore.dat) in `SYSTEM_DIRECTORY/fbneo/`.

## MAPPING

We don't have a handy tool like the OSD MAME, but we do use the RetroArch API to customize mappings, you can do this by going to `Quick menu` > `Controls`.
For those who don't want to fully customize their mappings, there are 2 handy presets you can apply by changing the "device type" for a player in this menu:

* **Classic/Classic**: it will apply the "square" mapping of the original neogeo cd in neogeo games, and use L/R as the 5th and 6th button for 6 button games like Street Fighter II.
* **Modern/Modern**: it will apply the King of Fighters mapping of Playstation 1 and above in neogeo fighting games, and it will use R1 / R2 as the 5th and 6th button for 6-button games like Street Fighter II (for the same reason as neogeo games), this is really convenient for most arcade sticks.

The following "device types" also exist, but they will not be compatible with all games:

* **Mouse (ball only)**: it will use the mouse / trackball for analog movements, the buttons will stay on retropad
* **Mouse (full)**: as above, but buttons will be on the mouse
* **Pointer / Pointeur**: it will use a "pointer" device (can be a mouse / trackball) to determine the coordinates on the screen, the buttons will stay on retropad
* **Lightgun**: it will use the lightgun to determine the coordinates on the screen, the buttons will also be on the lightgun.

## Dat

Use [clrmamepro](./../../../utilities/rom-management/clrmamepro) (which works well on Linux with Wine and on macOS with Crossover) to create valid romsets with data from the [dats](https://github.com/libretro/FBNeo/tree/master/dats) directory.
Do not report problems if you have not created a valid romset.  
Also, only "parent only" dat are provided, as it is not recommended to use only parent roms (some do not work and some clones are really different from their parent).

## Emulating consoles

You can emulate consoles (with specific romsets, the dats are also in the [dats](https://github.com/libretro/FBNeo/blob/master/dats) directory) by prefixing the name of the roms with `XXX_` and removing the `zip|7z` extension, or by using the `--subsystem XXX` command line argument, here is the list of available prefixes:

* CBS ColecoVision : `cv`
* MSX 1 : `msx`
* Nec PC-Engine : `pce`
* Nec SuperGrafX : `sgx`
* Nec TurboGrafx-16 : `tg`
* Nintendo Entertainment System : `nes`
* Nintendo Family Disk System : `fds`
* Sega GameGear : `gg`
* Sega Master System : `sms`
* Sega Megadrive/Genesis : `md`
* Sega SG-1000 : `sg1k`
* SNK Neo-Geo Pocket : `ngp`
* ZX Spectrum : `spec`

It is also possible to use the folder name for detection (this second method has been added because some devices are not compatible with the subsystems):

* CBS ColecoVision : `coleco`
* MSX 1 : `msx`
* Nec PC-Engine : `pce`
* Nec SuperGrafX : `sgx`
* Nec TurboGrafx-16 : `tg16`
* Nintendo Entertainment System : `nes`
* Nintendo Family Disk System : `fds`
* Sega GameGear : `gamegear`
* Sega Master System : `sms`
* Sega Megadrive : `megadriv`
* Sega SG-1000 : `sg1000`
* SNK Neo-Geo Pocket : `ngp`
* ZX Spectrum : `spectrum`