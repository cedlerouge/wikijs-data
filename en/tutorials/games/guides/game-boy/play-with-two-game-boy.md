---
title: Play with two Game Boy
description: 
published: true
date: 2021-08-08T08:09:38.179Z
tags: two, gameboy
editor: markdown
dateCreated: 2021-08-08T08:08:25.644Z
---

It is possible to play two players with two GameBoy on Recalbox. There are 3 ways to do this.

>Although it may be tempting, Recalbox does not support Netplay in this mode.
>
>Moreover, in the one game for two Game Boy mode, saves are not supported for the second Game Boy, unlike in the [two games two Game Boy](./play-two-players-games-with-gamelink) mode, where again, Netplay is not supported.
{.is-warning}

## The "no edit" method

1. In EmulationStation, select the Game Boy or Game Boy Color.
2. In the list of games, select the game you want to play and press `START`.

![](/tutorials/games/guides/game-boy/play-two-players/gameboytwoplayers1-en.png){.full-width}

3. Go to `MODIFY THE GAME` > `CLEAR WITH` and choose the core `LIBRETRO TGBDUAL`.

![](/tutorials/games/guides/game-boy/play-two-players/gameboytwoplayers2-en.png){.full-width}

4. Close all menus and start the game.
5. When the game starts, go to the RetroArch menu by pressing `Hotkey + B`.
6. Go back twice and then go to `Settings` > `Configuration`.

![](/tutorials/games/guides/game-boy/play-two-players/gameboytwoplayers3-en.png){.full-width}

7. Enable the `Save configuration on exit` setting.

![](/tutorials/games/guides/game-boy/play-two-players/gameboytwoplayers4-en.png){.full-width}

8. Do "Back" twice and go to `Main Menu` > `Quick Menu` > `Options`.

![](/tutorials/games/guides/game-boy/play-two-players/gameboytwoplayers5-en.png){.full-width}

9. Enable the `Link cable emulation (reload)` parameter.

![](/tutorials/games/guides/game-boy/play-two-players/gameboytwoplayers6-en.png){.full-width}

You can turn off `Save Configuration on Exit` again after restarting the game.

>If you use the option `RESET FACTORY SETTINGS`, these changes will be lost!
{.is-warning}

## Editing the recalbox.conf file

* Edit the file [recalbox.conf](./../../../../basic-usage/getting-started/recalboxconf-file) to replace the GameBoy emulator with `tgbdual` :

```text
gb.core=tgbdual ;GameBoy
gb.ratio=custom
gbc.core=tgbdual ;GameBoyColor
gbc.ratio=custom
```

* Save and restart your Recalbox.
* Start a GameBoy game (e.g. `Tetris`).
* Open the RetroArch menu with `Hotkey + B`.
* Press `Back` twice, then go to `Settings` > `Configuration`.

![](/tutorials/games/guides/game-boy/play-two-players/gameboytwoplayers7-en.png){.full-width}

* Enable the `Save configuration on exit` setting.

![](/tutorials/games/guides/game-boy/play-two-players/gameboytwoplayers8-en.png){.full-width}

* Do "Back" twice and go to `Main Menu` > `Quick Menu` > `Options`.

![](/tutorials/games/guides/game-boy/play-two-players/gameboytwoplayers9-en.png){.full-width}

* Enable the `Link cable emulation (reload)` parameter.

![](/tutorials/games/guides/game-boy/play-two-players/gameboytwoplayers10-en.png){.full-width}

* Go back to the game and quit the game to return to EmulationStation.
* Start your game again, press `Hotkey + B` to return to the RetroArch menu and go to `Options`.

![](/tutorials/games/guides/game-boy/play-two-players/gameboytwoplayers11-en.png){.full-width}

* For the `Screen layout` option, choose `left-right`.

![](/tutorials/games/guides/game-boy/play-two-players/gameboytwoplayers12-en.png){.full-width}

* For the switch player screens option, choose `normal`.

![](/tutorials/games/guides/game-boy/play-two-players/gameboytwoplayers13-en.png){.full-width}

* For the `Show player screen` option, choose `both players`.

![](/tutorials/games/guides/game-boy/play-two-players/gameboytwoplayers14-en.png){.full-width}

* Go back twice and then go to `Settings` > `Video` > `Scaling`.

![](/tutorials/games/guides/game-boy/play-two-players/gameboytwoplayers15-en.png){.full-width}

* For the `Aspect Ratio` option, choose `1:1 PAR (20:9 DAR)`.

![](/tutorials/games/guides/game-boy/play-two-players/gameboytwoplayers16-en.png){.full-width}

* Go back to the game and quit the game to return to EmulationStation.

>Think that if your TV is not set to 16:9 - if the Recalbox menu does not occupy the whole screen - then the aspect ratio will not be good.
>
>Note also that single player games will also be dubbed. Two GB side by side, good for a speed run duel, otherwise useless.
>
>To avoid too many manipulations, you can apply all these settings only on GB or only on GBC.
>
>If you use the option `RESTORE FACTORY SETTINGS`, these changes will be lost!
{.is-warning}

## The use of overload files (for advanced users)

Configuration overrides are a way to force settings for a set, a subfolder or a whole system. You can read more about it on the [dedicated page](./../../../../advanced-usage/configuration-override).

In this case, we will need a file named `gamename.extension.recalbox.conf` (e.g. `Tetris (World) (Rev 1).zip.recalbox.conf`) containing the following lines (force core `Libretro TGBDual` for this game only):

```text
gb.core=tgbdual
gbc.core=tgbdual
```

A `gamename.extension.core.cfg` file (for example: `Tetris (World) (Rev 1).zip.core.cfg`) containing these lines:

```text
tgbdual_audio_output = "Game Boy #1"
tgbdual_gblink_enable = "enabled"
tgbdual_screen_placement = "left-right"
tgbdual_single_screen_mp = "both players"
tgbdual_switch_screens = "normal"
```

>Here we have set TGBDual to use the sound of Game Boy #1, activate GameLink, display two GameBoys side by side (one on the left and one on the right), and the GameBoy of player #1 is on the left.
{.is-info}

And a final `gamename.extension.retroarch.cfg` file (for example: `Tetris (World) (Rev 1).zip.retroarch.cfg`) containing:

```text
aspect_ratio_index = "22"
input_overlay_enable = "false"
```

>On force l'aspect ratio dans le mode 22 : Core Provided, tout en désactivant les overlays.
{.is-info}

Ces fichiers doivent être placés au même endroit que le jeu souhaité (dans l'exemple pris précédemment, au même endroit que le jeu `Tetris (World) (Rev 1).zip`).

>Si les fichiers de surcharges se nomment uniquement `.recalbox.conf`, .`retroarch.cfg` et `.core.cfg`, alors ils s'appliqueront à tout le dossier dans lequel ils sont placés, ainsi que les sous-dossiers si il y en a.
{.is-info}

>En cas d'utilisation de l'option `RESTAURER LES PARAMÈTRES D'USINE`, ces modifications seront conservées !
{.is-success}

>Les fichiers de surcharges ne sont pas affectés par la configuration depuis EmulationStation, et ils prennent le dessus sur ces derniers. Il est donc impératif de pouvoir accéder à ces fichiers pour effectuer une modification ultérieure sur les paramètres qu'ils affectent.
{.is-warning}