---
title: Dreamcast
description: 
published: true
date: 2021-08-08T08:53:32.670Z
tags: sega, dreamcast
editor: markdown
dateCreated: 2021-08-07T23:44:03.896Z
---

With the tutorials available here, you will discover how to manage the contents of memory cards as well as how to change the disk for games on multiple disks.

Here are the available tutorials:

[Access VMU menu](access-vmu-menu)
[Multidisc games](multidisc-game) 