---
title: FAQ
description: 
published: true
date: 2021-08-08T09:38:40.304Z
tags: zx, spectrum, faq, trdos
editor: markdown
dateCreated: 2021-08-08T09:38:40.304Z
---

## TRDOS floppy disks

* To use TRDOS diskettes (_.scl or_ .trd), you must set the machine to "**Scorpion 256k**" in the options
  * RetroArch menu
  * Options
  * Switch the machine to "**Scorpion 256k**". 
  * Use the keyboard to validate "GLUK BOOT"
  * Then the name of the game.