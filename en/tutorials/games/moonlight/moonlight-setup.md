---
title: Moonlight setup
description: 
published: true
date: 2021-08-07T15:20:21.502Z
tags: moonlight, setup
editor: markdown
dateCreated: 2021-08-07T15:20:21.502Z
---

## Introduction

Moonlight is an open-source version of NVidia's Gamestream technology.

If your PC meets the requirements, you can stream most of your games to your Recalbox. On the other hand, Recalbox reads the pad configuration from EmulationStation and converts it to Moonlight.

### Moonlight features on Recalbox

* Stream games over your local network or the Internet
* Support up to 4 players
* Up to 1080p/60fps
* Hardware accelerated H264 decoding on any version of the Raspberry Pi
* Supports keyboard and mouse
* Up to GFE 3.12

### Prerequisites

Recalbox and PC requirements to enjoy Moonlight:

* A controller configured in EmulationStation
* Steam account (optional) or standalone supported games (see [https://shield.nvidia.com/game-stream](https://shield.nvidia.com/game-stream))
* An Nvidia compatible GPU (see [http://www.geforce.com/geforce-experience/system-requirements])
* An Ethernet connection is highly recommended, WiFi is not reliable enough.

## Moonlight configuration

Recalbox offers a nicer scrap, the ability to stream from multiple PCs on the network and a small option to find available GFE hosts.

Here's a demo for finding GFE hosts, connecting them and running them:

```shell
# /recalbox/scripts/moonlight/Moonlight.sh find
Listing available GFE servers :
GFE Host WIN10(192.168.111.35) GeForce GTX 760 running GFE 3.12.0.84
You can now run /recalbox/scripts/moonlight/Moonlight.sh pair <host>
<host> can be empty (not recommended if you have several GFE hosts), an IP or a PC name

# /recalbox/scripts/moonlight/Moonlight.sh pair
() /recalbox/share/system/configs/moonlight/moonlight.conf | /recalbox/share/system/configs/moonlight/keydir
Searching for server...
Connect to 192.168.111.35...
Generating certificate...done
Please enter the following PIN on the target PC: 3843
Succesfully paired
YOLO MODE !!!

# /recalbox/scripts/moonlight/Moonlight.sh init
YOLO Mode
Adding and scraping Brutal Legend ...
Adding and scraping Sacred Citadel ...
Adding and scraping Just Cause 3 ...
Adding and scraping Street Fighter V ...
Adding and scraping Just Cause 2 Multiplayer ...
Adding and scraping Tales of Zestiria ...
Adding and scraping Grand Theft Auto V ...
Adding and scraping Hell Yeah! Wrath of the Dead Rabbit ...
Adding and scraping Ultra Street Fighter IV ...
Adding and scraping Diablo III ...
Adding and scraping Pro Evolution Soccer 2017 ...
Adding and scraping Bionic Commando Rearmed ...
Adding and scraping Just Cause 2 ...
Adding and scraping Pro Evolution Soccer 2016 ...
Adding and scraping Broforce ...
Adding and scraping DmC: Devil May Cry ...
Adding and scraping Naruto Shippuden: Ultimate Ninja Storm 4 ...
Adding and scraping Steam ...
```

## APPENDIX

>Advanced users only: You can stream from a remote computer over the Internet. 
{.is-warning}

* You will need to modify `/recalbox/scripts/moonlight/Moonlight.sh` to:
  * Specify the IP address of the remote host,
  * Set the same IP address in moonlight.conf,
  * And set up port forwarding according to [https://github.com/moonlight-stream/moonlight-android/wiki/Setup-Guide#streaming-over-the-internet](https://github.com/moonlight-stream/moonlight-android/wiki/Setup-Guide#streaming-over-the-internet)