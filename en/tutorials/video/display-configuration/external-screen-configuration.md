---
title: Configure an external screen
description: 
published: true
date: 2021-08-06T17:19:58.762Z
tags: screen, configure, external
editor: markdown
dateCreated: 2021-08-06T17:19:58.762Z
---

With a laptop (for example) you can decide to have the video signal on an external monitor. This can be useful if the video card is not supported by Recalbox or your internal monitor is not working anymore.

## Know the available video outputs

There are 2 ways to know the available video outputs for your external monitor.

## {.tabset}

### Via the logs

* Connect your external display to your Recalbox and start your Recalbox.
* Once started, you must go to the Web Manager.
* In the web manager, go to `Logs` (or `Troubleshooting` > `Logs` on a cell phone).
* On the page, open the empty menu available and select the third option (`recalbox.log`).
* In this file, you should find lines similar to the following:

```ini
Connected screen(s) / Max resolution / Fallback resolution lower than FullHD:
  HDMI-0;2560x1440;1920x1080
  DP-5;2560x1440;1920x1080
Force selected output HDMI-0 limited to fallback resolution, disable others
xrandr --output HDMI-0 --mode 1920x1080 --output DP-5 --off
```

* Once you have this information, in the web manager menu, go to `recalbox.conf`.
* Once this file is open, fill in this part with your information:

```ini
## Prefered external screen retrieved from ssh : xrandr
system.externalscreen.prefered=DP-5
## Force selected external screen to resolution ex : 1920x1080
system.externalscreen.forceresolution=1920x1080
## Force selected external screen to frequency ex: 60.00
;system.externalscreen.forcefrequency=
```

>The **;** sign will be in front of the 2 lines you are modifying by default. You will have to remove this character in front of each of the 2 lines to activate your modifications.
{.is-info}

### Via the `xrandr` command

* Connect via [SSH](./../../system/access/root-access-terminal-cli)
* Type the following command:

```shell
xrandr
```

This will give you the available video outputs, their resolutions and refresh rates. For example:

```shell
Screen 0: minimum 8 x 8, current 1920 x 1080, maximum 32767 x 32767
DVI-D-0 disconnected primary (normal left inverted right x axis y axis)
HDMI-0 connected 1920x1080+0+0 (normal left inverted right x axis y axis) 598mm x 336mm
   2560x1440     59.95 +  69.93
   1920x1080     60.00*   59.94    50.00    23.98    60.05    60.00    50.04
   1680x1050     59.95
   1440x900      59.89
   1280x1024     75.02    60.02
   1280x960      60.00
   1280x800      59.81
   1280x720      60.00    59.94    50.00
   1152x864      75.00
   1024x768      75.03    70.07    60.00
   800x600       75.00    72.19    60.32    56.25
   720x576       50.00
   720x480       59.94
   640x480       75.00    72.81    59.94    59.93
DP-0 disconnected (normal left inverted right x axis y axis)
DP-1 disconnected (normal left inverted right x axis y axis)
DP-2 disconnected (normal left inverted right x axis y axis)
DP-3 disconnected (normal left inverted right x axis y axis)
DP-4 disconnected (normal left inverted right x axis y axis)
DP-5 connected (normal left inverted right x axis y axis)
   2560x1440     59.95 +
   1920x1080     59.94    50.00
   1280x720      59.94    50.00
   1024x768      60.00
   800x600       60.32
   720x576       50.00
   720x480       59.94
   640x480       59.94    59.93

```

* Open the file [recalbox.conf](./../../../basic-usage/getting-started/recalboxconf-file)
* Once you have selected the output, enter it in this part of the `recalbox.conf` file:

```ini
## Prefered external screen retrieved from ssh : xrandr
system.externalscreen.prefered=DP-5
## Force selected external screen to resolution ex : 1920x1080
system.externalscreen.forceresolution=1920x1080
## Force selected external screen to frequency ex: 60.00
system.externalscreen.forcefrequency=59.94
```

>The **;** sign will be in front of the 2 lines you are modifying by default. You will have to remove this character in front of each of the 2 lines to activate your modifications.
{.is-info}