---
title: Connect your Recalbox to a cathodic screen with HDMI and SCART
description: 
published: true
date: 2021-08-06T19:37:00.291Z
tags: hdmi, screen, cathodic, scart
editor: markdown
dateCreated: 2021-08-06T19:36:11.258Z
---

## Principles

### Cabling

First, you will have to create the super-cable to convert the VGA to SCART.  
Thanks to @ian57 for [this message](https://forum.recalbox.com/topic/3475/recalbox-sur-tv-crt-en-rgb/745)!

I used this (HDMI VGA SCART cable) on a terminal with a 36 cm Tv, there were no particular worries.

Here is the diagram of my wiring at the output of the VGA converter.

![](/tutorials/video/crt/ac9ba3d3-29fc-4ff2-8644-aee71b62f043.png){.full-width}

Then for the configurations, it is very different from VGA666 / pi2scart / rgbPi (they all 3 use DPI in 18 bits mode (6 bits/color)).

In the [file recalbox-user-config.txt](./../../../tutorials/system/modification/configtxt-configuration), we configure as if we had a 1920x240 screen:

```ini
hdmi_cvt=1920 240 60 1 1 0 0
hdmi_disable_edid=0xa5000080 #Enables the ignoring of EDID/display data if your display doesn't have an accurate EDID.

hdmi_pixel_encoding=2 #Force the pixel encoding mode. By default it will use the mode requested from edid so shouldn't need changing. 
avoid_safe_mode=1
disable_overscan=0 #oversan enabled
hdmi_drive=2
hdmi_group=1
hdmi_mode=6
hdmi_force=1
```

In the file [recalbox.conf](./../../../basic-usage/getting-started/recalboxconf-file)), we force EmulationStation to be displayed in 480 interlaced (it stings the eyes a bit); otherwise we use the `CEA 8 HDMI` in 240 non-interlaced, but we'll need a theme adapted to this low resolution.

```ini
system.es.videomode=CEA 6 HDMI
 
#global.videomode=CEA 4 HDMI
global.videomode=DMT 87 HDMI
```

Then you have to tell Libretro for each emulator to use only a part of the 1920x240 window.  
To do this, you need to [create a](https://forum.recalbox.com/topic/18927/recalbox-6-1-sur-%C3%A9cran-crt/9) config file for each emulator to put in `/share/system/configs/retroarch`.

For example, the `megadrive.cfg` file

```ini
aspect_ratio_index = "23"
custom_viewport_width = "1792"
custom_viewport_height = "224"
custom_viewport_x = "104"
custom_viewport_y = "16"
```

The last 4 lines have to be adapted to the geometry of the screen. It is possible to find the values by adjusting from the RetroArch menu.

If this is not done, the emulator image will be completely crushed in the middle of the screen.

## Useful links for RetroArch kernel configuration

[This link](https://surchargeur-ra-rb.netlify.app/) allows you to easily generate configuration files for RetroArch from a web interface.