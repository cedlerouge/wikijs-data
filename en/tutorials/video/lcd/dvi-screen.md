---
title: Connect your Recalbox to a DVI screen
description: 
published: true
date: 2021-08-06T17:17:33.352Z
tags: dvi, screen
editor: markdown
dateCreated: 2021-08-06T17:17:33.352Z
---

You have to connect your Recalbox to a DVI monitor but you get a black screen or a pink line on the edge of the screen?

* Make the [boot partition](./../../system/access/remount-partition-with-write-access) writeable.
* Edit the [recalbox-user-config.txt](./../../system/modification/configtxt-configuration), and comment out the line:

```ini
hdmi_drive=2
```

* With a # in front:

```ini
#hdmi_drive=2
```