---
title: Add themes into EmulationStation
description: 
published: true
date: 2021-08-06T15:18:31.686Z
tags: emulationstation, themes
editor: markdown
dateCreated: 2021-08-06T13:02:38.541Z
---

Want to try a new style for your EmulationStation? You are at the right place.

Feel free to click on the links for more information and to leave a comment on the respective posts to the different authors.

Thanks again to them for their work and sharing.

---

## Add a theme to EmulationStation

You can download themes created by the community in the [forum](https://forum.recalbox.com/category/14/themes-interface), just make sure you use a theme that is compatible with your version of Recalbox (i.e. an up-to-date theme with an up-to-date version).

You can manually add the theme of your choice by following these steps:

* Unzip the downloaded archive
* Copy the folder containing the theme you want to use to your EmulationStation in 2 different ways:
  * Either from the network:``Recalbox\share\themes``  
  * From the SD card or external storage if you have one: ``SHARE/themes/``.

> Be careful, all the folders of your theme must be in a single folder and not in the root of the `themes` folder.
This results in a tree structure that looks like the following:
📁 recalbox
┣ 📁 share
┃ ┣ 📁 themes
┃ ┃ ┣ 📁 Hyperwall 720p
{.is-warning}

You can now change the theme in the EmulationStation options and restart your Recalbox.

A video tutorial is available on YouTube [here](https://youtu.be/U185lY2tKTg).

## Compatibility

|  | GPICase | PI / OXu4 | PC | OGoA | OGoS | 16/9 | 4/3 |
| - | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| Darkswitch | ⚠️ | ✅ | ✅ | ⚠️ | ⚠️ | ✅ | ⚠️ |
| Dreambox | ⚠️ | ✅ | ✅ | ⚠️ | ⚠️ | ✅ | ⚠️ |
| Hyperwall |  | ✅ | ✅ | ⚠️ | ⚠️ | ✅ | ⚠️ |
| Mukashi | ❌ | ✅ | ✅ | ⚠️ | ⚠️ | ✅ | ⚠️ |
| Next Pixel | ✅ | ✅ | ✅ | ⚠️ |⚠️ |  ✅ | ✅ |
| Unifiedspin | ❌ | ✅ | ✅ | ⚠️ | ⚠️ | ✅ | ⚠️ |

Legend:

✅ Optimized: works perfectly, and has been optimized for the platform in question
⚠️ Compatible: works but is not optimized, some elements are not laid out as they should or the font sizes are not optimal but these problems are minor and do not prevent the theme from working properly
❌ Not compatible: does not work or the layout of the theme is not adapted at all (overlapping elements or texts, unreadable texts, distorted images...)

## Themes

### Darkswitch by Dwayne Hurst adapted by Butch Games

- Presence of all virtual systems or not
- Available resolution: 480p
- Option: none

> Enabling too many virtual systems (+/- 130) will cause the theme to crash
{.is-info}

![dark1.png](/tutorials/frontend-customization/add-themes-into-emulationstation/dark1.png)
![dark2.png](/tutorials/frontend-customization/add-themes-into-emulationstation/dark2.png)
Detailed presentation by clicking [here](https://forum.recalbox.com/topic/24439)

### Butch Games' Dreambox

- Presence of all virtual systems or not
- Available resolution: 480p
- Option : none

![dream1.png](/tutorials/frontend-customization/add-themes-into-emulationstation/dream1.png)
![dream2.png](/tutorials/frontend-customization/add-themes-into-emulationstation/dream2.png)
Detailed presentation by clicking [here](https://forum.recalbox.com/topic/23272)

### Hyperwall from Butch Games

- Presence of all virtual systems or not
- Available resolution: 720p
- Option : 4 different system views

![hyperwall1.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/hyperwall1.jpg)
![hyperwall2.jpg](/tutorials/frontend-customization/add-themes-into-emulationstation/hyperwall2.jpg)

Detailed presentation by clicking [here](https://forum.recalbox.com/topic/24597)

### Mukashi from mrseguin and airdream

- Missing some virtual systems or not but usable
- Available resolution : 720p - 1080p (beware very heavy at the moment when loading)
- Option : none

![muka1.png](/tutorials/frontend-customization/add-themes-into-emulationstation/muka1.png)
![muka2.png](/tutorials/frontend-customization/add-themes-into-emulationstation/muka2.png)
Detailed presentation by clicking [here](https://forum.recalbox.com/topic/14323)

### Next Pixel from mYSt

- Missing some virtual systems or not but usable
- Available resolutions: 480p - 720p - 1080p
- For OGoA / OGoS take 16/9 480p
- For the Pi and some PCs do not exceed 720p
- Options : very numerous
- The theme also uses mix/scrap specifically designed for Next Pixel (see theme documentation)

![next1.png](/tutorials/frontend-customization/add-themes-into-emulationstation/next1.png)
![next2.png](/tutorials/frontend-customization/add-themes-into-emulationstation/next2.png)
Detailed presentation by clicking [here](https://forum.recalbox.com/topic/16064)

### Unifiedspin by Butch Games inspired by Unified themes

- Presence of all virtual systems or not
- Available resolution : 720p
- Option : none
> Enabling too many virtual systems (+/- 130) will make the theme crash
{.is-info}

![unif1.png](/tutorials/frontend-customization/add-themes-into-emulationstation/unif1.png)
![unif2.png](/tutorials/frontend-customization/add-themes-into-emulationstation/unif2.png)
Detailed presentation by clicking [here](https://forum.recalbox.com/topic/24433)