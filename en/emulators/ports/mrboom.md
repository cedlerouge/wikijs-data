---
title: MrBoom
description: 
published: true
date: 2021-08-08T23:52:53.347Z
tags: ports, mrboom
editor: markdown
dateCreated: 2021-06-29T18:45:46.108Z
---

![](/emulators/ports/mrboom.png){.align-center}

## Technical data

* **Developer**: Javanaise

## Presentation

Mr.Boom is a Bomberman clone for up to 8 players for LibRetro / RetroArch.

**Bomberman** is a series of video games from Hudson Soft where the player embodies a bomb maker, the goal being to blow up the opponents/enemies to win. The game has been a great success, especially thanks to its multiplayer mode which, depending on the machine, allows up to ten people to play at the same time.

## Emulators

[Libretro MrBoom](libretro-mrboom)