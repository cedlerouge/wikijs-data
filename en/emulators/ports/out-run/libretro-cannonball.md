---
title: Libretro Cannonball
description: 
published: true
date: 2021-08-08T23:45:27.143Z
tags: libretro, out run, cannonball
editor: markdown
dateCreated: 2021-06-29T22:18:05.306Z
---

Libretro Cannonball is a recreation of the OutRun game engine written by Chris White in 2014.

## ![](/emulators/license.svg) License

This core is under a [**non-commercial**](https://github.com/libretro/cannonball/blob/master/docs/license.txt) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supporté |
| :---: | :---: |
| Saves | ✔ |
| Core Options | ✔ |
| Controls | ✔ |
| Remapping | ✔ |

## ![](/emulators/bios.svg) BIOS

>**No bios is required.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Supported extensions

La rom doit avoir l'extension :

* .88

### Location

Put your roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 ports
┃ ┃ ┃ ┃ ┣ 📁 Out Run
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **jeu.88**

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/cannonball/](https://github.com/libretro/cannonball)
* **Official source code**: [https://github.com/djyt/cannonball/](https://github.com/djyt/cannonball)
* **Official documentation**: [https://github.com/djyt/cannonball/wiki](https://github.com/djyt/cannonball/wiki)