---
title: Minecraft
description: 
published: true
date: 2021-08-08T23:45:44.715Z
tags: ports, minecraft
editor: markdown
dateCreated: 2021-06-29T18:44:36.522Z
---

![](/emulators/ports/minecraft.svg){.align-center}

## Technical data

* **Author**: Michael Fogleman

## Presentation

**Minecraft** is a sandbox video game (completely free construction) developed by the Swedish Markus Persson, alias Notch, then by the company Mojang Studios. It is a universe composed of voxels and randomly generated, which integrates a crafting system based on the exploitation and transformation of natural resources (mineralogical, fossil, animal and vegetable).

Minecon, a convention in honor of Minecraft, celebrates the official release of the game on November 18, 2011. Available in 95 languages, the video game has sold more than 100 million copies on all platforms in eight years, and is also available in several physical forms: papercraft (origami), derivative products (figurines, clothing, plush, etc.) and Lego game boxes.

Minecraft was originally developed as a web browser game, and later on Windows, Mac and Linux (using Java). A mobile port also exists, Minecraft Pocket Edition (abbreviated Minecraft PE), released on Android smartphones, iOS devices, Windows Phone and Windows 10 devices. A version for Xbox 360 was released on May 09, 2012, developed by 4J Studios. A PlayStation 3 version developed by Mojang was released on December 18, 2013. The PS4 version was released on September 04, 2014 on the PlayStation Store, the Xbox One version was released the next day while the Wii U version has been available for download on the Nintendo eShop since December 17, 2015 and in physical form since June 30, 2016. The Nintendo Switch version was released on May 12, 2017 and the New Nintendo 3DS version on September 14, 2017. As of May 2020, Minecraft has passed 200 million copies sold across all platforms, making it both the best-selling video game of all time and the sixth best-selling franchise of all time, with a community of 126 million monthly active players as of May 2020.

## Emulators

[Libretro Craft](libretro-craft)