---
title: Sigil
description: Doom 2019 en version Gore
published: true
date: 2021-08-08T23:47:08.030Z
tags: ports, sigil
editor: markdown
dateCreated: 2021-06-29T19:19:41.023Z
---

![](/emulators/ports/sigil.png){.align-center}

## Technical data

* **Created by**:
* **Year of release**: 2019
* **Engine**: id Tech 1 of Doom (1993)
* **Developer**: John Romero
* **Designer**: John Romero
* **Editor**: Romero Games
* **Music**: James Paddock, Buckethead

## Presentation

**Sigil** (stylized as **SIGIL**) is the unofficial fifth installment of the 1993 video game, Doom. Published by Romero Games on May 31, 2019, Megawad was created by one of Doom's writers, John Romero, independently of the current owner of the main game, Bethesda Softworks. It contains nine missions, cachun having a death match version and a new soundtrack created by James Paddock and Buckethead.

## Emulators

[Libretro PrBoom](libretro-prboom)