---
title: MSX Turbo R
description: 
published: true
date: 2021-08-09T14:52:58.999Z
tags: computers, msx-turbo-r
editor: markdown
dateCreated: 2021-07-28T20:53:44.448Z
---

![](/emulators/computers/msxturbor.svg){.align-center}

## Technical data

* **Manufacturer**: Panasonic
* **Year of release**: 1990
* **OS**: MSX BASIC V4.0 (32 kB)
* **CPU**: R800 (DAR800-X0G) @ 28.64 MHz and Z80A @ 3.58
* **RAM**: 256 KB (FS-A1ST) or 512 KB (FS-A1GT)
* **VRAM**: 128 KB
* **ROM**: 96 KB
* **GPU**: Yamaha V9958
* **Sound chip**: PSG (AY-3-8910 compatible), MSX-MUSIC, PCM, MSX-MIDI
* **Resolution**: 512 x 212 (4 or 16 colors) and 256 x 212 (16 to 19268 colors)

## Presentation

The **MSX** is a home (consumer) microcomputer standard of Japanese origin, dating back to the 1980s. Unlike most computers of that time, MSX was produced by various manufacturers. They were compatible with each other, both for hardware and software. Several versions of the standard followed one another.

Often interpreted as **M**icro**S**oft e**X**tension, the acronym MSX would mean **M**achines with **S**oftware e**X**changeability according to Kazuhiko Nishi, initiator of the project. The standard was created in 1983 and produced by many Japanese companies such as Canon, Casio, Panasonic, Sanyo, Sony, Toshiba or Yashica. The Japanese company Yamaha has produced MSX for music, including an MSX1 version with an eight-voice sound processor and DIN connectors to the MIDI standard. In Europe, Philips or Schneider have been present on the MSX scene.

The MSX standard was invented following a call for tenders from METI, which wanted computers to be compatible with each other (at the time, each brand/model of computer had its own language and its own operating system). Microsoft answered the call to develop the software layers: MSX-Basic interpreted programming language integrated as standard in a ROM and MSX-DOS 1 operating system. Later, MSX-DOS 2 was developed by ASCII, adding among other things the notions of directories (and sub-directories), SCSI hard disk partitions.

MSX had "national" particularities: the version marketed in Japan was equipped with a QWERTY/Kanji keyboard. In France, most had an AZERTY keyboard. A version supporting the Arabic language was marketed by the company Al Alamia. This version was called Sakhr ("rock" in Arabic).

The Sony MSX2 already had a mouse and a graphic desktop (interface).

There have been four generations of MSXs: the MSX (or MSX1), the MSX2, the MSX2+ and the MSX turbo R. They were very popular in Japan and Europe, especially during the MSX1 era. The MSX2+ is marketed in Europe thanks to a few importers in France, Spain and the Netherlands. The MSX turbo R is even rarer in Europe, because it is produced only by Panasonic and reserved exclusively for the Japanese market. However, there is an extension for MSX1 allowing to use it as an MSX2, the NEOS MA-20(v).

## Emulators

[ibretro blueMSX](libretro-bluemsx)