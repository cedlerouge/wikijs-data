---
title: Libretro PUAE
description: 
published: true
date: 2021-08-09T13:55:15.591Z
tags: libretro, amiga-600, amiga, puae, 600
editor: markdown
dateCreated: 2021-07-29T15:32:56.881Z
---

The **Libretro PUAE** emulator is based on P-UAE 2.6.1.

## ![](/emulators/license.svg) License

This core is under [**GPLv2**](https://github.com/libretro/PUAE/blob/master/COPYING) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features



## ![](/emulators/bios.svg) BIOS

### List of mandatory bios

You will find the bios with the name indicated in the column **Description** that you will have to rename with the name indicated in the column **Filename**.

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| kick34005.A500.rom | Kickstart v1.3 rev 34.5 (1987)(Commodore)(A500-A1000-A2000-CDTV).rom | 82a21c1890cae844b3df741f2762d48d | ❌ |
| kick37175.A500.rom | Kickstart v2.04 rev 37.175 (1991)(Commodore)(A500+).rom | dc10d7bdd1b6f450773dfb558477c230 | ❌ |
| kick40063.A600.rom | Kickstart v3.1 rev 40.63 (1993)(Commodore)(A500-A600-A2000)[!].rom | e40a5dfb3d017ba8779faba30cbd1c8e | ❌ |

### Location

Put the bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 🗒 **kick34005.A500.rom**
┃ ┃ ┃ ┣ 🗒 **kick37175.A500.rom**
┃ ┃ ┃ ┣ 🗒 **kick40063.A600.rom**

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .adf
* .adz
* .ipf
* .lha
* .lhz
* .lzx
* .zip
* .rp9
* .dms
* .fdi
* .hdf
* .hdz
* .m3u

### Location

Put the roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 amiga600
┃ ┃ ┃ ┃ ┣ 🗒 **game.ipf**

>The roms using **TOSEC** format are highly recommended.
{.is-success}

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://gitlab.com/recalbox/packages/libretro/libretro-uae/](https://gitlab.com/recalbox/packages/libretro/libretro-uae)
* **Libretro documentation**: [https://docs.libretro.com/library/puae/](https://docs.libretro.com/library/puae/)