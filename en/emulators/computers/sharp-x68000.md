---
title: Sharp X68000
description: 
published: true
date: 2021-08-09T15:02:48.300Z
tags: computers, sharp, x68000
editor: markdown
dateCreated: 2021-07-28T21:28:11.520Z
---

![](/emulators/computers/sharpx68000.svg){.align-center}

## Technical data

* **Manufacturer**: Sharp
* **Year of release**: 1987
* **OS**: Human68k
* **CPU**: Motorola 68000 @ 10 MHz
* **RAM**: 1-4 MB (expandable up to 12 MB)
* **VRAM**: 1056 KB
* **ROM**: 1 MB (128 KB BIOS, 768 KB Character Generator)
* **GPU**: Sharp-Hudson Custom Chipset
* **Sound chip**: Yamaha YM2151
* **Resolution**: 512x512 (65536 colors)

## Presentation

The **Sharp X68000**, commonly known as the **X68000** or **X68k**, is a personal computer from Sharp Corporation that was released in Japan starting in 1987. The first model used a Motorola 68000 microprocessor at 10 MHz, with 1 megabyte of RAM and no hard disk. The latest model, introduced in 1993, had a 25 MHz Motorola 68030, with 4 MB of RAM and an optional 80 MB SCSI hard drive. The base models had two 5"¼ floppy drives.

This computer runs on an operating system called Human68k, developed jointly by the manufacturer itself, namely Sharp and Hudson Soft. Human68k works on the same principle as MS-DOS, which is based on commands entered by the user. The executable files end with the extension `.X`. Three major versions of this operating system have been released.

Like Western personal computers such as the Amiga and Atari ST, the architecture of the X68000 brought it closer to the arcade systems of the time. As a result, a number of famous arcade games were developed on this computer, such as Bubble Bobble, Columns, Final Fight, Ys, Castlevania, Street Fighter, Ghosts'n Goblins, After Burner and Parodius Da!

The CPS-1 system is a stand-alone derivative of the X68000. Hence the almost pixel perfect versions of Final Fight and Ghouls'n Ghosts. Almost, because for example in Final Fight, the X68000 cannot display as many simultaneous sprites as the CPS1 version.

## Emulators

[Libretro PX68k](libretro-px68k)