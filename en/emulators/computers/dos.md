---
title: DOS
description: 
published: true
date: 2021-08-09T14:42:48.093Z
tags: dos, msdos, computers
editor: markdown
dateCreated: 2021-06-29T18:22:43.589Z
---

![](/emulators/computers/dos.svg){.align-center}

## Technical data

* **Manufacturer**: IBM
* **Year of release**: 1981

## Presentation

The **DOS** _(disk operating system)_ is generally called the **PC-DOS** operating system, as well as the **MS-DOS** variant sold by Microsoft for PC compatibles. There are later clones, such as **DR-DOS** from Digital Research and **FreeDOS**.

Until the early 1990s, DOS was the most used type of system on PC compatibles. This command line system was rudimentary: no multitasking, no virtual memory, management of the only 16-bit segmented mode of the x86 microprocessor.

There are other unrelated systems that contain the word DOS _(AMSDOS, AmigaDOS, Apple DOS, ProDOS, DOS on mainframe)_, but their names are overshadowed.

## Emulators

[DOSBox](dosbox)
[Libretro DOSBox_Pure](libretro-dosbox-pure)