---
title: SAM Coupé
description: 
published: true
date: 2021-08-09T15:00:13.424Z
tags: computers, sam, coupé
editor: markdown
dateCreated: 2021-07-28T21:21:39.391Z
---

![](/emulators/computers/samcoupe.svg){.align-center}

## Technical data

* **Developer**: Miles Gordon Technology
* **Manufacturer**: Miles Gordon Technology
* **OS**: SAM BASIC
* **Year of release**: 1989
* **Units sold**: 12000
* **CPU**: Zilog Z80B @ 6 MHz
* **Memory**: 256 KB/512 KB (4.5 MB max.)
* **Storage**: Tape recorder, 3.5" floppy disk
* **Graphics**: 4 Modes (256×192 / 512×192) and 128 colors palette

## Presentation

**SAM Coupe** is a British 8-bit personal computer released in 1989. It was originally manufactured by Miles Gordon Technology (MGT), located in Swansea, UK. The microprocessor is a Zilog Z80B clocked at 6 MHz. The production of this computer ended around 1995 and it was sold in 12 000 copies.

In France, the SAM Coupé was not officially imported. It was sold on the sly at Porte de Clignancourt under the name "Sam La Coupe".

The machine is built around the Z80B microprocessor running at 6 MHz, and contains an Application-specific integrated circuit (ASIC) which is comparable to the Uncommitted Logic Array of the Spectrum. The base model has 256 KB of RAM, expandable internally to 512 KB and 4 MB externally. The basic storage media is a cassette drive. It is possible to install one or two 3 1/2 inch floppy drives internally. Thanks to the Philips SAA1099 integrated circuit, there are 6 stereophonic channels on 8 octaves.

The machine has 32 Kb of ROM containing the boot code and a SAM BASIC interpreter written by Andrew Wright. There is no DOS included in the ROMs.

## Emulators

[SimCoupe](simcoupe)