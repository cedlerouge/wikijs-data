---
title: Apple 2
description: 
published: true
date: 2021-08-09T14:14:29.649Z
tags: computers, apple, apple-2
editor: markdown
dateCreated: 2021-07-28T19:07:08.638Z
---

![](/emulators/computers/apple2.svg){.align-center}

## Technical data

* **Manufacturer**: Apple
* **Year of release**: 1977
* **OS**: Integer BASIC
* **CPU**: MOS Technology 6502 @ 1 MHz
* **RAM**: 4KB to 64KB
* **ROM**: 8KB (Basic Integer)
* **Sound chip**: 1-bit speaker (built-in)
* **Resolution**:
  * Lo-res: 40x48, 16 colors
  * High-res: 280x192, 6 colors

## Presentation

The **Apple II** (sometimes spelled **Apple ]\[** or **Apple //**) is one of the first personal computers in the world to be manufactured on a large scale. Designed by Steve Wozniak, marketed on June 10, 1977 by Apple, it began its career with private individuals, but the release of the first spreadsheet, VisiCalc in 1979 allowed its entry in the professional world and a very important increase of its sales, making the sudden wealth of the Apple company at this time.

The Apple II evolved during the 1980s, and was sold until 1993. The Apple II had a primarily 8-bit architecture, and differs completely from the Apple Macintoshes introduced in 1984. The "II" part of the name was successively written using a multitude of graphic means and punctuation symbols. Thus the II and II+ were usually written "]\[" and "]\[+", the IIe and IIc were written "//e" and "//c", both on the manuals and the machines themselves.

It is the successor to the Apple I, which was a home-made machine sold to hobbyists. This machine was never produced in quantity, but paved the way for many of the features that made the Apple II a success.

## Emulators

[LinApple](linapple)
[GSPlus](./gsplus)