---
title: ScummVM
description: 
published: true
date: 2021-08-09T15:01:08.247Z
tags: scummvm, computers
editor: markdown
dateCreated: 2021-07-28T21:23:31.797Z
---

![](/emulators/computers/scummvm.svg){.align-center}

## Technical data

* **Original author**: Ludvig Strigeus
* **Year of release**: 2001

## Presentation

**ScummVM** is a virtual machine that allows games that use the SCUMM system (such as LucasArts or Humongous adventure games) to be played on platforms other than those originally supported by the game; in addition, the software supports the following non-SCUMM games: Simon the Sorcerer I and II, Beneath a Steel Sky, The Legend of Kyrandia, Flight of the Amazon Queen and Broken Sword 1 and 2. Since one of the goals of the program is to make it possible for a variety of users with different systems to play these classics, various versions of ScummVM supporting different operating systems are available: Microsoft Windows, Atari, WinCE, Pocket PC, Mac OS X, Linux, Android, Palm OS, HP webOS, Open webOS, AmigaOS, BeOS, Solaris, Dreamcast, MorphOS, Irix, PSP, PS2, GP32, Wii, DS and more. ScummVM is free software, under the terms of the GNU GPL.

## Emulators

[ScummVM](scummvm)