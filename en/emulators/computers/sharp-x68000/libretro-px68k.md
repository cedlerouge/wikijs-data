---
title: Libretro PX68k
description: 
published: true
date: 2021-08-09T15:03:11.266Z
tags: libretro, x68000, px68k
editor: markdown
dateCreated: 2021-07-29T17:38:28.195Z
---



## ![](/emulators/license.svg) License

This core is under [**GPLv2**](https://github.com/libretro/px68k-libretro/blob/master/COPYING) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features



## ![](/emulators/bios.svg) BIOS

### List od mandatory bios

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| iplrom.dat | BIOS X68000 | 7fd4caabac1d9169e289f0f7bbf71d8e | ✅ |
| cgrom.dat | Font file | cb0a5cfcf7247a7eab74bb2716260269 | ✅ |

### Location

Put the bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 keropi
┃ ┃ ┃ ┃ ┣ 🗒 **iplrom.dat**
┃ ┃ ┃ ┃ ┣ 🗒 **cgrom.dat**

### List of optional bios

| File | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| iplrom30.dat | BIOS X68000 #2 | f373003710ab4322642f527f567e020a | ⚠️ |
| iplromco.dat | BIOS X68000 #3 | cc78d4f4900f622bd6de1aed7f52592f | ⚠️ |
| iplromxv.dat | BIOS X68000 #4 | 0617321daa182c3f3d6f41fd02fb3275 | ⚠️ |

⚠️ Provided but it's not the correct bios.

### Location

Put the bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 keropi
┃ ┃ ┃ ┃ ┣ 🗒 **iplrom30.dat**
┃ ┃ ┃ ┃ ┣ 🗒 **iplromco.dat**
┃ ┃ ┃ ┃ ┣ 🗒 **iplromxv.dat**

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .dim
* .img
* .d88
* .88d
* .hdm
* .dup
* .2hd
* .xdf
* .hdf
* .cmd
* .m3u
* .zip
* .7z

This system supports compressed roms in .zip/.7z format. However, be careful, it is only an archive.

The files contained in the .zip/.7z must correspond to the extensions mentioned above.
Moreover, each .zip/.7z file must contain only one compressed rom.

### Location

Put the roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 x68000
┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**

>The roms using **TOSEC** format are highly recommended.
{.is-success}

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/px68k-libretro/](https://github.com/libretro/px68k-libretro/)
* **Libretro documentation**: [https://docs.libretro.com/library/px68k/](https://docs.libretro.com/library/px68k/)