---
title: Sharp X1
description: 
published: true
date: 2021-08-09T15:01:52.500Z
tags: computers, sharp, x1
editor: markdown
dateCreated: 2021-07-28T21:25:58.894Z
---

![](/emulators/computers/sharpx1.svg){.align-center}

## Technical data

* **Manufacturer**: Sharp Corporation
* **Year of release**: 1982
* **OS**: CP/M
* **CPU**: Sharp Z80 A @ 4 MHz
* **RAM**: 64 KB
* **VRAM**: 4 KB (up to 48 KB)
* **ROM**: 6 KB
* **GPU**: Sharp-Hudson Custom Chipset
* **Sound chip**: General Instrument AY-3-8910 or Yamaha YM2149
* **Display**: 320 x 200 / 640 x 200 (8 colors)

## Presentation

The **Sharp X1** is the name given to a series of Japanese microcomputers created by Sharp Corporation from 1982 to 1988. They all feature a Zilog Z80 processor.

Although it was Sharp's personal computer division that created the MZ series (such as the MZ-80K), this time it was the television design division that was responsible for this new series. At the time when the first X1 was released, most personal computers had read-only memory programmed in BASIC. However, the X1 did not use BASIC but loaded the interpreter directly from an audio cassette. In addition, this concept allowed access to RAM space via data logging. The design of the X1 was also more stylish than its competitors of the same time and a choice of different colors was available.

The computer monitor had a tuner, and a computer screen could be superimposed on the television. All television functions could be controlled from a computer program. The entire video memory was located in the input/output area. All of these elements made the X1 a quality gaming platform for its time.

As Sharp struggled to sell its X1, NEC's PC-88 gained notoriety in the Japanese market. In 1984, Sharp made the X1 Turbo series with a higher graphics resolution (640x400, while the original X1 was 640x200). This series had many improvements but the clock frequency stagnated at 4 MHz. In 1986, Sharp released a new series, the X1 Turbo Z with a range of 4096 colors. Finally, in 1987, a clone of the X1 was created as the last series. It integrated the features of the PC-Engine console.

The Sharp X1 was then replaced by the Sharp X68000 series.

## Emulators

[Libretro x1](libretro-x1)