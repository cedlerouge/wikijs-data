---
title: Libretro vice_x128
description: 
published: true
date: 2021-08-09T14:21:03.590Z
tags: libretro, commodore-64, c64, vice_x128
editor: markdown
dateCreated: 2021-07-29T15:59:00.362Z
---



## ![](/emulators/license.svg) License

This core is under [**GPLv2**](https://github.com/libretro/vice-libretro/blob/master/COPYING).

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features



## ![](/emulators/bios.svg) BIOS

### List of optional bios

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| JiffyDOS_C64.bin | JiffyDOS C64 Kernal | be09394f0576cf81fa8bacf634daf9a2 | ⚠️ |
| JiffyDOS_C128.bin | JiffyDOS C128 Kernal | cbbd1bbcb5e4fd8046b6030ab71fc021 | ⚠️ |
| JiffyDOS_C1541-II.bin | JiffyDOS 1541 drive BIOS | 1b1e985ea5325a1f46eb7fd9681707bf | ⚠️ |
| JiffyDOS_1571_repl310654.bin | JiffyDOS 1571 drive BIOS | 41c6cc528e9515ffd0ed9b180f8467c0 | ⚠️ |
| JiffyDOS_1581.bin | JiffyDOS 1581 drive BIOS | 20b6885c6dc2d42c38754a365b043d71 | ⚠️ |

⚠️ Provided but it's not the correct bios.

### Location

Put the bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 vice
┃ ┃ ┃ ┃ ┣ 🗒 **JiffyDOS_C64.bin**
┃ ┃ ┃ ┃ ┣ 🗒 **JiffyDOS_C128.bin**
┃ ┃ ┃ ┃ ┣ 🗒 **JiffyDOS_C1541-II.bin**
┃ ┃ ┃ ┃ ┣ 🗒 **JiffyDOS_1571_repl310654.bin**
┃ ┃ ┃ ┃ ┣ 🗒 **JiffyDOS_1581.bin**

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .crt
* .d71
* .d81
* .g64
* .nbz
* .nib
* .prg
* .t64
* .tap
* .zip
* .7z

This system supports compressed roms in .zip/.7z format. However, be careful, it is only an archive.

The files contained in the .zip/.7z must correspond to the extensions mentioned above.
Moreover, each .zip/.7z file must contain only one compressed rom.

### Location

Put the roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 c64
┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**

>The roms using **No-Intro** format are highly recommended.
{.is-success}

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/vice-libretro/](https://github.com/libretro/vice-libretro/)
* **Libretro documentation**: [https://docs.libretro.com/library/vice/](https://docs.libretro.com/library/vice/)