---
title: Libretro vice_xscpu64
description: 
published: true
date: 2021-08-09T14:40:01.204Z
tags: libretro, commodore-64, c64, vice, xscpu64
editor: markdown
dateCreated: 2021-07-29T16:19:39.574Z
---



## ![](/emulators/license.svg) License

This core is under [**GPLv2**](https://github.com/libretro/vice-libretro/blob/master/COPYING) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features



## ![](/emulators/bios.svg) BIOS

### List of optional bios

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| scpu-dos-1.4.bin | CMD SuperCPU Kernal 1.4 | cda2fcd2e1f0412029383e51dd472095 | ⚠️ |
| scpu-dos-2.04.bin | CMD SuperCPU Kernal 2.04 | b2869f8678b8b274227f35aad26ba509 | ⚠️ |

⚠️ Provided but it's not the correct bios.

### Location

Put the bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 vice
┃ ┃ ┃ ┃ ┣ 📁 SCPU64
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **scpu-dos-1.4.bin**
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **scpu-dos-2.04.bin**

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .crt
* .d64
* .d71
* .d81
* .g64
* .nbz
* .nib
* .p00
* .prg
* .t64
* .tap
* .z64
* .zip
* .7z

This system supports compressed roms in .zip/.7z format. However, be careful, it is only an archive.

The files contained in the .zip/.7z must correspond to the extensions mentioned above.
Moreover, each .zip/.7z file must contain only one compressed rom.

### Location

Put the roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 c64
┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**

>The roms using **No-Intro** format are highly recommended.
{.is-success}

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/vice-libretro/](https://github.com/libretro/vice-libretro/)
* **Libretro documentation**: [https://docs.libretro.com/library/vice/](https://docs.libretro.com/library/vice/)