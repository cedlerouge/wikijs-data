---
title: Amiga 1200
description: 
published: true
date: 2021-08-09T13:45:08.766Z
tags: amiga-1200, amiga, computers, 1200, commodore
editor: markdown
dateCreated: 2021-07-28T18:47:39.626Z
---

![](/emulators/computers/amiga1200.svg){.align-center}

## Technical data

* **Manufacturer**: Commodore
* **Year of release**: 1992
* **OS**: AmigaOS 3.0/3.1
* **CPU**: Motorola 68EC020 @ 14.32 MHz
* **RAM**: 2 MB
* **ROM**: 512 KB Kickstart 3.0-3.1 ROM 
* **GPU**: Advanced Graphics Architecture
* **Sound chip**: 4x 8-bit PCM channels
* **Resolution**: 320x200 à 1504x484 (16.8 million colors)

## Presentation

The **Amiga 1200**, or **A1200**, was Commodore International's third-generation Amiga microcomputer, aimed at the family market. It was released in October 1992, at a price of £399 in the UK and $599 in the US.

Like its predecessors, the Amiga 500 and the Amiga 600 (whose design it is based on, with the addition of a numeric keypad), it was conceived as an "all-in-one" computer, incorporating processors, keyboard and floppy drive in a single unit. It was sold with 2 Mio of memory, the third generation of Amiga graphics chipset (AGA), and the AmigaOS 3.0. It uses a Motorola MC68EC020 processor running at 14 MHz. As an extension, the Amiga 1200 has a memory/CPU slot and a PCMCIA Type II1 port.

As the memory is shared between the main processor and the video and sound coprocessors, extending the memory beyond 2 Mio increases the performance of the machine a lot. Several third-party manufacturers offered gas pedals containing 68020, 68030, 68040, 68060 and, later, PowerPC processors, which significantly increase the power of the machine and sometimes require the Amiga to be remounted in a tower, in order to avoid overheating the original motherboard.

## Emulators

[Amiberry](amiberry)
[Libretro PUAE](libretro-puae)