---
title: Oricutron
description: 
published: true
date: 2021-08-09T14:54:51.157Z
tags: oric, oric-atmos, oricutron
editor: markdown
dateCreated: 2021-07-29T17:00:27.938Z
---



## ![](/emulators/license.svg) License

This core is under [**GPLv2**](https://github.com/pete-gordon/oricutron/issues/159#issuecomment-749596344) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features



## ![](/emulators/bios.svg) BIOS

### List of mandatory bios

You will find the bios with the name indicated in the column **Description** that you will have to rename with the name indicated in the column **Filename**.

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| basic11b.rom | - | a330779c42ad7d0c4ac6ef9e92788ec6 | ❌ |
| basic10.rom | - | ebe418ec8a6c85d5ac32956c9a96c179 | ❌ |
| jasmin.rom | - | 5136f764a7dbd1352519351fbb53a9f3 | ❌ |
| microdis.rom | - | df864344d2a2091c3f952bd1c5ce1707 | ❌ |
| pravetzt.rom | - | 8712a22e7e078de3343667d9fc1f2390 | ❌ |
| teleass.rom | - | 2324c9cc227c1327a72a667c97ed2990 | ❌ |
| hyperbas.rom | - | 364bf095e0dc4222d75354d50b8cddfc | ❌ |
| telmon24.rom | - | 9a432244d9ee4a49e8ddcde64af94e05 | ❌ |

### Location

Put the bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 oricutron
┃ ┃ ┃ ┃ ┣ 🗒 **basic11b.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **basic10.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **jasmin.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **microdis.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **pravetzt.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **teleass.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **hyperbas.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **telmon24.rom**

### List of optional bios

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| bd500.rom | - | 2b6498fd29a0adbf1c529762c02c33ab | ❌ |
| 8dos2.rom | - | 5f3cd5a4307fed7a9dfe8faa4c044273 | ❌ |

### Location

Put the bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 oricutron
┃ ┃ ┃ ┃ ┣ 🗒 **bd500.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **8dos2.rom**

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .dsk
* .tap

### Location

Put the roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 oricatmos
┃ ┃ ┃ ┃ ┣ 🗒 **game.dsk**

>The roms using **TOSEC** format are highly recommended.
{.is-success}

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/pete-gordon/oricutron/](https://github.com/pete-gordon/oricutron/)
* **Documentation**: [http://www.petergordon.org.uk/oricutron/](http://www.petergordon.org.uk/oricutron/)