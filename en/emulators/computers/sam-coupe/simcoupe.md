---
title: SimCoupe
description: 
published: true
date: 2021-08-09T15:00:32.964Z
tags: sam, coupé, simcoupe
editor: markdown
dateCreated: 2021-07-29T17:16:35.902Z
---



## ![](/emulators/license.svg) License

This core is under [**GPLv2**](https://gitlab.com/recalbox/packages/standalone/simcoupe/-/blob/dev/License.txt) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features



## ![](/emulators/bios.svg) BIOS

>**No bios is required.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .dsk
* .sad
* .mgt
* .sdf
* .td0
* .sbt
* .cpm

### Location

Put the roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 samcoupe
┃ ┃ ┃ ┃ ┣ 🗒 **game.dsk**

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

SimCoupe can use the following disk format software:

`.MGT` - Simple sector dump of +D/SAM disks: 2 sides, 80 tracks per side, 10 sectors per track, 512 bytes per sector = 819200 bytes. Older images in this format may have the file extension .dsk. This is the preferred format for SAM disks in the normal format, and is compatible with Linux /dev/fd0u800 devices.

`.SAD` - SAM disk format, created by Aley Keprt. It is also a sector based dump but with a 22 byte header file allowing geometric adjustments to tracks per disk, tracks per side, sectors per track and bytes per sector. Normal SAM disks that are stored in SAD format contain 819222 bytes, but a difference in track order avoids removing the 22 byte header to give an equivalent MGT image. Version 2 of the SAD images are the same format but compressed with gzip.

`DSK` - Extended DSK (EDSK) images, originally used with Amstrad CPC and Spectum +3 media. A flexible format capable of representing all existing SAM disks, and also the preferred format of the worldofsam.org archive site. The size of the images is proportional to the geometry of the disks, with a normal SAM disk around 840K.

`.SBT` - Sam bootable files, created by Andrew Collier. These files are self-booting files made to be copied to an empty SAM disk when booted. Although they are not technically bootable images, SimCoupe treats them as such (read only).

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://gitlab.com/recalbox/packages/standalone/simcoupe/](https://gitlab.com/recalbox/packages/standalone/simcoupe/)