---
title: BBC Micro
description: 
published: false
date: 2021-09-23T23:08:52.428Z
tags: 7.3+, computers, bbc micro
editor: markdown
dateCreated: 2021-09-23T21:51:55.438Z
---

![](/emulators/computers/bbcmicro.svg){.align-center}

## Technical data

* **Manufacturer**: Acorn Computers
* **Year of release**: 1981
* **OS**: Acorn MOS
* **CPU**: MOS Technology 6502/6512 @ 2MHz
* **RAM**: 64KB - 128 KB
* **Drive**: Cassette tape, Floppy disk 5¼" or 3½", Hard disk also known as 'Winchester' (Rare), Laserdisc (BBC Domesday Project)
* **Sound chip**: Texas Instruments SN76489, 4 channels, mono
* **Resolution**: 640x256, 8 colours (various framebuffer modes)

## Presentation

The British Broadcasting Corporation Microcomputer System, or BBC Micro, is a series of microcomputers and associated peripherals designed and built by Acorn Computers in the 1980s for the BBC Computer Literacy Project. Designed with an emphasis on education, it was notable for its ruggedness, expandability, and the quality of its operating system. An accompanying 1982 television series, The Computer Programme, featuring Chris Serle learning to use the machine, was broadcast on BBC2.

After the Literacy Project's call for bids for a computer to accompany the TV programmes and literature, Acorn won the contract with the Proton, a successor of its Atom computer prototyped at short notice. Renamed the BBC Micro, the system was adopted by most schools in the United Kingdom, changing Acorn's fortunes. It was also successful as a home computer in the UK, despite its high cost. Acorn later employed the machine to simulate and develop the ARM architecture.

While nine models were eventually produced with the BBC brand, the phrase "BBC Micro" is usually used colloquially to refer to the first six (Model A, B, B+64, B+128, Master 128, and Master Compact); subsequent BBC models are considered part of Acorn's Archimedes series.

## Emulators

[BeebEm](beebem)