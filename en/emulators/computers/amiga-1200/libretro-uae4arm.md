---
title: Libretro UAE4ARM
description: 
published: false
date: 2021-10-03T15:41:44.001Z
tags: libretro, 7.3+, amiga-1200, amiga, 1200, uae4arm
editor: markdown
dateCreated: 2021-08-02T18:36:39.371Z
---



## ![](/emulators/license.svg) License

This core is under [**GPLv2**](https://github.com/libretro/PUAE/blob/master/COPYING) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ❌ | ❌ |

## ![](/emulators/features.png) Features



## ![](/emulators/bios.svg) BIOS

### List of mandatory bios

You will find the bios with the name indicated in the column Description that you will have to rename with the name indicated in the column Filename.

| filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| kick40068.A1200 | Kickstart v3.1 r40.068 (1993-12)(Commodore)(A1200)\[!\].rom | 646773759326fbac3b2311fd8c8793ee | ❌ |

### Location

Put the bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 🗒 **kick40068.A1200.rom**

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .adf
* .adz
* .ipf
* .lha
* .lhz
* .lzx
* .zip
* .rp9
* .dms
* .fdi
* .hdf
* .hdz
* .m3u

### Location

Put the roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 amiga1200
┃ ┃ ┃ ┃ ┣ 🗒 **game.ipf**

>The roms using **TOSEC** format are highly recommended.
{.is-success}

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/Chips-fr/uae4arm-rpi/](https://github.com/Chips-fr/uae4arm-rpi/)