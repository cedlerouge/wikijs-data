---
title: Libretro NP2Kai
description: 
published: true
date: 2021-08-09T14:58:25.514Z
tags: libretro, pc-98, np2kai
editor: markdown
dateCreated: 2021-07-29T17:13:51.576Z
---



## ![](/emulators/license.svg) License

This core is under [**MIT**](https://github.com/AZO234/NP2kai/blob/master/LICENSE) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features



## ![](/emulators/bios.svg) BIOS

### List of mandatory bios

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| bios.rom | - | 052f5f6010877522f78803498a625727 0c052b1f1bfd0ece11286d2ff45d296f 36293e23fe0c86cb115d59b653c13876 3af0ae018c5710eec6e2891064814138 50274bb5dcb707e4450011b09accffcb c70ee9df11794bd5cc8aadb3721b4a03 cd237e16e7e77c06bb58540e9e9fca68 e246140dec5124c5e404869a84caefce | ❌ |
| font.bmp | Text display | 0c8624e9ced7cca8769e5c7b0dd4279b 7da1e5b7c482d4108d22a5b09631d967 | ❌ |
| font.rom | Alternative font | 38d32748ae49d1815b0614970849fd40 4133b0be0d470920da60b9ed28d2614f 693fd1da3239d4bbeafc77d211718fc5 829b963e21334e0fc2092ebd58f2ab4a add4a225048c85ca2bc588696c6ecdc5 ca87908a99ea423093f6d497fc367f7d fd0b856899aec843cbe69d2940df547f | ❌ |
| itf.rom | - | 1d295699ffeab0f0e24e09381299259d 72ea51443070f0e9212bfc9b793ee28e a13d96da03a28af8418d7f86ab951f1a b49ea39a1f730f1c966babc11961dc9a e9fc3890963b12cf15d0a2eea5815b72 | ❌ |
| sound.rom | - | 42c271f8b720e796a484cc1165ff4914 524473c1a5a03b17e21d86a0408ff827 a77fc2bc7c696dd68dba18e02f89d386 caf90f22197aed6f14c471c21e64658d | ❌ |

### Location

Put the bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 np2kai
┃ ┃ ┃ ┃ ┣ 🗒 **bios.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **font.bmp**
┃ ┃ ┃ ┃ ┣ 🗒 **font.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **itf.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **sound.rom**

### List of optional bios

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| 2608_hd.wav | YM2608 RYTHM sample | 9c6637930b1779abe00b8b63e4e41f50 d94546e70f17fd899be8df3544ab6cbb | ⚠️ |
| 2608_hh.wav | YM2608 RYTHM sample | 73548a1391631ff54a1f7c838d67917e 08c54a0c1f774a5538a848a6665a34b4 | ⚠️ |
| 2608_rim.wav | YM2608 RYTHM sample | 43d54b3e05c081fa280c9bace3af1043 465ea0768b27da404aec45dfc501404b | ⚠️ |
| 2608_sd.wav | YM2608 RYTHM sample | 08124ccb84a9f65e2affc29581e690c9 d71004351c8bbfdad53b18222c061d49 | ⚠️ |
| 2608_tom.wav | YM2608 RYTHM sample | faed5664a2dd8b1b2308e8a50ac25ea 96a4ead13f364734f79b0c58af2f0e1f | ⚠️ |
| 2608_top.wav | YM2608 RYTHM sample | 3721ace646ffd56439aebbb2154e9263 593cff6597ab9380d822b8f824fd2c28 | ⚠️ |

⚠️ Provided but it's not the correct bios.

### Location

Put the bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 np2kai
┃ ┃ ┃ ┃ ┣ 🗒 **2608_hd.wav**
┃ ┃ ┃ ┃ ┣ 🗒 **2608_hh.wav**
┃ ┃ ┃ ┃ ┣ 🗒 **2608_rim.wav**
┃ ┃ ┃ ┃ ┣ 🗒 **2608_sd.wav**
┃ ┃ ┃ ┃ ┣ 🗒 **2608_tom.wav**
┃ ┃ ┃ ┃ ┣ 🗒 **2608_top.wav**

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .d98
* .98d
* .dcp
* .fdi
* .fdd
* .nfd
* .2hd
* .tfd
* .d88
* .88d
* .hdm
* .xdf
* .dup
* .cmd
* .hdi
* .thd
* .nhd
* .hdd
* .hdn
* .m3u
* .zip
* .7z

This system supports compressed roms in .zip/.7z format. However, be careful, it is only an archive.

The files contained in the .zip/.7z must correspond to the extensions mentioned above.
Moreover, each .zip/.7z file must contain only one compressed rom.

### Location

Put the roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 pc98
┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**

>The roms using **TOSEC** format are highly recommended.
{.is-success}

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/AZO234/NP2kai/](https://github.com/AZO234/NP2kai/)
* **Libretro documentation**: [https://docs.libretro.com/library/neko_project_ii_kai/](https://docs.libretro.com/library/neko_project_ii_kai/)