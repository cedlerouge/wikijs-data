---
title: TI-99/Sim
description: 
published: false
date: 2021-09-23T23:11:06.323Z
tags: 7.3+, ti-99/4a, ti-99/sim
editor: markdown
dateCreated: 2021-09-12T08:06:12.578Z
---



## ![](/emulators/license.svg) License

This core is under [**GPL-2.0**](https://gitlab.com/recalbox/packages/standalone/ti99sim/-/blob/master/doc/COPYING) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |


## ![](/emulators/bios.svg) BIOS

### List of mandatory bios

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| TI-994A.ctg | System cartridge | 412ecbf991edcb68edd0e76c2caa4a59 | ❌ |

### Location

Put the bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 ti994a
┃ ┃ ┃ ┃ ┣ 🗒 **TI-994A.ctg**

### List of optionnal bios

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| spchrom.bin | Speech synthesis | 7adcaf64272248f7a7161cfc02fd5b3f | ❌ |
| ti-disk.ctg | Disk Operating System | 04714f43347cefb2a051a77116344b3f | ❌ |

### Location

Put the bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 ti994a
┃ ┃ ┃ ┃ ┣ 🗒 **spchrom.bin**
┃ ┃ ┃ ┃ ┣ 🗒 **ti-disk.ctg**

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .ctg

### Location

Put the roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 ti994a
┃ ┃ ┃ ┃ ┣ 🗒 **game.ctg**

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://gitlab.com/recalbox/packages/standalone/ti99sim/](https://gitlab.com/recalbox/packages/standalone/ti99sim/)
* **Official website**: [https://www.mrousseau.org/programs/ti99sim/](https://www.mrousseau.org/programs/ti99sim/)