---
title: Apple 2GS
description: 
published: true
date: 2021-08-09T14:17:07.005Z
tags: computers, apple, apple-2gs
editor: markdown
dateCreated: 2021-07-28T19:10:54.415Z
---

![](/emulators/computers/apple2gs.svg){.align-center}

## Technical data

* **Manufacturer**: Apple
* **Year of release**: 1986
* **OS**: Apple ProDOS, Apple GS/OS, GNO/ME
* **CPU**: Western Design Center 65C816 16-bit @ 2.8 MHz
* **RAM**: 256 kB or 1 MB (expandable up to 8 MB)
* **ROM**: 128 KB expandable up to 1 MB
* **Sound chip**: Ensoniq ES5503 DOC 8-bit wavetable synthesis sound chip, 32-channels, stereo
* **Display**: VGC 12-bpp palette (4096 colors), 320×200, 640×200

## Presentation

The **Apple IIGS** featured a 2.8 MHz 65C816 processor with 16-bit and 24-bit addressing registers, an Ensoniq sound processor, more memory, better colors, more peripherals (with a swappable controller between IIe and IIc board models), some of which are compatible with those of the Macintosh (keyboard, mouse, LocalTalk network adapter) and GS/OS, an operating system derived from Mac OS. The Applied Engineering company developed several expansion cards for the Apple II range, including the PC Transporter, allowing a PC XT (NEC V30 processor, 768 kB RAM, CGA graphics mode) to be added under the hood. Note that Apple marketed a kit allowing to transform an Apple IIe into an Apple IIGS.

## Emulators

[GSPlus](gsplus)