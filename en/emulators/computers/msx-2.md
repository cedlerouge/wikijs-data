---
title: MSX 2
description: 
published: true
date: 2021-08-09T14:51:04.077Z
tags: computers, msx2
editor: markdown
dateCreated: 2021-07-28T20:51:06.170Z
---

![](/emulators/computers/msx2.svg){.align-center}

## Technical data

* **Manufacturer**: Various
* **Year of release**: 1985
* **OS**: MSX BASIC V3.0 (16 kB)
* **CPU**: Zilog Z80 compatible @ 3.58 MHz
* **RAM**: 64 KB
* **VRAM**: 128 KB
* **ROM**: 64 KB
* **GPU** : Yamaha V9958 (aka MSX-Vide)
* **Sound chip**: Yamaha YM2149 (PSG)
* **Resolution**: 256×212 (19268 simultaneous colors)

## Presentation

The **MSX** is a home (consumer) microcomputer standard of Japanese origin, dating back to the 1980s. Unlike most computers of that time, MSX was produced by various manufacturers. They were compatible with each other, both for hardware and software. Several versions of the standard followed one another.

Often interpreted as **M**icro**S**oft e**X**tension, the acronym MSX would mean **M**achines with **S**oftware e**X**changeability according to Kazuhiko Nishi, initiator of the project. The standard was created in 1983 and produced by many Japanese companies such as Canon, Casio, Panasonic, Sanyo, Sony, Toshiba or Yashica. The Japanese company Yamaha has produced MSX for music, including an MSX1 version with an eight-voice sound processor and DIN connectors to the MIDI standard. In Europe, Philips or Schneider have been present on the MSX scene.

The MSX standard was invented following a call for tenders from METI, which wanted computers to be compatible with each other (at the time, each brand/model of computer had its own language and its own operating system). Microsoft answered the call to develop the software layers: MSX-Basic interpreted programming language integrated as standard in a ROM and MSX-DOS 1 operating system. Later, MSX-DOS 2 was developed by ASCII, adding among other things the notions of directories (and sub-directories), SCSI hard disk partitions.

MSX had "national" particularities: the version marketed in Japan was equipped with a QWERTY/Kanji keyboard. In France, most had an AZERTY keyboard. A version supporting the Arabic language was marketed by the company Al Alamia. This version was called Sakhr ("rock" in Arabic).

The Sony MSX2 already had a mouse and a graphic desktop (interface).

There have been four generations of MSXs: the MSX (or MSX1), the MSX2, the MSX2+ and the MSX turbo R. They were very popular in Japan and Europe, especially during the MSX1 era. The MSX2+ is marketed in Europe thanks to a few importers in France, Spain and the Netherlands. The MSX turbo R is even rarer in Europe, because it is produced only by Panasonic and reserved exclusively for the Japanese market. However, there is an extension for MSX1 allowing to use it as an MSX2, the NEOS MA-20(v).

## Emulators

[Libretro blueMSX](libretro-bluemsx)
[Libretro FMSX](libretro-fmsx)