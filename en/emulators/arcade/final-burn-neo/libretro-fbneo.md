---
title: Libretro FBNeo
description: 
published: true
date: 2021-08-09T07:29:21.203Z
tags: libretro, fbneo, finalburn, neo
editor: markdown
dateCreated: 2021-07-05T21:55:46.002Z
---

**FinalBurn Neo**, an emulator for arcade games and some consoles. It is based on the FinalBurn emulators and the old versions of [MAME](https://www.mamedev.org/) (emulating less arcade cabinets) but it works on the same principles.

FB Neo (a.k.a. FinalBurn Neo) is an arcade emulator that supports the following platforms:

* Capcom CPS-1
* Capcom CPS-2
* Capcom CPS-3
* Cave
* Games based on Data East DEC-0, DEC-8, Cassette System, DECO IC16 and DECO-32
* Hardware based on Galaxian
* Irem M62, M63, M72, M90, M92 and M107 hardware
* Kaneko 16
* Konami
* Namco Mappy, System 86, System 1 & 2 and others
* Neo-Geo
* NMK16
* Pacman-based hardware
* PGM
* Psikyo 68EC020 and SH-2 based hardware
* Sega System 1, System 16 (and similar), System 18, X-Board and Y-Board
* Seta-Sammy-Visco (SSV) based hardware
* Super Kaneko Nova System
* Toaplan 1
* Toaplan 2
* Taito F2, F3, X, Z and others
* Various drivers for other hardware
* FB Neo also contains working drivers for the following systems:
  * Sega Megadrive/Genesis
  * ColecoVision
  * Sega SG-1000
  * Sega Master System
  * Game Gear
  * MSX-1
  * ZX Spectrum
  * PC-Engine
  * TurboGrafx 16
  * SuperGrafx
  * Fairchild Channel F.

## ![](/emulators/license.svg) License

This core is under [**non-commercial**](https://github.com/finalburnneo/FBNeo/blob/master/src/license.txt) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Saves | ✔ |
| States | ✔ |
| Rewind | ✔ |
| Netplay | ✔ |
| RetroAchievements | ✔ |
| RetroArch Cheats | ✔ |
| Native Cheats | ✔ |
| Controllers | ✔ |
| Multi-Mouse | ✔ |
| Subsystem | ✔ |

## ![](/emulators/bios.svg) Bios

### Bios list

Depending on the games, some bios will be necessary and will be placed in the same directory as the games.

### Location

Put your bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 fbneo
┃ ┃ ┃ ┃ ┣ 🗒 **bios.zip**

## ![](/emulators/roms.png) Roms

### Supported extensions

All FinalBurn Neo games use the following game formats:

* .fba
* .zip
* .7z

### Location

Put your roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 fbneo
┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**

>For more information about Roma, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options



## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/FBNeo/tree/master](https://github.com/libretro/FBNeo/tree/master)
* **Libretro documentation**: [https://docs.libretro.com/library/fbneo/](https://docs.libretro.com/library/fbneo/)
* **Official website**: [https://neo-source.com/](https://neo-source.com/)
* **Core Wiki**: [http://emulation.gametechwiki.com/index.php/FinalBurn_Neo](http://emulation.gametechwiki.com/index.php/FinalBurn_Neo)
* **Officiel wiki**: [https://github.com/finalburnneo/FBNeo/wiki](https://github.com/finalburnneo/FBNeo/wiki)