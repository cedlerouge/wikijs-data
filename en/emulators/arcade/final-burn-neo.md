---
title: FBNeo
description: 
published: true
date: 2021-08-09T07:28:48.975Z
tags: arcade, finalburn, neo
editor: markdown
dateCreated: 2021-07-04T18:32:51.535Z
---

![](/emulators/arcade/fbneo.svg){.align-center}

## History

Final Burn Alpha (FB Alpha or FBA) is an arcade game emulator. It takes its name from the game "After Burner" which was the only game emulated at the very beginning of its creation; it is originally based on the FinalBurn emulator, started in August 2000 by Dave3 (also known as FinalDave).

The project fizzled out and from the ashes of it was born FinalBurn Alpha (now buried since summer 2019). It was reborn one last time under the acronym FBNeo or FinalBurn Neo, after the sudden and recent termination of the rights to FBA by one of its main members: _Barry Harris_.
It currently includes more than thirty emulated systems/consoles and no less than 10,000 games.

## Emulators

[Libretro FBNeo](libretro-fbneo)
[PiFBA](pifba)