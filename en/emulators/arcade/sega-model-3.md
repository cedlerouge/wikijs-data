---
title: Sega Model 3
description: 
published: true
date: 2021-08-09T07:32:35.455Z
tags: arcade, model-3
editor: markdown
dateCreated: 2021-07-04T18:42:03.285Z
---

![](/emulators/arcade/model3.svg){.align-center}

## Technical data

* **CPU**: IBM 32bit RISC PowerPC 603ev 
  * 66 MHz (Version 1.0)
  * 100 MHz (Version 1.5)
  * 166 MHz (Version 2.0 and 2.1)
* **GPU**: 2 × Lockheed Martin Real3D/Pro-1000
* **Rendering**: 60 million pixels
* **Resolution**: 496 x 384
* **Sound Processor**: Motorola 68EC000 clocked at 12 MH, 2 × Yamaha SCSP/YM-292F 128-step DSP
* **MIDI interface:**
  * 64 voices, 4 channels, maximum 16.5 MB ROM
  * 64 PCM channels
* **RAM**: 1 Mb
* **Optional sound card** :
  * **DSB1:**
    * **Sound Processor** : Zilog Z80
    * **Audio chip** : NEC uD65654GF102
  * **DSB2:**
    * **CPU**: Motorola 68000
    * **Audio chip** : NEC uD65654GF102

## Presentation

With **Model 3**, Sega decided to design a new system from the ground up. Again, they chose the company they had established a good relationship with, now called the Real3D division of Lockheed Martin.

They were brought in to help develop the brand new graphics subsystem, which turned out to be the dual Real3D/Pro-1000 Chipset, surprisingly powerful for the time.

## Emulators

[Supermodel](supermodel)