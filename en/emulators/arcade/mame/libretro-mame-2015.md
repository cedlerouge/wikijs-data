---
title: Libretro MAME 2015
description: 
published: true
date: 2021-08-09T07:37:05.288Z
tags: libretro, mame, mame2015
editor: markdown
dateCreated: 2021-07-05T23:29:52.097Z
---



## ![](/emulators/license.svg) License

This core is under **MAME non-commercial** license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Saves |  |
| States |  |
| Rewind |  |
| Netplay |  |
| RetroAchievements |  |
| RetroArch Cheats |  |
| Native Cheats |  |
| Controllers |  |
| Multi-Mouse |  |
| Rumble |  |
| Sensors |  |
| Camera |  |
| Location |  |
| Subsystem |  |

## ![](/emulators/bios.svg) BIOS

BIOS romsets are not required when using "Full Non-Merged" arcade romsets. For "Split" and "Non-Merged" romsets, place the BIOS in the same directory as the game romset.

>**Note**:  
>Place the bios in the directory: `/recalbox/share/roms/mame/`   
>or in its subdirectory if you use several cores of Mame: `/recalbox/share/roms/mame/Mame 2015/`
{.is-warning}

## ![](/emulators/roms.png) Roms

* Based on romset: MAME 0.160 (March 2015)
* Size: 174gb
* Emulated romsets: 30907 (including clones, etc...)
* Active Sets: 30907
* Parents: 4535
* Clones: 20715
* Others: 5588
* BIOS: 69
* CHDs: 817
* Samples: 9044
* DatFile: [mame2015-xml.zip](https://github.com/libretro/mame2015-libretro/blob/master/metadata/mame2015-xml.zip)

### Location

Put your roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 mame
┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**
┃ ┃ ┃ ┃ ┣ 📁 **game**
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **game.chd**

BIOS romsets are not necessary when you want to use a subdirectory (useful if you want to have a mame set for another core).

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 mame
┃ ┃ ┃ ┃ ┣ 📁 Mame 2015
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**
┃ ┃ ┃ ┃ ┃ ┣ 📁 **game**
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 **game.chd**

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

## ![](/emulators/external-links.png) External links

* **Progettosnaps**: [https://www.progettosnaps.net/](https://www.progettosnaps.net/)
* **Source code used**: [https://github.com/libretro/mame2015-libretro/](https://github.com/libretro/mame2015-libretro)