---
title: Libretro MAME 2003 Plus
description: 
published: true
date: 2021-08-09T07:35:25.515Z
tags: libretro, mame, mame2003+, mame2003plus
editor: markdown
dateCreated: 2021-07-05T23:26:19.800Z
---

**Libretro MAME2003_Plus** (also called MAME 2003+ and mame2003-plus) is a **Libretro** arcade system emulator core that emphasizes high performance and broad compatibility with mobile devices, single board computers, embedded systems and similar platforms.

In order to take advantage of the lower performance and hardware requirements of an earlier MAME architecture, MAME 2003-Plus started with the MAME 2003 code base, itself derived from MAME 0.78.

On this basis, MAME 2003-Plus contributors retro ported support for several hundred additional games, as well as other features not originally present in MAME 0.78.

## ![](/emulators/license.svg) License

This core is under [**MAME non-commercial**](https://github.com/libretro/mame2003-plus-libretro/blob/master/LICENSE.md) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Saves | ✔ |
| States | Game-dependent |
| Rewind | ✔ |
| Netplay | ✔ |
| Native Cheats | ✔ |
| Controllers | ✔ |
| Multi-Mouse | ✔ |

## ![](/emulators/bios.svg) BIOS

BIOS romsets are not required when using "Full Non-Merged" arcade romsets. For "Split" and "Non-Merged" romsets, place the BIOS in the same directory as the game romset.

>**Note**:
>Place the bios in the directory: `/recalbox/share/roms/mame/`   
>or in its subdirectory if you use several Mame cores: `/recalbox/share/roms/mame/Mame 2003-Plus/`
{.is-warning}

## ![](/emulators/roms.png) Roms

* Based on romset: 0.78-0.188 (MAME 0.78 as baseline with others ROMs based on higher MAME romsets)
* Size: 32gb
* Emulated romsets: 4941 (including clones, etc...)
* Active Sets: 4941
* Parents: 1089
* Clones: 2123
* Others: 1713
* BIOS: 16
* CHDs: 30
* Samples: 66 + 6 Optional "Soundtrack Samples"
* DatFile: [mame2003-plus.xml](https://github.com/libretro/mame2003-plus-libretro/blob/master/metadata/mame2003-plus.xml)

### Location

Put your roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 mame
┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**
┃ ┃ ┃ ┃ ┣ 📁 **game**
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **game.chd**

You can also opt for a subdirectory (useful if you want to have a mame set for another core).

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 mame
┃ ┃ ┃ ┃ ┣ 📁 Mame 2003-Plus
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**
┃ ┃ ┃ ┃ ┃ ┣ 📁 **game**
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 **game.chd**

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

## ![](/emulators/external-links.png) External links

* **Progettosnaps**: [https://www.progettosnaps.net/](https://www.progettosnaps.net/)
* **Source code used**: [https://github.com/libretro/mame2003-plus-libretro/](https://github.com/libretro/mame2003-plus-libretro)
* **Libretro documentation**: [https://docs.libretro.com/library/mame2003\_plus/](https://docs.libretro.com/library/mame2003_plus/)