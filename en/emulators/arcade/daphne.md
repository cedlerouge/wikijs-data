---
title: Daphne
description: LaserDisc
published: true
date: 2021-08-09T07:25:48.216Z
tags: arcade, daphne
editor: markdown
dateCreated: 2021-07-04T18:30:37.049Z
---

![](/emulators/arcade/daphne.svg){.align-center}

## Technical data

* **Developers**: Matt Ownby (Daphne) and Jeffrey Clark (Hypseus)
* **Initial release**: 1999
* **Website**: [http://www.daphne-emu.com/](http://www.daphne-emu.com/site3/index_hi.php) en C++
* **Daphne** is a program that lets one play the original versions of many Laserdisc arcade games.

## Presentation

The **LaserDisc** was the first optical storage medium, initially for video, to be marketed in 1978 in North America, initially under the name MCA DiscoVision.

Although it offered better sound and image quality than contemporary media (notably VHS and Betamax tapes), LaserDisc was not very successful, mainly because of the high price of the players and the fact that it could not record television programs. Although it was very popular with home cinema owners from the start, it was only in Asia (Hong Kong, Malaysia and Singapore), in the 1990s, that the LaserDisc was really introduced in homes.

Nevertheless, it is from the LaserDisc technology that several optical storage media have been developed: in particular CDs and DVDs, which have enjoyed considerable success.

### What is a Laserdisc?

A **LaserDisc** video game is a game that **uses pre-recorded video** (movies or animations) **played from a laser disc**, either for **all** or for **parts of the graphics.**
**Some games** use videos on which **sprites** (cars, ships, ...) will be **superimposed.**

But the **most popular** games were **movies or interactive cartoons**, in which **the player** had to **press a specific** button or **move the joystick** in the right direction at the right time to **move to the next scene.**

### What is DAPHNE?

DAPHNE is an emulator; or rather an 'emulator' (because it was thought as a woman or <<**FEMALE**>>: _First Ever Multiple Arcade Laserdisc Emulator_) that allows you to play **LaserDisc arcade games** (LaserDisc brand), such as **Dragon's Lair** (where the name of the emulator comes from, based on the name of the princess of the game), **Badlands**, **Cobra Command**, **Space Ace** or many others.

It was created by **Matt Ownby** during 1999 on a Z80 processor emulator. The first game to be emulated was Dragon's Lair, the most famous! Together with other colleagues and friends, he managed to emulate almost twenty games. However, he stopped publishing and updating in 2009 due to the frustration of the emulator community, who expected a professional management of a project that was basically "for fun".

A rewrite of DAPHNE is currently underway on Matt Ownby's side, with a real business model defined in which he will be able to offer commercial quality support to his users.

## Emulators

[Hypseus](hypseus)