---
title: Libretro PicoDrive
description: 
published: true
date: 2021-08-09T21:04:34.671Z
tags: libretro, picodrive, sega cd
editor: markdown
dateCreated: 2021-07-30T22:36:41.029Z
---

**Libretro PicoDrive** is an open-source Sega **8**/**16** bit and **32X** emulator. It has been designed for portable devices based on the **ARM** architecture.

## ![](/emulators/license.svg) License

This core is under [**non-commercial**](https://github.com/libretro/picodrive/blob/master/COPYING) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Restart | ✔ |
| Saves | ✔ |
| States | ✔ |
| Rewind | ✔ |
| Netplay | ✔ |
| Core Options | ✔ |
| RetroAchievements | ✔ |
| RetroArch Cheats | ✔ |
| Controls | ✔ |
| Remapping | ✔ |
| Disk Control | ✔ |

## ![](/emulators/bios.svg) BIOS

### List of optional bios

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| bios_CD_E.sms | EU MegaCD BIOS (bootrom) | e66fa1dc5820d254611fdcdba0662372 | ❌ |
| bios_CD_U.sms | US SegaCD BIOS (bootrom) | 2efd74e3232ff260e371b99f84024f7f | ❌ |
| bios_CD_J.sms | JP MegaCD BIOS (bootrom) | 278a9397d192149e84e820ac621a8ed bdeb4c47da613946d422d97d98b21cda | ❌ |

### Location

Put the bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 🗒 **bios_CD_E.sms**
┃ ┃ ┃ ┣ 🗒 **bios_CD_U.sms**
┃ ┃ ┃ ┣ 🗒 **bios_CD_J.sms**

## ![](/emulators/isos.svg) Isos

### Supported extensions

Isos must have the extension:

* .bin/.cue
* .iso

### Location

Put the isos like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 segacd
┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**

>The isos using **Redump** format are highly recommended.
{.is-success}

>For more information about isos, go to [this tutorial](./../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/picodrive/](https://github.com/libretro/picodrive/)
* **Libretro documentation**: [https://docs.libretro.com/library/picodrive/](https://docs.libretro.com/library/picodrive/)