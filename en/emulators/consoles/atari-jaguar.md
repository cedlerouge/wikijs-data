---
title: Atari Jaguar
description: 
published: true
date: 2021-08-09T17:28:15.535Z
tags: consoles, atari, jaguar
editor: markdown
dateCreated: 2021-07-27T17:06:42.043Z
---

![](/emulators/consoles/atarijaguar.svg){.align-center}

## Technical data

* **Manufacturer**: Atari Corporation
* **Year of release**: 1993
* **Units sold**: less than 250000
* **CPU**: Five Main Processors in three chips:
  * Tom Chip @ 26.59 MHz, Graphics Processor (32-bit RISC), Object (64-bit RISC), Blitter (64-bit RISC)
  * Jerry Chip @ 26.59 MHz, Digital Signal Processor (32-bit)
  * Motorola 68000 (16-bit @ 13.295 MHz)
* **RAM**: 2 MB
* **Display**: Max resolution 800×576 at 24 bit "true" color

## Presentation

The **Jaguar** is a home video game console with cartridges dating from 1993, with a limited game library, and which was not very successful despite its innovative technology. It was the second to last console produced by Atari Corporation.

## Emulators

[Libretro Virtualjaguar](libretro-virtualjaguar)