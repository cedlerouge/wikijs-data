---
title: Vectrex
description: Milton Bradley Vectrex
published: true
date: 2021-10-03T21:46:44.008Z
tags: consoles, gce, vectrex, mb
editor: markdown
dateCreated: 2021-07-28T18:04:18.916Z
---

![](/emulators/consoles/vectrex.svg){.align-center}

## Technical data

* **Manufacturer**: General Consumer Electronics / MB
* **Year of release**: 1982
* **CPU**: Motorola 68A09 @ 1,6 MHz
* **Screen**: Samsung 240RB40 9" vector black and white
* **RAM**: 1 Ko (two 4-bit 2114 chips)
* **ROM**: 8 Ko (one 8-bit 2363 chip)
* **Sound**: General Instrument AY-3-8912
* **Inputs/outputs**: MOS Technology 6522
* **Media**: cartridge with 32KB ROM

## Presentation

The **Vectrex** is an 8-bit video game console developed by Smith Engineering and distributed by General Consumer Electronics, and later by MB (Milton Bradley Company) after GCE's buyout.
It was released in late 1982, and ceased production in 1984 after the video game crash of 1983.

Developed by Jay Smith of Western Technology & Smith Engineering, the Vectrex is an 8-bit console with its own screen. At first proposed with a 5" screen to several distributors, only GCE will be interested in the Vectrex if the screen is enlarged to 9".

The Vectrex was released in November 1982 at a price of $199 in the USA, against the Atari 2600 and 5200 as well as the Intellivision, which are consoles that can be connected to a television set. But the Vectrex, on the other hand, has its own display without having to occupy a place under the family's television set, because it was designed from the start as a mini arcade console.

At the beginning of 1983, MB bought GCE and became the distributor of the console. In comparison with the success of Mattel and its Intellivision, MB launched the Vectrex in Europe.

## Emulators

[Libretro-vecx](libretro-vecx)