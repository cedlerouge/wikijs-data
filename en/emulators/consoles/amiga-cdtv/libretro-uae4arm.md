---
title: Libretro UAE4ARM
description: 
published: false
date: 2021-10-03T15:50:42.263Z
tags: libretro, 7.3+, amiga-cdtv, amiga, uae4arm, cdtv
editor: markdown
dateCreated: 2021-08-02T18:53:32.451Z
---



## ![](/emulators/license.svg) License

This core is under **GPLv2** license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ❌ | ❌ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Restart | ✔ |
| Screenshots | ✔ |
| Saves | ✔ |
| States | ✔ |
| Rewind | ✔ |
| Core Options | ✔ |
| Controls | ✔ |
| Remapping | ✔ |
| Multi-Mouse | ✔ |
| Disk Control | ✔ |
| LEDs | ✔ |

## ![](/emulators/bios.svg) BIOS

### List of mandatory bios

You will find the bios with the name indicated in the column Description that you will have to rename with the name indicated in the column Filename.

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| kick34005.CDTV | CDTV Extended-ROM v1.0 (1991)(Commodore)(CDTV)\[!\] | 89da1838a24460e4b93f4f0c5d92d48d d98112f18792ee3714df16a6eb421b89 d1145ab3a0f89340f94c9e734762c198 | ❌ |

### Location

Put the bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 🗒 **kick34005.CDTV.rom**

## ![](/emulators/isos.svg) Isos

### Supported extensions

Isos must have the extension:

* .bin/.cue
* .iso
* .mds/.mdf
* .img/.ccd
* .nrg
* .chd
* .m3u
* .uae

### Location

Put the isos like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 cd32
┃ ┃ ┃ ┃ ┣ 🗒 **game.cue**
┃ ┃ ┃ ┃ ┣ 🗒 **game.bin**

>The isos using **Redump** format are highly recommended.
{.is-success}

>For more information about isos, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/Chips-fr/uae4arm-rpi/](https://github.com/Chips-fr/uae4arm-rpi/)