---
title: Reicast
description: 
published: true
date: 2021-10-03T18:17:49.957Z
tags: dreamcast, reicast
editor: markdown
dateCreated: 2021-07-30T17:38:27.830Z
---

**Reicast** is an emulator capable to emulate the **Dreamcast**.

## ![](/emulators/license.svg) License

This core is under [**BSD-3**](https://github.com/reicast/reicast-emulator/blob/alpha/LICENSE) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌  | ❌ | ✅ | ❌ | ❌ | ❌ | ❌ | ❌ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
|  |  |

## ![](/emulators/bios.svg) BIOS

### List of mandatory bios

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| dc_boot.bin | Dreamcast BIOS | e10c53c2f8b90bab96ead2d368858623 | ❌  |
| dc_flash.bin | Date / Hour / Language | 0a93f7940c455905bea6e392dfde92a4 | ❌  |

### Location

Put the bios like this:

┣ 📁 recalbox  
┃ ┣ 📁 share  
┃ ┃ ┣ 📁 bios  
┃ ┃ ┃ ┣ 🗒 **dc_boot.bin**  
┃ ┃ ┃ ┣ 🗒 **dc_flash.bin**  

## ![](/emulators/isos.svg) Isos

### Supported extensions

Isos must have the extension:

* .bin/.cue
* .bin/.gdi
* .cdi
* .chd (v4 format!)

### Location

Put the isos like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 dreamcast
┃ ┃ ┃ ┃ ┣ 🗒 **game.gdi**
┃ ┃ ┃ ┃ ┣ 🗒 **game.bin**

>The isos using **TOSEC** format are highly recommended.
{.is-success}

>For more information about isos, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/reicast/reicast-emulator/](https://github.com/reicast/reicast-emulator/)
* **Documentation**: [https://reicast.com/guide/](https://reicast.com/guide/)