---
title: Family Computer Disk System
description: 
published: true
date: 2021-08-09T19:51:21.256Z
tags: consoles, nintendo, famicom, family computer disk system
editor: markdown
dateCreated: 2021-07-27T17:26:57.968Z
---

![](/emulators/consoles/famicomdisksystem.svg){.align-center}

## Technical data

* **Manufacturer**: Nintendo
* **Year of release**: 1986
* **Units sold**: 4.44 million
* **Best-selling game**: Super Mario Bros. 2
* **CPU**: Ricoh 2A03 8 Bit processor
* **RAM**: 32 Ko
* **Disk space**: 112 Ko
* **Audio/Video processor**: uses Famicom hardware
* **Ports**: uses Famicom hardware

## Presentation

The **Famicom Disk System** or **FDS** (**Family Computer Disk System**) is a peripheral for the Famicom video game console. Sold by Nintendo, it is placed under the console and uses a proprietary floppy disk format to store games. Released on February 21, 1986 in Japan, the American version, planned for the end of 1986, was never released.

By the end of 1984, Nintendo was already starting to think about the post-Famicom era, as it did not imagine that its console would remain commercially viable for many years to come. The main problem with the first home console was its memory storage capacity, so Nintendo decided to remedy this by developing a hardware extension for its console that would allow it to read floppy disks, which had a larger memory capacity and also offered the possibility of saving games.

## Emulators

[Libretro FBNeo](libretro-fbneo)
[Libretro FCEUmm](libretro-fceumm)
[Libretro Mesen](libretro-mesen)
[Libretro Nestopia](libretro-nestopia)