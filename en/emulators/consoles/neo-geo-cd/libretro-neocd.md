---
title: Libretro NeoCD
description: 
published: true
date: 2021-08-09T20:19:22.382Z
tags: libretro, neo-geo cd, neocd
editor: markdown
dateCreated: 2021-08-01T11:08:57.799Z
---



## ![](/emulators/license.svg) License

This core is under [**LGPLv3**](https://github.com/libretro/neocd_libretro/blob/master/LICENSE.md) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
|  | ✔ |

## ![](/emulators/bios.svg) BIOS

### List of mandatory bios

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| neocd.bin | CDZ BIOS (MAME) | f39572af7584cb5b3f70ae8cc848aba2 | ❌ |
| uni-bioscd.rom | Universe BIOS CD 3.3 | 08ca8b2dba6662e8024f9e789711c6fc | ❌ |
| top-sp1.bin | Top Loader BIOS (MAME) | 122aee210324c72e8a11116e6ef9c0d0 | ❌ |
| front-sp1.bin | Front Loader BIOS (MAME) | 5c2366f25ff92d71788468ca492ebeca | ❌ |
| neocd_sz.rom | CDZ BIOS (SMKDAN 0.7b DEC 2010) | 971ee8a36fb72da57aed01758f0a37f5 | ❌ |
| neocd_z.rom | CDZ BIOS | 11526d58d4c524daef7d5d677dc6b004 | ❌ |
| neocd_st.rom | Top Loader BIOS (SMKDAN 0.7b DEC 2010) | f6325a33c6d63ea4b9162a3fa8c32727 | ❌ |
| neocd_t.rom | Top Loader BIOS | de3cf45d227ad44645b22aa83b49f450 | ❌ |
| neocd_sf.rom | Front Loader BIOS (SMKDAN 0.7b DEC 2010) | 043d76d5f0ef836500700c34faef774d | ❌ |
| neocd_f.rom | Front Loader BIOS | 8834880c33164ccbe6476b559f3e37de | ❌ |
| 000-lo.lo | ZOOM Rom | fc7599f3f871578fe9a0453662d1c966 | ❌ |
| ng-lo.rom | ZOOM Rom | e255264d85d5765013b1b2fa8109dd53 | ❌ |

### Location

Put the bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 neocd
┃ ┃ ┃ ┃ ┣ 🗒 **neocd.bin**
┃ ┃ ┃ ┃ ┣ 🗒 **uni-bioscd.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **top-sp1.bin**
┃ ┃ ┃ ┃ ┣ 🗒 **front-sp1.bin**
┃ ┃ ┃ ┃ ┣ 🗒 **neocd_sz.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **neocd_z.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **neocd_st.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **neocd_t.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **neocd_sf.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **neocd_f.rom**
┃ ┃ ┃ ┃ ┣ 🗒 **000-lo.lo**
┃ ┃ ┃ ┃ ┣ 🗒 **ng-lo.rom**

## ![](/emulators/isos.svg) Isos

### Supported extensions

Isos must have hte extension:

* .bin/.cue
* .iso/.cue
* .img/.cue
* .chd

### Location

Put the isos like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 neogeocd
┃ ┃ ┃ ┃ ┣ 🗒 **game.cue**
┃ ┃ ┃ ┃ ┣ 🗒 **game.bin**

>For more information about isos, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/neocd_libretro/](https://github.com/libretro/neocd_libretro/)