---
title: Sega CD
description: Sega CD
published: true
date: 2021-10-03T18:48:21.627Z
tags: consoles, sega, sega cd
editor: markdown
dateCreated: 2021-07-27T20:22:00.552Z
---

![](/emulators/consoles/segacd.svg){.align-center}

## Technical data

* **Manufacturer**: Sega
* **Year of release**: 1991
* **Units sold**: 30 millions
* **Best-selling game**: Sewer Shark
* **CPU**: 16-bit Motorola 68000 @ 12.5 MHz + Megadrive CPUs
* **RAM**: 512kB
* **VRAM**: 256kB
* **Sound RAM**: 16kB
* **Video**: Custom ASIC
* **Resolution**: 320×224 pixels, 64 colors (512 colors palette)
* **Sound**: Ricoh RF5C164 + Megadrive chips
* **Media**: 500MB CD-ROM discs

## Presentation

The **Sega CD**, also known as **Mega-CD** or **Mega CD**, is a peripheral for Sega's Mega Drive video game console. It is a CD-ROM drive that can read games in CD-ROM format, Audio CDs and CD-Gs. It was released in late 1991 in Japan, in 1992 in the United States and in 1993 in Europe.

## Emulators

[Libretro PicoDrive](libretro-picodrive)
[Libretro GenesisPlusGX](libretro-genesisplusgx)