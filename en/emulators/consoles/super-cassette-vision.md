---
title: Super Cassette Vision
description: Epoch Super Cassette Vision
published: true
date: 2021-10-03T21:09:30.460Z
tags: consoles, 7.2+, super cassette vision, epoch
editor: markdown
dateCreated: 2021-07-28T17:24:01.911Z
---

![](/emulators/consoles/scv.svg){.align-center}

## Technical data

* **Manufacturer**: Epoch
* **Year of release**: 1984
* **CPU**: 8-bit NEC PD7801G
* **RAM** : 128KB
* **VRAM** : 4KB
* **Video** : EPOCH TV-1
* **Resolution** : 309×246, 16 colors, 128 on-screen sprites
* **Sound chip** : PD1771C @ 6 MHz

## Presentation

The Super Cassette Vision, successor of the Cassette Vision, is a home video game console manufactured by Epoch Co. and released in Japan on July 17, 1984 and in Europe, more precisely in France, later in 1984.

The Super Cassette Vision was released in 1984 with an 8-bit processor and better performance more in line with its competitors. It was then published in France by ITMC under the Yeno brand. At least 16 games were brought back from Japan for a European release.  
A version of the system targeted the young female market, the Super Lady Cassette Vision. The console came in a pink carrying case, next to the game Milky Princess.

For the record, the Super Lady Cassette Vision hosted the first game in the Dragon Ball license. It also has a Basic Nyumon "game" that allows you to create your own programs.  
Some games allowed to save (there were two stacks in cartridge). The Pop&Chips game had a level editor.  
The console could display 128 sprites at a time, each of which could be composed of up to 8 different images. However, the number of games released is quite limited (about thirty).

Epoch left the console market in 1987.

## Emulators

[Libretro EmuSCV](libretro-emuscv)