---
title: Atari 7800
description: 
published: true
date: 2021-08-09T17:20:41.394Z
tags: consoles, atari, 7800, atari-7800
editor: markdown
dateCreated: 2021-07-27T16:57:47.437Z
---

![](/emulators/consoles/atari7800-us.svg){.align-center}

## Technical data

* **Manufacturer**: Atari Corporation
* **Year of release**: 1986
* **Units sold**: 3,77 millions
* **CPU**: Atari SALLY 6502 ("6502C") @ 1.19-1.79MHz
* **RAM**: 4KB
* **Cart size**: 48KB
* **Audio/Video Processor**: TIA/MARIA custom graphic controller
* **Ports**: 2 joystick ports, 1 cartridge port, 1 expandable connector, power in, RF output

## Presentation

The **Atari 7800** is a third generation 8-bit video game console, designed and marketed by the American manufacturer Atari. It was released at the beginning of 1986 in the United States, then during the year in Japan and only in 1991 in France.

Succeeding the Atari 5200, the Atari 7800 was released as a competitor to the Nintendo Entertainment System (NES) and the Sega Master System during the era of third generation video game consoles. However, the console will never really compete with its rivals.

## Emulators

[Libretro Prosystem](libretro-prosystem)