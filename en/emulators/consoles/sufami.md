---
title: SuFami Turbo
description: Bandai SuFami Turbo
published: true
date: 2021-10-03T21:08:11.848Z
tags: consoles, bandai, sufami turbo
editor: markdown
dateCreated: 2021-07-28T16:25:10.994Z
---

![](/emulators/consoles/sufamiturbo.svg){.align-center}

## Technical data

* **Manufacturer**: Bandai
* **Year of release**: 1996
* **Media**: SuFami Turbo Application Cartridge + Game Cartridges

## Presentation

The **SuFami Turbo**, sometimes compared to the Aladdin Deck Enhancer, is an accessory provided by Bandai for the Nintendo Super Famicom and was available in 1996.

This device is designed to snap onto the top of the Super Famicom and contains 2 cartridge slots. The goal is that games can be produced at a lower cost as well as reduce development time, not depending on Nintendo's cartridge production. Like the Aladdin Deck Enhancer, this device has been officially approved by Nintendo with the condition that Bandai produces all the hardware itself.

The 2 cartridge slots are designed to share data between cartridges. The cartridge placed in the first slot is the game that is being played, while the cartridge in the second slot provides additional data for use in the main game. Of the thirteen games available, 9 of them can be linked, between each set of games.

The games that can be linked are identified by a yellow diagram showing a SuFami Turbo with either one or two cartridges in the bottom right corner of the game box. If the image has 1 cartridge inserted in the SuFami Turbo, the game cannot be linked. If the image has 2 cartridges inserted in the SuFami Turbo, the game can be linked with the game list shown on the box.

The SuFami Turbo was available alone or in a pack with a game.

## Emulators

[Libretro Snes9x](libretro-snes9x)