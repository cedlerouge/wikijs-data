---
title: Saturn
description: 
published: true
date: 2021-08-09T21:00:01.560Z
tags: consoles, sega, saturn
editor: markdown
dateCreated: 2021-07-28T16:23:19.319Z
---

![](/emulators/consoles/saturn-us.svg){.align-center}

## Technical data

* **Manufacturer**: Sega
* **Year of release**: 1994
* **Units sold**: 9.26 millions
* **Best-selling game**: Virtua Fighter 2
* **CPU**: 2× Hitachi SH-2 @ 28.6 MHz
* **RAM**: 16Mb
* **VRAM**: 12Mb
* **Video**: VDP1 (sprite/texture and polygons), VDP2 (backgrounds)
* **Resolution**: 320x224 to 704x224, 16.77 millions colors
* **Sound chip**: Motorola 68EC000, Yamaha FH1 YMF292, 32 channels
* **Media**: CD-ROM

## Presentation

The **Saturn** or **Sega Saturn** is a fifth generation video game console marketed by Sega. It is the fourth home console designed by the Japanese company, succeeding the Mega Drive, and was released on November 22, 1994 in Japan, on May 11, 1995 in North America and on July 8, 1995 in Europe. The Saturn is equipped with an architecture integrating eight processors, including two microprocessors, two graphics processors and two dedicated to audio. Its games are published in CD-ROM format and its game library consists of many arcade games as well as original titles.

The design of the Saturn began in 1992, around the new SH-2 processor designed in collaboration between Sega and Hitachi, in the role of unique central processor. As the design progressed, it was doubled by a second SH-2, as was the addition of the second custom graphics processor in early 1994, to allow the console to compete technically with Sony's PlayStation. The Saturn was initially a success in Japan, but not in the United States, where it was released in May 1995, four months before its original release date. After the debut of the Nintendo 64 in late 1996, the Saturn quickly lost market share in the United States and Europe, where it was discontinued in 1998. In Japan, the console was in operation until 2000. It sold 9.26 million units worldwide and was considered a commercial failure (despite its success in Japan).

The Saturn had several iconic games, such as Nights into Dreams, Sega Rally Championship, Daytona USA and the Panzer Dragoon, Virtua Fighter and Virtua Cop series. However, it received a mixed reception from observers and players. Its complex architecture making it difficult to program, the limited support from third-party video game developers and the inability of Sega's development teams to release a game in the Sonic the Hedgehog series, known as Sonic X-treme, are considered the main factors for the console's poor results. The Japanese company's management is also being criticized for its decision making during the development and discontinuation of the console.

## Emulators

[Libretro Mednafen_Saturn](libretro-mednafen_saturn)
[Libretro Yabause](libretro-yabause)
[Libretro YabaSanshiro](libretro-yabasanshiro)
[Libretro Kronos](libretro-kronos)