---
title: GX-4000
description: 
published: true
date: 2021-08-09T19:56:54.517Z
tags: consoles, amstrad, gx-4000
editor: markdown
dateCreated: 2021-07-27T17:34:39.351Z
---

![](/emulators/consoles/gx4000.svg){.align-center}

## Technical data

* **Manufacturer**: Amstrad
* **Year of release**: 1990
* **CPU**: Zilog Z80A @ 4 MHz
* **RAM**: 64 KB
* **VRAM**: 16 KB
* **ROM**: 32 K
* **Sound chip**: General Instrument AY-3-8912, 3 channel stereo
* **Resolution**: 160x200 to 640x200, 16 sprites, 4096 colors palette - 32 onscreen

## Presentation

The **GX-4000** is an 8 bits video game console from Amstrad released in 1990. It is built on an Amstrad CPC Plus base and therefore shares the same characteristics as this one.

## Emulators

[Libretro Cap32](libretro-cap32)