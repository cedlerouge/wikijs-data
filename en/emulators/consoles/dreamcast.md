---
title: Dreamcast
description: 
published: true
date: 2021-08-09T17:46:16.652Z
tags: consoles, sega, dreamcast
editor: markdown
dateCreated: 2021-07-27T17:24:14.141Z
---

![](/emulators/consoles/dreamcast-us.svg){.align-center}

## Technical data

* **Manufacturer**: SEGA
* **Year of release**:
  * 27 novembre 1998 (Japon)
  * 9 septembre 1999 (USA/Canada)
  * 14 octobre 1999 (Europe)
* **CPU**: Hitachi SH-4 32-bit RISC @ 200MHz
* **RAM**: 16 MB
* **VRAM**: 8MB
* **Video**: 100 MHz NEC PowerVR2 (integrated with the system's ASIC)
* **Resolution**: 640x480 pixels
* **Sound**: Yamaha AICA[139] sound processor @ 67 MHz
* **VMU size**: 128kB

## Presentation

The last console of **SEGA** will remain engraved in the memories for a long time.

The console that offers online gaming as standard did not convince enough people despite its impressive catalog of games, perhaps too far ahead of its time.

Despite the fact that the console offers good value for money, it was a commercial failure.

The **Dreamcast** does not make the same mistake as the [**Saturn**](./saturn) and has a single main processor.

Concerning the graphics, the work is done by a Power VR II straight out of the PC world, which, combined with an OS derived from Windows CE, allows for easier programming (especially with regard to ports).

The proprietary **GD-Rom** format prevents games from being copied too easily, but it is above all the 1GB capacity that makes the support so interesting.

Now we come to the controller ports: the **Dreamcast** has 4 of them, which allows you to fully enjoy games like **Virtua Tennis** without having to invest in a multipad.

On the other hand, we don't have any ports for **VMS**. Indeed, these are inserted in the controllers, which will allow you to have up to 8 of them inserted in the console at the same time.

Now, the most interesting: the **modem**. This plugs into the side of the console, allowing you to change it for a faster one if you feel like it. 

To finish, we will note that the **Dreamcast** is the basis of the arcade system [**NAOMI**](./naomi). This system consists mainly of a card with almost the same technical characteristics as the **Dreamcast** (but with twice as much memory). The advantage of the [**NAOMI**](./naomi) system is that it allows to connect up to 16 of these cards which then work in parallel. Nevertheless, the [**NAOMI**](./naomi) system will follow its own evolution separately from the **Dreamcast**.

## Emulators

[Libretro Flycast](libretro-flycast)
[Reicast](reicast)