---
title: Libretro GenesisPlusGX
description: 
published: true
date: 2021-08-09T20:09:14.146Z
tags: libretro, system, genesisplusgx, master
editor: markdown
dateCreated: 2021-07-30T21:57:19.066Z
---

**Libretro GenesisPlusGX** is an open-source 8/16-bit Sega emulator focused on accuracy and portability.

## ![](/emulators/license.svg) License

This core is under [**non-commercial**](https://github.com/libretro/Genesis-Plus-GX/blob/master/LICENSE.txt) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Restart | ✔ |
| Saves | ✔ |
| States | ✔ |
| Rewind | ✔ |
| Netplay | ✔ |
| Core Options | ✔ |
| RetroAchievements | ✔ |
| RetroArch Cheats | ✔ |
| Controls | ✔ |
| Remapping | ✔ |
| Multi-Mouse | ✔ |

## ![](/emulators/bios.svg) BIOS

### List of optional bios

| Nom de fichier | Description | MD5 | Fourni |
| :---: | :---: | :---: | :---: |
| bios_E.sms | BIOS EU MasterSystem (bootrom) | 840481177270d5642a14ca71ee72844c 4187d96beaf36385e681a3cf3bd1663d e8b26871629b938887757a64798df6dc 02cbb2e348945c9ac41e37502a58ca76 5cd8f62cd8786af0226e6d2248279338 08b81aa6be18b92daef1b875deecf824 bdb34f2c471e5d0c358eeec621f9d029 | ⚠️ |
| bios_U.sms | BIOS US MasterSystem (bootrom) | 840481177270d5642a14ca71ee72844c b264bef9bda264ffe83afcebac21b81f e8b26871629b938887757a64798df6dc 02cbb2e348945c9ac41e37502a58ca76 5cd8f62cd8786af0226e6d2248279338 08b81aa6be18b92daef1b875deecf824 | ⚠️ |
| bios_J.sms | BIOS JP MasterSystem (bootrom) | 24a519c53f67b00640d0048ef7089105 | ⚠️ |

⚠️ Provided but it's not the correct bios.

### Location

Put the bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 🗒 **bios_E.sms**
┃ ┃ ┃ ┣ 🗒 **bios_U.sms**
┃ ┃ ┃ ┣ 🗒 **bios_J.sms**

## ![](/emulators/roms.png) Roms

### supported extensions

Roms must have the extension:

* .sms
* .zip
* .7z

This system supports compressed roms in .zip/.7z format. However, be careful, it is only an archive.

The files contained in the .zip/.7z must correspond to the extensions mentioned above.
Moreover, each .zip/.7z file must contain only one compressed rom.

### Location

Put the roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 mastersystem
┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**

>The roms using **No-Intro** format are highly recommended.
{.is-success}

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/Genesis-Plus-GX/](https://github.com/libretro/Genesis-Plus-GX/)
* **Libretro documentation**: [https://docs.libretro.com/library/genesis_plus_gx/](https://docs.libretro.com/library/genesis_plus_gx/)