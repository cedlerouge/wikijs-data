---
title: Libretro GenesisPlusGX Wide
description: Megadrive/Genesis emulator with wide screen support
published: false
date: 2021-08-09T20:01:05.835Z
tags: genesis, libretro, 7.3+, genesisplusgx wide
editor: markdown
dateCreated: 2021-06-22T12:13:24.636Z
---

**Libretro GenesisPlusGX Wide** is an open-source Sega 8/16-bit emulator focused on accuracy and portability.

It emulate Megadrive games with widescreen support.

Unlike other emulators, **Libretro GenesisPlusGX Wide** will not stretch the image to fill the screen, but will display game graphics outside the original 4/3 area.

An impressive example on Street of Rage with the Wide core:

![](/emulators/consoles/megadrive/libretro-genesisplusgxwide/169.png)

And the original version:

![](/emulators/consoles/megadrive/libretro-genesisplusgxwide/43.png)

## ![](/emulators/license.svg) License

This core is under [**non-commercial**](https://github.com/libretro/Genesis-Plus-GX/blob/master/LICENSE.txt) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4 | RPI-400 | ODROID XU4 | ODROID GO | PC x86 | PC X86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Restart | ✔ |
| Saves | ✔ |
| States | ✔ |
| Rewind | ✔ |
| Netplay | ✔ |
| Core Options | ✔ |
| RetroAchievements | ✔ |
| RetroArch Cheats | ✔ |
| Controls | ✔ |
| Remapping | ✔ |
| Multi-Mouse | ✔ |

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the following extensions:

* .bin
* .en
* .md
* .smd
* .zip
* .7z

This system supports compressed roms in .zip/.7z format. However, be careful, it is only an archive.

The files contained in the .zip/.7z must correspond to the extensions mentioned above.
Moreover, each .zip/.7z file must contain only one compressed rom.

### Location

Place the roms like this: 

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 megadrive
┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**

>The roms using **No-Intro** format roms are highly recommended.
{.is-success}

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an update, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via the RetroArch menu:

┣ 📁 RetroArch menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the `retroarch-core-options.cfg` file:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/Genesis-Plus-GX-Wide/](https://github.com/libretro/Genesis-Plus-GX-Wide/)