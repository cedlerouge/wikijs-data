---
title: Libretro EmuSCV
description: 
published: true
date: 2021-08-09T21:07:50.512Z
tags: libretro, 7.2+, super cassette vision, emuscv
editor: markdown
dateCreated: 2021-08-03T10:59:05.182Z
---



## ![](/emulators/license.svg) License

This core is under [**GPLv3**](https://gitlab.com/MaaaX-EmuSCV/libretro-emuscv/-/blob/master/licence.txt) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Restart | ✔ |
| Screenshots | ✔ |
| Saves | ✔ |
| Controls | ✔ |
| Remapping | ✔ |

## ![](/emulators/bios.svg) BIOS

### List of mandatory bios

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| upd7801g.s01 | BIOS | 635a978fd40db9a18ee44eff449fc126 | ❌ |

### Location

Put the bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 🗒 **upd7801g.s01**

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .bin
* .cart
* .zip
* .7z

This system supports compressed roms in .zip/.7z format. However, be careful, it is only an archive.

The files contained in the .zip/.7z must correspond to the extensions mentioned above.
Moreover, each .zip/.7z file must contain only one compressed rom.

### Location

Put the roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 scv
┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**

The particularity is that if you load a known `.bin`, `.rom` or `.0` (`.1`, `.2`, etc.) ROM, it creates a `.CART` ROM file which tells the emulator the right data mapping (the cartridge can contain several ROMs, possibly RAM, or even SRAM saved by a stack). For games where you could save, it also creates a `.SAVE` file which contains the SRAM data (saved when you quit).

The `.CART` is created in the `roms` directory.

If it can't create the `.CART`, it doesn't load the game, it starts on the console test screen like when the console can't read a cartridge.  
I'll modify the code so that it loads anyway and see how I can put it in the savegames directory.

>The roms using **No-Intro** format are highly recommended.
{.is-success}

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://gitlab.com/MaaaX-EmuSCV/libretro-emuscv/](https://gitlab.com/MaaaX-EmuSCV/libretro-emuscv/)