---
title: Libretro Mednafen_PSX_HW
description: 
published: true
date: 2021-08-09T20:47:00.147Z
tags: libretro, ps1, mednafen, playstation 1, psx hw
editor: markdown
dateCreated: 2021-08-01T23:19:14.348Z
---

[Beetle PSX](https://github.com/libretro/beetle-psx-libretro) is a fork of the [Mednafen](https://mednafen.github.io/) PSX module for Libretro, it currently runs on Linux, macOS and Windows, this core is available in `mednafen-psx-hw` version which requires OpenGL 3.3 for the OpenGL renderer.

## ![](/emulators/license.svg) License

This core is under [**GPLv2**](https://github.com/libretro/beetle-psx-libretro/blob/master/COPYING) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Restart | ✔ |
| Saves | ✔ |
| States | ✔ |
| Core Options | ✔ |
| RetroAchievements | ✔ |
| RetroArch Cheats | ✔ |
| Controls | ✔ |
| Remapping | ✔ |
| Multi-Mouse | ✔ |
| Rumble | ✔ |
| Disk Control | ✔ |

## ![](/emulators/bios.svg) BIOS

### List of mandatory bios

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| scph5500.bin | Japanese PS1 BIOS - required for japanese games | 8dd7d5296a650fac7319bce665a6a53c | ❌ |
| scph5501.bin | American PS1 BIOS - required for american games | 490f666e1afb15b7362b406ed1cea246 | ❌ |
| scph5502.bin | European PS1 BIOS - required for european games | 32736f17079d0b2b7024407c39bd3050 | ❌ |

### Location

Put the bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 🗒 **scph5500.bin**
┃ ┃ ┃ ┣ 🗒 **scph5501.bin**
┃ ┃ ┃ ┣ 🗒 **scph5502.bin**

## ![](/emulators/isos.svg) Isos

### Supported extensions

Isos must have the extension:

* .bin/.cue
* .bin/.toc
* .m3u
* .img/.ccd/.sub
* .exe
* .pbp
* .chd

### Location

Put the isos like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 psx
┃ ┃ ┃ ┃ ┣ 🗒 **game.bin**
┃ ┃ ┃ ┃ ┣ 🗒 **game.cue**

>The isos using **Redump** format are highly recommended.
{.is-success}

>For more information about isos, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

| Options | Options values | Variables (for configuration override) | Variables values |
| --- | --- | --- | --- |
| **Renderer (Restart)** | `Hardware (auto)` ✅ / `Hardware (OpenGL)` / `Hardware (Vulkan)` / `Software` | `beetle_psx_hw_renderer` | `hardware` / `hardware_gl` / `hardware_vk` / `software` |
| **Software Framebuffer** | `Enabled` ✅ / `Disabled` | `beetle_psx_hw_renderer` | `enabled` / `disabled` |
| **Internal GPU Resolution** | `1x (Native)` ✅ / `2x` / `4x` / `8x` / `16x` | `beetle_psx_hw_internal_resolution` | `1x(native)` / `2x` / `4x` / `8x` / `16x` |
| **Internal Color Depth** | `16 bpp (Native)` ✅ / `32 bpp` | `beetle_psx_hw_depth` | `16bpp(native)` / `32 bpp` |
| **Dithering Pattern** | `1x (Native)` ✅ / `Internal Resolution` / `Disabled` | `beetle_psx_hw_dither_mode` | `1x(native)` / `internal resolution` / `disabled` |
| **Texture Filtering** | `Nearest` ✅ / `SABR` / `xBR` / `Bilinear` / `3-Point` / `JINC2` | `beetle_psx_hw_filter` | `nearest` / `SABR` / `xBR` / `bilinear` / `3-point` / `JINC2` |
| **Track Textures** | `Enabled` / `Disabled` ✅ | `beetle_psx_hw_track_textures` | `enabled` / `disabled` |
| **Dump Textures** | `Enabled` / `Disabled` ✅ | `beetle_psx_hw_dump_textures` | `enabled` / `disabled` |
| **Replace Textures** | `Enabled` / `Disabled` ✅ | `beetle_psx_hw_replace_textures` | `enabled` / `disabled` |
| **Wireframe Mode (Debug)** | `Enabled` / `Disabled` ✅ | `beetle_psx_hw_wireframe` | `enabled` / `disabled` |
| **Display Full VRAM (Debug)** | `Enabled` / `Disabled` ✅ | `beetle_psx_hw_dispaly_vram` | `enabled` / `disabled` |
| **PGXP Operation Mode** | `Disabled` ✅ / `Memory Only` / `Memory + CPU (Buggy)` |`beetle_psx_hw_pgxp_mode` | `disabled` / `memory only` / `memory + CPU` |
| **PGXP Vertex Cache** | `Enabled` / `Disabled` ✅ | `beetle_psx_hw_pgxp_vertex` | `enabled` / `disabled` |
| **PGXP Perspective Correct Texturing** | `Enabled` / `Disabled` ✅ | `beetle_psx_hw_pgxp_texture` | `enabled` / `disabled` |
| **Display Internal FPS** | `Enabled` / `Disabled` ✅ | `beetle_psx_hw_display_internal_fps` | `enabled` / `disabled` |
| **Line-to-Quad Hack** | `Default` ✅ / `Aggressive` / `Désactivé` | `beetle_psx_hw_line_render` | `default` / `aggressive` / `disabled` |
| **CPU Dynarec** | `Disabled (Beetle Interpreter)` ✅ / `Max Performance` / `Cycle Timing Check` / `Lightrec Interpreter` | `beetle_psx_hw_cpu_dynarec` | `disabled` / `execute` / `execute_once` / `run_interpreter` |
| **Dynarec Code Invalidation** | `Full` ✅ / `DMA Only (Slightly Faster)` | `beetle_psx_hw_dynarec_invalidate` | `full` / `dma` |
| **Dynarec DMA/GPU Event Cycles** | `128 (Default)` ✅ / `256` / `384` / `512` / `640` / `768` / `896` / `1024` | `beetle_psx_hw_dynarec_eventcycles` | `128` / `256` / `384` / `512` / `640` / `768` / `896` / `1024` |
| **CPU Frequency Scaling (Overclock)** | `50%` to `750%`, default `100% (Native)` ✅ | `beetle_psx_hw_cpu_freq_scale` | `50%` to `750%`, default `100%` |
| **GTE Overclock** | `Enabled` / `Disabled` ✅ | `beetle_psx_hw_gte_overclock` | `enabled` / `disabled` |
| **GPU Resterizer Overclock** | `1x (Native)` ✅ / `2x` / `4x` / `8x` / `16x` / `32x` | `beetle_psx_hw_gpu_overclock` | `1x(native)` / `2x` / `4x` / `8x` / `16x` / `32x` |
| **Skip BIOS** | `Enabled` / `Disabled` ✅ | `beetle_psx_hw_skip_bios` | `enabled` / `disabled` |
| **Core-Reported FPS Timing** | `Progressive Rate (Default)` ✅ / `Force Interlaced Rate` / `Allow Automatic Toggling` | `beetle_psx_hw_core_timing_fps` | `force_progressive` / `force_interlaced` / `auto_toggle` |
| **Core Aspect Ratio** | `Corrected` ✅ / `Uncorrected` / `Force 4:3` / `Force NTSC` | `beetle_psx_hw_aspect_ratio` | `corrected` / `uncorrected` / `4:3` / `ntsc` |
| **Widescreen Mode Hack** | `Enabled` / `Disabled` ✅ | `beetle_psx_hw_widescreen_hack` | `enabled` / `disabled` |
| **PAL (European) Video Timing Override** | `Enabled` / `Disabled` ✅ | `beetle_psx_hw_pal_video_timing_override` | `enabled` / `disabled` |
| **Crop Horizontal Overscan** | `Enabled` ✅ / `Disabled` | `beetle_psx_hw_crop_overscan` | `enabled` / `disabled` |
| **Horizontal Image Offset (GPU Cycles)** | `-24` to `+24`, default `0` ✅ | `beetle_psx_hw_image_offset_cycles` | `-24` to `+24`, default `0` |
| **Initial Scanlines - PAL** | `0` to `40`, default `0` ✅ | `beetle_psx_hw_initial_scanline_pal` | `0` to `40`, default `0` |
| **Last Scanlines - PAL** | `230` to `287`, default `287` ✅ | `beetle_psx_hw_last_scanline_pal` | `230` to `287`, default `287` |
| **CD Access Method (Restart)** | `Synchronous` ✅ / `Asynchronous` / `Pre-Cache` | `beetle_psx_hw_cd_access_method` | `sync` / `async` / `precache` |
| **CD Loading Speed** | `2x (Native)` ✅ / `4x` / `6x` / `8x` / `10x` / `12x` / `14x` | `beetle_psx_hw_cd_fastload` | `2x(native)` / `4x` / `6x` / `8x` / `10x` / `12x` / `14x` |
| **Memory Card 0 Method (Restart)** | `Libretro` ✅ / `Mednafen` | `beetle_psx_hw_use_mednafen_memcard0_method` | `libretro` / `mednafen` |
| **Shared Memory Card 1 (Restart)** | `Enabled` ✅ / `Disabled` | `beetle_psx_hw_shared_memory_cards` | `enabled` / `disabled` |
| **Analog Self-Calibration** | `Enabled` / `Disabled` ✅ | `beetle_psx_hw_analog_calibration` | `enabled` / `disabled` |
| **Enable DualShock Analog Mode Toggle** | `Enabled` / `Disabled` ✅ | `beetle_psx_hw_analog_toggle` | `enabled` / `disabled` |
| **Port 1: Multitap Enable** | `Enabled` / `Disabled` ✅ | `beetle_psx_hw_enable_multitap_port1` | `enabled` / `disabled` |
| **Port 2: Multitap Enable** | `Enabled` / `Disabled` ✅ | `beetle_psx_hw_enable_multitap_port2` | `enabled` / `disabled` |
| **Gun Input Mode** | `Light Gun` ✅ / `Touchscreen` | `beetle_psx_hw_gun_input_mode` | `lightgun` / `touchscreen` |
| **Gun Cursor** | `Cross` ✅ / `Dot` / `Disabled` | `beetle_psx_hw_gun_cursor` | `cross` / `dot` / `off` |
| **Mouse Sensitivity** | `5%` to `200%`, default `100%` ✅ | `beetle_psx_hw_mouse_sensitivity` | `5%` to `200%`, default `100%` |
| **NegCon Twist Responses** | `Linear` ✅ / `Quadratic` / `Cubic` | `beetle_psx_hw_negcon_response` | `linear` / `quadratic` / `cubic` |
| **NegCon Twist Deadzone** | `0%` ✅ / `5%` / `10%` / `15%` / `20%` / `25%` / `30%` | `beetle_psx_hw_negcon_deadzone` | `5%` / `10%` / `15%` / `20%` / `25%` / `30%` |
| **Memory Card Left Index** | `0` to `63`, default `0` ✅ | `beetle_psx_hw_memcard_left_index` | `0` to `63`, default `0` |
| **Memory Card Right Index** | `0` to `63`, default `1` ✅ | `beetle_psx_hw_memcard_right_index` | `0` to `63`, default `1` |

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/beetle-psx-libretro/](https://github.com/libretro/beetle-psx-libretro/)
* **Libretro documentation**: [https://docs.libretro.com/library/beetle_psx_hw/](https://docs.libretro.com/library/beetle_psx_hw/)