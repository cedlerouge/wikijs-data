---
title: Libretro Mednafen_Saturn
description: 
published: true
date: 2021-08-09T21:01:11.788Z
tags: libretro, mednafen, saturn
editor: markdown
dateCreated: 2021-08-03T10:27:00.046Z
---

**Libretro Mednafen_Saturn** is a port of the **Mednafen Saturn** emulator (standalone) to Libretro supporting the Sega Saturn.

## ![](/emulators/license.svg) License

This core is under [**GPLv2**](https://github.com/libretro/beetle-saturn-libretro/blob/master/COPYING) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :--- | :--- |
| Restart | ✔ |
| Screenshots | ✔ |
| Saves | ✔ |
| States | ✔ |
| Rewind | ✔ |
| Netplay | ✔ |
| Core Options | ✔ |
| RetroAchievements | ✔ |
| Controls | ✔ |
| Remapping | ✔ |
| Multi-Mouse | ✔ |

## ![](/emulators/bios.svg) BIOS

### List of mandatory bios

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| sega_101.bin | Saturn JP BIOS - Required for Japanese games | 85ec9ca47d8f6807718151cbcca8b964 | ❌ |
| mpr-17933.bin | Saturn US/EU BIOS - Required for Europena/America games | 3240872c70984b6cbfda1586cab68dbe | ❌ |
| mpr-18811-mx.ic1 | The King of Fighters '95 - Required for this game | 255113ba943c92a54facd25a10fd780c | ❌ |
| mpr-19367-mx.ic1 | Ultraman: Hikari no Kyojin Densetsu - Required for this game | 1cd19988d1d72a3e7caa0b73234c96b4 | ❌ |

### Location

Put the bios like this:

┣ 📁 recalbox  
┃ ┣ 📁 share  
┃ ┃ ┣ 📁 bios  
┃ ┃ ┃ ┣ 🗒 **sega_101.bin**  
┃ ┃ ┃ ┣ 🗒 **mpr-17933.bin**  
┃ ┃ ┃ ┣ 🗒 **mpr-18811-mx.ic1**  
┃ ┃ ┃ ┣ 🗒 **mpr-19367-mx.ic1**  

## ![](/emulators/isos.svg) Isos

### Supported extensions

Isos must have the extension:

* .cue/.bin
* .m3u
* .img/.ccd
* .iso
* .chd

### Location

Put the isos like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 saturn
┃ ┃ ┃ ┃ ┣ 🗒 **game.bin**
┃ ┃ ┃ ┃ ┣ 🗒 **game.cue**

>The isos using **Redump** format are highly recommended.
{.is-success}

>For more information about isos, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/beetle-saturn-libretro/](https://github.com/libretro/beetle-saturn-libretro/)
* **Libretro documentation**: [https://docs.libretro.com/library/beetle_saturn/](https://docs.libretro.com/library/beetle_saturn/)