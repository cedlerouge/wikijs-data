---
title: SG-1000
description: 
published: true
date: 2021-08-09T20:51:42.060Z
tags: consoles, sega, sg-1000
editor: markdown
dateCreated: 2021-07-28T10:06:22.490Z
---

![](/emulators/consoles/sg1000.svg){.align-center}

## Technical data

* **Manufacturer**: Sega
* **Year of release**: 1983
* **Units sold**: 2 millions
* **Best-selling game**: Flicky
* **CPU**: 8-Bit Zilog Z80 @ 3.58 MHz
* **RAM**: 1kB
* **VRAM**: 2kB
* **Video**: Texas Instruments TMS9918
* **Resolution**: 256×192 pixels, 16 colors (21 colors palette)
* **Sound chip**: Texas Instruments SN76489 PSG
* **Cart size**: 8kB - 48kB

## Presentation

The **SG-1000**, also known as the **Sega Game 1000**, is a third-generation video game console designed and marketed by Japanese manufacturer Sega Enterprises, Ltd. The console marks Sega's entry into home video gaming and is a foundation for the more successful Master System. Unveiled in 1983, the SG-1000 was released on the same day as Nintendo's Famicom in Japan. It was released in several forms, including the **SC-3000** computer and the redesigned **SG-1000 II** launched in 1984.

Developed in response to a downturn in the arcade market in 1982, the SG-1000 was created on the advice of Hayao Nakayama, president of Sega of Japan. In direct competition with the Famicom, the SG-1000, although technologically weaker, had a better first year of operation than expected, due to the technical problems encountered by Nintendo's console. Shortly after its release, Sega was sold to CSK Corporation and the SG-1000 II model was launched.

In October 1985, Sega released the **Mark III**, whose technical characteristics rivaled those of the Famicom, even though it was backward compatible with the other games published on the two previous versions of the console. The latter gave way to the Master System the following year due to its release in the United States.

Despite a promising start, the SG-1000 had a disappointing commercial performance in Japan, with only two million units sold, the market being dominated by the Famicom and fragmented by the presence of numerous consoles. If observers judge that it did not fundamentally mark the history of the video game industry, the SG-1000 contributes to forge a community of enthusiasts around it and Sega.

## Emulators

[Libretro GenesisPlusGX](libretro-genesisplusgx)
[Libretro Gearsystem](libretro-gearsystem)
[Libretro FBNeo](libretro-fbneo)