---
title: Libretro Mupen64plus_Next
description: 
published: true
date: 2021-08-09T20:27:26.766Z
tags: libretro, 64dd, n64dd, mupen64plus next
editor: markdown
dateCreated: 2021-08-01T12:40:49.656Z
---



## ![](/emulators/license.svg) License

This core is under [**GPLv2**](https://github.com/libretro/mupen64plus-libretro-nx/blob/develop/LICENSE) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ✅ | ✅ | ❌ | ✅ 🐌 | ❌ | ❌ |

🐌 Low performances but playable

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Restart | ✔ |
| Screenshots | ✔ |
| Saves | ✔ |
| States | ✔ |
| Rewind | ✔ |
| Core Options | ✔ |
| RetroAchievements | ✔ |
| RetroArch Cheats | ✔ |
| Controls | ✔ |
| Remapping | ✔ |
| Rumble | ✔ |
| Subsystem | ✔ |

## ![](/emulators/bios.svg) BIOS

### List of mandatory bios

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| IPL64.bin | N64DD BIOS | 8d3d9f294b6e174bc7b1d2fd1c727530 | ❌ |

### Location

Put the bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 📁 Mupen64plus
┃ ┃ ┃ ┃ ┣ 🗒 **IPL64.bin**

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .n64
* .ndd
* .v64
* .z64

### Location

Put the roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 64dd
┃ ┃ ┃ ┃ ┣ 🗒 **game.ndd**

>The roms using **No-Intro** format are highly recommended.
{.is-success}

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

## ![](/emulators/external-links.png) External links

* **Source code**: [https://github.com/libretro/mupen64plus-libretro-nx/](https://github.com/libretro/mupen64plus-libretro-nx/)
* **Documentation**: [https://docs.libretro.com/library/mupen64plus/](https://docs.libretro.com/library/mupen64plus/)