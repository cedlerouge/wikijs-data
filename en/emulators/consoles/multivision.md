---
title: Multivision
description: Othello Multivision
published: true
date: 2021-10-03T18:54:12.344Z
tags: consoles, othello, multivision, tsukuda
editor: markdown
dateCreated: 2021-07-28T05:20:57.655Z
---

![](/emulators/consoles/multivision.svg){.align-center}

## Technical data

* **Manufacturer**: Tsukuda Original
* **Year of release**: 1983
* **CPU**: Zilog Z80 @ 3.58 MHz
* **RAM**: 1KB
* **VRAM**: 16KB
* **Video**: Texas Instruments TMS9918
* **Resolution**: 256×192 pixels, 16 colors (21 colors palette)
* **Sound chip**: Texas Instruments SN76489 PSG

## Presentation

The **Othello Multivision** is a licensed clone of Sega's SG-1000, designed in 1983 by Tsukuda Original. This console exists because Sega's original intention for the SC-3000 was to allow other manufacturers to produce compatible devices in the hope of having a worldwide standard. Unfortunately, perhaps with the emergence of the MSX, this tactic failed and only a few SG-1000 / SC-3000 compatible models were produced. The Othello Multivision was one of them.

Similar to the SG-1000, there are 2 versions of the Othello Multivision, named FG-1000 (released in 1983) and FG-2000 (released in 1984) respectively. Being able to use the cartridges for SG-1000 and SC-3000, the Othello Multivision saw a number of games and software available with the Othello Multivision brand. The system was available with the Othello game included.

One of the major problems with the Othello Multivision was that instead of having external controllers, these were on the console itself. It is compatible with some of the SG-1000 add-ons but includes the SK-1100 which activates the keyboard functions of the SC-3000. The FG-1000 models were supplied with an overlay that explained the functions of each button.

The FG-2000 model adds support for a second controller. This model can easily be identified by the fact that the buttons are blue instead of red and that the included joystick has been replaced by a directional pad.

## Emulators

[Libretro Gearsystem](libretro-gearsystem)