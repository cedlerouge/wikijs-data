---
title: Libretro Gearsystem
description: libretro-super
published: true
date: 2021-08-09T12:52:31.373Z
tags: libretro, gamegear, gg, gearsystem
editor: markdown
dateCreated: 2021-07-18T17:00:38.089Z
---

**Libretro Gearsystem** is an open source, multi-platform, Sega Master System / Game Gear / SG-1000 emulator written in C++.

* Very accurate Z80 kernel, including undocumented opcodes and behaviors such as R and MEMPTR registers.
* Multi-Mapper support: SEGA, Codemasters, SG-1000 and ROM cartridges only.
* Automatic region detection: NTSC-JAP, NTSC-USA, PAL-EUR.
* Internal database for Roma detection
* Highly accurate VDP emulation, including synchronization and SMS2 mode support only.
* Audio emulation using SDL Audio and Sms_Snd_Emu library.
* Support for battery powered RAM saver.
* Save states.
* Support for Game Genie and Pro Action Replay cheats.
* Runs on Windows, Linux, Mac OS X, Raspberry Pi, iOS and as Libretro core (RetroArch).

The core Libretro Gearsystem was created by Ignacio Sanchez

## ![](/emulators/license.svg) License

This core is under [**GPLv3**](https://github.com/drhelius/Gearsystem/blob/master/LICENSE) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Restart | ✔ |
| Screenshots | ✔ |
| Saves | ✔ |
| States | ✔ |
| Rewind | ✔ |
| Core Options | ✔ |
| RetroAchievements | ✔ |
| RetroArch Cheats - Game Genie | ✔ |
| RetroArch Cheats - Pro Acion Replay | ✔ |
| Controls | ✔ |
| Remapping | ✔ |
| Softpatching | ✔ |

## ![](/emulators/bios.svg) BIOS

>**No bios is required.**
{.is-success}

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have this extension:

* .gg
* .zip
* .7z

This system supports compressed roms in .zip/.7z format. However, be careful, it is only an archive.

The files contained in the .zip/.7z must correspond to the extensions mentioned above.
Moreover, each .zip/.7z file must contain only one compressed rom.

### Location

Put your roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 gamegear
┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**

>The roms using **No-Intro** format are highly recommended.
{.is-success}

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/Gearboy/](https://github.com/libretro/Gearboy/)
* **Libretro Documentation**: [https://docs.libretro.com/library/gearsystem/](https://docs.libretro.com/library/gearsystem/)
* **Official source code**: [https://github.com/drhelius/Gearsystem/](https://github.com/drhelius/Gearsystem/)