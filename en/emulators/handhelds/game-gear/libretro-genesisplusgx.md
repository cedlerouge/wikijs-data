---
title: Libretro GenesisPlusGX
description: 
published: true
date: 2021-08-09T12:52:59.614Z
tags: libretro, gamegear, gg, genesisplusgx
editor: markdown
dateCreated: 2021-07-18T17:20:50.862Z
---

**Libretro Genesis Plus GX** is an open-source 8/16-bit Sega emulator focused on accuracy and portability.  
The source code, originally based on Charles MacDonald's Genesis Plus 1.3, has been heavily modified and enhanced, with respect to the original goals and design, to improve emulation accuracy, implement new features and add support for additional peripherals, cartridges and system hardware.

Genesis Plus GX is 100% compatible with Genesis/Mega Drive, Sega/Mega CD, Master System, Game Gear, SG-1000 and Pico software (including all known unlicensed and pirate dumps), also emulating backwards compatibility modes when available.

## ![](/emulators/license.svg) License

This core is under [**non-commercial**](https://github.com/libretro/Genesis-Plus-GX/blob/master/LICENSE.txt) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Restart | ✔ |
| Saves | ✔ |
| States | ✔ |
| Rewind | ✔ |
| Netplay | ✔ |
| Core Options | ✔ |
| RetroAchievements | ✔ |
| RetroArch Cheats | ✔ |
| Controls | ✔ |
| Remapping | ✔ |
| Multi-Mouse | ✔ |

## ![](/emulators/bios.svg) BIOS

### List of optional bios

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| bios.gg | GameGear BIOS (bootrom) | 672e104c3be3a238301aceffc3b23fd6 | ⚠️ |

⚠️ Provided but it's not the correct bios.

### Location

Put your bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 🗒 **bios.gg**

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .gg
* .zip
* .7z

This system supports compressed roms in .zip/.7z format. However, be careful, it is only an archive.

The files contained in the .zip/.7z must correspond to the extensions mentioned above.
Moreover, each .zip/.7z file must contain only one compressed rom.

### Location

Put your roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 gamegear
┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**

>The roms using **No-Intro** format are highly recommended.
{.is-success}

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/Genesis-Plus-GX/](https://github.com/libretro/Genesis-Plus-GX/)
* **Libretro documentation**: [https://docs.libretro.com/library/genesis\_plus\_gx/](https://docs.libretro.com/library/genesis_plus_gx/)
* **Official source code**: [https://github.com/ekeeke/Genesis-Plus-GX/](https://github.com/ekeeke/Genesis-Plus-GX/)