---
title: Libretro PokeMini
description: 
published: true
date: 2021-08-09T13:08:21.160Z
tags: libretro, pokémon, mini, pokémini
editor: markdown
dateCreated: 2021-07-27T16:06:02.784Z
---

**Libretro PokeMini** is an emulator for the Nintendo Pokémon Mini handheld system developed in C and C++.

It is written by **JustBurn**.

## ![](/emulators/license.svg) License

This core is under [**GPLv3**](https://github.com/libretro/PokeMini/blob/master/LICENSE) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4 | RPI-400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Restart | ✔ |
| Saves | ✔ |
| States | ✔ |
| Rewind | ✔ |
| Core Options | ✔ |
| Controls | ✔ |
| Remapping | ✔ |
| Vibration | ✔ |

## ![](/emulators/bios.svg) BIOS

### List of optional bios

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| bios.min | Pokémon Mini BIOS | 1e4fb124a3a886865acb574f388c803d | ⚠️ |

⚠️ Provided but it's not the correct bios.

### Location

Put the bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 🗒 **bios.min**

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .min
* .zip

This system supports compressed roms in .zip format. However, be careful, it is only an archive.

The files contained in the .zip must correspond to the extensions mentioned above.
Moreover, each .zip file must contain only one compressed rom.

### Location

Put the roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 pokemini
┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**

>The roms using **No-Intro** format are highly recommended.
{.is-success}

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/PokeMini/](https://github.com/libretro/PokeMini/)
* **Libretro Documentation**: [https://docs.libretro.com/library/pokemini/](https://docs.libretro.com/library/pokemini/)
* **Official source code**: [https://sourceforge.net/projects/pokemini/](https://sourceforge.net/projects/pokemini/)
* **Official documentation**: [https://sourceforge.net/p/pokemini/wiki/Home/](https://sourceforge.net/p/pokemini/wiki/Home/)