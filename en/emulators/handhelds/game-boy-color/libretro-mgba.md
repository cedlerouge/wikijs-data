---
title: Libretro mGBA
description: 
published: true
date: 2021-08-09T12:50:54.934Z
tags: libretro, color, gameboy, mgba, gbc
editor: markdown
dateCreated: 2021-07-14T06:31:44.891Z
---

**Libretro mGBA** is an emulator for running Game Boy Advance games.  
It aims to be faster and more accurate than many existing Game Boy Advance emulators, as well as adding features that other emulators lack. It also supports Game Boy and Game Boy Color games.

## ![](/emulators/license.svg) License

This core is under [**MPLv2.0**](https://github.com/libretro/mgba/blob/master/LICENSE) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Restart | ✔ |
| Saves | ✔ |
| States | ✔ |
| Rewind | ✔ |
| Core Options | ✔ |
| RetroAchievements | ✔ |
| RetroArch Cheats | ✔ |
| Controls | ✔ |
| Remapping | ✔ |
| Rumble | ✔ |
| Softpatching | ✔ |

## ![](/emulators/bios.svg) BIOS

### List of optional bios

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| gbc_bios.bin | Game Boy Color BIOS | dbfce9db9deaa2567f6a84fde55f9680 | ❌ |
| sgb_bios.bin | Super Game Boy BIOS | d574d4f9c12f305074798f54c091a8b4 | ❌ |

### Location

Put your bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 🗒 **gbc_bios.bin**
┃ ┃ ┃ ┣ 🗒 **sgb_bios.bin**

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .bin
* .gbc
* .zip
* .7z

This system supports compressed roms in .zip/.7z format. However, be careful, it is only an archive.

The files contained in the .zip/.7z must correspond to the extensions mentioned above.
Moreover, each .zip/.7z file must contain only one compressed rom.

### Location

Put your roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 gbc
┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**

>The roms using **No-Intro** format are highly recommended.
{.is-success}

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/mgba/](https://github.com/libretro/mgba/)
* **Libretro documentation**: [https://docs.libretro.com/library/mgba/](https://docs.libretro.com/library/mgba/)
* **Official source code**: [https://github.com/mgba-emu/mgba](https://github.com/mgba-emu/mgba)
* **Official website**: [https://mgba.io/](https://mgba.io/)