---
title: PPSSPP
description: 
published: true
date: 2021-08-09T13:01:12.167Z
tags: psp, ppsspp
editor: markdown
dateCreated: 2021-07-27T13:20:35.647Z
---

**PPSSPP** is a free emulator for the PSP handheld console developed in C++.  
It is available on many platforms, including Android, Windows, iOS and GNU/Linux.

It is written by **Henrik Hrydgard**.

## Requirements

* OpenGL / Open GL ES 2.0 or higher for the OpenGL renderer.
* Vulkan for the Vulkan renderer.
* Direct3D 11 for the Direct3D 11 renderer.

## ![](/emulators/license.svg) License

This core is under [**GPLv2**](https://github.com/hrydgard/ppsspp/blob/master/LICENSE.TXT) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Restart | ✔ |
| Saves | ✔ |
| States | ✔ |
| Core Options | ✔ |
| Native Cheats | ✔ |
| Controls | ✔ |
| Remapping | ✔ |
| User | ✔ |
| Language | ✔ |

## ![](/emulators/bios.svg) BIOS

>**No bios is required.**
{.is-success}

## ![](/emulators/isos.svg) Isos

### Supported extensions

Isos must have the extension:

* .elf
* .iso
* .cso
* .prx
* .pbp

### Location

Put your isos like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 psp
┃ ┃ ┃ ┃ ┣ 🗒 **game.iso**

>The isos using **Redump** format are highly recommended.
{.is-success}

>For more information about isos, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/hrydgard/ppsspp/](https://github.com/hrydgard/ppsspp/)
* **Libretro documentation**: [https://docs.libretro.com/library/ppsspp/](https://docs.libretro.com/library/ppsspp/)
* **Official website**: [http://www.ppsspp.org/](http://www.ppsspp.org/)
* **Official forum**: [https://forums.ppsspp.org/](https://forums.ppsspp.org/)
* **Official documentation**: [http://www.ppsspp.org/guides.html](http://www.ppsspp.org/guides.html)