---
title: Palm
description: 
published: true
date: 2021-09-16T06:19:02.033Z
tags: handhelds, palm, palm-os
editor: markdown
dateCreated: 2021-07-06T09:13:21.304Z
---

![](/emulators/handheld/palm.svg){.align-center}

## Technical data

* **Manufacturer**: Palm, Inc.
* **Year of release**: 2002
* **OS**: Palm OS version 4.1
* **CPU**: Motorola DragonBall VZ MC68VZ328 @ 33 MHz
* **RAM**: 16 MB RAM
* **ROM**: 4 MB Flash
* **Display**: 160×160 pixels (65,000+ colors)

## Presentation

**Palm OS** (also known as **Garnet OS**) is an embedded operating system originally developed by U.S. Robotics Corp. owner Palm Computing, Inc. from 1995 for PDAs (personal digital assistants) in 1996.

The graphical user interface of Palm OS is designed to be used with a touch screen. It comes with a basic suite of applications for personal information manager. Later versions of this OS were enhanced to run on smartphones. Several other companies have been licensed to build electronic devices running Palm OS.

## Emulators

[Libretro Mu](libretro-mu)