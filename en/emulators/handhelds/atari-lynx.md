---
title: Atari Lynx
description: 
published: true
date: 2021-08-09T07:38:45.543Z
tags: atari, handhelds, lynx
editor: markdown
dateCreated: 2021-07-06T00:03:49.906Z
---

![](/emulators/handheld/lynx.svg){.align-center}

## Technical data

* **Manufacturer**: Epyx / Atari
* **Year of release**: 1989
* **Units sold**: 1 million
* **CPU**: 8-bit with 16-bit address space
* **Co-Processors**: Mikey and Suzy 2 x 16-bit
* **RAM**: 64K 120 ns DRAM
* **ROM**: 512 bytes
* **Resolution**: 160 x 102 pixels
* **Sound**: 4 canaux, 8-bit DAC

## Presentation

The **Atari Lynx** was Atari's only handheld console and the first handheld with a color LCD screen. It was released in 1989.

The machine was developed by Epyx under the name "Handy2" (today it is the name of the most advanced PC emulator of this console). During a very advanced stage of development, the company looked for investors for its project, and first chose Nintendo, which refused to invest in the project, which led Epyx to turn to Atari, which bought the rights in 1988.

The company modified the internal speaker and removed the stick that was then on the pad. Atari marketed the console two years later. The screen could be inverted to play as a left-handed player and up to 8 players could play in a network. The technical capabilities were much better than those of the Game Boy (color and 3D). The Lynx, on the other hand, was larger, too battery-intensive and lacked a lot of titles.

In 1991, Atari released a second version of its console in a new form, with redesigned cartridges. The new console (named by Atari "Lynx II") had grips, a better quality screen and a power saving option that allowed the console to be put in sleep mode.

## Emulators

[Libretro Handy](libretro-handy)
[Libretro Mednafen_Lynx](libretro-mednafen_lynx)