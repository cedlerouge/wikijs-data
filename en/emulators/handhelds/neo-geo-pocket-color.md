---
title: Neo-Geo Pocket Color
description: 
published: true
date: 2021-08-09T12:57:42.138Z
tags: color, snk, neo-geo, handhelds, pocket, ngpc
editor: markdown
dateCreated: 2021-07-06T07:57:06.701Z
---

![](/emulators/handheld/ngpc.svg){.align-center}

## Technical data

* **Manufacturer**: SNK
* **Year of release**: 1999
* **Units sold**: 2 millions, including color version
* **CPUs**: Toshiba TLCS900H @ 6.144 MHz, Z80 @ 3.072 MHz
* **RAM**: 12K for 900H, 4K for Z80
* **ROM**: 64 Kbytes
* **Picture Processor Unit**: 16-bit
* **Résolution**: 160x152 (256×256 virtual screen)
* **Média**: ROM cartridge 4 MB max

## Presentation

The **Neo-Geo Pocket Color** (or **NGPC**) is a 16-bit handheld console designed by SNK. The console was released on March 16, 1999 in Japan, on August 6, 1999 in North America, and on October 1, 1999 in some European countries. It is the successor of the monochrome Neo-Geo Pocket console, released one year earlier.

The operating system has an original function, the language is set in the console. Thus, the games display their texts in the selected language (assuming the cartridge provides the requested language). Other parameters can be set in the console, time and date for example, and the operating system can provide personalized horoscopes when a birth date is entered.

Cables to connect multiple consoles are available, as well as a cable to connect the console with a Sega Dreamcast for certain games. There is also a wireless connector released in Japan allowing several players to play together. An MP3 player extension was developed but not released due to SNK's closure.

## Emulators

[Libretro Mednafen_NGP](libretro-mednafen_ngp)
[Libretro RACE](libretro-race)