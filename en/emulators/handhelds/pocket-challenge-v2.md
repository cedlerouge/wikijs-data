---
title: Pocket Challenge V2
description: 
published: true
date: 2021-08-09T22:08:36.767Z
tags: handhelds, pocket, challenge, v2, pcv2, benesse, 7.2+
editor: markdown
dateCreated: 2021-07-06T09:16:40.163Z
---

![](/emulators/handheld/pcv2.svg){.align-center}

## Technical data

* **Manufacturer**: Benesse Corporation
* **Year of release**: 2000
* **CPU**: 16Bits NEC V20
* **RAM**: 512k video
* **Resolution**: 224 x 144 pixels
* **Sound**: 4 PCM canaux, 32 samples 4Bit pour chaque canal

## Presentation

The Pocket Challenge v2 is a handheld console by Benesse Corporation, a Japanese company focused on education. The console was created as an educational device. Benesse corporation, owner of Berlitz Language Schools, has released a few games on language learning. The Pocket Challenge v2 is fully compatible with the Bandai WonderSwan, created by Gunpei Yokoi, the same genius who created the Nintendo Game & Watch and the Nintendo GameBoy. As it is possible to play famous games like Final Fantasy, Gunpay, Klonoa, Mobile Suit Gundam or Pocket Fighters, the Pocket Challenge was one of the most fun educational consoles.

## Emulators

[Libretro Mednafen_WSwan](libretro-mednafen-wswan)