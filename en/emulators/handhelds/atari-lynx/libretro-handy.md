---
title: Libretro Handy
description: 
published: true
date: 2021-08-09T07:39:27.558Z
tags: libretro, atari, lynx, handy
editor: markdown
dateCreated: 2021-07-09T20:22:58.740Z
---

**Libretro Handy** is an Atari Lynx video game system emulator that can be used as the core of libretro.  
Handy was the original name of the Lynx project that was started at Epyx and then ended by Atari.

## ![](/emulators/license.svg) License

This core is under [**zlib**](https://sourceforge.net/projects/handy/) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Restart | ✔ |
| Screenshots | ✔ |
| States | ✔ |
| Rewind | ✔ |
| Netplay (State based) | ✔ (not link-cable emulation) |
| Core Options | ✔ |
| RetroAchievements | ✔ |
| Controls | ✔ |
| Remapping | ✔ |

## ![](/emulators/bios.svg) BIOS

### List of mandatory bios

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| lynxboot.img | Lynx Boot Image | fcd403db69f54290b51035d82f835e7b | ❌  |

### Location

Put the bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 🗒 **lynxboot.img**

## ![](/emulators/roms.png) Roms

### Supported extensions

Rom must have the extension:

* .lnx
* .o
* .zip
* .7z

This system supports compressed roms in .zip/.7z format. However, be careful, it is only an archive.

The files contained in the .zip/.7z must correspond to the extensions mentioned above.
Moreover, each .zip/.7z file must contain only one compressed rom.

### Location

Put your roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 lynx
┃ ┃ ┃ ┃ ┣ 🗒 **game.lnx**

>The roms using **No-Intro** format are highly recommended.
{.is-success}

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/libretro-handy](https://github.com/libretro/libretro-handy)
* **Libretro documentation**: [https://docs.libretro.com/library/handy/](https://docs.libretro.com/library/handy/)
* **Official source code**: [http://handy.sourceforge.net/download.htm](http://handy.sourceforge.net/download.htm)
* **Official website**:  [http://handy.sourceforge.net/](http://handy.sourceforge.net/)