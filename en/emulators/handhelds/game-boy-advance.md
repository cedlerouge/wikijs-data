---
title: Game Boy Advance
description: 
published: true
date: 2021-08-09T12:53:49.219Z
tags: nintendo, gameboy, handhelds, advance, gba
editor: markdown
dateCreated: 2021-07-06T00:11:54.128Z
---

![](/emulators/handheld/gameboyadvance.svg){.align-center}

## Technical data

* **Manufacturer**: Nintendo
* **Year of release**: 2001
* **Units sold**: 82 millions
* **Best-selling game**: Pokémon Ruby
* **CPU**: 16 MHz 32bit RISC-CPU + 8bit CISC-CPU
* **RAM**: 32KB WRAM + 256KB WRAM
* **Screen**: Reflective TFT Colour LCD
* **Resolution**: 240 x 160 pixels
* **Vidéo RAM**: 256KB
* **Display ability**: 32 000 colors

## Presentation

The **Game Boy Advance** is a portable video game system created by Nintendo in 2001. It has a color screen and a 32-bit processor, which made it the most powerful handheld console when it was released.

In 2003, Nintendo released the Game Boy Advance SP (SP for SPecial), which was technically identical to the Game Boy Advance, but with a more compact size and improvements such as a backlit screen and a rechargeable lithium-ion battery. Finally, in 2005, Nintendo released the Game Boy Micro, the latest version of its handheld, a "mini" size system with an improved screen.

Nintendo's 2006 annual report indicates that 75 million Game Boy Advance units were sold (all versions combined). On February 12, 2008, the last game of the console (Samurai Deeper Kyo) was released in the United States. The console will have lived for almost 7 years. Its final sales are estimated at 81.51 million copies, making the console a new success after those of the Game Boy and the Game Boy Color.

A descendant of the Game Boy Color (itself a descendant of the Game Boy), it offers slightly better performance than the 16-bit home consoles (Mega Drive and Super Nintendo).

Nintendo wanted its console to have an original look that would catch the eye and was not satisfied with the prototypes proposed by the company's traditional designers. Wanting a fresh look, Nintendo contacted **Gwénaël Nicolas**, a French designer who was visiting Japan and was a complete stranger to the world of video games. He proposed dozens of completely offbeat prototypes. Finally, the "panda" prototype was chosen and for this reason, the console seen from the front must suggest the head of this animal.

Its compatibility with the games of the first version and the Color version ensures a huge catalog of games (compatibility that is not required with the Game Boy Micro, the Nintendo DS and the Nintendo DS Lite). As the GBA is not zoned, it is possible to play American and Japanese games on a European console. In addition, the low cost of developing games for portable consoles (compared to games for home consoles at the same time) meant that many productions were released on Game Boy Advance.

The possibility of playing with four players, sometimes with a single game cartridge, by linking Game Boy Advance together is another strong point.

Its big weakness, although there are accessories to compensate for this, is the lack of backlighting, making the display very dark. This is one of the points that led Nintendo to release the Game Boy Advance SP, a model with a screen lit by diodes on its sides.

Another weak point is the sound of the console. Indeed, it does not have an integrated circuit dedicated to sound, forcing games to dedicate part of the power of the central processor to it.

## Emulators

[Libretro mGBA](libretro-mgba)
[Libretro gpSP](libretro-gpsp)
[Libretro Meteor](libretro-meteor)