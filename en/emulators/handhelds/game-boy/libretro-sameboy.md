---
title: Libretro SameBoy
description: 
published: true
date: 2021-08-09T07:43:57.570Z
tags: libretro, game, boy, gameboy, sameboy
editor: markdown
dateCreated: 2021-07-14T06:17:35.621Z
---

**Libretro SameBoy** is an extremely accurate open source Gameboy (DMG) and Gameboy Color (CGB) emulator written in C.

* Supports Game Boy (DMG); Game Boy Color (CGB) and GBC-Mode emulation; Game Boy Advance (AGB)
* Supports accurate high-level emulation of Super Game Boy (SGB; NTSC and PAL) and Super Game Boy 2 (SGB2)
* Allows you to choose the model you want to emulate regardless of ROM
* High quality 96 kHz audio
* Support for battery saving
* Save states
* Includes open source boot ROMs for all emulated models:

  * Full support for (and documentation of) all game-specific palettes in the CGB / AGB boot ROM, for accurate emulation of Game Boy games on a Game Boy Color
  * Supports manual palette selection with key combinations, with 4 additional new palettes (direction + A or B)
  * Supports palette selection in a CGB game, forcing it to run in "palletized" DMG mode, if the ROM allows it
  * Support for games with a non-Nintendo logo in the header
  * No long animation in the DMG boot ROM

* Four color correction settings
* Three high-pass audio filter settings
* Real-time clock emulation
* Turbo, rewind and slow motion modes
* Extremely high accuracy
* Link cable emulation

## ![](/emulators/license.svg) License

This core is under [**MIT**](https://github.com/libretro/SameBoy/blob/master/LICENSE) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Restart | ✔ |
| Screenshots | ✔ |
| Saves | ✔ |
| States | ✔ |
| Rewind | ✔ |
| Netplay | ✔ |
| Core Options | ✔ |
| RetroAchievements | ✔ |
| Controls | ✔ |
| Remapping | ✔ |
| Rumble | ✔ |
| Subsystem | ✔ |

## ![](/emulators/bios.svg) BIOS

### List of optional bios

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| dmg_boot.bin | Game Boy BIOS | 57f6a79fb41d6b2c8987fa529c3a90f2 | ✅ |

### Location

Put your bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 🗒 **dmg_boot.bin**

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .bin
* .gb
* .zip
* .7z

This system supports compressed roms in .zip/.7z format. However, be careful, it is only an archive.

The files contained in the .zip/.7z must correspond to the extensions mentioned above.
Moreover, each .zip/.7z file must contain only one compressed rom.

### Location

Put your roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 gb
┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**

>The roms using **No-Intro** format are highly recommended.
{.is-success}

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/SameBoy/](https://github.com/libretro/SameBoy/)
* **Libretro documentation**: [https://docs.libretro.com/library/sameboy/](https://docs.libretro.com/library/sameboy/)
* **Official source code**: [https://github.com/LIJI32/SameBoy](https://github.com/LIJI32/SameBoy/)
* **Official website**: [https://sameboy.github.io](https://sameboy.github.io)