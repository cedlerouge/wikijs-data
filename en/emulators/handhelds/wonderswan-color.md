---
title: Wonderswan Color
description: 
published: true
date: 2021-08-09T13:14:38.436Z
tags: color, handhelds, bandai, wonderswan
editor: markdown
dateCreated: 2021-07-06T09:41:13.679Z
---

![](/emulators/handheld/wonderswancolor.svg){.align-center}

## Technical data

* **Manufacturer**: Bandai
* **Year of release**: 2000
* **Units sold**: 740 727
* **CPU**: SSPGY-1002 @ 3.072 MHz 16-bit NEC V30MZ duplicate
* **Memory**: 64 kB partagé VRAM/WRAM
* **Resolution**: 224 x 144 pixels
* **Display**: FSTN reflective LCD, 2.8 in (71 mm), 241 colors
* **Sound**: 1 haut-parleur mono, casque optionnel

## Presentation

The **WonderSwan Color** is a portable video game console, created by Bandai.

It is an improvement of the original WonderSwan. It was released on December 30th 2000 in Japan and was quite successful, taking up to 8% of the Japanese market, dominated by Nintendo and its various Game Boys. The WonderSwan Color is compatible with the games of the original WonderSwan.

In November 2002, Bandai released an evolution of the WonderSwan Color, under the name SwanCrystal, with a better screen.

## Emulators

[Libretro Mednafen_WSwan](libretro-mednafen-wswan)