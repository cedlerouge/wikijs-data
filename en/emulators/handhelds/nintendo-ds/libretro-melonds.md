---
title: Libretro melonDS
description: 
published: true
date: 2021-08-09T13:00:24.516Z
tags: libretro, ds, melonds
editor: markdown
dateCreated: 2021-07-27T10:52:14.619Z
---

**Libretro melonDS** is a promising Nintendo DS/DSI emulator developed in C and C++.

The **melonDS** emulator aims to provide a fast and accurate Nintendo DS emulation. Although it is still a work in progress, it has a pretty solid set of features:

* Almost complete core (CPU, video, audio, ...)
* JIT recompiler for fast emulation
* OpenGL rendering, 3D upconversion
* RTC, microphone, closed / open cover
* Joystick support
* Savestates
* Different display modes / sizing / rotation
* (WIP) Wifi: local multiplayer, online connectivity
* DSi emulation (WIP)
* DLDI
* (WIP) GBA slot add-ons
* and more are planned!

It was written by **Arisotura** (known as StapleButter), a former contributor to DeSmuME.

## ![](/emulators/license.svg) License

This core is under [**GPLv3**](https://github.com/libretro/melonDS/blob/master/LICENSE) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Restart | ✔ |
| Screenshots | ✔ |
| Saves | ✔ |
| RetroAchievements | ✔ |
| RetroArch Cheats | ✔ |
| Controls | ✔ |
| Remapping | ✔ |

## ![](/emulators/bios.svg) BIOS

### List od mandatory bios

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| bios7.bin | ARM7 BIOS | df692a80a5b1bc90728bc3dfc76cd948 | ❌ |
| bios9.bin | ARM9 BIOS | a392174eb3e572fed6447e956bde4b25 | ❌ |
| firmware.bin | NDS Firmware | 145eaef5bd3037cbc247c213bb3da1b3 e45033d9b0fa6b0de071292bba7c9d13 3ad72b2c9a736b24953f2d391da4bfcc | ❌ |

### Location

Put the bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 🗒 **bios7.bin**
┃ ┃ ┃ ┣ 🗒 **bios9.bin**
┃ ┃ ┃ ┣ 🗒 **firmware.bin**

## ![](/emulators/roms.png) Roms

### Supported extensions

Roms must have the extension:

* .nds
* .zip
* .7z

This system supports compressed roms in .zip/.7z format. However, be careful, it is only an archive.

The files contained in the .zip/.7z must correspond to the extensions mentioned above.
Moreover, each .zip/.7z file must contain only one compressed rom.

### Location

Put the roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 ds
┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**

>The roms using **No-Intro** format are highly recommended.
{.is-success}

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/melonds/](https://github.com/libretro/melonds/)
* **Libretro documentation**: [https://docs.libretro.com/library/melonds/](https://docs.libretro.com/library/melonds/)
* **Official source code**: [https://github.com/Arisotura/melonDS](https://github.com/Arisotura/melonDS/)
* **Official website**: [http://melonds.kuribo64.net/](http://melonds.kuribo64.net/)
* **Official forum**: [http://melonds.kuribo64.net/board/](http://melonds.kuribo64.net/board/)