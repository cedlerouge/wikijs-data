---
title: Libretro DeSmuME
description: 
published: true
date: 2021-08-09T13:00:00.708Z
tags: libretro, ds, desmume
editor: markdown
dateCreated: 2021-07-27T10:47:22.706Z
---

**Libretro DeSmuME** is an emulator for the Nintendo DS game system developed in C and C++.

It is written by:

* YopYop156
* Zeromus

## ![](/emulators/license.svg) License

This core is under [**GPLv2**](https://github.com/libretro/desmume/blob/master/license.txt) license.

## ![](/emulators/compatibility.png) Compatibility

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ✅ | ✅ |

## ![](/emulators/features.png) Features

| Feature | Supported |
| :---: | :---: |
| Restart | ✔ |
| Screenshots | ✔ |
| Saves | ✔ |
| States | ✔ |
| Rewind | ✔ |
| Netplay | ✔ (pas de Download Play, câble link ou émulation du Wi-Fi) |
| Core Options | ✔ |
| RetroAchievements | ✔ |
| RetroArch Cheats | ✔ |
| Controls | ✔ |
| Remapping | ✔ |
| Username | ✔ |

## ![](/emulators/bios.svg) BIOS

### List of mandatory bios

| Filename | Description | MD5 | Provided |
| :---: | :---: | :---: | :---: |
| bios7.bin | ARM7 BIOS | df692a80a5b1bc90728bc3dfc76cd948 | ❌ |
| bios9.bin | ARM9 BIOS | a392174eb3e572fed6447e956bde4b25 | ❌ |
| firmware.bin | NDS Firmware | 145eaef5bd3037cbc247c213bb3da1b3 e45033d9b0fa6b0de071292bba7c9d13 3ad72b2c9a736b24953f2d391da4bfcc | ❌ |

### Location

Put the bios like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 bios
┃ ┃ ┃ ┣ 🗒 **bios7.bin**
┃ ┃ ┃ ┣ 🗒 **bios9.bin**
┃ ┃ ┃ ┣ 🗒 **firmware.bin**

## ![](/emulators/roms.png) Roms

### Supported extensions

La rom doit avoir l'extension :

* .nds
* .bin
* .zip
* .7z

This system supports compressed roms in .zip/.7z format. However, be careful, it is only an archive.

The files contained in the .zip/.7z must correspond to the extensions mentioned above.
Moreover, each .zip/.7z file must contain only one compressed rom.

### Location

Put the roms like this:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 ds
┃ ┃ ┃ ┃ ┣ 🗒 **game.zip**

>The roms using **No-Intro** format are highly recommended.
{.is-success}

>For more information about roms, go to [this tutorial](./../../../tutorials/games/generalities/isos-and-roms)!
{.is-info}

## ![](/emulators/advanced-configuration.png) Advanced configuration of the emulator

>To be able to keep your custom configurations during an upgrade, we advise you to use our [configuration override](./../../../advanced-usage/configuration-override) feature.
{.is-info}

### Accessing to options

You can configure various options in two different ways.

* Via RetroArch menu:

┣ 📁 RetroArch Menu
┃ ┣ 📁 Core options
┃ ┃ ┣ 🧩 Name_option

* Via the file `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Core options

## ![](/emulators/external-links.png) External links

* **Source code used**: [https://github.com/libretro/desmume/](https://github.com/libretro/desmume/)
* **Libretro documentation**: [https://docs.libretro.com/library/desmume/](https://docs.libretro.com/library/desmume/)
* **Source official source code**: [https://github.com/TASVideos/desmume](https://github.com/TASVideos/desmume/)
* **Official documentation**: [https://desmume.org/documentation/](https://desmume.org/documentation/)
* **Official website**: [https://desmume.org/](https://desmume.org/)
* **Forum officiel** : [http://forums.desmume.org/](http://forums.desmume.org/)