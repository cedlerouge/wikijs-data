---
title: Game Boy
description: 
published: true
date: 2021-08-09T12:53:25.014Z
tags: nintendo, game, boy, gameboy, handhelds
editor: markdown
dateCreated: 2021-07-06T00:08:47.229Z
---

![](/emulators/handheld/gameboy-us.svg){.align-center}

## Technical data

* **Manufacturer**: Nintendo
* **Year of release**: 1989
* **Units sold**: 118.69 millions
* **Best-selling game**: Tetris
* **CPU**: 8-bit Zilog Z80 @ 4.194304MHz
* **RAM**: 8kB
* **VRAM**: 8k
* **Video**: PPU (embedded in CPU)
* **Resolution**: 160x144 pixels, 4 shades of "gray"
* **Sound**: 4 channels stéréo sound
* **Cart sizes**: 32kB - 1MB

## Presentation

The Game Boy is a fourth-generation 8-bit handheld video game system developed and manufactured by Nintendo. Released in Japan on April 21, 1989, then in North America in October 1989, and finally in Europe on September 28, 1990, it is the first portable console in the Game Boy series. It was designed by Gunpei Yokoi and Nintendo Research & Development 1 - the same team that designed the Game and Watch series as well as many successful Nintendo Entertainment System games.

Despite the release of more technically advanced handheld consoles, the Game Boy was a huge success. It became the third best-selling console in history, with 119 million units sold, behind Sony's PlayStation 2 and the Nintendo DS. Despite its minimalist graphic quality, the Game Boy was able to impose itself thanks to its many assets: small size, low price, great autonomy and a rich and varied catalog of games.

## Emulators

[Libretro Gambatte](libretro-gambatte)
[Libretro Mesen_S](libretro-mesen_s)
[Libretro mGBA](libretro-mgba)
[Libretro SameBoy](libretro-sameboy)
[Libretro TGBDual](libretro-tgbdual)