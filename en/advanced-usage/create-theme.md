---
title: Create new themes
description: Create themes for the new Recalbox's frontend
published: false
date: 2021-06-27T20:17:31.222Z
tags: 
editor: markdown
dateCreated: 2021-06-26T15:41:17.121Z
---

# Recalbox themes

## Customizing UI controls

## Customizing views

# Building a new theme

## File structure

### Main xml

### System xml

### Resource storage

## Theme contents

### Structure

#### Object definition and overriding

#### Templating

#### Resource path

Path may be defined absolute or relative.
Absolute path are usefull to access system-wide resources provided by recalbox. Relavive path are always relative to the theme main folder to access theme resources.

Examples:
- `/recalbox/system/resources/splash/logo.png` : Absolute path to recalbox logo screen
- `images/gamelist_background.jpg` : Relative path to a theme's image
- `./images/gamelist_background.jpg` : Relative path to a theme's image

Accessing system-wide resources using double dots is **strongly discouraged** since theme folder may move without prior notice in future recalbox releases.

All path may contain variables as path or file part replacement. There is no limit in using variables in path.
Examples:
- `images/gamelist_background_${resolution}.jpg`
- `images/${system}/background_${region}.jpg`
- `images/${system}/${country}/${language}/background.jpg`

Path have an automatic fallback system, that's particularly usefull when usign variable-depending path.
Suppose you want to display an image with translated text in it. Supported languages may extend in future recalbox release, so the fallback system is just perfect:
`images/flags/${country}-${language}.png|images/flags/${country}.png|images/flags/default.png`
In such scenario, all variables are replaced in this path. Then the path is split up on `|` (pipe) character. Then sub-path are all checked from left to right until the first existing file.

Suppose my country is Switzerland and my language is Italian.
If the file `images/flags/sw-it.png` exists, then it is taken.
Otherwize, if the file `images/flags/sw.png` exists, recalbox takes it
If both file are unvailable, then `images/flags/default.png` is taken whether it exist or not.

There is no limit in fallback system and you can add as many path you want.

### Views

### Options

### Decorations

All views, including the menu, may be customized and decorated with images, texts and more.
Background images are the most common decoration, but you can do many more!

#### Decoration objects

There are four decoration objects available:
- Images
- Texts
- Rectangles
- Video

You can declare multiple texts, images & reclangles, there is no limit.
However, **only one** video can be declared. If you declare more than one video, all declarations but the first are ignored.

#### Z-Ordering

Decoration objects may be z-ordered. That means you can specify an integer position on the Z axis making farthest object being drawn first and the nearest being drawn last.

All the view objects that are not decorations are all drawn on the Z position 0.
This means that objects with positive Z values (including 0) are drawn in the back of the view. Then the view is drawn, and finally negative Z value decorations are drawn in front of the view.

For obvious reasons, negative Z values must be used sparingly in order to not disturb the user experience!

> Negative Z-ordered objects are ignored on splash screen to avoid Recalbox logo being covered.
{.is-info}

#### Common properties

All decoration objects are drawn in an enclosing rectangle.
Object may be named. Named objects can be overriden in options or systems. Unamed objects are always drawn and cannot be overriden.

Properties:
|Name|Function|Type|Default value|Remark|
|----|:------:|:--:|:-----------:|------|
|name|Optional name|string|empty string|Only named objects can be overriden!|
|zindex|Z Index value|integer|0|Positive value are drawn in back. Negatives in front.|
|x|Left rectangle position|ratio|0| |
|y|Top rectangle position|ratio|0| |
|width|Reclangle width|ratio|0| |
|height|Rectangle height|ratio|0| |
|enabled|Object drawn?|boolean|true|Enable overrides to switch off this object.|

Examples:

```
<Rectangle zindex="1" x="0.1" y="0.2" width="30%" height="50%" ... />
<Text zindex="2" x="60%" y="30%" width="40%" height="50%" ... />
<Image zindex="1" x="0" y="0" width="1" height="1" ... />
```

#### Rectangles

Rectangles are simple filled shape with only one color. They are mostly usefull to lighten or darken rectangular areas.

Properties:
|Name|Function|Type|Default value|Remark|
|----|:------:|:--:|:-----------:|------|
|color|Rectangle color|Color Template|Default (white)|Use non-opaque colors to lighten/darken areas.|

Examples:
```
<Rectangle zindex="1" x="0.1" y="0.2" width="30%" height="50%" color="custom1" />
```

#### Texts

#### Images

Images can be jpeg or png images of any size. However, keep in mind Recalbox store images as uncompressed raw ARGB in memory and in GPU memory.
For example, a fullHD images (1920x1080) takes 8Mb of RAM and GPU RAM. See chapter _Optimizing your theme_ at the end of this page.

Properties:
|Name|Function|Type|Default value|Remark|
|----|:------:|:--:|:-----------:|------|
|path|Image path|File path|Empty|Define the path to the image file.|
|displaymode|Display mode|DisplayMode*|FixKeepRatio|Set how the image is displayed in its bounding rectangle.|
|alignment|Alignment|Alignment*|Center|Set image alignment regarding the DisplayMode.|
|blendcolor|Blend color|Color Template|Custom32 (white)|Set an optional bland color. Default is white: no blending.|
|rotation|Rotation angle|Angle|0° (no rotation)|Set an optional rotation for the image and its bounding rectangle.|
|scaling|Scale|ratio|1 (no scaling)|Set an optional scaling for the image and its bounding rectangle.|
|originx|X Origin|ratio|0.5 (center)|Set the origin X position, in image ratio, for rotation & scaling|
|originy|Y Origin|ratio|0.5 (center)|Set the origin Y position, in image ratio, for rotation & scaling|

Remarks: origin position is expressed in bounding rectangle ratio but is not constrained from 0.0 to 1.0. You may want to specify an origin point outside the bound rectangle. This may be very interesting for rotation, less for scaling.

DisplayMode values:
|Value|Behavior|
|-----|--------|
|FixKeepRatio|Stretch the image to the nearest limit while keeping the image ratio. Alignment may modify the image position (not the size)|
|MaxKeepRatio|Stretch the image to the farthest limit while keeping the image ratio. The image may be croped. Alignment may modify the image position and the image part shown (but not the size)|
|Fit|Stretch the image in both direction to perfectly fit its bounding rectangle. The ratio may be modified. Alignment has no effect.|
|Aligned|Display the image in its original size. Alignment may modify the image position and the image part shown (but not the size)|
|Tile|Display the image in its original size and repeat the image in all directions. Alignment has no effect.|

`DisplayMode` and `Alignment` display samples:
![/advanced-usage/images_illustrations.jpg](/advanced-usage/images_illustrations.jpg){.full-width}

## Advanced

### Variables

### Optimizing your theme
