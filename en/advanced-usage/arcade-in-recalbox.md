---
title: Arcade on Recalbox
description: 
published: true
date: 2021-08-08T18:43:01.698Z
tags: arcade
editor: markdown
dateCreated: 2021-06-29T15:14:12.552Z
---

![](/advanced-usage/arcade/arcade-recalbox.png)

## I - THE ARCADE CABINET

The 70's mark the beginning of the arcade history !
Each arcade uses a specific and unique hardware, often with several processors, very special sound and video chips, and always the best computer display technology of the moment.  
The first arcade machines were dedicated and usable only for the game they were delivered with (Pac-man, Space Invaders, Pong etc.). When a game reached the end of its commercial life, it was difficult to reuse the cabinet.

The arcade systems in 1986 evolved and were often based on a motherboard coupled with a game cartridge or a [**PCB**](https://en.wikipedia.org/wiki/Printed_circuit_board) (connection [**JAMMA**](https://en.wikipedia.org/wiki/Japan_Amusement_Machine_and_Marketing_Association)).  
The arcade cabinets then became generic and the hardware was closer to a home video game console or a high-end computer.
This reduced production costs and creation times and made life easier for arcade operators.

However, there are still many arcade systems that still use specific hardware optimized for a single game: car games, games with optical guns, dance games...

## II - MAME

[**MAME**](https://en.wikipedia.org/wiki/MAME) (Multiple Arcade Machine Emulator) is a versatile emulation system.

MAME's goal is to preserve decades of software history.  
As electronic technology continues to go on, MAME prevents this important vintage software from being lost and forgotten.  
This is accomplished by documenting the hardware and its operation.

### 1 - MAME Documentation

The fact that the documentation is usable serves mainly to validate its accuracy (how else can you prove that you have faithfully recreated the hardware?).  
Over time, MAME absorbed the sister project [**MESS**](https://en.wikipedia.org/wiki/MESS) in 2015 (Multi Emulator Super System). Thus MAME now documents a wide variety of (mostly retro) computers, video game consoles, calculators, in addition to the arcade video games that were its original focus.

The first public version (0.1) of MAME, by Nicola SALMORIA, was released on February 5, 1997.  
On October 21, 2010, version 0.140 of the emulator supports over 4,510 games.  
On April 4, 2020, version 0.220 supports more than 6,000 games and computers.

As MAME is a constantly evolving emulator, it is necessary to upgrade your romset along with the emulator version.

### 2 - MAME documentation principle

MAME documents this fabulous library of machines using the [**DUMP**](https://en.wikipedia.org/wiki/Dump) principle.
The dump consists in transferring the data contained in the various components (motherboard, PCB, cartridges) to a digital format to be used with emulators.

![](/advanced-usage/arcade/board-to-zip.png)

For some more or less recent machines, this documentation is accompanied by a CD-ROM, DVD or even cassette called CHD (Compressed Hunks of Data).  
Without this CHD, the emulation will be imperfect or it will not start at all.

![](/advanced-usage/arcade/rom-and-chd.png)

This documentation is not always perfect and this results in "BAD DUMP" versions of some games unfortunately and therefore imperfect or impossible emulation.

## III - ROMSETS

### 1 - Content of a romset

As we explained above, there is plenty to eat and drink in your romsets.  
Indeed the fact of having wanted to document "vintage" and all the dead memories of the planet leads us to ask the question: what do we have in a ROMSET?

Thanks to the software [**CLRMAMEPRO**](./../tutorials/utilities/rom-management/clrmamepro) it is possible to read the whole content of a romset by having beforehand a reference: the file [**.dat**](https://en.wikipedia.org/wiki/Index.dat).
This file gathers the complete list of the data which constitute the romset in relation to the emulator.
It is important to compare your romset with this .dat file via clrmamepro and to avoid picking up ROMs left and right for compatibility problems !
  
In MAME, we will find the following families:

* **STANDARD:** this is the biggest part of the romset (55GB). You will find arcade games, pinball machines, consoles, slot machines, calculators, even tamagotchis... 
* **MECHANICAL:** these are machines that are difficult to emulate because they are half mechanical / half digital. 
* **DEVICE:** those are configuration files that come with some machines for emulation. 
* **BIOS :** some systems are classified by BIOS (about 70).

### 2 - BIOS / Drivers

Some ROMs of games of a romset may require BIOS files or drivers, the best known case being the [**Neo-Geo**](https://en.wikipedia.org/wiki/Neo_Geo#Neo_Geo_MVS_and_AES) games. 

* **Neo-Geo** example : if you want to use Neo-Geo games, you will have to copy the "BIOS" file (in this case neogeo.zip) in the same directory as the game.
* [**CPS**](https://en.wikipedia.org/wiki/CP_System) example : if you want to use some CPS games, you will have to copy the "driver" file (in this case qsound_hle.zip) in the same directory as the game.

Of course if you use subdirectories for your games (like genre or hardware for example), you will have to copy the BIOS and driver files in each directory containing games that need them.
Since these BIOS and driver files are rather small in size, it is easier to copy them all into each of your subdirectories.

Where to find these BIOS and driver files you say?
Well it's quite simple : in your romset !

### 3 - The CHD

As for a BIOS or a DRIVER, the CHD file must accompany its ROM but be careful because the format and the storage of this one is quite specific.

The CHD file (not compressed) does not have the same name as the ROM but must be stored in a folder (not compressed) with the exact name of the corresponding ROM to work.  
These CHD files are rarely used with romsets because they are very large.

**Exemple ROM+CHD : sfiii3**

![](/advanced-usage/arcade/roms-chd.png)

### 4 - Types of files in a romset

* **PARENT:** The ROM of the original game that the MAME team decided to name "original version". Except for the BIOS ROM files, the ROM files for these games contain everything that is necessary for the game to work. The original set is deemed to be the most recent revision and if multiple regional versions are available, the world or US revision is used.
* **CLONES:** ROM is a different version or variant of the same game (for example, Street Fighter II Turbo is considered a variant of Street Fighter II Champion Edition). There are also the versions: Bootleg / homebrew / hack...
* **BIOS:** the common ROM used by all the games on the same machine (like Neo-Geo MVS). These are the programs used at the startup of the machine, allowing to perform basic operations, when it is turned on.

### 5 - Type of romsets: split / merged / non-merged

**There are three types of romsets: **

* **Split :** [divided] the files in common between parents and clones are only in the parent zip, to use a clone you must have both parent and clone ROMs.
* **Non-merged :** all files needed for the clone are in the clone zip.
* **Merged:** parent and clone are merged in the same zip.

**Below is an example of ROM in the different types of romsets:** in red the elements of the parent, in yellow the elements of the clone and in white the elements used for the game.  
Note that in the case of the **split** romset, the **clone** folder cannot work without the **parent** folder.

![image manquante](/advanced-usage/arcade/rom-types.jpg){.full-width}

## IV - L' ARCADE ET RECALBOX

Les différents émulateurs dans **Recalbox** font référence à **différents romsets MAME** pris en charge par ceux-ci.

_**Il est important de comparer votre romset avec un fichier .dat via clrmamepro et d'éviter de piocher des rom à droite et à gauche pour des problèmes de compatibilité.**_

### 1 - Mame

* **LIBRETRO-MAME 2003 :** Based on **ROMSET 0.78** published on December 25, 2003.
* **LIBRETRO-MAME 2003-Plus:** (also called MAME 2003+ and mame2003-plus) is a Libretro arcade system emulator kernel that focuses on high performance and extensive compatibility with mobile devices, single board computers, embedded systems and other similar platforms.
  Based on **ROMSET 0.78-0.188** (MAME 0.78 as a baseline with other ROMs based on later MAME ROM sets).
* **LIBRETRO-MAME 2010** : based on **ROMSET 0.139** published on July 29, 2010.
* **LIBRETRO-MAME 2015 :** based on **ROMSET 0.160** published on March 25, 2015.
* **LIBRETRO-MAME :** based on **ROMSET 0.230**

### 2 - FbNeo

* **LIBRETRO-FBNEO :** based on **ROMSET FBNeo 1.0.0.01**

### 3 - Neo-Geo

* **LIBRETRO-FBNEO :** based on **ROMSET FBNeo 1.0.0.01**    

### 4 - Naomi

* [**Libretro-Flycast**](./../emulators/arcade/naomi/libretro-flycast#romset-mame) : based on **ROMSET 0.230**

### 5 - Naomi GD

* [**Libretro-Flycast** ](./../emulators/arcade/naomi-gd-rom/libretro-flycast#romset-mame) : based on **ROMSET 0.230**

### **6 - Atomiswave**

* [**Libretro-Flycast**](./../emulators/arcade/atomiswave/libretro-flycast#romset-mame) : based on **ROMSET 0.230**

## V - KNOWN PROBLEMS

| Type of problem | BAD ROM | BAD EMULATOR | BAD CHD | BAD DUMP | BAD MAPPING |
| :--- | :---: | :---: | :---: | :---: | :---: |
| **My game won't start** | ✅ | ✅ | ✅ | ✅ | |
| **Game sound problem** |  | ✅ |  | ✅ |  |
| **The game I'm using is buggy, glitchy** |  | ✅ |  | ✅ |  |
| **The game launches but the keys are wrong** |  |  |  |  | ✅ |

## VI - GLOSSARY

**A**

* **AtomisWave :** arcade system created by Sammy whose architecture is similar to Sega's Naomi.

**B**

* **Boot :** refers to the start-up phase of a system, such as an arcade card or a console.
* **Bootleg :** unofficial copies of games on PCB or cartridge, usually sold much cheaper than the originals, easily recognizable because there is almost never the brand of the game editor written on the pcb or cartridge unlike the originals. It is common that bootlegs are affected by various bugs, missing music, missing sound effects, random freezes...

**C**

* **Chihiro:** name of an arcade system published by SEGA in 2002. The architecture of this platform is based on that of Microsoft's Xbox console. Among the games published, the most popular are: Outrun 2, House of the Dead 3 and Virtua Cop 3.
* **CPS 1:** Capcom Play System 1 is the name of an arcade system created by Capcom and released in 1988. Among the games published on this system, Street Fighters II is one of the most famous!
* **CPS 2:** Capcom Play System 2 is the name of an arcade system created by Capcom and released in 1993. Among the games published are: Gigawing, Marvel Vs Capcom, the Street Fighters Zero or Alpha series, Vampire Savior or Progear no Arashi.
* **CPS 3:** acronym of Capcom Play System 3. Name of an arcade system created by Capcom released in 1996. This system is very powerful in the management of 2D graphics. Have been released on this system: Jojo's Bizarre Adventure, Warzard, and the Street Fighters III series.  
* **Crystal System:** [arcade system](https://en.wikipedia.org/wiki/Arcade_video_game#Technology) in JAMMA format released in 2001 manufactured and marketed by the company BrezzaSoft. The Crystal System was created in order to replace the Neo-Geo system on the market of cheap arcade games. Let's remember that almost all the employees working at BrezzaSoft are defectors from SNK, who left the company just after bankruptcy to create BrezzaSoft. This system offers games with very advanced and pleasant 2D graphics. The Crystal System will not have the expected success and it will be a commercial disaster, it will have a very short life span. Very few games were published on this new system, but we can note games like Evolution Soccer, a soccer game copied on Super Sidekicks, and Crystal of the Kings, a medieval Beat them all in the line of Golden Axe or Knights of the Round.

**D**

* **DECO Cassette System:** [arcade cabinet](https://en.wikipedia.org/wiki/Arcade_video_game#Technology) developed by Data East. Released in 1980, it was the first arcade system that allowed the owner of the cabinet to change games. The purchaser had to buy the arcade cabinet, while the games were available on audio cassettes. All that was needed was to insert the cassette and a security dongle into the booth (this was one of the first forms of digital rights management, preventing copying).
* **Dump / Dumping / Dumper :** technique that consists in transferring the data contained in the ROMs of an arcade game to the hard disk of a computer (ROM image) to be then put at the disposal on the internet and used in emulators like MAME.

**E**

* **Emulation:** process allowing to reproduce on a computer or a console the behavior of another device, thanks to what is called an emulator. For example the Kawaks emulator allows you to play the Neo.Geo on your PC.

**H**

* **Hyper Neo-Geo 64:** it was created by the SNK company and marketed in September 1997, to succeed the Neo-Geo MVS system, which was seven years old at the time. Technically, as its name suggests, the Hyper Neo-Geo 64 is equipped with a 64-bit microprocessor that allows 3D games to be played, displaying 4096 colors simultaneously from a palette of 16.7 million colors, in a resolution of 640 x 480 pixels. Among the 7 games released: Fatal Fury, Wild Ambition, Samurai Shodown 64, Samurai Shodown 64, Warriors Rage...

**J**

* **JAMMA :** standard of connectivity for all arcade games to work on all cabinets, created in the mid 80's by the "Japanese Amusement Machine Manufacturers Association". Technically, the standard defines connections for: two eight-way joysticks, two start buttons, and six control buttons (three per player), as well as sound support (mono).

**K**

* **Konami Sytem 573:** The System 573 is a series of arcade system cards from Konami based on the original Playstation. The hardware was used mainly for Konami's Benami arcade series of video games, most commonly the Dance Dance Revolution series introduced in 1998.
* **Konami Viper:** Konami launched the Konami Viper in 1998 for arcades. This arcade uses the JAMMA+PCB system. Among the 13 games released: Jurrassic Park 3, GTI club 2, Silent Scope EX, Thrill Drive II...

**L**

* **Lindbergh:** name of an arcade system created by SEGA released in 2006. The architecture and components of this system are more or less the same as those of a PC, which contrasts with other systems created by SEGA before. Among the games released on Lindbergh are Virtua Fighters 5, Virtua Tennis 3, House of The Dead 4, After Burner Climax... Lindbherg is also the name of one of the pioneers of aviation, the American Charles Lindbergh (1902-1974). He is the first person to have made the solo crossing of the Atlantic Ocean by plane.

**M**

* **MAME:** famous arcade emulator that emulates more than 6000 arcade systems. MAME is also considered as a real virtual museum of arcade games.
* **Model 1:** is the name of an arcade system created by SEGA and released in 1992. A real technological breakthrough, it is on this system that Virtua Racing and Virtua Fighter 1 were released.
* **Model 2:** name of an arcade system created by SEGA and released in 1993. Some of the most popular games published on this system are Daytona USA, House of the Dead, Sega Rally CHampionship and Virtua Fighter 2.
* **Model 3:** name of an arcade system created by SEGA released in 1996 which became the most powerful system of its time. Some of the most popular games released on this system are Daytona USA 2, Scud Race, Sega Rally 2 and Virtua Fighter 3.
* **Motherboard:** often refers to the rack that contains all the computing components and is needed to run the games that are to be installed on it. Example with the MVS or other cartridge or disk system: the computing units, sound or graphics processors, etc. (also called the hardware) are concentrated in the motherboard while the game (the software) is contained in the cartridge or disk.

**N**

* **Namco System 246:** System 246 is a video game system for arcades based on the Playstation 2. It was created by Namco in 2001 and has undergone several evolutions: System 256, Super System 256 and System 147.
* **Naomi and Naomi GD:** cartridge and GD-ROM video game system for arcade cabinets, released in 1998 as the successor to the Sega Model 3 system. An acronym for New Arcade Operation Machine Idea, Naomi also translates to "beauty" in Japanese.
* **Neo-geo MVS:** (Multi Video System) is a JAMMA-compatible arcade video game system for arcades created by the Japanese company SNK in 1990.
* **Nintendo Playchoice 10:** based on the Nintendo Entertainment System (NES), the Playchoice 10 motherboard allowed up to 10 ROMs to be installed at a time. Unlike most other arcade games, the coins add time rather than credits, allowing you to switch games on the fly whenever you want.

**P**

* **PCB:** initials of "Printed Circuit Board". A board designed to hold electronic components. In the arcade, a PCB is a printed circuit board that contains a game and is connected to the arcade cabinet by the JAMMA connector. 
* **PGM:** initial of "Poly Game Master" developed by IGS (International Games System). This arcade system is similar to the MVS in its performance, appearance and use.

**R**

* **ROM:** acronym for "Read Only Memory "** also called "read-only memory".
* **ROM Image:** copy ROMs (Read Only Memory), the electronic components found on arcade cartridges and cards that contain the game. Copying an arcade game to a PC's hard drive is called "dumping" a game and requires special hardware. The copies/images of the roms are then put on the internet in the form of files generally compressed in .zip readable by emulators, then downloaded by you and me. For the sake of simplicity, ROM images are simply called "roms" by most emulation sites.

**S**

* **Sega Hikaru:** Name of a system created by the Sega company in 1999. The Hikaru was originally designed to host the game Brave Fire Fighters. It has the ability to display on screen complex graphics representing fire or water movements. It was the first arcade system to feature Phong shading on the screen. This system had only six games and was soon abandoned, because it was too expensive to operate compared to the Naomi2.
* **Sega Mega Play:** Arcade system based on the Sega Megadrive home console. When you insert coins, you buy credits like a standard arcade game.
* **Sega Mega-Tech:** arcade system based on the Sega Megadrive home console. When you insert a coin, you buy time, the game is over when your time is up, you can add extra coins during the game to gain extra time. 
* **Set:** In arcade language, a set is a game or BIOS consisting of multiple ROMs. 
* **ST-V Titan** (acronym for Sega Titan Video): Sega's arcade system launched in 1994, with the same architecture as the Sega Saturn console (except that the ST-V used cartridges). One of the goals was to make life easier for programmers, to make arcade to console conversions much faster, and to get the most faithful conversions possible.

**T**

* **Triforce:** name of an arcade system jointly developed by Nintendo, SEGA and Namco and released in 2002. The architecture of this system is based on that of Nintendo's GameCube console. Among the games published, the most popular are: F-Zero AX, Virtua Striker 4 and Mario Kart Arcade GP. Note: the name Triforce represents the alliance between the 3 companies but is also a reference to The Legend Of Zelda series.

## VII - Sources:

* [http://www.system16.com/](http://www.system16.com/)
* [https://en.wikipedia.org/wiki/](https://en.wikipedia.org/wiki/)