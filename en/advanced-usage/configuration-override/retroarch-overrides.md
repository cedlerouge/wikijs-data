---
title: RetroArch override
description: Examples of keys to override the retroarch configuration.
published: true
date: 2021-10-03T22:22:31.307Z
tags: retroarch, override
editor: markdown
dateCreated: 2021-06-28T17:25:04.811Z
---

Here is a non-exhaustive list of keys that can be altered in a `.retroarch.cfg` type override file, these modifications can only be done if the emulator is a RetroArch core.

> A \* after the value means that the parameter can be overrided also via a .recalbox.conf
{.is-info}

## Audio

* `audio_enable = "true"` : turn audio on or off.
* `audio_volume = "0.000000"` : adjust the volume gain, 0 = default volume by default.

## RetroArch Menu

* `quick_menu_show_save_content_dir_overrides = "false"` : show or hide the option Configuration replacement for the directory.
* `quick_menu_show_save_core_overrides = "false"` : show or hide the option Configuration replacement for the core.
* `quick_menu_show_save_game_overrides = "false"` : show or hide the option Configuration replacement for the game.

> Configuration overrides are a feature of RetroArch, similar to overrides, but retain a lot of information, in the context of Recalbox, better to prefer overrides if possible.
{.is-info}

## Debug

* `fps_show = "true"` : show in-game FPS.
* `menu_driver = "ozone"` : choose the RetroArch menu, usually ozone, excepted on GPi Case, where rgui is used.
* `menu_enable_widgets = "true"` : enabling in-game popups, if set to false, will display notifications as yellow text.

## Folders

* `recording_output_directory = ""` : folder where the video recordings go.
* `savefile_directory = ""` : where to save / load backups.
* `savestate_directory = ""` : where to save / load snapshot backups.
* `screenshot_directory = ""` : folder where screenshots go.

## Translation Service

* `ai_service_enable = "true"` : activate or deactivate the translation service.
* `ai_service_mode = "0"` : translation service mode, 0: image, 1: voice.
* `ai_service_source_lang = "0"` : language to read for the translation service, 0 = not specified.
* `ai_service_target_lang = "1"` : language to translate into, 1 = English, 3 = French.
* `ai_service_url =` : internet link of the service to be used.

## Overlays

* `aspect_ratio_index = "23"` : ratio index, 23 = Custom.
* `input_overlay = ""` : link to the overlay configuration file.
* `input_overlay_enable = "true"` : activation of the overlay.
* `input_overlay_hide_in_menu = "true"` : overlay hidden in RetroArch menu.

### Coordinates to be defined with the overlay

* `custom_viewport_height = ""`
* `custom_viewport_width = ""`
* `custom_viewport_x = ""`
* `custom_viewport_y = ""`
* `video_message_pos_x = "0.050000"`
* `video_message_pos_y = "0.050000"`

## Netplay

* `netplay_nickname = ""` : Netplay nickname.

## Screen rotation

* `video_rotation = "1"` : rotates the video rendering, 0 = normal, 1 = 90 °, 2 = 180 °, 3 = 270 °, pay attention to the aspect ratio.

## Joystick and Directional Pad

To dissociate / associate the Directional Pad to one of the joysticks:

* `input_player1_analog_dpad_mode = "0"` : dissociate.
* `input_player1_analog_dpad_mode = "1"` : associate with the left joystick.
* `input_player1_analog_dpad_mode = "2"` : associate with the right joystick.
* `input_player1_analog_dpad_mode = "3"` : forcefully associate with the left joystick.
* `input_player1_analog_dpad_mode = "4"` : forcefully associate with the right joystick.

## Remapping of hotkeys

> The settings for changing the hotkeys depend on the joystick mapping in Recalbox. If the controller changes, the configuration overloaded with these lines may no longer work.
{.is-warning}

To have the numerical value for each key of your controller, look in the file `/recalbox/share/system/configs/retroarch/retroarchcustom.cfg` for the value of the desired key according to this table:

| Key name | Key Value associated |
| :--- | :--- |
| A | input_player1_a_btn |
| B | input_player1_b_btn |
| X | input_player1_x_btn |
| Y | input_player1_y_btn |
| Up | input_player1_up_btn |
| Down | input_player1_down_btn |
| Left | input_player1_left_btn |
| Right | input_player1_right_btn |
| Select | input_player1_select_btn |
| Start | input_player1_start_btn |
| L | input_player1_l_btn |
| L2 | input_player1_l2_btn |
| L3 | input_player1_l3_btn |
| R | input_player1_r_btn |
| R2 | input_player1_r2_btn |
| R3 | input_player1_r3_btn |
| Left Stick to the Up | input_player1_l_y_minus_axis |
| Left Stick to the Down | input_player1_l_y_plus_axis |
| Left Stick to the Left | input_player1_l_x_minus_axis |
| Left Stick to the Right | input_player1_l_x_plus_axis |
| Right Stick to the Up | input_player1_r_y_minus_axis |
| Right Stick to the Down | input_player1_r_y_plus_axis |
| Right Stick to the Left | input_player1_r_x_minus_axis |
| Right Stick to the Right | input_player1_r_x_plus_axis |

> The modifications to be made to the following values must be made in the override file.
>
> The previous file is only used to observe the current mapping.
{.is-warning}

* `input_enable_hotkey_btn =`: Hotkey button.
* `input_screenshot_btn =`: Button to take a screenshot.
* `input_exit_emulator_btn =`: Button to exit an game.
* `input_load_state_btn =`: Button to load a save state.
* `input_save_state_btn =`: Button to save a save state.
* `input_menu_toggle_btn =`: Button to open the RetroArch Menu.
* `input_reset_btn =`: Button to reset the emulator/game.
* `input_ai_service_btn =`: Button to translate the current screen view.

For example, `The Legend of Zelda: Link's Awakening` on Game Boy requires to use `Start + Select + A + B` to save. If your controller does not have a home button, and the hotkey is on `Select`, natural game saving will be impossible. Putting the `input_enable_hotkey_btn` key on the R key of your controller for this game only can be done. The value of the `input_player1_r_btn` key is 4 for the R key, so you will need to enter `input_enable_hotkey_btn = 4` in the game override.