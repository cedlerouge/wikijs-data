---
title: Systems display
description: How it works and displayed systems edit.
published: true
date: 2021-08-08T18:39:40.315Z
tags: display, systems
editor: markdown
dateCreated: 2021-06-29T12:51:24.737Z
---

## How the display works

The display of systems in the Recalbox menu is handled by the `es_systems.cfg` file. This file is located in the `/recalbox/share_init/system/.emulationstation/es_systems.cfg` folder.

It contains the names of the different systems that are supported by your version of Recalbox. It is constructed as follows:

```xml
<?xml version="1.0"?>
<systemList>
  <system>
    <fullname>Panasonic 3DO</fullname>
    <name>3do</name>
    <path>/recalbox/share/roms/3do</path>
    <extension>.iso .ISO .cue .CUE .chd .CHD</extension>
    <command>python /usr/lib/python2.7/site-packages/configgen/emulatorlauncher.pyc %CONTROLLERSCONFIG% -system %SYSTEM% -rom %ROM% -emulator %EMULATOR% -core %CORE% -ratio %RATIO% %NETPLAY%</command>
    <platform>3do</platform>
    <theme>3do</theme>
    <emulators>
      <emulator name="libretro">
        <cores>
          <core>4do</core>
        </cores>
      </emulator>
    </emulators>
  </system>
  <system>
      [. . .]
  </system>
  [. . .]
</systemList>
```

The display of the systems respects the order in which they are listed in this file. It also contains the configuration of these systems.

## Change the display order

>Do NOT change the original `es_systems.cfg` file (which is in the `share_init` directory). In case of problems with changes made later, this file remains the only source for getting Recalbox to work properly.
{.is-warning}

Changing the display order should only be done from the `es_systems.cfg` file in the ` /recalbox/share/system/.emulationstation/es_systems.cfg` directory.

Originally, this file does not exist. You must either copy the original file or create a new file. Once the new file is created, it is possible to put the systems in the order you want. The configuration of the systems will still be taken from the original `es_systems.cfg` file, but the order of the systems will be as defined in the new file.

If, in the new file, a system is missing or incorrectly filled in, priority is given to the original file. For the new file, there are only 2 entry keys that are mandatory: "**fulllname**" and "**platform**", all others are optional. The file should be constructed at least in the following way:

```xml
<?xml version="1.0"?>
<systemList>
  <system>
    <fullname>Nintendo Entertainment System</fullname>
    <platform>nes</platform>
  </system>
  <system>
    <fullname>Family Computer Disk System</fullname>
    <platform>fds</platform>
  </system>
  <system>
    <fullname>Super Nintendo Entertainment System</fullname>
    <platform>snes</platform>
  </system>
  <system>
    <fullname>Satellaview</fullname>
    <platform>satellaview</platform>
  </system>
  [. . .]
</systemList>
```

## Add a custom system

>You must **NOT MODIFY** the original `es_systems.cfg` file (which is in the `share_init` directory). If there are any problems with the changes you make later, this file is the only source to make Recalbox work properly.
{.is-warning}

This manipulation does not add a new emulator to Recalbox but adds a new system entry to the selection menu.

It is possible to combine the modification of the system order with the addition of one or more custom systems.

As with changing the system order, adding a custom system must be done only from the `es_systems.cfg` file in the following directory `/recalbox/share/system/.emulationstation/es_systems.cfg`.

Originally, this file does not exist. You must either copy the original file or create a new file. Once the new file is created, it is now possible to add a new system to it.

If, in the new file, a system is incorrectly filled in, priority is given to the original file. For the new file, all the input keys are mandatory. So, to create a new system, the easiest way is to start from an existing system (and corresponding to the roms you want to include) and to modify only what is strictly necessary:

- _**"fullname"**_ : Allows to give the name of the new system.
- _**"path"**_ : Allows to indicate the directory containing the roms of the new system.
- _**"theme"**_ : Allows you to indicate which theme to use. It is necessary, beforehand, to create this new theme (logo, background, ...)

**All the other entries must not be modified.**

Here is an example of an addition for a system based on the SNES to include only translated roms:

```xml
<?xml version="1.0"?>
<systemList>
  <system>
    <fullname>Super Nintendo Fan Trad</fullname>
    <name>snes</name>
    <path>/recalbox/share/roms/snestrad</path>
    <extension>.smc .sfc .mgd .zip .7z</extension>
    <command>python /usr/lib/python2.7/site-packages/configgen/emulatorlauncher.pyc %CONTROLLERSCONFIG% -system %SYSTEM% -rom %ROM% -emulator %EMULATOR% -core %CORE% -ratio %RATIO% %NETPLAY%</command>
    <platform>snes</platform>
    <theme>snestrad</theme>
    <emulators>
      <emulator name="libretro">
        <cores>
          <core>snes9x2005</core>
          <core>snes9x2010</core>
          <core>snes9x2002</core>
        </cores>
      </emulator>
    </emulators>
  </system>
</systemList>
```