---
title: 📋 MORE INFORMATIONS
description: How to contribute, licenses, useful links...
published: true
date: 2021-08-08T16:21:14.991Z
tags: more informations
editor: markdown
dateCreated: 2021-05-28T22:02:46.144Z
---

You will find here the general presentation of Recalbox, who we are, the licenses and the possibility to contribute to the project in different ways 