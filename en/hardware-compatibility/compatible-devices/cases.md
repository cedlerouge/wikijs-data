---
title: Cases
description: 
published: true
date: 2021-08-08T18:27:26.513Z
tags: compatibility, cases
editor: markdown
dateCreated: 2021-08-05T19:12:20.834Z
---

## Pi ZERO/Pi ZERO W

### GPi CASE

![For Pi Zero and Pi Zero W](/compatibility/cases/gpicase.png)

[Link to buy](https://www.kubii.fr/consoles-retro-gaming/2719-gpi-case-retroflag-kubii-3272496299276.html)

## PI2/PI3/PI3B/PI3B+

### NESPi CASE

![](/compatibility/cases/nespicase.png)

[Link to buy](https://www.reichelt.com/fr/en/case-for-raspberry-pi-3-nes-design-gray-rpi-nespi-case-p211465.html?&trstct=pos_0&nbc=1)

### NESPi CASE PLUS

![For Pi2, Pi3, Pi3B and Pi3B+](/compatibility/cases/nespicaseplus.png)

[Link to buy](https://www.kubii.fr/boitiers-et-supports/2036-boitier-nespi-case-pour-raspberry-pi-3-2-b-kubii-327249600861.html)

### SUPERPI CASE J

![For Pi2B, Pi3B and Pi3B+](/compatibility/cases/superpicase.png)

[Link to buy](https://www.kubii.fr/boitiers-et-supports/2283-superpicase-j-kubii-3272496011922.html)

### SUPERPI CASE U

![For Pi2, Pi3 and Pi3B+](/compatibility/cases/superpicaseusa.png)

[Link to buy](https://www.amazon.com/dp/B07G34TTKL?m=A3I7DCARNWUK2P&ref_=v_sp_detail_page&th=1)

### SUPERPI CASE U (New)

![For Pi2, Pi3 and Pi3B+](/compatibility/cases/superpicaseusanew.png)

[Link to buy](https://www.amazon.com/dp/B07W5L95KK?ref=myi_title_dp)

### MEGAPi CASE M

![For Pi2B, Pi3B and Pi3B+](/compatibility/cases/megapicase.png)

[Link to buy](https://www.kubii.fr/consoles-retro-gaming/2337-boitier-megapi-case-kubii-3272496012646.html)

## PI4

### NESPi 4 CASE

![For Pi4](/compatibility/cases/nespi4case.png)

[Link to buy](https://www.kubii.fr/boitiers/3031-boitier-nespi-case-pour-raspberry-pi-4-3272496302303.html)

### Argon One

![For Pi4](/compatibility/cases/argonone.png)

[Link to buy](https://www.kubii.fr/boitiers-et-supports/2762-boitier-argon1-pour-raspberry-pi-4-3272496299672.html)

### Thingivers 

There is no preview, there is much differents cases to print with a 3D printer.

[Link to buy](https://www.thingiverse.com/tag:raspberry_pi_4)

## PI4 Functional cheap case

### GeeekPi

![For Pi4](/compatibility/cases/geekpi.png)

[Link to buy](https://www.amazon.fr/dp/B07XXQ34PZ/ref=cm_sw_r_cp_apa_i_MdpqFb2V1Q6F6)

### GeeekPi

![For Pi4](/compatibility/cases/geekpi2.png)

[Link to buy](https://www.amazon.fr/GeeekPi-Raspberry-Ventilateur-40X40X10mm-Dissipateurs/dp/B07XCKNM8J/ref=sr_1_3?__mk_fr_FR=%C3%85M%C3%85%C5%BD%C3%95%C3%91&dchild=1&keywords=boitier+pi+4&qid=1598103872&sr=8-3)

### Bruphny

![For Pi4](/compatibility/cases/bruphny.png)

[Link to buy](https://www.amazon.fr/Bruphny-Ventilateur-Alimentation-Dissipateurs-Adaptateur/dp/B07WN3CHGH/ref=sr_1_5?__mk_fr_FR=%C3%85M%C3%85%C5%BD%C3%95%C3%91&dchild=1&keywords=boitier+pi+4&qid=1598103952&sr=8-5)

## Odroid XU4

### Gaming case

![For XU4](/compatibility/cases/ogst.png)

[Link to buy](https://www.kubii.fr/odroid/2214-boitier-gaming-pour-odroid-xu4-kubii-3272496011250.html)