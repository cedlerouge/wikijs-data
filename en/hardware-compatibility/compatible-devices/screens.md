---
title: Screens
description: 
published: true
date: 2021-08-08T18:28:43.111Z
tags: compatibility, screens
editor: markdown
dateCreated: 2021-08-05T22:01:25.700Z
---

### TFT HDMI screen

| Device | Size | Resolution | Status | Comments |
| :---: | :---: | :---: | :---: | :---: |
|  | 5" | 800x480 | ✅ | [banggood](https://www.banggood.com/5-Inch-800-x-480-HDMI-TFT-LCD-Touch-Screen-For-Raspberry-PI-2-Model-B-B-A-B-p-1023438.html) |
| PI-TOP CEED | 14" | 1366×768 | ✅* | Works but no sound. Need to install "pt-speaker" package [Source](https://www.neozone.org/tests/test-pi-top-ceed-lordi-ideal-pour-les-makers-en-herbe/) |

### TFT SPI Bus screen

| Périphérique | Taille | Résolution | État | Commentaires |
| :---: | :---: | :---: | :---: | :---: |
|  |  |  |  |  |

### TFT DPI Bus screen

| Périphérique | Taille | Résolution | État | Commentaires |
| :---: | :---: | :---: | :---: | :---: |
|  |  |  |  |  |