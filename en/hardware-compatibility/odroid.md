---
title: Odroid
description: 
published: true
date: 2021-08-08T18:32:58.376Z
tags: compatibility, odroid
editor: markdown
dateCreated: 2021-08-05T22:41:44.416Z
---

## Odroid XU4

![](/compatibility/xu4-exynos-8-coeurs.jpg)

ODROID-XU4 is a new generation of computing device with more powerful and more energy efficient hardware and a smaller form factor. 
Implementing the eMMC 5.0, USB 3.0 and Gigabit Ethernet interfaces, the ODROID-XU4 offers incredible data transfer speeds, a function that is increasingly required to support the advanced processing power of ARM devices. 
This allows users to truly experience a computer upgrade, especially with faster startup, web browsing, networking, and 3D games. 

### Features:

* **CPU**: 
  * Exynos 5422 (ARM Cortex-A15 Quad-Core @ 2.0 GHz / ARM Cortex-A7 Quad-Core @ 1.4 GHz)
* **GPU**:
  * Mali™ -T628 MP6 3D Accelerator (OpenGL ES 3.1 / 3.0 / 2.0 / 1.1 and OpenCL 1.2 Full profile)
* **RAM**:
  * 2Go LPDDR3 RAM PoP (750Mhz, memory bandwidth 12GB / s, bus 2x32 bits)
* **Connectors**: 
  * 1 X RJ45 Female
  * 1 X USB 2.0
  * 2 X USB 3.0
  * 1 X HDMI
  * MicroSD card slot (up to 128 Go)
* **Additional connector(s)**: 
  * Storage (Option) eMMC module plug: eMMC 5.0 Flash storage (up to 64 GB)
  * HDD / SSD SATA interface (optional) SuperSpeed USB adapter (USB 3.0) to Serial ATA3 for 2.5 "/ 3.5" HDD and SSD storage Network
  * USB sound card (Option)
  * WiFi (Option): USB IEEE 802.11 ac / b / g / n 1T1R WLAN with antenna (external USB adapter)
* **Network**:
  * Ethernet LAN 10/100 / 1000Mbps Fast Ethernet with RJ-45 socket (Auto-MDIX support)
* **Power supply**:
  * 4,8Volt ~ 5,2Volt (5V / 4A recommended)
* **Dimension**:
  * 83 x 58 x 20 mm (poids: 38 gr) without cooler approx.

## Odroid XU4Q

![](/compatibility/xu4q.jpg)

Whether you want to build a media center, a game console, an on-board surveillance system or anything else, the ultra-compact **Odroid XU4Q** motherboard will be an ideal choice. Embedding a Samsung Exynos 5422 Octo-Core 2.0 GHz processor and 2 GB of RAM, it is a very affordable solution. 
As for the heat sink already mounted on the card, it will rotate only when the CPU load is high and the temperature reaches a minimum threshold.

### Features:

* **CPU**:
  * Samsung Exynos 5422 (ARM Cortex-A15 Quad-Core clocked at 2.0 GHz / ARM Cortex-A7 Quad-Core clocked at 1.4 GHz)
* **Number of cores**:
  * 8
* **GPU**:
  * Mali T628 MP6 OpenGL ES 3.1 / 3.0 / 2.0 / 1.1 and OpenCL 1.2
* **RAM**:
  * 2 Go LPDDR3
* **Connectors**:
  * 1 X RJ45 Female
  * 1 X USB 2.0
  * 2 X USB 3.0
  * 1 X HDMI Female
  * microSD card, micro SDHC, micro SDXC (compatible)
* **Additional connector(s)**:
  * 1 X GPIO 12 pins (header)
  * 1 X GPIO 30 pins (header)
* **Network**:
  * Ethernet LAN 10/100/1000 Mbps
* **Power supply**:
  * 5V / 4A (_transformer not supplied_)
* **Dimensions**:
  * 83 x 58 x 20 mm
* **Weight**:
  * 38 g

## Odroid Go Advance 2

![](/compatibility/odroid_go_advance.png)

A D.I.Y portable video game console dedicated to emulation and retrogaming, inspired by the Gameboy Advance look and available in 2 colors  
Designed as a complete and seamless assembly kit, offering a fun and playful assembly, easily accessible to allCompatible with recent game systems (Playstation, PSP, ..) and emulators like Recalbox

Speakers and 3.5mm jack, brightness control, console sleep mode, "low power" mode for more than 7 hours of autonomy, ...

* **CPU**:
  * RockChip RK3326 (Quad-Core ARM Cortex A35 1,3 GHz)
* **GPU**:
  * Mali-G31 Dvalin
* **Memory**:
  * 1 Go (DDR3L 786 Mhz, 32-bit bus width)
* **Screen**:
  * LCD TFT 3,5 pouces 320 x 480 (ILI9488, interface MIPI)
* **Battery**:
  * Li-Polymer 3.7V / 3000mAh, up to 9 hours of play time
* **Inputs**:
  * F1, F2, F3, F4, F5, F6, A, B, X, Y, D-Pad, L1-button, R1-button, L2-button, R2-button, analogic joystick
* **Connectors**:
  * 2.0 Hôte USB 2.0 x 1
  * port 10 pins (I2C, GPIO, IRQ to 3,3 volts)
* **Headset**:
  * Jack, 0.5Watt 8&#x3A9; Mono
* **Storage**:
  * Flash SPI (start 16 Mo), Micro SD card slot (compatible UHS-1)
* **Power supply**:
  * Input 5V, power connector USB-C
* **Wi-Fi**:
  * 2.4Ghz 802.11b / g / n internal (ESP-WROOM-S2)
* **Consumption**:
  * Game emulation: 300 ~ 550mA (depends on the backlight brightness and the type of game emulations and wireless use)
  * Power off mode: < 1mA
  * If you disable WiFi, the power consumption will be the same as the previous revision 1.0.
* **Charging time**:
  * 2,5 ~ 3 hours when turned off.
  * In game, 4 to 5 hours.
* **Dimensions**:
  * 155x72x20 mm (6,1 x 2,8 x 0,8 pouces)
* **Weight**:
  * 349 g

## Odroid Go Super

![](/compatibility/odroid-go-super.png)

With a format close to the one of the Switch Lite, the new Odroid Go Super tries to conquer the hearts of retrogaming fans! With many of the same elements as its predecessor, the Odroid Go Advance, this version marks an important evolution in HardKernel's portable console range.

* **CPU**:
  * RockChip RK3326 (Quad-Core ARM 64bit Cortex-A35 1,3 GHz)
* **GPU**:
  * Mali-G31 MP2 OS Ubuntu 18.04. 19.10 and 20.04 on Kernel 4.4 Aarch64
* **Memory**:
  * 1 Go (DDR3L 786 Mhz, 32 bits bus width)
* **Storage**:
  * Flash SPI (start 16 Mo), Micro SD card slot (compatible UHS-1)
* **Display**:
  * 5 inches 854 × 480 TFT LCD (wide angle display, MIPI-DSI interface)
* **Audio**:
  * Headset, mono speakers 0,5 W 8Ω
* **Battery**:
  * Li-Polymer 3,7 V / 4000 mAh (76,5 × 54,5 × 7,5 mm (L * W * T)), up to 10 hours of game
* **Power supply**:
  * Input DC 5 V, USB-C.
* **Maximum drawing current**:
  * 1.5A
* **Inputs/Outputs**:
  * USB host 2.0 x 1, port 10 pins (I2C, GPIO, IRQ at 3,3 volts)
* **Inputs**:
  * F1, F2, F3, F4, F5, F6, A, B, X, Y, D-Pad, L1-button, R1-button, L2-button, R2-button, analogic joystick, analogic joystick 2
* **Wireless**:
  * Optional USB Wifi adapter
* **Consumption**:
  * Game emulation: 350 ~ 600mA (depends on the backlight brightness and the type of game emulations and wireless use)
* **Power off mode**:
  * <1mA
* **Charging time**:
  * 3,4 ~ 4 hours when turned off. In game 4,5 to 5,5 hours.
* **Dimensions**:
  * 204x86x25 mm
* **Weight**:
  * 280 g