---
title: Compatible devices
description: 
published: true
date: 2021-08-08T18:30:50.467Z
tags: compatibility, devices
editor: markdown
dateCreated: 2021-08-05T19:07:42.292Z
---

This is a project led by several community members to share all the devices that work "plug and play" and the devices that do not.

Please feel free to make any additions or changes to create a compatibility database for Recalbox!