---
title: Package license
description: 
published: true
date: 2021-08-08T18:38:50.354Z
tags: license, packages
editor: markdown
dateCreated: 2021-08-06T07:47:46.976Z
---

Recalbox is built using [Buildroot](https://buildroot.org/), which bundles many packages with different licenses and produces a complete operating system.

Each package is covered and protected by its own license(s) and remains the property of its respective author(s).

We [have been](https://gitlab.com/recalbox/recalbox/issues/1501) working on making the [legal information](https://buildroot.org/downloads/manual/manual.html#_complying_with_open_source_licenses) functional on the Recalbox source code in order to give an exhaustive overview of all licenses used by each package.

In the meantime, we can only invite you to manually check the license(s) of each package.

>Although some package licenses may be incompatible with each other, Recalbox is not considered a "derivative work" or a "modified version" of them, but a simple aggregation of them.
>
>As such, the virality of some licenses does not impact the licensing of other packages or the licensing of Recalbox itself.
>
>See the [General Public License (GPL) FAQ](https://www.gnu.org/licenses/gpl-faq.fr.html) for more details.
{.is-info}