---
title: 📜 Licensing model
description: 
published: true
date: 2021-08-08T18:37:28.090Z
tags: license, model
editor: markdown
dateCreated: 2021-08-06T07:46:40.057Z
---

Recalbox includes emulators and other tools that can be used under certain licenses.