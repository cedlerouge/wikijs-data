---
title: Notice Netplay Dolphin
description: Play online on Gamecube and Wii
published: true
date: 2021-08-08T19:06:14.304Z
tags: dolphin, netplay
editor: markdown
dateCreated: 2021-08-04T18:47:20.161Z
---

>Please note that in order to play online at the Gamecube, some configurations are required as well as a keyboard and a mouse.
{.is-info}

## Access the Netplay menu

* Launch any Gamecube or Wii game.
* In game, with a keyboard, press `ALT` + `F4` to access the Dolphin menu.
* In the menu, using your mouse, open the `Tools` menu.

![](/basic-usage/features/netplay/dolphin-netplay-tools-menu.png){.full-width}

### 1. Create a Netplay game session

* Click on `Start Netplay...`

![](/basic-usage/features/netplay/dolphin-host-netplay.png){.full-width}

* Click on the `Host` tab, select a region in the dropdown menu above the games list, select your game in the list and click the 'Host' button.

>You have the possibility on this window to define a password for the Netplay room as well as the port to use if you need to use a specific port.
{.is-info}

![](/basic-usage/features/netplay/dolphin-netplay-host-setting.png){.full-width}

* A chat window will appear and your netplay session can be joined!

![](/basic-usage/features/netplay/dolphin-netplay-host-chat-room.png){.full-width}

>In most games, add about 1 Buffer per 15 ms of latency per client.
{.is-warning}

>Two players with 50 ms latency is about 3 to 4 buffers, while three players with 50 and 65 ms latency is about 7 buffers.
{.is-success}

* To start the game, click `Start` !

### 2. Join a Netplay game session

* Click on `Browse NetPlay Sessions....`.

![](/basic-usage/features/netplay/dolphin-netplay-client-en.png){.full-width}

* In the Dolphin Netplay lobby, you can see all currently online games.

![](/basic-usage/features/netplay/dolphin-netplay-client-lobby-en.png){.full-width}

* Double-click on the session to access to the chat window.

>If a password has been set, you will be prompted to enter it at this time.
{.is-info}

* Wait until the game started by the host into the netplay session!

![](/basic-usage/features/netplay/dolphin-netplay-client-chat-room-en.png){.full-width}