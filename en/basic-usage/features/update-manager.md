---
title: Update manager
description: 
published: true
date: 2021-08-08T19:04:46.475Z
tags: update, manager, 7.0+
editor: markdown
dateCreated: 2021-06-29T09:03:04.773Z
---

![](/basic-usage/features/update.png)

## A new way to update!

Since Recalbox 7, the update system has been completely revised and cannot work with older versions.

* The new update system is much faster:
  * _**5 to 10mn maximum for an update**_ (excluding download time of course). 
* The new update system is much more robust:
  * No more crashing updates that force you to start all over again (even if it was rare, fortunately!) 
* New updates will be managed by EmulationStation: progress display, error handling, etc.
* You can update offline in a very simple way: by copying the image of the new version in a directory of the SD card accessible from all operating systems: Windows, Linux and macOS! A small reboot, the system detects the image, performs the update and you're done!
* It will now be possible to place your own boot videos, in an easily accessible directory on the network (in `/share/bootvideos/`) and to choose if the player should play videos only among those of Recalbox, only among yours, or among all that it has at disposal.
  * What's that got to do with updates you ask? Nothing, it's just that this new possibility is linked to the new structure of Recalbox to facilitate updates! 
* If you don't like those who rip off Recalbox as much as their customers, _**update notifications can no longer be disabled**_ in EmulationStation: only the intrusive popup notification can be disabled. But you will still see a non-intrusive popup notification that an update is available.

## How it works

The update can be done in 2 ways: online or offline.

## {.tabset}

### Online method (online)

The online method is the easiest:

* In EmulationStation, press the `START` button on your controller.
* Go to `UPDATES`.
* If an update is available, you will see the line `UPDATE AVAILABLE` with `YES` on the right. If it is, go to the line `START UPDATE` and confirm.
* The update will start to download.
* Once downloaded, your Recalbox will automatically restart and the update will be performed.

### Offline method

This way of updating is a bit more complex to do.

* Go to this page where you will find all the images of the latest version of Recalbox.
* Download the image corresponding to your architecture (rpi / odroid / pc) and the corresponding `.SHA1` file.
* Once downloaded, there are 2 possibilities :
  * You can use a software such as [WinSCP](./../../../tutorials/system/access/network-access-winscp), [Cyberduck](./../../tutorials/system/access/network-access-cyberduck) or [MobaXTerm](./../../.. /tutorials/system/access/network-access-mobaxterm) to put the 2 files in the `/boot/update/` directory (by mounting the [boot partition](./../../tutorials/system/access/remount-partition-with-write-access) in read/write mode).
  * Take the SD card of your switched off Recalbox, put it in your PC, open the drive named `RECALBOX` and put the 2 files in the directory `/update`.
* Once this is done, start or restart your Recalbox and the update will take place.

##

During the update, if all 7 bars immediately turn red instead of white, note the number of white bars that were visible and contact support ([forum](https://forum.recalbox.com) or [Discord](https://discord.gg/NbQFbGM)).