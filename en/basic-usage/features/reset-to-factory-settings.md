---
title: Reset to factory settings
description: 
published: true
date: 2021-09-04T17:41:56.170Z
tags: reset, factory, settings, 7.2+
editor: markdown
dateCreated: 2021-06-29T09:28:19.304Z
---

## Overview

Since Recalbox 7.2, there is a new option called `Reset to factory settings` that allows you to reset your system to the same state after doing a brand new clean install. This option allows you to return your system to a fresh state with more ease than having to regularly re-install.

>This option **does not touch** your bios, games and saves!
{.is-success}

## Using the option

This option is located in EmulationStation. To do this, just press `START`, go to `ADVANCED SETTINGS` and select `RESET TO FACTORY SETTINGS`.

![](/basic-usage/features/resetfactory1-en.png){.full-width}

Once you have selected the option, you will have two warning windows.

![](/basic-usage/features/resetfactory2-en.png){.full-width}

![](/basic-usage/features/resetfactory3-en.png){.full-width}

>Once these two windows are validated, **you will not be able to go back**.
{.is-danger}

When the restoration to factory settings is finished, you will find your Recalbox installation all clean !