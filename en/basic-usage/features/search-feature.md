---
title: Search
description: 
published: true
date: 2021-08-08T19:03:46.327Z
tags: search
editor: markdown
dateCreated: 2021-06-29T08:53:35.956Z
---

## Presentation

Long requested by our dear users, here it is at last!

_**Quickly search your game collection by typing a few letters...**_

It's a real time search, the list refreshes as you type. You can search in names, files, descriptions, or everywhere at once.

Of course, we couldn't offer such a feature without a thorough review of the virtual keyboard.  
Sitting in front of your TV, controller in hand, what could be more annoying than walking around on the old full-screen keyboard, where entering a single character required far too many manipulations.

We've redone a brand new, beautiful and much more ergonomic one: an arcade-style keyboard, with a character wheel where selecting, deleting and moving the cursor is a real pleasure.

We can't give you all the details, but you should know that almost all the buttons/dpad/joystick of a full controller are used by the new keyboard! Of course, it is natively compatible with a physical keyboard. And to make it even better, it is semi-transparent, so you can see in real time what is going on underneath.

Without further ado, some pictures!

![](/basic-usage/features/search.png){.full-width}

## How it works

* By pressing `R1` from the console menu, you can bring up the search function.
* Start writing the name of a game (Example above: Super).
* Enjoy the visual of the list of games starting with your search word.
* You just have to select the game you want to play.