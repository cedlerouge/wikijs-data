---
title: Internal Scraper
description: 
published: true
date: 2021-08-08T19:01:23.452Z
tags: internal, scraper
editor: markdown
dateCreated: 2021-06-29T11:33:00.969Z
---

## Introduction

This time, we didn't beat around the bush: we completely rebuilt the internal scraper, starting from scratch. There is nothing left of the old one, not even a crumb.

The result is a brand new scraper, much cleaner, faster, with a lot of options, and _**which fully exploits your ScreenScraper account**_ (number of threads, quota, ...).

![](/basic-usage/features/internalscraper1.png)

## Usage

* In the system list, press the `START` button and go to `SCRAPER`.
* Here you will have several scraping options:
  * `SCRAPE FROM`: allows you to choose your scrap source.
  * `SCRAPER OPTIONS`: allows you to choose all the scraping options:
    * `SELECT IMAGE TYPE`: allows you to choose the visual to retrieve for the games.
    * `SELECT THUMBNAIL TYPE`: allows you to choose if you want to recover the images.
    * `SELECT VIDEO TYPE` : allows you to choose if you want to recover videos and what type of video (optimized / normalized videos will take less space).
    * `SELECT REGION PRIORITY`: allows you to choose the region to use from your favorite region or the detected region.
    * `SELECT FAVORITE REGION`: allows you to choose your favorite region.
    * `SELECT FAVORITE LANGUAGE`: allows you to choose your favorite language for the texts of the scraps among others.
    * `DOWNLOAD GAME MANUALS`: allows you to download the original manuals for each game.
    * `DOWNLOAD GAME MAPS`: allows you to download the original maps for each game.
    * `DOWNLOAD PAD-2-KEYBOARD CONFIGURATIONS`: allows you to download the settings from the joysticks to the keyboard, very useful when using old computers.
    * `USERNAME`: allows you to indicate your ScreenScraper login.
    * `PASSWORD`: allows you to enter your ScreenScraper password.
  * `GET GAME NAME FROM` : allows you to choose if you want to have the scrap from the name of the roms or isos.
  * `FILTER` : allows you to choose if you scrap only the missing media or all the games of a system.
  * `SYSTEMS` : allows you to choose which systems to scrap.
* Once you have made all your choices, click on the `SCRAP NOW` button to start scratching.

Depending on the options, the quantity of games, the number of threads associated with your ScreenScraper account, and the number of scraps allowed per day, scraping can take anywhere from a few minutes to a few hours.