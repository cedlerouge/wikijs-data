---
title: Calibration
description: How to do to play games in a more optimal way ?!
published: true
date: 2021-08-09T22:25:42.796Z
tags: lightgun, calibration, 7.2+
editor: markdown
dateCreated: 2021-08-05T18:30:05.957Z
---

## Introduction

Often, in some games, especially arcade, it will be necessary to configure the game before playing it. In the case of the arcade, you will just have to configure it once and it will remain in your NVRAM or other .CFG that you will find by system and/or emulator in your "saves" directory of your share.

![](/basic-usage/features/lightgun/calibration/calibration1.png)

But to configure it will sometimes be necessary to go in the configuration of the core or even the game.