---
title: With MAME game
description: 
published: true
date: 2021-08-09T22:26:43.338Z
tags: lightgun, mame, calibration, 7.2+
editor: markdown
dateCreated: 2021-08-05T18:33:14.365Z
---

## Example of calibration in MAME 2003 plus

1) Launch the game and activate this menu by pressing L3 on your controller.

![](/basic-usage/features/lightgun/calibration/mame1.png){.full-width}

2) Go down with your dpad or left stick in "Analog Controls" and validate with the `X` key _(SNES controller as reference of course_)

![](/basic-usage/features/lightgun/calibration/mame2.png){.full-width}

3) Choose and set "Lightgun X sensitivity" and "Lightgun Y sensitivity" to 50 or 100% (it can be 25% by default) with your dpad or left stick.

![](/basic-usage/features/lightgun/calibration/mame3.png){.full-width}

4) Exit the menus by pressing `X` _(SNES controller as reference)_ to return to the game. The parameters will be effective immediately, Enjoy !!! 

5) At the next launch, the configuration will be kept (keep your "saves" warm !!!)

>It is necessary to keep these files. So you will not have to redo the manipulation, it will be in your saves: 
>
>![](/basic-usage/features/lightgun/calibration/calibration1.png){.full-width}
{.is-warning}