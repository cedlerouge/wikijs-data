---
title: Lightgun
description: Lightgun for all!
published: true
date: 2021-08-09T17:34:04.212Z
tags: lightgun, 7.2+
editor: markdown
dateCreated: 2021-08-05T17:51:15.723Z
---

![](/basic-usage/features/lightgun/lightgun.png)

## What is the principle?

The principle is to automatically configure lightgun compatible games for most of the systems supported by Recalbox. For cost and availability reasons, we want to be able to play with a Wiimote (or two for 2 players games). We can also connect a Nunchunk to deport the primary and secondary fire according to our preference. This lightgun mode is automatically activated when you have a Mayflash dolphin bar in mode 2 and only for games supporting this mode.

Recalbox's lightgun feature allows you to play bazooka guns and other retro pointing devices again!

**Systems that already support this feature in Recalbox:**

* 3DO
* Atomiswave
* Dreamcast
* Fbneo
* Mame
* Master System
* Megadrive
* Mega-CD
* Naomi
* Naomi GD ROM System
* NES
* Playstation 1
* Saturn
* SNES

>The cores of the libretro emulator are supported at this time, no standalone emulator yet, this may come later.
{.is-info}

>WARNING: this will only work if the games are scrapped with well identifiable names like `duck hunt (world) v1.1` and not `dhnes` !!! It must be possible to find the term "duck hunt" in the name.
{.is-warning}

## What are the hardware requirements?

One to two Nintendo Wiimotes (or compatible clone)

![One to two Nintendo Wiimotes (or compatible clone)](/basic-usage/features/lightgun/wiimote.png)

One to two Mayflash dolphin bar

![One to two Mayflash dolphin bar](/basic-usage/features/lightgun/dolphinbar.png)

>**You will need a Mayflash dolphin bar per Wiimote in lightgun mode (Wiimote acts as an USB mouse)**
{.is-warning}

Optional: Nunchuck if we want to fire with "C" and "Z" buttons

![Optional: Nunchuck if we want to fire with "C" and "Z" buttons](/basic-usage/features/lightgun/nunchuck.png)

![](/basic-usage/features/lightgun/gun1.png)![](/basic-usage/features/lightgun/gun2.png)

Optional: we can also use specific hardware for a more pleasure gameplay

![Optional: we can also use specific hardware for a more pleasure gameplay](/basic-usage/features/lightgun/crossbow.png)

## How to install?

### 1. Connect the Mayflash dolphin bar(s) in usb on your Recalbox

![](/basic-usage/features/lightgun/usbports.png)

### 2. Put in mode 2 on the Mayflash dolphin bar(s)

Press the mode button until you have the blue led in front of the number 2

![Press the mode button until you have the blue led in front of the number 2](/basic-usage/features/lightgun/dolphinbarmode2.png)

### 3. Synchronize the Wiimote with the Mayflash dolphin bar

Press the sync button and a blue led blinks

![Press the sync button and a blue led blinks](/basic-usage/features/lightgun/dolphinbarsync.png)

Then you have to press the sync button of the Wiimote

![Then you have to press the sync button of the Wiimote](/basic-usage/features/lightgun/wiimotesync.png)

>If you want to play with 2 users, you have to link a Wiimote to each Mayflash dolphin bar in the same way as explained before.
{.is-info}

## How to activate it?

This lightgun mode is automatically activated when you have a Mayflash dolphin bar in mode 2 and only for games that support this mode.

>This 'Lightgun' mode can be activated/deactivated just by turning on/off the Mayflash dolphin bar but honestly, it is not necessary to change unless you want to alternate lightgun mode and controller mode for the same game.
{.is-info}

## And finally... how to play?

### Default Wiimote commands

Start: ![](/basic-usage/features/lightgun/button_plus.png) to start a game mode / pause in some games

Select: ![](/basic-usage/features/lightgun/button_minus.png) to select a game mode / put credits

Home: ![](/basic-usage/features/lightgun/button_home.png) to exit the game and return to the game selection menu

Main shot: ![](/basic-usage/features/lightgun/button_z.png) on the Nunchuk

Secondary shot: ![](/basic-usage/features/lightgun/button_c.png) on the Nunchuk 

### Notes on the Wiimote buttons

>* Other keys can be configured for some systems on the crosshair for example (even if we try to avoid these cases)
>* The ![](/basic-usage/features/lightgun/button_a.png) key for credits.
>* Also some games can be launched with ![](/basic-usage/features/lightgun/button_a.png) for example.
>* Some games will also require you to press 2 keys at the same time to enter a game or to exit/enter a menu.
>* The left directional keys ![](/basic-usage/features/lightgun/stick.png) will behave like the Wiimote's directional cross.
>* The power button ![](/basic-usage/features/lightgun/button_power.png) can be used to turn off the Wiimote with a long press.
>* The following buttons are not functional in lightgun mode: ![](/basic-usage/features/lightgun/button_1.png) and ![](/basic-usage/features/lightgun/button_2.png)
>* If the Nunchuck is connected, the ![](/basic-usage/features/lightgun/button_a.png) and ![](/basic-usage/features/lightgun/button_b.png) buttons are reversed on the Wiimote.
{.is-info}

## The Lightgun virtual system

You can activate a virtual Lightgun system to collect all these games.  
To activate it, [look here](./../../basic-usage/features/virtual-systems).

![ENJOY !!!](/basic-usage/features/lightgun/duckhunt.png)