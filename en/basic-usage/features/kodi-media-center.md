---
title: Kodi Media Center
description: 
published: true
date: 2021-08-08T19:07:35.136Z
tags: kodi, media, center
editor: markdown
dateCreated: 2021-08-03T22:55:03.355Z
---

![](/basic-usage/features/kodi/kodilogo.png)

Kodi is an integrated media space in Recalbox.

You can connect to a wide variety of streams through an Ethernet connection to enjoy movies, music, and more.

Live and recorded media can be accessed with a properly configured KODI installation. Likewise, you can simply connect to your own local media via USB or the network.

>Controllers are now supported in kodi, but if you prefer, you can use HDMI CEC or a remote smartphone app.
{.is-info}

## Base

### Startup and shutdown

You can launch **Kodi Media Center** either by pressing the `X` button (in EmulationStation), pressing `START` + selecting the first menu entry (KODI).

To exit KODI, navigate to the power icon at the top left of the KODI main menu and select either:

* "Power Off System"
* "Reboot".

Both items will take you back to the EmulationStation main menu; follow the normal commands in EmulationStation if you want to power off.

You cannot power off directly from KODI, you must return to EmulationStation if you want to power off.

### New KODI users:

At first launch, KODI has only a few options. You need an Ethernet/Wifi connection, unless you are using only local or USB-connected media.

The [KODI Wiki](http://kodi.wiki/view/Main_Page) contains more information on the specifics of setting up and using KODI. KODI is fairly intuitive.
It usually finds USB media automatically when you enter the Video or Music options; you should see your USB-connected media or a path to it.

In addition, the Video, Music and other tabs have "Addons" (similar to an application).  
There are several "Addons" that you can download directly from the main registry within KODI itself, and several other registries (collection of addons) that users have created, so you may have to dig a little deeper to add some awesome content.

* To get music or video addons, navigate to the "Addons" tabs under the "Music" or "Video" tab and click on them. or
* Select the main tab by clicking on it and you will see all locally connected media or devices with "Addons" already installed. Click on "Addons" and find the button that says "Get More". This will take you to a list of KODI approved "Addons". Just select the one you are interested in and click on "Install".

This will happen in the background and if you have a decent Ethernet connection, the download and installation will usually take less than a minute.

If you have network support, you need to configure these paths in the file system settings.  
Again, all of these are well documented on their own [Wiki](http://kodi.wiki/view/Main_Page) and there are many videos available on how to configure KODI.

Notes:

* There are some options in the [configuration](./../../basic-usage/getting-started/recalboxconf-file) if you want to boot directly to KODI every time rather than EmulationStation (default).
* Some people sell dongles or instructions for getting content on your KODI, but these are just collections or preassembled directories that are free if you find them yourself.
* Some addons may require a subscription, like Netflix or Amazon Video, others may just require you to create an account like Pandora.
* If your controllers go crazy when you enter KODI, they are not properly configured. Review the controller configuration; [Recalbox controllers](./../../basic-usage/first-use-and-configuration#configuration-of-a-controller) section
* The Raspberry Pi version of KODI does not have all the features and Add-Ons, but most of them are present.
* The first time and every time you start KODI, it performs updates or background checks. This may cause irregular or delayed use of some features. Usually, you just have to wait a while to clear them up. The more addons you have, the longer it may take and some may be set to not be automated.
* The current version of Recalbox from KODI is "Leia".
* Click here for [SMB v2+ on KODI](https://discourse.osmc.tv/t/kodi-and-smbv1/37441/5) support.

## Options

The [recalbox.conf](./../../basic-usage/getting-started/recalboxconf-file) file contains some options for Kodi. You can enable or disable Kodi, enable the X button shortcut or start Kodi automatically at startup. You can also do this from EmulationStation (`START` > `ADVANCED SETTINGS` > `KODI OPTIONS`).
 
>Controllers are supported in Kodi. 
>
>You can use your TV's **HDMI CEC** function to control Kodi from your remote control or **Yatse**, a Kodi remote control for smartphone.
{.is-info}

## Miscellaneous

### Power management with HDMI CEC

If you try to quit or shut down Kodi, your Raspberry will always send an HDMI shutdown command to your A/V receiver + TV. As a result, all your "devices" will be turned off.

To avoid this behavior, you should disable the "Shutdown device on exit" setting by going to `System` -> `Settings` -> `System` -> `Input devices` -> `Devices` -> `CEC adapter`.

The CEC adapter, in some cases, will allow you to control your KODI via your regular TV remote if they all support CEC. CEC sends remote commands via VIA HDMI cable to your IP. [Wiki Kodi CEC](http://kodi.wiki/view/CEC)

### Configuration of Yatse

Yatse and some other applications are remote controls for KODI. These apps often offer more control and more intuitive control of KODI. It's not required, but if you enjoy KODI, you'll probably enjoy the experience more with the app's remote.

You must first launch KODI on your Recalbox.

After installing the Yatse application, Yatse will search your network for available KODI installations.

![](/basic-usage/features/kodi/configure_yatse_for_kodi_recalbox_1-300px.png)

You can skip this step and go to manual configuration. Just set the [IP address](./../../tutorials/network/ip/discover-recalbox-ip) of the Recalbox on your network and the port to **8081**.

![](/basic-usage/features/kodi/configure_yatse_for_kodi_recalbox_2-300px.png)

Your Yatse controller is now connected:

![](/basic-usage/features/kodi/configure_yatse_for_kodi_recalbox_3-300px.png)

If you like Yatse, buy the **Unlocker** to get new features.

### Joystick configuration

The configuration between the joystick and the action in KODI can be changed in the file `/recalbox/share/system/configs/kodi/input.xml` using the actions and functions listed here: [http://kodi.wiki/view/Keymap#Commands](http://kodi.wiki/view/Keymap#Commands)

The buttons on your controller have this correspondence:

| Controllers buttons | Function |
| ---: | :--- |
| `A` | Select |
| `B` | Back |
| `X` | Stop |
| `Y` | Menu |
| `START` | Pause |
| `SELECT` | Contextual menu |
| `UP` | Up |
| `DOWN` | Down |
| `RIGHT` | Right |
| `LEFT` | Left |
| `LEFT STICK UP` | Volume up |
| `LEFT STICK DOWN` | Volume down |
| `LEFT STICK LEFT` | Volume down |
| `LEFT STICK RIGHT` | Volume up |
| `RIGHT STICK UP` | Up |
| `RIGHT STICK DOWN` | Down |
| `RIGHT STICK LEFT` | Left |
| `RIGHT STICK RIGHT` | Right |

### No sound on headphone output:

If you have configured Kodi to use headphone audio output but you are not getting any sound, try putting these configuration variables in the `/boot/recalbox-user-config.txt` file:

```ini
hdmi_ignore_edid_audio=1
hdmi_force_edid_audio=0
```

It will force the audio to go through the headphone output.