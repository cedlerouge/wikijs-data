---
title: Snap video
description: 
published: true
date: 2021-08-08T19:04:07.168Z
tags: videos, snap
editor: markdown
dateCreated: 2021-06-29T11:41:16.663Z
---

The "Snap Videos" are nothing more or less than the display of a small gameplay video instead of your usual "scrap" in your list of games.

You can see a video clip of it [here](https://www.youtube.com/watch?v=r5oabVrfQpk).

>For those who have never used this function, you should know that videos take up space very quickly, despite their excellent MP4 compression _(count between 1 and 6MB/piece depending on the system)_.
>If you want to take full advantage of this function, a USB3 hard disk of 1 or 2TB will be essential !
{.is-warning}

## Configuration

In order to enjoy these videos, you will have to (re)scrape your game lists, using Skraper or the new internal scraper of Recalbox.

### With the internal scraper

See [this page](./../../basic-usage/features/internal-scraper) of this documentation.

### Via the Skraper software

You can watch [this video](https://www.youtube.com/watch?v=G388Gc6kkRs) to learn how to use Skraper (French - enable sub-titles).