---
title: Virtual systems
description: 
published: true
date: 2021-08-08T19:05:11.803Z
tags: virtual, systems
editor: markdown
dateCreated: 2021-06-29T11:39:06.226Z
---

## Presentation

Also long awaited, virtual systems (like the Favorites system) among the classics:

* All games.
* All multiplayer games (2 players and more).
* All last played games, automatically sorted by date.

Not forgetting the **Arcade** system, which gathers all the MAME, FBN, NEO-GEO systems, ... which will now be activated/deactivated in the menus.  
But that's not all.

For those who will rescrape their systems with the new internal scraper (Skraper not being updated yet), _**new virtual systems per game genre can be activated**_.  
Yes, you read that right:

* By genre.
* By Shoot'em up games ?
* By platform games?
* By fighting games?
* By puzzle games?

Activate the virtual systems of the genres you are interested in, and they will all be gathered in the same list!

## Configuration via EmulationStation Menu

* EmulationStation Menu

![](/basic-usage/features/virtualsystem1.png){.full-width}

* Advanced settings

![](/basic-usage/features/virtualsystem2.png){.full-width}

* Virtual systems
  * Show all-games system (On/Off)
  * Show multiplayer system (On/Off)
  * Show last games system (On/Off)
  * Show Lightgun system (On/Off)
  * Virtual systems per genre

![](/basic-usage/features/virtualsystem3.png){.full-width}

![](/basic-usage/features/virtualsystem4.png){.full-width}

  * Arcade virtual system
    * Activate arcade virtual system (On/Off)
    * Include Neo-Geo (On/Off)
    * Hide original systems (On/Off)
    * Position
      * Select the desired location in the system list.

![](/basic-usage/features/virtualsystem5.png){.full-width}

![](/basic-usage/features/virtualsystem6.png){.full-width}

## Configuration via recalbox.conf (Optional)

```ini
## Arcade metasystem
## Activate the Arcade metasystem to group all games from piFBA, FBN (libretro), MAME and optionally Neogeo
## into a single "Arcade" system.
;global.arcade=1
## You may want to specify its position in the system list. (Default: 0)
## Negatives values may be used to tart from the end (-1 = last position)
;global.arcade.position=0
## Include NeoGeo or not (default: 1)
;global.arcade.includeneogeo=1
## Hide included system or leave them in the system list (default: 1)
;global.arcade.hideoriginals=1
```

### Option to enable the position in the system list. 

```ini
## You may want to specify its position in the system list. (Default: 0) ## Negative values may be used to start from the end (-1 = last position)
;global.arcade.position=0
```

### Hide subfolders in the main folder 

```ini
## Hide included system or leave them in the system list (default: 1)
global.arcade.hideoriginals=1
```