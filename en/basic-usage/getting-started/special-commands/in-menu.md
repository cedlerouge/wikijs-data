---
title: In the Menu
description: When you are int he menu, there is some special commands available.
published: true
date: 2021-09-04T17:07:32.046Z
tags: special commands, menu
editor: markdown
dateCreated: 2021-06-28T22:39:39.058Z
---

## Search function

From the systems menu activate the **search function**.

* `R1`

## Shutdown menu

From the systems menu, brings up the Recalbox shutdown menu.

* `Select`

## Start game clips

If you select to use game clips as screensaver, you can start them without to wait the defined time.

* `Y`