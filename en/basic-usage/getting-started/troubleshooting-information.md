---
title: Troubleshooting information
description: 
published: true
date: 2021-08-08T18:51:52.862Z
tags: troubleshoot, information
editor: markdown
dateCreated: 2021-06-28T23:12:57.497Z
---

## Controllers

### My controller works in Emulationstation but not when I run emulators

We add as many controller configurations as possible to EmulationStation so that you can navigate through them, but for many of them you need to reconfigure them to work in the emulators.

To do this, you need to open the EmulationStation menu with the `START` button and go to `CONTROLLER SETTINGS` > `CONFIGURE A CONTROLLER`.

### PS3 controller flashes but does not pair

* Connect the controller to your Recalbox and wait 10 seconds.
* You can now unplug the controller and press the `PS` button.

### The PS3 controller seems to be dead

* You need to reset the controller by pressing a small button behind the controller in a small hole, with a paper clip.

## Display

### Other black border, image too big

* Use your TV to find the picture menu and set the picture size to 1: 1 pixel or full.

If this does not work, try enabling overscan in the menu system settings selection box. You can find more information [in this tutorial](./../../tutorials/video/display-configuration/image-size-settings-overscan-tft).

### Black screen on PC monitor

If you have a black screen on the PC monitor (HDMI or DVI) edit the `recalbox-user-config.txt` file (update at startup) and put a `#` in front of the `hdmi_drive = 2` line (if there is not one already). You can find more information [in this tutorial](./../../tutorials/video/lcd/dvi-screen).

## System

### Root access

Use the root username and password `recalboxroot`.

* You can connect via [SSH](./../../tutorials/system/access/root-access-terminal-cli) to your Recalbox. You can access a terminal by exiting the emulators with `F4`, then press `ALT` + `F2`. You can get a lot more information from [this tutorial](./../../tutorials/system/access/root-access-terminal-cli).