---
title: Update
description: 
published: true
date: 2021-08-08T18:51:08.171Z
tags: update
editor: markdown
dateCreated: 2021-06-28T23:08:41.955Z
---

## Upgrade process

Here are the **upgrade options** for Recalbox.  We hope this summary will help you and give you the answers you are looking for.

## Depending on your situation...

### Is it possible to upgrade from Recalbox x.x to Recalbox 7?

_**No**, it is not possible to upgrade to the new version of Recalbox 7. A clean install is required. See below for instructions._

### What if I am using a theme other than the one provided by default?

* Make sure you are up to date with the systems, configuration files for your theme.
* Contact the author of your theme or the subject of your theme to check if a new updated version is available.
* Use a custom theme in 720p on Raspberry Pi. In 1080p, your Raspberry Pi will suffer.
* We do not provide support for unofficial Recalbox custom themes. The theme creators are the only ones to support their themes.
* Please use the default Recalbox theme to check your bugs before posting your problem on the forum.

### What if I use OVERLAYS?

If you have created your own overlays, RetroArch has added a new aspect ratio that shifts some overlays:

* Test 22 or 23 on this line in the overlays configurations:

```ini
aspect_ratio_index = 21
```

### Arcades

Recalbox has updated the cores of the arcade emulators : MAME & FinalBurn Neo and added other emulators.

By this fact, you have to adapt your romsets:

* The global dat files are now in the following directories:

  * /recalbox/share/bios/fba
  * /recalbox/share/bios/fbneo
  * /recalbox/share/bios/mame
  * /recalbox/share/bios/mame2000
  * /recalbox/share/bios/mame2003
  * /recalbox/share/bios/mame2003-plus
  * /recalbox/share/bios/mame2010
  * /recalbox/share/bios/mame2015

* To make a parent-only dat, neogeo parent only etc... you will just have to use this [datutil tutorial](./../../tutorials/utilities/dat-management/datutil) to create them.

Here are the versions used:

* advanceMame : romset mame 0.106 (for experienced users only).
* FBA: romset 0.2.97.44
* FBNeo: romset 1.0.0.01
* Mame 2000: romset mame 0.37b5
* Mame 2003: romset mame 0.78
* Mame 2003-plus: romset mame 0.78-0.188.
* Mame 2010: romset mame 0.139
* Mame 2015: romset mame 0.160
* Mame : romset mame 0.230
* See the arcade documentation.

### Your Raspberry Pi in a box

* Make sure you have a good ventilation.
* A good power cable.
* If you have any problems, take your Raspberry Pi out of your case and test Recalbox 7 as is.

### Our advice:

* Have a backup of your share on your PC or external hard drive.
* Use a removable media (USB key or external hard drive), you will quickly understand that a reinstallation is faster.
* Use an external scraper
* Always have 2 microsd cards, 8GB is enough for the system (one for the stable version, another for your tests, or beta tests) where the interest to have its roms on removable media.
* Install heat sinks or fans if necessary:
  * If you overlock your Raspberry Pi.
  * If your Recalbox is in a case.

## Launch the update

>The **Recalbox update** is accessible by pressing `START` > `UPDATES`.
{.is-info}

* Set up your wifi or connect a network cable to your Recalbox.
* Select `UPDATES`.
* Then `START UPDATE`. 

The system will automatically restart after the update is downloaded.