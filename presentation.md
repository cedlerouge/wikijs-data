---
title: 📋 PLUS D'INFOS
description: Comment contribuer, les licences, les liens utiles...
published: true
date: 2021-08-12T07:13:59.596Z
tags: plus d'infos
editor: markdown
dateCreated: 2021-05-21T07:48:58.198Z
---

Vous trouverez ici la présentation générale de Recalbox, qui sommes-nous, les licences d'utilisations ainsi que la possibilité de contribuer au projet de différentes manières !