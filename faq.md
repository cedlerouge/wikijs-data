---
title: 💡 FAQ
description: 
published: true
date: 2021-08-12T07:34:15.400Z
tags: faq
editor: markdown
dateCreated: 2021-05-21T07:48:39.876Z
---

## Général

### Sur quoi puis-je installer Recalbox ?

Vous pouvez installer Recalbox sur les appareils suivants :

- **Raspberry Pi 4**
- Raspberry Pi 400
- Raspberry Pi Zero
- Raspberry Pi 1 (B/B+)
- Raspberry Pi 2
- Raspberry Pi 3 (B/B+)
- Odroid XU4
- Odroid Go Advance (Console Portable)
- Odroid Go Super (Console Portable)
- PC x86 (32bits)
- PC x86_64 (64bits)

### Comment puis-je participer ?

- Vous pouvez faire un don sur notre Paypal
- Vous pouvez commandez votre matériels sur kubii.fr/221-recalbox
- Vous pouvez remonter des bugs ou des idées d'amélioration sur notre [Gitlab](https://gitlab.com/recalbox/recalbox/issues)
- Vous pouvez participer sur les [forums recalbox](http://forum.recalbox.com)
- Vous pouvez [contribuer à ce wiki](./contribute)

### Où puis-je voir l'avancée du développement de la Recalbox?

Sur le [gitlab de Recalbox](https://gitlab.com/recalbox). Vous pouvez accéder :  
- aux [issues](https://gitlab.com/recalbox/recalbox/issues) qui sont les tâches en cours.  
- aux [milestones](https://gitlab.com/recalbox/recalbox/-/milestones) sur lesquelles vous pourrez voir les tâches qui sont encore à développer, et celles qui sont terminées.

### Recalbox contient combien de jeux ?

Nous essayons d'ajouter un maximum de jeux libres dans la distribution, pour que vous puissiez profiter de l'expérience Recalbox dès le premier lancement !
Rendez-vous sur [cette page](https://forum.recalbox.com/topic/52/homebrews-sur-la-recalbox) pour proposer vos homebrews !

### Puis-je utiliser Recalbox dans une borne / bartop ?

Oui. Le système Recalbox a été aussi pensé pour l'intégration dans les bornes et bartops.
- Vous pouvez brancher les boutons et joysticks directement sur les GPIO du Raspberry.
- Vous pouvez désactiver les menus pour éviter toute mauvaise manipulation.
- Il est aussi possible d'utiliser les sorties HDMI / DVI /VGA (avec adaptateur) ou RCA.
Vous trouverez plus d'informations sur [la page dédiée](./tutorials/system/installation/bartop-arcade-configuration).

### La Recalbox supporte beaucoup de systèmes mais je n'en vois que quelques uns dans l'interface, que faire?

Seuls les systèmes ayant des jeux disponibles s'affichent. Essayez d'ajouter vos roms via le réseau et redémarrez la Recalbox pour voir apparaître les nouveaux systèmes. De plus certains matériels ne supportent pas toutes les consoles.

### Dois-je overclocker mon Raspberry Pi ?

Sur Raspberry Pi 1 (y compris zero), il est vivement conseillé d'overclocker le Raspberry en mode EXTREM.

## EmulationStation

### Comment changer l'apparence du thème d'EmulationStation ?

Appuyez sur `Start` sur la manette → `PARAMÈTRES D'INTERFACE` → `THEME`

### Comment couper la musique de fond ?

Appuez sur `Start` sur la manette → `OPTIONS DU SON` → Mettre sur Off

### Où se situent les fichiers de configuration d'EmulationStation dans le système de fichiers ?

```
/recalbox/share/system/recalbox.conf
```

### Où sont situés les thèmes d'EmulationStation ?

```
/recalbox/share/themes
```

## Émulation

### Pourquoi y a-t-il plusieurs émulateurs pour chaque système ?

L'émulation n'est pas une opération sans faille, il y a un équilibre entre performances, fonctionnalités et fidélité.
Étant donné que l'émulation n'est pas parfaite, il est préférable d'avoir plusieurs options pour la gérer, parfois un émulateur plus rapide sera meilleur mais peut avoir des problèmes inattendus, tandis qu'un plus lent peut avoir une précision plus élevée mais ne prend pas en charge certaines fonctionnalités spécifiques.

### Comment changer l'émulateur par défaut pour un système donné ?

En appuyant sur `START` sur la manette de jeu, puis en accédant aux paramètres avancés, et enfin dans la configuration avancée des émulateurs. Vous pouvez sélectionner le système souhaité, vous pouvez changer les paramètres de l'émulateur et du noyau (notez que le paramètre de base ne changera pas si le paramètre de l'émulateur indique Défaut).

## Système

### Recalbox supporte une centaine de consoles mais je n'en vois que quelques-unes, que ce soit dans l'interface EmulationStation ou dans le dossier share ?

Selon la carte que vous utiliser vous aurais les émulateurs compatible avec celle-ci.  
Veuillez regarder la liste des compatibilités selon les cartes [ici](./hardware-compatibility/).

### Pourquoi sur certains systèmes comme la GBA, mes jeux affichent un écran noir puis un retour sous EmulationStation ?

> Certains systèmes nécessitent des bios pour fonctionner.

## Dépannage général

### Ma Recalbox vient de planter, dois-je la débrancher et la rebrancher ?

Non, dans la plupart des cas, le système n'a pas complètement planté, seule EmulationStation l'a fait.

Il existe plusieurs façons de gérer cette situation, la plus simple étant l'utilisation des fonctionnalités de débogage du Webmanager pour redémarrer EmulationStation ou pour arrêter correctement Recalbox.

Ne pas le faire peut corrompre vos données, surtout si vous utilisez un Raspberry Pi avec une carte SD.