---
title: Contrôle du ventilateur
description: gestion des ventilateurs sur les RPIs
published: true
date: 2021-08-11T20:34:04.783Z
tags: 7.3+, rpi, fan, ventilateur
editor: markdown
dateCreated: 2021-08-11T19:18:19.827Z
---

# Description

L'utilitaire **recalbox-wpaf** permet de contrôler certaines cartes (*hat*) disposant d'un ventilateur.  
La liste des cartes supportées par recalbox-wpaf est disponible [ici](./../hardware-compatibility/compatible-devices/rpi-hats).

NB: cet utilitaire est encore en bêta et sera disponible avec Recalbox 7.3.

## Configuration

La configuration est faite dans le fichier `recalbox.conf` sous les clés `hat.wpaf.enabled` et `hat.wpaf.board`.

### `hat.wpaf.enabled`

Mettez cette clée à `1` pour activer l'utilitaire recalbox-wpaf.

### `hat.wpaf.board`

Cette clé permet de sélectionner la carte qui équipe votre Raspberry PI. Les valeurs possibles sont:

* `wspoehatb` pour la carte Waveshare PoE hat (b),
* `argonforty` pour la carte qui équipe le boîtier Argon One,
* `piboy` pour le boîtier d'Experimental PI,
* `rpipoeplus` pour la carte Raspberry PI PoE+,
* `fanshim` pour la carte Pimoroni fan SHIM.

Exemple:

```ini
hat.wpaf.enabled=1
hat.wpaf.board=rpipoeplus
```

Redémarrez pour prendre en compte les paramètres.