---
title: Pad To Keyboard
description: Redonnez vie à vos jeux d'Ordinosaures en y jouant avec vos manettes, même quand ils ne sont prévus que pour le clavier !
published: true
date: 2021-08-22T17:00:25.032Z
tags: p2k, manette, clavier
editor: markdown
dateCreated: 2021-05-21T07:49:23.721Z
---

Sous ce nom barbare se cache un module qui tient particulièrement à cœur à l'un de nos développeurs, puisqu'il va permettre de **redonner vie à tous ces jeux d'ordinateurs**, oubliés de tous les Recalboxers qui n'ont pas de clavier branché, c'est a dire, presque tout le monde...

## De la Manette au Clavier...

Durant la glorieuse décennie des 80', nombre de micro-ordinateurs ont vu le jour... Mais à cette époque, la manette n'existe pas encore, et le Joystick n'est pleinement exploité que sur console.  
Les jeux sur micro-ordinateur ont une ergonomie orientée clavier, et pour les plus chanceux, clavier ou joystick. Souvent, le clavier est nécessaire pour passer l'intro ou pour démarrer le jeu.  
Il était donc impossible de profiter de ces jeux sans avoir un clavier connecté.

Du moins jusqu'à Recalbox 7.0, qui inclue un module Pad-To-Keyboard qui est chargé de mapper des actions pads vers des appuis/relâchements de touches clavier !  
Ce module permet à l'aide de petits fichiers de configuration très simples. J'en vois certains au fond qui me regardent avec des yeux dubitatifs...

C'est très simple :

* Vous avez un jeu génial sur Commodore 64, mais vous ne pouvez pas y jouer parce qu'il demande d'appuyer sur une touche pour lancer une partie ? Aucun soucis :
  * Mappez le bouton `START` de votre pad sur la touche `ESPACE`, et le problème est réglé.
  * Lancez le jeu, appuyez sur `START` et le jeu verra que vous avez appuyé sur la touche espace d"un clavier. 
* Vous avez un jeu sur Amstrad qui se joue avec les 4 touches fléchées et la barre espace? Pas de problème :
  * Mappez la croix directionnelle de votre pad sur les 4 flèches, le bouton `A` sur `ESPACE`.
  * Et jouez à votre jeux comme s'il avait toujours été fait pour un joystick!

Et nul doute que de talentueux développeurs externes pourront même nous faire une petite interface pour créer ces configurations sans effort.

### Comment ça marche ?

Lorsqu'il détecte une configuration _**P2K**_ (Pad to Keyboard) pour un jeu, EmulationStation va créer un clavier virtuel qui sera vu par le système comme un vrai clavier physique. Tout au long de la session de jeu, EmulationStation va intercepter les événements des pads, et en fonction de la configuration, il va générer des appuis et des relâchements de touches.

Les émulateurs qui sont à l'écoute du clavier vont alors utiliser ces événements clavier dans la machine qu'ils émulent, comme s'ils venaient d'un clavier physique.

### Et l'input-lag ?

_**Garantie 100% sans input-lag, et ça fonctionne sur tous les ordinateurs !**_

Le module P2K est en priorité haute, ce qui signifie qu'il prends la main dés qu'un événement pad survient, pour le traduire et le renvoyer vers le clavier virtuel instantanément.

Sur Raspberry Pi 3, les mesures effectuées entre la réception de l'action Pad et la réception de l'appui clavier donnent des résultats très inférieurs à ce qui pourrait générer le moindre input lag.

### Ce qu'on ne peut pas encore faire

Pour l'instant, le mapping est un mapping instantané 1 pour 1. C'est à dire que les actions du pad sont traduites instantanément en appui/relâchement d'une seule touche clavier.

On ne peut donc pas:

* Générer un appui multi-touche : SHIFT + une autre touche pas exemple.
* Générer des appuis/relâchements consécutifs pour taper plusieurs caractères : on ne peut pas simuler la frappe d'un mot par exemple.

Ça viendra peut-être dans les prochaines versions 😉 

## Configuration

### Fichier de configuration

Les fichiers de configuration d'un mapping pad-to-keyboard peuvent être définis pour un répertoire complet (et ses sous-répertoires) ou pour un jeu en particulier.

Pour un répertoire, il devra se situer à l’intérieur de celui-ci et s’appellera `.p2k.cfg`.
Ce qui donnera un chemin final du type `/chemin/vers/.p2k.cfg`.

Pour une rom (ou un jeu) donné, il devra porter le même nom que la rom, avec l'extension `.p2k.cfg` rajoutée à l'extension existante.
Ce qui, pour `/chemin/vers/monjeu.ext`, donnera au final `/chemin/vers/monjeu.ext.p2k.cfg`.

Les configurations de répertoires sont actives pour tous les jeux du répertoire et des sous-répertoires.

Si plusieurs fichiers sont présents, ils se surchargent dans l'ordre descendant des répertoires, jusqu'à la configuration propre aux jeux, si elle existe. Ce sera vu en détail un peu plus loin.

### Configuration du mapping

Partons d'un exemple utilisé lors des tests :

```ini
# dpad to arrow keys
0:up = up
0:down = down
0:right = right
0:left = left

# button A to SPACE
0:a = space
# B to Y key (for Y/N prompts)
0:b = y
# And finally, map START button to ENTER 
0:start = enter
```

Comme on peut le voir, la structure est très simple.

On peut laisser des lignes vides pour séparer les blocs, et utiliser # en début de ligne pour les commentaires/descriptifs/mémos.

Les lignes de configuration de mapping sont de la forme `PAD:ACTION = KEY` , avec :

* `PAD` : N° de manette tel que défini dans EmulationStation, ou l'ordre naturel des pads s'il n'y a pas d'affectation forcée. Rares sont les cas ou plus d'une manette sera mappé, donc le n° **0** sera utilisé dans la grande majorité des cas.
* `ACTION` : Action de la manette à mapper. On peut mapper tous les boutons, les flèches directionnelles (D-Pad), et les 4 directions primaires des joysticks analogiques, ce qui représente jusqu'à 25 actions qu'on peut affecter à des touches clavier, sur un pad standard complet.
* `KEY` : La touche du clavier qu'on associe à l'action de la manette. Lorsque l'action de la manette est enclenchée, la touche correspondante est appuyée. Lorsque l'action de la manette est désenclenchée, la touche correspondante est relâchée. Comme vu plus haut, le mapping est du 1 pour 1. Si vous voulez simuler par exemple _**LEFT-SHIFT + A**_, il faudra affecter _**LEFT-SHIFT**_ a un bouton de la manette (par exemple _**L1**_), et la touche _**A**_ à un autre bouton (disons le bouton_**Y**_). L'appui simultané de _**L1+Y**_ donnera un _**LEFT-SHIFT + A**_. Une exception existe pour les joysticks analogiques. Étant donné qu'on peut les placer en diagonale, cette particularité peut être utilisée pour appuyer sur 2 touches en même temps. 

La casse des `ACTION` et `KEY` n'a pas d'importance, pas plus que les espaces en début de ligne, en fin de ligne, ou autour du `=`.

#### Actions joysticks

Voici la liste complète des actions pad utilisables :

| Action | Description | Action | Description |
| :--- | :--- | :--- | :--- |
| `up` | Digital PAD, direction haute | `j1up` | Joystick #1 (gauche), direction haute |
| `down` | Digital PAD, direction basse | `j1down` | Joystick #1 (gauche), direction basse |
| `left` | Digital PAD, direction gauche | `j1left` | Joystick #1 (gauche), direction gauche |
| `right` | Digital PAD, direction droite | `j1right` | Joystick #1 (gauche), direction droite |
| `a` | Bouton A | `j2up` | Joystick #2 (droite), direction haute |
| `b` | Bouton B | `j2down` | Joystick #2 (droite), direction basse |
| `x` | Bouton X | `j2left` | Joystick #2 (droite), direction gauche |
| `y` | Bouton Y | `j2right` | Joystick #2 (droite), direction droite |
| `l1` | Bouton L1 | `l2` | Bouton L2 |
| `r1` | Bouton R1 | `r2` | Bouton R2 |
| `start` | Bouton START | `l3` | Bouton L3 |
| `select` | Bouton SELECT | `r3` | Bouton R3 |

#### Touches claviers utilisables

Le clavier virtuel créé par EmulationStation est un clavier agnostique. Il ne connait ni langue, ni disposition particulière. C'est donc un clavier 105 touches, QWERTY des plus basiques.

Pour ne pas perdre les utilisateurs, seules certaines touches communes à tous les claviers sont configurables. Et ce, pour deux bonnes raisons :

* Si je vous dis que sur mon clavier portugais, j'ai mappé le bouton Y sur la touche %, il y a toutes les chances qu'elle ne corresponde pas à la touche % sur votre clavier.
* L’émulateur va lui aussi devoir mapper un clavier 105 touche vers le clavier de la machine émulée. C'est parfois tellement complexe et peu intuitif que certains émulateurs proposent en plus un clavier virtuel à l'écran.

Cependant, pas d'affolement, dans la grande majorité des cas, vous allez utiliser des mapping de touches simples. Il faut juste garder en tête que la disposition des lettres est en QWERTY.

Une image vaut mieux qu'un long discours :

![Noms des touches clavier](/advanced-usage/p2k/p2k-layout-v2.png)

#### Surcharge des configurations (oui, encore !)

Comme évoqué plus haut, les fichiers de configuration, lorsqu'il y en a plusieurs à différents étages de répertoires et/ou pour le jeu lui même, se surchargent les uns les autres.

Le module P2K va commencer par lire celui le plus haut, puis va descendre jusque dans le répertoire de la rom en lisant tous les fichiers de configuration qu'il trouvera, pour finir avec celui de la rom, s'il existe.

Ce qui signifie, que les configurations peuvent s'ajouter, ou se modifier !

**Exemple 1: Les configuration s'additionnent**

**/chemin/vers/.p2k.cfg :**

```ini
# Map START to ENTER
0:start = enter
```

**/chemin/vers/monjeu.ext.p2k.cfg :**

```ini
# Map SELECT to SPACE
0:select = space
```

Donnera l’équivalent de:

```ini
# Map START to ENTER
0:start = enter
# Map SELECT to SPACE
0:select = space
```

**Exemple 2: Les configurations se modifient**

**/chemin/vers/.p2k.cfg**

```ini
# Map START to ENTER
0:start = enter
# Map SELECT to SPACE
0:select = space
```

**/chemin/vers/monjeu.ext.p2k.cfg :**

```ini
# Map SELECT to F1
0:select = f1
```

Donnera l’équivalent de :

```ini
# Map START to ENTER
0:start = enter
# Map SELECT to F1
0:select = f1
```

**Exemple 3: Les configurations se modifient en supprimant des mappings**

**/chemin/vers/.p2k.cfg**

```ini
# Map START to ENTER
0:start = enter
# Map SELECT to SPACE
0:select = space
```

**/chemin/vers/monjeu.ext.p2k.cfg :**

```ini
# Remove SELECT mapping!
0:select =
```

Donnera l’équivalent de :

```ini
# Map START to ENTER
0:start = enter
```

### Ajout d'une brève description

![](/advanced-usage/p2k/configp2k.png)

Pour ajouter une description comme ci-dessus :

* Utilisez deux points virgules pour que ça apparaisse.

```ini
# Recalbox's Pad-to-Keyboard configuration
#Arkanoid Revenge of Doh (1987)(Sharp - SPS)
#SHARP X6800

#left move key 4
0:left = kp4 ;; Move left
#right move key 6
0:right = kp6 ;; Move right
#shoot key space
0:a = space ;; Fire!
#hispeed move key shift
0:b = leftshift ;; Speedup!
```

## Extension du P2K v2 (A partir de Recalbox 7.3)

A partir de Recalbox 7.3, le P2K étends ses fonctionnalités pour supporter :

* L'émulation souris 3 boutons
* Les combinaisons de touches
* Les sequences de touches

### Emulation Souris 3 boutons

Vous allez désormais pouvoir émuler le déplacement de la souris et les appuis relâchement de ses 3 boutons directement depuis votre pad.
Pour une émulation précise, un pad avec un joystick analogique est conseillé, mais l'émulation est parfaitement possible avec des boutons ou un d-pad.

#### Configuration complète

Aux `KEY` touches du clavier viennent s'ajouter 7 valeurs, correspondantes aux 4 directions de déplacement plus les 3 boutons souris.

| Element souris | Description |
| :--- | :--- |
| `mouse.button.left` | Bouton gauche de la souris |
| `mouse.button.middle` | Bouton central de la souris ou clic molette |
| `mouse.button.right` | Bouton droit de la souris |
| `mouse.move.left` | Déplacement de la souris vers a gauche |
| `mouse.move.up` | Déplacement de la souris vers le haut |
| `mouse.move.right` | Déplacement de la souris vers la droite |
| `mouse.move.down` | Déplacement souris vers le bas |

Ces valeurs peuvent être affectée à n'importe quelle `ACTION`.
Admettons que nous ayons un simple pad SNES avec un DPAD et 4 boutons, et que nous souhaitons utiliser la souris sur un jeu DOS uniquement jouable à la souris, voilà à quoi ressemblera un fichier p2k:

```ini
# Recalbox's Pad-to-Keyboard configuration
# DOS GAME

# Mouse moves
0:up = mouse.move.up
0:right = mouse.move.right
0:down = mouse.move.down
0:left = mouse.move.left

# Mouse buttons
0:a = mouse.button.left
0:b = mouse.button.right
```

Avec une telle configuration, le déplacements du curseur accélère sur le départ avant de se stabiliser à une certaine vitesse.

Vous pouvez également utiliser un joystick analogique, par exemple le Joystick droit d'une manette XBox :

```ini
# Recalbox's Pad-to-Keyboard configuration
# DOS GAME

# Mouse moves
0:j2up = mouse.move.up  ;; Mouvement souris
0:j2right = mouse.move.right ;; Mouvement souris
0:j2down = mouse.move.down ;; Mouvement souris
0:j2left = mouse.move.left ;; Mouvement souris

# Mouse buttons
0:a = mouse.button.left ;; Bouton gauche
0:b = mouse.button.right ;; Bouton droit
```

Dans ce cas, c'est la poussée d'un joystick analogique qui régulera la vitesse de déplacement du curseur.

#### Configuration simplifiée

Si vous utilisez les joysticks gauche ou droit pour émuler le déplacement de la souris, vous pouvez utiliser la configuration simplifiée `0:j1 = mouse.moves` ou `0:j2 = mouse.moves` (*attention au 's' !*).
Si on reprends la configuration précédente, elle peut se simplifier comme suit :

```ini
# Recalbox's Pad-to-Keyboard configuration
# DOS GAME

# Mouse moves
0:j2 = mouse.moves ;; Mouvement souris

# Mouse buttons
0:a = mouse.button.left ;; Bouton gauche
0:b = mouse.button.right ;; Bouton droit
```

Pour les jeux qui ont uniquement besoin d'une souris, et ils sont nombreux, vous pouvez mapper la souris sur plusieurs éléments du pad, comme ceci:

```ini
# Recalbox's Pad-to-Keyboard configuration
# DOS GAME

# Mouse moves
0:up = mouse.move.up
0:right = mouse.move.right
0:down = mouse.move.down
0:left = mouse.move.left
0:j1 = mouse.moves ;; Mouvement souris
0:j2 = mouse.moves ;; Mouvement souris

# Mouse buttons
0:a = mouse.button.left ;; Bouton gauche
0:b = mouse.button.right ;; Bouton droit
```

Avec ce fichier p2k, les deux joysticks et le dpad sont utilisables pour piloter finement le curseur de la souris, en fonction des préférences de chacun et du niveau de précision requis.

### Configuration étendue du Clavier

Avec le P2K v2, les possibilités d'émulation du clavier s'étendent grandement, pour supporter les combinaisons de touches, et les séquences de touches (comme quand on tape au clavier).
Dans les 2 cas, le système supporte jusqu'à 32 touches. Donc soit 32 touches appuyées *presque* simultanément, ou une séquence de 32 touches consécutives.

#### Combinaisons de touches

Pour déclarer une combinaison de touches, c'est très simple, il suffit de séparer les touches par des espaces (et rien d'autre que des espaces !), comme ceci :

```ini
# Assign CTRL+C to R1
0:r1 = leftctrl c ;; Break the programm!
```

Notez qu'à l'appui de R1, les touches sont appuyées dans l'ordre exact de la déclaration. Puis lorsque R1 est relâché, les touches sont relâchées dans l'ordre inverse.

Dans le cas où l'émulateur nécessiterait un délai entre les touches, ou dans le cas ou vous auriez besoin d'un délai explicite pour une quelconque raison, vous pouvez précéder les touches par `+xxx` ou xxx est un délai en millisecondes.

Exemple :

```ini
# Assign CTRL+C to R1
0:r1 = +100 leftctrl c ;; Break the programm!
```

Dans ce cas : lorsqu'on appui sur R1, le P2K appuis sur la touche CONTROL GAUCHE, attends 100ms, puis appuis sur la touche C. La dernière touche, n'introduit pas de délai. Puis lorsque R1 est relâché, les touches sont relâchées dans l'ordre inverse : C est relâché, on attend 100ms, puis on relâche CONTROL GAUCHE

Le `+` indique explicitement que c'est une combinaison de touches. Il peut être utilisé seul, sans le délai, mais ne servira à rien dans ce cas, car en l'absence d'un indicateur explicite, plusieurs touches déclarées formeront une combinaison.

#### Séquences de touches

Pour une sequence de touche, nous devons utiliser un indicateur explicite `/`, comme ceci :

```ini
# Assign the sequence FGHJ to R1
0:r1 = / f g h j ;; Type FGHJ!
```

Ici, lorsque R1 va être appuyé, le système va générer la séquence suivante : appui F, relâchement F, appui G, relâchement G, appui H, relâchement H, appuis J, relâchement J. Sans délai entre les appuis et relâchements, et dans l'ordre strict de déclaration.

Notez qu'au relâchement de R1, il ne se passe rien. La séquence est envoyée uniquement sur un appui.

Encore une fois, certains émulateurs/jeux pourraient avoir des soucis avec des séquences de touches sans délais. Nous pouvons donc déclarer un délai de 25ms entre chaque appui/relâchement, comme ceci :

```ini
# Assign the sequence FGHJ to R1
0:r1 = /25 f g h j ;; Type FGHJ!
```

Dans ce cas, la séquence sera la suivante : appui F, attente de 25ms, relâchement F, attente de 25ms, appui G, attente de 25ms, relâchement G, attente de 25ms, appui H, attente de 25ms, relâchement H, attente de 25ms, appui J, attente de 25ms, relâchement J.
Pas de délai après le dernier relâchement.

### Qu'est-ce qui n'est toujours pas supporté ?

Une seule chose : les sequences de touches avec des combinaisons au milieu. C'est soit l'un, soit l'autre.
Il vous sera par exemple impossible d'écrire un mot qui nécessite des caractères accessibles avec SHIFT sur la machine émulée.

Peut-être dans une version 3... :)

## Section spéciale pour partager vos configurations

Ça y est, vous avez configuré vos jeux favoris pour qu'ils soient totalement jouables à la manette ?  
Alors ne laissez pas les autres refaire le même travail encore et encore : partagez vos configurations !  
Nous avons ouvert une discussion spéciale [**ici sur le forum de Recalbox**](https://forum.recalbox.com/topic/21287/partager-vos-fichiers-pad2keyboard-p2k-cfg?_=1600282719468), afin que chacun partage ses configurations.

Cependant, attention : essayez de rester logique, et d'avoir un mapping qui soit au maximum intuitif :

* Un jeu doit se lancer avec START ou le bouton A.
* Si un jeu nécessite une action clavier pour configurer le joystick (souvent le cas sur ZX Spectrum par exemple) restez en clavier et faite un mapping complet de toutes les touches.

Bons jeux !