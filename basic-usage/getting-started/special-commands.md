---
title: Commandes spéciales
description: 
published: true
date: 2021-09-04T17:04:50.901Z
tags: commandes spéciales
editor: markdown
dateCreated: 2021-05-21T07:54:46.078Z
---

Pour vous faciliter vos tâches sur votre Recalbox, nous vous avons intégrer plusieurs commandes spéciales.

* [Dans le Menu](in-menu) 
* [Pendant le jeu](in-game)

Vous avez même la possibilité d'avoir une version au format [PDF](pdf-memo) pour vous servir de mémo de partout !