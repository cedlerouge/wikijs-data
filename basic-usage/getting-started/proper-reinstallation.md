---
title: Réinstallation propre
description: 
published: true
date: 2021-09-04T17:04:25.609Z
tags: réinstallation propre
editor: markdown
dateCreated: 2021-05-21T07:54:33.364Z
---

## Que dois je faire avant une réinstallation propre du système ?

### 1. Je ne dispose pas de support amovible, uniquement la microsd

* Toujours avoir une copie de ses roms, sauvegardes, bios sur son PC ou sur un disque de sauvegarde.
* Faire une sauvegarde du dossier Recalbox contenant les roms, bios, saves sur votre PC.
* Télécharger et réinstaller la [dernière version de Recalbox](https://download.recalbox.com/) sur votre microsd.

#### Les bios

* Vérifier la signature de vos bios dans EmulationStation.
* Recalbox ajoute de nouveaux émulateurs qui ont parfois besoin de bios.

### 2. Je dispose d'un support amovible (clé usb/disque dur externe usb)

* Le format exFAT est conseillé pour les disques durs de grosses capacités.
* Brancher le disque dur externe ou la clé USB sur votre PC, puis renommer le dossier `recalbox` en `recalbox.old`.
* Télécharger et installer [la dernière version de Recalbox](https://download.recalbox.com/) sur votre microsd.
* Brancher votre disque dur externe ou clé usb sur votre Raspberry Pi / Odroid.
* Choisir votre support amovible pour votre support.
* L'arborescence du dossier Recalbox sera de nouveau créé sur votre périphérique externe.
* Éteindre votre recalbox.
* Brancher votre disque dur externe ou clé usb sur votre PC.
* Déplacer UNIQUEMENT LES ROMS, BIOS ET SAVE.
* **NE PAS déplacer les gamelist/scrap, custom theme et OVERLAYS** (responsable de nombreux bugs).
* Tester votre Recalbox avec les paramètres par défaut.
* Si tout va bien, vous pouvez commencer à personnaliser vos réglages, progressivement.

## Réinstallation propre du système

Suivez le guide [Préparation et Installation](./../preparation-and-installation)