---
title: Dépannage
description: 
published: true
date: 2021-09-04T17:02:15.788Z
tags: dépannage
editor: markdown
dateCreated: 2021-05-21T07:54:51.495Z
---

## Contrôleurs

### Ma manette fonctionne dans Emulationstation mais pas lorsque je lance les émulateurs

Nous ajoutons un maximum de configuration de manettes dans EmulationStation afin de vous permettre d'y naviguer, mais pour beaucoup d'entre elles, vous devez les reconfigurer pour qu'elles fonctionnent dans les émulateurs.

Pour cela, vous devez ouvrir le menu d'EmulationStation avec le bouton `START` et aller dans `RÉGLAGES MANETTES` > `CONFIGURER UNE MANETTE`.

### La manette PS3 clignote mais ne s'associe pas

* Branchez une manette sur votre Recalbox et attendez 10 secondes.
* Vous pouvez maintenant débrancher la manette et appuyer sur le bouton `PS`.

### La manette PS3 semble morte

* Vous devez réinitialiser la manette par un petit bouton derrière la manette dans un petit trou, avec un trombone.

## Affichage

### Autre bordure noire, image trop grande

* Utilisez votre téléviseur pour trouver le menu d'image et réglez la taille de l'image sur 1: 1 pixel ou plein.

Si cela ne fonctionne pas, essayez d'activer le surbalayage dans la boîte de sélection des paramètres du système de menus. Vous trouverez plus d'informations [dans ce tutoriel](./../../tutorials/video/display-configuration/image-size-settings-overscan-tft).

### Écran noir sur moniteur PC

Si vous avez un écran noir sur le moniteur PC (HDMI ou DVI) modifiez le fichier `recalbox-user-config.txt` (mise à jour au démarrage) et mettez un `#` devant la ligne `hdmi_drive = 2` (si il n'y en a pas déjà un). Vous trouverez plus d'informations [dans ce tutoriel](./../../tutorials/video/lcd/dvi-screen).

## Système

### Accès root

Utilisez la racine du nom d'utilisateur et le mot de passe `recalboxroot`.

* Vous pouvez vous connecter via [SSH](./../../tutorials/system/access/root-access-terminal-cli) à votre Recalbox. Vous pouvez accéder à un terminal en quittant les émulateurs avec `F4`, puis appuyez sur `ALT` + `F2`. Vous aurez beaeucoup plus d'informations en consultant [ce tutoriel](./../../tutorials/system/access/root-access-terminal-cli).