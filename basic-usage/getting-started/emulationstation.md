---
title: EmulationStation
description: Recalbox's Frontend
published: true
date: 2021-09-04T17:02:59.398Z
tags: frontend, emulationstation
editor: markdown
dateCreated: 2021-05-21T07:54:26.787Z
---

## Présentation

Lorsque vous allumez votre Recalbox, vous accédez à l'interface du logiciel appelé **EmulationStation** (ES).

Celle-ci vous permet de lancer vos jeux, de régler certaines options ou de mettre à jour votre Recalbox.

### Liste des systèmes

Le premier écran est celui de la liste des systèmes. Il liste les consoles et systèmes disponibles.

![](/basic-usage/getting-started/es1.png){.full-width}

Lorsque vous sélectionnez un système, l'écran change et vous accédez à votre liste de jeux disponibles.

### Liste des jeux

Une liste des jeux est disponible pour le système sélectionné à l'écran précédent.

Une fois le jeu lancé, reportez-vous à la section [Pendant le jeu](./../../basic-usage/getting-started/special-commands/in-game) pour connaître les options disponibles.

## Options dans EmulationStation

En appuyant sur le bouton `START`, vous serez en mesure de modifier certains réglages de votre Recalbox.

![](/basic-usage/getting-started/es2.png){.full-width}

### Kodi Media Center

* Permet de démarrer **Kodi Media Center** (anciennement connu sous le nom de `XBMC`).

Vous pouvez démarrer Kodi depuis l'écran d'accueil en pressant le bouton `X` de votre manette.

Pour quitter Kodi, sélectionnez `QUIT` dans le programme, et vous serez de retour dans EmulationStation.

Les manettes sont supportées dans Kodi. Mais si vous préférez, vous pouvez également utiliser le HDMI-CEC (permet d'utiliser la télécommande de votre TV pour naviguer dans Kodi) ou une application smartphone de contrôle à distance.

Pour plus d'informations sur Kodi, vous pouvez consulter ses tutoriels :

[Kodi Media Center](./../features/kodi-media-center)
[Utiliser une télécommande infrarouge](./../features/kodi-media-center/remote-control-usage)

### Réglages système

Vous accéderez aux informations de votre Recalbox.

* **Version de Recalbox** : la version de Recalbox actuellement installée. 
* **Espace disque** : espace de stockage utilisé/disponible.  
* **Média de stockage** : liste les médias de stockage disponible :
  * Partition share interne (1er disque dur utilisé par le système pour le premier boot).
  * N'importe quel stockage externe (tous les disques durs et autre clés USB détectés).
* **Langue** : la sélection de la langue des menus. 
* **Clavier** : le type de clavier que vous voulez utiliser.

### Mise à jour

Ce menu propose d'activer ou non les mises à jour et de choisir si on veut installer uniquement une version stable ou aussi les versions bêta de Recalbox.

* **Vérifier les mises à jour** : permet de de,ander à vérifier si une mise à jor existe.
* **Mise à jour disponible** : permet de vous indiquer si une mise à jour est disponible.
* **Nouveautés de la mise à jour** : permet de prendre connaissance du contenu de la mise à jour.
* **Lancer la mise à jour** : permet de lancer le processus de mise à jour.

### Options des jeux

Ce menu propose les réglages suivants :

* **Format jeux** :
  * Auto
  * Configuration RetroArch
  * Fourni par le core
  * Ne pas régler
  * Pixel carré
  * Spécifique RetroArch
  * 1/1
  * 16/10
  * 16/15
  * 16/9
  * 19/12
  * 19/14
  * 2/1
  * 21/9
  * 3/2
  * 3/4
  * 30/17
  * 32/9
  * 4/1
  * 4/3
  * 5/4
  * 6/5
  * 7/9
  * 8/3
  * 8/7
  * 9/16 
* **Lisser les jeux** : permet de donner un petit effet de flou aux pixels pour que se soit plus joli sur nos TV modernes. 
* **Rembobinage** : vous autorise à effectuer des retours dans le temps lors de votre partie.

>Cela peut ralentir certains émulateurs (PS1, Dreamcast, ...) si vous l'activez par défaut sur ces émulateurs.  
>Vous pouvez l'activer seulement pour certains émulateurs via le menu [Paramètres avancés](#parametres-avances). 
{.is-warning}

* **Sauvegarde/Chargement auto** : permet de reprendre une partie là où on l'avait laissé en quittant le jeu. 
* **Appuyer deux fois pour quitter le jeu** : permet de confirmer que l'on veux bien quitter le jeu en cours.
* **Ajuster l'échelle (pixel parfait)** : affiche les jeux dans leur résolution d'origine.
* **Shaders** : vous pouvez appliquer un filtre sur l'image de votre jeu afin de lui donner un effet particulier comme une image plus rétro.
* **Shaders prédéfinis** : vous pouvez configurer facilement les _shaders_ pour les différents systèmes :
  * Les shaders **scanlines** activent les scanlines sur tous les systèmes afin d'obtenir un rendu proche d'un écran CRT. 
  * Les shaders **retro** sont un pack de shaders, correspondant au meilleur shader à appliquer à chaque système. Les shaders composant ce pack, sont choisis par la communauté et vous apporteront l'expérience de jeux la plus proche de l'expérience originale pour chaque système !
  Pour plus d'informations, vous pouvez consulter le tutoriel [Configuration des shaders](./../../tutorials/games/generalities/shaders-configuration).

>Vous pouvez aussi changer de _shader_ pendant le jeu, en utilisant votre manette. Utilisez les [Commandes spéciales](./special-commands/in-game) `Hotkey` + `R2` ou `Hotkey` + ` L2` pour voir s'afficher le shader suivant ou précédent.
{.is-info}

* **Options de Retroachievements** : permet de paramétrer Retroachievements (Équivalent des succès/trophées mais pour les anciennes consoles). 
* **Options NetPlay** : permet de paramétrer le NetPlay (jeu en ligne).
* **Cacher les jeux pré-installés** : permet de cacher les jeux pré-installés- de votre système.

### Réglages manettes

C'est ici que vous pouvez configurer vos manettes.

* **Configurer une manette** : permet de définir les boutons d'une de vos manettes.
* **Associer une manette Bluetooth** : permet d'associer une manette sans fil à votre Recalbox.
* **Oublier les manettes Bluetooth** : permet de désassocier toutes les manettes sans fil.
* **Pilote** : permet de définir le pilote à utiliser avec votre manette. Vous n'avez pas à y toucher tant que votre manette fonctionne.

### Options de l'interface

Ce menu propose l'accès aux réglages de l'interface.

* **Économiseur d'écran** : permet de modifier l'économiseur d'écran avec différentes options.
* **Horloge dans le menu** : permet d'afficher l'heure actuelle dans les menus d'EmulationStation.
* **Affichage de l'aide** : permet d'afficher une popup d'aide en haut à droite de votre écran pour chacune des options en appuyant sur le bouton `Y`.
* **Réglage des popups** : permet de modifier les options des popups d'aidem de musique et du NetPlay.

>Les popups de mise à jour ne sont pas configurables.
{.is-info}

* **Sélection rapide du système** : permet de basculer d'un système à l'autre par les touches droite ou gauche dans les listes de jeux.
* **Thème** : choix des différents thèmes créés par la communauté (ces nouveaux thèmes ne sont pas disponibles par défaut avec Recalbox).
* **Configuration du thème** : certains des thèmes installés ont des options de configuration.
* **Mise à jour de la liste des jeux** : si vous avez ajouté ou supprimé un jeu, vous devez mettre à jour la liste des jeux pour que vos modifications soient prises en compte.
* **Inverser les boutons Valider/Annuler** : inverse les boutons valider et annuler de vos manettes.

### Options du son

Ce menu vous propose les options suivantes :

* **Volume du système** : régler le volume de tout le système.
* **Mode audio** : permet de choisir quels sons vous souhaitez entendredans EmulationStation.
* **Sortie audio** : permet de sélectionner la sortie audio
  * VC4-HDMI-0 - HDMI/DISPLAYPORT
  * HEADPHONES - ANALOG OUTPUT

D'autres sorties audio peuvent être listées, à vous de voir ce qui vous convient.

### Options réseau

Ce menu propose les options suivantes :

* **Statut** : permet d'afficher si vous êtes connecté ou pas.
* **Adresse IP** : permet d'obtenir l'adresse IP de votre Recalbox.
* **Activer le wifi** : permet d'activer ou de désactiver le wifi.
* **SSID wifi** : permet d'indiquer le nom de votre réseau wifi.
* **Clef wifi** : permet d'indiquer votre mot de passe wifi.
* **Nom du réseau** : permet de modifier le nom de votre Recalbox sur votre réseau local.

### Scrapeur

Pour chaque jeu, vous pouvez obtenir des informations (pochette, résumé, etc...) qui seront affichées dans le menu de sélection des jeux de EmulationStation.

Pressez `START` et aller dans `SCRAPEUR`. Suivez alors les instructions à l'écran.  Vous trouverez plus d'infos sur le scrapeur interne [ici](./../features/internal-scraper)".

### Paramètres avancés

* **Overclock** : permet de régler l'overclocking.
  * AUCUN
  * HIGH
  * TURBO
  * EXTREM
  * NOLIMIT

>Les réglages TURBO et EXTREM peuvent causer des dommages irréversibles au Raspberry Pi s'ils ne sont pas fait dans de bonnes conditions (dissipation de chaleur, ventilation, etc.). La garantie de votre matériel peut être annulée.
{.is-danger}

* **Paramètres de démarrage** : permet de définir certains paramètres de démarrage.
* **Systèmes virtuels** : permet de faire afficher des systèmes virtuels dans la liste des systèmes.
* **Cacher les jeux pour adultes** : permet de cacher les jeux indiqués comme étant pour adultes. Fonctionne une fois que vos jeux auront été scrapés.
* **Configuration avancée de l'émulateur** : permet de configurer les options des émulateurs indépendamment des autres. 
* **Options Kodi** : permet d'activer/désactiver Kodi, de démarrer automatique,ent Kodi au démarrage de Recalbox ou d'activer le raccourci pour démarrer Kodi depuis la liste des systèmes.
* **Sécurité** : Permet de renforcer la sécurité de Recalbox et de modifier le mot de passe root.
* **Overscan** : permet de vous aider si vous avez des bandes noires ou si l'image est plus grande que l'écran.
* **Afficher la fréquence** : permet d'afficher la fréquence (images par secondes) dans EmulationStation et dans les jeux.
* **Gestionnaire web Recalbox** : permet d'activer ou de désactiver le gestionnaire web Recalbox.
* **Restaurer les paramètres d'usine** : permet de réinitialiser votre Recalbox au même état comme si c'était une nouvelle installation. Ceci ne touche absolument pas vos données personnelles.

### Vérification des bios

Ce menu vous permet de gérer les bios requis par certains des émulateurs de Recalbox. Vous obtiendrez plus d'informations sur [cette page](./../features/bios-manager).

### Licence open-source

Ce menu vous affichera la licence d'utilisation de Recalbox.

### Quitter

Menu qui permet **d'éteindre Recalbox.**

* Éteindre
* Extinction rapide
* Redémarrage

## Contrôles

#### Les commandes dans l'interface :

* `A` → Sélectionner
* `B` → Retour
* `Y` → Favoris dans la liste des jeux, démarre les clips de jeux dans la liste des systèmes
* `X` → Démarrer Kodi ou le lobby Netplay dans la liste des systèmes
* `Start` → Menu
* `Select` → Options pour quitter dans la liste des systèmes, affiche les favoris du système dans les listes des jeux
* `R` → Groupe de jeux alphabétique suivant dans les listes de jeux, ouvre la fonction recherche dans la liste des systèmes
* `L` → Groupe de jeux alphabétique précédent dans les listes de jeux

## Favoris

Vous pouvez définir un jeu comme favoris en pressant la touche `Y` sur le jeu. Le jeu sera alors placé en début de liste avec une ☆ devant son nom.

>Le système doit être éteint proprement en utilisant le menu de EmulationStation pour que que la liste de jeux en favoris soit sauvegardée et que vous la retrouviez au prochain démarrage.
{.is-warning}