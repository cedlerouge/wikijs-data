---
title: Avec un jeu Naomi
description: 
published: true
date: 2021-09-04T17:54:42.949Z
tags: lightgun, calibration, naomi, 7.2+
editor: markdown
dateCreated: 2021-05-21T08:45:47.606Z
---

## Exemple de calibration dans un jeu Naomi

1) Activer le mode service de Naomi dans RetroArch (`Hotkey` + `B`).

2) Configurer un "controller" player 1.

3) Retourner dans le jeu (`Hotkey` + `B`) et lancer le MENU TEST avec le bouton `L3`.

4) Sélectionner le menu avec `R3` et valider avec `L3`.

5) Arrivé dans la calibration ou juste avant, on retourne dans RetroArch (`Hotkey` + `B`) mettre un "lightgun" en player 1.

6) Quand la calibration est fini, il faudra peut-être refaire l'étape 2 pour sortir du MENU TEST.

7) Quand on repasse dans le jeu, on retourne encore dans RetroArch (`Hotkey` + `B`) pour remettre le lightgun en player 1.

8) On peut tester maintenant le résultat de la calibration

## Example de calibration dans un jeu Naomi (_le cas de The House of The Dead 2_)

Par exemple, pour ce type de jeu (The house of dead 2 sur Naomi), on rentre dans le menu "**NAOMI TEST MODE**" mais c'est pas simple...

**Pré-requis :** on a besoin d'une manette

1) Il faudra rentrer dans RetroArch (`Hotkey` + `B`) - Menu rapide

![](/basic-usage/features/lightgun/calibration/naomi1.png){.full-width}

2) Puis aller dans "Options"

![](/basic-usage/features/lightgun/calibration/naomi2.png){.full-width}

3) Où Il faudra vérifier que l'option "Allow NAOMI Service Buttons" est activé

![](/basic-usage/features/lightgun/calibration/naomi3.png){.full-width}

4) On devra ensuite aller dans le menu principale / Réglages / Entrées

![](/basic-usage/features/lightgun/calibration/naomi4.png){.full-width}

5) Puis dans "Touches du port 1"

![](/basic-usage/features/lightgun/calibration/naomi5.png){.full-width}

6) Pour sélectionner le "controller" (on a besoin d'une manette et pas du lightgun pour activer le menu)

7) Puis pour finir (`Hotkey` + `B`) pour ressortir du menu rapide et retourner au jeu.

**NAOMI TEST MODE :** on y accède à partir de `L3` à la manette

![](/basic-usage/features/lightgun/calibration/naomi6.png){.full-width}

8) On sélection le menu "**GAME TEST MODE**" grâce à `R3` et on valide l'entrée du menu par `L3`.

![Sélection du menu](/basic-usage/features/lightgun/calibration/naomi7.png){.full-width}

![Après validation, on arrive dans le menu "GAME TEST MODE"](/basic-usage/features/lightgun/calibration/naomi8.png){.full-width}

9) On peut maintenant sélectionner le menu "**GUN SETTING**" grâce à `R3` et on valide l'entrée du menu par `L3`.

![Sélection du menu](/basic-usage/features/lightgun/calibration/naomi9.png){.full-width}

![Après validation, on arrive dans le menu "GAME TEST MODE"](/basic-usage/features/lightgun/calibration/naomi10.png){.full-width}

10) On peut maintenant sélectionner le menu "**PLAYER1 GUN ADJUSTEMENT**" grâce à `R3` et on valide l'entrée du menu par `L3`.

![Sélection du menu](/basic-usage/features/lightgun/calibration/naomi11.png){.full-width}

![](/basic-usage/features/lightgun/calibration/naomi12.png){.full-width}

11) **Attention**, il faudra retourner dans RetroArch (`Hotkey` + `B`) puis dans le menu principale / Réglages / Entrées / Touches du port 1 pour réactiver le "light gun" (à la place de "controller") pour pouvoir le calibrer.

On peut maintenant calibrer en commençant par **shooter sur le viseur bleu en haut à gauche puis en bas à droite**.

![](/basic-usage/features/lightgun/calibration/naomi13.png){.full-width}

![Après selection des 2 coins, le calcul de la calibration opère...](/basic-usage/features/lightgun/calibration/naomi14.png){.full-width}

12) On peut voir le résultat et tester le OFF SCREEN (qui se trouve en bas et à gauche dans ce jeu).

![](/basic-usage/features/lightgun/calibration/naomi15.png){.full-width}

![](/basic-usage/features/lightgun/calibration/naomi16.png){.full-width}

13) **Attention**, il faudra retourner encore une fois dans RetroArch (`Hotkey` + `B`) puis dans le menu principale / Réglages / Entrées/ Touches du port 1 pour réactiver le "controller" (à la place de "light gun") pour pouvoir sortir.

On peut valider la configuration par la touche **R3** de la manette et revenir au menu précédent.

14) On pourra recommencer à partir de l'étape 3 mais en sélectionnant le menu "**PLAYER2 GUN ADJUSTEMENT**" si on a un setup pour deux joueurs.

15) Et pour finir, on sélectionne EXIT avec `L3`/`R3` et dans chaque menu pour sortir et revenir au jeu.

![On sort du menu "GUN SETTING"](/basic-usage/features/lightgun/calibration/naomi17.png){.full-width}

![On sort du menu "GAME TEST"](/basic-usage/features/lightgun/calibration/naomi18.png){.full-width}

![On sort du menu "NAOMI TEST MODE"](/basic-usage/features/lightgun/calibration/naomi19.png){.full-width}

![Le système de jeu se relance](/basic-usage/features/lightgun/calibration/naomi20.png){.full-width}

16) Et pour finir, avant de jouer, on conseil de redémarrer le jeu pour que toutes les confs de RetroArch soient remises à la configuration par défaut.

![](/basic-usage/features/lightgun/calibration/naomi21.png){.full-width}

>Il faudra bien garder ses fichiers, vous n'aurez plus à le refaire, cela sera dans vos sauvegardes :  
>
>![](/basic-usage/features/lightgun/calibration/calibration2.png){.full-width}
{.is-warning}