---
title: Systèmes virtuels
description: 
published: true
date: 2021-09-04T17:46:20.040Z
tags: systèmes, virtuels
editor: markdown
dateCreated: 2021-06-12T00:06:46.772Z
---

## Présentation

Longtemps attendus également, des systèmes virtuels (comme le système Favoris) parmi les classiques :

* Tous les jeux.
* Tous les jeux multi-joueurs (2 joueurs et plus).
* Tous les derniers jeux joués, triés automatiquement par date.

Sans oublier le système **Arcade**, qui réunit tous les systèmes MAME, FBN, NEO-GEO, ... qui sera désormais activable/désactivable dans les menus.  
Mais ce n'est pas tout.

Pour ceux qui rescraperont leurs systèmes avec le nouveau scrapeur interne (Skraper n’étant pas encore à jour), _**de nouveaux systèmes virtuels par genre de jeux pourront être activés**_.  
Oui, vous avez bien lu :

* Par genre.
* Par jeux de Shoot'em up ?
* Par jeux de plateforme ?
* Par jeux de combat ?
* Par jeux de puzzle ?

Activez les systèmes virtuel des genres qui vous intéressent, et ils seront tous réunis au sein d'une même liste !

## Configuration via le Menu EmulationStation

* Menu EmulationStation

![](/basic-usage/features/virtualsystem1.png){.full-width}

* Paramètres avancées

![](/basic-usage/features/virtualsystem2.png){.full-width}

* Systèmes Virtuels
  * Montrer tout les jeux (On/Off)
  * Montrer les jeux multijoueurs(On/Off)
  * Montrer les derniers jeux jouer (On/Off)
  * Afficher le système Lightgun (On/Off)
  * Systèmes Virtuels par genre

![](/basic-usage/features/virtualsystem3.png){.full-width}

![](/basic-usage/features/virtualsystem4.png){.full-width}

  * Système Virtuel Arcade
    * Activer le système Virtuel Arcade (On/Off)
    * Inclure la Neo-Geo (On/Off)
    * Cacher les systèmes originels (On/Off)
    * Position
      * Veuillez sélectionner l'emplacement voulu dans la liste des systèmes.

![](/basic-usage/features/virtualsystem5.png){.full-width}

![](/basic-usage/features/virtualsystem6.png){.full-width}

## Configuration via le fichier recalbox.conf (Facultatif)

```ini
## Arcade metasystem
## Activate the Arcade metasystem to group all games from piFBA, FBN (libretro), MAME and optionally Neogeo
## into a single "Arcade" system.
;global.arcade=1
## You may want to specify its position in the system list. (Default: 0)
## Negatives values may be used to tart from the end (-1 = last position)
;global.arcade.position=0
## Include NeoGeo or not (default: 1)
;global.arcade.includeneogeo=1
## Hide included system or leave them in the system list (default: 1)
;global.arcade.hideoriginals=1
```

### Option pour activer la position dans la liste de systèmes. 

```ini
## You may want to specify its position in the system list. (Default: 0) ## Negatives values may be used to start from the end (-1 = last position)
;global.arcade.position=0
```

###  Cacher les sous dossiers dans le dossier principale 

```ini
## Hide included system or leave them in the system list (default: 1)
global.arcade.hideoriginals=1
```