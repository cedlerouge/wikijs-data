---
title: Économiseurs d'écran
description: 
published: true
date: 2021-09-04T17:48:31.841Z
tags: économiseurs d'écran
editor: markdown
dateCreated: 2021-06-11T19:20:18.478Z
---

## Introduction

Dans EmulationStation, il y a la possibilité de régler l'économiseur d'écran à utiliser, et certains d'entre-eux ont des options détaillés dans leurs propres pages.

## Utilisation

* Dans la liste des systèmes, pauuyez sur le bouton `START` et allez dans `OPTIONS DE L'INTERFACE` puis dans `ÉCONOMISEUR D'ÉCRAN`.
* Ici, vous aurez plusieurs choix :

  * `MISE EN VEILLE APRÈS` : permet de définir au bout de combien de minutes l'économiseur d'écran se met en route. Entre 0 et 30 minutes au choix.
  * `COMPORTEMENT DE L'ÉCRAN DE VEILLE` : permet de choisir quel économiseur d'écran appliquer. Plusieurs choix sont disponibles.
  * `SYSTÈMES À UTILISER EN DÉMOS / CLIPS DE JEUX` : permet de choisir les systèmes avec lesquels l'économiseur d'écran agira. Valide uniquement pour les démos de jeux et les clips de jeux.

## {.tabset}

### Mode démo

Le mode démo n'est ni plus, ni moins qu'un écran de veille qui lance vos jeux en fond d'écran.

Le mode demo pioche dans une liste de systèmes (par défaut il y a toutes les consoles 8/16 bits, hormis les portables), et il lance un jeu au hasard dans un système choisi. Il conserve un historique interne pour éviter de lancer 2 fois le même système d'affilée ou de lancer 2 fois le même jeu.

![](/basic-usage/features/screensavers/demomode1.png)

#### Activation

Pour activer cette fonction, rien de plus simple :

* Dans EmulationStation, ouvrez le menu avec le bouton `START`.
* Allez dans `OPTIONS DE L'INTERFACE` > `ÉCONOMISEUR D'ÉCRAN`.
* Ici, allez dans `COMPORTEMENT DE L'ÉCRAN DE VEILLE` et choisisssez `DÉMOS DE JEUX`.

#### Configuration

Vous pouvez configurer les systèmes à utiliser pour les démos de jeux :

* Dans EmulationStation, ouvrez le menu avec le bouton `START`.
* Allez dans `OPTIONS DE L'INTERFACE` > `ÉCONOMISEUR D'ÉCRAN`.
* Ici, allez dans `SYSTÈMES À UTILISER EN DÉMOS / CLIPS DE JEUX` et sélectionnez les systèmes auxquels les jeux seront utilisés.

### Mode clips de jeux

Le mode clips de jeux va utiliser les vidéos de vos jeux scrapés pour les afficher à l'écran en tant qu'économiseur d'écran.

Le mode clips de jeux pioche dans une liste de systèmes (par défaut il y a toutes les consoles 8/16 bits, hormis les portables), et il lance une vidéo au hasard dans un système choisi. Il conserve un historique interne pour éviter de lancer 2 fois le même système d'affilée ou de lancer 2 fois la même vidéo.

#### Activation

Pour activer cette fonction, rien de plus simple :

* Dans EmulationStation, ouvrez le menu avec le bouton `START`.
* Allez dans `OPTIONS DE L'INTERFACE` > `ÉCONOMISEUR D'ÉCRAN`.
* Ici, allez dans `COMPORTEMENT DE L'ÉCRAN DE VEILLE` et choisisssez `CLIPS DE JEUX`.

#### Configuration

Vous pouvez configurer les systèmes à utiliser pour les clips de jeux :

* Dans EmulationStation, ouvrez le menu avec le bouton `START`.
* Allez dans `OPTIONS DE L'INTERFACE` > `ÉCONOMISEUR D'ÉCRAN`.
* Ici, allez dans `SYSTÈMES À UTILISER EN DÉMOS / CLIPS DE JEUX` et sélectionnez les systèmes auxquels les jeux seront utilisés.