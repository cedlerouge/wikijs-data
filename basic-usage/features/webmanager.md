---
title: Gestionnaire web
description: 
published: true
date: 2021-09-04T17:26:11.258Z
tags: web, gestionnaire
editor: markdown
dateCreated: 2021-05-21T07:54:20.497Z
---

## Pour vous connecter

Il suffit de saisir dans votre navigateur web simplement son nom :

## {.tabset}

### Windows

[http://recalbox/](http://recalbox/)

Si cela ne fonctionne pas, vous pouvez utiliser [l'adresse IP de votre Recalbox](./../../tutorials/network/ip/discover-recalbox-ip).

### Linux et macOS

[http://recalbox.local/](http://recalbox.local/)

Si cela ne fonctionne pas, vous pouvez utiliser [l'adresse IP de votre Recalbox](./../../tutorials/network/ip/discover-recalbox-ip).

## Interface

Lorsque vous êtes sur la page principale, vous pouvez :

* Modifier le fichier `recalbox.conf`.
* Ajouter/Supprimer des roms.
* Consulter les logs.
* Monitorez votre board.
* Lancer le gamepad virtuel (pratique pour configurer en 5min sa recalbox pour la 1ère utilisation) - via smartphone ou tablette uniquement.
* Consulter les fichiers logs (journaux) de Recalbox.

## Désactivation du Gestionnaire web

Par défaut, le gestionnaire web démarre automatiquement. Si vous souhaitez, vous pouvez le désactiver.

* Dans EmulationStation, appuyez sur le bouton `START`.
* Allez dans `PARAMÈTRES AVANCÉS`.
* Mettez l'option `GESTIONNAIRE WEB RECALBOX` sur OFF.

## Manette virtuelle

Pour y accéder directement, il suffit de saisir l'adresse IP de votre Recalbox avec le port 8080.

```text
http://192.168.0.X:8080
```

```text
http://recalbox:8080
```

Consulter le manuel d'utilisation de la manette virtuelle [ici](./../../basic-usage/first-use-and-configuration#7-manettes-virtuelles)

Si vous trouvez un potentiel problème ou bug, veuillez le signaler [ici](https://github.com/recalbox/recalbox-manager).