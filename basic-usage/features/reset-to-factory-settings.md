---
title: Restaurer les paramètres d'usine
description: 
published: true
date: 2021-09-04T17:26:56.050Z
tags: 7.2+, restaurer, paramètres, usine
editor: markdown
dateCreated: 2021-05-21T07:53:43.100Z
---

## Présentation

Depuis Recalbox 7.2, il y a une nouvelle option nommée `Retour aux paramètres d'usine` qui vous permet de remettre votre système à neuf, dans le même état après avoir fait une installation au propre tout neuve. Cette option vous permet de revenir à un état neuf avec plus de facilité que de refaire régulièrement des nouvelles installations.

>Cette option **ne touche pas** à vos bios, jeux et sauvegardes !
{.is-success}

## Utilisation de l'option

Cette option se trouve dans EmulationStation. Pour cela, il vous suffit d'appuyer sur `START`, de vous rendre dans `PARAMÈTRES AVANCÉS` et de sélectionner `RESTAURER LES PARAMÈTRES D'USINE`.

![](/basic-usage/features/resetfactory1.png){.full-width}

Une fois que vous aurez sélectionné l'option, vous aurez deux fenêtres de mise en garde.

![](/basic-usage/features/resetfactory2.png){.full-width}

![](/basic-usage/features/resetfactory3.png){.full-width}

>Une fois ces deux fenêtres validées, **vous n'aurez plus la possibilité de revenir en arrière**.
{.is-danger}

Quand la restauration aux paramètres d'usine sera terminé, vous retrouverez votre installation de Recalbox toute propre !