---
title: Gestionnaire de mise à jour
description: 
published: true
date: 2021-09-04T17:25:42.634Z
tags: mise à jour, 7.0+, gestionnaire
editor: markdown
dateCreated: 2021-05-21T07:54:08.366Z
---

![](/basic-usage/features/update.png)

## Une nouvelle façon de faire les mises à jour!

Depuis Recalbox 7, le système de mise à jour a entièrement été revu et ne peut pas fonctionner avec les anciennes versions.

* Le nouveau système de mise à jour est bien plus rapide :
  * _**5 à 10mn maximum pour une mise à jour**_ (hors temps de téléchargement bien sûr). 
* Le nouveau système de mise à jour est bien plus robuste :
  * Terminé les mises à jour qui crashent et qui vous obligent à tout recommencer (même si ça restait rare, heureusement !) 
* Les nouvelles mises à jour seront gérées par EmulationStation : affichage de la progression, gestion des erreurs, etc.
* Il vous sera possible de faire des mises à jour hors ligne de façon extrêmement simple : en copiant l'image de la nouvelle version dans un répertoire de la carte SD accessible à partir de tous les systèmes d'exploitation : Windows, Linux et macOS ! Un petit redémarrage, le système détecte l'image, opère la mise à jour et hop, terminé !
* Il vous sera possible désormais de placer vos propres vidéos de boot, dans un répertoire facilement accessible sur le réseau (dans `/share/bootvideos/`) et de choisir si le lecteur doit jouer des vidéos seulement parmi celles de Recalbox, seulement parmi les vôtres, ou parmi tout ce qu'il a à disposition.
  * Qu'est-ce que ça vient faire dans les mises à jour vous demandez-vous ? Rien, c'est juste que cette nouvelle possibilité est liée à la nouvelle structure de Recalbox pour faciliter les mises à jour ! 
* N'en déplaise à ceux qui escroquent et volent Recalbox autant que leurs clients, les _**notifications de mises à jour ne peuvent plus être désactivées**_ dans EmulationStation : seule la notification par popup intrusif est désactivable. Mais vous verrez toujours un popup non intrusif vous signaler qu'une mise à jour est disponible.

## Fonctionnement

La mise à jour peut se faire de 2 façons : en ligne (online) ou hors-ligne (offline).

## {.tabset}

### Méthode en ligne (online)

La méthode en ligne est la plus simple :

* Dans EmulationStation, appuyez sur le bouton `START` de votre manette.
* Allez dans `MISES À JOUR`.
* Si une mise à jour est disponible, vous aurez la ligne `MISE À JOUR DISPONIBLE` indiquant `OUI` à droite. Si c'est le cas, allez sur la ligne `LANCER LA MISE À JOUR` et validez.
* La mise à jour se mettra à être téléchargée.
* Une fois téléchargé, votre Recalbox redémarrera automatiquement et la mise à jour s'effectuera.

### Méthode hors-ligne (offline)

Cette façon de mettre à jour est un peu plus complexe à être effectuée.

* Rendez-vous sur cette page où vous trouverez toutes les images de la dernière version de Recalbox.
* Téléchargez l'image correspondant à votre architecture (rpi / odroid / pc) ainsi que le fichier `.SHA1` correspondant.
* Une fois téléchargez, il y a 2 possibilitées :
  * Vous pouvez utiliser un logiciel tel que [WinSCP](./../../../tutorials/system/access/network-access-winscp), [Cyberduck](./../../../tutorials/system/access/network-access-cyberduck) ou [MobaXTerm](./../../../tutorials/system/access/network-access-mobaxterm) pour mettre les 2 fichiers dans le répertoire `/boot/update/` (en montant la [partition de démarrage](./../../../tutorials/system/access/remount-partition-with-write-access) en lecture/écriture).
  * Prendre la carte SD de votre Recalbox éteinte, le mettre dans votre PC, d'ouvrir le lecteur nommé `RECALBOX` et de mettre les 2 fichiers dans le répertoire `/update`.
* Une fois cela fait, démarrez ou redémarrez votre Recalbox et la mise à jour s'effectuera.

## 

Pendant la mise à jour, si les 7 barres passent aussitôt toutes au rouge au lieu d'être en blanc au fur et à mesure, notez le nombre de barres blanches qui étaient visibles et contactez le support ([forum](https://forum.recalbox.com) ou [Discord](https://discord.gg/NbQFbGM)).