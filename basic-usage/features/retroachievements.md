---
title: Retroachievements
description: 
published: true
date: 2021-09-04T17:44:38.724Z
tags: retroachievements
editor: markdown
dateCreated: 2021-05-21T07:53:49.009Z
---

## Défis / Succès avec RetroArch

Le site internet [retroachievements.org](http://www.retroachievements.org/) propose des défis / succès / trophées sur les plateformes tels que NES, SNES, GB, GBC, GBA, Megadrive/Genenis, PCengine etc...

## Création d'un compte

* Vous devez [créer un compte](http://retroachievements.org/createaccount.php) sur le site [retroachievements.org](http://www.retroachievements.org/). 
* Notez votre pseudonyme (_Username_) et votre mot de passe (_password_) dans un coin car ils vont servir à configurer les Retroachievements.

## Activation du compte dans Recalbox

Cela se passe dans le menu d'EmulationStation.

* Ouvrez le menu d'EmulationStation avec le bouton `START`. 
* Allez dans `OPTIONS DES JEUX` > `OPTIONS DE RETROACHIEVEMENTS`.
* Modifiez comme ceci :

  * `RETROACHIEVEMENTS` : ON
  * `NOM UTILISATEUR` : mettre votre pseudonyme
  * `MOT DE PASSE` : votre mot de passe

Vous avez aussi l'option du mode hardcore, à vous de décider si vous l'activez ou non.

>Le mode hardcore désactive les fonctions de rembobinage, crédits illimités, etc...
{.is-info}

## Les émulateurs compatibles avec Retroachievements.

Tous les cores Libretro ne sont pas compatibles avec les Retroachievements. Si vous souhaitez savoir pour un core particuier si cela est supportée, vous devez aller dans la page de l'émulateur dans la partie [émulateurs](./../../emulators).

## Afficher les défis d'un jeu en cours

Pour visionner les défis / trophées dans RetroArch, il suffit d'activer le menu de RetroArch (`Hotkey`+`B`) > `Succès`.

## Quelles roms sont compatibles ?

La liste des jeux compatibles sont disponibles sur [cette page](http://retroachievements.org/gameList.php)

En général, pour les consoles les roms No-Intro en version USA fonctionnent mieux pour les Retroachievements, à quelques rares exceptions où l'on peut avoir aussi quelques roms européennes et pour l'arcade avoir la bonne version du set.