---
title: 🔧 COMPATIBILITÉ MATÉRIEL
description: 
published: true
date: 2021-08-12T07:41:04.689Z
tags: compatibilité, matériel
editor: markdown
dateCreated: 2021-05-21T07:48:46.220Z
---

Sur les pages suivantes, vous pouvez vérifier si votre matériel a été testé et est supportée, ainsi que vérifier si un système ou émulateur spécifique est compatible avec votre matériel.