---
title: ¡Bienvenido a la wiki de Recalbox!
description: ¿Cómo empiezo a usar Recalbox?
published: true
date: 2021-08-02T16:14:26.452Z
tags: 
editor: markdown
dateCreated: 2021-07-28T12:18:47.035Z
---

![welcome.png](/home/welcome.png)

## ¿Qué es Recalbox?

[Recalbox](https://www.recalbox.com) es **la consola de juegos retro definitiva** que te permite volver a jugar a todos los juegos, consolas y ordenadores de tu infancia.

Con [Recalbox](https://www.recalbox.com), (re)descubre y (re)juega a los títulos que han marcado la historia de los videojuegos, ¡muy fácilmente!

[Recalbox](https://www.recalbox.com) puede instalarse fácilmente en nanoordenadores muy baratos como la Raspberry Pi 4, en consolas portátiles como el Odroid Go Super, o en cualquier ordenador (reciente o antiguo).

## Quiero instalar Recalbox, ¿cómo lo hago?

Para instalar Recalbox en tu placa/pc, dirígete a la página [Preparación e instalación ](./basic-usage/preparation-and-installation)

Para saber más sobre los sistemas y emuladores compatibles, puedes consultar la sección [Compatibilidad de hardware](./hardware-compatibility)

## ¿Busco una página en particular?

Utilice la función de **búsqueda** en la parte superior de esta página.

Sólo tiene que introducir su búsqueda en el cuadro de texto:
![search-text.png](/home/search-text.png)

O busque las páginas por etiquetas:
![search-tags.png](/home/search-tags.png)

## Soy principiante, ¿por dónde empiezo?

* Le sugerimos que consulte la documentación:
  * [Uso básico](./basic-usage), para entender **los fundamentos del funcionamiento de** [Recalbox](https://www.recalbox.com).
 * [Las F.A.Q](./faq).
  * [Compatibilidad con emuladores](./hardware-compatibility/emulators-compatibility)
  * [Compatibilidad de dispositivos](./hardware-compatibility/compatible-devices).
  * También puedes consultar la sección [Emuladores](./emulators) para obtener más información sobre los que están en [Recalbox](https://www.recalbox.com).
  * Si algunos términos le parecen demasiado técnicos, el [glosario](/basic-usage/glossary) puede ser un valioso aliado.
* Para un uso más avanzado de Recalbox:
  * Consulte la sección [Tutoriales](./tutorials).
  * Visite la sección [Uso-avanzado](./advanced-use).
* También puede consultar para obtener información adicional: 
  * Sitio web de [Recalbox](https://www.recalbox.com/fr/).
  * El [foro](https://forum.recalbox.com/).

## ¿Tienes algún consejo para un nuevo usuario?

[Recalbox](https://www.recalbox.com) es un proyecto de **código abierto** y, por tanto,  **gratuito**.

Como tal, las personas que trabajan en él lo hacen en su tiempo libre, y el proyecto y su documentación evolucionan a medida que la gente contribuye. 

**El mejor consejo** que podemos darte es :

* Ser curioso.
* Leer la documentación.
* Leer los [tutoriales](./tutorials).
* Participar en la redacción y traducción de esta wiki (ver [Cómo contribuir](./contribute))
* Buscar en el [foro](https://forum.recalbox.com/).
* Haz siempre una copia de seguridad de tu carpeta **SHARE** antes de hacer cualquier cosa de la que no estés seguro.