---
title: 💡 FAQ
description: 
published: true
date: 2021-08-02T17:24:54.960Z
tags: 
editor: markdown
dateCreated: 2021-08-02T17:19:35.360Z
---

## General

### ¿En qué puedo instalar Recalbox?

Puedes instalar Recalbox en los siguientes dispositivos:

- Raspberry Pi 4
- Raspberry Pi 400
- Raspberry Pi Zero
- Raspberry Pi 1 (B/B+)
- Raspberry Pi 2
- Raspberry Pi 3 (B/B+)
- Odroid XU4
- Odroid Go Advance (consola portátil)
- Odroid Go Super (consola portátil)
- PC x86 (32 bits)
- PC x86_64 (64 bits)

### ¿Cómo puedo participar?

- Puedes hacer una donación en nuestro Paypal
- Puede pedir su material en kubii.fr/221-recalbox
- Puede informar de errores o ideas de mejora en nuestro [Gitlab](https://gitlab.com/recalbox/recalbox/issues)
- Puede participar en los [foros de recalbox](http://forum.recalbox.com)
- Puedes [contribuir a esta wiki](./contribute)

### ¿Dónde puedo ver el progreso del desarrollo de Recalbox?

En el [Recalbox gitlab](https://gitlab.com/recalbox). Puede acceder a :  
- los [temas](https://gitlab.com/recalbox/recalbox/issues) que son las tareas actuales.  
- los [milestones](https://gitlab.com/recalbox/recalbox/-/milestones) donde se pueden ver las tareas que aún están por desarrollar, y las que están terminadas.

### ¿Cuántos juegos contiene Recalbox?

Estamos intentando añadir el mayor número posible de juegos gratuitos a la distribución, para que puedas disfrutar de la experiencia Recalbox desde el primer lanzamiento.
Vaya a [esta página](https://forum.recalbox.com/topic/52/homebrews-sur-la-recalbox) para enviar sus homebrews!

### ¿Puedo utilizar Recalbox en un quiosco / bartop?

Sí, el sistema Recalbox también ha sido diseñado para ser integrado en quioscos y bartops.
- Puedes conectar los botones y joysticks directamente a los GPIOs de la Raspberry.
- Puedes desactivar los menús para evitar cualquier mal uso.
- También es posible utilizar las salidas HDMI / DVI /VGA (con adaptador) o RCA.
Puede encontrar más información en [la página dedicada](./tutorials/system/installation/bartop-arcade-configuration).

### Recalbox soporta muchos sistemas pero sólo veo algunos en la interfaz, ¿qué puedo hacer?

Sólo se muestran los sistemas con juegos disponibles. Prueba a añadir tus roms a través de la red y reinicia Recalbox para que aparezcan los nuevos sistemas. Además, algunos equipos no son compatibles con todas las consolas.

### ¿Debería overclockear mi Raspberry Pi?

En la Raspberry Pi 1 (incluida la Zero), se aconseja encarecidamente overclockearla en modo EXTREMO.

## EmulationStation

### ¿Cómo puedo cambiar el aspecto del tema de EmulationStation?

Pulse `Start` en el mando → `CONFIGURACION DE INTERFACE` → `TEMA`.

### ¿Cómo puedo silenciar la música de fondo?

Pulse `Start` en el mando → `CONFIGURACION DE AUDIO` → `MODO DE AUDIO` → SIN SONIDO

### ¿Dónde se encuentran los archivos de configuración de EmulationStation en el sistema de archivos?

```
/recalbox/share/system/recalbox.conf
```

### ¿Dónde se encuentran los temas de EmulationStation?

```
/recalbox/share/themes
```

## Emulación

### ¿Por qué hay varios emuladores para cada sistema?

La emulación no es una operación impecable, hay un equilibrio entre rendimiento, funcionalidad y fidelidad.
Como la emulación no es perfecta, es mejor tener varias opciones para manejarla, a veces un emulador más rápido será mejor pero puede tener problemas inesperados, mientras que uno más lento puede tener mayor precisión pero no soportar características específicas.

### ¿Cómo puedo cambiar el emulador por defecto para un sistema determinado?

Pulsando `START` en el mando, debes de ir a opciones avanzadas, y finalmente a la configuración del emulador avanzada. Puedes seleccionar el sistema deseado, puedes cambiar la configuración del emulador y del kernel (ten en cuenta que la configuración de la base no cambiará si la configuración del emulador dice Default).

## Sistema

### Recalbox soporta alrededor de 100 consolas, pero sólo veo algunas, ya sea en la interfaz de EmulationStation o en la carpeta compartida.

Dependiendo de la placa/pc que utilices tendrás emuladores compatibles con ella.  
Consulte la lista de compatibilidad por placa/pc [aquí](./hardware-compatibility/).

### ¿Por qué en algunos sistemas como la GBA mis juegos muestran una pantalla negra y luego vuelven en EmulationStation?

> Algunos sistemas requieren bios para funcionar.

## Solución de problemas generales

### Mi Recalbox acaba de fallar, ¿debo desconectarlo y volver a conectarlo?

No, en la mayoría de los casos, el sistema no se bloqueó por completo, sólo lo hizo EmulationStation.

Hay varias formas de manejar esta situación, la más fácil es utilizar las funciones de depuración del Webmanager para reiniciar EmulationStation o cerrar Recalbox correctamente.

No hacerlo puede corromper tus datos, especialmente si estás usando una Raspberry Pi con una tarjeta SD.


