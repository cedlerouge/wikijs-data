---
title: MÁS INFORMACIÓN
description: Cómo contribuir, licencias, enlaces útiles...
published: true
date: 2021-08-02T17:36:47.648Z
tags: 
editor: markdown
dateCreated: 2021-08-02T17:36:47.648Z
---

Aquí encontrarás la presentación general de Recalbox, quiénes somos, las licencias y la posibilidad de contribuir al proyecto de diferentes maneras.