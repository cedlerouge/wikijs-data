---
title: Contribuir al proyecto
description: Cómo contribuir al proyecto Recalbox
published: true
date: 2021-08-02T17:54:35.306Z
tags: 
editor: markdown
dateCreated: 2021-08-02T17:50:13.881Z
---

Recalbox es un proyecto gratuito de código abierto y nos encantaría tenerte como colaborador.

## Perfiles buscados
Si tienes habilidades y te gustaría utilizarlas para mejorar el proyecto, puedes contarlo en nuestro discord.

Siempre estamos buscando perfiles que puedan ayudarnos:
- **Diseñador gráfico** : recalbox, página web, redes sociales
- **Diseñador de UX**: ux recalbox, web, ui
- **Desarrollador**: bash, c, c++, python (python 2 > 3)
- **Traductor**: traducción y corrección de Recalbox, sitios, wiki
- **Usuarios avanzados**: informes de errores detallados

## Informes de errores

La primera forma de participación es informar detalladamente de los fallos que encuentres en Recalbox.

Los **[Issues](https://gitlab.com/recalbox/recalbox/-/issues)** gitlab son tickets de solicitud de errores o mejoras que permiten a los usuarios, desarrolladores y probadores discutir el tema en un solo lugar.

Cuando informes de un fallo, no dudes en ser lo más específico posible. Describa el contexto, añada capturas de pantalla y, por qué no, explique cómo ha solucionado el fallo si es el caso.

¡Vaya a https://gitlab.com/recalbox/recalbox/-/issues !

## Desarrollo

Las fuentes de Recalbox están disponibles en https://gitlab.com/recalbox/recalbox

El desarrollo de Recalbox requiere la lectura de un mínimo de documentación sobre el funcionamiento de las bases del sistema: **buildroot**.

Una vez que entiendas cómo funciona, puedes empezar haciendo tu propia imagen de Recalbox como se indica en el [README.md](https://gitlab.com/recalbox/recalbox/-/blob/master/README.md) del proyecto.

Entonces, será necesario conocer los fundamentos de **bash** y los comandos de linux.

Dependiendo de tus habilidades, puedes empezar con diferentes temas:

- Actualización de paquetes en buildroot (bash, compilación)
- Añadir un paquete en buildroot (bash, compilación)
- Añadir un emulador independiente (bash, compilación, c/c++, python)
- Añadir funcionalidad a EmulationStation (c++)

No buscamos necesariamente personas súper técnicas, sino alguien motivado, autónomo y capaz de trabajar en equipo en el modelo de código abierto :D

## Traducciones

### Wiki

Si quieres participar en la **traducción de la wiki**, visita la página [Contribuir a la wiki](../contribute)

### Interfaz

Si quieres participar en la **traducción de la interfaz de EmulationStation**, puedes unirte a la página del proyecto en [poeditor.com](https://poeditor.com/join/project/hEp5Khj4Ck). Se realizan actualizaciones periódicas del archivo de idioma fuente, que le aportamos para que lo traduzca a los idiomas de su elección.

### Videos de Youtube

Si quieres participar en la **traducción de subtítulos para los vídeos tutoriales de Recalbox**, ponte en contacto con el equipo en nuestras **[redes sociales, Discord o foro](./../../presentation/useful-links).**
