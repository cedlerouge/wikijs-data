---
title: Função de pesquisa
description: 
published: true
date: 2021-07-26T22:55:30.137Z
tags: 
editor: markdown
dateCreated: 2021-07-26T22:55:30.137Z
---

## Apresentação

Há muito solicitado por nossos queridos usuários, aqui está, finalmente!

_**Procure rapidamente sua coleção de jogos digitando algumas letras...**_

É uma busca em tempo real, a lista se atualiza à medida que você digita. Você pode pesquisar em nomes, arquivos, descrições, ou tudo de uma só vez.

Naturalmente, não poderíamos oferecer tal recurso sem uma revisão completa do teclado virtual.  
Sentado diante de sua TV, controle na mão, o que poderia ser mais doloroso do que andar no velho teclado de tela cheia, onde entrar com um único personagem exigia manipulações demais?

Redesenhamos um teclado novíssimo, bonito e muito mais ergonômico: um teclado em estilo arcade, com uma roda de caracteres onde selecionar, apagar e mover o cursor é um verdadeiro prazer.

Não podemos lhe dar todos os detalhes, mas você deve saber que quase todos os botões/pads/joysticks de um controle completo são usados pelo novo teclado! Naturalmente, é nativamente compatível com um teclado físico. E para completar, é semi-transparente, para que você possa ver o que está acontecendo por baixo em tempo real.

Sem mais delongas, algumas fotos!

![](/basic-usage/features/search.png){.full-width}

## Como funciona

* Pressionando `R1` no menu do console, você abre a função de pesquisa.
* Comece a escrever o nome de um jogo (Exemplo acima: Super).
* Aproveite o visual da lista de jogos, começando com sua palavra de busca.
* Selecione o jogo que você deseja jogar.