---
title: Manual Netplay Dolphin
description: Jogar online no Gamecube e no Wii
published: true
date: 2021-07-26T22:49:31.105Z
tags: 
editor: markdown
dateCreated: 2021-07-26T22:49:31.105Z
---

>Atenção, note que para jogar online no Gamecube, você precisará ter um teclado e um mouse.
{.is-warning}

## Acesse o menu Netplay 

* Inicie qualquer jogo Gamecube ou Wii.
* No jogo, com um teclado, pressione `ALT` + `F4` para acessar o menu Dolphin.
* No menu, utilizando o mouse, abra o menu `Ferramentas`.

![](/basic-usage/features/netplay/dolphin-netplay-tool-menu.png)

### 1. Criar uma sala de jogos 

* Clique em `Iniciar Netplay...`.

![](/basic-usage/features/netplay/dolphin-hote-netplay.png)

* Clique na aba `Host` e selecione seu jogo na lista.

![](/basic-usage/features/netplay/dolphin-netplay-hote-setting.png)

* Uma janela de bate-papo de texto aparece e sua sala já está acessível!

![](/basic-usage/features/netplay/dolphin-netplay-hote-chat-room.png)

>Na maioria dos jogos, adicione cerca de 1 Buffer para 15 ms de latência por cliente.
{.is-warning}

>Dois jogadores com 50 ms de latência são cerca de 3-4 buffers, enquanto três jogadores com 50 e 65 ms de latência são cerca de 7 buffers.
{.is-success}

* Para iniciar o jogo, clique em `START`!

### 2. Participar de uma sala de jogos

* Clique em `Procurar sessões NetPlay`.

![](/basic-usage/features/netplay/dolphin-netplay-client.png)

* Na Sala de jogos em rede do Dolphin você pode ver todos os jogos online.

![](/basic-usage/features/netplay/dolphin-netplay-client-lobby.png)

* Clique duas vezes na sala para acessar o bate-papo de texto.
* Espere que o jogo seja lançado pelo host na sala de jogos!

![](/basic-usage/features/netplay/dolphin-netplay-client-chat-room.png)