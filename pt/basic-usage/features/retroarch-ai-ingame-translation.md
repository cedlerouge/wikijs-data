---
title: Retroarch AI (tradução dentro do jogo)
description: Traduza seus jogos em tempo real!
published: true
date: 2021-08-06T00:02:46.871Z
tags: 
editor: markdown
dateCreated: 2021-08-06T00:02:46.871Z
---

## O que é Retroarch AI?

Bem-vindo ao futuro! 

Há algum tempo, o RetroArch permite o uso de um serviço de tradução chamado "[**OCR**](./../../basic-usage/glossary)**"** (Reconhecimento Óptico de Caracteres) e **Síntese de Voz,** o que lhe permitirá traduzir seus jogos estrangeiros quase instantaneamente!


> Esta característica requer uma conexão à Internet e um controle com pelo menos um stick analógico esquerdo!
{.is-danger}

![Antes](/basic-usage/features/ocr1-pt.png)

![Depois](/basic-usage/features/ocr2-pt.png)

## Configuração:

Como você sabe, tudo é feito para que a configuração deste tipo de serviço seja o mais simples possível. 


O primeiro passo é criar uma conta em [Ztranslate.net](https://ztranslate.net/) um serviço de tradução de terceiros chamado API, mas outras APIs podem ser configuradas!

![](/basic-usage/features/ztranslate_signup.png)

* Uma vez registrado e logado no site, vá para a seção de configurações para obter sua `API KEY`.

![](/basic-usage/features/ztranslate_settings.png)

* Abra seu arquivo `recalbox.conf` localizado em `/recalbox/share/system/`.
* Encontre a linha `;global.translate.apikey=RECALBOX`.
* Apague o `;` no início da linha e cole sua `API KEY` no lugar de `RECALBOX`.
* Aproveite a oportunidade para dizer ao sistema em que idioma você quer que a tradução esteja, alterando o parâmetro `global.translate.to`. Você também pode deixar o parâmetro definido como `auto`, neste caso o RetroArch tentará adivinhar o idioma alvo.

![](/basic-usage/features/recalbox.conf_fr.png)

## Aqui vamos nós!

Divirta-se e esqueça a frustração, inicie um jogo 100% japonês!

* Para ativar a tradução, nada poderia ser mais fácil: com seu controle, pressione os botões `HOTKEY + R3 (PRESSIONE O STICK ANALÓGICO ESQUERDO)`, o jogo é pausado e lhe mostra uma imagem com o texto de substituição.
* Pressione a mesma combinação de teclas novamente para retomar o jogo.

Por padrão, O Recalbox usa o modo imagem em vez de texto-para-fala. Para utilizar este último, basta uma simples [sobreposição de configuração](./../../advanced-usage/configuration-override/retroarch-overrides)