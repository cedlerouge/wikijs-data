---
title: Netplay (Jogos Online)
description: 
published: true
date: 2021-07-20T23:46:18.736Z
tags: 
editor: markdown
dateCreated: 2021-07-20T23:46:18.736Z
---

![](/basic-usage/features/netplay/logo-recalbox_netplay.png)

## Introdução

Netplay é um recurso que permite a conexão em rede multiplayer com alguns emuladores via rede peer-to-peer (P2P) para reduzir a latência da rede, a fim de garantir a melhor experiência possível, sincronizando perfeitamente várias instâncias executando o mesmo núcleo e conteúdo de emulação.
Você também tem a possibilidade de ter pessoas juntando-se em modo espectador (latência adicional).
Jogue on-line em nossos consoles antigos que ainda nem conheciam essa capacidade!

## Recomendações

#### Conexão

* Conecte sua máquina com um cabo Ethernet RJ45 (uma conexão wifi é muito menos estável).
* Tenha uma internet com suporte UPNP ou abra sua porta manualmente.
* Se o Recalbox estiver conectado em um roteador que está conectado diretamente à Internet, está tudo bem (NAT simples).
* Se o Recalbox estiver conectado em um roteador que está ligado em outro roteador(ao invés da internet), então o Netplay não funciona (Double NAT).

### Roms

* Para consoles de cartuchos
  * Usar roms de romsets **No-Intro** (dumps mais próximos dos jogos originais)
* Para consoles de CD
  *Utilizar roms de romsets **Redump** (dumps mais próximas dos jogos originais)

### Sistema

* Se você estiver usando um Raspberry Pi0, Pi1, Pi2, Pi3-b e Pi3-b+, um **overclock** (a 1300 MHZ) é **aconselhável ou obrigatório***.
* Para Raspberry Pi4, x86_64 (64bit), x86 (32bit) PCs e Odroid, o overclocking não é necessário.

## Mensagem de erro Netplay

#### Como cliente: "Failed to Initialize Netplay" ==> "Falha na inicialização do Netplay"

"Failed to initialize Netplay" muitas vezes significa que você não pode se conectar com o host. Confirme que você tem o endereço IP correto para o host e que o host começou a hospedar uma sessão NetPlay. Diga ao host para verificar se seu firewall baseado no host permite que a RetroArch aceite conexões e confirme que o encaminhamento de portas está funcionando.

#### Como um anfitrião: "Port Mapping Failed" ==> "Falha no Mapeamento de Porta"

O "Port Mapping Failed" provavelmente indica um problema de encaminhamento de porta UPnP.  
Se você puder configurar manualmente seu gateway de rede para encaminhar a porta TCP 55435 para o endereço IP de rede local de seu dispositivo RetroArch, você deve fazê-lo. Você também pode habilitar o uso de um servidor de relé (adiciona latência).

#### "Content not found, try loading content manually" ==> "Conteúdo não encontrado, tente carregar o conteúdo manualmente"

* Carregue o conteúdo manualmente, coloque o conteúdo em sua lista de histórico recente ou escaneie seu conteúdo em uma lista de reprodução.

## FAQ

#### 1 - Por que não posso hospedar?

Há 2 possibilidades:

* Conexão de Internet insuficiente

  * **Experimente como cliente.**

* Sua porta não se abre automaticamente

  * **Ative a opção UPNP de seu modem** ou

  ou

  * **Abra sua porta manualmente (Recomendado)** ou

  ou como último recurso

  * **Tente utilizando a função `SERVIDOR RELÉ NETPLAY`**.

#### 2 - Se eu começar a hospedar, o jogo trava e volta para o menu Recalbox

Verifique se seus núcleos são compatíveis com sua máquina (PC, Raspberry Pi ou outro). Além disso, algumas máquinas suportam apenas a função de cliente.

#### 3 - O Netplay requer abertura de portas para funcionar?

Sim, tanto o host quanto o cliente devem abrir as portas corretamente.

Há uma opção de segurança que pode ser usada por aqueles que não querem abrir suas portas (função `SERVIDOR RELÉ NETPLAY`).

#### 4 - O que você precisa para que o Netplay funcione corretamente?

* Mesma versão do Recalbox.
* Mesma versão do Retroarch ou Dolphin.
* O mesmo núcleo para o emulador.
* Mesma ROM (com nome, pontuação e assinatura ROM idênticos).
* Estar conectado via cabo RJ45 (recomendado).

#### 5 - O Netplay no PSX / N64 / Playstation / PSP / Dreamcast / DS funciona ?

Não, os requisitos de desempenho desses consoles dificultam o Netplay.

#### 6 - Posso jogar jogos GB / GBC / GBA / PSP / DS com várias pessoas via Netplay?

Não, o Netplay do RetroArch não faz emulação do Cabo Link.  
GB, GBC, GBA e PSP não são atualmente possíveis com nossa implementação.  
Mas, uma exceção notável é o mesmo jogo GB/GBC (Netplay através dos núcleos TGB-Dual e Sameboy).  
A troca de Pokémons e situações similares não são, portanto, possíveis.