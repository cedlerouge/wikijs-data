---
title: Sistemas Virtuais
description: 
published: true
date: 2021-08-12T00:15:56.975Z
tags: 
editor: markdown
dateCreated: 2021-08-12T00:15:56.975Z
---

## Apresentação

Há muito esperados, os sistemas virtuais (como o sistema Favoritos) estão entre os clássicos:

* Todos os jogos.
* Todos os jogos multijogador (2 jogadores ou mais).
* Todos os últimos jogados, classificados automaticamente por data.

Sem esquecer o sistema **Arcade**, que reúne todos os sistemas MAME, FBN, NEO-GEO, etc. que agora pode ser ativado/desativado nos menus.
Mas isso não é tudo.

Para aqueles que estão fazendo scrape de seus sistemas com o novo scraper interno (Skraper ainda não atualizado), _**novos sistemas virtuais por gênero de jogo podem ser ativados**_.
Sim, você leu certo :

* Por gênero.
* Por jogos de tiro
* Por jogos de plataforma
* Por jogos de luta
* Por jogos de quebra-cabeça

Ative os sistemas virtuais dos gêneros que lhe interessam, e todos estarão reunidos em uma lista!

## Configuração através do menu do EmulationStation

* Menu EmulationStation

![](/basic-usage/features/virtualsystem1-pt.png){.full-width}

* Configurações avançadas

![](/basic-usage/features/virtualsystem2-pt.png){.full-width}

* Sistemas Virtuais
  * Mostrar o sistema todos os jogos (On / Off)
  * Mostrar o sistema multijogadores (On / Off)
  * Mostrar o sistema últimos jogados (On / Off)
  * Mostrar o sistema Lightgun (On / Off)
  * Sistemas virtuais por gênero

![](/basic-usage/features/virtualsystem3-pt.png){.full-width}

![](/basic-usage/features/virtualsystem4-pt.png){.full-width}

  * Sistema Virtual Arcade
    * Habilitar o sistema Virtual Arcade (On / Off)
    * Inclui Neo-Geo (On / Off)
    * Esconder sistemas originais (On / Off)
    * Posição
      * Selecione o local desejado na lista de sistemas.


![](/basic-usage/features/virtualsystem5-pt.png){.full-width}

![](/basic-usage/features/virtualsystem6-pt.png){.full-width}

## Configuração por meio do arquivo recalbox.conf (opcional)

```ini
## Arcade metasystem
## Activate the Arcade metasystem to group all games from piFBA, FBN (libretro), MAME and optionally Neogeo
## into a single "Arcade" system.
;global.arcade=1
## You may want to specify its position in the system list. (Default: 0)
## Negatives values may be used to tart from the end (-1 = last position)
;global.arcade.position=0
## Include NeoGeo or not (default: 1)
;global.arcade.includeneogeo=1
## Hide included system or leave them in the system list (default: 1)
;global.arcade.hideoriginals=1
```

### Opção para ativar a posição na lista do sistema.

```ini
## You may want to specify its position in the system list. (Default: 0) ## Negatives values may be used to start from the end (-1 = last position)
;global.arcade.position=0
```

### Ocultar subpastas na pasta principal

```ini
## Hide included system or leave them in the system list (default: 1)
global.arcade.hideoriginals=1
```