---
title: Video Snaps
description: 
published: true
date: 2021-08-12T23:14:14.156Z
tags: 
editor: markdown
dateCreated: 2021-08-12T23:14:14.156Z
---

Os "Video Snaps" são, nada mais e nada menos, do que pequenos vídeos dos jogos sendo exibidos na sua lista de jogos ao invés do seu "scrape" habitual.

Você pode ver um exemplo [aqui](https://www.youtube.com/watch?v=r5oabVrfQpk).

>Para quem nunca utilizou esta função, você deve saber que os vídeos ocupam espaço muito rapidamente, apesar de sua excelente compressão MP4 _(considere entre 1Mb e 6Mb por arquivo, dependendo do sistema)_.
> Se você quiser tirar o máximo proveito desta função, um disco rígido USB3 de 1TB ou 2TB será essencial!
{.is-warning}

## Configuração

Para desfrutar destes vídeos, você precisará primeiro refazer o scrape das suas listas de jogos, usando Skraper ou o novo scraper interno do Recalbox.

#### Com o scraper interno

Veja [esta página](./../../basic-usage/features/internal-scraper) da documentação.

### Através do software Skraper

Você pode assistir [este vídeo](https://www.youtube.com/watch?v=G388Gc6kkRs) para aprender a usar o Skraper.
