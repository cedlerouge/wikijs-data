---
title: Em um jogo Naomi
description: 
published: true
date: 2021-07-18T21:34:08.584Z
tags: 
editor: markdown
dateCreated: 2021-07-18T14:41:43.533Z
---

## Exemplo de calibração em um jogo de Naomi

1) Ativar o modo de serviço Naomi em RetroArch (`Hotkey` + `B`).

2) Configurar um "controller" no player 1.

3) Voltar ao jogo (`Hotkey` + `B`) e iniciar o TEST MENU com o botão `L3`.

4) Selecionar o menu com `R3` e confirmar com `L3`.

5) Uma vez dentro ou pouco antes da calibração, volte ao RetroArch (`Hotkey` + `B`) e coloque uma lightgun no player 1.

6) Quando a calibração estiver concluída, você poderá ter que fazer o passo 2 novamente para sair do TEST MENU.

7) Quando voltamos ao jogo, voltamos ao RetroArch (`Hotkey` + `B`) para colocar uma lightgun no player 1.

8) Agora podemos testar o resultado da calibração

## Exemplo de calibração em um jogo de Naomi (_o caso de The House of The Dead 2_)

Por exemplo, para este tipo de jogo (The house of dead 2 do Naomi), entramos no menu "**NAOMI TEST MODE***", mas não é simples...

**Pré-requisitos:** você precisa de um controle

1) Você tem que entrar no RetroArch (`Hotkey` + `B`) - Menu rápido

![](/basic-usage/features/lightgun/calibration/naomi1-pt.png){.full-width}

2) Então vá para "Opções" !

![](/basic-usage/features/lightgun/calibration/naomi2-pt.png){.full-width}

3) Onde você tem que verificar se a opção "Allow NAOMI Service Buttons" (Permitir botões de serviço NAOMI) está ativada

![](/basic-usage/features/lightgun/calibration/naomi3-pt.png){.full-width}

4) Devemos então voltar para o Menu principal e ir para Configurações/Entrada

![](/basic-usage/features/lightgun/calibration/naomi4-pt.png){.full-width}

5) Então em "Controles da Porta 1"!

![](/basic-usage/features/lightgun/calibration/naomi5-pt.png){.full-width}

6) Para selecionar o "controller" (precisamos de um joystick e não da lightgun para ativar o menu)

7) Depois, finalmente (`Hotkey` + `B`) para sair do menu rápido e voltar ao jogo.

**NAOMI TEST MODE :** acessado a partir de `L3` no joystick

![](/basic-usage/features/lightgun/calibration/naomi6.png){.full-width}

8) Selecione o menu "**GAME TEST MODE**" com `R3` e confirme a entrada do menu com `L3`.

![Seleção do menu](/basic-usage/features/lightgun/calibration/naomi7.png){.full-width}

![Após validação, chegamos ao menu "GAME TEST MODE"](/basic-usage/features/lightgun/calibration/naomi8.png){.full-width}

9) O menu "**GUN SETTING**" pode agora ser selecionado com `R3` e a entrada do menu é confirmada com `L3`.

![Seleção de menu](/basic-usage/features/ lightgun/calibration/naomi9.png){.full-width}

![Após validação, chegamos ao menu "GAME TEST MODE"](/basic-usage/features/lightgun/calibration/naomi10.png){.full-width}

10) O menu "**PLAYER1 GUN ADJUSTMENT**" pode agora ser selecionado com `R3` e a entrada do menu é confirmada com `L3`.

![Seleção de menu](/basic-usage/features/lightgun/calibration/naomi11.png){.full-width}

![](/basic-usage/features/lightgun/calibration/naomi12.png){.full-width}

11) **Por favor, atenção**, você terá que voltar ao RetroArch (`Hotkey` + `B`) e depois ao menu principal / Configurações / Entrada / Controles da Porta 1 para reativar o "lightgun" (ao invés de "controller") para conseguir calibrá-la.

Agora você pode calibrar primeiro **atirando na mira azul superior esquerda e depois na inferior direita***.

![](/basic-usage/features/lightgun/calibration/naomi13.png){.full-width}

![Após selecionar os 2 cantos, o cálculo de calibração ocorre...](/basic-usage/features/lightgun/calibration/naomi14.png){.full-width}

12) Você pode ver o resultado e testar o OFF SCREEN (que está na parte inferior esquerda deste jogo).

![](/basic-usage/features/lightgun/calibration/naomi15.png){.full-width}

![](/basic-usage/features/lightgun/calibration/naomi16.png){.full-width}

13) **Nota por favor**, você terá que voltar ao RetroArch (`Hotkey` + `B`) e depois ao menu principal / Configurações / Entradas / Chaves Porta 1 para reativar o "controller" (ao invés de "lightgun") para conseguir sair.

Você pode validar a configuração pressionando **R3** no controle e retornar ao menu anterior.

14) Podemos começar novamente a partir do passo 3, mas selecionando o menu "**PLAYER2 GUN ADJUSTMENT**" se tivermos uma configuração para dois jogadores.

15) Finalmente, selecione EXIT com `L3`/`R3` em cada menu para sair e voltar ao jogo.

![Sair do menu "GUN SETTING"](/basic-usage/features/lightgun/calibration/naomi17.png){.full-width}

![Sair do menu "GAME TEST"](/basic-usage/features/lightgun/calibration/naomi18.png){.full-width}

![Sair do menu "NAOMI TEST MODE"](/basic-usage/features/lightgun/calibration/naomi19.png){.full-width}

![O sistema de jogo reinicia](/basic-usage/features/lightgun/calibration/naomi20.png){.full-width}

16) E finalmente, antes de jogar, é aconselhável reiniciar o jogo para que todas as configurações da RetroArch sejam reiniciadas para a configuração padrão.

![](/basic-usage/features/lightgun/calibration/naomi21.png){.full-width}

> Você manterá essa configuração em seus arquivos, não terá que fazê-la novamente, estará em seus saves:  

>![](/basic-usage/features/lightgun/calibration/calibration2.png){.full-width}
{.is-warning}