---
title: E para os usuários avançados
description: Como adicionar você mesmo jogos "lightgun" como um especialista ;-)
published: true
date: 2021-07-20T00:16:24.709Z
tags: 
editor: markdown
dateCreated: 2021-07-20T00:16:24.709Z
---

## Como vamos mais longe?

### A idéia básica...

O princípio é ter um arquivo de configuração único reunindo todas as informações necessárias para os jogos "Lightgun" de todos os sistemas e emuladores. 

Desta forma, poderemos fazer herança, agrupamento de jogos, mas também gerenciar particularidades sem ter um arquivo por jogo ou por sistema como já podemos fazer, mas sem poder agrupar sistemas, por exemplo. Isto é possível com este xml único. 

Além disso, nas versões seguintes, poderemos acrescentar ajuda on-line, mensagens durante o carregamento dos jogos, etc...

Outra vantagem em comparação com as clássicas sobrecargas é que você pode adicionar funções na python no Configgen para buscar/identificar se um hardware está presente como a Mayflash dolphin bar ou para saber em qual placa/pc você está. Você pode decidir configurar entre 1 e 3 jogadores Lightgun desta forma.

>Como lembrete, isto só é possível para o Libretro no momento. Poderemos fazer isso para outros emuladores com o arquivo xml que já temos.
{.is-info}

### O arquivo de configuração XML

Na verdade, a configuração dos jogos está em um arquivo xml (para informações, na verdade é um ".cfg").

O arquivo é organizado assim em termos de estrutura _(aqui ele está vazio sem configurações para mostrar a estrutura xml completa)_ :

```xml
<?xml version="1.0"?>
<root>
	<common_inputs>
 	</common_inputs>
  <common_options>
  </common_options>
  <system>
 		<platform></platform>
		<emulator>
			<core></core>
		</emulator>
    <inputs>
    </inputs>
    <options>
    </options>
    <games>
      <inputs>
      </inputs>
      <options>
      </options>
      <game>
        <name></name> 
        <inputs>
        </inputs>
        <options>
        </options>
      </game>
    </games>
  </system>
</root>
```

### Tags XML

**&lt;root>** : esta é a base do XML onde tudo será encontrado, assim como uma versão para acompanhar a evolução deste arquivo

```xml
<?xml version="1.0"?>
<root>
	<!-- internal version of the file to help follow-up of update, same version until release -->
	<version>1.0 - Beta8 - 20-10-2020</version>
</root>
```

**&lt;common_inputs>**: esta é a parte em &lt;root> e sobrecarga comum de `retroarchcustom.cfg` que teremos para todos os sistemas, no caso do lightgun, colocaremos as teclas básicas como select / Sair / Start / Disparar.

```xml
	<!--MANDATORY inputs (for retroarchcustom.cfg): comon part to use for all systems (using dolphinbar and until 2 wiimotes) -->
	<common_inputs>
        <string name="input_player1_gun_start" value="enter" />
        <string name="input_player2_gun_start" value="enter" />
        <string name="input_player1_gun_select" value="escape" />
        <string name="input_player2_gun_select" value="escape" />
        <!-- force selecte and start for consoles games -->
        <string name="input_player1_select" value="escape" />
        <string name="input_player2_select" value="escape" />
        <string name="input_player1_start" value="enter" />
        <string name="input_player2_start" value="enter" />
        <!--for many it's necessary to move in menu or else -->
        <string name="input_player1_gun_dpad_up" value="up" />
        <string name="input_player1_gun_dpad_down" value="down" />
        <string name="input_player1_gun_dpad_right" value="right" />
        <string name="input_player1_gun_dpad_left" value="left" />
        <string name="input_player2_gun_dpad_up" value="up" />
        <string name="input_player2_gun_dpad_down" value="down" />
        <string name="input_player2_gun_dpad_right" value="right" />
        <string name="input_player2_gun_dpad_left" value="left" />
        <!-- index to manage distinction between guns -->
        <string name="input_player1_mouse_index" value="GUNP1" />
        <string name="input_player2_mouse_index" value="GUNP2" />
        <!-- wiimotes home button for exit game -->
		    <string name="input_exit_emulator" value="lsuper" />
	  	  <string name="input_enable_hotkey" value="lsuper" />
        <!-- set on fullscreen for don't see cursor mouse on x86(-64) -->
	  	  <string name="video_fullscreen" value="true" />
        <!-- to have help using overlay switch and finally ask to press 2 times home to exit -->
        <string name="input_overlay_next" value="lsuper" />
        <string name="quit_press_twice" value="true" />
 	</common_inputs>
```

**&lt;common_options>**: esta é a parte em &lt;root> e a substituição comum de `retroarch-core-options.cfg` que teremos. Em nosso caso, não é usado de uma forma comum, mas seria possível.

```xml
    <!--OPTIONAL options (for retroarch-core-options.cfg): comon part to use for all systems (using dolphinbar and until 2 wiimotes) -->
  	<common_options>
        <!-- only for more log on test -->
        <!-- <string name="libretro_log_level" value="2" />
        <string name="log_verbosity" value="true" /> -->
  	</common_options>
```

**&lt;string>**: estes são os valores básicos de configuração sempre compostos de 2 atributos _"name" e "value"_ :

```xml
<string name="input_player1_gun_start" value="enter" />
```

**&lt;system>**:  Esta é a parte que agrupa toda a configuração de um sistema. Ele contém as tags _&lt;platform>, &lt;emulator>, &lt;inputs>, &lt;options>, &lt;games> e &lt;game>_.

```xml
  <!-- ********************************************************* Nintendo Entertainment System ************************************************************** -->
	<system>
    <!-- MANDATORY: name of the platform as in roms directory -->
		<platform>nes</platform>
		<!-- MANDATORY: emulator and core selected for lightgun-->
		<emulator name="libretro">
			<core>fceumm</core>
		</emulator>
    <!--MANDATORY: inputs common to this system -->
		<inputs>
      	<!-- Wiimote button B -->
		  	<string name="input_player2_gun_trigger_mbtn" value="1" />
      	<!-- gamepad -->
      	<string name="input_libretro_device_p1" value="513" />
      	<!-- lightgun -->
      	<string name="input_libretro_device_p2" value="258" />
        <!-- Set index to GUNP1 on player 2 to have first wiimote as player 1 -->
        <string name="input_player2_mouse_index" value="GUNP1" />
		</inputs>
		<!--MANDATORY: options common to this system-->
		<options>
		  	<string name="fceumm_zapper_mode" value="lightgun" />
		  	<string name="fceumm_zapper_tolerance" value="6" />
      	<string name="fceumm_show_crosshair" value="enabled" />
		</options>
        <games>
            <!--list of games using lightgun-->
            <!-- see 'duckhunt' game to see more explainations to know how to setup game part -->
            <game tested="no">
                <name>2in1</name>
            </game>
```

**&lt;platform>**: é a tag que corresponde ao nome do sistema mas, finalmente, pode-se ter várias plataformas no mesmo sistema como para os sistemas baseados no emulador _flycast_ por exemplo.

```xml
  	<!-- ******************************* Sega Naomi & Sega NaomiGd & Sega Atomiswave ********************************************************* -->
  	<system>
    <!-- MANDATORY: name of the platform as in roms directory -->
        <platform>atomiswave</platform>
        <platform>naomi</platform>
        <platform>naomigd</platform>
    		<!-- MANDATORY: emulator and core selected for lightgun-->
    		<emulator name="libretro">
      			<core>flycast</core>
    		</emulator>
```


> É possível ter vários sistemas distintos com a mesma plataforma para ter jogos usando emuladores e/ou núcleos diferentes. Este é o caso da plataforma MAME utilizando um núcleo "MAME" (no PC) ou "mame2003plus" (nas placas como RPi por exemplo).
{.is-warning}

**&lt;emulator>**: esta tag tem como atributo o nome do emulador a ser utilizado para o sistema em questão:

```xml
    	<!-- MANDATORY: emulator and core selected for lightgun-->
    	<emulator name="libretro">
      		<core>flycast</core>
    	</emulator>
```

**&lt;core>**: Esta tag é o nome do núcleo a ser usado. É sempre encontrada em _&lt;emulator>_ como acima.

```xml
<core>flycast</core>
```

**&lt;inputs>**: esta tag permite que você sobrecarregue _retroarchcustom.cfg_ em adição a parte de _&lt;common_inputs>_ ao nível de _&lt;system>, &lt;games> e/ou &lt;game>_

```xml
<system>
.....
		<inputs>
      	<!-- Wiimote button B -->
		  	<string name="input_player2_gun_trigger_mbtn" value="1" />
      	<!-- gamepad -->
      	<string name="input_libretro_device_p1" value="513" />
      	<!-- lightgun -->
      	<string name="input_libretro_device_p2" value="258" />
        <!-- Set index to GUNP1 on player 2 to have first wiimote as player 1 -->
        <string name="input_player2_mouse_index" value="GUNP1" />
.....
```

```xml
.....
        <games>
            <inputs>
                <!-- no reload, this game is a free fire but need secondary input fo grenade and more -->
                <string name="input_player1_a_mbtn" value="2" />
                <string name="input_player2_a_mbtn" value="2" />     
            </inputs>
            <game tested="ok">
                <name>alien3thegun</name>
            </game>
.....
```

```xml
.....            
            <game tested="ok">
                <name>gunbuster</name>
                <inputs>
                    <!-- no reload free fire game but ned secondary input fo grenade and more -->
                    <!-- move players with dpad on wiimote  -->
                    <string name="input_player1_a" value="2" />
                    <string name="input_player1_gun_offscreen_shot_mbtn" value="nul" />
                    <string name="input_player2_a" value="2" />
                    <string name="input_player2_gun_offscreen_shot_mbtn" value="nul" />
                </inputs>
            </game>
.....
```

**&lt;options>**: esta tag permite que você sobrecarregue _retroarch-core-options.cfg_  em adição a parte &lt;common_options> ao nível de _&lt;system>, &lt;games> e/ou &lt;game>._ 

```xml
        <!-- options common to this system-->
        <options>
            <string name="genesis_plus_gx_gun_input" value="lightgun" />
            <string name="genesis_plus_gx_gun_cursor" value="enabled" />
        </options>
```

**&lt;games> :** esta tag é a raiz dos grupos de jogos. Isto permite ter uma configuração separada para alguns dos jogos no mesmo sistema, assim você pode ter várias seções _&lt;games>_  no mesmo sistema.

```xml
        <games>
            <inputs>
                <!-- no reload, these games have a free fire but need secondary input for grenade and more -->
                <string name="input_player1_a_mbtn" value="2" />
                <string name="input_player2_a_mbtn" value="2" />     
            </inputs>
            <game tested="ok">
                <name>alien3thegun</name>
            </game>
            <game tested="ok">
                <name>beastbusters</name>
            </game>
            <game tested="ok">
                <name>dragongun</name>
            </game>
        </games>

```

**&lt;game>:** esta tag que está em &lt;games> é para definir a configuração do jogo em particular.

```xml
            <game tested="ok">
                <name>gunbuster</name>
                <inputs>
                    <!-- no reload free fire game but ned secondary input fo grenade and more -->
                    <!-- move players with dpad on wiimote  -->
                    <string name="input_player1_a" value="2" />
                    <string name="input_player1_gun_offscreen_shot_mbtn" value="nul" />
                    <string name="input_player2_a" value="2" />
                    <string name="input_player2_gun_offscreen_shot_mbtn" value="nul" />
                </inputs>
            </game>
```


>**&lt;game>** tem o atributo _ "tested" _ para saber se já testamos esta rom em particular e para saber seu status (3 valores possíveis):
>
>* **no** -> não testado, mas será levado em consideração usando a configuração padrão do sistema.
>* **ok** -> testado com a configuração definida.
>* **nok** -> testado, mas não funcional, então não será configurado para funcionar no modo lightgun por enquanto (mas ainda temos esperança).
{.is-warning}

**&lt;name> :** esta tag em  _&lt;game>_ permite que você especifique o nome que será usado para identificá-lo para ativar o recurso de lightgun do Recalbox durante o início.

```xml
.....
<name>duckhunt</name>
.....
<name>mobilsuitgundamfinalshooting</name>
.....
```

> Você deve sempre colocar o nome **sem espaços / caracteres especiais e em minúsculas** ao analisar a "gamelist" de um sistema para o Recalbox. Atenção, é o nome do jogo em sua forma mais simples, não é nem o nome do arquivo da ROM nem o nome com a zona geográfica, etc... é necessário deixar o mais simples possível para que a correspondência ocorra com precisão. **Fazer um scrape garantirá que você tenha um jogo com um nome identificável.**
{.is-warning}

### Herança

Na verdade, neste parágrafo, gostaríamos de explicar que, a ordem da configuração será a seguinte: 

_**common -> system -> games -> game**_

.... Nós deixamos você nos dizer o que será igual _input_player2_gun_trigger_mbtn_ no exemplo a seguir

```xml
<?xml version="1.0"?>
<root>
	<common_inputs>
	  <string name="input_player2_gun_trigger_mbtn" value="1" />
 	</common_inputs>
  <common_options>
  </common_options>
  <system>
 		<platform></platform>
		<emulator>
			<core></core>
		</emulator>
    <inputs>
      <string name="input_player2_gun_trigger_mbtn" value="2" />
    </inputs>
    <options>
    </options>
    <games>
      <inputs>
        <string name="input_player2_gun_trigger_mbtn" value="1" />
      </inputs>
      <options>
      </options>
      <game>
        <name></name> 
        <inputs>
          <string name="input_player2_gun_trigger_mbtn" value="2" />
        </inputs>
        <options>
        </options>
      </game>
    </games>
  </system>
</root>
```

### Gerenciamento de index para vários jogadores

Tivemos que gerenciar os índices de "mouse" e especificá-los de uma maneira comum ou mais precisa por jogo. Por exemplo, quando o jogador 1 está na porta 2 e o jogador 2 na porta 3. Portanto, temos palavras-chave como **"GUNP1" , "GUNP2" ou "GUNP3".**

```xml
.....
     <common_inputs>
        <!-- index to manage distinction between guns -->
        <string name="input_player1_mouse_index" value="GUNP1" />
        <string name="input_player2_mouse_index" value="GUNP2" />
        .....
```

```xml
        <games> <!-- games using justifier -->
            <inputs>
                <!-- lightguns always on port 2 -->
                <!-- For Justifier device (blue cross) -->
                <string name="input_libretro_device_p2" value="516" />
                <string name="input_player2_mouse_index" value="GUNP1" />
                <!-- Wiimote button B or Z -->
                <string name="input_player2_gun_trigger_mbtn" value="1" />
                <!-- For Justifier(P2) device (pink cross) -->
                <string name="input_libretro_device_p3" value="772" />
                <string name="input_player3_mouse_index" value="GUNP2" />
                <!-- Wiimote button B or Z -->
                <string name="input_player3_gun_trigger_mbtn" value="1" />
                <!-- Common inputs part replicated for this game using player 3 -->
                <string name="input_player3_gun_start" value="enter" />
                <string name="input_player3_gun_select" value="escape" />
                <!-- Common inputs part replicated to force select and start for consoles games -->
                <string name="input_player3_select" value="escape" />
                <string name="input_player3_start" value="enter" />
            </inputs>
            <game tested="ok">
                <name>lethalenforcers</name>
            </game>
        </games>
```

## Mas de forma mais simples ...

Na verdade, o arquivo xml de configuração não pode ser encontrado na partição “SHARE”, mas está no sistema, acessível apenas em SSH em: `/recalbox/share_init/system/.emulationstation`

e com o nome: `lightgun.cfg`

Aqui está o que é exibido em um console SSH:

```shell
# cd /recalbox/share_init/system/.emulationstation
# ls -l
total 252
-rwxr-xr-x    1 root     root         51359 Oct 22 19:13 es_bios.xml
-rwxr-xr-x    1 root     root          1254 Oct 22 19:13 es_bios.xsd
-rwxr-xr-x    1 root     root         70383 Oct 22 19:13 es_input.cfg
-rwxr-xr-x    1 root     root          2979 Oct 22 19:13 es_settings.cfg
-rw-r--r--    1 root     root         61973 Oct 23 02:31 es_systems.cfg
-rw-r--r--    1 root     root         68513 Oct 22 14:19 lightgun.cfg
drwxr-xr-x    3 root     root            36 Oct 23 02:28 themes

```

>Para modificar este arquivo, você precisará permitir o acesso a escrita na [partição do sistema](./../../../tutorials/system/access/remount-partition-with-write-access).
{.is-warning}

Início deste arquivo quando aberto com um editor de texto :

![](/basic-usage/features/lightgun/advanced-users/advancedusers1.png)

#### Como adicionar um jogo/ROM ?

Você só tem que encontrar o sistema, exemplo para o NES :

![](/basic-usage/features/lightgun/advanced-users/advancedusers2.png)

E adicionar na seção &lt;games> a tag &lt;game> e depois &lt;name> assim :

```xml
           <game tested="ok">
                <name>monjeu2</name>
            </game>
```


> LEMBRETE: Você deve sempre colocar o nome **sem espaços / caracteres especiais e em minúsculas** ao analisar a "gamelist" de um sistema para o Recalbox. Atenção, é o nome do jogo em sua forma mais simples, não é nem o nome do arquivo da ROM nem o nome com a zona geográfica, etc... é necessário deixar o mais simples possível para que a correspondência ocorra com precisão. **Fazer um scrape garantirá que você tenha um jogo com um nome identificável.**
{.is-warning}

### Para verificar se está configurado corretamente

* Se o arquivo estiver formatado incorretamente, você retornará diretamente para a lista de jogos.
* Se o nome não for encontrado / reconhecido, você não poderá ativar o modo Lightgun.
* Se você estiver realmente no modo Lightgun, deverá conseguir sair do jogo com a tecla HOME no Wiimote.

### Para adicionar configurações a um jogo

O melhor é ver o que fizemos para os outros jogos... Em geral, você precisará modificar os parâmetros correspondentes aos botões de disparo primário e secundário, ou mesmo desativar os botões (veja abaixo), mas isso pode ser ainda mais complicado... Se for esse o caso, você terá que ir ver os manuais dos jogos da época e passar muito tempo nos menus RetroArch... então domine a arte de sobrecarregar!

```xml
        <game tested="ok">
            <name>ninjaassault</name>
            <inputs>
                <!--for this game start p1 for P2 aux_c -->
                <string name="input_player1_gun_start" value="nul" />
                <string name="input_player1_gun_aux_b" value="enter" />
                <!--for this game trigger -->
                <string name="input_player1_gun_trigger_mbtn" value="nul" />
                <string name="input_player1_gun_aux_a_mbtn" value="1" />
                <!--for this game real offscreen reload -->
                <string name="input_player1_gun_offscreen_shot_mbtn" value="nul" />
            </inputs>
        </game>
```

### Para adicionar um novo sistema

Neste caso, você realmente precisa conhecer o core a usar, os parâmetros RetroArch e em geral também configurar o "device" em _&lt;inputs>_ assim como os parâmetros "lightgun" do núcleo em  _&lt;options>_.

```xml
......
<system>
<!-- MANDATORY: name of the platform as in roms directory -->
    <platform>megadrive</platform>
    <!-- MANDATORY: emulator and core selected for lightgun-->
    <emulator name="libretro">
        <core>genesisplusgx</core>
    </emulator>
    <!-- MANDATORY: inputs common to this system-->
    <inputs>
        <!-- Wiimote button B or Z -->
        <string name="input_player2_gun_trigger_mbtn" value="1" />
        <!-- Wiimote button A or c - aux when it's necessary as jump, rocket, etc... -->
        <string name="input_player2_gun_aux_a_mbtn" value="2" />
        <!-- <string name="input_player2_gun_aux_a_mbtn" value="2" /> -->
        <!-- gamepad always on port 1-->
        <string name="input_libretro_device_p1" value="1" />
    </inputs>
    <!--MANDATORY: options common to this system-->
    <options>
        <string name="genesis_plus_gx_gun_input" value="lightgun" />
        <string name="genesis_plus_gx_gun_cursor" value="enabled" />
    </options>
    .....
```

**Boa sorte !!!**