---
title: A "Calibração"
description: Como podemos fazer os jogos funcionarem melhor?
published: true
date: 2021-07-18T14:25:33.278Z
tags: 
editor: markdown
dateCreated: 2021-07-18T14:25:33.278Z
---

## Introdução

Muitas vezes, em alguns jogos, especialmente jogos arcade, será necessário configurar o jogo antes de jogá-lo. No caso do arcade, você só terá que configurá-lo uma vez e ele permanecerá em sua NVRAM ou outro .CFG que você encontrará por sistema e/ou emulador em seu diretório "saves" da partição SHARE.

![](/basic-usage/features/lightgun/calibration/calibration1.png)

Mas para configurá-lo, você pode ter que ir para a configuração do núcleo ou mesmo para a configuração do jogo.