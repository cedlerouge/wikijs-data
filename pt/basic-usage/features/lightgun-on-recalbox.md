---
title: "Lightgun" no Recalbox
description: "Lightgun" (Pistola de luz) para todos ;-)
published: true
date: 2021-07-18T14:22:32.272Z
tags: 
editor: markdown
dateCreated: 2021-07-18T14:22:32.272Z
---

![](/basic-usage/features/lightgun/lightgun.png)

## Qual é o princípio?

O princípio é configurar automaticamente jogos compatíveis com lightgun para a maioria dos sistemas suportados pelo Recalbox. Por razões de custo e disponibilidade, queremos poder jogar com um Wiimote (ou dois para jogos de 2 jogadores). Também podemos conectar um Nunchunk para deportar o tiro primário e secundário, dependendo de nossa preferência. Este modo lightgun é ativado automaticamente quando você tem uma barra de golfinhos Mayflash no modo 2 e somente para jogos que suportam este modo.

O recurso de revólver da Recalbox permite que você jogue armas de bazuca e outros dispositivos retrorientadores novamente!

O princípio é configurar automaticamente jogos compatíveis com lightgun para a maioria dos sistemas suportados pela Recalbox. Por razões de custo e disponibilidade, queremos poder jogar com um Wiimote (ou dois para jogos de 2 jogadores). Também podemos conectar um Nunchunk para deportar o fogo primário e secundário, dependendo de nossa preferência. Este modo de pistola de luz é ativado automaticamente quando você tem uma Mayflash dolphin bar no modo 2 e somente para jogos que suportam este modo.

**Sistemas que já suportam este recurso no Recalbox:**

* 3DO
* Atomiswave
* Dreamcast
* Fbneo
* Mame
* Master System
* Megadrive
* Mega-CD
* Naomi
* Naomi GD ROM System
* NES
* Playstation 1
* Saturn
* SNES

>Os núcleos do emulador libretro são suportados neste momento, nenhum emulador ‘standalone’ (autônomo) por enquanto, talvez no futuro.
{.is-info}

>AVISO: isto só funcionará se os scrapes dos jogos ficarem com nomes claramente identificáveis como `duck hunt (world) v1.1` e não `dhnes`!! Deve ser possível encontrar o termo "duck hunt" no nome.
{.is-warning}

## Quais são os requisitos de hardware?

![Um a dois Nintendo Wiimotes (ou cópia compatível)](/basic-usage/features/lightgun/wiimote.png)

![Uma a duas Mayflash dolphin bar](/basic-usage/features/lightgun/dolphinbar.png)

>**Você precisará de uma Mayflash dolphin bar para Wiimote no modo lightgun (é como se o Wiimote se tornasse um mouse USB)**.
{.is-warning}

![Opcional : Nunchuck se você quiser atirar com "C" & "Z"](/basic-usage/features/lightgun/nunchuck.png)

![](/basic-usage/features/lightgun/gun1.png)![](/basic-usage/features/lightgun2.png)

![Opcional: você também pode se equipar para ser mais confortável](/basic-usage/features/lightgun/crossbow.png)

## Como instalar ?

### 1. Conecte a(s) Mayflash dolphin bar(s) ao seu Recalbox

![](/basic-usage/features/lightgun/usbports.png)

### 2. Colocar a(s) Mayflash dolphin bar(s) no modo 2

![Pressione o botão mode até que o led azul esteja na frente do número 2](/basic-usage/features/lightgun/dolphinbarmode2.png)

### 3. Sincronizar o Wiimote com a Mayflash dolphin bar

![Pressione o botão sync e um led azul piscará](/basic-usage/features/lightgun/dolphinbarsync.png)

![Então você tem que pressionar o botão sync no Wiimote](/basic-usage/features/lightgun/wiimotesync.png)

> Se você quiser jogar com 2 pessoas, você tem que ligar um Wiimote a cada Mayflash dolphin bar da mesma forma que foi explicado acima.
{.is-info}

## Como faço para ativá-lo?

Este modo lightgun é ativado automaticamente quando você tem uma Mayflash dolphin bar no modo 2 e somente para jogos que suportam este modo.

> Este modo lightgun pode ser ativado/desativado apenas ligando/desligando a Mayflash dolphin bar, mas, honestamente, não é necessário mudar a menos que você queira alternar o modo lightgun e o modo controle para o mesmo jogo.
{.is-info}

## E finalmente... como jogar?

### Comandos Wiimote padrão

Start: ![](/basic-usage/features/lightgun/button_plus.png) para iniciar um modo de jogo / pausa em alguns jogos

Select: ![](/basic-usage/features/lightgun/button_minus.png) para selecionar um modo de jogo / colocar créditos

Home: ![](/basic-usage/features/lightgun/button_home.png) para sair do jogo e retornar ao menu de seleção de jogos

Tiro principal : ![](/basic-usage/features/lightgun/button_z.png) no Nunchuk

Tiro secundário: ![](/basic-usage/features/lightgun/button_c.png) no Nunchuk 

### Notas sobre os botões Wiimote

>* Outros botões podem ser configurados para alguns sistemas na barra transversal, por exemplo (embora tentemos evitar esses casos)
>* A tecla ![](/basic-usage/features/lightgun/button_a.png) para créditos.
>* Também alguns jogos podem ser iniciados com ![](/basic-usage/features/lightgun/button_a.png) por exemplo.
>* Alguns jogos também exigirão que você pressione 2 teclas ao mesmo tempo para entrar em um jogo ou para sair/entrar em um menu.
>* As teclas direcionais esquerdas ![](/basic-usage/features/lightgun/stick.png) se comportarão como a cruz direcional do Wiimote.
>* O botão "power" ![](/basic-usage/features/lightgun/button_power.png) pode ser usado para desligar o Wiimote se pressionado por um longo período de tempo.
>* Os seguintes botões não funcionam no modo Lightgun: ![](/basic-usage/features/lightgun/button_1.png) e ![](/basic-usage/features/lightgun/button_2.png)
>* Se o Nunchuck estiver conectado, os botões ![](/basic-usage/features/lightgun/button_a.png) e ![](/basic-usage/features/lightgun/button_b.png) são invertidos no Wiimote.
{.is-info}

## O sistema virtual Lightgun

Você pode ativar o sistema virtual Lightgun para agregar todos esses jogos.  
Para ativá-lo, [veja aqui](./.../.../basic-usage/features/virtual-systems).

![APROVEITE!!!](/basic-usage/features/lightgun/duckhunt.png)
