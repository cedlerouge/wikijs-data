---
title: Retroachievements
description: 
published: true
date: 2021-08-04T00:09:10.000Z
tags: 
editor: markdown
dateCreated: 2021-08-04T00:09:10.000Z
---

## Desafios / Conquistas com o RetroArch

O site [retroachievements.org](http://www.retroachievements.org/) oferece desafios/realizações/troféus em plataformas como NES, SNES, GB, GBC, GBA, Megadrive/Genis, PCengine etc...

## Criar uma conta

* Você deve [criar uma conta](http://retroachievements.org/createaccount.php) no site [retroachievements.org](http://www.retroachievements.org/). 
* Anote seu nome de usuário(_Username_) e senha(_password_) em algum lugar, pois eles serão usados para configurar os Retroachievements.

## Ativação da conta no Recalbox

Isto é feito no menu EmulationStation.

* Abra o menu do EmulationStation com o botão `START`. 
* Vá para `CONFIGURAÇÕES DOS JOGOS` > `CONFIGURAÇÕES DO RETROACHIEVEMENTS`.
* Modifique dessa forma:

  * `RETROACHIEVEMENTS` : ON
  * `USUÁRIO`: coloque seu apelido
  * `SENHA`: coloque sua senha

Você também tem a opção do modo hardcore, você pode decidir se quer ativá-lo ou não.

>O Modo Hardcore desativa o rebobinar, créditos ilimitados, etc...
{.is-info}

## Emuladores compatíveis com Retroachievements.

Nem todos os núcleos Libretro são compatíveis com os Retroachievements. Se você quiser saber se um núcleo específico é suportado, você tem que ir para a página do emulador na seção [emuladores](./../../emulators).

## Visualizando os desafios de um jogo em andamento

Para ver os desafios/troféus no RetroArch, basta ativar o menu RetroArch (`Hotkey`+`B`) > `Successo`.

## Quais ROMs são compatíveis?

A lista de jogos compatíveis está disponível [nesta página](http://retroachievements.org/gameList.php)

Em geral, para consoles, as ROMs No-Intro na versão americana funcionam melhor para os Retroachievements, com algumas raras exceções onde você também pode obter algumass ROMs européias e para o arcade bastas você ter a versão certa do romset.