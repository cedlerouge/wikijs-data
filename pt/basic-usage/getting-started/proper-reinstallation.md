---
title: Reinstalação limpa
description: 
published: true
date: 2021-07-14T00:04:13.546Z
tags: 
editor: markdown
dateCreated: 2021-07-14T00:04:13.546Z
---

## O que devo fazer antes de uma reinstalação limpa do sistema?

### 1. Não tenho mídia removível, apenas o microsd

* Tenha sempre uma cópia de suas ROMs, *saves*, BIOS em seu PC ou em um disco de backup.
* Faça um backup da pasta Recalbox contendo as ROMs, BIOS, e *saves* em seu PC.
* Baixe e reinstale a [última versão do Recalbox](https://download.recalbox.com/) em seu microsd.

#### As BIOS

* Verifique a assinatura de sua bios no EmulationStation.
* Recalbox acrescenta novos emuladores que às vezes precisam de BIOS.

### 2. Eu tenho mídia removível (pen drive/ disco rígido usb externo)

* O formato exFAT é o mais recomendado para discos rígidos de grande capacidade.
* Conecte o disco rígido externo ou pen drive em seu PC, depois renomeie a pasta `recalbox` para `recalbox.old`.
* Baixe e instale [a última versão do Recalbox](https://download.recalbox.com/) em seu microSD.
* Conecte seu disco rígido externo ou pen drive ao seu Raspberry Pi / Odroid.
* Escolha sua mídia removível no armazenamento.
* A árvore de pastas do Recalbox será criada novamente em seu dispositivo externo.
* Desligue o Recalbox.
* Conecte seu disco rígido externo ou pen drive ao seu PC.
* Mova **SOMENTE** as pastas ROMS, BIOS e *saves*.
* **NÃO mova a gamelist/scrapes, tema personalizado e os OVERLAYS** (responsáveis por muitos bugs).
* Teste seu Recalbox com as configurações padrão.
* Se tudo estiver bem, você pode começar a customizar suas configurações, gradualmente.

## Reinstalação limpa do sistema

Siga o guia [Preparação e Instalação](./../preparation-and-installation)