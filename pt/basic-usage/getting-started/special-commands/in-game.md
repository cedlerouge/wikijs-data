---
title: Durante o jogo
description: Ao jogar, e se o emulador o permitir, é possível usar alguns comandos especiais para melhorar sua experiência de jogo.
published: true
date: 2021-07-16T00:20:36.721Z
tags: 
editor: markdown
dateCreated: 2021-07-16T00:15:55.880Z
---

> No Recalbox, os atalhos são, em sua maioria, uma combinação de teclas envolvendo uma tecla chamada **HOTKEY** (`HK`) que deve primeiro ser definida nas opções de configuração do controle.
>
> Os nomes usados ​​para os botões são os do controle do Super Nintendo (os botões `L1`,`R1`, `L2`,`R2`, `L3` e `R3` são baseados no controle do PlayStation).
>
> Veja [esta página](./../../../basic-usage/first-use-and-configuration#configurando-um-controle) para mais detalhes.
{.is-info}

![](/basic-usage/getting-started/special-commands/controllers.png)

## Comandos básicos

### Voltar para a lista de jogos [Back to Gamelist]

Sair do jogo atual e retornar à lista de jogos:

* `HK` + `START`

### Adicionar fichas/créditos [Insert Coin].

Adicionar crédito para jogos de arcade e jogos NeoGeo:

* `SELECT`

### Reiniciar o jogo [Reset]

Reiniciar o jogo atual:

* `HK` + `A`

### Rebobinar [Rewind]

Para rebobinar o jogo:

* `HK` + `ESQUERDA`

### AVANÇO RÁPIDO [Fast Forward]

Avançar rapidamente o jogo:

* `HK` + `DIREITA`

## Saves

### Salvar estado [Save State]

Crie um ponto de salvamento no slot atual:

* `HK` + `Y`

### Carregar estado [Load State]

Carregue o último ponto de salvamento do slot atual:

* `HK` + `X`

### Alterar o slot de save [Select Slot]

Selecione um slot para salvar entre os 10 slots disponíveis (por jogo, numerados de 0 a 9):

* `HK` + `CIMA` ou `BAIXO`

## Trocar CD

### Ejetar o CD [Disk Eject]

Permite simular a ejeção ou inserção de um CD no console (função destinada a jogos compostos por vários CDs ou disquetes):

* `HK` + `STICK L` para _CIMA_

### Trocar de CD [Disk Swap]

> Para poder trocar os discos, todas as imagens do seu jogo devem estar em um arquivo `.M3U`.
{.is-info}

Seleciona o disco anterior ou o próximo (função destinada a jogos que consistem em vários CDs ou disquetes):

* `HK` + `STICK L` para _ESQUERDA_ ou para _DIREITA_

> Para resumir simplesmente como trocar o CD em 3 etapas:
>
> 1. _ **Eu pressiono Ejetar e o leitor de CD abre:** _
> * `HK` + `STICK L` para _CIMA_
> 2. _ **Eu mudo CD (-1, -2, ... ou +1, +2, ...):** _
> * `HK` + `STICK L` para _ESQUERDA_ ou para _DIREITA_
> 3. _ **Eu pressiono Ejetar, o leitor de CD fecha e eu continuo jogando:** _
> * `HK` + `STICK L` para _CIMA_
{.is-sucesso}

## Exibição

### Captura de tela [Screenshot]

Faça uma captura de tela da tela atual (arquivo no formato .png salvo na pasta `screenshots`):

* `HK` + `L1`

### Gravação de vídeo [Recording]

Salve uma sequência de vídeo (arquivo salvo na pasta `screenshots`):

* `HK` + `R3` (pressione `Stick R`)

> Pressionar a 1ª vez inicia a gravação e pressionar uma 2ª vez para a gravação.
{.is-info}

### Alterar o shader [Shaders Swap]

Altere o filtro de vídeo (shader) exibido na tela:

* `HK` + `L2` ou `R2`

### Traduzir [Translate]

Traduza "na hora" e sobreponha o texto exibido na tela:

* `HK` + `STICK L` para _BAIXO_

> Requer uma conexão com a Internet para funcionar. Mais informações na página [Retroarch AI (tradução no jogo)](./../../../basic-usage/features/retroarch-ai-ingame-translation)
{.is-warning}

### Exibir FPS [FPS Display]

Exibir o número de quadros por segundo (FPS) renderizados atualmente:

* `HK` + `STICK R` para o _BAIXO_

## Usuário avançado

### Acessar o menu RetroArch [RetroArch Menu]

Acesse o menu RetroArch:

* `HK` + `B`

> Se você deseja modificar e salvar a configuração do RetroArch, lembre-se de ativar a opção "**Salvar configurações ao sair**" no menu RetroArch.
> Uma vez ativada esta opção, **todas as modificações** feitas neste menu serão salvas antes de sair do menu.
{.is-info}

## Case GPi da Retroflag

![](/basic-usage/preparation-and-installation/gpicase3-pt.png){.full-width}