---
title: Gerenciamento de arquivos
description: 
published: true
date: 2021-06-25T00:53:37.752Z
tags: 
editor: markdown
dateCreated: 2021-06-25T00:46:33.049Z
---

O **Recalbox** permite gerenciar seus arquivos através da pasta "SHARE".

Você tem duas possibilidades:

* Conexão direta
* Através da rede

## A pasta "SHARE"

>A pasta "SHARE" é a que permite gerenciar o conteúdo de seu Recalbox, ela está disponível **através da rede**.
{.is-info}

![](/basic-usage/share1.png)

Ela possui várias pastas:

![](/basic-usage/share2.png)

* **BIOS**

Coloque aqui as **BIOS** que o Recalbox precisa, você pode verificar a lista de BIOS necessárias pelo EmulationStation (`START` > `VERIFICAÇÃO DAS BIOS`).

>Alguns emuladores exigem que a BIOS esteja em uma subpasta ou que haja uma cópia extra na mesma pasta que as ROMs. Consulte a documentação de cada emulador.
{.is-warning}

* **bootvideos (Vídeos de Inicialização)**

Coloque aqui os vídeos que você quer ver quando ligar ou reiniciar seu Recalbox.

* **cheats (Trapaças)**

As Trapaças *(cheats)* podem ser baixadas através do menu RetroArch.

* **extractions (extrações)**

Pasta onde são extraídas suas ROMs compactadas no momento antes de iniciar o jogo.

* **kodi***

Coloque aqui a mídia que você deseja integrar ao Kodi nas subpastas correspondentes:

  * music - Músicas
  * movie - Filmes
  * picture - Imagens
   
* **music (Músicas)***

Coloque seus arquivos aqui para substituir a música de fundo padrão no EmulationStation.

>Os arquivos devem estar no formato `.MP3` ou `.OGG` e ter uma taxa de amostragem de 44100Hz e uma taxa de bits maior que 256kb/s.
{.is-info}

* **overlays (Sobreposições)***

Coloque suas sobreposições aqui para cobrir as bordas de sua tela quando um jogo estiver rodando.

* **roms**

Coloque suas ROMs aqui nas subpastas correspondentes ao console desejado.

* **saves (jogos salvos)**

Pasta onde seus *savegames* são armazenados.

* **screenshots (capturas de tela)**

Pasta onde são armazenadas as capturas de telas dos seus jogos.

* **shaders (sonmbreadores)**

Pasta onde são armazenados os *shaders* do seu sistema.

* **system (Sistema)**

Pasta relacionada às configurações e ao sistema Recalbox.

* **themes (Temas)**

Coloque aqui seus temas para a interface do EmulationStation do Recalbox.

* **userscripts (Scripts do usuário)**

Pasta onde seus scripts pessoais podem ser armazenados.

## Transferência de arquivos

Para adicionar seus arquivos (ROMs, savegames, BIOS, informações de scrape, etc...) ao seu dispositivo de armazenamento, você tem duas possibilidades:

### Conexão direta

* Conecte seu dispositivo de armazenamento ao seu computador.
* Abra o explorador de arquivos em seu sistema operacional.
* Vá para a guia que lista seus dispositivos.
* Selecione o nome de seu dispositivo de armazenamento, depois a pasta `recalbox` e finalmente a pasta `SHARE`.
* Copie seus arquivos para a pasta desejada.
* Uma vez concluída a transferência, basta conectar seu dispositivo novamente ao seu Recalbox, ligá-lo e jogar.

#### Através da rede

> Quando você configura seu wifi ou conecta um cabo de rede ao Recalbox, ele automaticamente compartilhará pastas na rede local.
{.is-info}

* Uma vez que sua máquina esteja conectada via cabo wifi ou Ethernet, abra o explorador de arquivos em seu sistema operacional.
* Vá para a aba Redes, depois `RECALBOX` e finalmente `SHARE`.
* Copie seus arquivos para a pasta desejada.

>Se você não consegue ver seu Recalbox na rede, tente digitar ``RECALBOX'' na barra de endereços do Explorador de arquivos. 

>Se isso ainda não funcionar, obtenha seu [endereço IP](./../tutorials/network/ip/discover-recalbox-ip). Em seguida, digite seu endereço IP na barra de endereços do explorador de arquivos. (Por exemplo: 192.168.1.30).

>Se isto ainda não funcionar e você estiver no Windows 10, você também pode seguir [este tutorial](./.../tutorial/network/share/access-network-share-with-windows-10).
{.is-info}

#### Para fazer aparecer o conteúdo adicionado:

* Atualizar a lista de jogos:

  * Abra o menu do EmulationStation apertando `START`.
  * `CONFIGURAÇÕES DA INTERFACE`.
  * `ATUALIZAR LISTAS DE JOGOS`.

* Reiniciar seu Recalbox:

  * Abra o menu do EmulationStation apertando `SELECT`.
  * Selecione `REINICIAR`.

## Adicionar conteúdo

### Adicionando jogos

Você só precisa copiar seus arquivos ROMs para a pasta do console correspondente.

>**Exemplo para Super Nintendo:**
>
>`/recalbox/share/roms/snes/`
{.is-success}

Você pode usar ROMs compactadas (.zip, .7z) ou ROMs descompactadas, dependendo do emulador.

>Para mais informações sobre os formatos das ROMs que são suportadas por emulador, consulte o arquivo `_readme.txt` em cada pasta ROMs.
{.is-info}

### Adicionando BIOS

Alguns emuladores requerem uma BIOS para emular corretamente os jogos.

* Se você quiser adicionar uma BIOS ao seu sistema, abra a pasta das BIOS via Samba ou vá diretamente para `/recalbox/share/bios/`.
* Os nomes das BIOS e suas assinaturas MD5 devem corresponder à lista na [VERIFICAÇÃO DAS BIOS](./.../basic-usage/features/bios-manager).

Para verificar a assinatura MD5 de uma BIOS, consulte a [seguinte](./../tutorials/utilities/rom-management/check-md5sum-of-rom-or-bios) página.

> Para o Neo-Geo, você precisa adicionar a BIOS `neogeo.zip` na pasta BIOS ou na pasta das ROMs do  Neo-Geo /recalbox/share/roms/neogeo ou /recalbox/share/roms/fbneo
> Para o Neo-Geo CD, você precisa adicionar as BIOS `neogeo.zip` e `neocdz.zip` na pasta /recalbox/share/bios/
{.is-warning}