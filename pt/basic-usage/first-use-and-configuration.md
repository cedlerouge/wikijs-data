---
title: Primeiro uso e configuração
description: Para configurar seu sistema Recalbox pela primeira vez
published: true
date: 2021-06-28T23:35:20.571Z
tags: 
editor: markdown
dateCreated: 2021-06-26T20:54:34.333Z
---

## Primeiros passos

#### Primeira inicialização / Primeiro *boot*

Após a instalação, a primeira coisa a fazer é conectar sua *case* com o Recalbox à sua TV através do cabo HDMI. Para alimentar e ligar seu Recalbox, basta conectar o cabo de alimentação micro USB ou USB-C (conforme o modelo do seu dispositivo).

>Muitos controles funcionam automaticamente quando você usa o Recalbox pela primeira vez. Entretanto, se você quiser configurar um controle USB que não foi reconhecido automaticamente, conecte um teclado USB... veja a parte [Configurando um controle](#configurando-um-controle).
{.is-info}

### Configurar o Recalbox para Português

Na tela de seleção de sistemas, pressione `START`. Escolha `SYSTEM SETTINGS` > `LANGUAGE`, selecione `PORTUGUES`, e saia do menu. O Recalbox lhe dirá que precisa reiniciar, confirme. Uma vez terminada a inicialização, os menus estarão completamente em português.

### Desligar o Recalbox

Na tela de seleção de sistemas, pressione `SELECT` e escolha `DESLIGAR SISTEMA`. Espere até que o LED verde/laranja do Pi pare de piscar para não danificar o cartão microSD e, uma vez que ele pare de piscar, desconecte o cabo de energia.

### Jogos padrão

O Recalbox vem com alguns jogos livres de *royalties* (*homebrews*) e/ou demonstrações para vários sistemas.

## Configurando um controle

#### Adicionar e configurar um controle USB

Você pode adicionar controles USB ao Recalbox. A maioria dos modelos são compatíveis (ver [lista de compatibilidade](./../hardware-compatibility/compatible-devices/controllers)).

Após conectar seu controle USB (ou emparelhar seu controle Bluetooth), pressione `START` com um controle previamente configurado. Selecione `CONFIGURAÇÕES DOS CONTROLES` e siga as instruções.

![](/basic-usage/first-use-and-configuration/padconfig1-pt.png)

Selecione `CONFIGURAR UM CONTROLE` e siga as instruções.

![](/basic-usage/first-use-and-configuration/padconfig2-pt.png)

Pressione uma tecla para iniciar a configuração.

![](/basic-usage/first-use-and-configuration/padconfig3-pt.png)

Mantenha pressionado qualquer botão no controle para configurá-lo.

![](/basic-usage/first-use-and-configuration/padconfig4-pt.png)

Agora você pode atribuir botões ao seu controle. Os nomes usados para atribuir os botões são os do controle do Super Nintendo:

![Super Nintendo (SNES) controller](/basic-usage/snespad.jpg)

>Os botões L1, R1, L2, R2, L3 e R3 são baseados no controle do Playstation.
{.is-info}

>**HOTKEY** (a _Tecla de atalho_)
> 
>O último botão a ser configurado, a **HOTKEY**, é o botão que será usado para ativar os [comandos especiais](./getting-started/special-commands) quando você estiver em um emulador:
> 
>Para os controles Xbox 360 e Xbox One, o botão **Hotkey** é atribuído ao botão `HOME`.
>Para os controles PS3 e PS4, o botão **Hotkey** é atribuído ao botão `PS`.
> 
>Se não houver um botão dedicado ou se ainda não tiver sido atribuída, recomenda-se que a **Hotkey** seja atribuída ao botão `Select'.
{.is-warning}

>Para pular qualquer botão (exceto a **HOTKEY**), pressione a tecla `BAIXO` para passar para o próximo botão.
{.is-info}

>Para controles de 6 botões (SNES, arcade, ...), os botões são atribuídos em correspondência com o controle SNES (ver acima).
>Para controles de 2 botões (NES, PC Engine, Gameboy, ...), os botões atribuídos são `A` e `B`.
{.is-warning}

![](/basic-usage/first-use-and-configuration/padconfig5-pt.png)

![](/basic-usage/first-use-and-configuration/padconfig6-pt.png)

### Atribuindo um controle

* De volta à tela de configuração, você pode atribuir o controle a um jogador.
* Selecione `CONTROLE P1` para definir o controle do *Player* 1.

![](/basic-usage/first-use-and-configuration/padassign1-pt.png)

* Selecione um controle.

![](/basic-usage/first-use-and-configuration/padassign2-pt.png)

* Seu controle já está configurado.

![](/basic-usage/first-use-and-configuration/padassign3-pt.png)

### Usando um teclado

Se você não puder configurar seu controle, você pode conectar um teclado USB com fio ao seu Recalbox para configurá-lo. Você pode encontrar todas as combinações de teclas para o teclado [aqui](./../tutorials/others/emulationstation-use-with-keyboard-and-mouse).

### Controle PS3

Para **associar** um controle PS3:

* Primeiro conecte o controle à porta USB (através de um cabo adequado) e aguarde 10 segundos.
* Depois disso, você pode desconectar o controle e pressionar o botão **PS** para iniciar a conexão sem fio.

>**Nota:**  
> Para cópias asiáticas do PS3 Dualshock 3 (como GASIA ou SHANWAN), você precisará ajustar algumas configurações.
{.is-warning}

>**Aviso:**  
>Em caso de dúvida sobre o fornecimento e consumo de energia elétrica, é melhor evitar carregar o controle PS3 no Raspberry Pi, pois ele pode causar problemas de estabilidade.
> 
>Por favor, conecte o controlador ao Raspberry Pi apenas para associar seu controlador à sua Recalbox.
{.is-danger}

Se você entender as configurações envolvidas ou quiser utilizar seu controle com uma conexão USB, você precisa desativar o driver bluetooth PS3 no arquivo recalbox.conf, encontrando e modificando a linha `controllers.ps3.enabled=0`.

Lembre-se de que a configuração do controle no Recalbox é baseada na configuração do botão SNES:

| Controle PS3 | Controle SNES |
| :---: | :---: |
| X | B |
| ◯ | A |
| ⬜ | Y |
| △ | X |

>**Informação:**  
>Por padrão, **HOTKEY** está atribuída ao botão **PS** (o que está no meio do joystick).  
>Para mais informações sobre HOTKEY, consulte a página [comandos especiais](./getting-started/special-commands).
{.is-info}

### Controle Xbox 360 

>**Nota:**  
Os controles sem fio Xbox 360 exigem um receptor sem fio especial do tipo *dongle*.
{.is-warning}

Lembre-se de que a configuração do controle no Recalbox é baseada na configuração do controle SNES:

| Controle Xbox 360 | Controle SNES |
| :---: | :---: |
| A | B |
| B | A |
| X | Y |
| Y | X |

>**Informação**:
>Por padrão, **HOTKEY** está atribuída ao botão **HOME** (o que está no meio do joystick).  
>Para mais informações sobre **HOTKEY**, consulte a página [comandos especiais](./getting-started/special-commands).
{.is-info}

### Adicionando um controle Bluetooth

Para adicionar um controle Bluetooth:

* Coloque seu controle no modo de _pareamento_.
* Em seguida, pressione `START` e escolha `CONFIGURAÇÕES DOS CONTROLES`.
* Em seguida, `PAREAR CONTROLE BLUETOOTH`.

Aparecerá uma **lista de aparelhos detectados**!

* Basta selecionar o seu e seu controle e ele será emparelhado.
* Você pode agora continuar a configurar (se o seu controle ainda não for reconhecido automaticamente pelo Recalbox).

>**Informação:**  
>Para controles **8bitdo**, consulte a página [**8bitdo no Recalbox**](./../hardware-compatibility/compatible-devices/8bitdo-on-recalbox).
{.is-info}


### Controle GPIO

Você pode conectar seus botões e joysticks diretamente aos GPIOs do Raspberry Pi (veja a página [**Os controles GPIO**](./.../tutorials/controllers/gpio/gpio-controllers)). Você também pode conectar controles originais de PSX, NES, SNES, Megradrive, etc... (ver **Controles DB9** e **Controles Gamecon**).

### Controles virtuais

Com o projeto Miroof Virtual Gamepads, você pode adicionar até 4 controles com seus smartphones/tablets!

Para fazer isso, inicie o navegador web de seu smartphone e digite o endereço IP do seu Recalbox seguido da porta de comunicação (porta `:8080`). 

>**Informação:**  
>Você pode encontrar o endereço IP do seu Recalbox no menu `CONFIGURAÇÕES DE REDE` 
{.is-info}

![Zonas de Toque do Controlador Virtual](/basic-usage/virutalgamepad_touch_zones.png)

## Usando armazenamento externo

>**Informação:**  
>Você pode facilmente **utilizar um dispositivo de armazenamento USB** (*pen drive*, HD externo com alimentação USB, etc.) para armazenar suas ROMs, arquivos pessoais, etc...
> 
>**Com este método**, o **sistema** (no cartão SD) e a **partição SHARE** (no dispositivo externo) **serão separados**.
>  
>Portanto **se você precisar reinstalar o sistema**, você **irá manter todos os seus dados pessoais**.
>Tudo que você terá que fazer então é **conectar seu dispositivo** ao seu Recalbox, depois **selecioná-lo no sistema, e jogar.**
{.is-info}

### O formato de sua mídia

Primeiro, você deve utilizar **um dispositivo que utilize um dos seguintes sistemas de arquivo**: **FAT32, EXFAT, EXT4 ou NTFS**.

>**Nota:**
> 
>É altamente recomendável utilizar o sistema de arquivo **EXFAT**.
{.is-info}

>**Atenção:**
>
> As taxas de transferência podem ser **lentas** caso você use **NTFS.**  
> Certifique-se também de que o **sistema de arquivo** que você escolher **é compatível com o sistema operacional do seu PC.**
{.is-danger}

| Formatos | Tamanho máximo de volume | Tamanho máximo de arquivo | Número máximo de arquivos | Modo de leitura/escrita |
| :---: | :---: | :---: | :---: | :---:|
| Fat | 2TB | 4GB (4,2949GB) | Mais de **250 milhões*** | Windows: &#x2705; Linux: &#x2705; |
| NTFS | 256TB (2.815.8492TB) | 16TB (17.5921TB) | 4.294.967.295 | Windows: &#x2705; Linux: &#x2705; |
| exFAT | 128PB (144.115.1880TB) | 128PB (144.115.1880TB) | 2.796.202 por pasta | Windows: &#x2705; Linux: &#x2705; |
| ext4 | 1 EB (1.152.921.5046TB - limitado a 16TB pelo e2fsprogs) | 16TB (17.5921TB) | 4 bilhões | Windows: &#x274C; Leitura/gravação via ext2fsd Linux: &#x2705; |

>**Atenção**
>
> Recalbox **não formatará seu dispositivo**, ele apenas **criará novos arquivos nele.**
{.is-danger}

### Configuração

Para configurar o Recalbox para **usar um dispositivo de armazenamento USB**

* Conecte seu dispositivo ao seu Recalbox, depois ligue-o.
* Na interface do Recalbox, pressione o botão `START` em seu controle, vá para `CONFIGURAÇÕES DE SISTEMA` e depois para `ARMAZENAMENTO`.
* Agora selecione seu dispositivo na lista, confirme e aguarde que o sistema seja reinicializado.

>**Informação:**
>
>**Durante esta fase de reinicialização**, o Recalbox irá **criar na raiz** de seu dispositivo **uma nova pasta** chamada **"recalbox"** que conterá toda a árvore de sua **partição "SHARE"**.
{.is-info}

### Resolução de problemas

#### Disco rígido invisível

O que fazer se depois de reiniciar, o Recalbox ainda não encontrar o disco rígido?

>**Informação:** 
>
>Por vezes, após selecionar o dispositivo em EmulationStation e reiniciar, o Recalbox não consegue criar o sistema de arquivo nele. Ele então continuará a utilizar os arquivos no cartão SD. Isto acontece especialmente com alguns discos rígidos que são lentos para inicializar.
{.is-info}

O que você pode fazer:

* Conecte via [SSH](./.../tutorials/system/access/root-access-terminal-cli)
* Monte a [partição boot](./.../tutorials/system/access/remount-partition-with-write-access) como leitura/gravação.
* Digite os seguintes comandos: `cd /boot` e `nano recalbox-boot.conf`.
* Adicione esta linha `sharewait=30`. Este valor é em segundos.
* Salvar com `Ctrl+X`,`Y`,`Enter`. 
* Digite `reboot` e confirme para reiniciar o Recalbox.

## Rede

### Configurar através do cabo RJ45

* Conecte seu cabo RJ45 ao seu Recalbox e pronto, seu Recalbox está configurado.

### Configurar Wifi

* Na interface geral, pressione `START` > `CONFIGURAÇÕES DE REDE`. Para ativa, altere a opção `ATIVAR WIFI` para **ON**.
* Você agora precisa configurar a wifi via WPS. No seu modem deve ter um botão físico (ou na interface virtual do seu modem) para ativar a função WPS. Uma vez pressionado, ela permanece ativa por 2 minutos.
* Ainda no Recalbox, selecione `CONEXÃO WPS` na parte inferior do menu. Isto tentará encontrar todas as conexões WPS existentes ao seu redor.
* Quando sua conexão for encontrada, ela será automaticamente salva em seu Recalbox.