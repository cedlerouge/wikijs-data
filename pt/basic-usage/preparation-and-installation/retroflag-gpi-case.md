---
title: GPi Case da RetroFlag
description: 
published: true
date: 2021-06-29T23:57:49.734Z
tags: 
editor: markdown
dateCreated: 2021-06-29T00:03:42.553Z
---

## Preparação

### Elementos necessários

* 1 x *Case* RetroFlag GPi
* 1 x Raspberry Pi Zero W
* 1 x Cartão SD
* 1 x adaptador de cartão SD
* 1 x dissipador de calor (para sua placa Raspberry Pi Zero W)
* 3 X Pilhas

### Baterias recomendadas

* [Pilhas Duracell Recarregáveis tipo AA 2500 mAh](https://www.amazon.com.br/Duracell-Recarreg%C3%A1vel-Longa-Vida-Pacote/dp/B00007ISWA/ref=pd_sbs_2/131-5191575-9602538?pd_rd_w=7pyp5&pf_rd_p=a07f0ae0-2cd5-4527-9b2f-68214df6fcff&pf_rd_r=SV1C5C3S4FZR3DQXKMJ4&pd_rd_r=a548aeb1-1285-405c-8982-5cf01e5f63e3&pd_rd_wg=j1yjM&pd_rd_i=B00007ISWA&psc=1) 
* [Pilhas Panasonic Eneloop Pro Recarregáveis tipo AA 2550mAh](https://www.amazon.com.br/Pilhas-Recarreg-C3-A1veis-Eneloop-Pro-2550-dp-B00JHKSL28/dp/B00JHKSL28)

Com uma bateria externa de 10 000maH, fica perfeito!

## Apresentação da *case* Gpi

![](/basic-usage/preparation-and-installation/gpicase1.png){.full-width}

![](/basic-usage/preparation-and-installation/gpicase2.png){.full-width}

![](/basic-usage/preparation-and-installation/gpicase3-pt.png){.full-width}

## Gravar e iniciar!

> _**Não instale a *case* GPi com as pilhas!**_   
> Sempre utilize o cabo USB para a instalação e para os primeiros testes.
{.is-warning}

![*Case* GPi](/basic-usage/preparation-and-installation/gpicase4.png)

Basta clicar e começar, é simples assim! O Recalbox detecta e instala tudo o que é necessário automaticamente.

Para fazer sua *case* GPi funcionar :

* Basta gravar a última versão do Recalbox para Raspberry Pi1/Pi0 em seu cartão SD, inseri-lo no cartucho de seu GPI, e ligá-lo!
* Espere alguns segundos, então você verá esta imagem em sua tela:

![Imagem de instalação](/basic-usage/preparation-and-installation/gpicase5.png)

* Espere mais alguns segundos (pode levar mais tempo com cartões SD de alta capacidade) antes que a EmulationStation inicie com sua tela habitual:

![EmulationStation iniciando...](/basic-usage/preparation-and-installation/gpicase6.png)

* Uma vez feito, desfrute de um tema otimizado que torna sua experiência rica, fluida e divertida!

## O que é diferente de um Recalbox normal?

Quando o Recalbox detecta e instala os elementos da *case* GPi, ele também muda algumas configurações para permitir que você vá rapidamente jogar.  
  
Naturalmente, todas as configurações estão disponíveis através do menu `START` e você pode reconfigurar o que quiser depois.

#### Aqui está uma lista das configurações padrões:

* Todos os modos de vídeo (videomode) são definidos como `default` para evitar mudanças desnecessárias na resolução HDMI
* O nome do host do GPI está definido para **RECALBOXGPI**
* Kodi e o botão X estão desativados.
* O controle virtual está desativado
* As atualizações estão desativadas
* Netplay está desativado
* Bluetooth está desativado
* Os drivers XArcade & PS3 estão desativados
* O popup de música está desativado
* A configuração do XBOX360 foi substituída pela configuração da *case* GPI (sim, o controle GPI também é um controle XBOX360!)
* Nosso tema otimizado `recalbox-gpicase` já foi copiado para `/recalbox/share/themes` e ativado

### E também instalado:

* dpi-pizero-gpicase.dtbo: Driver de tela GPI 
* pwm-audio-pizero-gpicase.dtbo : driver de som do GPI
* Um Script de desligamento especial do GPI, feito originalmente pela Retroflag e fortemente modificado para fazer o GPI desligar rapidamente.

## Instalando jogos

![](/basic-usage/preparation-and-installation/gpicase7.png)

Agora que você está conectado à internet, você pode instalar seus jogos como de costume, utilizando o compartilhamento SAMBA (`\\recalboxgpi\share` em um explorador de arquivos) ou através do WebManager (`http://recalboxgpi` em um navegador de internet)

Divirta-se com seus jogos favoritos!

## O que você deve saber...

### Que tipo de jogos posso rodar no GPI?

> Você pode executar qualquer jogo desde consoles de 8/16 bits, computadores, e até Arcade.  
>_**Sim, você pode, sério!**_
{.is-info}

Para o arcade, você tem **PiFBA**, uma versão ARM-otimizada do FBA que requer um romset 0.2.96.37.  
**FBNeo** (antigo FBAlpha), **MAME2000** até **MAME2003plus** também funcionam muito bem e muitos jogos podem ser jogados neles.

Alguns jogos **GBA/SNES**, assim como alguns jogos Arcade, podem sofrer atraso ou a lentidão. Neste caso, você pode tentar desativar a opção `REBOBINAR` nas opções avançadas do emulador. Entretanto, mesmo com opções otimizadas, alguns jogos não funcionarão em velocidade máxima.
 
> Não é recomendado instalar jogos de computador, muitas vezes eles precisam de um teclado e/ou mouse para iniciar.
{.is-info}

#### Posso fazer *overclock* no meu Pi0?

Infelizmente, o Raspberry Pi0 não é fácil de fazer *overclock*. Além disso, a *case* GPi não contém nenhum orifício de ventilação para deixar sair o ar quente (pelo menos na *case* GPi v1 usada no momento em que esta página foi escrita).

_**Você ainda pode brincar com as opções de *Overclock* por sua própria conta e risco!**_

>Se seu GPI não estiver estável após selecionar uma opção de *Overclock* ao ponto de não poder alterar as opções, insira o cartão SD em seu PC e remova as linhas de *O/C* do arquivo `recalbox-oc-config.txt`. Este arquivo está localizado na partição `RECALBOX`.
{.is-danger}

Você pode adicionar um pequeno dissipador de cobre, como mostrado na figura abaixo. Sua espessura não deve exceder 2mm.

![](/basic-usage/preparation-and-installation/gpicase8.jpg)

### Quanto tempo dura a bateria?

Os primeiros testes mostram que você terá cerca de 2h de jogo com uma carga.

Não esqueça que seu *case* GPi tem uma entrada USB. No carro ou em casa, use o cabo USB.

>Se a parte de trás do cartucho começar a ficar muito quente, recomendamos que você desligue a *case* GPi e espere por um tempo até esfriar.
{.is-info}

### Como eu posso fazer *scrape* dos meus jogos?

* ***Scraper* interno:** Você sempre pode usar o *scraper* interno, mas, ele é configurado para fazer *scrape* de arquivos grandes para exibição em um Recalbox normal. A sua utilização por WIFI é lenta e ineficiente. 
* ***Scraper* externo:** **Recomendamos fortemente o uso de um *scraper* externo**. Será melhor e mais rápido.

> Com o ***Scraper* externo**, você também pode ajustar o tamanho máximo da imagem para 170x170 para economizar espaço e aumentar a velocidade do EmulationStation.
{.is-info}

## Resolução de problemas

* Se a tela ficar preta ou "pular" ao encostar no cartucho ou ao colocar o console para baixo
  * Verifique todas as conexões.
  * Levante o RPi no cartucho, para aproximá-lo do circuito impresso: trabalhe com os 4 pinos que suportam o RPi 0, desatarraxando-os levemente até que o RPi 0 esteja perfeitamente paralelo com o cartucho. Dependendo do caso, desatarraxar os 2 pinos dourados superiores pode ser suficiente.

> Este erro ocorre quando há um problema com a usinagem do cartucho, a conexão entre o RPi 0 e o cartucho não é feita corretamente.
{.is-info}

* **Resolver questões de estabilidade:**

  * **1. O Micro-SD**

  Verifique seu microSD.  
  Alguns modelos têm problemas com o RPi, sem mencionar os cartões micro SD chineses de má qualidade.  
  Teste com outro SD, de preferência de outra marca.

  * **2. As pilhas**

  Não instale a *case* GPi com as pilhas!   
  Use sempre o cabo USB para instalação e testes iniciais.  
  Se funcionar corretamente com o cabo e não com as pilhas, ou elas são descarregadas ou não fornecem energia o suficiente, apesar do regulador do GPi.  
  A maioria das pilhas recarregáveis são 1,2V, não 1,5V.  

  * **3. O modelo de RPi0.**

  Parece que alguns modelos têm problemas de estabilidade, pelo menos com os *clocks* originais, configurados com o firmware Pi.  
  Abra o arquivo `recalbox-oc-config.txt` na partição `RECALBOX`, e adicione as seguintes linhas ao final do arquivo:

  ```ini
  arm_freq=1000
  gpu_freq=500
  core_freq=500
  sdram_freq=500
  sdram_schmoo=0x02000020
  over_voltage=6
  sdram_over_voltage=2
  ```

  Isto forçará os *clocks* a serem configurados com um leve *overclock* da GPU como um bônus.  

  * **4. Montagem**

  Embora a montagem do RPi seja relativamente fácil, pode ser que sua montagem resulte em contatos imperfeitos. Remova o cartucho, limpe os contatos da co Rpi e da GPi com uma solução de limpeza à base de álcool, e monte novamente o RPi.
  Também limpe os contatos do cartucho na *case* GPi.
  Certifique-se de que o cartucho esteja devidamente encaixado. O botão de ligar não deve ser forçado, nem ao ligar, nem ao desligar. Se isso acontecer, significa que seu cartucho não está devidamente inserido ou que algo está bloqueando ele.

  * **5. E por fim...**

  Se apesar de tudo, seu RPi não funcionar, ou se permanecer instável, tente com outro para descobrir qual o problema: o RPi ou a *case* GPi. Pegue um emprestado ou, em últimos casos, pelo preço, compre outro.
   Se a *case* GPi estiver em falta, você pode tentar uma troca ou reembolso. Em caso de problemas com o vendedor, diga a ele todo o procedimento que você seguiu para provar sua boa fé em relação à incriminação da *case* GPi.

* Se você estiver em uma das seguintes situações:

  * Quando você liga, não há nada na tela, mesmo depois de um tempo de espera.
  * Você não pode configurar o WIFI. Entre em contato conosco no servidor Discord ou no Fórum e nos forneça os seguintes arquivos, disponíveis na partição `RECALBOX` de seu cartão SD:

    * `config.txt`
    * `recalbox-backup.conf`
    * `hardware.log`

>Para outros problemas, entre em contato:   
>[Fórum](https://forum.recalbox.com/) ou [*issues* do Gitlab](https://gitlab.com/recalbox/recalbox/issues)
{.is-info}

