---
title: Preparação e instalação
description: Como instalar o Recalbox
published: true
date: 2021-06-30T23:48:16.646Z
tags: 
editor: markdown
dateCreated: 2021-06-30T23:47:42.358Z
---

## I - Placas compatíveis

É possível instalar o Recalbox em diferentes tipos de dispositivos:

- Raspberry Pi 4
- Raspberry Pi 3
- Raspberry Pi 2
- Raspberry Pi 1/Zero
- Odroid Go Advance (console portátil)
- Odroid Go Super (console portátil)
- Computador de 32 ou 64 bits
- Odroid XU4

Para descobrir o Recalbox, o **Raspberry Pi 4** é definitivamente a escolha da equipe!

## II - Instalação do Recalbox

### 1 - Baixar/Gravar a imagem

Vá para [download.recalbox.com](https://download.recalbox.com/) e siga as instruções para baixar e gravar a imagem do Recalbox em sua mídia de armazenamento (cartão SD, cartão eMMC ou disco rígido).

>Apenas a **versão mais recente** do Recalbox está disponível.
>**Não é possível baixar versões antigas** e a equipe de desenvolvimento **não dá suporte para elas.**
{.is-warning}

>Você deve usar mídias de 8GB para o sistema (recomendamos **Sandisk Ultra series** para cartões SD).
>
>* Para a **instalação no Raspberry Pi**:
>	* Como mídia de armazenamento: um **cartão SD**.
>* Para a **instalação no Odroid**:
>	* Como mídia de armazenamento: um **cartão SD ou eMMC**. 
>* Para **instalação em x86 e x86_64**:
>	* Como mídia de armazenamento: um **disco rígido**.

#### 2 - Instalação

#### Raspberry Pi / Odroid Go Advance / Odroid Go Super

* Insira o cartão microSD no dispositivo com o qual você deseja usar o Recalbox.
* Ligue / conecte sua placa e a instalação do Recalbox começará automaticamente.
* A instalação leva alguns minutos e varia dependendo de seu hardware.

#### GPi Case da RetroFlag

A instalação em uma *case* Gpi é igualmente fácil: insira o cartão micro SD, conecte sua *case* Gpi  à fonte de alimentação e o Recalbox detectará e configurará automaticamente a *case*. Para mais informações, vá para a página [GPi Case da RetroFlag](./preparation-and-installation/retroflag-gpi-case)

#### Odroid XU4 / Odroid XU4Q

* Insira seu cartão microSD ou eMMC no dispositivo com o qual você deseja usar o Recalbox.
* Inicie-o e você deve ver o Recalbox ganhar vida.

#### x86/x86_64

A instalação em um dispositivo USB (indiferente do hardware) é recomendada apenas para fins de teste. Mas como não é permanente, pode causar o aparecimento de bugs que não estarão presentes ao instalar em um disco rígido.

Assim, nós recomendamos **a instalação em disco rígido:**

* Um para o **Sistema**.
* Um para o **Armazenamento** das ROMs, BIOS, saves, etc...

* Inicialize-o e você deve ver o Recalbox ganhar vida.

## III - Hardware e acessórios necessários

Verifique se você tem o dispositivo de armazenamento e a fonte de alimentação necessários para o dispositivo escolhido.

| Categoria | Raspberry Pi 0/0w/1/2/3 | Raspberry Pi 4 | Odroid XU4 | Odroid Go | PC |
| :---: | :---: | :---: | :---: | :---: | :---: |
| Fonte de alimentação | µUSB 2.5A (boa qualidade) | USB-C 3A 5V | Usar fonte de alimentação oficial Odroid | Bateria interna | Fonte de alimentação padrão do PC |
| Vídeo | Cabo HDMIc, HDMI2VGA ou HDMI2DVI | Cabo µHDMI-HDMI | Cabo HDMI | Interno | HDMI/VGA  (DVI e DP provavelmente)
| Controles | USB ou Bluetooth | USB ou Bluetooth | USB ou Bluetooth | USB ou Bluetooth | USB ou Bluetooth
| Armazenamento do sistema | Cartão µSD 8GB+ classe 10 , exceto Pi 1 : SD | Cartão µSD 8GB+ | Cartão µSD 8GB+ | Cartão µSD 8GB+ | Disco Rígido 8GB+ |
| Armazenamento das configurações, BIOS, ROMs | Disco rígido externo com fonte de alimentação própria | Disco rígido externo com fonte de alimentação própria | Disco rígido externo com fonte de alimentação própria | Cartão µSD | Disco rígido interno ou Disco rígido USB externo (Plugado na placa-mãe) | 
>Você precisa dos seguintes itens para criar seu Recalbox:
>Venha visitar [nossa loja](https://www.kubii.fr/221-recalbox) na Kubii (França)!
{.is-info}

## Solução de problemas x86/x86_64:

* **Seu computador só inicia no Windows:** Verifique se ***Secure Boot*** (inicialização segura) está desativado.

  * Descubra na Internet onde a opção Secure boot pode ser encontrada na sua BIOS (difere dependendo do fabricante e do modelo do computador).
  * Em seguida, atribuia `OFF` ou `DISABLE` para o  ***Secure Boot***.
  * Desligue seu computador.

* **Recalbox não inicializa:** Defina sua **BIOS** para ***Legacy***.
  * Para definir sua BIOS para *Legacy*, no momento da inicialização, pressione a tecla de acesso à BIOS `F*` (teste de `F1` a `F12`, pois diferente dependendo do fabricante) ou `Del`.
  * Descubra na Internet onde se encontra a opção ***Legacy*** na sua BIOS (difere dependendo do fabricante e do modelo do computador).
  * Em seguida, atribuia `ON` ou `ENABLE` para o  ***LEGACY***.
* **No caso de um PC dedicado a *Multiboot*** , se você quiser que seu **Recalbox inicie** antes de qualquer outro sistema:
  * Vá para o **Menu de Boot** dos discos rígidos.
  * Pressione `F*` para acessar o disco rígido **Menu de Boot** (teste de `F1` a `F12`, diferente dependendo do fabricante).
  * Selecione seu **disco rígido com o sistema** Recalbox.