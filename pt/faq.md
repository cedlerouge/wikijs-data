---
title: 💡 FAQ
description: 
published: true
date: 2021-06-24T23:27:33.359Z
tags: 
editor: markdown
dateCreated: 2021-06-24T23:24:07.417Z
---

## Geral

### Em que posso instalar o Recalbox?

Você pode instalar o Recalbox nos seguintes dispositivos:

- Raspberry Pi 4
- Raspberry Pi 400
- Raspberry Pi Zero
- Raspberry Pi 1 (B/B+)
- Raspberry Pi 2
- Raspberry Pi 3 (B/B+)
- Odroid XU4
- Odroid Go Advance (Console Portátil)
- Odroid Go Super (Console Portátil)
- PC x86 (32bits)
- PC x86_64 (64bits)

### Como posso participar?

- Você pode fazer uma doação em nosso [Paypal](https://www.paypal.com/donate?token=NwGkmyeo6i13jHSM_tLaa6sH9W12_pJ6xHYTu8PDASs9ZI5SoUuJMBVdDBxEZPhieLS2aO5alMyoLmm2)
- Você pode encomendar seu hardware no kubii.fr/221-recalbox
- Você pode relatar bugs ou idéias de melhorias em nosso [Gitlab](https://gitlab.com/recalbox/recalbox/issues)
- Você pode participar no [fórum do Recalbox](http://forum.recalbox.com)
- Você pode [contribuir para esta Wiki](./contribute)

### Onde posso ver o progresso do desenvolvimento do Recalbox?

No [gitlab do Recalbox](https://gitlab.com/recalbox). Você pode acessar :  
- Os [*issues*](https://gitlab.com/recalbox/recalbox/issues) que são as tarefas atuais.  
- Os [*milestones*](https://gitlab.com/recalbox/recalbox/-/milestones) onde você pode ver quais tarefas ainda precisam ser desenvolvidas e quais estão concluídas.

### O Recalbox vêm com quantos jogos?

Estamos tentando adicionar o maior número possível de jogos **gratuitos** na distribuição, para que você possa desfrutar da experiência Recalbox desde o primeiro lançamento!
Vá para [esta página](https://forum.recalbox.com/topic/52/homebrews-sur-la-recalbox) para enviar seus *homebrews*!

### Posso usar o Recalbox em um bartop/fliperama caseiro?

Sim, o sistema Recalbox também foi projetado para ser integrado em bartops e fliperamas caseiros.
- Você pode conectar os botões e joysticks diretamente aos GPIOs do Raspberry.
- Você pode desativar os menus para evitar qualquer uso indevido.
- Também é possível utilizar as saídas HDMI / DVI /VGA (com adaptador) ou RCA.
Você pode encontrar mais informações [na página dedicada](./tutorials/system/installation/bartop-arcade-configuration).

### O Recalbox suporta muitos sistemas, mas eu vejo apenas alguns na interface, o que posso fazer?

Somente os sistemas com jogos disponíveis são exibidos. Tente adicionar suas ROMs através da rede e reinicie o Recalbox para ver os novos sistemas aparecerem. Além disso, alguns dispositivos não suportam **todos** os consoles.

#### Devo fazer overclock no meu Raspberry Pi?

No Raspberry Pi 1 (incluindo zero), é fortemente aconselhado fazer overclock do Raspberry no modo EXTREMO.

## EmulationStation

### Como mudar o visual do tema EmulationStation?

Pressione `Start` no controle → `CONFIGURAÇÕES DA INTERFACE` → `TEMA`.

### Como silenciar a música de fundo?

Pressione `Start` no controle → `CONFIGURAÇÕES DE ÁUDIO` → Configure para OFF.

### Onde estão localizados os arquivos de configuração do EmulationStation no sistema de arquivos?

```
/recalbox/share/system/recalbox.conf
```

### Onde estão localizados os temas do EmulationStation?

```
/recalbox/share/themes
```

## Emulação

### Porque existem múltiplos emuladores para cada sistema?

A emulação não é uma operação sem falhas, há um equilíbrio entre desempenho, funcionalidade e fidelidade.
Como a emulação não é perfeita, é melhor ter várias opções para manuseá-la, às vezes um emulador mais rápido será melhor, mas pode ter problemas inesperados, enquanto um mais lento pode ter maior precisão, mas não suportar algumas características específicas.

### Como posso mudar o emulador padrão para um determinado sistema?

Pressionando `START` no controle, depois indo para as Configurações Avançadas, e finalmente para a Configuração Avançada do Emulador. Você pode selecionar o sistema desejado, pode mudar as configurações do emulador e do núcleo (note que a configuração base não mudará se a configuração do emulador disser "Padrão").

## Sistema

### O Recalbox suporta cerca de 100 consoles, mas só vejo alguns, tanto na interface do EmulationStation quanto na pasta SHARE?

Dependendo da placa utilizada, você terá emuladores compatíveis com ela.  
Por favor, veja a lista de compatibilidade para cada placa [aqui](./hardware-compatibility/).

### Por que em alguns sistemas como o GBA, meus jogos mostram uma tela preta e depois retornam para o EmulationStation?

> Alguns sistemas requerem BIOS para funcionar.

## Resolução de problemas gerais

### Meu Recalbox acabou de "cair", devo desligá-lo e ligá-lo novamente?

Não, na maioria dos casos o sistema não falhou completamente, apenas o EmulationStation falhou.

Há várias maneiras de lidar com esta situação, a mais fácil é usar os recursos de depuração do Webmanager para reiniciar o EmulationStation ou para desligar corretamente o Recalbox.

Não fazer isso pode corromper seus dados, especialmente se você estiver usando um Raspberry Pi com um cartão SD.