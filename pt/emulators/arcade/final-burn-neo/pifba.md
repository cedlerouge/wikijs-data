---
title: PiFBA
description: 
published: true
date: 2021-09-23T23:48:49.194Z
tags: finalburn, neo, pifba
editor: markdown
dateCreated: 2021-09-23T23:48:49.194Z
---

**PiFBA** é o Final Burn Alpha 2x para o Raspberry Pi portado por Squid.  
Emula antigos jogos arcade usando roms baseados em MAME para CPS1, CPS2, Neogeo, Toaplan e muitos outros jogos.

Deve suportar as versões mais recentes das ROMs MAME, mas infelizmente não há uma versão definitiva para usar. Uma lista completa dos jogos suportados pode ser encontrada na interface do programa.

Alguns jogos não funcionarão corretamente, alguns são tão grandes que parecem ficar sem memória.

>Apenas use este emulador se você tiver um Pi0/1 ou quiser melhorar o desempenho de um jogo em particular que é muito lento no FBNeo.
>piFBA é o emulador FBA mais otimizado no Recalbox para o Pi0/1, mas é compatível com muito menos jogos do que o FBNeo.
{.is-warning}

## ![](/emulators/license.svg) Licença

Este núcleo é licenciado sob **GPLv2**.

## ![](/emulators/compatibility.png) Compatibilidade

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ❌ | ❌ | ❌ | ❌ | ❌ |

## ![](/emulators/features.png) Recursos

| Recurso | Suportado |
| :---: | :---: |
| Saves | ✔ |
| Netplay | ✔ |
| RetroAchievements | ✔ |

## ![](/emulators/bios.svg) BIOS

### Lista das BIOS

Dependendo do jogo, algumas bios serão necessárias e devem ser colocadas na mesma pasta que os jogos.

#### Local

Coloque as BIOS dessa forma:

┣ 📁 recalbox  
┃ ┣ 📁 share  
┃ ┃ ┣ 📁 roms  
┃ ┃ ┃ ┣ 📁 fba  
┃ ┃ ┃ ┃ ┣ 🗒 bios.zip  

## ![](/emulators/roms.png) Roms

* Baseado no Romseto MAME: **FBA 0.2.96.71** (abril de 2007) ele próprio é baseado no Romset MAME 0.114
* Tamanho: 3,62GB
* Romsets emulados: 684 (Sem clones neste conjunto)
* Lista de compatibilidade: [/recalbox/share/roms/fba/clrmamepro/piFBA_gamelist.txt](https://gitlab.com/recalbox/recalbox/-/raw/master/board/recalbox/fsoverlay/recalbox/share_init/bios/fba/piFBA_gamelist.txt)
* Arquivo dat: [/recalbox/share/roms/fba/clrmamepro/fba_029671_od_release_10_working_roms.dat](https://gitlab.com/recalbox/recalbox/-/raw/master/board/recalbox/fsoverlay/recalbox/share_init/bios/fba/fba_029671_od_release_10_working_roms.dat)

Algumas ROMs importantes podem precisar ser convertidos para o formato `.fba` para funcionar.
O FBACache_windows.zip está incluso para este fim, mas funciona somente em Windows.

Um arquivo DAT clrmamepro / romcenter está incluído `fba_029671_clrmame_dat.zip` que suporta a maioria das ROMs que funcionam, e pode gerar ROMs compatíveis a partir de versões MAME recentes.

## ![](/emulators/advanced-configuration.png) Configuração avançada do emulador

> Para poder manter suas configurações personalizadas durante uma atualização, aconselhamos que você use nosso recurso de [Sobrecarga de configuração](./../../../advanced-usage/configuration-override).
{.is-info}

## ![external-links.png](/emulators/external-links.png) Links externos

* **Código fonte utilizado** : [https://github.com/recalbox/pifba/](https://github.com/recalbox/pifba)
* **Código fonte original**: [https://github.com/squidrpi/pifba/](https://github.com/squidrpi/pifba)