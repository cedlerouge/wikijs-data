---
title: Atomiswave
description: 
published: true
date: 2021-09-22T00:09:21.800Z
tags: arcade, atomiswave
editor: markdown
dateCreated: 2021-09-22T00:09:21.800Z
---

![](/emulators/arcade/atomiswave.svg){.align-center}

## Ficha técnica

* **Fabricante:** Sammy Corporation
* **Ano de lançamento:** 2003
* **Processador:** CPU RISC Hitachi SH-4 de 32 bits (@ 200 MHz 360 MIPS / 1.4 GFLOPS)
* **Processador Gráfico:** NEC-Videologic PowerVR (PVR2DC/CLX2) @ 100 MHz
* **Chip de som:** ARM7 Yamaha AICA 45 MHZ (com processador interno RISC de 32 bits, 64 canais ADPCM)
* **RAM:** 40 MB
* **Mídia:** Placa ROM (tamanho máximo 168 MB)

## Apresentação

O **Atomiswave** é um sistema de videogame de **cartucho** para máquinas de arcade compatível com JAMMA, criado pela empresa japonesa **Sammy**. Sua comercialização foi lançada no início de 2003. Os jogos são na forma de cartuchos pequenos, muito leves, sólidos e facilmente intercambiáveis.

O Atomiswave pretendia ser um sistema de jogo de baixo custo, compacto, muito fácil de usar e facilmente modular, com a possibilidade de conectar facilmente uma pistola, analógicos duplos, um volante ou uma trackball, e pretendia ser o novo sistema de sucesso do arcade, o sucessor espiritual do Neo-Geo MVS.

Tecnicamente, o sistema Atomiswave é nada mais e nada menos do que um Dreamcast aprimorado para arcade, portanto todo o hardware é da Sega. Com este sistema de arcade, Sammy queria lançar jogos que fossem baratos e o mais rentáveis possível para os operadores. Além dos arcades japoneses, Sammy tinha como alvo o mercado internacional de arcades.

Assim que foi lançado, o Atomiswave foi apoiado pela **SNK Playmore**, que decidiu abandonar o desenvolvimento de arcade no **Neo-Geo MVS** em favor deste sistema para seus futuros jogos (antes de passar para o Taito Type X²). Outros desenvolvedores que deram suporte ao sistema foram **Dimps**, **IGS** e **Arc System Works**, mas a maioria dos títulos Atomiswave foram desenvolvidos pela Sammy.

Também deve ser lembrado que Sammy e Sega, ambos já tinham uma história em videogames e no arcade, e **se fundiram em 2004**, durante o funcionamento deste sistema. As duas empresas fazem parte do mesmo grupo, agora liderado pela empresa matriz **Sega Sammy Holdings**.

Em 2009, três anos após o lançamento do último jogo Atomiswave, a Sega Amusement USA decidiu lançar dois novos jogos para o sistema, Sega Clay Challenge e Sega Bass Fishing Challenge, com o objetivo de atualizar os terminais Atomiswave ainda em operação.

Atomiswave foi um semi-fracasso para a Sammy. Ela só lançou cerca de 30 jogos no sistema antes que ele fosse **descontinuado no final de 2009**.

## Emuladores

[Libretro Flycast](libretro-flycast)