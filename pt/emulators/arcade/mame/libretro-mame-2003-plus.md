---
title: Libretro MAME 2003 Plus
description: 
published: true
date: 2021-10-01T00:05:24.493Z
tags: libretro, mame, mame2003+, mame2003plus
editor: markdown
dateCreated: 2021-10-01T00:02:14.862Z
---

**Libretro MAME2003_Plus** (também chamado de MAME 2003+ e mame2003-plus) é um núcleo de emulador de sistema de arcade **Libretro** que enfatiza alto desempenho e ampla compatibilidade com dispositivos móveis, computadores de placa única, sistemas integrados e outras plataformas semelhantes.

A fim de aproveitar o desempenho e os baixos requisitos de hardware de uma arquitetura MAME anterior, o MAME 2003-Plus começou com o código base do MAME 2003, que por sua vez derivou do MAME 0.78.

Com base nisso, os colaboradores do MAME 2003-Plus retro-portaram suporte para várias centenas de jogos adicionais, bem como outros recursos não presentes originalmente no MAME 0.78.

## ![](/emulators/license.svg) Licença

Este núcleo é licenciado sob uma licença [**MAME não-comercial**](https://github.com/libretro/mame2003-plus-libretro/blob/master/LICENSE.md).

## ![](/emulators/compatibility.png) Compatibilidade

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ | ✅ |

## ![](/emulators/features.png) Recursos

| Recurso | Suportado |
| :---: | :---: |
| Saves | ✔ |
| Savestates | Depende do jogo |
| Rebobinar | ✔ |
| Netplay | ✔ |
| Trapaças Nativas | ✔ |
| Controles | ✔ |
| Multi-Mouse | ✔ |

## ![](/emulators/bios.svg) BIOS

Os Romsets BIOS não são necessários quando se utilizam romsets de arcade "Full Non-Merged" (Completamente Não-Mesclado). Para romsets "Split"(divididos) e "Non-Merged"(Não-mesclado), coloque a BIOS na mesma pasta que o romset do jogo.

>**Nota** :
>Coloque as BIOS na pasta: `/recalbox/share/roms/mame/`   
>ou sua subpasta se você estiver utilizando múltiplos núcleos Mame, por exemplo: :`/recalbox/share/roms/mame/Mame 2003-Plus/`
{.is-warning}

## ![](/emulators/roms.png) Roms

* Baseado no romset: 0.78-0.188 (MAME 0.78 como linha de base com outras ROMs com base em conjuntos de ROMs MAME posteriores)
* Tamanho: 32gb
* Romsets emulados: 4941 (incluindo clones, etc ...)
* Conjuntos ativos: 4941
* Pais: 1089
* Clones: 2123
* Outros: 1713
* BIOS: 16
* CHDs: 30
* Amostras: 66 + 6 "Amostras de trilha sonora" opcionais
* Arquivo dat: [mame2003-plus.xml] (https://github.com/libretro/mame2003-plus-libretro/blob/master/metadata/mame2003-plus.xml)


### Local

Coloque as roms assim:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 mame
┃ ┃ ┃ ┃ ┣ 🗒 **jogo.zip**
┃ ┃ ┃ ┃ ┣ 📁 **jogo**
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **jogo.chd**

Você também pode optar por uma subpasta (útil se você quiser ter um Set MAME para outro núcleo).

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 mame
┃ ┃ ┃ ┃ ┣ 📁 Mame 2003-Plus
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **jogo.zip**
┃ ┃ ┃ ┃ ┃ ┣ 📁 **jogo**
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 **jogo.chd**

## ![](/emulators/advanced-configuration.png) Configuração avançada do emulador

> Para poder manter suas configurações personalizadas durante uma atualização, aconselhamos que você use nosso recurso de [Sobrecarga de configuração](./../../../advanced-usage/configuration-override).
{.is-info}

### Acessar as Opções

Você pode configurar várias opções de duas maneiras diferentes.

* Por meio do Menu RetroArch:

┣ 📁 Menu RetroArch
┃ ┣ 📁 Opções do núcleo
┃ ┃ ┣ 🧩 Nome_da_opção

* Por meio do arquivo `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opções de núcleo

## ![external-links.png](/emulators/external-links.png) Links externos

* **Progettosnaps** : [https://www.progettosnaps.net/](https://www.progettosnaps.net/)
* **Código fonte utilizado** : [https://github.com/libretro/mame2003-plus-libretro/](https://github.com/libretro/mame2003-plus-libretro)
* **Documentação Libretro** : [https://docs.libretro.com/library/mame2003\_plus/](https://docs.libretro.com/library/mame2003_plus/)