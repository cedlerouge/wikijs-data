---
title: Libretro MAME
description: 
published: true
date: 2021-10-01T00:22:18.021Z
tags: libretro, mame
editor: markdown
dateCreated: 2021-10-01T00:19:41.107Z
---


## ![](/emulators/license.svg) Licença

Este núcleo é licenciado sob uma licença **MAME não-comercial**.

## ![](/emulators/compatibility.png) Compatibilidade

| RPI0/RPI1 | RPI2 | RPI3 | RPI4/RPI400 | ODROID XU4 | ODROID GO | PC x86 | PC x86_64 |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| ❌ | ❌ | ❌ | ❌ | ❌ | ❌ | ✅ | ✅ |

## ![](/emulators/features.png) Recursos



## ![](/emulators/bios.svg) BIOS

Os Romsets BIOS não são necessários quando se utilizam romsets de arcade "Full Non-Merged" (Completamente Não-Mesclado). Para romsets "Split"(divididos) e "Non-Merged"(Não-mesclado), coloque a BIOS na mesma pasta que o romset do jogo.

>**Nota** :
>Coloque as BIOS na pasta: `/recalbox/share/roms/mame/`   
>ou sua subpasta se você estiver utilizando múltiplos núcleos Mame, por exemplo: `/recalbox/share/roms/mame/`
{.is-warning}

## ![](/emulators/roms.png) Roms

O Libretro-MAME é baseado nas últimas versões do emulador MAME e o catálogo de jogos é extenso.

[Clique aqui](https://www.mamedev.org/) para mais informações sobre o desenvolvimento do Mame.

* Baseado no romset: MAME 0.235 (agosto de 2021)
* Tamanho: 263 gb
* Romsets emulados: 36282 (incluindo clones, etc ...)
* Conjuntos ativos: 36282
* Pais: 5150
* Clones: 23423
* Outros: 7636
* BIOS: 73
* CHDs: 1173
* Amostras: 18483
* Arquivo dat:  [http://www.progettosnaps.net/dats/MAME/](http://www.progettosnaps.net/dats/MAME/)
* Arquivo dat : [MAME_Dats_235.7z](https://www.progettosnaps.net/download/?tipo=dat_mame&file=/dats/MAME/packs/MAME_Dats_235.7z)

### Extensões suportadas

As ROMs devem ter as seguintes extensões:

* .zip
* .chd

Os arquivos CHD são necessários apenas para algumas roms. Se não houver nenhum arquivo CHD para uma ROM, a ROM funcionará.

### Local

Coloque as roms assim:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 mame
┃ ┃ ┃ ┃ ┣ 🗒 **jogo.zip**
┃ ┃ ┃ ┃ ┣ 📁 **jogo**
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **jogo.chd**

Você também pode optar por uma subpasta (útil se você quiser ter um Set MAME para outro núcleo).

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 roms
┃ ┃ ┃ ┣ 📁 mame
┃ ┃ ┃ ┃ ┣ 📁 Mame
┃ ┃ ┃ ┃ ┃ ┣ 🗒 **jogo.zip**
┃ ┃ ┃ ┃ ┃ ┣ 📁 **jogo**
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🗒 **jogo.chd**

## ![](/emulators/advanced-configuration.png) Configuração avançada do emulador

> Para poder manter suas configurações personalizadas durante uma atualização, aconselhamos que você use nosso recurso de [Sobrecarga de configuração](./../../../advanced-usage/configuration-override).
{.is-info}

### Acessar as Opções

Você pode configurar várias opções de duas maneiras diferentes.

* Por meio do Menu RetroArch:

┣ 📁 Menu RetroArch
┃ ┣ 📁 Opções do núcleo
┃ ┃ ┣ 🧩 Nome_da_opção

* Por meio do arquivo `retroarch-core-options.cfg`:

┣ 📁 recalbox
┃ ┣ 📁 share
┃ ┃ ┣ 📁 system
┃ ┃ ┃ ┣ 📁 configs
┃ ┃ ┃ ┃ ┣ 📁 retroarch
┃ ┃ ┃ ┃ ┃ ┣ 📁 cores
┃ ┃ ┃ ┃ ┃ ┃ ┣ 🧩 **retroarch-core-options.cfg**

### Opções de núcleo

## ![external-links.png](/emulators/external-links.png) Links externos

* **Progettosnaps** : [https://www.progettosnaps.net/](https://www.progettosnaps.net/)
* **Site Oficial** : [https://github.com/libretro/mame/](https://github.com/libretro/mame)