---
title: Daphne
description: LaserDisc
published: true
date: 2021-09-23T00:29:53.105Z
tags: arcade, daphne
editor: markdown
dateCreated: 2021-09-23T00:28:25.017Z
---

![](/emulators/arcade/daphne.svg){.align-center}

## Ficha técnica

* **Desenvolvedores:** Matt Ownby (Daphne) e Jeffrey Clark (Hypseus)
* **Ano de lançamento:** 1999
* **Website:** [http://www.daphne-emu.com/](http://www.daphne-emu.com/site3/index_hi.php) em C+++
* **Daphne** é um programa que lhe permite reproduzir as versões originais de muitas versões originais do arcade LaserDisc.

## Apresentação

O **LaserDisc** foi o primeiro meio de armazenamento óptico, inicialmente para vídeo, a ser lançado em 1978 na América do Norte, inicialmente sob o nome de MCA DiscoVision.

Embora oferecesse melhor qualidade de som e imagem do que a mídia contemporânea (notadamente as fitas VHS e Betamax), o LaserDisc teve pouco sucesso, principalmente devido ao alto preço dos reprodutores e ao fato de não poder gravar programas de televisão. Embora fosse popular entre os proprietários de home cinema desde o início, foi somente na Ásia (Hong Kong, Malásia e Cingapura), nos anos 90, que o LaserDisc realmente se espalhou pelos lares.

Entretanto, é a partir da tecnologia LaserDisc que várias mídias de armazenamento ótico foram desenvolvidas: em particular CDs e DVDs, que obtiveram considerável sucesso.

### O que é um jogo LaserDisc?

Um jogo de **LaserDisc** é um jogo que **utiliza vídeo pré-gravado** (filmes ou animações) **reproduzido a partir de um laserdisc**, seja para **toda** ou para **parte dos gráficos**, **alguns jogos** utilizam vídeos em que **sprites** (carros, navios, ...) serão **sobrepostos.**

Mas os **mais populares** jogos eram **filmes ou desenhos animados interativos**, nos quais **o jogador** tinha que **pressionar um botão específico** ou **mover o joystick** na direção certa no momento certo para **mover para a próxima cena.**

### O que é DAPHNE?

O DAPHNE é um emulador; ou melhor, uma 'emuladora' (porque foi pensado como uma mulher ou &lt;&lt;**FEMALE**&gt;&gt; :_First Ever Multiple Arcade Laserdisc Emulator_) que permite que você jogue **jogos de arcade no disco laser** (marca LaserDisc), como **Dragon's Lair** (de onde vem o nome do emulador, baseado no nome da princesa do jogo), **Badlands**, **Cobra Command**, **Space Ace** ou muitos outros.

Foi criado por **Matt Ownby** durante 1999 em um emulador de processador Z80. O primeiro jogo a ser emulado foi o Dragon's Lair, o mais famoso! Juntamente com outros colegas e amigos, ele conseguiu emular quase vinte jogos. Entretanto, ele parou de publicar e atualizar em 2009 devido à frustração com a comunidade emuladora, que esperava uma gestão profissional de um projeto que era basicamente "por diversão".

Uma reescrita do DAPHNE está atualmente em andamento do lado de Matt Ownby, com um verdadeiro modelo de negócios definido no qual ele poderá oferecer suporte comercial a esses usuários.

## Emuladores

[Hypseus](hypseus)