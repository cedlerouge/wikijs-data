---
title: Arcade
description: 
published: true
date: 2021-09-21T23:59:31.945Z
tags: arcade
editor: markdown
dateCreated: 2021-09-21T23:59:31.944Z
---

![](/emulators/arcade/arcade-logo.png)

![](/emulators/arcade/arcade-bartop.svg)

Um jogo de Arcade (fliperama) ou jogo de azar é um entretenimento operado por moedas. A maioria dos jogos arcade são jogos de vídeo, máquinas de pinball, jogos eletro-mecânicos, jogos comerciais ou promocionais.

Enquanto as datas exatas são debatidas, a era dourada dos videogames arcade é geralmente definida como um período que começa no final dos anos 70 e termina em meados dos anos 80.

Além de um breve ressurgimento no início dos anos 90, a indústria arcade então diminuiu no Hemisfério Ocidental à medida que o número de consoles de videogame concorrentes em termos de gráficos e jogabilidade aumentou e os custos diminuíram.

