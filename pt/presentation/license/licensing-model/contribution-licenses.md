---
title: Licenças de contribuição
description: 
published: true
date: 2021-08-10T23:58:01.643Z
tags: 
editor: markdown
dateCreated: 2021-08-10T23:58:01.643Z
---

Para o desenvolvimento de software e projetos de trabalhos criativos, aplica-se o regime de licenciamento usual "entrada = saída", a menos que especificado de outra forma.

Conforme explicado claramente nos [guias de código aberto](https://opensource.guide/pt/legal/) do GitHub, isso significa que uma licença de código aberto serve implicitamente como uma licença de entrada (de contribuidores) e de saída. (Para outros contribuidores e usuários).

Isso significa que ao contribuir para um de nossos projetos, você aceita os termos de sua licença e envia sua contribuição nos mesmos termos.