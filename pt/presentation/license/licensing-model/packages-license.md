---
title: Licença dos pacotes
description: 
published: true
date: 2021-08-10T23:54:40.211Z
tags: 
editor: markdown
dateCreated: 2021-08-10T23:54:40.211Z
---

O Recalbox é construído usando [Buildroot](https://buildroot.org/), que agrupa muitos pacotes com licenças diferentes e produz um sistema operacional completo.

Cada pacote é coberto e protegido por sua(s) própria(s) licença(s) e permanece como propriedade de seu(s) respectivo(s) autor(es).

[Trabalhamos](https://gitlab.com/recalbox/recalbox/issues/1501) para tornar as [informações legais](https://buildroot.org/downloads/manual/manual.html#_complying_with_open_source_licenses) funcionais no código-fonte do Recalbox para dar uma visão abrangente de todas as licenças usadas por cada pacote.

Enquanto isso, só podemos convidá-lo a verificar manualmente a(s) licença(s) de cada pacote.

> Embora algumas licenças de pacotes possam ser incompatíveis umas com as outras, o Recalbox não é considerado um "trabalho derivado" ou "versão modificada" dele, mas um mero agregado disso.
>
> Como tal, a viralidade de algumas licenças não tem impacto na licença de outros pacotes ou na licença do próprio Recalbox.
>
> Consulte as [Perguntas frequentes sobre a Licença Pública Geral (GPL)](https://www.gnu.org/licenses/gpl-faq.fr.html) para obter mais detalhes.
{.is-info}