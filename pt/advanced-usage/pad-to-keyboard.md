---
title: Pad To Keyboard
description: Traga seus jogos de Computadores antigos de volta à vida, jogando-os com seus controles, mesmo quando eles são projetados apenas para teclado!
published: true
date: 2021-09-12T13:49:46.818Z
tags: p2k, controle, teclado
editor: markdown
dateCreated: 2021-09-06T13:12:13.217Z
---

Sob este nome bárbaro está escondido um módulo que é particularmente importante ao coração de um de nossos desenvolvedores, pois permitirá **dar vida a todos aqueles jogos de computadores**, esquecidos por todos os Recalboxers que não têm um teclado conectado, ou seja, quase todos...

## Do Joystick ao Teclado...

Durante a gloriosa década de 80, nasceram muitos microcomputadores. Mas, naquela época, o joystick para computadores ainda não existia, e o Joystick só era totalmente explorado em consoles.  
Os jogos em microcomputadores tinham uma ergonomia orientada pelo teclado, e nos melhores casos, teclado ou joystick. Muitas vezes, o teclado era necessário para saltar a introdução ou para iniciar o jogo.  
Portanto, era impossível desfrutar destes jogos sem um teclado conectado.

Pelo menos até o Recalbox 7.0, que inclui um módulo Pad-To-Keyboard que mapeia as ações do controle para os pressionamentos/liberações de teclas do teclado!
Este módulo permite o uso de pequenos e simples arquivos de configuração. Vejo alguns de vocês ao fundo, olhando para mim com olhos duvidosos...

É muito simples:

* Você tem um ótimo jogo no Commodore 64, mas não pode jogá-lo porque é preciso apertar uma tecla para iniciar um jogo? Não há problema:
  * Mapeie o botão `START` do seu controle para a tecla `ESPAÇO`, e o problema será resolvido.
  * Inicie um jogo, pressione `START` e o jogo entenderá que você pressionou a tecla de espaço de um teclado. 
* Você tem um jogo no Amstrad que é jogado com as 4 teclas de setas e a barra de espaço? Não há problema:
  * Mapeie o direcional de seu controle como as 4 teclas de setas do teclado, e o botão `A` para `Espaço`.
  * E jogue seu jogo como se ele sempre tivesse sido feito para um joystick!

E, sem dúvidas, algum desenvolvedor externo talentoso fará até mesmo uma pequena interface para criar estas configurações sem esforço.

### Como funciona?

Quando detecta uma configuração _**P2K**_ (Pad to Keyboard) para um jogo, o EmulationStation criará um teclado virtual que será visto pelo sistema como um teclado físico real. Durante toda a sessão de jogo, o EmulationStation interceptará os eventos do controle e, dependendo da configuração, gerará pressionamentos e liberações de teclas do teclado.

Os emuladores que estão aguardando respostas do teclado, usarão então estes eventos de teclado na máquina que estão emulando, como se viessem de um teclado físico.

#### E quanto ao *input-lag* (atraso de entrada)?

Garantia de 100% de entrada livre de atraso, e funciona em todos os computadores!

O módulo P2K está em alta prioridade, o que significa que ele assume o controle assim que um evento ocorre no controle, para traduzi-lo e enviá-lo de volta para o teclado virtual instantaneamente.

No Raspberry Pi 3, as medidas tomadas entre a recepção do controle e a recepção da ação no teclado dão resultados que são muito inferiores ao que poderia gerar o menor atraso de entrada.

### O que ainda não podemos fazer

No momento, o mapeamento é um mapeamento instantâneo de 1 para 1, ou seja, as ações do teclado são instantaneamente traduzidas em uma única tecla sendo pressionada/liberada.

Portanto, não é possível:

* Gerar um evento de pressionamento multi-toque: SHIFT + outra tecla, por exemplo.
* Gerar pressionamentos/liberações consecutivas para digitar vários caracteres: você não pode simular a digitação de uma palavra, por exemplo.

Mas pode ser que isso venha em versões futuras 😉 

## Configuração

### Arquivo de configuração

Os arquivos de configuração para um mapeamento pad-to-keyboard podem ser definidos para uma pasta completo (e suas subpastas) ou para um jogo em específico.

Para uma pasta, ele deve ser colocado dentro da pasta e nomeado como `.p2k.cfg`.
Isto dará um caminho final do tipo `/caminho/para/.p2k.cfg`.

Para uma determinada ROM (jogo), ele deve ter o mesmo nome da ROM com a extensão, mais o sufixo `.p2k.cfg`.
Isto resultará em `/caminho/para/o_jogo.ext` ser `/caminho/para/o_jogo.ext.p2k.cfg`.

As configurações da pasta estão ativas para todos os jogos na pasta e subpastas.

Se vários arquivos estiverem presentes, eles se sobrepõem em ordem decrescente de pasta, até a configuração específica do jogo, se houver. Isto será discutido em detalhes posterioremente.

#### Configuração de mapeamento

Vamos começar com um exemplo utilizado em testes:

```ini
# direcional do controle para setas do teclado
0:up = up
0:down = down
0:right = right
0:left = left

# Botão A para ESPAÇO
0:a = space
# B para Y (quando o jogo exige confirmação com Y em "YES")
0:b = y
# E finalmente mapear o START para ENTER
0:start = enter
```

Como você pode ver, a estrutura é muito simples.

Você pode deixar linhas em branco para separar os comandos, e usar # no início da linha para adicionar comentários/descrições/lembretes.

As linhas de configuração do mapeamento são da forma `CONTROLE`:`AÇÃO` = `TECLA` , onde:

* `CONTROLE`: Número do controle conforme definido no EmulationStation, ou a ordem natural dos controles se não houver atribuição forçada. Raramente mais de um controle será mapeado, portanto **0** será usado na grande maioria dos casos.
* `AÇÃO`: A ação do controle a ser mapeado. Todos os botões, setas direcionais (D-Pad) e 4 direções principais de joysticks analógicos podem ser mapeados, de modo que até 25 ações podem ser atribuídas a teclas de teclado em um teclado padrão completo.
* `TECLA`: A tecla do teclado que está associada à ação do joystick. Quando a ação do controle é acionada, a tecla correspondente é pressionada. Quando a ação do joystick é liberada, a chave correspondente é liberada. Como mencionado acima, o mapeamento é 1:1. Se você quiser simular _**SHIFT  ESQUERDO + A**_, por exemplo, você terá que atribuir _**SHIFT ESQUERDO**_ a um botão no joystick (por exemplo _**L1**_), e a tecla _**A**_ a outro botão (digamos o botão_**Y**_). Pressionar _**L1+Y**_ simultaneamente resultará em um _**SHIFT ESQUERDO + A**_. Existe uma exceção para joysticks analógicos. Como podem ser colocados na diagonal, esta característica pode ser usada para pressionar duas teclas ao mesmo tempo. 

`AÇÃO` e `TECLA` não são sensíveis a maiúsculas/minúsculas, também não importa os espaços no início de uma linha, no final de uma linha, ou em torno do `=`.

#### Ações do joystick

A seguir está uma lista completa de ações do controle que podem ser utilizadas:

| Ação | Descrição | Ação | Descrição |
| :--- | :--- | :--- | :--- |
| `up` | Direcional CIMA | `j1up` | Analógico esquerdo CIMA |
| `down` | Direcional BAIXO | `j1down` | Analógico esquerdo BAIXO |
| `left` | Direcional ESQUERDA | `j1left` | Analógico esquerdo ESQUERDA |
| `right` | Direcional DIREITA | `j1right` | Analógico esquerdo DIREITA |
| `a` | Botão A | `j2up` | Analógico direito CIMA |
| `b` | Botão B | `j2down` | Analógico direito BAIXO |
| `x` | Botão X | `j2left` | Analógico direito ESQUERDA |
| `y` | Botão Y | `j2right` | Analógico direito DIREITA |
| `l1` | Botão L1 | `l2` | Botão L2 |
| `r1` |Botão R1 | `r2` | Botão R2 |
| `start` | Botão START | `l3` | Botão L3 |
| `select` | Botão SELECT | `r3` | Botão R3 |

#### Teclas de teclado utilizáveis

O teclado virtual criado pelo EmulationStation é um teclado "agnóstico" (layout de teclado independente). Não tem linguagem e nenhum layout especial. É, portanto, um teclado QWERTY básico, com 105 teclas.

Para não confundir os usuários, somente certas teclas comuns a todos os teclados são configuráveis. Isto se deve a duas boas razões:

* Se eu lhe disser que em meu teclado português mapeei o botão Y para a tecla %, há todas as chances de que ela não corresponda à tecla % em seu teclado.
* O emulador também terá que mapear um teclado de 105 teclas para o teclado da máquina emulada. Isto às vezes é tão complexo e pouco intuitivo que alguns emuladores também oferecem um teclado virtual na tela.

Entretanto, não entre em pânico, na grande maioria dos casos, você estará usando o mapeamento simples de teclas. Basta ter em mente que o layout do teclado é QWERTY.

Uma imagem vale mais do que mil palavras:

![Nomes do teclado](/advanced-usage/p2k/p2k-layout-v2.png){.full-width}

#### Sobrecarga de configuração (sim, novamente!)

Como mencionado acima, os arquivos de configuração, quando há vários em diferentes níveis de pastas e/ou para o próprio jogo, sobrecarregam-se mutuamente.

O módulo P2K começará lendo o superior, depois descerá para a pasta da ROM e lerá todos os arquivos de configuração que encontrar, para terminar com o da ROM, se existir.

Isso significa que as configurações podem ser adicionadas ou modificadas!

**Exemplo 1: As configurações são somadas**

**/caminho/para/.p2k.cfg:**

```ini
# Mapear o START como ENTER
0:start = enter
```

**/caminho/para/o_jogo.ext.p2k.cfg:**

```ini
# Mapear o SELECT como ESPAÇO
0:select = space
```

Será o equivalente a:

```ini
# Mapear o START como ENTER
0:start = enter
# Mapear o SELECT como ESPAÇO
0:select = space
```

**Exemplo 2: Mudança de configurações**

**/caminho/para/.p2k.cfg:**

```ini
# Mapear o START como ENTER
0:start = enter
# Mapear o SELECT como ESPAÇO
0:select = space
```

**/caminho/para/o_jogo.ext.p2k.cfg:**
```ini
# Mapear SELECT como F1
0:select = f1
```

Será o equivalente a:

```ini
# Mapear o START como ENTER
0:start = enter
# Mapear SELECT como F1
0:select = f1
```

**Exemplo 3: Mudança de configurações através da eliminação de mapeamentos**

**/caminho/para/.p2k.cfg:**

```ini
# Mapear o START como ENTER
0:start = enter
# Mapear o SELECT como ESPAÇO
0:select = space
```

**/caminho/para/o_jogo.ext.p2k.cfg:**

```ini
# Remover o mapeamento do SELECT!
0:select = 
```

Será o equivalente a:

```ini
# Mapear o START como ENTER
0:start = enter
```

#### Adicione uma breve descrição

![](/advanced-usage/p2k/configp2k-pt.png){.full-width}

Para acrescentar uma descrição como acima:

* Use dois ponto-e-vírgula para fazê-la aparecer:

```ini
# Direcional do joystick como setas do teclado
0:up = up ;; Cima
0:down = down ;; Baixo
0:right = right ;; Direita
0:left = left ;; Esquerda

# Analógico esquerdo como setas do teclado
0:j1up = up ;; Cima
0:j1down = down ;; Baixo
0:j1right = right ;; Direita
0:j1left = left ;; Esquerda

# Start como Enter
0:start = enter ;; Enter (Entrar)
# Select como ESC
0:select = esc ;; ESC (Retornar)

# X como espaço
0:b = space ;; Espaço
# O como Ctrl
0:a = leftctrl ;; Pular
# [] como Alt
0:y = leftalt ;; Atirar

#L1 como F1
0:l1 = F1 ;; Opções do jogo
#L2 como H
0:l2 = H ;; Ajuda

#R1 como F5
0:r1 = F5 ;; F5
#R2 como B
0:r2 = b ;; B
#L3 como N
0:l3 = n ;; Não/No (N)
#R3 como Y
0:r3 = y ;; Sim/Yes (Y)
```

## Extensão P2K v2 (Do Recalbox 7.3)

A partir do Recalbox 7.3, o P2K amplia sua funcionalidade para suportar :

* Emulação de 3 botões do mouse
* Combinações de teclas
* Seqüências de teclas

#### Emulação de 3 botões do mouse

Agora você será capaz de emular o movimento do mouse e a liberação de seus 3 botões diretamente pelo seu controle.
Para uma emulação precisa, recomenda-se um controle com analógico, mas a emulação é perfeitamente possível com os botões de comando ou o direcional.

#### Configuração completa

Além das teclas `TECLA` do teclado, há 7 valores, correspondentes às 4 direções de movimento mais os 3 botões do mouse.

| Elemento do Mouse | Descrição |
| :--- | :--- |
| `mouse.button.left` | Botão esquerdo do mouse |
| `mouse.button.middle` | Botão central do mouse / clique do *scroll* (a "rodinha") |
| `mouse.button.right` | Botão direito do do mouse |
| `mouse.move.left` | Mover o mouse para a esquerda |
| `mouse.move.up` | Mover o mouse para cima |
| `mouse.move.right` | Mover o mouse para a direita |
| `mouse.move.down` | Mover o mouse para baixo |

Estes valores podem ser atribuídos a qualquer `AÇÃO`.
Digamos que temos um simples controle de SNES com um DPAD (direcional) e 4 botões, e queremos usar o mouse em um jogo DOS apenas com o mouse, isto é o que um arquivo p2k pareceria:

```ini
# Configuração Pad-to-Keyboard do Recalbox
# JOGO DE DOS

# Movimento do Mouse
0:up = mouse.move.up
0:right = mouse.move.right
0:down = mouse.move.down
0:left = mouse.move.left

# Botões do Mouse
0:a = mouse.button.left
0:b = mouse.button.right
```

Com tal configuração, o movimento do cursor acelera no início antes de estabilizar-se a uma certa velocidade.

Você também pode usar um joystick analógico, por exemplo, o analógico direito de um controle XBox:

```ini
# Configuração Pad-to-Keyboard do Recalbox
# JOGO DE DOS

# Movimento do Mouse
0:j2up = mouse.move.up  ;; Movimento do Mouse
0:j2right = mouse.move.right ;; Movimento do Mouse
0:j2down = mouse.move.down ;; Movimento do Mouse
0:j2left = mouse.move.left ;; Movimento do Mouse

# Botões do Mouse
0:a = mouse.button.left ;; Botão esquerdo
0:b = mouse.button.right ;; Botão direito
```

Neste caso, a pressão utilizada no analógico que irá regular a velocidade do movimento do cursor.

#### Configuração simplificada

Se você estiver utilizando os joysticks analógicos esquerdo ou direito para emular o movimento do mouse, você pode utilizar a configuração simplificada `0:j1 = mouse.moves` ou `0:j2 = mouse.moves` (*atenção para os 's'!)*.
Se tomarmos a configuração anterior, ela pode ser simplificada da seguinte forma:

```ini
# Configuração Pad-to-Keyboard do Recalbox
# JOGO DE DOS

# Movimento do Mouse
0:j2 = mouse.moves ;; Movimento do Mouse

# Botões do Mouse
0:a = mouse.button.left ;; Botão esquerdo
0:b = mouse.button.right ;; Botão direito
```

Para jogos que precisam apenas de um mouse, e são muitos, você pode mapear o mouse para vários elementos do controle, como este:

```ini
# Configuração Pad-to-Keyboard do Recalbox
# JOGO DE DOS

# Movimento do Mouse
0:up = mouse.move.up
0:right = mouse.move.right
0:down = mouse.move.down
0:left = mouse.move.left
0:j1 = mouse.moves ;; Movimento do Mouse
0:j2 = mouse.moves ;; Movimento do Mouse

# Botões do Mouse
0:a = mouse.button.left ;; Botão esquerdo
0:b = mouse.button.right ;; Botão direito
```

Com este arquivo p2k, tanto os analógicos (direito e esquerdo), como o dpad(direcional), podem ser usados para controlar o cursor do mouse facilmente, dependendo das preferências individuais e do nível de precisão necessário.

### Configuração estendida do teclado

Com o P2K v2, as capacidades de emulação de teclado são grandemente expandidas para suportar combinações de teclas e seqüências de teclas (como digitação).
Em ambos os casos, o sistema suporta até 32 teclas. Portanto, ou 32 teclas pressionadas *quase* simultaneamente, ou uma seqüência de 32 teclas consecutivas.

#### Combinações de teclas

Para definir uma combinação de teclas, é muito simples, basta separar as teclas com espaços (e nada mais que espaços!), assim:

```ini
# Atribuir CTRL+C a R1
0:r1 = leftctrl c ;; Fecha o programa!
```

Observe que quando R1 é pressionado, as teclas são pressionadas na ordem exata da definição. Depois, quando R1 é liberado, as chaves são liberadas em ordem inversa.

Caso o emulador exija um atraso entre teclas, ou caso você precise de um atraso explícito por algum motivo, você pode preceder as teclas com `+xxx` onde xxx é um atraso em milissegundos.

Exemplo:

```ini
# Atribuir CTRL+C a R1
0:r1 = +100 leftctrl c ;; Fecha  o programa!
```

Neste caso: quando `R1` é pressionado, o P2K pressiona a tecla `CONTROL ESQUERDO`, espera 100ms, depois pressiona a tecla `C`. A última tecla não introduz um atraso. Então, quando `R1` é liberado, as teclas são liberadas na ordem inversa: `C` é liberado, espera 100ms, então libera o `CONTROL ESQUERDO`.

O `+` indica explicitamente que esta é uma combinação de teclas. Ela pode ser usada por conta própria, sem atraso, mas não terá nenhuma utilidade neste caso, pois sem um indicador explícito, várias teclas definidas formarão uma combinação.

#### Seqüência de teclas

Para uma seqüência de teclas, precisamos utilizar um indicador explícito `/`, como este:

```ini
# Atribuir a seqüência FGHJ a R1
0:r1 = / f g h j ;; Digita FGHJ!
```

Aqui, quando `R1` é pressionado, o sistema gerará a seguinte seqüência: pressionar `F`, soltar `F`, pressionar `G`, soltar `G`, pressionar `H`, soltar `H`, pressionar `J`, soltar `J`. Sem atraso entre os pressionamentos e as liberações, e em ordem estrita de definição.

Note que quando o `R1` é liberado, nada acontece. A seqüência é enviada apenas ao pressionar.

Mais uma vez, alguns emuladores/jogos podem ter problemas com toques de teclas sem atrasos. Assim, podemos declarar um atraso de 25ms entre cada pré-liberação/liberação, desta forma:

```ini
# Atribuir a seqüência FGHJ a R1
0:r1 = /25 f g h j ;; Digita FGHJ!
```

Neste caso a seqüência será: pressionar `F`, esperar 25ms, soltar `F`, esperar 25ms, pressionar `G`, esperar 25ms, soltar `G`, esperar 25ms, pressionar `H`, esperar 25ms, soltar `H`, esperar 25ms, pressionar `J`, esperar 25ms, soltar `J`.
Sem atraso após a última liberação.

### O que ainda não é suportado?

Apenas uma coisa: seqüências de teclas com combinações no meio. É necessário decidir entre um ou outro.
Será impossível escrever uma palavra que exija caracteres `SHIFT` na máquina emulada.

Talvez em uma versão 3... :)

## Seção especial para compartilhar suas configurações

Você configurou seus jogos favoritos para serem totalmente jogáveis com o controle?  
Portanto, não deixe que outros tenham o mesmo trabalho novamente à toa: compartilhe suas configurações!  
Abrimos um tópico especial [**em um lugar no fórum Recalbox**](https://forum.recalbox.com/topic/21287/partager-vos-fichiers-pad2keyboard-p2k-cfg?_=1600282719468), para que todos possam compartilhar suas configurações.

Entretanto, tenha cuidado: tente manter-se lógico e ter um mapeamento o mais intuitivo possível:

* Um jogo deve ser iniciado com START ou com o botão A.
* Se um jogo requer uma ação de teclado para configurar o joystick (muitas vezes o caso no ZX Spectrum, por exemplo) fique com o teclado e faça um mapeamento completo de todas as teclas.

Bons jogos!