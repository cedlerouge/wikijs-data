---
title: Visualização dos sistemas
description: Funcionamento e modificações na visualização dos sistemas.
published: true
date: 2021-08-17T23:39:22.731Z
tags: 6.1+
editor: markdown
dateCreated: 2021-08-17T23:39:22.731Z
---

## Funcionamento da visualização

A exibição dos sistemas no menu do Recalbox é gerenciada pelo arquivo `es_systems.cfg`. Este arquivo está localizado na pasta `/recalbox/share_init/system/.emulationstation/es_systems.cfg`.

Ele contém os nomes dos vários sistemas que são suportados por sua versão do Recalbox. Ele é construída da seguinte forma:

```xml
<?xml version="1.0"?>
<systemList>
  <system>
    <fullname>Panasonic 3DO</fullname>
    <name>3do</name>
    <path>/recalbox/share/roms/3do</path>
    <extension>.iso .ISO .cue .CUE .chd .CHD</extension>
    <command>python /usr/lib/python2.7/site-packages/configgen/emulatorlauncher.pyc %CONTROLLERSCONFIG% -system %SYSTEM% -rom %ROM% -emulator %EMULATOR% -core %CORE% -ratio %RATIO% %NETPLAY%</command>
    <platform>3do</platform>
    <theme>3do</theme>
    <emulators>
      <emulator name="libretro">
        <cores>
          <core>4do</core>
        </cores>
      </emulator>
    </emulators>
  </system>
  <system>
      [. . .]
  </system>
  [. . .]
</systemList>
```

A exibição dos sistemas segue a ordem na qual eles estão listados neste arquivo. Ele também contém a configuração desses sistemas.

## Alterar a ordem de exibição

> Você **NÃO deve** alterar o arquivo original `es_systems.cfg` (que está na pasta `share_init`). Se houver algum problema com as mudanças feitas posteriormente, este arquivo será a fonte para que o Recalbox funcione corretamente.
{.is-warning}

A alteração da ordem de exibição só deve ser feita a partir do arquivo `es_systems.cfg` na pasta `/recalbox/share/system/.emulationstation/es_systems.cfg`.

Originalmente, este arquivo não existe. Você deve copiar o arquivo original ou criar um novo arquivo. Uma vez que o novo arquivo tenha sido criado, é possível colocar os sistemas em qualquer ordem que desejar. A configuração dos sistemas ainda será extraída do arquivo original `es_systems.cfg`, mas a ordem dos sistemas será a definida no novo arquivo.

Se, no novo arquivo, um sistema estiver faltando ou incorretamente preenchido, a prioridade é dada ao arquivo original. Para o novo arquivo, há apenas 2 chaves de entrada que são obrigatórias: "**fullname**" e "**platform***", todas as outras são opcionais. O arquivo deve ser construído pelo menos da seguinte forma:

```xml
<?xml version="1.0"?>
<systemList>
  <system>
    <fullname>Nintendo Entertainment System</fullname>
    <platform>nes</platform>
  </system>
  <system>
    <fullname>Family Computer Disk System</fullname>
    <platform>fds</platform>
  </system>
  <system>
    <fullname>Super Nintendo Entertainment System</fullname>
    <platform>snes</platform>
  </system>
  <system>
    <fullname>Satellaview</fullname>
    <platform>satellaview</platform>
  </system>
  [. . .]
</systemList>
```

## Adicionar um sistema "Personalizado"

> Você **NÃO deve** alterar o arquivo original `es_systems.cfg` (que está na pasta `share_init`). Se houver algum problema com as mudanças feitas posteriormente, este arquivo será a fonte para que o Recalbox funcione corretamente.
{.is-warning}

Esta modificação não adicionará um novo emulador ao Recalbox, acrescentará apenas uma nova entrada de sistema ao menu de seleção.

É possível combinar a alteração da ordem dos sistemas com a adição de um ou mais sistemas personalizados.

Assim como na mudança da ordem do sistema, a adição de um sistema personalizado só deve ser feita a partir do arquivo `es_systems.cfg` na seguinte pasta `/recalbox/share/system/.emulationstation/es_systems.cfg`.

Originalmente, este arquivo não existe. Você deve copiar o arquivo original ou criar um novo arquivo. Uma vez criado o novo arquivo, agora é possível acrescentar um novo sistema a ele.

Se um sistema no novo arquivo for incorretamente preenchido, é dada prioridade ao arquivo original. Para o novo arquivo, todas as chaves de entrada são obrigatórias. Assim, para criar um novo sistema, a maneira mais fácil é partir de um sistema existente (e correspondente as roms que você deseja incluir) e modificar apenas o que é estritamente necessário:

- _**&lt;fullname>**_ : Permite dar o nome do novo sistema.
- _**&lt;path>**_ : Permite indicar a pasta que contém as roms do novo sistema.
- _**&lt;theme>**_ : Permite indicar o tema a ser utilizado. É necessário, com antecedência, criar este novo tema (logotipo, fundo, ...)

**Todas as outras entradas não devem ser modificadas.**

Aqui está um exemplo de adição de um sistema baseado no SNES para incluir apenas as roms traduzidas:

```xml
<?xml version="1.0"?>
<systemList>
  <system>
    <fullname>Super Nintendo Traduzidos</fullname>
    <name>snes</name>
    <path>/recalbox/share/roms/snestrad</path>
    <extension>.smc .sfc .mgd .zip .7z</extension>
    <command>python /usr/lib/python2.7/site-packages/configgen/emulatorlauncher.pyc %CONTROLLERSCONFIG% -system %SYSTEM% -rom %ROM% -emulator %EMULATOR% -core %CORE% -ratio %RATIO% %NETPLAY%</command>
    <platform>snes</platform>
    <theme>snestrad</theme>
    <emulators>
      <emulator name="libretro">
        <cores>
          <core>snes9x2005</core>
          <core>snes9x2010</core>
          <core>snes9x2002</core>
        </cores>
      </emulator>
    </emulators>
  </system>
</systemList>
```