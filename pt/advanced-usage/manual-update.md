---
title: Atualização manual e/ou offline
description: Como atualizar o Recalbox manualmente e sem uma conexão com a Internet
published: true
date: 2021-09-01T06:22:46.288Z
tags: offline, atualização
editor: markdown
dateCreated: 2021-09-01T00:14:13.562Z
---

## Quando usar a atualização manual?
Pode haver momentos em que você precise atualizar manualmente seu Recalbox:
- Para atualizar um Recalbox que não tem conexão com a internet.
- Quando a equipe pede que você teste uma imagem *em desenvolvimento* para verificar a correção de um bug.
- Quando você tem um problema no sistema (muito raro).

> Os exemplos a seguir serão usados com a imagem do Raspberry Pi 4, você terá que adaptar estes exemplos à sua placa/seu hardware.
> {.is-info}

Para fazer isso, você precisará obter dois arquivos:
- O arquivo de **imagem** da Recalbox, chamado `recalbox-rpi4.img.xz`.
- O arquivo **checksum***, que contém o checksum da imagem para verificar automaticamente sua integridade. Tem o mesmo nome da imagem, mas com uma extensão `.sha1`, portanto, em nosso exemplo `recalbox-rpi4.img.xz.sha1`.


## Baixar a imagem oficial
Para atualizar seu Recalbox para a última versão disponível no modo offline, vá para download.recalbox.com/pt e selecione `Veja todas as imagens do Recalbox`:

![seeallimages.png](/advanced-usage/manual-update/seeallimages-pt.png)

Em seguida, baixe a imagem correspondente ao seu hardware/sua placa e o checksum `sha1` correspondente:

![download.png](/advanced-usage/manual-update/download.png)

## Copiar os arquivos para o cartão SD
Agora que você tem os dois arquivos `recalbox-rpi4.img.xz` e `recalbox-rpi4.img.xz.sha1`, você só precisa colocá-los na pasta `update` da partição chamada `RECALBOX` do cartão SD no qual o Recalbox está instalado:

![images-in-boot-partition.png](/advanced-usage/manual-update/images-in-boot-partition.png)

## Iniciar a instalação
A instalação será iniciada automaticamente na próxima vez que você iniciar seu Recalbox. Para verificar se a instalação foi bem sucedida, você pode abrir o menu do EmulationStation com o botão `Start` e ver a mudança de versão.
