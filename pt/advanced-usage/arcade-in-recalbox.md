---
title: O Arcade no Recalbox
description: 
published: true
date: 2021-08-27T18:50:18.981Z
tags: arcade
editor: markdown
dateCreated: 2021-08-19T00:04:40.484Z
---

![](/advanced-usage/arcade/arcade-recalbox.png)

## I - O TERMINAL ARCADE

Os anos 70 marcam o início da história dos arcades!  
Cada arcade usa um hardware específico e único, muitas vezes com vários processadores, chips de som e vídeo muito especiais, e sempre a melhor tecnologia de exibição de computador da época.  
As primeiras máquinas de arcade eram dedicadas e utilizáveis apenas para o jogo com que vieram (Pac-man, Space Invaders, Pong etc.). Quando um jogo chegava ao fim de sua vida comercial, era trabalhoso reutilizar o gabinete.

Os sistemas Arcade em 1986 evoluíram e eram freqüentemente baseados em uma placa-mãe acoplada a um cartucho de jogo ou um conector [**PCB**](https://pt.wikipedia.org/wiki/Circuito_impresso) (conexão [**JAMMA**](https://pt.wikipedia.org/wiki/JAMMA)).  
As máquinas arcade então se tornaram genéricas e o hardware se tornou mais como um console de videogame doméstico ou um computador de alta tecnologia.  
Isto reduziu os custos de produção e os tempos de criação e facilitou a vida dos operadores de arcade.

Entretanto, ainda há muitos sistemas de arcade que ainda utilizam hardware específico otimizado para um único jogo: jogos de carro, jogos com armas óticas (*lightguns*), jogos de dança...

## II - MAME

[**MAME**](https://pt.wikipedia.org/wiki/MAME) (***M**ultiple **A**rcade **M**achine **E**mulator* - Emulador de Múltiplas Máquinas de Arcade) é um sistema de emulação polivalente.

O objetivo do MAME é preservar décadas de história do software.  
Como a tecnologia eletrônica continua a avançar, MAME evita que este importante software "vintage" seja perdido e esquecido.  
Isto é conseguido documentando o hardware e seu funcionamento.

### 1 - A Documentação MAME

O fato da documentação ser utilizável serve principalmente para validar sua precisão (de que outra forma você pode provar que recriou fielmente o hardware?).  
Com o tempo, MAME absorveu o projeto irmão [**MESS**](https://pt.wikipedia.org/wiki/MESS) (*Multiple Emulator Super System*) em 2015. Assim, MAME agora documenta uma grande variedade de computadores (a maioria retro), consoles de videogame, calculadoras, além dos videogames arcade que eram seu foco original.

A primeira versão pública (0.1) do MAME, de Nicola SALMORIA, foi lançada em 5 de fevereiro de 1997.  
Em 21 de outubro de 2010, a versão 0.140 do emulador já suportava mais de 4.510 jogos.  
Em 4 de abril de 2020, a versão 0.220 suportava mais de 6.000 jogos e computadores.

Como o MAME é um emulador em constante evolução, é necessário evoluir seu romset em paralelo com a versão do emulador.

### 2 - Princípio da documentação MAME

MAME documenta esta fabulosa biblioteca de máquinas usando o princípio [**DUMP**](https://pt.wikipedia.org/wiki/Dump_de_banco_de_dados).
O *dump* consiste em transferir os dados contidos nos diversos componentes (placa-mãe, PCB, cartuchos) para um formato digital a ser usado com emuladores.

![](/advanced-usage/arcade/board-to-zip.png)

Para algumas máquinas mais ou menos recentes, esta documentação é acompanhada por um CD-ROM, DVD ou mesmo um cassete chamado CHD (Compressed Hunks of Data).  
Sem este CHD a emulação será imperfeita ou não começará de forma alguma.

![](/advanced-usage/arcade/rom-and-chd.png)

Esta documentação nem sempre é perfeita e isto infelizmente resulta em versões "BAD DUMP" de alguns jogos e, portanto, imperfeita ou impossível de emular.

## III - ROMSETS

### 1 - Conteúdo de um ROMSET

Como explicado acima, há muito para ser absorvido em seus ROMSETs.  
De fato, o que querermos documentar de "vintage" e todas as memórias mortas do planeta nos leva a fazer a pergunta: o que temos em um ROMSET?

Graças ao software [**CLRMAMEPRO**](./../tutorials/utilities/rom-management/clrmamepro) é possível ler todo o conteúdo de um ROMSET tendo uma referência: o arquivo [**.dat**](https://pt.wikipedia.org/wiki/MAME#*.DAT).
Este arquivo reúne a lista completa dos dados que constituem o ROMSET em relação ao emulador.  
_**É importante comparar seu ROMSET com este arquivo .dat via CLMAMEPRO e evitar escolher ROMs aleatoriamente por problemas de compatibilidade!**_  
  
No MAME, encontraremos as seguintes famílias:

* **STANDARD:** Esta é a maior parte do ROMSET (55GB). Você vai encontrar jogos arcade, máquinas de pinball, consoles, caça-níqueis, calculadoras, até mesmo tamagotchis... 
* **MECHANICAL:** são máquinas difíceis de emular porque são metade mecânicas / metade digitais. 
* **DEVICE:** "hardware" são arquivos de configuração que vêm com algumas máquinas para emulação. 
* **BIOS:** Alguns sistemas são classificados pela BIOS (cerca de 70).

### 2 - BIOS / Drivers

Algumas ROMs de jogos em um ROMSET podem exigir arquivos BIOS ou drivers, sendo o caso mais famoso os jogos [**Neo-Geo**](https://pt.wikipedia.org/wiki/Neo_Geo). 

* Exemplo **Neo-Geo**: se você quiser usar os jogos Neo-Geo, você terá que copiar o arquivo da "BIOS" (neste caso neogeo.zip) para a mesma pasta que o jogo. 
* Exemplo [**CPS**](https://pt.wikipedia.org/wiki/CPS-1): se você quiser usar alguns jogos CPS, você terá que copiar o arquivo "driver" (neste caso qsound_hle.zip) para a mesma pasta que o jogo. 

É claro que se você usar subpastas para seus jogos (por exemplo: gênero ou hardware), você terá que copiar os arquivos de BIOS e drivers em cada pasta que contenha jogos que os exijam.
Como estes arquivos BIOS e drivers são bastante pequenos em tamanho, é mais fácil copiá-los todos em cada uma de suas pastas.

Você se pergunta: Onde encontrar esses arquivos de BIOS e drivers?
Bem, é muito simples: em seu ROMSET!

### 3 - O CHD

Assim como uma BIOS ou um DRIVER, o CHD deve acompanhar sua ROM, mas tenha cuidado porque o formato e a disposição deles é bastante específica.

O arquivo CHD (descomprimido) não tem o mesmo nome da ROM, mas deve ser armazenado em uma pasta (descomprimido) com o nome exato da ROM para funcionar.
Estes arquivos CHD raramente ficam junto dos ROMSETs porque são grandes.

**Exemplo de ROM+CHD: sfiii3**

![](/advanced-usage/arcade/roms-chd.png)

### 4 - Tipos de arquivos em um ROMSET

* **PARENT ("ROM Pai"):**  A ROM do jogo original que a equipe MAME decidiu nomear como "versão original". Com exceção dos arquivos ROM da BIOS, os arquivos ROM para estes jogos contêm tudo o que é necessário para que o jogo funcione. O conjunto original é considerado como a revisão mais recente e se várias versões regionais estiverem disponíveis, a revisão mundial ou americana é utilizada.
* **CLONES ("Rom clone"):** A ROM é uma versão ou variante diferente do mesmo jogo (por exemplo, Street Fighter II Turbo é considerado uma variante da Street Fighter II Champion Edition). Há também versões: Bootleg / homebrew / hack...
* **BIOS:** uma ROM comum utilizada por todos os jogos na mesma máquina (como o Neo-Geo MVS). Estes são os programas utilizados quando a máquina é ligada, permitindo que as operações básicas sejam realizadas quando a máquina é ligada.

### 5 - Tipo de romsets: split (divididos) / merged (mesclados) / non-merged (não mesclados)

Há três tipos de romsets:

* **split (divididos):** arquivos em comum entre ROM pai (PARENT) e ROM clone (CLONES) estão apenas no zip dos pais, então para usar um clone você precisa ter tanto os ROMs dos pais quanto os dos clones.
* **non-merged (não mesclados):** todos os arquivos necessários para o clone estão no zip do clone.
* **merged (mesclados)** pai e clone são fundidos no mesmo zip.

**Abaixo está um exemplo de ROM nos diferentes tipos de romsets:** em vermelho, os componentes da ROM pai (Parent), em amarelo, os componentes das ROMs clones (Clones), e em branco, os componentes usados para o funcionamento do jogo.  
Observe que no caso do ROMSET **split** a pasta **clone** (ROM clone) não funcionará sem a pasta **parent** (ROM pai).

![](/advanced-usage/arcade/rom-types.jpg){.full-width}

## IV - O ARCADE E O RECALBOX

Os diferentes emuladores no **Recalbox** referem-se a **diferentes emuladores MAME** suportados por eles.

_**É importante comparar seu ROMSET com um arquivo .dat via clrmamepro e evitar a escolha aleatória das ROMs por questões de compatibilidade.**_

### 1 - Mame

* **LIBRETRO-MAME 2003:** Baseado no **ROMSET 0.78** foi lançado em 25 de dezembro de 2003.
* **LIBRETRO-MAME 2003-Plus:** (também chamado de MAME 2003+ e mame2003-plus) é um núcleo de emulador de sistema de Arcade Libretro que se concentra em alto desempenho e ampla compatibilidade com dispositivos móveis, computadores de placa única, sistemas integrados e outras plataformas similares.
  Baseado no **ROMSET 0.78-0.188** (MAME 0.78 como linha de base com outras ROMs baseadas em conjuntos posteriores de ROMs MAME).
* **LIBRETRO-MAME 2010:**  baseado no **ROMSET 0.139** foi lançado em 29 de julho de 2010.
* **LIBRETRO-MAME 2015:** baseado no **ROMSET 0.160** foi lançado em 25 de março de 2015.
* **LIBRETRO-MAME:** baseado no **ROMSET 0.230** foi lançado em 25 de março de 2015.

### 2 - FbNeo

* **LIBRETRO-FBNEO:** baseado no **ROMSET FBNeo 1.0.0.01**

### 3 - Neo-Geo

* **LIBRETRO-FBNEO:** baseado no **ROMSET FBNeo 1.0.0.01**

### 4 - Naomi

* [**Libretro-Flycast**](./../emulators/arcade/naomi/libretro-flycast#romset-mame): baseado no **ROMSET 0.230**

### 5 - Naomi GD

* [**Libretro-Flycast** ](./../emulators/arcade/naomi-gd-rom/libretro-flycast#romset-mame): baseado no **ROMSET 0.230**

### 6 - Atomiswave

* [**Libretro-Flycast**](./../emulators/arcade/atomiswave/libretro-flycast#romset-mame): baseado no **ROMSET 0.230**


## V - PROBLEMAS CONHECIDOS

| Tipo de problema | ROM RUIM | EMULADOR ERRADO | CHD AUSENTE | BAD DUMP | MAPEAMENTO RUIM |
| :--- | :---: | :---: | :---: | :---: | :---: |
| **Meu jogo não inicia mais**  | ✅ | ✅ | ✅ | ✅ |  |
| **Problema de som no jogo** |  | ✅ |  | ✅ |  |
| **O jogo apresentas bugs e glitches**  |  | ✅ |  | ✅ |  |
| **O jogo inicia, mas os botões estão errados** |  |  |  |  | ✅ |

## VI - GLOSSÁRIO

**A**

* **AtomisWave:** Sistema arcade criado por Sammy com uma arquitetura similar à Naomi da Sega.

**B**

* **Boot:** refere-se à fase de inicialização de um sistema, como o de uma placa de Arcade ou de um console.
* **Bootleg:** cópias não oficiais de jogos em PCB ou cartucho, geralmente vendidos a um preço muito mais baixo do que os originais, facilmente reconhecíveis porque quase nunca há a marca da editora de jogos no pcb ou cartucho, ao contrário dos originais. É comum que os piratas sofram com vários bugs como falta de música, falta de efeitos sonoros, congelamentos aleatórios...

**C**

* **Chihiro:** nome de um sistema de arcade lançado pela SEGA em 2002. A arquitetura desta plataforma é baseada na do console Xbox da Microsoft. Entre os jogos publicados, os mais populares são: Outrun 2, House of the Dead 3 e Virtua Cop 3.
* **CPS 1:** acrônimo para Capcom Play System 1, o nome de um sistema de arcade criado pela Capcom e lançado em 1988. Entre os jogos publicados sobre este sistema, Street Fighters II é um dos mais famosos!
* **CPS 2:** acrônimo para Capcom Play System 2 é o nome de um sistema de arcade criado pela Capcom e lançado em 1993. Entre os jogos lançados estão: Gigawing, Marvel Vs Capcom, a série Street Fighters Zero ou Alpha, Vampire Savior ou Progear no Arashi.
* **CPS 3:** acrônimo para Capcom Play System 3. Nome de um sistema de arcade criado pela Capcom e lançado em 1996. Este sistema é muito poderoso no gerenciamento de gráficos 2D. Lançados neste sistema são: Jojo's Bizarre Adventure, Warzard, e a série Street Fighters III.  
* **Crystal System:** [sistema arcade](https://pt.wikipedia.org/wiki/Arcade) em formato JAMMA lançado em 2001 e fabricado e comercializado pela empresa BrezzaSoft. O Crystal System foi criado para substituir o sistema Neo-Geo no mercado de jogos arcade baratos. Deve-se lembrar que quase todos os funcionários que trabalham na BrezzaSoft são desertores da SNK, tendo deixado a empresa logo após a falência para criar a BrezzaSoft. Este sistema oferece jogos com gráficos 2D muito avançados e agradáveis. O Sistema Crystal não teve o sucesso esperado e foi um desastre comercial, teve um tempo de vida muito curto. Muito poucos jogos foram publicados neste novo sistema, mas podemos notar jogos como o Evolution Soccer, um jogo de futebol copiado de Super Sidekicks, e o Crystal of the Kings, um Beat-them-all medieval,no estilo de Golden Axe ou Knights of the Round.

**D**

* **DECO Cassette System:** [sistema arcade](https://pt.wikipedia.org/wiki/Arcade) desenvolvido pela Data East. Lançado em 1980, foi o primeiro sistema de arcade que permitiu ao proprietário do terminal mudar os jogos. O comprador tinha que comprar o gabinete de arcade, enquanto os jogos estavam disponíveis em cassetes de áudio. Bastava então inserir na cabine o cassete e um dongle de segurança (uma das primeiras formas de gerenciamento de direitos digitais, impedindo a cópia).
* **Dump / Dumping / Dumpar / Dumper:** uma técnica que consiste em transferir os dados contidos na(s) ROM(s) de um jogo arcade para o disco rígido de um computador (imagem ROM) a ser disponibilizado na Internet e utilizado em emuladores como o MAME.

**E**

* **Emulação:** o processo de reprodução do comportamento de outro dispositivo em um computador ou console, usando o que se chama de emulador. Por exemplo, o emulador Kawaks permite que você jogue Neo-Geo em seu PC.

**H**

* **Hyper Neo-Geo 64:** foi criado pela empresa SNK e lançado em setembro de 1997, como um sucessor do então sistema Neo-Geo MVS de sete anos. Tecnicamente, como seu nome sugere, o Hyper Neo-Geo 64 está equipado com um microprocessador de 64 bits que permite jogar jogos 3D, exibindo 4096 cores simultaneamente a partir de uma paleta de 16,7 milhões de cores, em uma resolução de 640 x 480 pixels. Entre os 7 jogos lançados: Fatal Fury, Wild Ambition, Samurai Shodown 64, Samurai Shodown 64, Warriors Rage...

**I**

* **Imagem ROM**: cópias de ROMs (Read Only Memory), os componentes eletrônicos encontrados em cartuchos e cartões arcade que contêm o jogo. Copiar um jogo arcade para o disco rígido de um PC é chamado de "dumpar" um jogo e requer hardware especial. As cópias/imagens dos roms são então colocadas na Internet como arquivos .zip que normalmente são lidos por emuladores, e depois baixados por você e por mim. Por uma questão de simplicidade, as imagens ROM são simplesmente chamadas de "roms" pela maioria dos locais de emulação.

**J**

* **JAMMA:** um padrão para todos os jogos arcade para trabalhar em todas as máquinas arcade, criado em meados dos anos 80 pela Associação Japonesa de Fabricantes de Máquinas de Diversão. Tecnicamente, o padrão define conexões para: dois joysticks de oito direções, dois botões de start e seis botões de controle (três por jogador), bem como suporte de som (mono).

**K**

* **Konami Sytem 573:** O Sistema 573 é uma série de placas de sistema de arcade da Konami baseadas no Playstation original. O hardware foi usado principalmente para a série de videogames Benami da Konami, mais comumente a série Dance Dance Revolution, introduzida em 1998.
* **Konami Viper:** A Konami lançou o Konami Viper em 1998 para as arcades. Este arcade utiliza o sistema JAMMA+PCB. Entre os 13 jogos lançados: Jurrassic Park 3, GTI club 2, Silent Scope EX, Thrill Drive II...

**L**

* **Lindbergh:** nome de um sistema de arcade criado pela SEGA e lançado em 2006. A arquitetura e os componentes deste sistema são similares aos de um PC, em contraste com outros sistemas criados anteriormente pela SEGA. Entre os jogos lançados em Lindbergh estão Virtua Fighters 5, Virtua Tennis 3, House of The Dead 4, After Burner Climax... Lindbherg é também o nome de um dos pioneiros da aviação, o americano Charles Lindbergh (1902-1974). Ele foi a primeira pessoa a voar sozinho através do Oceano Atlântico.

**M**

* **MAME:** famoso emulador de arcade que emula mais de 6000 sistemas de arcade. MAME também é considerado um museu virtual de jogos arcade.
* **Model 1:** nome de um sistema arcade criado pela SEGA e lançado em 1992. Um verdadeiro avanço tecnológico, foi neste sistema que Virtua Racing e Virtua Fighter 1 foram lançadas.
* **Model 2:** nome de um sistema arcade criado pela SEGA e lançado em 1993. Alguns dos jogos mais populares lançados neste sistema são Daytona USA, House of the Dead, Sega Rally CHampionship e Virtua Fighter 2.
* **Model 3:** nome de um sistema arcade criado pela SEGA lançado em 1996 que se tornou o sistema mais poderoso de seu tempo. Alguns dos jogos mais populares lançados neste sistema são Daytona USA 2, Scud Race, Sega Rally 2 e Virtua Fighter 3.

**N**

*  **Namco System 246:** O Sistema 246 é um sistema de videogame para arcades baseado no Playstation 2. Foi criado pela Namco em 2001 e passou por várias evoluções: o System 256, o Super System 256 e o System 147.
* **Naomi e Naomi GD:** cartucho e sistema de videogame GD-ROM para terminais de arcade, lançado em 1998 como o sucessor do sistema Sega Model 3. Um acrônimo para New Arcade Operation Machine Idea (literalmente "nova idéia de máquina de arcade"), Naomi também se traduz como "beleza" em japonês.
* **Neo-geo MVS:** (Multi Video System) é um sistema de videogame de arcade compatível com JAMMA para arcade criado pela empresa japonesa SNK em 1990.
* **Nintendo Playchoice10 :** Com base no Nintendo Entertainment System (NES), a placa-mãe Playchoice 10 permitiu que até 10 ROMs fossem instaladas de cada vez. Ao contrário da maioria dos outros jogos arcade, as moedas acrescentam tempo em vez de créditos, permitindo que você troque os jogos quando quiser.

**P**

* **Placa mãe:** frequentemente se refere ao meio que contém todos os componentes de computação e que é necessária para executar os jogos que devem ser instalados nela. Exemplo com o MVS ou outros sistemas de cartucho ou disco: as unidades de computação, processadores de som ou gráficos, etc. (também chamados de hardware) estão concentrados na placa-mãe enquanto o jogo (software) está contido no cartucho ou disco.
* **PCB:** iniciais para Printed Circuit Board ("Placa de Circuito Impresso"). Uma placa projetada para conter componentes eletrônicos. No arcade, um PCB é uma placa de circuito impresso que contém um jogo e é conectado ao terminal de arcade através do conector JAMMA. 
* **PGM:** Inicial para "Poly Game Master" desenvolvido pela IGS (International Games System). Este sistema arcade é similar ao MVS em seu desempenho, aparência e uso.
* **Pinball:** Também conhecido como flipper.

**R**

* **ROM:** um acrônimo para "**Read Only Read Memory**".

**S**

* **Sega Hikaru:** nome de um sistema criado pela empresa Sega em 1999. O Hikaru foi originalmente projetado para abrigar o jogo Brave Fire Fighters. Ele tem a capacidade de exibir gráficos complexos de fogo e água na tela. Foi o primeiro sistema de arcade a apresentar na tela o sombreamento Phong. Este sistema tinha apenas seis jogos e logo foi abandonado por ser muito caro para funcionar, especialmente em comparação com o Naomi2.
* **Sega Mega Play:** Sistema Arcade baseado no console doméstico Sega Megadrive. Quando você insere moedas, você compra créditos como um jogo arcade padrão.
* **Sega Mega-Tech:** sistema arcade baseado no console doméstico Sega Megadrive. Quando você insere uma moeda, você ganha tempo, o jogo termina quando seu tempo acaba, você pode adicionar moedas extras durante o jogo para ganhar tempo extra. 
* **Set:** Em linguagem arcade um set=jogo ou BIOS que consiste em múltiplas ROMs. 
* **ST-V Titan** (acrônimo para Sega Titan Video): o sistema de arcade da Sega lançado em 1994, com a mesma arquitetura do console Sega Saturn (exceto que o ST-V usava cartuchos). Um dos objetivos era facilitar a vida dos programadores, tornar as conversões para console muito mais rápidas e obter as conversões mais fiéis possíveis.

**T**

* **Triforce:** nome de um sistema arcade desenvolvido conjuntamente pela Nintendo, SEGA e Namco e lançado em 2002. A arquitetura deste sistema é baseada na do console GameCube da Nintendo. Entre os jogos lançados, os mais populares são: F-Zero AX, Virtua Striker 4 e Mario Kart Arcade GP. Nota: o nome Triforce representa a aliança entre as 3 empresas, mas também é uma referência à série The Legend Of Zelda.

## VII - Fontes:

* [http://www.system16.com/](http://www.system16.com/)
* [https://pt.wikipedia.org/wiki/Wikip%C3%A9dia:P%C3%A1gina_principal](https://pt.wikipedia.org/wiki/Wikip%C3%A9dia:P%C3%A1gina_principal)


