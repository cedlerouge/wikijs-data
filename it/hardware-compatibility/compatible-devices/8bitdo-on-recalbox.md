---
title: 8bitdo su Recalbox
description: 
published: true
date: 2021-06-19T15:31:44.208Z
tags: 
editor: markdown
dateCreated: 2021-06-19T15:31:44.208Z
---

![8bitdo](http://support.8bitdo.com/images/Logo-black.svg)

![Controller in produzione 8Bitdo al 13 novembre 2019](/compatibility/8bitdocollection.png)

## Controller Supportati 8Bitdo

### Controller stile SNES :

| Controller | FW (min / rec*) | USB | BT | Accensione | Associazione BT* | Reinizializzazione BT | Ripristino impostazioni di fabbrica |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| SN30 Pro+ | ?? / 4.01 | ✅ | ☑️ | START | Premere il tasto PAIR per 2 secondi | Premere il tasto PAIR per 2 secondi | [Controlla il manuale sul sito 8Bitdo](https://download.8bitdo.com/Manual/Controller/SN30-Pro+/SN30_Pro+_Manual.pdf) |
| SN30 Pro v2.0 | ? / 1.32 | ✅ | ☑️ | START + B | Premere il tasto PAIR per 3 secondi | Premere il tasto Select per 3 secondi | [Controlla il manuale sul sito 8Bitdo](https://download.8bitdo.com/Manual/Controller/SN30pro+SF30pro/SN30pro_Manual.pdf) |
| SN30 & SF30 Pro | 1.23 / 1.33 | ✅ | ☑️ | START + B | Premere il tasto PAIR per 3 secondi | Premere il tasto Select per 3 secondi | [Premere il tasto Select per 8 secondi](https://download.8bitdo.com/Manual/Controller/SN30pro+SF30pro/SN30pro+SF30pro_Manual.pdf) |
| SN30 Pro USB | ? / 1.03 | ? | n/a | n/a | n/a| n/a | [Controlla il manuale sul sito 8Bitdo](https://download.8bitdo.com/Manual/Controller/SN30pro-USB/SN30-Pro-USB.pdf) |
| SN30 GP | ? / 6.11 | ? | ? | START | Premere il tasto Select per 3 secondi | Premere il tasto Select per 3 secondi | [Controlla il manuale sul sito 8Bitdo](https://download.8bitdo.com/Manual/Controller/SN30+SF30/SN30_Manual.pdf) |
| SN30 Retro Set | ? / 4.10 | ? | ? | START | Premere il tasto Select per 3 secondi | Premere il tasto Select per 3 secondi | [Controlla il manuale sul sito 8Bitdo](https://download.8bitdo.com/Manual/Controller/SN30+SF30/SN30_Analogue.pdf) |
| SN30 | 2.62 / 4.20 | ✅ | ☑️ | START + R | Premere il tasto Select per 3 secondi | Premere il tasto Select per 3 secondi | [Premere il tasto Select per 8 secondi](https://download.8bitdo.com/Manual/Controller/SN30+SF30/SN30_Analogue.pdf) |
| SF30 &amp; SFC30 | 2.70 / 4.20 | ✅ | ☑️ | START + R | Premere il tasto Select per 3 secondi | Premere il tasto Select per 3 secondi | [Premere il tasto Select per 8 secondi](https://download.8bitdo.com/Manual/Controller/SN30+SF30/SN30+SF30_Manual_V4.pdf) |

### Controller stile SNESxNES :

| Controller | FW (min / rec*) | USB | BT | Accensione | Associazione BT* | Reinizializzazione BT | Ripristino impostazioni di fabbrica |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| N30 Pro 2 | ? / 6.10 | ? | ? | START | Premere il tasto PAIR per 1 secondo | Premere il tasto PAIR per 3 secondi | [Controlla il manuale sul sito 8Bitdo](https://download.8bitdo.com/Manual/Controller/N30pro2/N30pro2-Manual.pdf) |
| N30 &amp; NES30 Pro | 1.62 / 4.10 | ✅ | ☑️ | POWER | Premere il tasto PAIR per 1 secondo | Premere il tasto PAIR per 3 secondi | [Premere il tasto START per 8 secondi](https://download.8bitdo.com/Manual/Controller/N30pro+F30pro/N30pro+F30pro_Manual_V4.pdf) |
| F30 &amp; FC30 Pro | 1.62 / 4.10 | ✅ | ☑️ | POWER + X | Premere il tasto PAIR per 1 secondo | Premere il tasto PAIR per 3 secondi | [Premere il tasto START per 8 secondi](https://download.8bitdo.com/Manual/Controller/N30pro+F30pro/N30pro+F30pro_Manual_V4.pdf) |

### Controller stile NES :

| Controller | FW (min / rec*) | USB | BT | Accensione | Associazione BT* | Reinizializzazione BT | Ripristino impostazioni di fabbrica |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| N30 NS | ? / 6.10 | ? | ? | START | Premere il tasto Select per 3 secondi | Premere il tasto Select per 3 secondi | [Controlla il manuale sul sito 8Bitdo](https://download.8bitdo.com/Manual/Controller/N30-NS/N30-NS.pdf) |
| N30 &amp; NES30 | 2.62 / 4.20 | ✅ | ☑️ | START + R | Premere il tasto Select per 3 secondi | Premere il tasto Select per 3 secondi | [Premere il tasto Select per 8 secondi](https://download.8bitdo.com/Manual/Controller/N30+F30/N30+F30_Manual_V4.pdf) |
| F30 &amp; FC30 | 2.62 / 4.20 | ✅ | ☑️ | START + R | Premere il tasto Select per 3 secondi | Premere il tasto Select per 3 secondi | [Premere il tasto Select per 8 secondi](https://download.8bitdo.com/Manual/Controller/N30+F30/N30+F30_Manual_V4.pdf) |

### Controller stile MEGADRIVE :

| Manette | FW (min / rec*) | USB | BT | Accensione | Associazione BT* | Reinizializzazione BT | Ripristino Impostazioni di fabbrica |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| M30 | 1.13 / 1.14 | ❌ | ☑️ | START | Premere il tasto Select 1 secondo | Premere il tasto Select 3 secondi | [Controlla il manuale sul sito 8Bitdo](https://download.8bitdo.com/Manual/Controller/M30/M30_Manual.pdf) |

### Controller stile ARCADE/CABINATO :

| Controller | FW (min / rec*) | USB | BT | Accensione | Associazione BT* | Reinizializzazione BT | Ripristino impostazioni di fabbrica |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| N30 &amp; NES30 Arcade Stick | 1.41 / 5.01 | ✅ | ☑️ | HOME | Premere il tasto Select per 3 secondi | Premere il tasto Select per 3 secondi | [Controlla il manuale sul sito 8Bitdo](https://download.8bitdo.com/Manual/Controller/Joystick/N30-Arcade-Manual.pdf) |
| F30 & FC30 Arcade Stick | 1.42 | ✅ | ☑️ | HOME | Premere il tasto Select per 3 secondi | Premere il tasto Select per 3 secondi | [Controlla il manuale sul sito 8Bitdo](https://download.8bitdo.com/Manual/Controller/Joystick/F30-ArcadeJoystick-eng.pdf) |

### Altri stili di controller :

| Controller | FW (min / rec*) | USB | BT | Accensione | Associazione BT* | Reinizializzazione BT | Ripristino impostazioni di fabbrica |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| Lite | ?? / 1.20 | ? | ? | HOME | Premere il tasto PAIR per 2 secondi | Premere il tasto PAIR per 1 secondo | [Controlla il manuale sul sito 8Bitdo](https://download.8bitdo.com/Manual/Controller/Lite/Lite_Manual.pdf) |
| Zero | n/a | ❌ | ☑️ | START + R1 | Premere il tasto Select per 3 secondi | Premere il tasto Select per 3 secondi | [Premere il tasto Select per 8 secondi](https://download.8bitdo.com/Manual/Controller/ZERO/ZERO_Manual_V4.pdf) |

* FW = Firmware
* min = Minimo
* rec = Raccomandato
* BT = Bluetooth

## Dongles/Chiavette 8Bitdo Supportati

| Dongle | FW (min / rec*) | Compatibilita con Recalbox | Controller Compatibili |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| USB Adapter for PS Classic (in X-Input Mode) | 1.28 / 1.33 | ✅ | PS3, PS4, Xbox One S, Wii Mote, Wii U Pro, Switch Joycons, Switch Pro, M30, SFC30, SF30 Pro. |
| USB Adapter Receiver (in X-Input Mode) | 1.28 / 2.00 | ✅ | PS3, PS4, PS5, Xbox One S, Xbox One X, Wii Mote, Wii U Pro, Switch Joycons, Switch Pro, M30, SFC30, SF30 Pro. |
| GBros Adapter Emeter (With 8Bitdo Receiver) | 2.23 / 2.24 | ✅ | GameCube, GameCube Wavebrid (avec le câble d'extension), Wii Classic, Wii Classic Pro, NES Mini, SNES Mini. |
| NES & SNES Mini Receiver | ? / 1.14 | ? | ? |
| SNES Receiver | ? / 1.34 | ? | ? |

## Support 8Bitdo

* [[Firmware] Firmware 8Bitdo](https://support.8bitdo.com/).
* [[Firmware] Firmware 8Bitdo in versione beta e applicazioni per tester](http://forum.8bitdo.com/forum.php?mod=viewthread&tid=1179). 
* [[Ultimate Software] (Windows / macOS) Ottieni il massimo controllo su ogni parte del tuo controller: personalizza la mappatura dei pulsanti, regola gli stick e la loro sensibilità, controlla le vibrazioni e crea macro con qualsiasi combinazione di pulsanti. Crea profili controller e passa da uno all'altro in qualsiasi momento. Solo per il modello SN30 Pro+](https://support.8bitdo.com/ultimate-software.html). 
* [Utility di aggiornamento (Windows / macOS) Riconoscimento automatico del controller. Scarica l'aggiornamento del firmware quando sei connesso a Internet. Aggiornamento manuale del firmware in modalità avanzata (per firmware beta).](https://support.8bitdo.com/firmware-updater.html) 
* [[XPad] (Windows / macOS) Attiva la modalità tastiera. Aggiunge la modalità X-input al controller. Consente di utilizzare più controller contemporaneamente.](https://support.8bitdo.com/xpad.html) 
* [[Applicazione 8BitDo] (Android, Apple, Microsoft) Attiva la modalità touch. Non sono necessari root per la mappatura dei pulsanti. Connessione in un solo passaggio.](https://support.8bitdo.com/8bitdo-app.html)

[Utility di associazione PS3] (Windows / macOS) Connetti il tuo controller PS3 ai dongle 8BitDo.

I controller 8Bitdo vanno disassociati al Recalbox prima di associarli nuovamente. Se hai un problema di associazione, puoi provare il metodo "Ripristino Bluetooth" e, se non funziona, puoi provare il metodo "Ripristino impostazioni di fabbrica".

Puoi trovare il manuale per ciascuno dei tuoi controller sul sito di [supporto 8Bitdo](https://support.8bitdo.com/).

## FAQ / Domande frequenti

### Come ricaricare il mio controller?

Utilizzare preferibilmente la porta USB del computer o della console per caricare il controller.
Se utilizzi un caricabatterie convenzionale o un caricabatterie per telefono, assicurati che non superi mai i 5V 1A.
Un'intensità troppo elevata può danneggiare la batteria e rendere il controller inutilizzabile in Bluetooth.

### Come aggiornare il Firmware?

Accedi alla pagina [support 8Bitdo](https://support.8bitdo.com/) e scarica il firmware più aggiornato (nella scheda Firmware). Oppure scarica il programma [8bitdo Firmware Updater](https://support.8bitdo.com/firmware-updater.html) che renderà il processo più automatizzato.

### {.tabset}
#### Windows

1. Vai alla cartella in cui hai scaricato il firmware.
2. Decomprimi il file .zip scaricato.
3. Vai alla cartella appena decompressa (**non nel file .zip**).
4. Apri il file 8Bitdo_Update.exe nella cartella decompressa (il programma chiederà la posizione del file .dat)
5. Trovare il file .dat (digitare Firmware _ *** pad-name*** _ V***. Dat) nella radice della cartella.
6. L'aggiornamento si avvierà e ti chiederà di connettere il controller con una combinazione di tasti specifica.
7. Accendere il pad con la combinazione richiesta (ad esempio R1 + R2 + Start premuti contemporaneamente)
8. Il pulsante disattivato e inaccessibile diventa accessibile.
9. Premere il pulsante Aggiorna, il firmware si aggiornerà e indicherà quando è possibile scollegare il controller.

#### macOS

1. Vai alla cartella in cui hai scaricato il firmware.
2. Decomprimi il file .zip scaricato.
3. Vai alla cartella appena decompressa (**non nel file .zip**).
4. Apri il file 8Bitdo_Update.exe nella cartella decompressa (il programma chiederà la posizione del file .dat)
5. Cercare il file .dat (digitare Firmware _ *** pad-name*** _ V***. Dat) nella radice della cartella.
6. Il programma di aggiornamento si avvia.
7. Il programma chiede di collegare il pad con una combinazione di tasti specifica.
8. Accendere il pad con la combinazione richiesta (ad esempio R1 + R2 + Start premuti contemporaneamente)
9. Il pulsante disattivato e inaccessibile diventa accessibile.
10. Premere il pulsante Aggiorna, il firmware si aggiornerà e indicherà quando è possibile scollegare il pad.

### Come accendere/collegare correttamente il mio controller?

A seconda del dispositivo (Windows, Mac OS, Android, Switch, ...), ci sono vari modi per accendere il controller 8bitdo:

* Y + Start = Modalità Switch
* A + Start = Modalità macOS
* X + Start = Modalità PC
* B + Start = Modalità Bluetooth (comunemente consigliata per Android)

**!! Tenere premuti contemporaneamente i tasti fino all'accensione del controller!!** Successivamente eseguire l'associazione.
**In caso di connessione tramite USB**, tenere premuti i tasti contemporaneamente fino all'accensione del controller, quindi collegare il cavo USB.

### Come collego il mio controller a un altro dispositivo?

Il controller ricorda solo l'indirizzo di un dispositivo esterno. Se vuoi passare dal tuo Switch ad Android, ad esempio, dovrai reimpostare i dati di associazione del controller. A seconda del modello, la procedura varia leggermente.

FC30 / NES30 / SNES30 / SFC30 : Tenere premuto il pulsante SELECT per più di 8 secondi. I due led verde e blu lampeggiano.

NES30 PRO (V1) : Premere il pulsante di destra (a sinistra di ON).

SN30 / SN30 PRO / M30 : Premere il pulsante di sincronizzazione per più di 3 secondi (vicino alla porta USB).

### Adattatore USB + controller PS3

**Scarica l'utility per il controller PS3.**

### {.tabset}
#### Windows
http://download.8bitdo.com/Tools/Receiver/8Bitdo_PS3_Tool_Win_V1.1.zip

**Se hai un problema con Windows:**

**Soluzione 1 :**  
Installa SixaxisPairToolSetup-0.3.0.exe  
Puoi scaricarlo da: http://sixaxispairtool.en.lo4d.com/download  
  
**Soluzione 2 :**  
Disabilita temporaneamente l'applicazione della firma del driver di Windows
Troverai immagini di aiuto nella cartella: "Come disabilitare temporaneamente l'applicazione della firma del driver su Win8 e Win10" contenuto nel 8Bitdo_PS3_Tool_Win_V*.*.zip

Se altri problemi persistono: installa il driver 'libusb-win32'.  
  
**Nota:** L'applicazione della firma del driver di Windows può essere riattivata durante un aggiornamento o altro.

#### macOS
http://download.8bitdo.com/Tools/Receiver/8Bitdo_PS3_Tool_MacOS_V1.0.zip  

### Arcade Stick

Windows modalità XINPUT.  
Android modalità DINPUT.  
MacOS e Switch non necessarie.  
  
Procedura di accensione:

1. Se necessario, selezionare la modalità XINPUT o DINPUT.
2. Premere il pulsante PAIR per inizializzare il dispositivo (il led blu lampeggia).
3. Configura il controller sul tuo dispositivo (Windows, macOS, Android o Switch).
4. Il LED dovrebbe diventare blu fisso e il controller è pronto per l'uso.

**!! Tenere premuti contemporaneamente i tasti fino all'accensione della levetta!!** Successivamente eseguire l'associazione.
**In caso di connessione tramite USB**, tenere premuti i tasti contemporaneamente fino all'accensione del controller, quindi collegare il cavo USB.

### Problemi di connessione alla Nintendo Switch?

Il tuo firmware è aggiornato, stai usando Y + START ma non riesci a connettere il controller allo Switch.
Puoi riavviare la console Switch. Questa azione ha l'effetto di completare alcuni aggiornamenti incompleti sullo Switch e risolve il problema il 90% delle volte.

### Firmware consigliati su Recalbox?

FC30 / NES30 : 4.1

SNES30 / SFC30 : 4.1

FC30 PRO / NES30 PRO (V1) : 4.1

SNES30 PRO / SFC30 PRO / SN30 PRO : 1.30

Arcade N30 : 5.01

SN30 GP ed. : 6.1

NES30 PRO 2 : 6.1

### Pad zero V1 e Recalbox ?

Funziona solamente con il Bluetooth. Accendi il controller tenendo premuti i pulsanti R1 + START.

Quando si utilizzano due pad zero V1:
- Primo controller, tieni premuti i pulsanti R1 + START
- Secondo controller, tieni premuti i pulsanti R1 + B + START

### Il tuo pad non viene riconosciuto con il cavo USB?

Verifica di utilizzare il cavo fornito con il controller, altrimenti assicurati di disporre di un cavo dati USB e non solo di ricarica USB. Alcuni cavi USB lasciano passare solo la corrente di carica e non il flusso di dati.