---
title: Contribuire alla Wiki di Recalbox
description: Per favore leggi questa guida prima di contribuire alla wiki.
published: true
date: 2021-06-17T21:40:06.692Z
tags: 
editor: markdown
dateCreated: 2021-06-17T21:26:10.640Z
---

## Sistema di Nomenclatura della Documentazione

La documentazione segue una certa nomenclatura che permette alla wiki di Recalbox di avere tutti URL identici per ogni pagina tradotta.

Prendi come esempio questa pagina. La traduzione in Francese ha questo URL:
`/fr/contribute/`
E quella in Italiano ha questa:
`/it/contribute`

Questo permette agli utenti di trovare facilmente la traduzione di una sezione, e permette a chi contribuisce a creare traduzioni in maniera più semplice.
È per questo motivo che gli URL di tutti i documenti compaiono sempre in Inglese.

## Regole di Formattazione

Tutti i documenti sono scritti in [markdown](https://docs.requarks.io/editors/markdown). Markdown è uno strumento molto semplice ed accessibile per la formattazione di documenti. Sii libero di usare questa pagina come esempio per iniziare a scrivere il tuo documento.

### Titoli

Il titolo della pagina è definito nelle opzioni della pagina dell'editor.
![title-metadata.png](/contribute/title-metadata.png)

Dovrebbe essere l'unico heading di livello 1 nel documento.

Nei contenuti della pagina, i titoli devono iniziare con un heading di livello 2 (il doppio hash: `##`):
![double-hash.png](/contribute/double-hash.png)

### Blocco Citazioni

Per permettere ai lettori di avere informazioni importanti o dettagli, si possono usare i blocchi per citazioni:
```markdown
> Questa è un'informazione!
> {.is-info}
```

> Questa è un'informazione!
> {.is-info}

## Versione supportata da Markdown

Per maggiori informazioni sulle capacità supportate da Markdown, visita https://docs.requarks.io/editors/markdown.

## Immagini

Quando aggiungi immagini ai tuoi documento, sii certo di replicare la gerarchia del documento nelle cartelle delle immagini, omettendo però la lingua.

Per esempio, in questa pagina, le immagini devono essere create nella cartella `/contribute`.

## Localizzazione

### Come tradurre una pagina

1. Loggati alla wiki con il tuo account Discord
2. Vai alla pagina che vuoi tradurre, nella lingua di default della wiki (Francese per adesso)
3. Clicca nell'icona della lingua in cima alla pagina e seleziona la lingua con cui vuoi tradurre la pagina
![select-lang.png](/contribute/select-lang.png)
4. Una schermata di permette di creare una nuova pagina, clicca sul tasto "Crea Pagina"
5. Clicca sull'editor Markdown
![markdown.png](/contribute/markdown.png)
6. Apri una nuova tab dalla pagina originale e clicca su Modifica nel menù delle azioni per la pagina
![edit.png](/contribute/edit.png)
7. Accedi al titolo, tag e descrizioni della pagina cliccando sul pulsante PAGINA:
![editpage.png](/contribute/editpage.png)
8. Puoi tradurre questi elementi ed aggiungerli al form della pagina appena creata
9. Dopo aver chiuso il popup della pagina originale, copia il contenuto markdown della pagina, e vai su  https://www.deepl.com/translator per incollare il testo e ricavare una versione pre-tradotta della pagina.
![deepl.png](/contribute/deepl.png)
10. Ora devi solamente ricontrollare la traduzione, le immagini ed i link della nuova pagina!
11. Non dimentcare di rimuovere l'ultima riga di testo (quella che dice che è stato tradotto con deepl)
12. Grazie ancora per il tuo conributo! 🙏


### Link Relativi

Eccetto per alcuni casi, i link delle pagine devono essere relativi alla lingua in uso corrente. Per esempio, in questa pagina, un link che rimanda alla pagina `/en/home` deve puntare a `./home`:

```markdown
Visita la [home page](./home)
```
Questo ti permetterà di creare nuove pagine per la tua lingua più facilmente, così non dovrai specificare la lingua ogni volta.

### Aggiungere nuove lingue

Se la tua lingua non appare nel menù della Wiki, e vuoi iniziare una traduzione, per favore contatta il team sui nostri [social networks, Discord, o sul forum](./presentation/useful-links).

## Livello difficoltà Tutorial

Per rendere l'esperienza di Recalbox divertente ed accessibile, quando crei un tutoria, ricorda di aggiungere un livello di difficoltà, calibrandolo secondo queste specifiche:

1. Qualsiasi azione che non richiede di modificare le opzioni avanzate del menù principale di Recalbox o il file `recalbox.conf` sarà considerato come : _level:star:_

1. Qualsiasi azione che richiede configurare le opzioni avanzate per avere delle modifiche che potrebbero causare instabilità: _level:star::star:_

1. Qualsiasi azione che richiede di cambiare linee di codice nel file `recalbox.conf`: _level :star::star::star:_ (richiesta attenzione maggiore).

1. Qualsiasi azione che richiede la manipolazione in SSH e/o modifica di file di boot (MobaXTerm oppure Powershell dove lo Staff non ha ancora deciso quale software usare): _level_ :star::star::star::star:

1. Qualsiasi azione che richiede di fare un'uscito o una richiesta di *merge*: _level_ :star::star::star::star::star:

1. Infine, qualsiasi tutorial che vorrebbe spiegare l'uso di clrmamepro verrà automaticamente classificato nella categoria: :scream: _**Nightmare**_

Esempi:

- Metti una rom, un bios, cambiare la lingua di sistema: :star:
- Cambiare un tema, reimpostare le opzioni di fabbrica, cambiare un emulatore/core di default: :star::star: 
- Attivare la traduzione AI di Retroarch: :star::star::star:

*Per creare l'emoji della stella e farla apparire nei tuoi tutorial, aggiungi ":" davanti e dopo la parola "star":*
```
:star:
```
